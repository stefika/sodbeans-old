/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.sodbeans.login.actions;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionRegistration;
import org.openide.util.NbBundle.Messages;
import org.sodbeans.login.authentication.AuthService;
import org.sodbeans.login.gui.GUIUtils;
import org.sodbeans.login.gui.LoginDialog;

@ActionID(
    category = "Window",
id = "org.sodbeans.login.actions.LoginAction")
@ActionRegistration(
    displayName = "#CTL_LoginAction")
@ActionReference(path = "Menu/Window", position = 3333)
@Messages("CTL_LoginAction=Log in to Sodbeans...")
public final class LoginAction implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent e) {
        // If the user is already logged in, and being logged in is required,
        // the user must restart sodbeans.
        String username = AuthService.getInstance().getUsername();
        
        if (username != null && AuthService.getInstance().getRequiredLogin()) {
            GUIUtils.showErrorMessage(null, "You are logged in as " + username + ". To log in with a different name, please reload Sodbeans.");
            return;
        }
        
        // Otherwise, show the form, without forcing the login.
        LoginDialog d = new LoginDialog(null, false);
        d.setVisible(true);
    }
}
