package org.sodbeans.login.gui;

import java.awt.Component;
import javax.swing.JOptionPane;
import org.sodbeans.phonemic.SpeechPriority;
import org.sodbeans.phonemic.TextToSpeechFactory;
import org.sodbeans.phonemic.tts.TextToSpeech;
import org.sodbeans.tts.options.api.TextToSpeechOptions;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author jeff
 */
public class GUIUtils {
    private static TextToSpeech speech = TextToSpeechFactory.getDefaultTextToSpeech();
    public static void showMessage(Component owner, String message) {
        showMessage(owner, message, "Sodbeans");
    }
    
    public static void showMessage(Component owner, String message, String title) {
        if (TextToSpeechOptions.isScreenReading()) {
            speech.speak(message + " Press OK to continue", SpeechPriority.HIGH);
        }
        JOptionPane.showMessageDialog(owner, message, title, JOptionPane.INFORMATION_MESSAGE);
    }
    
    public static void showErrorMessage(Component owner, String message) {
        showErrorMessage(owner, message, "Sodbeans");
    }
    
    public static void showErrorMessage(Component owner, String message, String title) {
        if (TextToSpeechOptions.isScreenReading()) {
            speech.speak(message + " Press OK to continue", SpeechPriority.HIGH);
        }
        JOptionPane.showMessageDialog(owner, message, title, JOptionPane.ERROR_MESSAGE);
    }}
