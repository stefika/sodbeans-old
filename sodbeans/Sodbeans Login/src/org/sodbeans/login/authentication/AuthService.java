/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.sodbeans.login.authentication;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openide.util.NbPreferences;

/**
 *
 * @author jeff
 */
public class AuthService {
    /**
     * The stored login username preference name.
     */
    private static String SODBEANS_LOGIN_USERNAME = "sodbeans_login_username";
    
    /**
     * Is the user required to log in?
     */
    private static String SODBEANS_REQUIRE_LOGIN = "sodbeans_require_login";
    
    /**
     * Singleton instance handle.
     */
    private static AuthService instance = null;
    
    /**
     * The URL for logins.
     */
    private static final String LOGIN_URL = "http://sbauth.quorumlanguage.com/authenticate.php";
    
    /**
     * The URL for forgotten passwords.
     */
    private static final String FORGOT_URL = "http://sbauth.quorumlanguage.com/forgot_password.php";
    
    /**
     * The URL for new accounts.
     */
    private static final String CREATE_URL = "http://sbauth.quorumlanguage.com/create_user.php";

    /**
     * Are they currently logged in?
     */
    private boolean loggedIn = false;
    
    /**
     * If the user is logged in, what is their username?
     */
    private String username = null;
    
    private AuthService() {
        // If there is a preference for the sodbeans username, they have already
        // authenticated previously.
        String loginUsername = NbPreferences.forModule(AuthService.class).get(SODBEANS_LOGIN_USERNAME, null);
        
        if (loginUsername != null) {
            this.loggedIn = true;
            this.username = loginUsername;
        }
    }
    
    /**
     * Update the logged in username (as well as the logged in state), and
     * save this data to the NetBeans preferences.
     * @param username 
     */
    private void setUsername(String username) {
        this.username = username;
        this.loggedIn = true;
        NbPreferences.forModule(AuthService.class).put(SODBEANS_LOGIN_USERNAME, username);
    }
    
    /**
     * Is the user required to log in? This is only true for school installations.
     * 
     * @param required 
     */
    public void setRequiredLogin(boolean required) {
        NbPreferences.forModule(AuthService.class).putBoolean(SODBEANS_REQUIRE_LOGIN, required);
    }
    
    /**
     * Is the user required to log in?
     * 
     * @return 
     */
    public boolean getRequiredLogin() {
        return NbPreferences.forModule(AuthService.class).getBoolean(SODBEANS_REQUIRE_LOGIN, false);
    }
    
    public static AuthService getInstance() {
        if (instance == null) {
            instance = new AuthService();
        }
        
        return instance;
    }
    
    public boolean login(String username, String password) throws IOException {
        URL loginURL = new URL(LOGIN_URL);
        HttpURLConnection connection = (HttpURLConnection)loginURL.openConnection();
        String urlParameters = "username=" + username + "&password=" + password;
        connection.setDoOutput(true);
        connection.setDoInput(true);
        connection.setInstanceFollowRedirects(false);
        connection.setRequestMethod("POST");
        connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        connection.setRequestProperty("charset", "utf-8");
        connection.setRequestProperty("Content-Length", "" + Integer.toString(urlParameters.getBytes().length));
        connection.setUseCaches (false);
        DataOutputStream wr = new DataOutputStream(connection.getOutputStream ());
        wr.writeBytes(urlParameters);
        wr.flush();
        wr.close();
        int result = connection.getResponseCode();
        connection.disconnect();
        
        setUsername(username);
        return result == 200;
    }
    
    public boolean forgotPassword(String username) throws IOException {
        URL loginURL = new URL(FORGOT_URL);
        HttpURLConnection connection = (HttpURLConnection)loginURL.openConnection();
        String urlParameters = "username=" + username;
        connection.setDoOutput(true);
        connection.setDoInput(true);
        connection.setInstanceFollowRedirects(false);
        connection.setRequestMethod("POST");
        connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        connection.setRequestProperty("charset", "utf-8");
        connection.setRequestProperty("Content-Length", "" + Integer.toString(urlParameters.getBytes().length));
        connection.setUseCaches (false);
        DataOutputStream wr = new DataOutputStream(connection.getOutputStream ());
        wr.writeBytes(urlParameters);
        wr.flush();
        wr.close();
        int result = connection.getResponseCode();
        connection.disconnect();
        return result == 200;
    }
    
    public boolean createAccount(String username, String password) throws IOException {
        URL loginURL = new URL(CREATE_URL);
        HttpURLConnection connection = (HttpURLConnection)loginURL.openConnection();
        String urlParameters = "username=" + username + "&password=" + password;
        connection.setDoOutput(true);
        connection.setDoInput(true);
        connection.setInstanceFollowRedirects(false);
        connection.setRequestMethod("POST");
        connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        connection.setRequestProperty("charset", "utf-8");
        connection.setRequestProperty("Content-Length", "" + Integer.toString(urlParameters.getBytes().length));
        connection.setUseCaches (false);
        DataOutputStream wr = new DataOutputStream(connection.getOutputStream ());
        wr.writeBytes(urlParameters);
        wr.flush();
        wr.close();
        int result = connection.getResponseCode();
        connection.disconnect();
        return result == 200;
    }
    
    /**
     * Log out the current user, erasing their username from the NetBeans
     * preferences.
     */
    public void logout() {
        this.username = null;
        this.loggedIn = false;
        NbPreferences.forModule(AuthService.class).put(SODBEANS_LOGIN_USERNAME, null);
    }
    
    /**
     * Determine whether or not the user is currently logged in.
     * @return 
     */
    public boolean isLoggedIn() {
        return this.loggedIn;
    }
    
    /**
     * Returns the username of the currently logged in user, or null
     * if no user is logged in.
     * 
     * @return 
     */
    public String getUsername() {
        return this.username;
    }
}
