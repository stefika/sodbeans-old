/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.sodbeans.tts.startup;

import java.awt.AWTEvent;
import java.awt.Component;
import java.awt.Container;
import java.awt.Toolkit;
import java.awt.Window;
import java.awt.event.AWTEventListener;
import java.awt.event.HierarchyEvent;
import java.awt.event.WindowEvent;
import java.util.prefs.Preferences;
import javax.accessibility.AccessibleContext;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JViewport;
import javax.swing.SwingUtilities;
import org.netbeans.spi.editor.hints.Fix;
import org.openide.modules.ModuleInstall;
import org.openide.util.Exceptions;
import org.openide.util.NbPreferences;
import org.sodbeans.phonemic.SpeechPriority;
import org.sodbeans.phonemic.TextToSpeechFactory;
import org.sodbeans.phonemic.tts.TextToSpeech;
import org.sodbeans.tts.options.api.TextToSpeechOptions;

/**
 * Manages a module's lifecycle. Remember that an installer is optional and
 * often not needed at all.
 */
public class Installer extends ModuleInstall {

    private static Starter starter = new Starter();
    private TextToSpeech speech = TextToSpeechFactory.getDefaultTextToSpeech();
    private static long event = 0;
    
    /**
     * Do we have permission to collect usage statistics information without
     * the user explicitly clicking that "I Agree" button? The school version
     * should be set to "true" and the general public release should be "false".
     */
    private static final boolean CAN_COLLECT_STATISTICS_BY_DEFAULT = Starter.REQUIRE_LOGIN;

    public Installer() {
    }

    @Override
    public void restored() {
        // Update the NetBeans Preferences for gesture collection, if it is
        // to be enabled automatically.        
        setUsageStatisticsFlags();
            
        starter.start();
        try {
            //if you need to log events to the console, uncomment this line
            //logEvents();
            Toolkit.getDefaultToolkit().addAWTEventListener(new GlobalListener(), AWTEvent.KEY_EVENT_MASK);

            // For the magnifier
            Toolkit.getDefaultToolkit().addAWTEventListener(new MagnifierListener(), AWTEvent.MOUSE_MOTION_EVENT_MASK | AWTEvent.WINDOW_EVENT_MASK);
            Toolkit.getDefaultToolkit().addAWTEventListener(new AWTEventListener() {

                public void eventDispatched(AWTEvent e) {

                    if (e instanceof WindowEvent) {
                        WindowEvent w = (WindowEvent) e;
                        String param = w.paramString();
                        if (w.getID() == WindowEvent.WINDOW_OPENED) {
                            
                            Window window  = w.getWindow();
                            // Is it a code completion window?
                            boolean isCodeCompletion = isCodeCompletionWindow(window, true, !CodeCompletionListener.isCodeCompletionOpen());

                            if (!isCodeCompletion) {
                                AccessibleContext ac = window.getAccessibleContext();
                                if (ac != null) {
                                    String name = ac.getAccessibleName();
                                    if (name == null) {
                                        name = "";
                                    }
                                    if (TextToSpeechOptions.isScreenReading() && !name.trim().isEmpty()) {
                                        speech.speak(name + " window opened", SpeechPriority.MEDIUM_HIGH);
                                    }
                                }
                            }
                        } else if (w.getID() == WindowEvent.WINDOW_CLOSING) {
                            Window window = w.getWindow();
                            AccessibleContext ac = window.getAccessibleContext();
                            // Is it a code completion window?
                            if (ac != null) {
                                String name = ac.getAccessibleName();
                                if (name == null) {
                                    name = "";
                                }
                                if (TextToSpeechOptions.isScreenReading()) {
                                    speech.speak(name + " window closing", SpeechPriority.MEDIUM_HIGH);
                                }
                            }
                        }
                    }
                }
            }, AWTEvent.WINDOW_EVENT_MASK);

            // Window opening/closing as well as Code Completion support
            Toolkit.getDefaultToolkit().addAWTEventListener(new AWTEventListener() {

                public void eventDispatched(AWTEvent e) {
                    if (e instanceof WindowEvent) {
                        final int MINIMIZED = 7;
                        final int MINIMIZED2 = 1;
                        WindowEvent w = (WindowEvent) e;
                        //I would think that there are constants in java.awt.Frame,
                        //but I did not immediatley see them for minimized windows.
                        if (w.getNewState() == MINIMIZED || w.getNewState() == MINIMIZED2) {
                            Window window = w.getWindow();
                            AccessibleContext ac = window.getAccessibleContext();
                            if (ac != null) {
                                String name = ac.getAccessibleName();
                                if (name == null) {
                                    name = "";
                                }
                                if (TextToSpeechOptions.isScreenReading()) {
                                    speech.speak(name + " window minimized", SpeechPriority.MEDIUM_HIGH);
                                }
                            }
                        }
                    }
                }
            }, AWTEvent.WINDOW_STATE_EVENT_MASK);

            // Editor hint support
            Toolkit.getDefaultToolkit().addAWTEventListener(new AWTEventListener() {

                public void eventDispatched(AWTEvent evt) {
                    HierarchyEvent e = (HierarchyEvent) evt;
                    if (e.getChangeFlags() == HierarchyEvent.DISPLAYABILITY_CHANGED) {
                        Object source = e.getSource();
                        
                        // Is this the code completion window?
                        if (source instanceof JList && source.getClass().getName().equals("org.netbeans.modules.editor.completion.CompletionJList")) {
                            JList jl = (JList)source;
                            if (jl.isDisplayable()) {
                                //if (!CodeCompletionListener.isCodeCompletionOpen()) {
                                    JList list = (JList) source;
                                    list.addListSelectionListener(CodeCompletionListener.getInstance());
                                    SwingUtilities.getWindowAncestor(jl).addWindowListener(CodeCompletionListener.getInstance());

                                    Object selectedValue = list.getSelectedValue();
                                    CodeCompletionListener.getInstance().read(selectedValue);
                                //}
                            }
                        }
                        // Is it the combo box popup?
                        if (source instanceof JList && source.getClass().getName().equals("org.netbeans.modules.editor.hints.borrowed.ListCompletionView")) {
                            JList list = (JList) source;

                            if (list.isDisplayable()) { // about to become visible
                                list.addListSelectionListener(EditorHintsListener.getInstance());
                                list.addComponentListener(EditorHintsListener.getInstance());

                                // Attempt to speak selection.
                                Object selectedValue = list.getSelectedValue();
                                if (TextToSpeechOptions.isScreenReading() && selectedValue != null && selectedValue instanceof Fix) {
                                    Fix f = (Fix) selectedValue;
                                    speech.speak(f.getText(), SpeechPriority.MEDIUM_HIGH);
                                }
                            } else {
                                if (TextToSpeechOptions.isScreenReading()) {
                                    speech.speak("Editor hints closing", SpeechPriority.MEDIUM_HIGH);
                                }
                            }
                        } // ... or the tooltip? Editor Tooltips, (as determined from the Netbeans Source),
                        // are instances of JTextAreas.
                        else if (source.getClass().getName().startsWith("org.netbeans.editor.ext.ToolTipSupport")) {
                            if (source instanceof JTextArea) {
                                JTextArea t = (JTextArea) source;
                                if (TextToSpeechOptions.isScreenReading() && t.isDisplayable()) // it is about to be displayed
                                {
                                    speech.speak("Tooltip: " + t.getText(), SpeechPriority.MEDIUM_HIGH);
                                }
                            }
                        }
                    }
                }
            }, AWTEvent.HIERARCHY_EVENT_MASK);

        } catch (Exception ex) {
            Exceptions.printStackTrace(ex);
        }
    }

    @Override
    public void close() {
        starter.close();
    }

    private void outputEventNumber() {
        System.out.print("" + event + " - ");
        event++;
    }

    public void logEvents() {
        Toolkit.getDefaultToolkit().addAWTEventListener(new AWTEventListener() {

            public void eventDispatched(AWTEvent e) {
                outputEventNumber();
                System.out.println("ACTION_EVENT_MASK: " + e);
            }
        }, AWTEvent.ACTION_EVENT_MASK);

        Toolkit.getDefaultToolkit().addAWTEventListener(new AWTEventListener() {

            public void eventDispatched(AWTEvent e) {
                outputEventNumber();
                System.out.println("ADJUSTMENT_EVENT_MASK: " + e);
            }
        }, AWTEvent.ADJUSTMENT_EVENT_MASK);

        Toolkit.getDefaultToolkit().addAWTEventListener(new AWTEventListener() {

            public void eventDispatched(AWTEvent e) {
                outputEventNumber();
                System.out.println("COMPONENT_EVENT_MASK: " + e);
            }
        }, AWTEvent.COMPONENT_EVENT_MASK);


        Toolkit.getDefaultToolkit().addAWTEventListener(new AWTEventListener() {

            public void eventDispatched(AWTEvent e) {
                outputEventNumber();
                System.out.println("CONTAINER_EVENT_MASK: " + e);
            }
        }, AWTEvent.CONTAINER_EVENT_MASK);

        Toolkit.getDefaultToolkit().addAWTEventListener(new AWTEventListener() {

            public void eventDispatched(AWTEvent e) {
                outputEventNumber();
                System.out.println("FOCUS_EVENT_MASK: " + e);
            }
        }, AWTEvent.FOCUS_EVENT_MASK);

        Toolkit.getDefaultToolkit().addAWTEventListener(new AWTEventListener() {

            public void eventDispatched(AWTEvent e) {
                outputEventNumber();
                System.out.println("HIERARCHY_BOUNDS_EVENT_MASK: " + e);
            }
        }, AWTEvent.HIERARCHY_BOUNDS_EVENT_MASK);


        Toolkit.getDefaultToolkit().addAWTEventListener(new AWTEventListener() {

            public void eventDispatched(AWTEvent e) {
                outputEventNumber();
                System.out.println("HIERARCHY_EVENT_MASK: " + e);
            }
        }, AWTEvent.HIERARCHY_EVENT_MASK);

        Toolkit.getDefaultToolkit().addAWTEventListener(new AWTEventListener() {

            public void eventDispatched(AWTEvent e) {
                outputEventNumber();
                System.out.println("INPUT_METHOD_EVENT_MASK: " + e);
            }
        }, AWTEvent.INPUT_METHOD_EVENT_MASK);

        Toolkit.getDefaultToolkit().addAWTEventListener(new AWTEventListener() {

            public void eventDispatched(AWTEvent e) {
                outputEventNumber();
                System.out.println("INVOCATION_EVENT_MASK: " + e);
            }
        }, AWTEvent.INVOCATION_EVENT_MASK);


        Toolkit.getDefaultToolkit().addAWTEventListener(new AWTEventListener() {

            public void eventDispatched(AWTEvent e) {
                outputEventNumber();
                System.out.println("ITEM_EVENT_MASK: " + e);
            }
        }, AWTEvent.ITEM_EVENT_MASK);

        Toolkit.getDefaultToolkit().addAWTEventListener(new AWTEventListener() {

            public void eventDispatched(AWTEvent e) {
                outputEventNumber();
                System.out.println("KEY_EVENT_MASK: " + e);
            }
        }, AWTEvent.KEY_EVENT_MASK);

        Toolkit.getDefaultToolkit().addAWTEventListener(new AWTEventListener() {

            public void eventDispatched(AWTEvent e) {
                outputEventNumber();
                System.out.println("MOUSE_EVENT_MASK: " + e);
            }
        }, AWTEvent.MOUSE_EVENT_MASK);

        Toolkit.getDefaultToolkit().addAWTEventListener(new AWTEventListener() {

            public void eventDispatched(AWTEvent e) {
                outputEventNumber();
                System.out.println("MOUSE_MOTION_EVENT_MASK: " + e);
            }
        }, AWTEvent.MOUSE_MOTION_EVENT_MASK);

        Toolkit.getDefaultToolkit().addAWTEventListener(new AWTEventListener() {

            public void eventDispatched(AWTEvent e) {
                outputEventNumber();
                System.out.println("MOUSE_WHEEL_EVENT_MASK: " + e);
            }
        }, AWTEvent.MOUSE_WHEEL_EVENT_MASK);

        Toolkit.getDefaultToolkit().addAWTEventListener(new AWTEventListener() {

            public void eventDispatched(AWTEvent e) {
                //outputEventNumber();
                //System.out.println("PAINT_EVENT_MASK: " + e);
            }
        }, AWTEvent.PAINT_EVENT_MASK);

        Toolkit.getDefaultToolkit().addAWTEventListener(new AWTEventListener() {

            public void eventDispatched(AWTEvent e) {
                //outputEventNumber();
                //System.out.println("RESERVED_ID_MAX: " + e);
            }
        }, AWTEvent.RESERVED_ID_MAX);

        Toolkit.getDefaultToolkit().addAWTEventListener(new AWTEventListener() {

            public void eventDispatched(AWTEvent e) {
                outputEventNumber();
                System.out.println("TEXT_EVENT_MASK: " + e);
            }
        }, AWTEvent.TEXT_EVENT_MASK);

        Toolkit.getDefaultToolkit().addAWTEventListener(new AWTEventListener() {

            public void eventDispatched(AWTEvent e) {
                outputEventNumber();
                System.out.println("WINDOW_EVENT_MASK: " + e);
            }
        }, AWTEvent.WINDOW_EVENT_MASK);

        Toolkit.getDefaultToolkit().addAWTEventListener(new AWTEventListener() {

            public void eventDispatched(AWTEvent e) {
                outputEventNumber();
                System.out.println("WINDOW_FOCUS_EVENT_MASK: " + e);
            }
        }, AWTEvent.WINDOW_FOCUS_EVENT_MASK);

        Toolkit.getDefaultToolkit().addAWTEventListener(new AWTEventListener() {

            public void eventDispatched(AWTEvent e) {
                outputEventNumber();
                System.out.println("WINDOW_STATE_EVENT_MASK: " + e);
            }
        }, AWTEvent.WINDOW_STATE_EVENT_MASK);
    }

    /**
     * Determine if the given window instance is a code-completion pop-up.
     * Optionally, the appropriate event listeners are also added.
     *
     * @param window
     * @param connectListeners should the key and list listeners be hooked?
     * @param speak should the current selection be spoken?
     * @return
     */
    private boolean isCodeCompletionWindow(Window window, boolean connectListeners, boolean speak) {
        try {
            Component component = window.getComponent(0);
            if (component instanceof JRootPane) {
                JRootPane jrp = (JRootPane) component;
                    Component firstComponent = jrp.getContentPane().getComponent(0);
                if (firstComponent != null && firstComponent instanceof Container) {
                    Container c = (Container) firstComponent;
                    return findCompletionJList(window, c, connectListeners, speak);
                }
            }
        } catch (Exception e) {
        }

        return false;
    }

    private boolean findCompletionJList(Window window, Container c, boolean connectListeners, boolean speak) {
                Component[] components = c.getComponents();
        for (int i = 0; i < components.length; i++) {
            Component o = components[i];
            if (o.getClass().getName().equals("org.netbeans.modules.editor.completion.CompletionJList")) {
                if (connectListeners) {
                    JList list = (JList) o;
                    list.addListSelectionListener(CodeCompletionListener.getInstance());
                    window.addWindowListener(CodeCompletionListener.getInstance());

                    if (speak) {
                        Object selectedValue = list.getSelectedValue();
                        CodeCompletionListener.getInstance().read(selectedValue);
                    }
                }
                return true;
            } else if (o instanceof Container) {
                return findCompletionJList(window, (Container) o, connectListeners, speak);
            }
        }

        return false;
    }

    /**
     * If this release is being built for schools where we have legal permission
     * to collect usage statistics collection by default, set the appropriate
     * NbPreferences option before Sodbeans has loaded the main window.
     */
    private void setUsageStatisticsFlags() {
        Preferences autoPrefs = NbPreferences.root().node("org/netbeans/modules/uihandler");
        autoPrefs.putBoolean("autoSubmitWhenFull", true);
        if (CAN_COLLECT_STATISTICS_BY_DEFAULT) {
            // These values were copied directly from the NetBeans source code,
            // specifically, the file
            // uihandler/src/org/netbeans/modules/uihandler/Installer.java,
            // in the package org.netbeans.modules.uihandler, class Installer.
            String USAGE_STATISTICS_ENABLED = "usageStatisticsEnabled"; // NOI18N
            String USAGE_STATISTICS_SET_BY_IDE = "usageStatisticsSetByIde"; // NOI18N
            String USAGE_STATISTICS_NB_OF_IDE_STARTS = "usageStatisticsNbOfIdeStarts"; // NOI18N
            String CORE_PREF_NODE = "org/netbeans/core"; // NOI18N
            Preferences corePref = NbPreferences.root().node(CORE_PREF_NODE);

            // We need to set "usageStatisticsEnabled" and "usageStatisticsSetByIde" to true to avoid
            // the "enable usage statistics" pop-up.
            corePref.putBoolean(USAGE_STATISTICS_ENABLED, true);
            corePref.putBoolean(USAGE_STATISTICS_SET_BY_IDE, true);
            //}
        }
    }
}
