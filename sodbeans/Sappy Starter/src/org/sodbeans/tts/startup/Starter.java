/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.sodbeans.tts.startup;

import javax.swing.JFrame;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.magnification.options.api.MagnificationOptions;
import org.magnify.MagnifierFactory;
import org.magnify.MagnifierInterface;
import org.openide.awt.StatusDisplayer;
import org.openide.util.Lookup;
import org.openide.util.Lookup.Result;
import org.openide.windows.WindowManager;
import org.sodbeans.controller.api.ScreenReader;
import org.sodbeans.login.authentication.AuthService;
import org.sodbeans.login.gui.LoginDialog;
import org.sodbeans.phonemic.SpeechLanguage;
import org.sodbeans.phonemic.SpeechPriority;
import org.sodbeans.phonemic.SpeechVoice;
import org.sodbeans.phonemic.TextToSpeechFactory;
import org.sodbeans.phonemic.tts.TextToSpeech;
import org.sodbeans.phonemic.tts.TextToSpeechEngine;
import org.sodbeans.tts.options.api.TextToSpeechOptions;

/**
 * This class starts up the system and gives the user notification if the system
 * is ready to go.
 *
 * @author Andreas Stefik
 */
public class Starter implements Runnable, ChangeListener {

    private StatusDisplayer displayer;
    private boolean first = true;
    private TextToSpeech speech = TextToSpeechFactory.getDefaultTextToSpeech();
    private MagnifierInterface magnifier = MagnifierFactory.getDefaultMagnifier();
    //TextToSpeech ttsService = Lookup.getDefault().lookup(TextToSpeech.class);
    ScreenReader accessibleController = Lookup.getDefault().lookup(ScreenReader.class);
    private String frameTitle = "";
    private static final String SODBEANS_VERSION = "Sodbeans 4.6";
    /**
     * Should a login be required? This flag is set to true for school
     * installations, allowing us to track usage data on a per-user basis.
     */
    public static final boolean REQUIRE_LOGIN = false;

    public Starter() {
    }

    public void start() {
        WindowManager manager = WindowManager.getDefault();
        manager.invokeWhenUIReady(this);
        displayer = StatusDisplayer.getDefault();
        displayer.addChangeListener(this);
    }

    public void run() {
        JFrame frame = (JFrame) WindowManager.getDefault().getMainWindow();
        if (TextToSpeechOptions.isScreenReading()) {


            String title = SODBEANS_VERSION;
            frame.setTitle(title);
            frameTitle = frame.getTitle();
            if (TextToSpeechOptions.isScreenReading() && !REQUIRE_LOGIN) {
                speech.speak("Finished loading " + frameTitle + ". ", SpeechPriority.HIGH);
            }



            loadMagnifierOptions();
        }
        displayer.removeChangeListener(this);
        accessibleController.gainControl();

        // If we require logins and the user is NOT logged in, show the
        // login form. They will not be able to proceed until a valid
        // login is given.
        AuthService.getInstance().setRequiredLogin(REQUIRE_LOGIN);
        if (REQUIRE_LOGIN) {
            LoginDialog f = new LoginDialog(frame, true);
            f.setVisible(true);
        }
    }

    public void close() {
        if (TextToSpeechOptions.isScreenReading()) {
            speech.speak("Leaving " + frameTitle + ". ");
        }
    }

    public void stateChanged(ChangeEvent e) {
        if (first) {
            loadTTSOptions();
            if (TextToSpeechOptions.isScreenReading()) {
                if (TextToSpeechOptions.isScreenReading()) {
                    speech.speak("Initializing " + SODBEANS_VERSION + ", please wait.");
                }
                first = false;
            }
        }
    }

    /**
     * Loads the user's TTS preferences.
     */
    private void loadTTSOptions() {
        // Set the engine first.
        String engineName = TextToSpeechOptions.getSpeechEngine();

        // Make sure it isn't the default engine option.
        if (!engineName.equals("")) {
            speech.setTextToSpeechEngine(TextToSpeechEngine.valueOf(engineName));
        } else {
            // If there's no speech engine, and we're on Windows, choose SAPI
            // by default.
            String os = System.getProperty("os.name");
            if (os.startsWith("Windows")) {
                speech.setTextToSpeechEngine(TextToSpeechEngine.MICROSOFT_SAPI);
            }
        }

        // Set whether or not speech is enabled.
        speech.setSpeechEnabled(TextToSpeechOptions.isSpeechEnabled());

        // Set the voice, if it is set in preferences.
        String voice = TextToSpeechOptions.getSelectedVoice();

        if (!voice.equals("")) {
            speech.setVoice(new SpeechVoice(voice, SpeechLanguage.ENGLISH_US));
        }

        // Set the volume, speed and pitch.
        speech.setVolume((double) TextToSpeechOptions.getSpeechVolume() / 100.0);
        speech.setSpeed((double) TextToSpeechOptions.getSpeechSpeed() / 100.0);
        speech.setPitch((double) TextToSpeechOptions.getSpeechPitch() / 100.0);
    }

    /**
     * Loads the user's magnifier preferences.
     */
    private void loadMagnifierOptions() {
        // Should we start it?
        if (MagnificationOptions.isMagnificationEnabled()) {
            magnifier.start();
            magnifier.setFullScreen(MagnificationOptions.isFullscreenEnabled());
            magnifier.setZoom(MagnificationOptions.getZoomLevel());
            magnifier.setSize(MagnificationOptions.getMagnifierHeight(), MagnificationOptions.getMagnifierWidth());
        }
    }
}
