/*
 Copyright (c) 2013, Andreas Stefik
 All rights reserved.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met: 

 1. Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer. 
 2. Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution. 

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 The views and conclusions contained in the software and documentation are those
 of the authors and should not be interpreted as representing official policies, 
 either expressed or implied, of the FreeBSD Project.
 */
package org.sodbeans.actions;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import org.debugger.Debugger;
import org.openide.util.Lookup;
import org.quorum.debugger.DebuggerFactory;
import org.sodbeans.phonemic.SpeechPriority;
import org.sodbeans.phonemic.TextToSpeechFactory;
import org.sodbeans.phonemic.tts.TextToSpeech;
import org.sodbeans.tts.options.api.TextToSpeechOptions;
import org.tod.TODSessionFactory;
import org.tod.TODUtils;

/**
 * 
 * This class controls what happens when the user clicks "Halt Debugging".
 * 
 * @author Andreas Stefik
 */
public final class SodbeansStopDebuggerAction implements ActionListener {
    private final Debugger debugger = DebuggerFactory.getQuorumDebugger();
    private final TextToSpeech speech = TextToSpeechFactory.getDefaultTextToSpeech();
    
    public void actionPerformed(ActionEvent e) {
        debugger.stop();
        String message = "Debugging session ended";
        if (TextToSpeechOptions.isScreenReading()) {
            speech.speak(message, SpeechPriority.HIGHEST);
        }
        
        if(SodbeansDebugAction.progress != null) {
            SodbeansDebugAction.progress.finish();
        }
        // Get the compiler, text-to-speech engine, and Windows controls
//        org.sodbeans.compiler.api.Compiler compiler =
//                Lookup.getDefault().lookup(org.sodbeans.compiler.api.Compiler.class);
//        TextToSpeech speech = TextToSpeechFactory.getDefaultTextToSpeech();
//
//        if (!TODUtils.isTODEnabled()) {
//            // If the debugger is inactive, and you aren't running a program,
//            // then there is nothing to do
//            if (!compiler.isExecuting() && !compiler.isDebuggerActive())
//               return;
//
//            // Stop the debugger
//            compiler.stopDebugger(false);
//            speech.stop();
//            String message = "Stop debugging";
//            if (TextToSpeechOptions.isScreenReading()) {
//                speech.speak(message, SpeechPriority.HIGHEST);
//            }
//        } else {
//            // Stop the TOD Session.
//            TODSessionFactory.getDefault().getActionHandler().stop();
//            speech.stop();
//            String message = "Debugging session ended";
//            if (TextToSpeechOptions.isScreenReading()) {
//                speech.speak(message, SpeechPriority.HIGHEST);
//            }
//        }
    }
}
