/*
 Copyright (c) 2013, Andreas Stefik
 All rights reserved.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met: 

 1. Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer. 
 2. Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution. 

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 The views and conclusions contained in the software and documentation are those
 of the authors and should not be interpreted as representing official policies, 
 either expressed or implied, of the FreeBSD Project.
 */
package org.sodbeans.actions;

import java.awt.Desktop;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Date;
import java.util.Iterator;
import java.util.Properties;
import org.debugger.Debugger;
import org.netbeans.api.progress.ProgressHandle;
import org.netbeans.api.progress.ProgressHandleFactory;
import org.netbeans.api.project.Project;
import org.openide.filesystems.FileUtil;
import org.openide.util.Cancellable;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.quorum.debugger.DebuggerFactory;
import org.quorum.web.QuorumServerHelper;
import static org.sodbeans.actions.SodbeansRunAction.QUORUM_COMPILED_WEB_PROJECT;
import static org.sodbeans.actions.SodbeansRunAction.QUORUM_PROJECT_TYPE;
import org.sodbeans.phonemic.SpeechPriority;
import org.sodbeans.phonemic.TextToSpeechFactory;
import org.sodbeans.phonemic.tts.TextToSpeech;
import org.sodbeans.api.quorum.MainFileProvider;
import org.sodbeans.compiler.api.CompilerEvent;
import org.sodbeans.compiler.api.CompilerListener;
import org.sodbeans.compiler.api.descriptors.CompilerErrorDescriptor;
import org.sodbeans.io.CommandLine;
import org.sodbeans.tts.options.api.TextToSpeechOptions;

/**
 * 
 * This class controls the "Debug Main Project" button event. It will 
 * start a new process and hook this process into a debugger for Quorum.
 * 
 * @author Andreas Stefik
 */
public final class SodbeansDebugAction extends SodbeansAction implements ActionListener {
    private Project projectRequestedToRun = null;
    private org.sodbeans.compiler.api.Compiler compiler =
            Lookup.getDefault().lookup(org.sodbeans.compiler.api.Compiler.class);
    private TextToSpeech speech = TextToSpeechFactory.getDefaultTextToSpeech();
    private CommandLine console = Lookup.getDefault().lookup(CommandLine.class);
    private final Debugger debugger = DebuggerFactory.getQuorumDebugger();
    private boolean buildRequested = false;
    private String taskName = "";
    public static ProgressHandle progress;
    private static final QuorumServerHelper javaEEServer = QuorumServerHelper.getInstance();
    
    public SodbeansDebugAction() {
        compiler.addListener(new CompilerListener() {
            public void actionPerformed(CompilerEvent event) {
                if (!buildRequested) {
                    return;
                }
                if (event.isBuildAllEvent()) {
                    Date date = new Date();
                    endTime = date.getTime();

                    String message = "";
                    if (event.isBuildSuccessful()) {
                        addSuccessfulBuildLog(startTime, endTime, true, 0);
                        
                        // Get the project the user is referencing
                        final Project proj = projectRequestedToRun;
                        MainFileProvider provider = getMainFile(proj);
                        if(!isProjectSetupCorrectly(provider)) {
                            return;
                        }
                        
                        Properties properties = proj.getLookup().lookup(Properties.class);
                        String property = properties.getProperty(QUORUM_PROJECT_TYPE);
                        if(property != null && property.compareTo(QUORUM_COMPILED_WEB_PROJECT) == 0) {
                            File war = new File(proj.getProjectDirectory().getPath() + "/Run/Default.war");
                            String deploy = javaEEServer.deploy(FileUtil.toFileObject(war), true);
                            
                            debugger.setRemoteDebugging(true);
                            debugger.setRemotePort(9009);
                            debugger.setHost("localhost");
                            debugger.launch();
                            
                            try { //it's a web browser, so load the browser instead of running the program.
                                Desktop.getDesktop().browse(new URI(deploy));
                            } catch (URISyntaxException ex) {
                                    Exceptions.printStackTrace(ex);
                            } catch (IOException ex) {
                                Exceptions.printStackTrace(ex);
                            }
                        } else { //console project
                            debugger.setRemoteDebugging(false);
                            File projectFolder = new File(proj.getProjectDirectory().getPath());
                            debugger.setWorkingDirectory(proj.getProjectDirectory().getPath());
                            String location = projectFolder + "/Run/Default.jar";
                            debugger.setExecutable(location);
                            debugger.launch();
                            
                            taskName = proj.getProjectDirectory().getName() + " (run)";
                            final Thread currentThread = Thread.currentThread();
                            progress = ProgressHandleFactory.createHandle(taskName, new Cancellable() {
                            public boolean cancel() {
                                currentThread.interrupt();
                                if(progress != null) {
                                    progress.finish();
                                }
                                return true;
                            }
                            });
                            progress.start();

                            QuorumProcessWatcher watch = new QuorumProcessWatcher(debugger.getInputStream());
                            watch.start();
                        }
                        
                        
                        
                        
                        debugger.forward();
                    } else {
                        int numberErrors = compiler.getCompilerErrorManager().getNumberOfSyntaxErrors();
                        addSuccessfulBuildLog(startTime, endTime, false, numberErrors);

                        Iterator<CompilerErrorDescriptor> errorIt = compiler.getCompilerErrorManager().iterator();
                        while (errorIt.hasNext()) {
                            CompilerErrorDescriptor error = errorIt.next();
                            addCompilerErrorBuildLog(error);
                        }
                        message = computeStandardSpokenCompilerError(event, compiler);
                        if (TextToSpeechOptions.isScreenReading()) {
                            speech.speak(message, SpeechPriority.HIGHEST);
                        }
                    }
                    buildRequested = false;
                }
            }
        });
    }

    public void actionPerformed(ActionEvent e) {
        if (compiler.isDebuggerActive()) {
            compiler.stopDebugger(true);
        }

        // Get the project the user is referencing
        Project proj = getAppropriateProject(isMainMenuRequest(e));
        
        MainFileProvider provider = getMainFile(proj);
        if(!isProjectSetupCorrectly(provider)) {
            return;
        }

        SodbeansAction.server.setProject(proj);
        SodbeansAction.server.setProvider(provider);
        projectRequestedToRun = proj;
        
        // Compile the project and display the message
        compiler.setBuildFolder(provider.getBuildDirectory());
        compiler.setDistributionFolder(provider.getDistributionDirectory());
        compiler.clearDependencies();
        SodbeansBuildAction.addCompilerDependencies(compiler);
        buildRequested = true;
        compiler.compile(proj);
    }

    @Override
    protected String getDisplayName() {
        return "Debug";
    }

    public boolean wasBuildSuccessful() {
        return compiler.wasBuildSuccessful();
    }

    /**
     * This private class watches the process running in the debugger and 
     * outputs any information it dumps to standard out to the console.
     */
    private class QuorumProcessWatcher implements Runnable {
        private BufferedReader bufferedReader = null;
        private Thread blinker = null;
        private boolean running = false;

        public QuorumProcessWatcher(InputStream in) {
            bufferedReader = new BufferedReader(new InputStreamReader(in));
        }

        public void start() {
            if (!running) {
                blinker = new Thread(this);
                blinker.setDaemon(true);
                blinker.setName("Quorum Process Watcher");
                blinker.start();
            }
        }

        public void run() {
            Thread thisThread = Thread.currentThread();
            running = true;
            // Watch the input stream, send its output to the console.
            while (thisThread == blinker) {
                try {
                    String line = bufferedReader.readLine();
                    // If the line is null, the end of the input has been reached.
                    if (line == null) {
                        return;
                    }
                    console.post(line);
                    Thread.sleep(0);
                } catch (IOException ex) {
                    return;
                } catch (InterruptedException ex) {
                    return;
                }
            }
            if(progress != null) {
                progress.finish();
            }
        }
    }
}
