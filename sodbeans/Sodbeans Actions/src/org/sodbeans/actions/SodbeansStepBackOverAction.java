/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sodbeans.actions;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import org.openide.util.Lookup;
import org.tod.TODSessionFactory;
import org.tod.TODUtils;

/**
 *
 * @author Andreas Stefik
 */
public class SodbeansStepBackOverAction implements ActionListener {

    public void actionPerformed(ActionEvent e) {

        // Get the compiler, text-to-speech engine, and Windows controls
        org.sodbeans.compiler.api.Compiler compiler =
                Lookup.getDefault().lookup(org.sodbeans.compiler.api.Compiler.class);

        if (!TODUtils.isTODEnabled()) {
            // If the debugger is inactive, do nothing
            if (!compiler.isDebuggerActive())
               return;
            //Do a stepInto back over
            compiler.stepBackOver();
        } else {
            TODSessionFactory.getDefault().getActionHandler().stepBackOver();
        }
    }

}
