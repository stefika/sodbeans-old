/*
 * This class controls what happens when the user selects the "Step Back"
 * button action.
 *
 */
package org.sodbeans.actions;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import org.openide.util.Lookup;
import org.tod.TODSessionFactory;
import org.tod.TODUtils;

public final class SodbeansStepBackIntoAction implements ActionListener {

    public void actionPerformed(ActionEvent e) {

        // Get the compiler, text-to-speech engine, and Windows controls
        org.sodbeans.compiler.api.Compiler compiler =
                Lookup.getDefault().lookup(org.sodbeans.compiler.api.Compiler.class);

        if (!TODUtils.isTODEnabled()) {
            // If the debugger is inactive, do nothing
            if (!compiler.isDebuggerActive())
               return;

        // Step backward in execution
        compiler.stepBackInto();
        } else {
            TODSessionFactory.getDefault().getActionHandler().stepBackInto();
        }
    }
}
