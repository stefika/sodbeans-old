/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sodbeans.actions;

import java.awt.event.ActionEvent;
import java.beans.PropertyChangeListener;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import javax.swing.Action;
import org.netbeans.api.project.FileOwnerQuery;
import org.netbeans.api.project.Project;
import org.netbeans.api.project.ui.OpenProjects;
import org.openide.filesystems.FileObject;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.Utilities;
import org.sodbeans.api.quorum.MainFileProvider;
import org.sodbeans.compiler.api.CompilerEvent;
import org.sodbeans.compiler.api.descriptors.CompilerErrorDescriptor;

/**
 *
 * @author Andreas Stefik
 */
public abstract class SodbeansAction implements Action{
    protected static final QuorumWebServer server = new QuorumWebServer();
    
    static { //start the web server once and only once
        server.start();
    }
    
    protected boolean enabled = true;
    private HashMap<String, String> values = new HashMap<String, String>();
    protected long startTime = 0;
    protected long endTime = 0;
    private static final String UI_LOGGER_NAME = "org.netbeans.ui.sodbeansBuild";
    //private static final Lookup.Result lookupResult = Utilities.actionsGlobalContext().lookupResult(Project.class);
    private static final Lookup.Result globalSelectedFileObjectForOwnerQuery = Utilities.actionsGlobalContext().lookupResult(FileObject.class);
    protected static Project globalProjectSelection = null;
    protected static MainFileProvider globalSelectionProvider = null;
    protected static FileObject globalSelectionMainFile = null;
    protected static Collection globalSelectedProjects = null;
    protected static FileObject currentlySelectedFile = null;
    
    static {
//        lookupResult.addLookupListener(new LookupListener() {
//            public void resultChanged(LookupEvent event) {
//                Collection fileObjects = ((Lookup.Result) event.getSource()).allInstances();
//                globalSelectedProjects = fileObjects;
//                if(fileObjects.size() == 1) {
//                    globalProjectSelection = (Project) fileObjects.iterator().next();
//                    
//                    //if the project is a QuorumProject isntance, it will provide
//                    //MainFileProvider, which says where to run from
//                    globalSelectionProvider = globalProjectSelection.getLookup().lookup(MainFileProvider.class);
//                    if (globalSelectionProvider != null && globalSelectionProvider.getMainFile() != null) {
//                        //a main is set and this is a quorum project
//                        globalSelectionMainFile = globalSelectionProvider.getMainFile();
//                    } else {
//                        globalSelectionMainFile = null;
//                    }
//                } else if (fileObjects.size() > 1 || fileObjects.isEmpty()) {
//                    globalProjectSelection = null;
//                    globalSelectionProvider = null;
//                    globalSelectionMainFile = null;
//                }
//                int a = 5;
//            }
//        } );
        
        
        globalSelectedFileObjectForOwnerQuery.addLookupListener(new LookupListener() {
            public void resultChanged(LookupEvent event) {
                try {
                    Collection fileObjects = ((Lookup.Result) event.getSource()).allInstances();
                    if(fileObjects.size() >= 1) {
                        FileObject file = (FileObject) fileObjects.iterator().next();
                        if(file != null) {
                            globalProjectSelection = FileOwnerQuery.getOwner(file);
                            //if the project is a QuorumProject isntance, it will provide
                            //MainFileProvider, which says where to run from
                            Lookup lookup = globalProjectSelection.getLookup();
                            globalSelectionProvider = lookup.lookup(MainFileProvider.class);
                            if (globalSelectionProvider != null && globalSelectionProvider.getMainFile() != null) {
                                //a main is set and this is a quorum project
                                globalSelectionMainFile = globalSelectionProvider.getMainFile();
                            } else {
                                globalSelectionMainFile = null;
                            }
                        }
                    } else if (fileObjects.size() > 1 || fileObjects.isEmpty()) {
                        globalProjectSelection = null;
                        globalSelectionProvider = null;
                        globalSelectionMainFile = null;
                    }
                } catch (Exception e) {//if something goes wrong, just ignore it.
                }
            }
        });
    }
    public SodbeansAction() {
        values.put("popupText", getDisplayName());
    }
    
    public static String computeStandardSpokenCompilerError(CompilerEvent event,
            org.sodbeans.compiler.api.Compiler compiler) {
        String message = "";
        int errors = compiler.getCompilerErrorManager().getNumberOfSyntaxErrors();
        if(errors > 1) {
            message = errors + " errors. The first is ";
        }
        else {
            message = "Error, ";
        }

        Iterator<CompilerErrorDescriptor> iterator = compiler.getCompilerErrorManager().iterator();
        if(iterator.hasNext()) {
            CompilerErrorDescriptor next = iterator.next();
            message += next.getDescription();
        }
        else {
            message += "unknown";
        }
        return message;
    }

    /**
     * Returns a project with the following rules:
     * 
     * 1. If a main project is set, it returns that.
     * 2. If a main project is not set, but there is a global project selection, 
     *      it returns that.
     * 3. If there is no main project set and no global selection, but there
     *      is only one project open, it returns that. 
     * 4. If none of the above is true, it returns null.
     * 
     * Additionally, before a project is returns, it checks to see if this
     * project is a quorum project. If it is not, this returns null.
     * 
     * @return 
     */
    private Project getMainProject() {
        OpenProjects open = OpenProjects.getDefault();
        Project project = open.getMainProject();
        
        //in NetBeans 7.2, we may not have a main project
        //check the global selection to see if there's a project in there
        if(project == null) { 
            project = globalProjectSelection;
        }
        
        //if their is no main project and the global selection does
        //not contain a quorum project, check to see if there's only 
        //one project
        if(project == null && open.getOpenProjects()!= null && open.getOpenProjects().length == 1) {
            project = open.getOpenProjects()[0];
        }
        
        if(project != null) {
            MainFileProvider localProvider = 
                project.getLookup().lookup(MainFileProvider.class);
            if (localProvider == null) {
                //either this is not a quorum project or we should return
                return null;
            }
        }
        
        return project;
    }
    
    /**
     * This method returns fileObjects with similar rules to getMainProject. The
     * primary difference with this method is that the main project setting is
     * ignored in favor of the global selection. If the global selection
     * does not exist, then this method checks to see if there is only one
     * open project. If there is, it uses that. If not, then it checks to see
     * if there is a main project set as a last resort. In other words, the ordering
     * is different than getMainProject.
     * 
     * @return 
     */
    private Project getSelectedProject() {
        OpenProjects open = OpenProjects.getDefault();
        Project project = globalProjectSelection;
        
        
        //if their is no global selection does
        //not contain a quorum project, check to see if there's only 
        //one project
        if(project == null && open.getOpenProjects()!= null && open.getOpenProjects().length == 1) {
            project = open.getOpenProjects()[0];
        }
        
        //if it's still null, check the main project as a last resort 
        if(project == null) {
            project = open.getMainProject();
        }
        
        if(project != null) {
            MainFileProvider localProvider = 
                project.getLookup().lookup(MainFileProvider.class);
            if (localProvider == null) {
                //either this is not a quorum project or we should return
                return null;
            }
        }
        
        return project;
    }
    
    /**
     * This method returns a project depending on whether the request 
     * comes from a popup menu on the project or from a main menu. This method
     * conforms (hopefully) to the new spec for NetBeans 7.2.
     * 
     * @param isMainMenuRequest
     * @return 
     */
    public Project getAppropriateProject(boolean isMainMenuRequest) {
        if(isMainMenuRequest) {
            return getMainProject();
        } else {
            return getSelectedProject();
        }
    }
    
    /**
     * If this project is a Quorum project, it must have set a main file
     * for the particular project. This method checks to see if the project
     * is a Quorum project and then checks that it has a main file set. If it
     * does have these properties, the main file is returned. Otherwise, 
     * null is returned.
     * 
     * @param project
     * @return 
     */
    public MainFileProvider getMainFile(Project project) {
        if(project != null) {
            MainFileProvider localProvider = 
                project.getLookup().lookup(MainFileProvider.class);
            if (localProvider == null) {
                //either this is not a quorum project or we should return
                return null;
            } else {
                return localProvider;
            }
        }
        return null;
    }
    
    /**
     * Given a particular action event, this method returns whether or
     * not this event was fired from the main menu in NetBeans or whether 
     * it was a request from a popup menu selected on a quorum project.
     * 
     * @param event
     * @return 
     */
    public boolean isMainMenuRequest(ActionEvent event) {
        if(event == null) {
            return true;
        }
        return false;
    }
    
    public boolean isProjectSetupCorrectly(MainFileProvider provider) {
        if(provider == null || provider.getMainFile() == null) {
            return false;
        }
        return true;
    }
    
    public Object getValue(String key) {
        return values.get(key);
    }

    public void putValue(String key, Object value) {
    }

    public void setEnabled(boolean b) {
        enabled = b;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
    }

    public abstract void actionPerformed(ActionEvent e);

    protected abstract String getDisplayName();
    
    public static void addSuccessfulBuildLog(long startTime, long endTime, boolean buildSuccess, int compilerErrorCount) {
        LogRecord record = new LogRecord(Level.INFO, "SODBEANS_BUILD");
        record.setParameters(new Object[]{startTime, endTime, buildSuccess, compilerErrorCount});
        record.setLoggerName(UI_LOGGER_NAME);
        Logger.getLogger(UI_LOGGER_NAME).log(record);
    }
    
    public static void addCompilerErrorBuildLog(CompilerErrorDescriptor error){
        LogRecord record = new LogRecord(Level.INFO, "SODBEANS_COMPILER_ERROR");
        record.setParameters(new Object[]{error.getErrorType().toString(), error.getFileName(), error.getAbsolutePath(), error.getLine(), error.getDescription()});
        record.setLoggerName(UI_LOGGER_NAME);
        Logger.getLogger(UI_LOGGER_NAME).log(record);
    }

}
