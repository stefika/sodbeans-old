/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.sodbeans.actions;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
import java.awt.Desktop;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.api.project.Project;
import org.openide.filesystems.FileUtil;
import org.sodbeans.api.quorum.MainFileProvider;

/**
 *
 * @author astefik
 */
public class QuorumWebServer {
    private HttpServer server;
    private String jarLocation = null;
    private boolean isRunning = false;
    private Project project;
    private MainFileProvider provider;
    
    /**
     * Starts an http server using the current jar location. If the jar location
     * is null, this method does nothing.
     * 
     */
    public void start() {
        try {
            server = HttpServer.create(new InetSocketAddress(8027), 1);
            server.createContext("/", new MyHandler());
            server.setExecutor(null); // creates a default executor
            server.start();
            isRunning = true;
            //Desktop.getDesktop().browse(new URI("http://localhost:8000/"));
//        } catch (URISyntaxException ex) {
//            Logger.getLogger(QuorumWebServer.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(QuorumWebServer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * Stops the web server.
     */
    public void stop() {
        isRunning = false;
        server.stop(0);
    }
    
    /**
     * @return the jarLocation
     */
    public String getJarLocation() {
        return jarLocation;
    }

    /**
     * @param jarLocation the jarLocation to set
     */
    public void setJarLocation(String jarLocation) {
        this.jarLocation = jarLocation;
    }

    /**
     * @return the isRunning
     */
    public boolean isRunning() {
        return isRunning;
    }

    /**
     * @return the project
     */
    public Project getProject() {
        return project;
    }

    /**
     * @param project the project to set
     */
    public void setProject(Project project) {
        this.project = project;
    }

    /**
     * @return the provider
     */
    public MainFileProvider getProvider() {
        return provider;
    }

    /**
     * @param provider the provider to set
     */
    public void setProvider(MainFileProvider provider) {
        this.provider = provider;
        File file = FileUtil.toFile(provider.getDistributionDirectory());
        this.setJarLocation(file.getAbsolutePath() + "/Default.jar");
    }
    
    private class MyHandler implements HttpHandler {
        @Override
        public void handle(HttpExchange t) throws IOException {
            OutputStream os = t.getResponseBody();
            String response = "";
            if(t.getRequestURI().toString().compareTo("/" + jarLocation)==0) {
                response = "<html><body><h2>404 Error</h2>"
                        + "<p>We could not find the page you were looking for.</p></body></html>";
                t.sendResponseHeaders(200, response.length());
                os.write(response.getBytes());
            } else {
                
                URI uri = t.getRequestURI();
                String request = "";
                if(uri != null) {
                    request += uri.toString();
                    if(request != null && !request.isEmpty()) {
                        request = request.substring(1);
                    }
                }
                
                final String URL_PROPERTY_NAME = "quorum.url";
                ProcessBuilder builder = new ProcessBuilder("java", "-Dsodbeans=1", "-jar", jarLocation);
                builder.redirectErrorStream(true);  
                Map<String,String> env = builder.environment(); 
                env.put(URL_PROPERTY_NAME, request);  
                
                Process process = builder.start();  
                t.sendResponseHeaders(200, response.length());
                BufferedReader in = new BufferedReader(new InputStreamReader(process.getInputStream()));  
                
                String line;  
                while ((line = in.readLine()) != null)  
                {  
                    os.write(line.getBytes());
                }
            }
            os.close();
        }
    }
}
