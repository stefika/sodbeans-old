/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sodbeans.actions;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import javax.swing.JFileChooser;
import org.netbeans.api.project.Project;
import org.netbeans.api.project.ui.OpenProjects;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.util.Lookup;
import org.sodbeans.phonemic.SpeechPriority;
import org.sodbeans.phonemic.TextToSpeechFactory;
import org.sodbeans.phonemic.tts.TextToSpeech;
import org.sodbeans.api.quorum.MainFileProvider;
import org.sodbeans.tts.options.api.TextToSpeechOptions;

/**
 *
 * @author Andreas Stefik
 */
public class SodbeansDocumentAction extends SodbeansAction implements ActionListener{

    @Override
    public void actionPerformed(ActionEvent e) {
        // Get the compiler, text-to-speech engine, and Windows controls
        org.sodbeans.compiler.api.Compiler compiler =
                Lookup.getDefault().lookup(org.sodbeans.compiler.api.Compiler.class);
        TextToSpeech speech = TextToSpeechFactory.getDefaultTextToSpeech();

        // If debugger is active, stop it
        if (compiler.isDebuggerActive()) {
            compiler.stopDebugger(true);
        //    windowService.localVariableWindowStop();
        }

        // Get the project the user is referencing
        Project proj = getAppropriateProject(isMainMenuRequest(e));
        MainFileProvider provider = getMainFile(proj);
        if(!isProjectSetupCorrectly(provider)) {
            return;
        }
        FileObject docs = provider.getDocumentsDirectory();
        compiler.setDocumenationFolder(FileUtil.toFile(docs));
        if(compiler.document()) {
                    //speak some kind of message saying that documentation was generated
            if (TextToSpeechOptions.isScreenReading())
                speech.speak("Documentation was built successfully", SpeechPriority.HIGH);
        }
        else {
            if (TextToSpeechOptions.isScreenReading())
                speech.speak("Error while building documentation", SpeechPriority.HIGH);
        }
    }

    @Override
    protected String getDisplayName() {
        return "Document";
    }

}
