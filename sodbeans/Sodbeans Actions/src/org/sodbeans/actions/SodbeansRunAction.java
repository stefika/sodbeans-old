/*
 * This class controls what happens when the "Run Project" button action
 * is selected.
 */
package org.sodbeans.actions;

import java.awt.Desktop;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.util.Date;
import java.util.Iterator;
import java.util.Properties;
import org.netbeans.api.project.Project;
import org.netbeans.api.project.ui.OpenProjects;
import org.openide.util.Lookup;
import org.sodbeans.phonemic.SpeechPriority;
import org.sodbeans.phonemic.TextToSpeechFactory;
import org.sodbeans.phonemic.tts.TextToSpeech;
import org.sodbeans.api.quorum.MainFileProvider;
import org.sodbeans.compiler.api.CompilerEvent;
import org.sodbeans.compiler.api.CompilerListener;
import org.sodbeans.compiler.api.descriptors.CompilerErrorDescriptor;
import org.netbeans.api.progress.ProgressHandle;
import org.netbeans.api.progress.ProgressHandleFactory;
import org.openide.filesystems.FileUtil;
import org.openide.util.Cancellable;
import org.openide.util.Exceptions;
import org.openide.util.RequestProcessor;
import org.quorum.web.QuorumServerHelper;
import org.sodbeans.io.CommandLine;
import org.sodbeans.tts.options.api.TextToSpeechOptions;

public final class SodbeansRunAction extends SodbeansAction implements ActionListener { 
    private Project projectRequestedToRun = null;
    
    //this is temporarily duplicated in this class. Refactor this out.
    public static final String QUORUM_PROJECT_TYPE = "Quorum_Project_Type";
    public static final String QUORUM_CONSOLE_PROJECT = "Quorum_Console_Project";
    public static final String QUORUM_WEB_PROJECT = "Quorum_Web_Project";
    public static final String QUORUM_COMPILED_WEB_PROJECT = "Quorum_Compiled_Web_Project";
    public static final String QUORUM_TOMCAT_LOCATION = "Quorum_Tomcat_Location";
    private static final QuorumServerHelper javaEEServer = QuorumServerHelper.getInstance();
    
    private class QuorumRunner implements Runnable {
        public Project project = null;
        public QuorumWatcher stdoutWatcher = null;
        public QuorumWatcher stderrWatcher = null;
        private ProgressHandle progress;
        private Process process = null;
        private String taskName = "";
        
        public void run() {
            Properties properties = project.getLookup().lookup(Properties.class);
            String property = properties.getProperty(QUORUM_PROJECT_TYPE);
            if((property != null && property.compareTo(QUORUM_WEB_PROJECT) == 0)) {
                try { //it's a web browser, so load the browser instead of running the program.
                    Desktop.getDesktop().browse(new URI("http://localhost:8027/"));
                } catch (URISyntaxException ex) {
                        Exceptions.printStackTrace(ex);
                } catch (IOException ex) {
                    Exceptions.printStackTrace(ex);
                }
            } else if(property != null && property.compareTo(QUORUM_COMPILED_WEB_PROJECT) == 0) {
                File war = new File(project.getProjectDirectory().getPath() + "/Run/Default.war");
                String deploy = javaEEServer.deploy(FileUtil.toFileObject(war));
                try { //it's a web browser, so load the browser instead of running the program.
                    Desktop.getDesktop().browse(new URI(deploy));
                } catch (URISyntaxException ex) {
                        Exceptions.printStackTrace(ex);
                } catch (IOException ex) {
                    Exceptions.printStackTrace(ex);
                }
            } else {
                final Thread currentThread = Thread.currentThread();
                final ProgressHandle progress = ProgressHandleFactory.createHandle(taskName, new Cancellable() {
                public boolean cancel() {
                    currentThread.interrupt();
                    return true;
                }
                });
                try {
                    progress.start();

                    // Compute the location of the project's root directory.
                    File projectDirectory = new File(project.getProjectDirectory().getPath());

                    // Spawn a new Java process that will run "Default.jar" from the project directory.
                    ProcessBuilder builder = new ProcessBuilder("java", "-Dsodbeans=1", "-jar", "Run/Default.jar");
                    builder.directory(projectDirectory);

                    // Start the process.
                    process = builder.start();

                    // Spawn a new thread to run QuorumRunner in.
                    stdoutWatcher = new QuorumWatcher(process.getInputStream());
                    stderrWatcher = new QuorumWatcher(process.getErrorStream());
                    stdoutWatcher.start();
                    stderrWatcher.start();

                    // Wait for the process to exit.
                    process.waitFor();
                } catch (InterruptedException ex) {
                    // thread interrupt indicates cancelling of task
                } catch (IOException ex) {
                    // error spawning Quorum process
                } finally {
                    process.destroy();

                    if (stdoutWatcher != null)
                        stdoutWatcher.close();

                    if (stderrWatcher != null)
                        stderrWatcher.close();
                    progress.finish();
                }
            }
        }
    }
    
    private class QuorumWatcher implements Runnable {
        public Project project = null;
        public Process process = null;
        private BufferedReader bufferedReader = null;
        private Thread blinker = null;
        private boolean running = false;
        
        public QuorumWatcher(InputStream in) {
            bufferedReader = new BufferedReader(new InputStreamReader(in));
        }
        
        public void start() {
            if (!running) {
                blinker = new Thread(this);
                blinker.setDaemon(true);
                blinker.setName("Quorum Process Watcher");
                blinker.start();
            }
        }
        
        public void run() {
            Thread thisThread = Thread.currentThread();
            running = true;
            // Watch the input stream, send its output to the console.
            while (thisThread == blinker) {
                try {
                    String line = bufferedReader.readLine();

                    // If the line is null, the end of the input has been reached.
                    if (line == null)
                        return;

                    console.post(line);
                    Thread.sleep(0);
                } catch (IOException ex) {
                    return;
                } catch (InterruptedException ex) {
                    return;
                }
            }
        }
        
        // The process exited. We close the buffered reader as soon as no more
        // data is available.
        public void close() {
            // Read any remaining bytes from the process.
            try {
                while (bufferedReader.ready()) {
                    String line;
                    line = bufferedReader.readLine();

                    console.post(line);
                }
                
                // Close the stream.
                bufferedReader.close();
            } catch (IOException e) {
                return;
            }
            
            blinker = null;
            running = false;
        }
    }
    
    private org.sodbeans.compiler.api.Compiler compiler =
                Lookup.getDefault().lookup(org.sodbeans.compiler.api.Compiler.class);
    private TextToSpeech speech = TextToSpeechFactory.getDefaultTextToSpeech();
    private CommandLine console = Lookup.getDefault().lookup(CommandLine.class);

    private boolean buildRequested = false;
    public SodbeansRunAction() {
        compiler.addListener(new CompilerListener() {
            public void actionPerformed(CompilerEvent event) {
                if(!buildRequested) {
                    return;
                }
                if(event.isBuildAllEvent()) {
                    String message = "";
                    if(event.isBuildSuccessful()) {
                        Date date = new Date();
                        endTime = date.getTime();
                        
                        addSuccessfulBuildLog(startTime, endTime, true, 0);
                        
                        message = "Running";
                        if (TextToSpeechOptions.isScreenReading())
                            speech.speak(message, SpeechPriority.MEDIUM_HIGH);
                        //compiler.run();
                        
                        runProgram();
                    }
                    else {
                        int numberErrors = compiler.getCompilerErrorManager().getNumberOfSyntaxErrors();
                        addSuccessfulBuildLog(startTime, endTime, false, numberErrors);
                        
                        Iterator<CompilerErrorDescriptor> errorIt = compiler.getCompilerErrorManager().iterator();
                        while(errorIt.hasNext()){
                            CompilerErrorDescriptor error = errorIt.next();
                            addCompilerErrorBuildLog(error);
                        }
                        
                        if (TextToSpeechOptions.isScreenReading()) {
                            message = computeStandardSpokenCompilerError(event, compiler);
                            speech.speak(message, SpeechPriority.HIGHEST);
                        }
                    }
                    buildRequested = false;
                }
            } 
        });
    }
    
    public void actionPerformed(ActionEvent e) {
        if (compiler.isDebuggerActive()) {
            compiler.stopDebugger(true);
        }

        // Get the project the user is referencing
        Project proj = getAppropriateProject(isMainMenuRequest(e));
        
        MainFileProvider provider = getMainFile(proj);
        if(!isProjectSetupCorrectly(provider)) {
            return;
        }

        SodbeansAction.server.setProject(proj);
        SodbeansAction.server.setProvider(provider);
        projectRequestedToRun = proj;
        
        // Compile the project and display the message
        compiler.setBuildFolder(provider.getBuildDirectory());
        compiler.setDistributionFolder(provider.getDistributionDirectory());
        compiler.clearDependencies();
        SodbeansBuildAction.addCompilerDependencies(compiler);
        compiler.compile(proj);
        buildRequested = true;
    }

    @Override
    protected String getDisplayName() {
        return "Run";
    }
    
    /**
     * Run a Quorum program after compilation by calling out to the 'java'
     * command-line utility. This method will invoke the project's
     * "Default.jar" file, watch its output, and place it in the
     * Sodbeans Output Window. This method makes use of the Progress API
     * to allow cancellation of running programs.
     * 
     */
    private void runProgram() {
        // The name the user sees in the task region.
        String taskName = "";
 
        /**
         * These watchers keep track of the 'java 'process's output to stdout and stderr.
         */
        QuorumWatcher stdoutWatcher = null;
        QuorumWatcher stderrWatcher = null;
        
        final Project project = projectRequestedToRun;
        
        taskName = project.getProjectDirectory().getName() + " (run)";
        
        // Create a new "task" to be run by the process API. This task does
        // the following:
        //
        // 1. Spawns a new 'java' process.
        // 2. Spawns two threads to watch the output streams of this 'java' process (stdout and stderr).
        // 3. Waits for the 'java' process to exit (successfully or unsuccessfully). When the process exits, the thread terminates.
        //
        // This spawned process will catch the InterruptedException exception. When it does, the thread will terminate.
        QuorumRunner runner = new QuorumRunner();
        runner.project = project;
        runner.taskName = taskName;
        RequestProcessor requestProcessor = new RequestProcessor(taskName, 1, true);
        RequestProcessor.Task processTask = requestProcessor.create(runner);
        processTask.schedule(0);
    }
}
