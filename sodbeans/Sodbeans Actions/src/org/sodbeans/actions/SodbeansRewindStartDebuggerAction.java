/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.sodbeans.actions;

import java.awt.event.ActionEvent;
import org.openide.util.Lookup;
import org.tod.TODSessionFactory;
import org.tod.TODUtils;

/**
 * This debugger action rewinds a program to the beginning, without restarting
 * the debugger.
 * 
 * @author Andreas Stefik
 */
public class SodbeansRewindStartDebuggerAction {
    public void actionPerformed(ActionEvent e) {

        // Get the compiler, text-to-speech engine, and Windows controls
        org.sodbeans.compiler.api.Compiler compiler =
                Lookup.getDefault().lookup(org.sodbeans.compiler.api.Compiler.class);

        if (!TODUtils.isTODEnabled()) {
            // If the debugger is inactive, do nothing
            if (!compiler.isDebuggerActive())
               return;

            // Step to the previous breakpoint
            compiler.stepBackToBreakpoint();
        } else {
            // Rewind to the start.
            TODSessionFactory.getDefault().getActionHandler().rewind(false);
        }
    }
}
