/*
 Copyright (c) 2013, Andreas Stefik
 All rights reserved.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met: 

 1. Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer. 
 2. Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution. 

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 The views and conclusions contained in the software and documentation are those
 of the authors and should not be interpreted as representing official policies, 
 either expressed or implied, of the FreeBSD Project.
 */
package org.sodbeans.actions;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Iterator;
import javax.swing.JEditorPane;
import javax.swing.SwingUtilities;
import org.debugger.Debugger;
import org.netbeans.modules.editor.NbEditorUtilities;
import org.openide.cookies.EditorCookie;
import org.openide.filesystems.FileObject;
import org.openide.nodes.Node;
import org.openide.text.Line;
import org.openide.util.Lookup;
import org.openide.windows.TopComponent;
import org.quorum.debugger.DebuggerFactory;
import org.sodbeans.compiler.api.descriptors.CompilerClassDescriptor;
import org.sodbeans.compiler.api.descriptors.CompilerFileDescriptor;

/**
 * Runs the program to the cursor or, if the cursor is at an invalid position,
 * runs the program either until it halts or fails.
 *
 * @author Andreas Stefik
 */
public class SodbeansRunToCursorAction implements ActionListener {
    private final Debugger debugger = DebuggerFactory.getQuorumDebugger();
    public void actionPerformed(ActionEvent e) {
        final org.sodbeans.compiler.api.Compiler compiler =
                Lookup.getDefault().lookup(org.sodbeans.compiler.api.Compiler.class);
        
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                Node[] n = TopComponent.getRegistry().getActivatedNodes();
                if (n.length == 1) {
                    EditorCookie ec = n[0].getCookie(EditorCookie.class);
                    if (ec != null) {
                        JEditorPane[] panes = ec.getOpenedPanes();
                        if (panes.length > 0) {
                            int cursor = panes[0].getCaret().getDot();
                            Line line = NbEditorUtilities.getLine(panes[0].getDocument(), cursor, true);
                            FileObject fo = NbEditorUtilities.getFileObject(panes[0].getDocument());
                            CompilerFileDescriptor cfd = compiler.getFileDescriptor(fo);
                            String jvmName = findJVMClassName(cfd, line.getLineNumber() + 1);
                            debugger.runForwardToLine(jvmName, line.getLineNumber() + 1);
                        }
                    }
                }
            }
        });
    }
    
    //The below two methods are duplicates of Quorum Support. They can be safely
    //refactored if that class is exposed outside of the debugger module.
    private static String staticKeyToJVMName(String name) {
        String newName = name.replace('.', '/');
        if (newName.startsWith("/")) {
            return "quorum" + newName;
        }
        return "quorum/" + newName;
    }
    
    private static String findJVMClassName(CompilerFileDescriptor fileDescriptor, int targetLine) {
        Iterator<CompilerClassDescriptor> classes = fileDescriptor.getClasses();
        while (classes.hasNext()) {
            CompilerClassDescriptor next = classes.next();
            if (targetLine >= next.getLine()) {
                return staticKeyToJVMName(next.getStaticKey());
            }
        }
        return null;
    }
}
