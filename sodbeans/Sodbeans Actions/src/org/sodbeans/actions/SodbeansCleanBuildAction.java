/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sodbeans.actions;

import java.awt.event.ActionEvent;
import java.util.Date;
import java.util.Iterator;
import java.util.Properties;
import org.netbeans.api.project.Project;
import org.openide.util.Lookup;
import static org.sodbeans.actions.SodbeansRunAction.QUORUM_COMPILED_WEB_PROJECT;
import static org.sodbeans.actions.SodbeansRunAction.QUORUM_PROJECT_TYPE;
import org.sodbeans.phonemic.SpeechPriority;
import org.sodbeans.phonemic.TextToSpeechFactory;
import org.sodbeans.phonemic.tts.TextToSpeech;
import org.sodbeans.api.quorum.MainFileProvider;
import org.sodbeans.compiler.api.CompilerEvent;
import org.sodbeans.compiler.api.CompilerListener;
import org.sodbeans.compiler.api.descriptors.CompilerErrorDescriptor;
import org.sodbeans.tts.options.api.TextToSpeechOptions;

/**
 *
 * @author Andreas Stefik
 */
public class SodbeansCleanBuildAction extends SodbeansAction{

    private org.sodbeans.compiler.api.Compiler compiler =
                Lookup.getDefault().lookup(org.sodbeans.compiler.api.Compiler.class);
    private TextToSpeech speech = TextToSpeechFactory.getDefaultTextToSpeech();
    
    private boolean buildRequested = false;
    public SodbeansCleanBuildAction() {
        compiler.addListener(new CompilerListener() {
            public void actionPerformed(CompilerEvent event) {
                if(!buildRequested) {
                    return;
                }
                if(event.isBuildAllEvent()) {
                    Date date = new Date();
                    endTime = date.getTime();
                    
                    String message = "";
                    if(event.isBuildSuccessful()) {
                        addSuccessfulBuildLog(startTime, endTime, true, 0);
                        message = "Clean and Build Successful";
                    }
                    else {
                        int numberErrors = compiler.getCompilerErrorManager().getNumberOfSyntaxErrors();
                        addSuccessfulBuildLog(startTime, endTime, false, numberErrors);

                        Iterator<CompilerErrorDescriptor> errorIt = compiler.getCompilerErrorManager().iterator();
                        while(errorIt.hasNext()){
                            CompilerErrorDescriptor error = errorIt.next();
                            addCompilerErrorBuildLog(error);
                        }
                        
                        message = computeStandardSpokenCompilerError(event, compiler);
                    }
                    if (TextToSpeechOptions.isScreenReading()) {
                        speech.speak(message, SpeechPriority.HIGHEST);
                    }
                    buildRequested = false;
                }
            } 
        });
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        // If debugger is active, stop it
        if (compiler.isDebuggerActive()) {
            compiler.stopDebugger(true);
        }

        // Get the project the user is referencing
        Project proj = getAppropriateProject(isMainMenuRequest(e));
        MainFileProvider provider = getMainFile(proj);
        if(!isProjectSetupCorrectly(provider)) {
            return;
        }
        
        Properties properties = proj.getLookup().lookup(Properties.class);
        String property = properties.getProperty(QUORUM_PROJECT_TYPE);
        if((property != null && property.compareTo(QUORUM_COMPILED_WEB_PROJECT) == 0)) {
            compiler.setGenerateWar(true);
        } else {
            compiler.setGenerateWar(false);
        }
        
        //clean and build
        compiler.setBuildFolder(provider.getBuildDirectory());
        compiler.setDistributionFolder(provider.getDistributionDirectory());
        compiler.clean();
        compiler.clearDependencies();
        SodbeansBuildAction.addCompilerDependencies(compiler);
        compiler.compile(proj);
        buildRequested = true;
    }

    @Override
    protected String getDisplayName() {
        return "Clean and Build";
    }

}
