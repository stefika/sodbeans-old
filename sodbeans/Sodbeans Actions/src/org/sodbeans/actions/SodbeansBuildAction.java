/*
 * This class represents the "Build" action event.  This controls what happens
 * when the user clicks "build".
 */
package org.sodbeans.actions;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.Date;
import java.util.Iterator;
import java.util.Properties;
import org.netbeans.api.project.Project;
import org.openide.modules.InstalledFileLocator;
import org.openide.util.Lookup;
import static org.sodbeans.actions.SodbeansRunAction.QUORUM_PROJECT_TYPE;
import static org.sodbeans.actions.SodbeansRunAction.QUORUM_COMPILED_WEB_PROJECT;
import org.sodbeans.compiler.api.CompilerEvent;
import org.sodbeans.phonemic.SpeechPriority;
import org.sodbeans.phonemic.TextToSpeechFactory;
import org.sodbeans.phonemic.tts.TextToSpeech;
import org.sodbeans.api.quorum.MainFileProvider;
import org.sodbeans.compiler.api.CompilerListener;
import org.sodbeans.compiler.api.descriptors.CompilerErrorDescriptor;
import org.sodbeans.tts.options.api.TextToSpeechOptions;

public final class SodbeansBuildAction extends SodbeansAction implements ActionListener{
    private org.sodbeans.compiler.api.Compiler compiler =
            Lookup.getDefault().lookup(org.sodbeans.compiler.api.Compiler.class);
    private TextToSpeech speech = TextToSpeechFactory.getDefaultTextToSpeech();
    
    private boolean buildRequested = false;
    public SodbeansBuildAction() {
        compiler.addListener(new CompilerListener() {
            public void actionPerformed(CompilerEvent event) {
                if(!buildRequested) {
                    return;
                }
                if(event.isBuildAllEvent()) {
                    String message = "";
                    Date date = new Date();
                    endTime = date.getTime();
                    
                    if(event.isBuildSuccessful()) {
                        addSuccessfulBuildLog(startTime, endTime, true, 0);
                        message = "Build Successful";
                    }
                    else {
                        int numberErrors = compiler.getCompilerErrorManager().getNumberOfSyntaxErrors();
                        addSuccessfulBuildLog(startTime, endTime, false, numberErrors);

                        Iterator<CompilerErrorDescriptor> errorIt = compiler.getCompilerErrorManager().iterator();
                        while(errorIt.hasNext()){
                            CompilerErrorDescriptor error = errorIt.next();
                            addCompilerErrorBuildLog(error);
                        }
                        message = computeStandardSpokenCompilerError(event, compiler);
                    }
                    if (TextToSpeechOptions.isScreenReading())
                        speech.speak(message, SpeechPriority.HIGHEST);
                    buildRequested = false;
                }
            } 
        });
    }
        
    public void actionPerformed(ActionEvent e) {
        if (compiler.isDebuggerActive()) {
            compiler.stopDebugger(true);
        }

        // Get the project the user is referencing
        //Utilities.get
        Project proj = getAppropriateProject(isMainMenuRequest(e));
        MainFileProvider provider = getMainFile(proj);
        if(!isProjectSetupCorrectly(provider)) {
            return;
        }
        Properties properties = proj.getLookup().lookup(Properties.class);
        String property = properties.getProperty(QUORUM_PROJECT_TYPE);
        if((property != null && property.compareTo(QUORUM_COMPILED_WEB_PROJECT) == 0)) {
            compiler.setGenerateWar(true);
        } else {
            compiler.setGenerateWar(false);
        }
        
        compiler.setBuildFolder(provider.getBuildDirectory());
        compiler.setDistributionFolder(provider.getDistributionDirectory());
        compiler.clearDependencies();
        addCompilerDependencies(compiler);
        compiler.compile(proj);
        buildRequested = true;
    }

    @Override
    protected String getDisplayName() {
        return "Build";
    }
    
    public static void addCompilerDependencies(org.sodbeans.compiler.api.Compiler compiler) {
        final String codebase = "org.sodbeans.phonemic";
        final String path = "jni/";
        final String libLocation = "modules/lib/";
        final String extLocation = "modules/ext/";
        
        //add a dependency for phonemic
        File phonemic = InstalledFileLocator.getDefault().locate(extLocation + "phonemic.jar", codebase, false);
        compiler.addDependency(phonemic);
        
        //add a dependency for all of phonemic's native libraries
        //windows
        File phonemicJNI = InstalledFileLocator.getDefault().locate(libLocation + "Interop.SpeechLib.5.3.dll", codebase, false);
        compiler.addDependency(phonemicJNI, path);
        phonemicJNI = InstalledFileLocator.getDefault().locate(libLocation + "SappyJNI.dll", codebase, false);
        compiler.addDependency(phonemicJNI, path);
        phonemicJNI = InstalledFileLocator.getDefault().locate(libLocation + "SappyJNI64.dll", codebase, false);
        compiler.addDependency(phonemicJNI, path);
        
        //mac
        phonemicJNI = InstalledFileLocator.getDefault().locate(libLocation + "libCarbonSpeakJNI.jnilib", codebase, false);
        compiler.addDependency(phonemicJNI, path);
        
        //linux
        phonemicJNI = InstalledFileLocator.getDefault().locate(libLocation + "libLinuxSpeakJNI.so", codebase, false);
        compiler.addDependency(phonemicJNI, path);
        phonemicJNI = InstalledFileLocator.getDefault().locate(libLocation + "libLinuxSpeakJNI64.so", codebase, false);
        compiler.addDependency(phonemicJNI, path);
        
        //nvda
        phonemicJNI = InstalledFileLocator.getDefault().locate(libLocation + "nvdaControllerClient32.dll", codebase, false);
        compiler.addDependency(phonemicJNI, path);
        phonemicJNI = InstalledFileLocator.getDefault().locate(libLocation + "nvdaControllerClient64.dll", codebase, false);
        compiler.addDependency(phonemicJNI, path);
        
        phonemicJNI = InstalledFileLocator.getDefault().locate(libLocation + "WindowsAccessibleHandler32.dll", codebase, false);
        compiler.addDependency(phonemicJNI, path);
        phonemicJNI = InstalledFileLocator.getDefault().locate(libLocation + "WindowsAccessibleHandler64.dll", codebase, false);
        compiler.addDependency(phonemicJNI, path);
        phonemicJNI = InstalledFileLocator.getDefault().locate(libLocation + "MacAccessibleHandler.dylib", codebase, false);
        compiler.addDependency(phonemicJNI, path);
    }
}
