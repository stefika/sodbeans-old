/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sodbeans.actions;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JEditorPane;
import javax.swing.SwingUtilities;
import org.netbeans.modules.editor.NbEditorUtilities;
import org.openide.cookies.EditorCookie;
import org.openide.filesystems.FileObject;
import org.openide.nodes.Node;
import org.openide.text.Line;
import org.openide.util.Lookup;
import org.openide.windows.TopComponent;
import org.sodbeans.compiler.api.descriptors.CompilerFileDescriptor;
import org.sodbeans.tod.TODCompilerUtils;
import org.tod.TODSessionFactory;
import org.tod.TODUtils;

/**
 * Runs the program backward to the cursor or, if the cursor is at an
 * invalid position, runs the program either until it is at the beginning
 * or fails.
 *
 * @author Andreas Stefik
 */
public class SodbeansRunBackToCursorAction implements ActionListener {

    public void actionPerformed(ActionEvent e) {
        // Get the compiler, text-to-speech engine, and Windows controls
        final org.sodbeans.compiler.api.Compiler compiler =
                Lookup.getDefault().lookup(org.sodbeans.compiler.api.Compiler.class);

        // If the debugger is inactive, do nothing
        if (!compiler.isDebuggerActive() && !TODUtils.isTODEnabled()) {
           return;
        }

        SwingUtilities.invokeLater(new Runnable() {

            public void run() {
                Node[] n = TopComponent.getRegistry().getActivatedNodes();
                if (n.length == 1) {
                    EditorCookie ec = n[0].getCookie(EditorCookie.class);
                    if (ec != null) {
                        JEditorPane[] panes = ec.getOpenedPanes();
                        if (panes.length > 0) {
                            int cursor = panes[0].getCaret().getDot();
                            Line line = NbEditorUtilities.getLine(panes[0].getDocument(), cursor, true);
                            FileObject fo = NbEditorUtilities.getFileObject(panes[0].getDocument());
                            CompilerFileDescriptor cfd = compiler.getFileDescriptor(fo);
                            if (!TODUtils.isTODEnabled()) {
                                compiler.runBackToCursor(fo, line.getLineNumber());
                            } else {
                                String jvmName = TODCompilerUtils.findJVMClassName(cfd, line.getLineNumber() + 1);
                                TODSessionFactory.getDefault().getActionHandler().runBackToLine(jvmName, line.getLineNumber() + 1, false);
                            }
                        }
                    }
                }
            }
        });
    }

}
