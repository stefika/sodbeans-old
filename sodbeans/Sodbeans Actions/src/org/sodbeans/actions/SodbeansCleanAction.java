/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sodbeans.actions;

import java.awt.event.ActionEvent;
import org.netbeans.api.project.Project;
import org.netbeans.api.project.ProjectInformation;
import org.netbeans.api.project.ui.OpenProjects;
import org.openide.util.Lookup;
import org.sodbeans.phonemic.SpeechPriority;
import org.sodbeans.phonemic.TextToSpeechFactory;
import org.sodbeans.phonemic.tts.TextToSpeech;
import org.sodbeans.api.quorum.MainFileProvider;
import org.sodbeans.tts.options.api.TextToSpeechOptions;

/**
 *
 * @author Andreas Stefik
 */
public class SodbeansCleanAction extends SodbeansAction{
    private org.sodbeans.compiler.api.Compiler compiler =
                Lookup.getDefault().lookup(org.sodbeans.compiler.api.Compiler.class);
    private TextToSpeech speech = TextToSpeechFactory.getDefaultTextToSpeech();
        
    @Override
    public void actionPerformed(ActionEvent e) {
        
        // If debugger is active, stop it
        if (compiler.isDebuggerActive()) {
            compiler.stopDebugger(true);
        }

        // Get the project the user is referencing
        Project proj = getAppropriateProject(isMainMenuRequest(e));
        MainFileProvider provider = getMainFile(proj);
        if(!isProjectSetupCorrectly(provider)) {
            return;
        }
        compiler.setBuildFolder(provider.getBuildDirectory());
        compiler.setDistributionFolder(provider.getDistributionDirectory());

        //clean the build.
        compiler.clean();
        ProjectInformation info = proj.getLookup().lookup(ProjectInformation.class);
        if (TextToSpeechOptions.isScreenReading())
            speech.speak("Cleaning project " + info.getDisplayName(), SpeechPriority.HIGHEST);
    }

    @Override
    protected String getDisplayName() {
        return "Clean";
    }
}
