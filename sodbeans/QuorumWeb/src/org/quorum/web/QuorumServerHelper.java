/*
 Copyright (c) 2013, Andreas Stefik
 All rights reserved.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met: 

 1. Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer. 
 2. Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution. 

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 The views and conclusions contained in the software and documentation are those
 of the authors and should not be interpreted as representing official policies, 
 either expressed or implied, of the FreeBSD Project.
 */
package org.quorum.web;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.modules.j2ee.deployment.devmodules.api.Deployment;
import org.netbeans.modules.j2ee.deployment.devmodules.api.J2eeModule;
import org.netbeans.modules.j2ee.deployment.devmodules.spi.J2eeModuleFactory;
import org.openide.filesystems.FileObject;

/**
 * This is a helper class for starting and stopping a tomcat installation.
 * 
 * @author Andreas Stefik
 */
public class QuorumServerHelper {
    private static QuorumServerHelper instance = null;
    
    private QuorumServerHelper() {
    }
    
    /**
     * This method deploys a Quorum generated .war file to a java EE container
     * of the user's preference.
     * 
     * @param war
     * @return 
     */
    public String deploy(FileObject war) {
        return deploy(war, false);
    }
    
    /**
     * This method deploys the .war file to the Java EE container and then
     * either debugs the module or runs it.
     * 
     * @param war the .war file to be deployed
     * @param debug whether to debug the war file.
     * @return 
     */
    public String deploy(FileObject war, boolean debug) {
        Deployment deployment = Deployment.getDefault();
        String[] serverIDs = deployment.getServerInstanceIDs();
        String serverID = serverIDs[0];
        try {
            //create a module and prepare the war for deployment
            QuorumJ2eeModule quorumModule = new QuorumJ2eeModule(war);
            J2eeModule module = J2eeModuleFactory.createJ2eeModule(quorumModule);
            
            //create a provider that can give the war file to the server
            QuorumJ2eeModuleProvider provider = new QuorumJ2eeModuleProvider();
            provider.setJ2eeModule(module);
            provider.setServerID(serverID);
            provider.setServerInstanceID(serverID);
            
            //chek if this is valid and we can deploy to it
            boolean canDeploy = deployment.canFileDeploy(serverID, module);
            if(canDeploy) {
                String deploy = null;
                if(!debug) { //run normally
                    deploy = deployment.deploy(provider, Deployment.Mode.RUN, "/", "/", true);
                } else { //run itin the debugger on port
                    deploy = deployment.deploy(provider, Deployment.Mode.DEBUG, "/", "/", true);
                }
                return deploy;
            }
        } catch (Deployment.DeploymentException ex) {
            Logger.getLogger(QuorumServerHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
        //we could not deploy, so return null
        return null;
    }
    
    /**
     * This method ensures only one copy of the Server helper can exist on
     * the system.
     * 
     * @return 
     */
    public synchronized static QuorumServerHelper getInstance() {
        if(instance == null) {
            instance = new QuorumServerHelper();
        }
        return instance;
    }
}
