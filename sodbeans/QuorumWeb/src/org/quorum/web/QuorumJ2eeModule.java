/*
 Copyright (c) 2013, Andreas Stefik
 All rights reserved.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met: 

 1. Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer. 
 2. Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution. 

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 The views and conclusions contained in the software and documentation are those
 of the authors and should not be interpreted as representing official policies, 
 either expressed or implied, of the FreeBSD Project.
 */
package org.quorum.web;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.Iterator;
import org.netbeans.modules.j2ee.dd.api.web.WebAppMetadata;
import org.netbeans.modules.j2ee.deployment.devmodules.api.J2eeModule;
import org.netbeans.modules.j2ee.deployment.devmodules.spi.J2eeModuleImplementation2;
import org.netbeans.modules.j2ee.metadata.model.api.MetadataModel;
import org.netbeans.modules.j2ee.metadata.model.spi.MetadataModelFactory;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;

/**
 * A basic module for passing EE war files back to the system.
 * 
 * @author Andreas Stefik
 */
public class QuorumJ2eeModule implements J2eeModuleImplementation2 {
    private final FileObject war;
    private final PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(this);
    private final MetadataModel<WebAppMetadata> webAppMetadata;
    
    public QuorumJ2eeModule(FileObject value) {
        war = value;
        webAppMetadata = MetadataModelFactory.createMetadataModel(new SimpleMetadataModelImpl<WebAppMetadata>());
    }
    
    @Override
    public FileObject getArchive() throws IOException {
        return null;
    }

    @Override
    public Iterator<J2eeModule.RootedEntry> getArchiveContents() throws IOException {
        return Collections.<J2eeModule.RootedEntry>emptySet().iterator();
    }

    @Override
    public FileObject getContentDirectory() throws IOException {
        return war;
    }
    
    @Override
    public J2eeModule.Type getModuleType() {
        return J2eeModule.Type.WAR;
    }

    @Override
    public String getModuleVersion() {
        return J2eeModule.JAVA_EE_5;
    }

    @Override
    public String getUrl() {
        return null;
    }
    
    public void setUrl(String url) {
        // noop
    }

    public File getResourceDirectory() {
        return new File(FileUtil.toFile(war), "resources");
    }

    public File getDeploymentConfigurationFile(String name) {
        if (name.equals(J2eeModule.WEB_XML)) {
            return new File(FileUtil.toFile(war), name);
        } else {
            return new File(FileUtil.toFile(war), name);
        }
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.removePropertyChangeListener(listener);
    }
    
    public void firePropertyChange(String propertyName, Object oldValue, Object newValue) {
        propertyChangeSupport.firePropertyChange(propertyName, oldValue, newValue);
    }

    public <T> MetadataModel<T> getMetadataModel(Class<T> type) {
        if (type == WebAppMetadata.class) {
            return (MetadataModel<T>) webAppMetadata;
        } else {
            return null;
        }
    }
}
