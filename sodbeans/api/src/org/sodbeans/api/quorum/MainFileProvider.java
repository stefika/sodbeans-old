/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sodbeans.api.quorum;

import org.openide.filesystems.FileObject;

/**
 * A helper class for handling the file the user considers "Main" in a 
 * Quorum project.
 * 
 * @author Andrew Haywood and Andreas Stefik
 */
public interface MainFileProvider {

    /**
     * Returns the file the user has indicated is the "Main" for their project.
     * @return 
     */
    public FileObject getMainFile();

    /**
     * Sets the user's preference for the main file.
     * 
     * @param file 
     */
    public void setMainFile (FileObject file);

    /**
     * Given a file object, returns true if the passed file is the Main
     * file.
     * 
     * @param obj
     * @return 
     */
    public boolean isMainFile (FileObject obj);

    /**
     * Returns all source code files for the project.
     * 
     * @return 
     */
    public FileObject[] getSourceFiles();
    
    /**
     * Returns the folder by which all files should be built.
     * 
     * @return 
     */
    public FileObject getBuildDirectory();
    
    /**
     * Returns the folder by which the final built project will be placed
     * relative to a particular project's folder.
     * 
     * @return 
     */
    public FileObject getDistributionDirectory();
    
    /**
     * Returns the default directory where documentation should be placed,
     * if generated.
     * 
     * @return 
     */
    public FileObject getDocumentsDirectory();
}
