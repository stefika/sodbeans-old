/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.sodbeans.output;

import org.netbeans.api.settings.ConvertAsProperties;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.beans.PropertyVetoException;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

import javax.swing.AbstractAction;
import javax.swing.JEditorPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JViewport;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;

import org.openide.cookies.EditorCookie;
import org.openide.cookies.EditorCookie.Observable;
import org.openide.cookies.LineCookie;
import org.openide.explorer.ExplorerManager;
import org.openide.explorer.ExplorerUtils;
import org.openide.explorer.view.TreeTableView;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectNotFoundException;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.nodes.Node.PropertySet;
import org.openide.text.Line;
import org.openide.util.Exceptions;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.windows.TopComponent;
import org.openide.windows.WindowManager;

import org.sodbeans.compiler.api.CompilerEvent;
import org.sodbeans.compiler.api.CompilerListener;
import org.sodbeans.compiler.api.descriptors.CompilerErrorDescriptorNode;
import org.sodbeans.compiler.api.descriptors.CompilerFileDescriptor;
import org.sodbeans.io.CommandLine;
import org.sodbeans.io.CommandLineEvent;
import org.sodbeans.io.CommandLineListener;
import org.sodbeans.phonemic.SpeechPriority;
import org.sodbeans.phonemic.TextToSpeechFactory;
import org.sodbeans.phonemic.tts.TextToSpeech;
import org.sodbeans.tts.options.api.TextToSpeechOptions;

/**
 * Top component which displays something.
 */
@ConvertAsProperties(dtd = "-//org.sodbeans.output//SodbeansOutput//EN",
autostore = false)
public final class SodbeansOutputTopComponent extends TopComponent implements ExplorerManager.Provider, ActionListener, CompilerListener {

    //private static Logger UILOG = Logger.getLogger("org.netbeans.ui.actions");
    private CommandLine commandLine = Lookup.getDefault().lookup(CommandLine.class);
    private ExplorerManager mgr = new ExplorerManager();
    private org.sodbeans.compiler.api.CompilerErrorManager compilerErrorManager;
    private static TextToSpeech speech = TextToSpeechFactory.getDefaultTextToSpeech();
    private static SodbeansOutputTopComponent instance;
    private Node.Property[] properties;

    protected Node errorNode;
    FileObject fileObjectToOpen;


    /** path to the icon used by the component and its open action */
    static final String ICON_PATH = "org/sodbeans/output/SodbeansLogo2_16.png";
    private static final String PREFERRED_ID = "SodbeansOutputTopComponent";
    private static final Logger logger = Logger.getLogger(SodbeansOutputTopComponent.class.getName());
    private org.sodbeans.compiler.api.Compiler compiler = Lookup.getDefault().lookup(org.sodbeans.compiler.api.Compiler.class);

    public SodbeansOutputTopComponent() {
        initComponents();
        compiler = Lookup.getDefault().lookup(org.sodbeans.compiler.api.Compiler.class);
        associateLookup(ExplorerUtils.createLookup(mgr, getActionMap()));
        resetModelToCurrentFile();

        compilerErrorManager = compiler.getCompilerErrorManager();

        setName(NbBundle.getMessage(SodbeansOutputTopComponent.class, "CTL_SodbeansOutputTopComponent"));
        setToolTipText(NbBundle.getMessage(SodbeansOutputTopComponent.class, "HINT_SodbeansOutputTopComponent"));
        setIcon(ImageUtilities.loadImage(ICON_PATH, true));
        putClientProperty("netbeans.winsys.tc.keep_preferred_size_when_slided_in", Boolean.TRUE);

        KeyStroke enterKey = KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, true);
        ((TreeTableView) jScrollPane1).getInputMap(JScrollPane.WHEN_IN_FOCUSED_WINDOW).put(enterKey, "enter");
        ((TreeTableView) jScrollPane1).getActionMap().put("enter", new EnterKeyHandler());
        KeyStroke rightKey = KeyStroke.getKeyStroke(KeyEvent.VK_RIGHT, 0, true);
        ((TreeTableView) jScrollPane1).getInputMap(JScrollPane.WHEN_IN_FOCUSED_WINDOW).put(rightKey, "right");
        ((TreeTableView) jScrollPane1).getActionMap().put("right", new RightKeyHandler());
        
        compiler.addListener(this);
        commandLine.add(new CommandLineListener() {
            @Override
            public void actionPerformed(CommandLineEvent event) {
                try {
                updateOutput();
                } catch (Exception exception) {
                logger.log(Level.INFO, "An exception was thrown when trying to update the compiler error window.", exception);
                }
           }
        });
    }

    @Override
    public void actionPerformed(CompilerEvent e) {
        final CompilerEvent event = e;
        // Bugfix for ArrayIndexOutOfBounds exception
        this.runInEventDispatchThread(new Runnable() {
           @Override
           public void run() {
                try {
                    if (event.isBuildAllEvent() || event.isDebuggerStartEvent()) {
                        stopOutputWindow();
                        startOutputWindow();
                        requestVisible();
                    }
                } catch (Exception exception) {
                    logger.log(Level.INFO, "An exception was thrown when trying to update the compiler error window.", exception);
                }
           }
        });
    }

    /**
     * Start compiler error : output window.
     */
    private void compilerErrorWindowStart() {
        this.startOutputWindow();
        
        if(!SwingUtilities.isEventDispatchThread()) {
            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    requestVisible(); // variables tab visible but not in focus.
                }
            });
        }
    }

    /**
     * Update the compiler error : output window.
     */
    private void compilerErrorWindowUpdate() {
        this.updateOutput();
    }

    /**
     * Stop the compiler error : output window.
     */
    private void compilerErrorWindowStop() {
        this.stopOutputWindow();
    }

    /**
     * initialize and start the output window
     */
    private void startOutputWindow() {
        ((TreeTableView) jScrollPane1).getAccessibleContext().setAccessibleName("Sodbeans Output");
        jScrollPane1.setVisible(true);
        ((TreeTableView) jScrollPane1).setRootVisible(false);

        mgr.setRootContext(AbstractNode.EMPTY);
        Node node = compilerErrorManager.getRootCompilerErrorNode();
        Node outputNode = commandLine.getOutputNode();
        if (node != null) {//dealing with compiler error sheet


            Node[] nodes = node.getChildren().getNodes();

            PropertySet set[];
            if (nodes.length != 0) {
                set = nodes[0].getPropertySets();
            } else {
                set = node.getPropertySets();
            }

            if (set.length > 0) {
                properties = set[0].getProperties();
                if(properties.length > 0){
                    ((TreeTableView) jScrollPane1).setProperties(properties);
                }
            }

            mgr.setRootContext(node);

        } else if (outputNode != null) {//dealing with output console sheet
            PropertySet set[] = outputNode.getPropertySets();
            if (set.length > 0) {
                properties = set[0].getProperties();
            }
            try {
                ((TreeTableView) jScrollPane1).setProperties(properties);
                mgr.setRootContext(outputNode);
                Date date = new Date();
                commandLine.post("Build Successfully completed at " + date.toString() + ".");
            } catch (Exception e) {
                logger.log(Level.INFO, e.getStackTrace().toString());
            }
        } else {

            AbstractNode newNode = new AbstractNode(Children.LEAF);
            mgr.setRootContext(newNode);
        }
    }

    /**
     * Clears output window
     */
    private void stopOutputWindow() {
        commandLine.clear();
        mgr.setRootContext(AbstractNode.EMPTY);
    }

    /**
     * update the output window for compiler errors and the output console
     */
    private void updateOutput() {
        compilerErrorManager.getRootCompilerErrorNode();
        commandLine.getOutputNode();
    }

    @Override
    public boolean requestFocusInWindow(boolean val) {
        boolean bool = super.requestFocusInWindow(val);
        //kind of a hack, but it forces the focus
        //to be in the correct window. It also selects
        //the top most node in the window.
        JViewport viewport = jScrollPane1.getViewport();
        if (viewport != null) {
            Component[] components = viewport.getComponents();
            if (components != null && components.length > 0) {
                Component comp = components[0];
                if (comp != null) {
                    if (comp instanceof JPanel) {
                        JPanel panel = (JPanel) comp;
                        components = panel.getComponents();
                        if (components != null && components.length > 0) {
                            comp = components[0];
                            if (comp instanceof JScrollPane) {
                                JScrollPane p = (JScrollPane) comp;
                                viewport = p.getViewport();
                                if (viewport != null) {
                                    components = viewport.getComponents();
                                    if (components != null && components.length > 0) {
                                        Component comp1 = components[0];
                                        if (comp1 instanceof JTable) {
                                            JTable table = (JTable) comp1;
                                            table.requestFocusInWindow();
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        try {
            if (compilerErrorManager != null && compilerErrorManager.getNodeAt(0) != null) {
                Node[] n = {compilerErrorManager.getNodeAt(0)};
                mgr.setSelectedNodes(n);
            }
        } catch (PropertyVetoException ex) {
            logger.log(Level.INFO, "The Sodbeans output window threw an exception in componentActivated().", ex);
        }
        return bool;
    }

    @Override
    protected void componentActivated() {
        try {
            this.requestFocusInWindow(true);
            this.requestFocus(true);
            if (TextToSpeechOptions.isScreenReading()) {
                speech.speak("Sodbeans output window gained focus.",
                        SpeechPriority.MEDIUM);
            }
            ExplorerUtils.activateActions(mgr, true);

            String UI_LOGGER_NAME = "org.netbeans.ui.sodbeansOutput";
            LogRecord record = new LogRecord(Level.INFO, "SODBEANS_OUTPUT_SELECTED");
            record.setParameters(new Object[]{compilerErrorManager.getNumberOfSyntaxErrors()});
            record.setLoggerName(UI_LOGGER_NAME);
            Logger.getLogger(UI_LOGGER_NAME).log(record);
        } catch (Exception ex) {
            logger.log(Level.INFO, "The Sodbeans output window threw an exception in componentActivated().", ex);
        }
    }

    @Override
    protected void componentDeactivated() {
        try {
            mgr.setSelectedNodes(new Node[]{});
        } catch (PropertyVetoException ex) {
            logger.log(Level.INFO, "The Sodbeans output window threw an exception in componentDeactivated().", ex);
        }
        ExplorerUtils.activateActions(mgr, false);
    }
    
    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new TreeTableView();

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 400, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 300, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane jScrollPane1;
    // End of variables declaration//GEN-END:variables

    /**
     * Gets default instance. Do not use directly: reserved for *.settings files only,
     * i.e. deserialization routines; otherwise you could get a non-deserialized instance.
     * To obtain the singleton instance, use {@link #findInstance}.
     */
    public static synchronized SodbeansOutputTopComponent getDefault() {
        if (instance == null) {
            instance = new SodbeansOutputTopComponent();
            instance.startOutputWindow();
        }
        return instance;
    }

    /**
     * Obtain the SodbeansOutputTopComponent instance. Never call {@link #getDefault} directly!
     */
    public static synchronized SodbeansOutputTopComponent findInstance() {
        TopComponent win = WindowManager.getDefault().findTopComponent(PREFERRED_ID);
        if (win == null) {
            Logger.getLogger(SodbeansOutputTopComponent.class.getName()).warning(
                    "Cannot find " + PREFERRED_ID + " component. It will not be located properly in the window system.");
            return getDefault();
        }
        if (win instanceof SodbeansOutputTopComponent) {
            return (SodbeansOutputTopComponent) win;
        }
        Logger.getLogger(SodbeansOutputTopComponent.class.getName()).warning(
                "There seem to be multiple components with the '" + PREFERRED_ID
                + "' ID. That is a potential source of errors and unexpected behavior.");
        return getDefault();
    }

    @Override
    public int getPersistenceType() {
        return TopComponent.PERSISTENCE_ALWAYS;
    }


    /** replaces this in object stream */
    @Override
    public Object writeReplace() {
        return new ResolvableHelper();
    }

    void writeProperties(java.util.Properties p) {
        // better to version settings since initial version as advocated at
        // http://wiki.apidesign.org/wiki/PropertyFiles
        p.setProperty("version", "1.0");
        // TODO store your settings
    }

    Object readProperties(java.util.Properties p) {
        if (instance == null) {
            instance = this;
        }
        instance.readPropertiesImpl(p);
        return instance;
    }

    private void readPropertiesImpl(java.util.Properties p) {
        String version = p.getProperty("version");
        // TODO read your settings according to their version
    }

    @Override
    protected String preferredID() {
        return PREFERRED_ID;
    }

    @Override
    public ExplorerManager getExplorerManager() {
        return mgr;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    /**
     * Tells the class to gather information about the current file from
     * the compiler and display it to the user.
     */
    public void resetModelToCurrentFile() {
        if (fileObjectToOpen != null) {
            CompilerFileDescriptor cfd = compiler.getFileDescriptor(fileObjectToOpen);
            if (cfd != null) {
                AbstractNode root = cfd.getRootNode();
                if (root != null) {
                    mgr.setRootContext(root);
                    ((TreeTableView) jScrollPane1).setRootVisible(false);
                }
            }
        }
    }

    /**
     * Get the selected error in the output window.
     * @return Compiler error descriptor node
     */
    public CompilerErrorDescriptorNode getSelectedErrorNode() {
        Node[] nodes = mgr.getSelectedNodes();
        CompilerErrorDescriptorNode error = null;
        if (nodes != null) {
            if (nodes.length != 0) {
                if (nodes[0] instanceof CompilerErrorDescriptorNode) {
                    error = (CompilerErrorDescriptorNode) nodes[0];
                }
            }
        }
        return error;
    }

    /**
     * Determines if a Node should be ignored.
     * @param node
     * @return
     */
    public boolean isIgnorableNode(Node node) {
        if (node.getDisplayName().compareTo("Description") == 0) {
            return true; //ignore it if it's the root row?
        } else {
            return false;
        }
    }

    /**
     * Enter key handler for navigating to errors in the editor.
     */
    public class EnterKeyHandler extends AbstractAction {

        @Override
        public void actionPerformed(ActionEvent evt) {

            returnHandler();
        }
    }

    public void returnHandler() {
        int lineNumber = 0;
        CompilerErrorDescriptorNode n = getSelectedErrorNode();

        if (n == null) {
            return;
        }


        File fileToOpen = new File(n.getAbsolutePath()); //pass absolute path to the file you want
        fileObjectToOpen = FileUtil.toFileObject(fileToOpen);

        try {



            DataObject dob = DataObject.find(fileObjectToOpen);

            //Bug fix: now actually make sure it's open in the editor
            openEditor(dob);

            EditorCookie ck = dob.getCookie(EditorCookie.class);
            if (ck != null) {
                ck.openDocument();
                JEditorPane[] p = ck.getOpenedPanes();
                if (p.length > 0) {
                    //Need to do this since we're disabling the window system's
                    //auto focus mechanism
                    p[0].requestFocus();
                    if (dob != null) {
                        LineCookie lc = dob.getCookie(LineCookie.class);
                        if (lc == null) {
                            return;
                        }
                        lineNumber = n.getLine() - 1;
                        Line l = lc.getLineSet().getOriginal(lineNumber);
                        l.show(Line.ShowOpenType.OPEN, Line.ShowVisibilityType.FOCUS);
                        
                        if (TextToSpeechOptions.isScreenReading()) {
                            speech.speak("Focussed to " + fileObjectToOpen.getName()
                                    + " dot " + fileObjectToOpen.getExt() + " "
                                    + n.getShortDescription() + " " + n.getDisplayName(),
                                    SpeechPriority.MEDIUM);
                        }
                    }
                }
            }
        } catch (DataObjectNotFoundException e) {
            Exceptions.printStackTrace(e);
        } catch (IOException ioe) {
            Exceptions.printStackTrace(ioe);
        }
    }

    public class RightKeyHandler extends AbstractAction {
        @Override
        public void actionPerformed(ActionEvent evt) {
            Node n = getSelectedErrorNode();

            if (n == null) {
                return;
            }
            try {
                Node.Property[] prop = n.getPropertySets()[0].getProperties();
                if (isIgnorableNode(n)) {
                    return;
                }
                
                if (TextToSpeechOptions.isScreenReading() && n.getShortDescription().compareTo("Error") == 0) {
                    speech.speak("Compiler error " + n.getDisplayName()
                            + "           " + " Line  "
                            + prop[0].getValue().toString()
                            + "           " + " File  "
                            + prop[1].getValue().toString()
                            + "           " + " Path  "
                            + prop[2].getValue().toString(),
                            SpeechPriority.HIGHEST);
                }

            } catch (IllegalAccessException ex) {
                Exceptions.printStackTrace(ex);
            } catch (InvocationTargetException ex) {
                Exceptions.printStackTrace(ex);
            }
            errorNode = getSelectedErrorNode();
        }
    }

    private void openEditor(DataObject dataObj) {
        final Observable ec = dataObj.getCookie(Observable.class);

        if (ec != null) {
            runInEventDispatchThread(new Runnable() {

                @Override
                public void run() {
                    ec.open();
                }
            });
        }
    }

    private void runInEventDispatchThread(Runnable runnable) {
        if (javax.swing.SwingUtilities.isEventDispatchThread()) {
            runnable.run();
        } else {
            javax.swing.SwingUtilities.invokeLater(runnable);
        }
    }

    final static class ResolvableHelper implements Serializable {

        private static final long serialVersionUID = 1L;

        public Object readResolve() {
            return SodbeansOutputTopComponent.getDefault();
        }
    }
}
