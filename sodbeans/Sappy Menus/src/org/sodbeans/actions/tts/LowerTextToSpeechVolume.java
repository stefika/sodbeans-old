/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.sodbeans.actions.tts;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import org.openide.awt.ActionRegistration;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionReferences;
import org.openide.awt.ActionID;
import org.openide.util.NbBundle.Messages;
import org.sodbeans.phonemic.TextToSpeechFactory;
import org.sodbeans.phonemic.tts.TextToSpeech;
import org.sodbeans.tts.options.api.TextToSpeechOptions;

@ActionID(category = "TextToSpeech",
id = "org.sodbeans.actions.tts.LowerTextToSpeechVolume2")
@ActionRegistration(displayName = "#CTL_LowerTextToSpeechVolume2")
@ActionReferences({
    @ActionReference(path = "Menu/Tools/Text to Speech", position = 400, separatorAfter = 450),
    @ActionReference(path = "Shortcuts", name = "DO-F5")
})
@Messages("CTL_LowerTextToSpeechVolume2=Lower Speech Volume")
public final class LowerTextToSpeechVolume implements ActionListener {
    private TextToSpeech speech = TextToSpeechFactory.getDefaultTextToSpeech();

    public void actionPerformed(ActionEvent e) {
        // Does this engine support pitch changes?
        if (!speech.canSetVolume()) {
            if(TextToSpeechOptions.isScreenReading()) {
                speech.speak("The engine you are using does not support volume changes.");
            }
            return;
        }
        
        double currentVolume = speech.getVolume();
        
        if (currentVolume - 0.1 <= 0.1) { // we don't want to turn *off* speech, which on all systems = 0
            // We are at the minimum.
            if(TextToSpeechOptions.isScreenReading()) {
                speech.speak("You are already at the lowest volume.");
            }
        }     
        else if (currentVolume > 0) {
            // Up the volume by 0.1
            double newVolume = currentVolume - 0.1;
            speech.setVolume(newVolume);
            if(TextToSpeechOptions.isScreenReading()) {
                speech.speak("Text to Speech volume lowered.");
            }
            
            // TODO: Store this setting.
            if (newVolume >= 0.0 && newVolume <= 1.0)
            TextToSpeechOptions.setSpeechVolume((int)(newVolume * 100));
        }

    }
}
