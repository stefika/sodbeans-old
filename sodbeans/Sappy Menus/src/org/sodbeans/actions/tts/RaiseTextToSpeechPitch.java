/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.sodbeans.actions.tts;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import org.openide.awt.ActionRegistration;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionReferences;
import org.openide.awt.ActionID;
import org.openide.util.NbBundle.Messages;
import org.sodbeans.phonemic.TextToSpeechFactory;
import org.sodbeans.phonemic.tts.TextToSpeech;
import org.sodbeans.tts.options.api.TextToSpeechOptions;

@ActionID(category = "TextToSpeech",
id = "org.sodbeans.actions.tts.RaiseTextToSpeechPitch")
@ActionRegistration(displayName = "#CTL_RaiseTextToSpeechPitch")
@ActionReferences({
    @ActionReference(path = "Menu/Tools/Text to Speech", position = 700),
    @ActionReference(path = "Shortcuts", name = "DO-F8")
})
@Messages("CTL_RaiseTextToSpeechPitch=Raise Speech Pitch")
public final class RaiseTextToSpeechPitch implements ActionListener {
    private TextToSpeech speech = TextToSpeechFactory.getDefaultTextToSpeech();

    public void actionPerformed(ActionEvent e) {
        // Does this engine support pitch changes?
        if (!speech.canSetPitch()) {
            if(TextToSpeechOptions.isScreenReading()) {
                speech.speak("The engine you are using does not support pitch changes.");
            }
            return;
        }
        
        double currentPitch = speech.getPitch();
        
        if (currentPitch < 1) {
            // Up the pitch by 0.1
            double newPitch = currentPitch + 0.1;
            speech.setPitch(newPitch);
            if(TextToSpeechOptions.isScreenReading()) {
                speech.speak("Text to Speech pitch raised.");
            }
            
            if (newPitch >= 0 && newPitch <= 1.0)
                TextToSpeechOptions.setSpeechPitch((int)(newPitch * 100));
        }
        else {
            // We are at the minimum.
            if(TextToSpeechOptions.isScreenReading()) {
                speech.speak("You are already at the highest pitch.");
            }
        }
    }
}
