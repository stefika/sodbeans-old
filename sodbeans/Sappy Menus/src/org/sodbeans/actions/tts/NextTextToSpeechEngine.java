/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.sodbeans.actions.tts;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Iterator;
import org.openide.awt.ActionRegistration;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionReferences;
import org.openide.awt.ActionID;
import org.openide.util.NbBundle.Messages;
import org.sodbeans.phonemic.TextToSpeechFactory;
import org.sodbeans.phonemic.tts.TextToSpeech;
import org.sodbeans.phonemic.tts.TextToSpeechEngine;
import org.sodbeans.tts.options.api.TextToSpeechOptions;

@ActionID(category = "TextToSpeech",
id = "org.sodbeans.actions.tts.NextTextToSpeechEngine")
@ActionRegistration(displayName = "#CTL_NextTextToSpeechEngine")
@ActionReferences({
    @ActionReference(path = "Menu/Tools/Text to Speech", position = 100),
    @ActionReference(path = "Shortcuts", name = "DO-F2")
})
@Messages("CTL_NextTextToSpeechEngine=Next Speech Engine")
public final class NextTextToSpeechEngine implements ActionListener {
    private TextToSpeech speech = TextToSpeechFactory.getDefaultTextToSpeech();
    private final String JAWS_READABLE_NAME = "Jaws";
    private final String NVDA_READABLE_NAME = "NVDA";
    private final String SAPI_READABLE_NAME = "Microsoft SAPI";
    private final String CARBON_READABLE_NAME = "Mac OS X VoiceOver";
    private final String SAY_READABLE_NAME = "Mac OS X Say";
    private final String SPEECH_DISPATCHER_READABLE_NAME = "Speech Dispatcher";
    
    public void actionPerformed(ActionEvent e) {
        boolean changed = false;
        Iterator<TextToSpeechEngine> engines = speech.getAvailableEngines();
        TextToSpeechEngine firstEngine = engines.next();
        TextToSpeechEngine currentEngine = firstEngine;
        TextToSpeechEngine newEngine = firstEngine;
        
        // Loop through, find the current voice (if found) and set to next voice.
        // If the current voice couldn't be found, set it to the first one we find.
        TextToSpeechEngine eng = speech.getTextToSpeechEngine();
        
        if (eng != null) {
            boolean found = false;
            while (engines.hasNext() && !changed) {
                if (eng == currentEngine) {
                    while (engines.hasNext() && !changed) {
                        newEngine = engines.next();
                        changed = speech.setTextToSpeechEngine(newEngine);
                    }
                }
                if (engines.hasNext())
                    currentEngine = engines.next();
            }
        }
        
        if (!changed) {
            // Go to the first engine.
            newEngine = firstEngine;
            changed = speech.setTextToSpeechEngine(firstEngine);
        }
        
        // Was that change successful?
        if (changed) {
            speech.setVolume(0.5);
            speech.setSpeed(0.5);
            speech.setPitch(0.5);
            if(TextToSpeechOptions.isScreenReading()) {
                speech.speak(lookupEngineObject(newEngine) + " is the new Text to Speech engine.");
            }

            TextToSpeechOptions.setSpeechEngine(newEngine.toString());
        }
        else {
            if(TextToSpeechOptions.isScreenReading()) {
                speech.speak("Text to Speech engine was not changed. There were no usable options.");
            }
        }
        // Reset  all other settings.
        TextToSpeechOptions.setSelectedVoice("");
        TextToSpeechOptions.setSpeechSpeed(50);
        TextToSpeechOptions.setSpeechPitch(50);
        TextToSpeechOptions.setSpeechVolume(50);
    }

    /**
     * Returns a human readable form of the TTS object given
     * @param engine the engine requested
     * @return the human readable name
     */
    private String lookupEngineObject(TextToSpeechEngine engine) {
        HashMap<TextToSpeechEngine, String> names = new HashMap<TextToSpeechEngine, String>();
        
        names.put(TextToSpeechEngine.JAWS, JAWS_READABLE_NAME);
        names.put(TextToSpeechEngine.NVDA, NVDA_READABLE_NAME);
        names.put(TextToSpeechEngine.MICROSOFT_SAPI, SAPI_READABLE_NAME);
        names.put(TextToSpeechEngine.APPLE_CARBON, CARBON_READABLE_NAME);
        names.put(TextToSpeechEngine.APPLE_SAY, SAY_READABLE_NAME);
        names.put(TextToSpeechEngine.SPEECH_DISPATCHER, SPEECH_DISPATCHER_READABLE_NAME);
        
        return names.get(engine);        
    }
}
