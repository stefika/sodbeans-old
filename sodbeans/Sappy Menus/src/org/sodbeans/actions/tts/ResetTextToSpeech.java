/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.sodbeans.actions.tts;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import org.openide.awt.ActionRegistration;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionReferences;
import org.openide.awt.ActionID;
import org.openide.util.NbBundle.Messages;
import org.sodbeans.phonemic.SpeechVoice;
import org.sodbeans.phonemic.TextToSpeechFactory;
import org.sodbeans.phonemic.tts.TextToSpeech;
import org.sodbeans.tts.options.api.TextToSpeechOptions;

@ActionID(category = "TextToSpeech",
id = "org.sodbeans.actions.tts.ResetTextToSpeech")
@ActionRegistration(displayName = "#CTL_ResetTextToSpeech")
@ActionReferences({
    @ActionReference(path = "Menu/Tools/Text to Speech", position = 0),
    @ActionReference(path = "Shortcuts", name = "DO-F1")
})
@Messages("CTL_ResetTextToSpeech=Reset Speech Engine")
public final class ResetTextToSpeech implements ActionListener {
    private TextToSpeech speech = TextToSpeechFactory.getDefaultTextToSpeech();
    
    @Override
    public void actionPerformed(ActionEvent e) {
        if(speech != null) {
            speech.reinitialize();
            
            // Reload all user settings.
            // TODO: actually reload all user settings.
            SpeechVoice currentVoice = speech.getCurrentVoice();
            if(TextToSpeechOptions.isScreenReading()) {
                if(currentVoice != null) {
                    speech.speak("Speech engine reset. The current voice is now " + currentVoice.getName());
                }
                else {
                    speech.speak("Speech engine reset");
                }
            }
        }
    }
}
