/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.sodbeans.actions.tts;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import org.openide.awt.ActionRegistration;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionReferences;
import org.openide.awt.ActionID;
import org.openide.util.NbBundle.Messages;
import org.sodbeans.phonemic.TextToSpeechFactory;
import org.sodbeans.phonemic.tts.TextToSpeech;
import org.sodbeans.tts.options.api.TextToSpeechOptions;

@ActionID(category = "TextToSpeech",
id = "org.sodbeans.actions.tts.RaiseTextToSpeechSpeed")
@ActionRegistration(displayName = "#CTL_RaiseTextToSpeechSpeed")
@ActionReferences({
    @ActionReference(path = "Menu/Tools/Text to Speech", position = 500),
    @ActionReference(path = "Shortcuts", name = "DO-F6")
})
@Messages("CTL_RaiseTextToSpeechSpeed=Raise Speech Speed")
public final class RaiseTextToSpeechSpeed implements ActionListener {
    private TextToSpeech speech = TextToSpeechFactory.getDefaultTextToSpeech();

    public void actionPerformed(ActionEvent e) {
        // Does this engine support pitch changes?
        if (!speech.canSetSpeed()) {
            if(TextToSpeechOptions.isScreenReading()) {
                speech.speak("The engine you are using does not support speed changes.");
            }
            return;
        }
        
        double currentSpeed = speech.getSpeed();
        
        if (currentSpeed < 1) {
            // Up the speed by 0.1
            double newSpeed = currentSpeed + 0.1;
            speech.setSpeed(newSpeed);
            if(TextToSpeechOptions.isScreenReading()) {
                speech.speak("Text to Speech speed raised.");
            }
            
            if (newSpeed >= 0 && newSpeed <= 1.0)
                TextToSpeechOptions.setSpeechSpeed((int)(newSpeed * 100));
        }
        else {
            // We are at the maximum.
            if(TextToSpeechOptions.isScreenReading()) {
                speech.speak("You are already at the highest speed.");
            }
        }
    }
}
