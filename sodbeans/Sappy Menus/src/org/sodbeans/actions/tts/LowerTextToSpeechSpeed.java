/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.sodbeans.actions.tts;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import org.openide.awt.ActionRegistration;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionReferences;
import org.openide.awt.ActionID;
import org.openide.util.NbBundle.Messages;
import org.sodbeans.phonemic.SpeechPriority;
import org.sodbeans.phonemic.TextToSpeechFactory;
import org.sodbeans.phonemic.tts.TextToSpeech;
import org.sodbeans.tts.options.api.TextToSpeechOptions;

@ActionID(category = "TextToSpeech",
id = "org.sodbeans.actions.tts.LowerTextToSpeechSpeed")
@ActionRegistration(displayName = "#CTL_LowerTextToSpeechSpeed")
@ActionReferences({
    @ActionReference(path = "Menu/Tools/Text to Speech", position = 600, separatorAfter = 650),
    @ActionReference(path = "Shortcuts", name = "DO-F7")
})
@Messages("CTL_LowerTextToSpeechSpeed=Lower Speech Speed")
public final class LowerTextToSpeechSpeed implements ActionListener {
    private TextToSpeech speech = TextToSpeechFactory.getDefaultTextToSpeech();

    public void actionPerformed(ActionEvent e) {
        // Does this engine support pitch changes?
        if (!speech.canSetSpeed()) {
            if(TextToSpeechOptions.isScreenReading()) {
                speech.speak("The engine you are using does not support speed changes.");
            }
            return;
        }

        double currentSpeed = speech.getSpeed();
        
        if (currentSpeed > 0) {
            // Lower the pitch by 0.1
            double newSpeed = currentSpeed - 0.1;
            speech.setSpeed(newSpeed);
            if(TextToSpeechOptions.isScreenReading()) {
                speech.speak("Text to Speech speed lowered.");
            }
            
            // TODO: Store this setting.
            if (newSpeed >= 0 && newSpeed <= 1.0)
                TextToSpeechOptions.setSpeechPitch((int)(newSpeed * 100));
        }
        else {
            // We are at the minimum.
            if(TextToSpeechOptions.isScreenReading()) {
                speech.speak("You are already at the lowest speed.");
            }
        }
    }
}
