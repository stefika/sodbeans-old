/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.sodbeans.actions.tts;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Iterator;
import org.openide.awt.ActionRegistration;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionReferences;
import org.openide.awt.ActionID;
import org.openide.util.NbBundle.Messages;
import org.sodbeans.phonemic.SpeechVoice;
import org.sodbeans.phonemic.TextToSpeechFactory;
import org.sodbeans.phonemic.tts.TextToSpeech;
import org.sodbeans.tts.options.api.TextToSpeechOptions;

@ActionID(category = "TextToSpeech",
id = "org.sodbeans.actions.tts.NextTextToSpeechVoice")
@ActionRegistration(displayName = "#CTL_NextTextToSpeechVoice")
@ActionReferences({
    @ActionReference(path = "Menu/Tools/Text to Speech", position = 200, separatorAfter = 250),
    @ActionReference(path = "Shortcuts", name = "DO-F3")
})
@Messages("CTL_NextTextToSpeechVoice=Next Speech Voice")
public final class NextTextToSpeechVoice implements ActionListener {
    private TextToSpeech speech = TextToSpeechFactory.getDefaultTextToSpeech();

    public void actionPerformed(ActionEvent e) {
        if (!speech.canSetVoice()) {
            if(TextToSpeechOptions.isScreenReading()) {
                speech.speak("The engine you are using does not support voice changes.");
            }
            return;
        }
        
        Iterator<SpeechVoice> voices = speech.getAvailableVoices();
        if (!voices.hasNext()) {
            if(TextToSpeechOptions.isScreenReading()) {
                speech.speak("Text to Speech voice was not changed, as there are no alternative voices.");
            }
            return;
        }
        
        SpeechVoice firstVoice = voices.next();
        SpeechVoice currentVoice = firstVoice;
        SpeechVoice newVoice = firstVoice;
        
        // Loop through, find the current voice (if found) and set to next voice.
        // If the current voice couldn't be found, set it to the first one we find.
        SpeechVoice v = speech.getCurrentVoice();
        
        if (v != null) {
            boolean found = false;
            while (voices.hasNext() && !found) {
                if (v.getName().equals(currentVoice.getName())) {
                    if (voices.hasNext())
                        newVoice = voices.next();
                    found = true;
                }
                if (voices.hasNext())
                    currentVoice = voices.next();
            }
        }
        
        boolean result = speech.setVoice(newVoice);
        
        if (result) {
            if(TextToSpeechOptions.isScreenReading()) {
                speech.speak(newVoice.getName() + " is the new Text to Speech voice.");
            }
            TextToSpeechOptions.setSelectedVoice(newVoice.getName());
        }
    }
}
