/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sodbeans.quorumeditor;

import javax.swing.JEditorPane;
import org.netbeans.modules.editor.NbEditorKit;

/**
 *
 * @author Andrew Haywood
 */
public class QuorumEditorKit extends NbEditorKit{
    //QuorumKeyListener quorumKeyListener;
    public QuorumEditorKit() {
        super();
    }

    @Override
    public String getContentType() {
        return "text/x-quorum";
    }

    @Override
    public void install(JEditorPane pane) {
        super.install(pane);
        //quorumKeyListener = new QuorumKeyListener(pane);
    }
}
