/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.sodbeans.tod.customtypes;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Vector;
import org.tod.api.TODSession;
import org.tod.meta.variables.FieldVariable;
import org.tod.meta.variables.MirroredObjectInstance;
import org.tod.meta.variables.Variable;
import org.tod.meta.variables.MirrorFactory;
import org.tod.meta.variables.MirroredPrimitive;
import org.tod.meta.variables.MirroredValue;
import tod.core.database.browser.IEventBrowser;
import tod.core.database.event.IBehaviorCallEvent;
import tod.core.database.event.ILogEvent;
import tod.core.database.structure.IBehaviorInfo;
import tod.core.database.structure.ObjectId;
import tod.impl.common.event.FieldWriteEvent;

/**
 *
 * @author jeff
 */
public class MirroredQuorumArray extends MirroredObjectInstance {

    /**
     * Method names we are interested in for the Quorum array.
     */
    private static final String ADD = "AddNative";
    private static final String REMOVE_AT = "RemoveAtNative";
    private static final String SET = "SetNative";
    private static final String SET_SIZE = "SetSizeNative";
    private static final String SET_AUTO_RESIZE = "SetAutoResizeNative";
    private static final String SET_MAX_SIZE = "SetMaxSizeNative";
    private static final String EMPTY = "Empty";
    /**
     * Our emulating array.
     */
    private QuorumArray array = new QuorumArray();
    /**
     * Have we loaded the array?
     */
    private boolean loaded = false;
    /**
     * The TOD object we came from.
     */
    private ObjectId todObject = null;
    private TODSession session = null;
    /**
     * The maximum point in time in which this object is being considered. A
     * value of Long.MAX_VALUE indicates that the most recent event data is to
     * be used when constructing the object's field values.
     */
    private long validTimestamp = Long.MAX_VALUE;

    public MirroredQuorumArray(TODSession session, ObjectId ref, String typeSig, long validTimestamp) {
        this.init(session, ref, typeSig, validTimestamp);
    }

    private void init(TODSession session, ObjectId ref, String typeSig, long validTimestamp) {
        this.session = session;
        this.todObject = ref;
        this.validTimestamp = validTimestamp;
    }

    @Override
    public Iterator<FieldVariable> getFields() {
        // don't display any fields
        ArrayList<FieldVariable> empty = new ArrayList<FieldVariable>();

        return empty.iterator();
    }

    @Override
    public FieldVariable getField(String name) {
        return null;
    }

    @Override
    public int getNumberOfFields() {
        if (!loaded) {
            loadArray();
        }

        return this.array.getArray().length;
    }

    public MirroredValue[] getArray() {
        if (!loaded) {
            loadArray();
        }
        return this.array.getArray();
    }

    /**
     * Load all field information for this class.
     */
    private void loadArray() {
        if (loaded) {
            return;
        }

        IEventBrowser allEvents = session.getTODHandler().getObjectBrowser(this.todObject);
        IEventBrowser methodCalls = session.getTODHandler().filterMethodCalls(allEvents);

        if (!methodCalls.hasNext()) {
            return;
        }

        ILogEvent currentEvent;
        do {
            currentEvent = methodCalls.next();

            if (currentEvent instanceof IBehaviorCallEvent) {
                IBehaviorCallEvent call = (IBehaviorCallEvent) currentEvent;
                this.updateArray(call);
            }
        } while (methodCalls.hasNext());

        this.loaded = true;
        // TODO
    }

    private void updateArray(IBehaviorCallEvent call) {
        if (call.getCalledBehavior() == null) {
            return;
        }

        String methodName = call.getCalledBehavior().getName();
        Object[] arguments = call.getArguments();

        if (methodName.equals(ADD)) {
            this.array.Add(arguments);
        } else if (methodName.equals(REMOVE_AT)) {
            this.array.RemoveAt(arguments);
        } else if (methodName.equals(SET)) {
            this.array.Set(arguments);
        } else if (methodName.equals(SET_SIZE)) {
            this.array.SetSize(arguments);
        } else if (methodName.equals(SET_AUTO_RESIZE)) {
            this.array.SetAutoResize(arguments);
        } else if (methodName.equals(SET_MAX_SIZE)) {
            this.array.SetMaxSize(arguments);
        } else if (methodName.equals(EMPTY)) {
            this.array.Empty();
        }
    }

    @Override
    public String getTypeSignature() {
        return "Lquorum/Libraries/Containers/Array;";
    }

    @Override
    public String toString() {
        return "#" + this.todObject.hashCode();
    }

    /**
     * This class emulates a real array on the system, so we may display its
     * contents to the variables window.
     */
    private class QuorumArray {

        private Vector<MirroredValue> array = new Vector<MirroredValue>();

        public QuorumArray() {
        }

        // object
        // int object
        public void Add(Object[] arguments) {
            if (arguments.length == 1) {
                MirroredValue i = MirrorFactory.instanceFromTOD(session, arguments[0], "Lquorum/Libraries/Language/Object");
                array.add(i);
            } else if (arguments.length == 2) {
                MirroredPrimitive i = (MirroredPrimitive) MirrorFactory.instanceFromTOD(session, arguments[0], "I");
                MirroredValue ii = MirrorFactory.instanceFromTOD(session, arguments[1], "Lquorum/Libraries/Language/Object");
                array.add((Integer) i.getValue(), ii);
            }
        }

        // int
        public void RemoveAt(Object[] arguments) {
            if (arguments.length == 1) {
                MirroredPrimitive i = (MirroredPrimitive) MirrorFactory.instanceFromTOD(session, arguments[0], "I");
                int idx = (Integer) i.getValue();
                array.remove(idx);
            }
        }

        // int object
        public void Set(Object[] arguments) {
            if (arguments.length == 2) {
                MirroredPrimitive i = (MirroredPrimitive) MirrorFactory.instanceFromTOD(session, arguments[0], "I");
                MirroredValue ii = MirrorFactory.instanceFromTOD(session, arguments[1], "Lquorum/Libraries/Language/Object");
                array.set((Integer) i.getValue(), ii);
            }
        }

        // int
        public void SetSize(Object[] arguments) {
            if (arguments.length == 1) {
                MirroredPrimitive i = (MirroredPrimitive) MirrorFactory.instanceFromTOD(session, arguments[0], "I");
                int idx = (Integer) i.getValue();
                array.setSize(idx);
            }
        }

        // boolean
        public void SetAutoResize(Object[] arguments) {
            // do nothing
        }

        // int
        public void SetMaxSize(Object[] arguments) {
            if (arguments.length == 1) {
                MirroredPrimitive i = (MirroredPrimitive) MirrorFactory.instanceFromTOD(session, arguments[0], "I");
                int size = (Integer) i.getValue();
                if (array.capacity() > size) {
                    array.trimToSize();
                }
                array.ensureCapacity(size);
            }
        }

        public void Empty() {
            array.clear();
        }

        public MirroredValue[] getArray() {
            return array.toArray(new MirroredValue[array.size()]);
        }
    }
}
