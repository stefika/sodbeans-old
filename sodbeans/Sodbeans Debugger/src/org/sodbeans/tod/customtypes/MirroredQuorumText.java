/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.sodbeans.tod.customtypes;

import org.tod.meta.variables.MirrorFactory;
import com.sun.jdi.Field;
import com.sun.jdi.ObjectReference;
import com.sun.jdi.ReferenceType;
import com.sun.jdi.Value;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import org.tod.api.TODSession;
import org.tod.meta.variables.FieldVariable;
import org.tod.meta.variables.MirroredObjectInstance;
import org.tod.meta.variables.MirroredPrimitive;
import org.tod.meta.variables.MirroredValue;
import org.tod.meta.variables.Variable;
import tod.core.database.browser.IEventBrowser;
import tod.core.database.event.ICallerSideEvent;
import tod.core.database.event.ILogEvent;
import tod.core.database.structure.ObjectId;
import tod.impl.common.event.FieldWriteEvent;


/**
 * Represents an autoboxed text in Quorum.
 * 
 * @author jeff
 */
public class MirroredQuorumText extends MirroredObjectInstance {
    /**
     * The fields for this object.
     */
    private HashMap<String, FieldVariable> fields = new HashMap<String, FieldVariable>();
    
    /**
     * Have we bothered to load fields yet? We won't do so until asked. 
     */
    private boolean loadedFields = false;
    
    /**
     * The TOD object we came from.
     */
    private ObjectId todObject = null;
    
    /**
     * The object type signature from the JVM.
     */
    private String typeSig = null;
    
    private TODSession session = null;
    
    /**
     * The maximum point in time in which this object is being considered.
     * A value of Long.MAX_VALUE indicates that the most recent event data
     * is to be used when constructing the object's field values.
     */
    private long validTimestamp = Long.MAX_VALUE;
    
    public MirroredQuorumText(TODSession session, ObjectId ref, String typeSig, long validTimestamp) {
        this.init(session, ref, typeSig, validTimestamp);
    }
    
    private void init(TODSession session, ObjectId ref, String typeSig, long validTimestamp) {
        this.session = session;
        this.todObject = ref;
        this.typeSig = typeSig;
        this.validTimestamp = validTimestamp;
    }
    
    /**
     * Get all the fields for this instance. This includes both static and
     * non-static fields.
     * 
     * @return 
     */
    @Override
    public Iterator<FieldVariable> getFields() {
        if (!loadedFields) {
            loadFields();
        }
        
        // Don't return any fields.
        ArrayList<FieldVariable> empty = new ArrayList<FieldVariable>();
        return empty.iterator();
    }
    
    /**
     * Get a particular field by name. Note that if the field does not exist,
     * a null value will be returned.
     * @param name the field to grab
     * @return a FieldVariable instance, or null if the variable specified by
     * name doesn't exist.
     */
    @Override
    public FieldVariable getField(String name) {
        if (!loadedFields) {
            loadFields();
        }
        
        return fields.get(name);
    }

    @Override
    public int getNumberOfFields() {
        if (!loadedFields) {
            loadFields();
        }
        
        return 0;
    }

    /**
     * Load all field information for this class.
     */
    private void loadFields() {
        IEventBrowser allEvents = session.getTODHandler().getObjectBrowser(this.todObject);
        IEventBrowser variableWrites = session.getTODHandler().filterFieldWrites(allEvents);
        
        if (!variableWrites.hasNext()) {
            return;
        }
 
        ILogEvent currentEvent;
        do {
            currentEvent = variableWrites.next();
            
            if (currentEvent instanceof FieldWriteEvent) {
                FieldWriteEvent fw = (FieldWriteEvent)currentEvent;                
                this.updateFieldVariable(fw);
            }
        } while (variableWrites.hasNext() && currentEvent.getTimestamp() <= this.validTimestamp);
        
        this.loadedFields = true;
        // TODO
    }


    private void updateFieldVariable(FieldWriteEvent fw) {
        String varName = fw.getField().getName();
        if (!this.session.getFilter().acceptVariableName(varName)) {
            return;
        }
        
        Variable variableFromTOD = MirrorFactory.variableFromTOD(session, fw);
        if (!(variableFromTOD instanceof FieldVariable)) {
            return;
        }
        
        if (this.fields.containsKey(variableFromTOD.getName())) {
            this.fields.remove(variableFromTOD.getName());
        }
        
        this.fields.put(variableFromTOD.getName(), (FieldVariable)variableFromTOD);
    }
    
    @Override
    public String getTypeSignature() {
        return "Lquorum/Libraries/Language/Types/Number;";
    }
    
    @Override
    public String toString() {
        FieldVariable field = this.getField("value");
        
        if (field == null) {
            return "";
        }
        
        MirroredValue value = field.getValue();
        
        if (value instanceof MirroredPrimitive) {
            MirroredPrimitive p = (MirroredPrimitive)value;
            
            return p.getValue().toString();
        } else {
            return "";
        }
    }
}
