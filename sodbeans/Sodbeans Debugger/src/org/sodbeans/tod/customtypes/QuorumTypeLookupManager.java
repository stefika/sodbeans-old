/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.sodbeans.tod.customtypes;

import java.util.HashMap;
import org.tod.meta.variables.MirroredObjectInstance;
import org.tod.meta.variables.TypeLookupManager;

/**
 * An implementation of the TOD {@link TypeLookupManager} for Quorum.
 * This permits us to implement our own views for containers, such as
 * Array.
 * 
 * @author jeff
 */
public class QuorumTypeLookupManager extends TypeLookupManager {
    private HashMap<String, Class<? extends MirroredObjectInstance>> types =
            new HashMap<String, Class<? extends MirroredObjectInstance>>();
    
    public QuorumTypeLookupManager() {
        types.put("Lquorum/Libraries/Language/Types/Boolean;", MirroredQuorumBoolean.class);
        types.put("Lquorum/Libraries/Language/Types/Integer;", MirroredQuorumInteger.class);
        types.put("Lquorum/Libraries/Language/Types/Number;", MirroredQuorumNumber.class);
        types.put("Lquorum/Libraries/Language/Types/Text;", MirroredQuorumText.class);
        types.put("Lquorum/Libraries/Containers/Array;", MirroredQuorumArray.class);
    }
    
    @Override
    public Class<? extends MirroredObjectInstance> lookup(String typeSignature) {
        return types.get(typeSignature);
    }
}
