/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.sodbeans.tod;

import java.util.HashMap;
import org.sodbeans.tod.audio.AssignmentAction;
import org.sodbeans.tod.audio.AudioAction;
import org.sodbeans.tod.audio.MethodCallAction;
import org.sodbeans.tod.audio.MethodReturnAction;
import org.sodbeans.tod.audio.NullAction;
import org.sodbeans.tod.audio.TestConditionalAction;
import org.sodbeans.tod.audio.TestLoopAction;
import org.tod.meta.steps.StepType;

/**
 *
 * @author jeff
 */
public class AudioActionMap {
    private HashMap<StepType, AudioAction> actions = new HashMap<StepType, AudioAction>();
    public AudioActionMap() {
        loadMap();
    }

    private void loadMap() {
        actions.put(StepType.METHOD_CALL, new MethodCallAction());
        actions.put(StepType.ASSIGNMENT, new AssignmentAction());
        actions.put(StepType.UNDEFINED , new NullAction());
        actions.put(StepType.TEST_CONDITIONAL, new TestConditionalAction());
        actions.put(StepType.TEST_LOOP, new TestLoopAction());
        actions.put(StepType.METHOD_RETURN, new MethodReturnAction());
    }
    
    public AudioAction get(StepType type) {
        return actions.get(type);
    }
}
