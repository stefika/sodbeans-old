/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.sodbeans.tod;

import org.sodbeans.phonemic.SpeechPriority;
import org.sodbeans.phonemic.TextToSpeechFactory;
import org.sodbeans.phonemic.tts.TextToSpeech;
import org.sodbeans.tod.audio.AudioAction;
import org.sodbeans.tts.options.api.TextToSpeechOptions;
import org.tod.api.DebugEventListener;
import org.tod.meta.StateInfo;
import org.tod.meta.steps.ProgramStep;

/**
 *
 * @author jeff
 */
public class QuorumAudioListener extends DebugEventListener {
    public static boolean enableSpeaking = true;
    private static AudioActionMap map = new AudioActionMap();
    private static TextToSpeech speech = TextToSpeechFactory.getDefaultTextToSpeech();

    @Override
    public void steppedOver(StateInfo info) {
        String stepAudio = "";
        if (info.hasNext()) {
            ProgramStep step = info.next();

            AudioAction action = map.get(step.getType());
            action.setStep(step);
            stepAudio = action.speak();
        }
        // Speak the text (if any), plus the line number and class.
        int lineNumber = info.getLineNumber();
        String className = TODCompilerUtils.friendlyClassName(info.getClassInfo().getFullyQualifiedName());
        String lineNumberAndClassName = lineNumber + " " + className;
        String actionAudio = lineNumberAndClassName;
        if(TextToSpeechOptions.isScreenReading()) {
            speech.speak(stepAudio + " " + actionAudio, SpeechPriority.HIGH);
        }
    }
    
    @Override
    public void steppedBackOver(StateInfo info) {
        if (!info.hasNext()) {
            return;
        }
        ProgramStep step = info.next();
        
        AudioAction action = map.get(step.getType());
        action.setStep(step);
        if(TextToSpeechOptions.isScreenReading()) {
            speech.speak(action.speak(), SpeechPriority.HIGH);
        }
    }
}
