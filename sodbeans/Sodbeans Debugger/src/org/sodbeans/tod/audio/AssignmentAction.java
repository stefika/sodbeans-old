/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.sodbeans.tod.audio;

import org.tod.meta.steps.AssignmentStep;
import org.tod.meta.steps.ProgramStep;

/**
 *
 * @author jeff
 */
public class AssignmentAction extends AudioAction {
    private AssignmentStep step = null;
    
    @Override
    public void setStep(ProgramStep step) {
        if (step instanceof AssignmentStep) {
            this.step = (AssignmentStep)step;
        }
    }
    
    @Override
    public ProgramStep getStep() {
        return step;
    }
    @Override
    public String speak() {
        if (step == null) {
            return "";
        }
        
        if (step.isForward()) {
            return step.getVariable().getName() + " to " + step.getVariable().getValue() + ".";
        } else {
            return step.getVariable().getName() + " unset from " + step.getVariable().getValue() + ".";
        }
    }
    
}
