/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.sodbeans.tod.audio;

import org.tod.meta.steps.ProgramStep;
import org.tod.meta.steps.TestLoopStep;

/**
 *
 * @author melissa
 */
public class TestLoopAction extends AudioAction {
    private TestLoopStep step = null;
    
    @Override
    public void setStep(ProgramStep step) {
        if (!(step instanceof TestLoopStep)) {
            return;
        }
        
        this.step = (TestLoopStep)step;
    }

    @Override
    public ProgramStep getStep() {
        return step;
    }
    
    @Override
    public String speak() {
        if (step.isForward()) {
            return step.getLoop().getType().name().toLowerCase() + step.loopInfo() + ".";
        } else {
            return step.getLoop().getType().name().toLowerCase() + step.loopInfo() + " restored.";
        }
    }
    
}
