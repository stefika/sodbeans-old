/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.sodbeans.tod.audio;

import org.openide.util.Lookup;
import org.sodbeans.io.CommandLine;
import org.tod.meta.steps.MethodCallStep;
import org.tod.meta.steps.ProgramStep;

/**
 *
 * @author jeff
 */
public class MethodCallAction extends AudioAction {
    private static CommandLine console = Lookup.getDefault().lookup(CommandLine.class);
    public MethodCallStep step = null;
    
    @Override
    public void setStep(ProgramStep step) {
        if (step instanceof MethodCallStep) {
            this.step = (MethodCallStep)step;
        }
    }

    @Override
    public ProgramStep getStep() {
        return step;
    }
    
    @Override
    public String speak() {
        if (step == null) {
            return "";
        }
        
        String name = step.getTargetMethodInfo().getName();
        
        if (name.equals("println")) {
            if (step.isForward()) {
                if (!step.isLive()) {
                    console.post(step.getParameters()[0].toString());
                }
                return step.getParameters()[0] + " added to the console.";
            } else {
                console.unpost();
               return step.getParameters()[0] + " removed from the console.";
            }            
        } else if (name.equals("StaticSay")) {
            // We only speak "say" on a non-live traversal, as it will be
            // spoken by the Quorum program in live mode.
            if (!step.isLive()) {
                return step.getParameters()[0].toString();
            } else {
                return ""; // say nothing
            }
        } else {
            if (step.isForward()) {
                return "Calling action " + name + ".";
            } else {
                return "Un-calling action " + name + ".";
            }
        }
    }
    
}
