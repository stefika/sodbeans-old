/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.sodbeans.tod.audio;

import org.tod.meta.steps.ProgramStep;

/**
 * Speaks nothing in the event of an undefined step.
 * @author jeff
 */
public class NullAction extends AudioAction {

    @Override
    public void setStep(ProgramStep step) {
    }

    @Override
    public String speak() {
        return "";
    }
    
    @Override
    public ProgramStep getStep() {
        return null;
    }
}
