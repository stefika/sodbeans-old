/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.sodbeans.tod.audio;

import org.openide.util.Lookup;
import org.sodbeans.io.CommandLine;
import org.tod.meta.steps.MethodCallStep;
import org.tod.meta.steps.MethodReturnStep;
import org.tod.meta.steps.ProgramStep;

/**
 *
 * @author jeff
 */
public class MethodReturnAction extends AudioAction {

    private static CommandLine console = Lookup.getDefault().lookup(CommandLine.class);
    public MethodReturnStep step = null;

    @Override
    public void setStep(ProgramStep step) {
        if (step instanceof MethodReturnStep) {
            this.step = (MethodReturnStep) step;
        }
    }

    @Override
    public ProgramStep getStep() {
        return step;
    }

    @Override
    public String speak() {
        if (step == null) {
            return "";
        }

        String name = step.getMethodInfo().getName();

        if (step.isForward()) {
            return "Returning from action.";
        } else {
            return "Returning to action.";
        }
    }
}
