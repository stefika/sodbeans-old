/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.sodbeans.tod.audio;

import org.tod.meta.steps.ProgramStep;

/**
 *
 * @author jeff
 */
public abstract class AudioAction {
    public abstract void setStep(ProgramStep step);
    public abstract ProgramStep getStep();
    public abstract String speak();
}
