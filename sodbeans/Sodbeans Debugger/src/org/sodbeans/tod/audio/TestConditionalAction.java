/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.sodbeans.tod.audio;

import org.tod.meta.steps.ProgramStep;
import org.tod.meta.steps.TestConditionalStep;

/**
 * Tell the user whether a conditional evaluated to true or false.
 * 
 * @author jeff
 */
public class TestConditionalAction extends AudioAction {
    private TestConditionalStep step = null;
    
    @Override
    public void setStep(ProgramStep step) {
        if (!(step instanceof TestConditionalStep)) {
            return;
        }
        
        this.step = (TestConditionalStep)step;
    }

    @Override
    public ProgramStep getStep() {
        return step;
    }
    
    @Override
    public String speak() {
        if (step.isForward()) {
            return step.getConditional().getType().name().toLowerCase() + step.wasTrue() + ".";
        } else {
            return step.getConditional().getType().name().toLowerCase() + step.wasTrue() + " restored.";
        }
    }
    
}
