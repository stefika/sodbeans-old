/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.sodbeans.tod;

import org.tod.api.DebugEventListener;
import org.tod.meta.BreakpointInfo;
import org.tod.meta.StateInfo;

/**
 *
 * @author jeff
 */
public class TODDebugEventListener extends DebugEventListener {
    private TODAnnotationUpdater annotationUpdater = new TODAnnotationUpdater();
    
    public TODDebugEventListener(TODAnnotationUpdater annotationUpdater) {
        this.annotationUpdater = annotationUpdater;
    }
    
    @Override
    public void breakpointReached(BreakpointInfo info) {
        String fullyQualifiedClassName = info.getClassInfo().getFullyQualifiedName();
        int lineNumber = info.getLineNumber();
        this.annotationUpdater.update(fullyQualifiedClassName, lineNumber);
    }

    @Override
    public void steppedOver(StateInfo info) {
        String fullyQualifiedClassName = info.getClassInfo().getFullyQualifiedName();
        int lineNumber = info.getLineNumber();
                
        // If we end up back in Java's "main" method, the debugging session
        // has reached the end of the execution, and we should not update the
        // annotator.
        if (!info.getMethodInfo().getSignature().equals("main([Ljava/lang/String;)V")) {
            this.annotationUpdater.update(fullyQualifiedClassName, lineNumber);
        }
    }

    @Override
    public void steppedInto(StateInfo info) {
        String fullyQualifiedClassName = info.getClassInfo().getFullyQualifiedName();
        int lineNumber = info.getLineNumber();

        this.annotationUpdater.update(fullyQualifiedClassName, lineNumber);
    }

    @Override
    public void steppedOut(StateInfo info) {
        //throw new UnsupportedOperationException("Not supported yet.");
    }
    
    @Override
    public void steppedBackOver(StateInfo info) {
        String fullyQualifiedClassName = info.getClassInfo().getFullyQualifiedName();
        int lineNumber = info.getLineNumber();

        this.annotationUpdater.update(fullyQualifiedClassName, lineNumber);
    }
}
