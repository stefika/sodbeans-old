/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.sodbeans.tod;

import java.util.ArrayList;
import java.util.Iterator;
import org.netbeans.spi.viewmodel.ModelEvent;
import org.netbeans.spi.viewmodel.ModelListener;
import org.netbeans.spi.viewmodel.NodeModel;
import org.netbeans.spi.viewmodel.TableModel;
import org.netbeans.spi.viewmodel.TreeModel;
import org.netbeans.spi.viewmodel.UnknownTypeException;
import org.sodbeans.tod.customtypes.MirroredQuorumArray;
import org.tod.TODSessionFactory;
import org.tod.api.DebugEventListener;
import org.tod.meta.variables.VariablesWatcher;
import org.tod.meta.BreakpointInfo;
import org.tod.meta.variables.FieldVariable;
import org.tod.meta.variables.MirroredObjectInstance;
import org.tod.meta.variables.MirroredValue;
import org.tod.meta.variables.Variable;

/**
 *
 * @author jeff
 */
public class TODDataStorage extends DebugEventListener implements TreeModel, NodeModel, TableModel {
    private final String TOD_ROOT = "Root";
    private static final String TYPE = "LocalsType";
    private static final String VALUE = "LocalsValue";
    private ArrayList<ModelListener> listeners = new ArrayList<ModelListener>();
    public static final String LOCAL_VARIABLE_IMAGE =
        "org/netbeans/modules/debugger/resources/localsView/LocalVariable";

    public static final String LOCAL_VARIABLE_GROUP_IMAGE =
        "org/netbeans/modules/debugger/resources/localsView/LocalVariablesGroup";

    public static final String SUPER_VARIABLE_IMAGE =
        "org/netbeans/modules/debugger/resources/watchesView/SuperVariable";

    public static final String FIELD_IMAGE =
        "org/netbeans/modules/debugger/resources/watchesView/Field";
    
    private static TODDataStorage instance = null;
    
    private TODDataStorage() {
        
    }
    
    public static TODDataStorage getInstance() {
        if (instance == null) {
            instance = new TODDataStorage();
        }
        
        return instance;
    }
    @Override
    public Object getRoot() {
        return this.TOD_ROOT;
    }

    @Override
    public Object[] getChildren(Object parent, int i, int i1) throws UnknownTypeException {
        boolean running = TODSessionFactory.getDefault().isActive();
        if (!running) {
            return new Object[]{};
        }
        
        VariablesWatcher vars = TODSessionFactory.getDefault().getVarialesWatcher();
        ArrayList<Object> kids = new ArrayList<Object>();
        if(parent instanceof String) {
            String s = (String) parent;
            if(s.compareTo(this.TOD_ROOT)==0) {
                MirroredObjectInstance moi = vars.getThis();
                if(moi != null) {
                    TODDataStorage.ThisObject th = new TODDataStorage.ThisObject();
                    th.myThis = moi;
                    kids.add(th);
                }
                
                // Add variables in scope.
                Iterator<Variable> locals = vars.getVariablesInScope();
                while (locals.hasNext()) {
                    Variable var = locals.next();
                    ChildNode n = new ChildNode();
                    n.data = var;
                    n.parent = moi;
                    kids.add(n);
                }
            }
        } else if (parent instanceof ThisObject) {
            ThisObject t = (ThisObject)parent;
            MirroredObjectInstance me = t.myThis;
            Iterator<FieldVariable> fields = me.getFields();
            while (fields.hasNext()) {
                ChildNode n = new ChildNode();
                n.data = fields.next();
                n.parent = me;
                kids.add(n);
            }
        } else if (parent instanceof ChildNode) {
            ChildNode t = (ChildNode)parent;
            if (t.data.getValue() instanceof MirroredObjectInstance) {
                MirroredObjectInstance o = (MirroredObjectInstance)t.data.getValue();
                if (o instanceof MirroredQuorumArray) {
                    MirroredQuorumArray array = (MirroredQuorumArray)o;
                    MirroredValue[] values = array.getArray();
                    for (int x = 0; x < values.length; x++) {
                        ArrayElementNode n = new ArrayElementNode();
                        n.index = x;
                        n.parent = array;
                        n.data = values[x];
                        kids.add(n);
                    }
                } else {
                    Iterator<FieldVariable> fields = o.getFields();
                    while (fields.hasNext()) {
                        ChildNode n = new ChildNode();
                        n.data = fields.next();
                        n.parent = o;
                        kids.add(n);
                    }
                }
            }
        } else if (parent instanceof ArrayElementNode) {
            ArrayElementNode t = (ArrayElementNode)parent;
            if (t.data instanceof MirroredObjectInstance) {
                MirroredObjectInstance o = (MirroredObjectInstance)t.data;
                if (o instanceof MirroredQuorumArray) {
                    MirroredQuorumArray array = (MirroredQuorumArray)o;
                    MirroredValue[] values = array.getArray();
                    for (int x = 0; x < values.length; x++) {
                        ArrayElementNode n = new ArrayElementNode();
                        n.index = x;
                        n.parent = array;
                        n.data = values[x];
                        kids.add(n);
                    }
                } else {
                    Iterator<FieldVariable> fields = o.getFields();
                    while (fields.hasNext()) {
                        ChildNode n = new ChildNode();
                        n.data = fields.next();
                        n.parent = o;
                        kids.add(n);
                    }
                }
            }
        }
        
        return kids.toArray();
    }

    @Override
    public boolean isLeaf(Object o) throws UnknownTypeException {
        return getChildrenCount(o) == 0;
    }

    @Override
    public int getChildrenCount(Object parent) throws UnknownTypeException {
        boolean running = TODSessionFactory.getDefault().isActive();
        if (!running) {
            return 0;
        }
        
        VariablesWatcher vars = TODSessionFactory.getDefault().getVarialesWatcher();

        if(parent instanceof String) {
            String s = (String) parent;
            if(s.compareTo(this.TOD_ROOT)==0) {
                MirroredObjectInstance moi = vars.getThis();
                if (moi != null) {
                    return moi.getNumberOfFields() + 1;
                }
            }
        } else if (parent instanceof ThisObject) {
            ThisObject t = (ThisObject)parent;
            MirroredObjectInstance me = t.myThis;
            return me.getNumberOfFields();
        } else if (parent instanceof ChildNode) {
            ChildNode n = (ChildNode)parent;
            if (n.data.getValue() instanceof MirroredObjectInstance) {
                MirroredObjectInstance moi = (MirroredObjectInstance)n.data.getValue();
                return moi.getNumberOfFields();
            }
        } else if (parent instanceof ArrayElementNode) {
            ArrayElementNode array = (ArrayElementNode)parent;
            MirroredValue value = array.data;
            if (value instanceof MirroredObjectInstance) {
                MirroredObjectInstance moi = (MirroredObjectInstance)value;
                return moi.getNumberOfFields();
            }
        }
        
        return 0;
    }

    @Override
    public void addModelListener(ModelListener ml) {
        listeners.add(ml);
    }

    @Override
    public void removeModelListener(ModelListener ml) {
        listeners.remove(ml);
    }

    @Override
    public String getDisplayName(Object node) throws UnknownTypeException {
        boolean running = TODSessionFactory.getDefault().isActive();
        if (!running) {
            return "";
        }
        if (node instanceof ThisObject) {
            return "me";
        }
        else if (node instanceof ChildNode) {
            ChildNode n = (ChildNode)node;
            return n.data.getName();
        }
        else if (node instanceof ArrayElementNode) {
            ArrayElementNode n = (ArrayElementNode)node;
            return "[" + n.index + "]";
        }
        return "";
    }

    @Override
    public String getIconBase(Object o) throws UnknownTypeException {
        VariablesWatcher vars = TODSessionFactory.getDefault().getVarialesWatcher();

        if(o instanceof String) {
            String s = (String) o;
            if(s.compareTo(this.TOD_ROOT)==0) {
                MirroredObjectInstance moi = vars.getThis();
                if (moi != null) {
                    return TODDataStorage.FIELD_IMAGE;
                }
            }
        } else if (o instanceof ThisObject) {
            ThisObject t = (ThisObject)o;
            MirroredObjectInstance me = t.myThis;
            return TODDataStorage.FIELD_IMAGE;
        } else if (o instanceof ChildNode) {
            ChildNode n = (ChildNode)o;
            if (n.data.getValue() instanceof MirroredObjectInstance) {
                MirroredObjectInstance moi = (MirroredObjectInstance)n.data.getValue();
                return TODDataStorage.FIELD_IMAGE;
            }
        }
        
        return TODDataStorage.LOCAL_VARIABLE_IMAGE;
    }

    @Override
    public String getShortDescription(Object node) throws UnknownTypeException {
        boolean running = TODSessionFactory.getDefault().isActive();
        if (!running) {
            return "";
        }
        
        if (node instanceof ThisObject) {
            return "The reference to me";
        }
        else if (node instanceof ChildNode) {
            ChildNode n = (ChildNode)node;
            return n.data.getName() + ", " + TODCompilerUtils.friendlyTypeSignature(n.data.getTypeSignature());
        }
        
        return "";
    }

    @Override
    public Object getValueAt(Object node, String columnID) throws UnknownTypeException {
        boolean running = TODSessionFactory.getDefault().isActive();
        if (!running) {
            return "";
        }
        
        if(node instanceof ThisObject) { //a parent
            ThisObject t = (ThisObject)node;
            MirroredObjectInstance me = t.myThis;
            
            if(columnID.compareTo(TYPE) == 0) {
                return TODCompilerUtils.friendlyTypeSignature(me.getTypeSignature());
            }
            if(columnID.compareTo(VALUE) == 0) {
                return me.toString();
            }
        }
        else if(node instanceof ChildNode) {
            ChildNode n = (ChildNode)node;
            Variable data = n.data;
            if(columnID.compareTo(TYPE) == 0) {
                return TODCompilerUtils.friendlyTypeSignature(data.getTypeSignature());
            }
            if(columnID.compareTo(VALUE) == 0) {
                return data.getValue();
            }
        } else if(node instanceof ArrayElementNode) {
            ArrayElementNode n = (ArrayElementNode)node;
            MirroredValue v = n.data;
            if(columnID.compareTo(TYPE) == 0) {
                if (v == null) {
                    return "undefined";
                }
                return TODCompilerUtils.friendlyTypeSignature(v.getTypeSignature());
            }
            if(columnID.compareTo(VALUE) == 0) {
                if (v == null) {
                    return "undefined";
                }
                return v.toString();
            }
        }
        
        return "";
    }

    @Override
    public boolean isReadOnly(Object o, String columnID) throws UnknownTypeException {
        // Only the "value" column can be changed.
        return !columnID.equals(VALUE);
    }

    @Override
    public void setValueAt(Object o, String string, Object o1) throws UnknownTypeException {
        // TODO
    }
    
    @Override
    public void localsChanged() {
        this.update();
    }
    
    private void update() {
        ModelEvent event = new ModelEvent.TreeChanged(this);
        for(ModelListener l : listeners) {
            l.modelChanged(event);
        }
    }
    
    private class ThisObject {
        MirroredObjectInstance myThis;
    }
    
    private class ChildNode { //represents a DataObject that can be found in a particular scope
        Variable data;
        MirroredObjectInstance parent;
    }
    
    private class ArrayElementNode { // represents an array element
        int index;
        MirroredValue data;
        MirroredQuorumArray parent;
    }
}
