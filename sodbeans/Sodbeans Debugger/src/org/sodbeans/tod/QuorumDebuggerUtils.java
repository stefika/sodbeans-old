/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.sodbeans.tod;

import java.util.HashMap;
import java.util.Iterator;
import javax.swing.JEditorPane;
import javax.swing.text.Element;
import javax.swing.text.StyledDocument;
import org.netbeans.api.debugger.Breakpoint;
import org.netbeans.api.debugger.DebuggerManager;
import org.netbeans.modules.editor.NbEditorUtilities;
import org.openide.cookies.EditorCookie;
import org.openide.cookies.LineCookie;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectNotFoundException;
import org.openide.text.Line;
import org.openide.util.Lookup;
import org.sodbeans.compiler.api.descriptors.CompilerClassDescriptor;
import org.sodbeans.compiler.api.descriptors.CompilerFileDescriptor;
import org.sodbeans.debugger.quorum.QuorumBreakpoint;
import org.tod.TODSessionFactory;

/**
 *
 * @author jeff
 */
public class QuorumDebuggerUtils {
    private static final HashMap<String, String> commonTypes = new HashMap<String, String>();
    private static final String quorumPrefix = "Lquorum/";
    private static org.sodbeans.compiler.api.Compiler compiler =
            Lookup.getDefault().lookup(org.sodbeans.compiler.api.Compiler.class);

    public static void addLineBreakpoint(String name, int targetLine) {
        TODSessionFactory.getDefault().getJVMHandler().addLineBreakpoint(name, targetLine);
    }

    public static void removeLineBreakpoint(String name, int targetLine) {
        TODSessionFactory.getDefault().getJVMHandler().removeLineBreakpoint(name, targetLine);
    }

    public static void refreshTODBreakpoints() {
        Breakpoint[] breakpoints = DebuggerManager.getDebuggerManager().getBreakpoints();
        TODSessionFactory.getDefault().getJVMHandler().removeAllBreakpoints();
        for (int i = 0; i < breakpoints.length; i++) {
            Breakpoint bp = breakpoints[i];
            if (bp instanceof QuorumBreakpoint) {
                QuorumBreakpoint qb = (QuorumBreakpoint)bp;
                Line line = qb.getLine();
                FileObject fo = qb.getFileObject();
                if (compiler != null) {
                    CompilerFileDescriptor fileDescriptor = compiler.getFileDescriptor(fo);
                    int targetLine = line.getLineNumber() + 1;
                    String name = TODCompilerUtils.findJVMClassName(fileDescriptor, targetLine);
                    if (name != null) {
                        QuorumDebuggerUtils.addLineBreakpoint(name, targetLine);
                    }
                }
            }
        }
    }
    
    public static void jumpToCallStackLocation(String fullyQualifiedClassName, int targetLine){
        if(compiler == null) {
            return;
        }
        FileObject fo = lookupQuorumFile(fullyQualifiedClassName);
        if(fo == null) {
            return;
        }
        DataObject dataObject = null;

        try { //if this is not the file, open it
            
            dataObject = DataObject.find(fo);
        } catch (DataObjectNotFoundException exception) {
        }

        try {
            EditorCookie ck = dataObject.getCookie(EditorCookie.class);
            if (ck != null) {
                StyledDocument document = ck.getDocument();
                int lineNumber = targetLine - 1;
                if(document != null) {
                    
                    Element e = document.getDefaultRootElement();
                    if (e != null && lineNumber != -1) {
                        e = e.getElement(lineNumber);
                        //final int startOfLine = e.getStartOffset();
                        //Line myLine = NbEditorUtilities.getLine(document, startOfLine, false);
                        //updateLine(myLine);
                        jumpToLine(dataObject, lineNumber);
                    } else {
                        if (dataObject != null) {
                            openEditor(dataObject);
                        }
                    }
                }
                else {
                    if (dataObject != null) {
                        openEditorAndJump(dataObject, lineNumber);
                    }
                }
            }
        } catch (Exception exception) {
        }
    }
    
        /**
     * Forces the editor to jump to the line in question, useful for debugging.
     *
     * @param dataObj
     * @param line
     */
    private static void jumpToLine(final DataObject dataObj, final int line) {
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                try {
                    EditorCookie ck = dataObj.getCookie(EditorCookie.class);
                    if (ck != null) {
                        //ck.openDocument();
                        JEditorPane[] p = ck.getOpenedPanes();
                        if (p.length > 0) {
                            //Need to do this since we're disabling the window system's
                            //auto focus mechanism
                            p[0].requestFocus();
                            if (dataObj != null) {
                                LineCookie lc = dataObj.getCookie(LineCookie.class);
                                if (lc == null) {
                                    return;
                                }
                                Line l = lc.getLineSet().getOriginal(line);
                                l.show(Line.ShowOpenType.OPEN, Line.ShowVisibilityType.FOCUS);
                            }
                        }
                    }
                } catch (Exception exception) {
                }
            }
        });
    }
    
        /**
     * Opens an editor on the screen.
     *
     * TODO: If opening a system file, make it open in a read only editor.
     * 
     * @param dataObj
     */
    private static void openEditor(DataObject dataObj) {
        final EditorCookie.Observable ec = dataObj.getCookie(EditorCookie.Observable.class);
        if (ec != null) {
            javax.swing.SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    ec.open();
                }
            });
        }
    }

    private static void openEditorAndJump(final DataObject dataObj, final int line) {
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                try {
                    EditorCookie ck = dataObj.getCookie(EditorCookie.class);
                    final EditorCookie.Observable ec = dataObj.getCookie(EditorCookie.Observable.class);
                    if (ck != null && ec != null) {
                        ec.open();
                        //open the document
                        JEditorPane[] p = ck.getOpenedPanes();
                        if (p.length > 0) {
                            //Need to do this since we're disabling the window system's
                            //auto focus mechanism
                            p[0].requestFocus();
                            if (dataObj != null) {
                                LineCookie lc = dataObj.getCookie(LineCookie.class);
                                if (lc == null) {
                                    return;
                                }
                                Line l = lc.getLineSet().getOriginal(line);
                                l.show(Line.ShowOpenType.OPEN, Line.ShowVisibilityType.FOCUS);
                            }
                        }

                        //highlight the line
//                        StyledDocument document = ck.getDocument();
//                        if(document != null) {
//                            Element e = document.getDefaultRootElement();
//                            if (e != null && line != -1) {
//                                e = e.getElement(line);
//                                final int startOfLine = e.getStartOffset();
//                                Line myLine = NbEditorUtilities.getLine(document, startOfLine, false);
//                                updateLine(myLine);
//                            }
//                        }
                    }
                } catch (Exception exception) {
                }
            }
        });
    }
        
    private static FileObject lookupQuorumFile(String fullyQualifiedClassName) {
        FileObject[] projectFiles = compiler.getProjectFiles();
        String className = getClassName(fullyQualifiedClassName);
        if (projectFiles == null) {
            return compiler.getCurrentFileBeingExecuted();
        }
        
        if (fullyQualifiedClassName.startsWith("quorum/")) {
            // Search project files.
            for (int i = 0; i < projectFiles.length; i++) {
                FileObject file = projectFiles[i];
                if (file.getExt().equals("quorum")) {
                    CompilerFileDescriptor fd = compiler.getFileDescriptor(file);
                    CompilerClassDescriptor cd = fd.getClass(className);
                    if (cd != null && cd.getName().equals(className)) {
                         return file;
                    }
                }
            }
            
            // If we make it here, we found nothing, so check the global scope.
            Iterator<CompilerFileDescriptor> allFiles = compiler.getFileDescriptors();
            while (allFiles.hasNext()) {
                CompilerFileDescriptor fd = allFiles.next();
                CompilerClassDescriptor cd = fd.getClass(className);
                
                if (cd != null && cd.getName().equals(className)) {
                    return FileUtil.toFileObject(fd.getFile());
                }
            }  
        }
        
        // No results
        return null;
    }
    private static String getClassName(String fullyQualifiedClassName) {

        int lastSlash = fullyQualifiedClassName.lastIndexOf("/");
        
        return fullyQualifiedClassName.substring(lastSlash + 1);
    }  
}


