package org.sodbeans.tod;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import javax.swing.Action;
import org.netbeans.spi.debugger.ui.Constants;
import org.netbeans.spi.viewmodel.ModelEvent;
import org.netbeans.spi.viewmodel.ModelListener;
import org.netbeans.spi.viewmodel.Models;
import org.netbeans.spi.viewmodel.NodeActionsProvider;
import org.netbeans.spi.viewmodel.NodeModel;
import org.netbeans.spi.viewmodel.TableModel;
import org.netbeans.spi.viewmodel.TreeModel;
import org.netbeans.spi.viewmodel.UnknownTypeException;
import org.tod.api.DebugEventListener;
import org.tod.meta.MethodInfo;
import org.tod.meta.StateInfo;

public class CallStackDataStorage extends DebugEventListener
        implements TreeModel, NodeModel, TableModel, NodeActionsProvider {
    private static CallStackDataStorage instance = null;
    private ArrayList<ModelListener> listeners = new ArrayList<ModelListener>();
    public static final String CALL_STACK =
            "org/netbeans/modules/debugger/resources/"
            + "callStackView/NonCurrentFrame";
    public static final String CURRENT_CALL_STACK =
            "org/netbeans/modules/debugger/resources/"
            + "callStackView/CurrentFrame";

    protected List<MethodInfo> stack = new LinkedList<MethodInfo>();
    
    private CallStackDataStorage() {
    }
    
    public static CallStackDataStorage getInstance() {
        if (instance == null) {
            instance = new CallStackDataStorage();
        }
        
        return instance;
    }
    
    @Override
    public Object getRoot() {
        return ROOT;
    }

    @Override
    public Object[] getChildren(Object parent, int from, int to) {
        if (parent == ROOT) {
            return stack.toArray();
        }
        
        return new Object[0];
    }

    @Override
    public boolean isLeaf(Object node) throws UnknownTypeException {
        if (node == ROOT) {
            return false;
        }
        
        return true;
    }

    @Override
    public int getChildrenCount(Object node) throws UnknownTypeException {
        if (node == ROOT) {
            return stack.size();
        }
        
        return 0;
    }

    @Override
    public String getDisplayName(Object node) {
        if (node instanceof MethodInfo) {
            MethodInfo method = (MethodInfo) node;
            String name = node.toString();
            //remove the extra package name hidden to quorum users
            name = name.substring("quorum.".length());
            //remove the extra parentheses at the end of the name
            name = name.substring(0, name.length() - 2);
            if(method.getLineNumber() != -1) {
                name += ":" + method.getLineNumber();
            }
            if(stack != null && !stack.isEmpty()) {
                MethodInfo top = stack.get(0);
                if(node == top) { //it's the top of the stack.
                    return "<html><strong>" + name + "</strong></html>";
                }
            }
            return name;
        }
        
        return "";
    }

    @Override
    public String getIconBase(Object node) {
        if (node instanceof MethodInfo) {
            if(stack != null && !stack.isEmpty()) {
                MethodInfo top = stack.get(0);
                if (top == node) {
                    return CURRENT_CALL_STACK;
                } else {
                    return CALL_STACK;
                }
            }
        } else if (node == ROOT) {
            return null;
        }

        return null;
    }

    @Override
    public String getShortDescription(Object node) {
        return null;
    }

    @Override
    public Object getValueAt(Object node, String columnID) {
        if (node == ROOT) {
            return null;
        } else if (node instanceof String) {
            if (Constants.CALL_STACK_FRAME_LOCATION_COLUMN_ID.equals(columnID)) {
                return node.toString();
            } else {
                return "";
            }
        }

        return "unknown column ID";
    }

    
    @Override
    public boolean isReadOnly(Object node, String columnID) {
        return true;
    }

    @Override
    public void setValueAt(Object node, String columnID, Object value) {
    }

    @Override
    public void addModelListener(ModelListener ml) {
        listeners.add(ml);
    }

    @Override
    public void removeModelListener(ModelListener ml) {
        listeners.remove(ml);
    }

    private void update() {
        ModelEvent event = new ModelEvent.TreeChanged(this);
        for (ModelListener l : listeners) {
            l.modelChanged(event);
        }
    }

    @Override
    public void performDefaultAction(Object o) throws UnknownTypeException {
        // TODO: dont run on event dispatch
        if (o instanceof MethodInfo) {
            MethodInfo mi = (MethodInfo)o;
            QuorumDebuggerUtils.jumpToCallStackLocation(mi.getOwningClass().getFullyQualifiedName(), mi.getLineNumber());
        }
    }

    @Override
    public Action[] getActions(Object o) throws UnknownTypeException {
        Action[] actions = new Action[1];
        actions[0] = Models.createAction("Go to Source", new GoToSourceCallStack(),Models.MULTISELECTION_TYPE_EXACTLY_ONE);
        return actions;
    }
    
    @Override
    public void callStackChanged(List<MethodInfo> list) {
        stack.clear();
        stack.addAll(list);
        this.update();
    }
    
    private static class GoToSourceCallStack implements Models.ActionPerformer {
        @Override
        public boolean isEnabled(Object arg) {
            return true;
        }

        @Override
        public void perform(Object[] nodes) {
            if (nodes[0] instanceof MethodInfo) {
                MethodInfo mi = (MethodInfo)nodes[0];
                QuorumDebuggerUtils.jumpToCallStackLocation(mi.getOwningClass().getFullyQualifiedName(), mi.getLineNumber());
            }
        }
    }
}
