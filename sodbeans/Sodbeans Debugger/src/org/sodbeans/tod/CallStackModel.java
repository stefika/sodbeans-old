package org.sodbeans.tod;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import javax.swing.Action;
import org.netbeans.spi.debugger.ui.Constants;
import org.netbeans.spi.viewmodel.ModelEvent;
import org.netbeans.spi.viewmodel.ModelListener;
import org.netbeans.spi.viewmodel.NodeActionsProvider;
import org.netbeans.spi.viewmodel.NodeModel;
import org.netbeans.spi.viewmodel.TableModel;
import org.netbeans.spi.viewmodel.TreeModel;
import org.netbeans.spi.viewmodel.UnknownTypeException;
import org.tod.api.DebugEventListener;
import org.tod.meta.MethodInfo;
import org.tod.meta.StateInfo;

public class CallStackModel
        implements TreeModel, NodeModel, TableModel, NodeActionsProvider {
    private final CallStackDataStorage data;
    
    public CallStackModel() {
        this.data = CallStackDataStorage.getInstance();
    }
    
    @Override
    public Object getRoot() {
        return this.data.getRoot();
    }

    @Override
    public Object[] getChildren(Object parent, int from, int to) {
        return this.data.getChildren(parent, from, to);
    }

    @Override
    public boolean isLeaf(Object node) throws UnknownTypeException {
        return this.data.isLeaf(node);
    }

    @Override
    public int getChildrenCount(Object node) throws UnknownTypeException {
        return this.data.getChildrenCount(node);
    }

    @Override
    public String getDisplayName(Object node) {
        return this.data.getDisplayName(node);
    }

    @Override
    public String getIconBase(Object node) {
        return this.data.getIconBase(node);
    }

    @Override
    public String getShortDescription(Object node) {
        return this.data.getShortDescription(node);
    }

    @Override
    public Object getValueAt(Object node, String columnID) {
        return this.data.getValueAt(node, columnID);
    }

    
    @Override
    public boolean isReadOnly(Object node, String columnID) {
         return this.data.isReadOnly(node, columnID);
    }

    @Override
    public void setValueAt(Object node, String columnID, Object value) {
        this.data.setValueAt(node, columnID, value);
    }

    @Override
    public void addModelListener(ModelListener ml) {
        this.data.addModelListener(ml);
    }

    @Override
    public void removeModelListener(ModelListener ml) {
        this.data.removeModelListener(ml);
    }

    @Override
    public void performDefaultAction(Object o) throws UnknownTypeException {
        this.data.performDefaultAction(o);
    }

    @Override
    public Action[] getActions(Object o) throws UnknownTypeException {
        return this.data.getActions(o);
    }
}
