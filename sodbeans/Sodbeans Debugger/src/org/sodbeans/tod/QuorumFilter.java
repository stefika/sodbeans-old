/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.sodbeans.tod;

import org.tod.api.Filter;
import org.tod.meta.ClassInfo;
import org.tod.meta.MethodInfo;

/**
 *
 * @author jeff
 */
public class QuorumFilter extends Filter {

    @Override
    public boolean acceptVariableName(String variableName) {
        return variableName.matches("[a-zA-Z][a-zA-Z0-9_]*");
    }

    @Override
    public boolean acceptMethod(MethodInfo method) {
        // First, is this a class we care about?
        ClassInfo owningClass = method.getOwningClass();
        if (owningClass.getFullyQualifiedName().startsWith("java") && !method.getName().equals("println")) {
            return false;
        }
        else if (method.getName().equals("toString") &&
                method.getOwningClass().getFullyQualifiedName().startsWith("java/lang")) {
            return false;
        } else if (method.getOwningClass().getFullyQualifiedName().startsWith("quorum/Libraries/Language/Types")) {
            return false;
        }
        
        return true;
    }
    
}
