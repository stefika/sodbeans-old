/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.sodbeans.tod.xml;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.openide.util.Exceptions;
import org.tod.meta.ClassInfo;
import org.tod.meta.sourcecode.Scope;
import org.tod.meta.sourcecode.SourceInfo;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 *
 * @author jeff
 */
public class QuorumXMLParser extends DefaultHandler {
    /**
     * Constants for tag names.
     */
    private static final String ASSIGNMENT = "assignment";
    private static final String ACTION = "action";
    private static final String CLASS = "class";
    private static final String DEBUG = "debug";
    private static final String ELSE = "else";
    private static final String ELSEIF = "elseif";
    private static final String IF = "if";
    private static final String NAME = "name";
    private static final String PRINT = "print";
    private static final String LOOP = "loop";
    private static final String RETURN = "return";
    private static QuorumXMLParser instance = null;
    private String tmpValue = "";
    
    /**
     * Constants for element names.
     */
    private static final String END = "end";
    private static final String LINE = "line";
    private static final String VARIABLE = "variable";
    
    /**
     * The class information we are constructing.
     */
    private ClassInfo classInfo = null;
    
    private QuorumXMLParser() {}
    
    public static QuorumXMLParser getInstance() {
        if (instance == null) {
            instance = new QuorumXMLParser();
        }
        
        return instance;
    }
    
    /**
     * Parses the class information XML provided by the Quorum compiler.
     * 
     * @param classIndex
     * @return 
     */
    public ClassInfo parse(String classIndex) {
        SAXParserFactory factory = SAXParserFactory.newInstance();
        try {
            SAXParser parser = factory.newSAXParser();
            InputStream rawXML = new ByteArrayInputStream(classIndex.getBytes());
            parser.parse(rawXML, this);
        } catch (ParserConfigurationException ex) {
            Exceptions.printStackTrace(ex);
        } catch (SAXException ex) {
            Exceptions.printStackTrace(ex);
        } catch (IOException ex) {
            Exceptions.printStackTrace(ex);
        }
        
        return this.classInfo;
    }
    
    @Override
    public void startElement(String s, String s1, String name, Attributes attributes) throws SAXException {
        //System.out.println("start el: " + name);
        
        if (name.equalsIgnoreCase(CLASS)) {
            this.classInfo = new ClassInfo();
        } else if (name.equalsIgnoreCase(ACTION)) {
            action(attributes);
        } else if (name.equalsIgnoreCase(NAME)) {
            
        } else if (name.equalsIgnoreCase(ASSIGNMENT)) {
            
        } else if (name.equalsIgnoreCase(IF)) {
            conditional(Scope.IF, attributes);
        } else if (name.equalsIgnoreCase(ELSEIF)) {
            conditional(Scope.ELSEIF, attributes);
        } else if (name.equalsIgnoreCase(ELSE)) {
            conditional(Scope.ELSE, attributes);
        } else if (name.equalsIgnoreCase(PRINT)) {
            
        } else if (name.equalsIgnoreCase(LOOP)){
            conditional(Scope.REPEAT, attributes);
        } else if (name.equalsIgnoreCase(RETURN)) {
            returnTag(attributes);
        }
    }
    
    @Override
    public void endElement(String s, String s1, String name) throws SAXException {
        //System.out.println("end el: " + name);
        
        if (name.equalsIgnoreCase(CLASS)) {
            
        } else if (name.equalsIgnoreCase(NAME)) {
            this.classInfo.setFullyQualifiedName(fromStaticKey(this.tmpValue));
        } else if (name.equalsIgnoreCase(ASSIGNMENT)) {
            
        } else if (name.equalsIgnoreCase(IF)) {
            
        } else if (name.equalsIgnoreCase(ELSEIF)) {
            
        } else if (name.equalsIgnoreCase(ELSE)) {
            
        } else if (name.equalsIgnoreCase(PRINT)) {
            
        } else if (name.equalsIgnoreCase(RETURN)) {
        }
    }
    
    @Override
    public void characters(char[] ac, int i, int j) throws SAXException {
        tmpValue = new String(ac, i, j);
    }


    /**
     * Handler for return markers.
     * 
     * @param attributes 
     */
    private void action(Attributes attributes) {
        // Parse attributes
        for (int i = 0; i < attributes.getLength(); i++) {
            String name = attributes.getQName(i);
            String value = attributes.getValue(i);
            
            if (name.equalsIgnoreCase(END)) {
                this.classInfo.getSourceInfo().addActionEnd(Integer.parseInt(value));
            }
        }   
    }
    /**
     * Handler for return markers.
     * 
     * @param attributes 
     */
    private void returnTag(Attributes attributes) {
        // Parse attributes
        for (int i = 0; i < attributes.getLength(); i++) {
            String name = attributes.getQName(i);
            String value = attributes.getValue(i);
            
            if (name.equalsIgnoreCase(LINE)) {
                this.classInfo.getSourceInfo().addReturn(Integer.parseInt(value));
            }
        }   
    }
    /**
     * Handler for if, elseif and else tags
     * @param conditional
     * @param attributes 
     */
    private void conditional(Scope conditional, Attributes attributes) {
        int start = 0;
        int end = 0;
        
        // Parse attributes
        for (int i = 0; i < attributes.getLength(); i++) {
            String name = attributes.getQName(i);
            String value = attributes.getValue(i);
            
            if (name.equalsIgnoreCase(LINE)) {
                start = Integer.parseInt(value);
            } else if (name.equalsIgnoreCase(END)) {
                end = Integer.parseInt(value);
            }
        }
        
        SourceInfo sourceInfo = this.classInfo.getSourceInfo();
        sourceInfo.addScope(conditional, start, end);
    }

    private String fromStaticKey(String name) {
        String newName;
        if (name.startsWith(".")) {
            newName = "quorum" + name;
        } else {
            newName = "quorum/" + name;
        }
        return newName.replace(".", "/");
    }

}
