/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.sodbeans.tod.xml;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.tod.meta.ClassInformationProvider;
import org.tod.meta.ClassInfo;
import org.xml.sax.SAXException;

/**
 *
 * @author jeff
 */
public class QuorumClassInformationProvider extends ClassInformationProvider {
    org.sodbeans.compiler.api.Compiler compiler =
                Lookup.getDefault().lookup(org.sodbeans.compiler.api.Compiler.class);
    
    @Override
    public ClassInfo getClassInfo(String className) {
        String staticKey = getStaticKey(className);
        String classIndex = compiler.getClassIndex(staticKey);
        
        if (classIndex != null) {
            return QuorumXMLParser.getInstance().parse(classIndex);
        } else {
            return super.getClassInfo(className);
        }
    }
    
    @Override
    public void reset() {
        // TODO: clear cache
    }

    /**
     * Convert a JNI-style class name to a Quorum static key.
     * @param className the class name, e.g. "quorum/Libraries/Language/Error"
     * @return 
     */
    private String getStaticKey(String className) {
        if (!className.startsWith("quorum")) {
            return className;
        }
        
        String staticKey = className.substring(7); // trim off "quorum/"
        String newKey = staticKey.replace("/", ".");
        
        if (!newKey.contains(".")) {
            return "." + newKey;
        }
        
        return newKey;
    }
}
