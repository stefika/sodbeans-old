/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.sodbeans.tod;

import java.util.ArrayList;
import org.netbeans.spi.viewmodel.ModelEvent;
import org.netbeans.spi.viewmodel.ModelListener;
import org.netbeans.spi.viewmodel.NodeModel;
import org.netbeans.spi.viewmodel.TableModel;
import org.netbeans.spi.viewmodel.TreeModel;
import org.netbeans.spi.viewmodel.UnknownTypeException;

/**
 *
 * @author jeff
 */
public class TODVariablesModel implements TreeModel, NodeModel, TableModel {
    private TODDataStorage data = TODDataStorage.getInstance();
    
    @Override
    public Object getRoot() {
        return data.getRoot();
    }

    @Override
    public Object[] getChildren(Object o, int i, int i1) throws UnknownTypeException {
        return data.getChildren(o, i, i1);
    }

    @Override
    public boolean isLeaf(Object o) throws UnknownTypeException {
        return data.isLeaf(o);
    }

    @Override
    public int getChildrenCount(Object o) throws UnknownTypeException {
        return data.getChildrenCount(o);
    }

    @Override
    public void addModelListener(ModelListener ml) {
        data.addModelListener(ml);
    }

    @Override
    public void removeModelListener(ModelListener ml) {
        data.removeModelListener(ml);
    }

    @Override
    public String getDisplayName(Object o) throws UnknownTypeException {
        return data.getDisplayName(o);
    }

    @Override
    public String getIconBase(Object o) throws UnknownTypeException {
        return data.getIconBase(o);
    }

    @Override
    public String getShortDescription(Object o) throws UnknownTypeException {
        return data.getShortDescription(o);
    }

    @Override
    public Object getValueAt(Object o, String string) throws UnknownTypeException {
        return data.getValueAt(o, string);
    }

    @Override
    public boolean isReadOnly(Object o, String string) throws UnknownTypeException {
        return data.isReadOnly(o, string);
    }

    @Override
    public void setValueAt(Object o, String string, Object o1) throws UnknownTypeException {
        data.setValueAt(o, string, o1);
    }
}
