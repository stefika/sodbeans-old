/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sodbeans.io;

/**
 *
 * @author Melissa Stefik
 */
public interface CommandLineListener {
    /**
     * perform an action in the command line.
     * @param event
     */
    public void actionPerformed(CommandLineEvent event);
}
