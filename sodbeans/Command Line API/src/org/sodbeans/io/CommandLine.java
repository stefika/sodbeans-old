/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sodbeans.io;

import org.openide.nodes.Node;

/**
 *
 * @author Andreas Stefik
 */
public interface CommandLine  {

    /**
     * Posts a string to the Sodbeans command line
     * @param post
     */
    public void post(String post);
    
    /**
     * This method can be called to post a value. If isError is true, 
     * an error indicator is used.
     * 
     * @param post
     * @param isError 
     */
    public void post(String post, boolean isError);

    /**
     * Removes the previous post from the command line.
     */
    public void unpost();

    /**
     * Polls the command line for input from the user.
     * 
     * @return
     */
    public String getInput(String post);

    /**
     * This methods sets whether or not the speech component of this interface
     * will block on the call. This method is useful for forcing the system
     * to read input and wait, especially when programs are running, not
     * being debugged.
     * 
     * @param block
     */
    public void setSpeechBlocking(boolean block);

    /**
     * Returns a node representing the current output on the system.
     * 
     * @return
     */
    public Node getOutputNode();

    /**
     * Removes all previous post to the command line.
     */
    public void clear();

    /**
     * Add listener to the command line.
     * @param listener
     */
    public void add(CommandLineListener listener);

    /**
     * remove a specific listener from the command line.
     * @param listener
     */
    public void remove(CommandLineListener listener);
}
