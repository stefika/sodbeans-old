/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sodbeans.io.impl;

import java.awt.Dialog.ModalityType;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Stack;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.nodes.Sheet;
import org.sodbeans.io.CommandLine;
import org.sodbeans.io.CommandLineEvent;
import org.sodbeans.io.CommandLineListener;
import org.sodbeans.phonemic.SpeechPriority;
import org.sodbeans.phonemic.TextToSpeechFactory;
import org.sodbeans.phonemic.tts.TextToSpeech;
import org.sodbeans.tts.options.api.TextToSpeechOptions;

/**
 *
 * @author Andreas Stefik
 */
public class CommandLineImpl implements CommandLine{
    private TextToSpeech speech = TextToSpeechFactory.getDefaultTextToSpeech();
    private Stack<CommandLineDescriptor> commandLineNodes = new Stack<CommandLineDescriptor>();
    private CommandLineNode childRoot = new CommandLineNode();
    private CommandLineRootNode root = new CommandLineRootNode(childRoot);

    /**
     * Stores the events from the command line.
     */
    private ArrayList<CommandLineListener> listeners = new ArrayList<CommandLineListener>();

    private CommandLineEvent event = new CommandLineEvent();
    private boolean speechBlock = false;

    @Override
    public void post(String post) {
        CommandLineDescriptor clDescriptor = new CommandLineDescriptor();
        clDescriptor.setOutput(post);
        commandLineNodes.push(clDescriptor);
        this.throwEventToListeners(event);
    }
    
    @Override
    public void post(String post, boolean isError) {
        CommandLineDescriptor clDescriptor = new CommandLineDescriptor();
        clDescriptor.setError(isError);
        clDescriptor.setOutput(post);
        commandLineNodes.push(clDescriptor);
        this.throwEventToListeners(event);
    }

    @Override
    public void unpost() {
        commandLineNodes.pop();
        this.throwEventToListeners(event);
    }

    @Override
    public String getInput(String post) {
        if (TextToSpeechOptions.isScreenReading()) {
            if(speechBlock) {
                speech.speakBlocking(post);
            }
            else {
                speech.speak(post, SpeechPriority.HIGH);
            }
        }
        Object answer = "";
        JOptionPane optionPane = new JOptionPane();
        optionPane.setOptionType(JOptionPane.OK_CANCEL_OPTION);
        optionPane.setMessageType(JOptionPane.QUESTION_MESSAGE);
        optionPane.setWantsInput(true);
        optionPane.setMessage(post);

        //roll our own JDialog and make it Document Modal, so
        //you can still stop the debugger.
        JDialog dialog = optionPane.createDialog("Input Dialog");
        dialog.setModalityType(ModalityType.DOCUMENT_MODAL);
        dialog.setVisible(true);

        answer = optionPane.getInputValue();
        if (answer == null) {
            return "";
        } else {
            return (String) answer;
        }
    }

    @Override
    public Node getOutputNode() {
        childRoot.setcNodes(this.commandLineNodes);
        root.setDisplayName("Output Console");
        return root;
    }

    @Override
    public void clear() {
        commandLineNodes.clear();
        childRoot.setcNodes(this.commandLineNodes);
    }

    @Override
    public void add(CommandLineListener listener) {
        listeners.add(listener);
    }

    @Override
    public void remove(CommandLineListener listener) {
        if(listeners.contains(listener))
            listeners.remove(listener);
    }

    private void throwEventToListeners(CommandLineEvent event) {
        Iterator<CommandLineListener> it = this.listeners.iterator();
        while(it.hasNext()) {
            CommandLineListener listener = it.next();
            listener.actionPerformed(event);
        }
    }

    public void setSpeechBlocking(boolean block) {
        this.speechBlock = block;
    }

    public class CommandLineRootNode extends AbstractNode{

        public CommandLineRootNode(Children children) {
            super(children);
        }

        @Override
        protected Sheet createSheet(){
            Sheet sheet = Sheet.createDefault();
            return sheet;
        }


    }

    public class CommandLineNode extends Children.Keys<CommandLineDescriptor>{
        private Stack<CommandLineDescriptor> cNodes = new Stack<CommandLineDescriptor>();

        @Override
        protected Node[] createNodes(CommandLineDescriptor descriptor) {
            return new Node[]{descriptor.getRoot()};
        }


        /**
         * @return the cNodes
         */
        public Stack<CommandLineDescriptor> getcNodes() {
            return cNodes;
        }

        /**
         * @param cNodes the cNodes to set
         */
        public void setcNodes(Stack<CommandLineDescriptor> cNodes) {
            this.cNodes = cNodes;
            if(!cNodes.isEmpty()){
                setKeys(cNodes);
            }
            else{
                setKeys(new Stack<CommandLineDescriptor>());


            }
        }

    }


}
