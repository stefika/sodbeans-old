/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sodbeans.io.impl;

import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;

/**
 * This class represents the graphical component of the output. 
 * 
 * @author Melissa Stefik
 */
public class CommandLineDescriptor  {
    private boolean isError = false;
    private String output;

    /**
     * @return the output
     */
    public String getOutput() {
        return output;
    }

    /**
     * @param output the output to set
     */
    public void setOutput(String output) {
        this.output = output;
    }

    
    /**
     * Get the constructed node
     * @return
     */
    public Node getRoot(){
        AbstractNode node = new AbstractNode(Children.LEAF);
        if(!isError) {
            node.setIconBaseWithExtension("org/sodbeans/resources/consoleoutput.png");
        } else {
            node.setIconBaseWithExtension("org/sodbeans/resources/errorConsole.png");
        }
        node.setDisplayName(this.getOutput());
        return node;
    }

    /**
     * @return the isError
     */
    public boolean isError() {
        return isError;
    }

    /**
     * @param isError the isError to set
     */
    public void setError(boolean isError) {
        this.isError = isError;
    }

}
