/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.sodbeans.main;

import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import org.openide.modules.ModuleInstall;
import org.openide.windows.WindowManager;
import org.sodbeans.tts.options.api.TextToSpeechOptions;
import org.sodbeans.login.authentication.AuthService;

/**
 * These version numbers and release flags must be set at each release.
 * 
 * @author Melissa Stefik
 */
public class Installer extends ModuleInstall implements Runnable{
    public static final String SODBEANS_VERSION_FLAG = "SODBEANS_VERSION";
    public static final String SODBEANS_VERSION = "4.6.0";
    public static final String QUORUM_VERSION = "2.2";
    public static final String SODBEANS_IS_RELEASE = "true";
    public static final String UI_LOGGER_NAME = "org.netbeans.ui.sodbeansVersion";
    
    @Override
    public void restored() {
        WindowManager manager = WindowManager.getDefault();
        manager.invokeWhenUIReady(this);
    }

    @Override
    public void run() {
        String usrName = AuthService.getInstance().getUsername();
        if(usrName == null){
            usrName = "anonymous";
        }
        
        LogRecord record = new LogRecord(Level.INFO, SODBEANS_VERSION_FLAG);
        //add the SODBEANS_IS_RELEASE flag in. 
        //It should be false by default for us, but true when in release mode.
        record.setParameters(new Object[]{SODBEANS_VERSION, QUORUM_VERSION, SODBEANS_IS_RELEASE,
        TextToSpeechOptions.isScreenReading(), usrName});
        record.setLoggerName(UI_LOGGER_NAME);
        Logger.getLogger(UI_LOGGER_NAME).log(record);
    }
}
