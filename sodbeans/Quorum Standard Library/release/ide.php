<div class="runCode">
    <h2>Try Quorum! <span class="subtitle">Enter some Quorum code below and press "Run" to execute it</span></h2>
    <div class="controller">
        <div class="btn-group">
                <a id = "dropdown-button"class="btn dropdown-toggle" data-toggle="dropdown" href="#">Hello, World! <span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li class="code-example" data-toggle="dropdown" href="#"> Hello, World! </li>
                        <li class="code-example" data-toggle="dropdown" href="#"> Conditionals </li>
                        <li class="code-example" data-toggle="dropdown" href="#"> Loops </li>
                        <li class="code-example" data-toggle="dropdown" href="#"> Actions </li>
                        <li class="code-example" data-toggle="dropdown" href="#"> Classes </li>
                </ul>
        </div>
        <div id="run" class="btn-group">
                <a class="btn btn-success" href="#">Run</a>
        </div>
    </div>
    <div class="input">
        <textarea class="ide inputArea">output "Hello, World!"</textarea>
    </div>
    <div class="output">
        <pre class="outputArea"></pre>
    </div>
    <div class="disclaimer-holder">
        <span class="disclaimer">Not all Quorum code works on the web. For example, please download Quorum to use sound or music.</span>
    </div>
</div>