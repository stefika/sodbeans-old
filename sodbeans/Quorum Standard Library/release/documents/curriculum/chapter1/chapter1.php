<?php include("../../../static/templates/pageheader.template.php"); ?> <?php include("../../../static/templates/contentwrapperheader.template.php"); ?>
<script type="text/javascript">
    document.title = 'Chapter 1';
</script>

<h1>Chapter 1: A Quick Tour of Sodbeans and Quorum</h1>
<h2>In Class Lab Assignments</h2>
<ul>
<li><a href="/documents/curriculum/chapter1/lab1_1.php">Lab Manual 1.1</a></li>
<ul>
<li>Become familiar with the Sodbeans IDE.</li>
<li>Learning how to use the tutorial and navigation systems.</li>
<li>Open a new Quorum project.</li>
</ul>
</ul>
<ul>
<li><a href="/documents/curriculum/chapter1/lab1_2.php">Lab Manual 1.2</a></li>
<ul>
<li>Learn how to open a sample project.</li>
<li>Become familiar with running and debugging a project.</li>
<li>See how a game of Hangman is programmed in Quorum.</li>
</ul>
</ul>
<ul>
<li><a href="/documents/curriculum/chapter1/lab1_3.php">Lab Manual 1.3</a></li>
<ul>
<li>Build and execute a Jar file.</li>
<li>Learn about the JVM and Java Bytecode.</li>
<li>Use the command line.</li>
</ul>
 <?php include("../../../static/templates/contentwrapperheader.template.php"); ?>  <?php include("../../../static/templates/pageheader.template.php"); ?>