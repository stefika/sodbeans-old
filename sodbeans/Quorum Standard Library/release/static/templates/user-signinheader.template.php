<div class="user-controls-loggedout">
	<span class="signin-signup-message">
		<a class="btn btn-primary" data-toggle="modal" data-target="#modal-login">Sign In</a>
		<em>or</em>
  		<a class="btn" data-toggle="modal" data-target="#modal-registration">Sign Up</a>
	</span>
</div>