<?php include("static/templates/pageheader.template.php"); ?>
<script type="text/javascript">
    document.title = 'Curriculum for the Quorum Programming Language';
</script>
<div class="hero-unit">
	<div class="hero-unit-container">
		<h1>Quorum Curriculum</h1>
		<p>These pages provide extra curricular material that can be 
        freely used in the classroom.</p>
	</div>
</div>

<div class="content curriculum-content">
    <h1>Quorum Curriculum</h1>
    <p>
        The focus in this material is to provide 
        extra resources for each chapter, including the following:
    </p>
    <ol>
        <li>Cheat sheets for each chapter that summarize the major syntax
            learned for those lessons</li>
        <li>A test pool of short answer and essay questions</li>
        <li>In-class Lab assignments (1 hour completion time)</li>
        <li>Small programming projects (1-2 hours completion time)</li>
        <li>Challenge programming projects (5-10 hours completion time)</li>
    </ol>
    <h2><a href="/documents/curriculum/chapter1/chapter1.php">Chapter 1: A Quick Tour of Sodbeans and Quorum</a></h2>
    <h2><a href="/documents/curriculum/chapter2/chapter2.php">Chapter 2: Variables and Types</a></h2>
    <h2><a href="/documents/curriculum/chapter3/chapter3.php">Chapter 3: Control Structures</a></h2>
    <h2><a href="/documents/curriculum/chapter4/chapter4.php">Chapter 4: Actions</a></h2>
    <h2><a href="/documents/curriculum/chapter5/chapter5.php">Chapter 5: Classes</a></h2>
    <h2><a href="/documents/curriculum/chapter6/chapter6.php">Chapter 6: Inheritance</a></h2>
    <h2><a href="/documents/curriculum/chapter7/chapter7.php">Chapter 7: Errors and Data</a></h2>
    <h2><a href="/documents/curriculum/chapter8/chapter8.php">Chapter 8: Input and Output</a></h2>
</div>
</div>
<?php include("static/templates/pagefooter.template.php"); ?>
