/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sodbeans.tts.options.api;

import java.net.MalformedURLException;
import java.net.URL;
import org.openide.filesystems.MultiFileSystem;
import org.openide.filesystems.XMLFileSystem;
import org.openide.util.Exceptions;
import org.xml.sax.SAXException;

/**
 *
 * @author Andreas Stefik
 */
public class OptionsFileSystem extends MultiFileSystem{
    private XMLFileSystem omniscientDebuggerFileSystem;

    public OptionsFileSystem() {
        try {
            //org.sodbeans.tts.options
            URL url = new URL("nbresloc:/org/sodbeans/tts/options/OmniscientHiding.xml");
            omniscientDebuggerFileSystem = new XMLFileSystem(url);
        } catch (MalformedURLException ex) {
            Exceptions.printStackTrace(ex);
        }
        catch (SAXException ex) {
            Exceptions.printStackTrace(ex);
        }
    }

    public void setOmniscientDebuggerButtonVisibility(boolean on) {
        if(on) {
            setDelegates(omniscientDebuggerFileSystem);
        }
        else {
            setDelegates();
        }
    }
}
