/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sodbeans.tts.options.api;

import java.util.Collection;
import java.util.Iterator;
import org.openide.filesystems.FileSystem;
import org.openide.util.Lookup;
import org.openide.util.NbPreferences;
import org.sodbeans.tts.options.TextToSpeechPanel;

/**
 *
 * @author Andreas Stefik
 */
public class TextToSpeechOptions {
    
    /**
     * Determines whether screen reading is active on the system. If this
     * property is off, then menus, navigation, and the editor, will
     * not read.
     */
    public static final String SCREEN_READING = "screenReading";
    
    /**
     * Determines whether editors should talk as the user manipulates it
     * using the keyboard. This is of type boolean.
     */
    public static final String SPEAK_WHILE_TYPING = "speakWhileTyping";

    /**
     * Returns the currently selected voice on the system. If
     * this value returns the empty string, then its value is set to
     * whatever voice the system defaults to.
     */
    public static final String SELECTED_VOICE = "selectedVoice";

    /**
     * A flag to indicate whethere speech is enabled at all in the interface.
     */
    public static final String SPEECH_ENABLED = "speechEnabled";

    /**
     * Stores the current speech volume. (0.0 to 1.0).
     */
    public static final String SPEECH_VOLUME = "speechVolume";
    
    /**
     * Stores the current speech speed. (0.0 to 1.0).
     */
    public static final String SPEECH_SPEED = "speechSpeed";
    
    /**
     * Stores the current speech pitch. (0.0 to 1.0)
     */
    public static final String SPEECH_PITCH = "speechPitch";
    
    /**
     * Stores the current speech engine.
     */
    public static final String SPEECH_ENGINE = "speechEngine";
    
    private static OptionsFileSystem optionsFileSystem;
    
    /** Gathers the appropriate FileSystem to be merged for dynamic buttons.
     * 
     */
    private static void updateFileSystem() {
        if(optionsFileSystem != null) { //we've already grabbed it from lookup
            return;
        }
        
        Collection<? extends FileSystem> collection = Lookup.getDefault().lookupAll(FileSystem.class);
        Iterator<? extends FileSystem> it = collection.iterator();
        boolean found = false;
        while(it.hasNext() && !found) {
            FileSystem fs = it.next();
            if(fs instanceof OptionsFileSystem) {
                optionsFileSystem = (OptionsFileSystem) fs;
                found = true;
            }
        }
    }
    
    public static final String OMNISCIENT_DEBUGGER_FLAG = "OmniscientDebugger";

    /**
     * Returns true if the system currently uses an omniscient debugger
     * for the sodbeans compiler.
     * @return
     */
    public static boolean isOmniscientDebugger() {
        return NbPreferences.forModule(TextToSpeechPanel.class).getBoolean(OMNISCIENT_DEBUGGER_FLAG, true);
    }
    
    /**
     * Turn the Omniscient Debugger controls on or off.
     * @param on 
     */
    public static void setOmniscientDebugger(boolean on) {
        NbPreferences.forModule(TextToSpeechPanel.class).putBoolean(OMNISCIENT_DEBUGGER_FLAG, on);
        optionsFileSystem.setOmniscientDebuggerButtonVisibility(on);
    }
//
//    /**
//     * Returns true if the local variables window contains variable history
//     * information for every variable on the system.
//     * 
//     * @return
//     */
//    public static boolean isHistoryAvailable() {
//        return NbPreferences.forModule(SodbeansPanel.class).getBoolean(SodbeansPanel.DEBUGGER_HISTORY_FLAG, true);
//    }
//
//    /**
//     * Determines whether the user has has asked for machine time stamps to be
//     * visible in the watch/local variables windows.
//     * 
//     * @return
//     */
//    public static boolean isMachineTimeStampVisibile() {
//        return NbPreferences.forModule(SodbeansPanel.class).getBoolean(SodbeansPanel.MACHINE_TIME_STAMP_FLAG, false);
//    }
//
//    /**
//     * Determines whether the user has has asked for time stamps to be
//     * visible in the watch/local variables windows.
//     *
//     * @return
//     */
//    public static boolean isTimeStampVisibile() {
//        return NbPreferences.forModule(SodbeansPanel.class).getBoolean(SodbeansPanel.TIME_STAMP_FLAG, true);
//    }


    /**
     * Uses the preferences set in the options panel to appropriately update
     * user interface preferences. For example, if isOmniscientDebugger
     * returns true, this method will merge the omniscient debugger buttons
     * back into the user interface.
     */
    public static void updateInterfacePreferences() {
        updateFileSystem();
        optionsFileSystem.setOmniscientDebuggerButtonVisibility(isOmniscientDebugger());

        if(javax.swing.SwingUtilities.isEventDispatchThread()) {
            //updateLocals();
        }
        else {
            javax.swing.SwingUtilities.invokeLater(new Runnable() {

                public void run() {
                    //updateLocals();
                }
            } );
        }
        
    }
    /**
     * Determines whether editors should talk as the user manipulates it
     * using the keyboard. This is of type boolean.
     *
     * @return
     */
    public static boolean getSpeakWhileTyping() {
        return NbPreferences.forModule(TextToSpeechPanel.class).getBoolean(SPEAK_WHILE_TYPING, true);
    }

    /**
     * Returns the currently selected voice on the system. If
     * this value returns the empty string, then its value is set to
     * whatever voice the system defaults to.
     * 
     * @return
     */
    public static String getSelectedVoice() {
        return NbPreferences.forModule(TextToSpeechPanel.class).get(SELECTED_VOICE, "");
    }

    /**
     * Returns whether or not the system has text-to-speech features enabled
     * globally.
     * 
     * @return
     */
    public static boolean isSpeechEnabled() {
        return NbPreferences.forModule(TextToSpeechPanel.class).getBoolean(SPEECH_ENABLED, true);
    }
    
    /**
     * Determines whether the Sodbeans screen reader is currently active on the
     * system.
     * 
     * @return 
     */
    public static boolean isScreenReading() {
        return NbPreferences.forModule(TextToSpeechPanel.class).getBoolean(SCREEN_READING, true);
    }
    
    /**
     * Sets the Sodbeans screen reader is being either active or inactive.
     * 
     * @param read if true, the screen will be read aloud.
     */
    public static void setScreenReading(boolean read) {
        NbPreferences.forModule(TextToSpeechPanel.class).putBoolean(SCREEN_READING, read);
    }
    
    public static int getSpeechVolume() {
        return NbPreferences.forModule(TextToSpeechPanel.class).getInt(SPEECH_VOLUME, 100);
    }
    
    public static int getSpeechSpeed() {
        return NbPreferences.forModule(TextToSpeechPanel.class).getInt(SPEECH_SPEED, 50);
    }

    public static int getSpeechPitch() {
        return NbPreferences.forModule(TextToSpeechPanel.class).getInt(SPEECH_PITCH, 50);
    }

    /**
     * Returns the speech engine currently set.
     * @return the speech engine
     */
    public static String getSpeechEngine() {
        return NbPreferences.forModule(TextToSpeechPanel.class).get(SPEECH_ENGINE, "");
    }
    
    public static void setSpeechEngine(String engine) {
        NbPreferences.forModule(TextToSpeechPanel.class).put(SPEECH_ENGINE, engine);
    }
    
    public static void setSelectedVoice(String voice) {
        NbPreferences.forModule(TextToSpeechPanel.class).put(SELECTED_VOICE, voice);
    }
    
    public static void setSpeechVolume(int vol) {
        NbPreferences.forModule(TextToSpeechPanel.class).putInt(SPEECH_VOLUME, vol);
    }

    public static void setSpeechSpeed(int speed) {
        NbPreferences.forModule(TextToSpeechPanel.class).putInt(SPEECH_SPEED, speed);
    }
    
    public static void setSpeechPitch(int pitch) {
        NbPreferences.forModule(TextToSpeechPanel.class).putInt(SPEECH_PITCH, pitch);
    }
}
