<?xml version = "1.0"?>
<TUTORIAL>
    <INSTRUCTION>
        <SYSTEM>Mac</SYSTEM>
        <TEXT>In this tutorial, you will learn about classes in the Quorum programming language. To return to the Tutorial Remote during the tutorial, press Command + Alt + T. To get help during the tutorial, press Function + F2.
        </TEXT>
    </INSTRUCTION>
    <INSTRUCTION>
        <SYSTEM>Win</SYSTEM>
        <TEXT>In this tutorial, you will learn about classes in the Quorum programming language. To return to the Tutorial Remote during the tutorial, press Control + Alt + T. To get help during the tutorial, press F2.
        </TEXT>
    </INSTRUCTION>
    <INSTRUCTION>
        <SYSTEM>All</SYSTEM>
        <TEXT>Before beginning the tutorial, create a new Quorum project in the NetBeansProjects folder, and give it a project name consisting of your name and the words ClassesTutorial. If you don't know how to open a project, walk through the Projects and Files tutorial.
        </TEXT>
    </INSTRUCTION>
    <INSTRUCTION>
        <SYSTEM>All</SYSTEM>
        <TEXT>Now, open the file main.quorum in your new project.
        </TEXT>
    </INSTRUCTION>
    <INSTRUCTION>
        <SYSTEM>All</SYSTEM>
        <TEXT>Then, create a new Empty Quorum File in the Source Code folder called Cat. Open this file as well.
        </TEXT>
    </INSTRUCTION>
    <INSTRUCTION>
        <SYSTEM>All</SYSTEM>
        <TEXT>In Quorum, all actions and variables are wrapped in classes. In general, a class is a set of variables and actions that model some concept and can interact with each other. For instance, if you wanted to represent a cat in a program, you could write a Cat class, which could store its name, its year of birth, its weight, and whether it is allowed to go outside or not, and whether the cat is currently inside or outside.
        </TEXT>
    </INSTRUCTION>
    <INSTRUCTION>
        <SYSTEM>All</SYSTEM>
        <TEXT>To write a class, you first type the word class followed by the class name. At the end of the class, type the word end. Inside, you write variables and actions. Usually, the class name starts with a capital letter, and variables will be written at the top of the class.
        </TEXT>
    </INSTRUCTION>
    <INSTRUCTION>
        <SYSTEM>All</SYSTEM>
        <TEXT>Now, write a Cat class. Give it three variables, one text variable for its name, named "name", one boolean variable for whether it can go outside or not, named "canGoOutside", and one boolean variable for whether it is outside or not, named "isOutside".
        </TEXT>
    </INSTRUCTION>
    <INSTRUCTION>
        <SYSTEM>All</SYSTEM>
        <TEXT>The Cat class could also have actions. For instance, you could write a SayMeow action that would say the word "meow," and you could write an action that would let it outside, if it was allowed to go outside.
        </TEXT>
    </INSTRUCTION>
    <INSTRUCTION>
        <SYSTEM>All</SYSTEM>
        <TEXT>Now, write these actions for the Cat class. The SayMeow action needs to say "meow," and the LetOutside action needs to set your isOutside variable to true.
        </TEXT>
    </INSTRUCTION>
    <INSTRUCTION>
        <SYSTEM>All</SYSTEM>
        <TEXT>In classes, you often want to protect your data from other classes that are using the class. To do this, you can set the accessibility of your variables and actions, using the keywords public and private. Private variables and actions can only be used by actions inside the class. Public variables and actions can also be used by actions outside of the class.
        </TEXT>
    </INSTRUCTION>
    <INSTRUCTION>
        <SYSTEM>All</SYSTEM>
        <TEXT>To set the accessibility of a variable, type the word public or private before the data type. To set the accessibility of an action, type the word public or private before the word action.
        </TEXT>
    </INSTRUCTION>
    <INSTRUCTION>
        <SYSTEM>All</SYSTEM>
        <TEXT>Now, in the Cat class, set the accessibility of the variables to private and the accessibility of the actions to public.
        </TEXT>
    </INSTRUCTION>
    <INSTRUCTION>
        <SYSTEM>All</SYSTEM>
        <TEXT>By default, variables are set to private and actions are set to public. However, it is useful to specify this for all variables and actions. Also, you will not always want to use the default values for accessibility.
        </TEXT>
    </INSTRUCTION>
    <INSTRUCTION>
        <SYSTEM>All</SYSTEM>
        <TEXT>Because the variables are private, you will not be able to change their values outside of the class or access the values of the variables. To do these things, you will need to create mutator and accessor actions.
        </TEXT>
    </INSTRUCTION>
    <INSTRUCTION>
        <SYSTEM>All</SYSTEM>
        <TEXT>Mutator actions set the value of a variable. If there is a restricted set of values, the mutator action can verify that the value stays within that set. For instance, if you had an age variable, the mutator could verify that the value you want to give it is not negative.
        </TEXT>
    </INSTRUCTION>
    <INSTRUCTION>
        <SYSTEM>All</SYSTEM>
        <TEXT>Mutator actions are usually named "Set" plus the name of the variable. It will take a parameter that is the type of the variable being set. For instance, the mutator action for the name variable will be called SetName and take a text parameter, perhaps named n.
        </TEXT>
    </INSTRUCTION>
    <INSTRUCTION>
        <SYSTEM>All</SYSTEM>
        <TEXT>Inside the mutator action, you set the class variable equal to the value of the parameter. In the SetName action, you would write, name equals n.
        </TEXT>
    </INSTRUCTION>
    <INSTRUCTION>
        <SYSTEM>All</SYSTEM>
        <TEXT>Write public mutator actions for the three variables in your class. Each action should take one parameter and set the class variable equal to the value of the parameter.
        </TEXT>
    </INSTRUCTION>
    <INSTRUCTION>
        <SYSTEM>All</SYSTEM>
        <TEXT>Accessor actions return the value of a variable in the class. They are usually named "Get" plus the name of the variable. They do not take a parameter. For instance, the accessor for the name variable will be called GetName and will return text.
        </TEXT>
    </INSTRUCTION>
    <INSTRUCTION>
        <SYSTEM>All</SYSTEM>
        <TEXT>Inside the accessor action, you return the value of the class variable. In the GetName action, you would write, return name.
        </TEXT>
    </INSTRUCTION>
    <INSTRUCTION>
        <SYSTEM>All</SYSTEM>
        <TEXT>Write public accessor actions for the three variables in your class. Each action should have a return type that is the type of the variable being returned and return the value of the variable.
        </TEXT>
    </INSTRUCTION>
    <INSTRUCTION>
        <SYSTEM>All</SYSTEM>
        <TEXT>To use a class in another class--for instance, the Main class--you create an instance of the class just as you would declare a variable. Type the name of the class and then the name of the object, just like a variable name.
        </TEXT>
    </INSTRUCTION>
    <INSTRUCTION>
        <SYSTEM>All</SYSTEM>
        <TEXT>Go to the Main class and in the Main action, create a Cat object named pet. On the first line after action Main, type the word Cat followed by the object name, pet.
        </TEXT>
    </INSTRUCTION>
    <INSTRUCTION>
        <SYSTEM>All</SYSTEM>
        <TEXT>To use actions of the Cat object, type the name of the variable, then a colon, then the action call for the action. For instance, to make the Cat meow, type pet, colon, SayMeow, parentheses.
        </TEXT>
    </INSTRUCTION>
    <INSTRUCTION>
        <SYSTEM>All</SYSTEM>
        <TEXT>Now, using the mutator actions, set the name of pet to "Felix" and canGoOutside to true. Then, let pet outside using the LetOutside action and make pet meow using the SayMeow action.
        </TEXT>
    </INSTRUCTION>
    <INSTRUCTION>
        <SYSTEM>All</SYSTEM>
        <TEXT>The last thing we will cover in this tutorial is the on create action. The on create action is used to set the value of variables and do other operations when an object is created. It starts with the words "on create" without the word "action", takes no parameters, and returns no value.
        </TEXT>
    </INSTRUCTION>
    <INSTRUCTION>
        <SYSTEM>All</SYSTEM>
        <TEXT>In the on create action, text variables should be set to an empty string, integers and numbers could be set to 0, and boolean variables can be set to false. Objects of classes should be set to undefined, using the keyword undefined.
        </TEXT>
    </INSTRUCTION>
    <INSTRUCTION>
        <SYSTEM>All</SYSTEM>
        <TEXT>Now, go back to the Cat class and create an on create action after the class variables. Remember: the on create action does not start with the word action. Inside the on create action, set the name variable to an empty string, and set isOutside and canGoOutside to false.
        </TEXT>
    </INSTRUCTION>
    <INSTRUCTION>
        <SYSTEM>All</SYSTEM>
        <TEXT>You have now learned about writing and using classes in Quorum.
        </TEXT>
    </INSTRUCTION>
</TUTORIAL>
