<?xml version = "1.0"?>
<TUTORIAL>
     <INSTRUCTION>
        <SYSTEM>Mac</SYSTEM>
        <TEXT>In this tutorial, you will learn about error handling in the Quorum programming language. To return to the Tutorial Remote during the tutorial, press Command + Alt + T. To get help during the tutorial, press Function + F2.
        </TEXT>
     </INSTRUCTION>
     <INSTRUCTION>
        <SYSTEM>Win</SYSTEM>
        <TEXT>In this tutorial, you will learn about error handling in the Quorum programming language. To return to the Tutorial Remote during the tutorial, press Control + Alt + T. To get help during the tutorial, press F2.
        </TEXT>
     </INSTRUCTION>
     <INSTRUCTION>
         <SYSTEM>All</SYSTEM>
         <TEXT>Before beginning the tutorial, create a new Quorum project in the NetBeansProjects folder, and give it a project name consisting of your name and the words ErrorsTutorial. If you don't know how to open a project, walk through the Projects and Files tutorial.
         </TEXT>
     </INSTRUCTION>
     <INSTRUCTION>
        <SYSTEM>All</SYSTEM>
        <TEXT>Now, open the file main.quorum in your new project.
        </TEXT>
     </INSTRUCTION>
     <INSTRUCTION>
        <SYSTEM>All</SYSTEM>
        <TEXT>Sometimes, programs can do things that they aren't allowed to do, they aren't able to do, or that you don't want them to do. When these things happen, they produce errors.
        </TEXT>
     </INSTRUCTION>
     <INSTRUCTION>
        <SYSTEM>All</SYSTEM>
        <TEXT>In the first two cases, the computer will detect and produce an error and will tell your program about them. While these errors will stop your program from running, if you write your program to be prepared for them, you can allow it to fail gracefully.
        </TEXT>
     </INSTRUCTION>
     <INSTRUCTION>
        <SYSTEM>All</SYSTEM>
        <TEXT>A program in which errors will be handled must include a use statement before the class declaration. The use statement starts with the word use and is followed by a library that will be used in the program. In this case, the library Libraries.Language.Errors.Error must be used.
        </TEXT>
     </INSTRUCTION>
     <INSTRUCTION>
        <SYSTEM>All</SYSTEM>
        <TEXT>In the Main class file that has been created, write the use statement before the declaration of the Main class.
        </TEXT>
     </INSTRUCTION>
     <INSTRUCTION>
        <SYSTEM>All</SYSTEM>
        <TEXT>The first part of error handling is the check statement. The check statement starts with the word "check", and you use it to wrap code that could cause errors.
        </TEXT>
     </INSTRUCTION>
     <INSTRUCTION>
        <SYSTEM>All</SYSTEM>
        <TEXT>In the program, write a check statement. Inside the check statement, declare two number variables, x and y. Set x equal to 7 and y equal to 0. Declare a third number variable, named z, and set it equal to x divided by y.
        </TEXT>
     </INSTRUCTION>
     <INSTRUCTION>
        <SYSTEM>All</SYSTEM>
        <TEXT>The next part of error handling is the detect statement. The detect statement comes directly after the check statement. The detect statement watches for an error, either any error or a specific error, and does something when it detects that the error has occurred.
        </TEXT>
     </INSTRUCTION>
     <INSTRUCTION>
        <SYSTEM>All</SYSTEM>
        <TEXT>The detect statement begins with the word detect, which is followed by a variable name for the error, then the words "of type" and the error type or types that are being detected. If there is more than one type being detected, the types are separated by the word "or". There are multiple kinds of errors, but in this example, we will check for any Error, which you can do by just checking for Error, starting with a capital E. The statement ends with the word end.
        </TEXT>
     </INSTRUCTION>
	 <INSTRUCTION>
        <SYSTEM>All</SYSTEM>
        <TEXT>The detect statement begins with the word detect, which is followed by a variable name for the error, then the words "of type" and the error type or types that are being detected. If there is more than one type being detected, the types are separated by the word "or". There are multiple kinds of errors, but in this example, we will check for any Error, which you can do by just checking for Error, starting with a capital E. The statement ends with the word end.
        </TEXT>
     </INSTRUCTION>
     <INSTRUCTION>
        <SYSTEM>All</SYSTEM>
        <TEXT>Write a detect statement after the check statement. Call the error e, and check for any Error. This should read "detect e of type Error".
        </TEXT>
     </INSTRUCTION>
     <INSTRUCTION>
        <SYSTEM>All</SYSTEM>
        <TEXT>One thing that you can do when an error occurs is to output or say the error message. Errors have an action called GetErrorMessage.
        </TEXT>
     </INSTRUCTION>
     <INSTRUCTION>
        <SYSTEM>All</SYSTEM>
        <TEXT>Now, in the detect statement, output the error message by calling the GetErrorMessage method for e.
        </TEXT>
     </INSTRUCTION>
     <INSTRUCTION>
        <SYSTEM>All</SYSTEM>
        <TEXT>Sometimes, when code is error-prone, there are things that should always execute afterward. For this, we use an always statement. The always statement begins with the word always, ends with the word end, and wraps code that will be executed after running error-prone code.
        </TEXT>
     </INSTRUCTION>
     <INSTRUCTION>
        <SYSTEM>All</SYSTEM>
        <TEXT>After the detect statement, write an always statement in which the program says, "Calculation finished." using a say statement.
        </TEXT>
     </INSTRUCTION>
     <INSTRUCTION>
        <SYSTEM>All</SYSTEM>
        <TEXT>The last thing to know about error handling is that you can create your own errors. To do this, you use an alert action.
        </TEXT>
     </INSTRUCTION>
     <INSTRUCTION>
        <SYSTEM>All</SYSTEM>
        <TEXT>There are two ways to use an alert action. The first way is to pass an error message, which is just text. For instance, if you wanted to create an error that sends an error message when dividing by zero, you could use the alert action, reading alert, left paren, double quote, Cannot divide by zero., double quote, right paren.
        </TEXT>
     </INSTRUCTION>
     <INSTRUCTION>
        <SYSTEM>All</SYSTEM>
        <TEXT>In the check statement in this program, write an alert action that uses an error message after the line that says, z equals x slash y.
        </TEXT>
     </INSTRUCTION>
     <INSTRUCTION>
        <SYSTEM>All</SYSTEM>
        <TEXT>The other way to use an alert action is to pass an Error. First, you must create an Error instance. You can then set the error message by using the SetErrorMessage action of the Error class, and the error can be passed in alert statements.
        </TEXT>
     </INSTRUCTION>
     <INSTRUCTION>
        <SYSTEM>All</SYSTEM>
        <TEXT>In the check statement, create an Error object named error, and set the error message to the message you are currently using in your alert statement, "Cannot divide by zero". Then, instead of passing the message in the call to the alert statement, pass the object, error.
        </TEXT>
     </INSTRUCTION>
     <INSTRUCTION>
        <SYSTEM>All</SYSTEM>
        <TEXT>Error handling is a useful tool that will allow your programs to experience problems without failing and to ensure that programs that experience problems are still usable.
        </TEXT>
     </INSTRUCTION>
     <INSTRUCTION>
        <SYSTEM>All</SYSTEM>
        <TEXT>You have now learned about error handling in Quorum.
        </TEXT>
     </INSTRUCTION>
</TUTORIAL>
