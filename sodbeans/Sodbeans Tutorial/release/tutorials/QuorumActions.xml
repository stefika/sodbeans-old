<?xml version = "1.0"?>
<TUTORIAL>
     <INSTRUCTION>
        <SYSTEM>Mac</SYSTEM>
        <TEXT>In this tutorial, you will learn about actions in the Quorum programming language. During the tutorial, to return to the Tutorial Remote, press Command + Alt + T. To get help during the tutorial, press Function + F2.
        </TEXT>
     </INSTRUCTION>
     <INSTRUCTION>
        <SYSTEM>Win</SYSTEM>
        <TEXT>In this tutorial, you will learn about actions in the Quorum programming language. To return to the Tutorial Remote during the tutorial, press Control + Alt + T. To get help during the tutorial, press F2.
        </TEXT>
     </INSTRUCTION>
     <INSTRUCTION>
        <SYSTEM>All</SYSTEM>
        <TEXT>Before beginning the tutorial, create a new project in the NetBeansProjects folder, and give it a project name consisting of your name and the words ActionsTutorial. If you don't know how to open a project, walk through the Projects and Files tutorial.
        </TEXT>
     </INSTRUCTION>
     <INSTRUCTION>
        <SYSTEM>All</SYSTEM>
        <TEXT>Now, open the file main.quorum in your new project.
        </TEXT>
     </INSTRUCTION>
     <INSTRUCTION>
        <SYSTEM>All</SYSTEM>
        <TEXT>Sometimes in a program, you perform the same tasks many times in different places. Instead of rewriting this code, it's convenient and good programming practice to put this in an action.
        </TEXT>
     </INSTRUCTION>
     <INSTRUCTION>
        <SYSTEM>All</SYSTEM>
        <TEXT>First of all, you may have noticed that the code in a Quorum program is in an action called "action Main". The main action holds the code where a program starts. Every program must have a main action.
        </TEXT>
     </INSTRUCTION>
     <INSTRUCTION>
        <SYSTEM>All</SYSTEM>
        <TEXT>Every action begins with the word action and a name for the action. You can name an action anything that you can name a variable. The name must start with a letter and can be followed by letters, numbers, or underscores. Every action ends with the word "end".
        </TEXT>
     </INSTRUCTION>
     <INSTRUCTION>
        <SYSTEM>All</SYSTEM>
        <TEXT>In Quorum, every action is written outside of other actions. A program can have more than one action, and most programs do, but an action has to start and end outside of other actions.
        </TEXT>
     </INSTRUCTION>
     <INSTRUCTION>
        <SYSTEM>All</SYSTEM>
        <TEXT>Now, go to the beginning of the last line in the program, the second line that says "end", and press enter. On this line, type "action SayStuff", press enter twice, and type "end". There should be one line below this that reads "end".
        </TEXT>
     </INSTRUCTION>
     <INSTRUCTION>
        <SYSTEM>All</SYSTEM>
        <TEXT>Now, on the empty line between the beginning and end of this action, have the program say the word "stuff" using a say statement. If you don't remember how to write a say statement, just type the word "say" and then type whatever you want the program to say in quotation marks.
        </TEXT>
     </INSTRUCTION>
     <INSTRUCTION>
        <SYSTEM>All</SYSTEM>
        <TEXT>This is a very simple action that will say the word "stuff."
        </TEXT>
     </INSTRUCTION>
     <INSTRUCTION>
        <SYSTEM>All</SYSTEM>
        <TEXT>To use an action, you call the action. You do this by writing an action call. To write an action call, type the name of the action followed by an empty set of parentheses.
        </TEXT>
     </INSTRUCTION>
     <INSTRUCTION>
        <SYSTEM>All</SYSTEM>
        <TEXT>Now, go to action Main and call the action you just wrote, by typing "SayStuff" and a set of empty parentheses.
        </TEXT>
     </INSTRUCTION>
     <INSTRUCTION>
        <SYSTEM>All</SYSTEM>
        <TEXT>At this point in action Main, the SayStuff action would be called, and the program would execute the instructions inside the SayStuff action.
        </TEXT>
     </INSTRUCTION>
     <INSTRUCTION>
        <SYSTEM>All</SYSTEM>
        <TEXT>Actions can also use information from the part of the program that is calling it. For instance, in the previous example, the function always says "stuff."  You could also write the action so that it says what the calling action tells it to say. To use information from the calling action, you need to use parameters.
        </TEXT>
     </INSTRUCTION>
     <INSTRUCTION>
        <SYSTEM>All</SYSTEM>
        <TEXT>Parameters are a list of variables written inside parentheses after the action name, separated by commas. Each parameter has a specified type and a name, like when declaring a variable.
        </TEXT>
     </INSTRUCTION>
     <INSTRUCTION>
        <SYSTEM>All</SYSTEM>
        <TEXT>Edit your code above so that the action SayStuff takes a text parameter with the name stuff. This will look like: action, SayStuff, left parenthesis, the word text, the word stuff, right parenthesis. Then, change the say statement so that it no longer says the text "stuff" and says whatever is stored in the parameter named stuff. You can just remove the quotation marks.
        </TEXT>
     </INSTRUCTION>
     <INSTRUCTION>
        <SYSTEM>All</SYSTEM>
        <TEXT>Now, your action call has to provide some information, so the call in the main action will no longer work. To fix it, put some text in quotation marks inside the parentheses of the action call. For instance, if you want it to say "Quorum program," it would read: SayStuff, left parenthesis, quotation marks, "Quorum program", quotation marks, right parenthesis.
        </TEXT>
     </INSTRUCTION>
     <INSTRUCTION>
        <SYSTEM>All</SYSTEM>
        <TEXT>Now, add text in the action call that the SayStuff action can use when you call it.
        </TEXT>
     </INSTRUCTION>
     <INSTRUCTION>
        <SYSTEM>All</SYSTEM>
        <TEXT>Finally, not only can actions take input but they can produce output. They do this by using a return statement.
        </TEXT>
     </INSTRUCTION>
     <INSTRUCTION>
        <SYSTEM>All</SYSTEM>
        <TEXT>Every action can only return one thing, and to determine what the action is allowed to return, it must declare its return type, or the data type of its output. Let's say we want our SayStuff action to return whatever text it speaks. We'll need to add two short pieces of code to allow this.
        </TEXT>
     </INSTRUCTION>
     <INSTRUCTION>
        <SYSTEM>All</SYSTEM>
        <TEXT>First, at the end of the first line of the action, after the parentheses, add a space and the word "returns" followed by the data type--in this case, text. The first line of the action should now read action SayStuff, left paren, text, stuff, right paren, returns text.
        </TEXT>
     </INSTRUCTION>
     <INSTRUCTION>
        <SYSTEM>All</SYSTEM>
        <TEXT>Next, at the end of the action before the line that says end, add the word "return" followed by the variable stuff. This will return the value of the variable stuff.
        </TEXT>
     </INSTRUCTION>
     <INSTRUCTION>
        <SYSTEM>All</SYSTEM>
        <TEXT>To edit our action call, we'll need to provide somewhere for the main action to store the output of the action. To do this, add a variable declaration before the action call for a text variable called output and set it equal to the action call. This line should read text output equals SayStuff, left paren, quotation marks, Quorum program, quotation marks, right paren.
        </TEXT>
     </INSTRUCTION>
     <INSTRUCTION>
        <SYSTEM>All</SYSTEM>
        <TEXT>If you ran this program, the words "Quorum program" would be spoken and the output variable in action Main would be equal to the words "Quorum program".
        </TEXT>
     </INSTRUCTION>
     <INSTRUCTION>
        <SYSTEM>All</SYSTEM>
        <TEXT>You can also use action calls like variables in your expressions. For instance, if you had two actions that returned integers, you could multiply the action calls together, and the values returned by the actions would be multiplied.
        </TEXT>
     </INSTRUCTION>
     <INSTRUCTION>
        <SYSTEM>All</SYSTEM>
        <TEXT>One final point about actions is that, unlike variables, you can have two actions with the same name. However, they need to have distinctly different parameter lists. That is, at least one parameter must be of a different type or the actions must have a different number of parameters.
        </TEXT>
     </INSTRUCTION>
     <INSTRUCTION>
        <SYSTEM>All</SYSTEM>
        <TEXT>You have now learned about writing actions in Quorum.
        </TEXT>
     </INSTRUCTION>
</TUTORIAL>
