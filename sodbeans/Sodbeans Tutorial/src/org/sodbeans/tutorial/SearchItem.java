/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sodbeans.tutorial;
import java.util.Vector;

/**
 *
 * @author user
 */
public class SearchItem {
    private int numberOfOccurences;
    private String tutorialName;
    private String filePath;

    SearchItem() { }

    /**
     * @return the numberOfOccurences
     */
    public int getNumberOfOccurences() {
        return numberOfOccurences;
    }

    /**
     * @param numberOfOccurences the numberOfOccurences to set
     */
    public void setNumberOfOccurences(int numberOfOccurences) {
        this.numberOfOccurences = numberOfOccurences;
    }

    /**
     * @return the tutorialName
     */
    public String getTutorialName() {
        return tutorialName;
    }

    /**
     * @param tutorialNames the tutorialName to set
     */
    public void setTutorialNames(String tutorialNames) {
        this.tutorialName = tutorialName;
    }
    /**
     * @return the filePaths
     */
    public String getFilePath() {
        return filePath;
    }

    /**
     * @param filePaths the filePaths to set
     */
    public void setFilePaths(String filePaths) {
        this.filePath = filePath;
    }
}
