/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * MainWindow.java
 *
 * Created on Mar 2, 2011, 10:42:27 AM
 */

package org.sodbeans.tutorial;

import org.sodbeans.phonemic.TextToSpeechFactory;
import org.sodbeans.phonemic.tts.TextToSpeech;
import org.sodbeans.phonemic.SpeechPriority;
import java.awt.event.*;
import java.awt.*;
import javax.swing.*;
import org.openide.util.NbPreferences;
import org.sodbeans.tts.options.api.TextToSpeechOptions;
import org.sodbeans.tutorial.SelectionWindow.*;
import org.sodbeans.tutorial.HotkeysRefWindowSingleton;

/**
 *
 * @author Kim Slattery
 */
public class MainWindow extends javax.swing.JDialog{
    public static final String LOAD_AT_STARTUP = "LoadAtStartup";

    /** Creates new form MainTutorialMenu */
    public MainWindow(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        buttons = new JButton[6];
        buttons[0] = hotkeysBtn;
        buttons[1] = sodbeansBtn;
        buttons[2] = hopBtn;
        buttons[3] = hslBtn;
        buttons[4] = moreHelpBtn;
        buttons[5] = closeBtn;
        instructionsString = "Main tutorials window. "
            + "To close this window and go to Sodbeans, press the escape key. "
            + "To navigate the window, use number keys 1 through 6, and the arrow keys or tab key. "
            + "To activate the current selection, press enter or the spacebar. "
            + "To find out where you are in the window, press control + shift + F5. ";

        if (System.getProperty("os.name").startsWith("Mac")){
            instructionsString += "To hear instructions for any window, press Function Key + F2. ";
        }
        else {
            instructionsString += "To hear instructions for any window, press F2. ";
        }

        // By default, the tutorial window will not display.
        showOnStartupCheckbox.setSelected(NbPreferences.forModule(MainWindow.class).getBoolean(LOAD_AT_STARTUP, false));
        
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        sodbeansBtn = new javax.swing.JButton();
        hotkeysBtn = new javax.swing.JButton();
        closeBtn = new javax.swing.JButton();
        hopBtn = new javax.swing.JButton();
        hslBtn = new javax.swing.JButton();
        showOnStartupCheckbox = new javax.swing.JCheckBox();
        moreHelpBtn = new javax.swing.JButton();
        headerPnl = new javax.swing.JPanel();
        titleLbl = new java.awt.Label();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle(org.openide.util.NbBundle.getMessage(MainWindow.class, "MainWindow.title")); // NOI18N
        setBackground(new java.awt.Color(204, 204, 204));
        setModalExclusionType(java.awt.Dialog.ModalExclusionType.TOOLKIT_EXCLUDE);
        setResizable(false);

        sodbeansBtn.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        sodbeansBtn.setText(org.openide.util.NbBundle.getMessage(MainWindow.class, "MainWindow.sodbeansBtn.text")); // NOI18N
        sodbeansBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sodbeansBtnActionPerformed(evt);
            }
        });
        sodbeansBtn.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                MainWindow.this.focusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                MainWindow.this.focusLost(evt);
            }
        });
        sodbeansBtn.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                MainWindow.this.keyPressed(evt);
            }
        });

        hotkeysBtn.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        hotkeysBtn.setText(org.openide.util.NbBundle.getMessage(MainWindow.class, "MainWindow.hotkeysBtn.text")); // NOI18N
        hotkeysBtn.setAutoscrolls(true);
        hotkeysBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                hotkeysBtnActionPerformed(evt);
            }
        });
        hotkeysBtn.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                MainWindow.this.focusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                MainWindow.this.focusLost(evt);
            }
        });
        hotkeysBtn.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                MainWindow.this.keyPressed(evt);
            }
        });

        closeBtn.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        closeBtn.setText(org.openide.util.NbBundle.getMessage(MainWindow.class, "MainWindow.closeBtn.text")); // NOI18N
        closeBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                closeBtnActionPerformed(evt);
            }
        });
        closeBtn.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                MainWindow.this.focusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                MainWindow.this.focusLost(evt);
            }
        });
        closeBtn.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                MainWindow.this.keyPressed(evt);
            }
        });

        hopBtn.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        hopBtn.setText(org.openide.util.NbBundle.getMessage(MainWindow.class, "MainWindow.hopBtn.text")); // NOI18N
        hopBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                hopBtnActionPerformed(evt);
            }
        });
        hopBtn.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                MainWindow.this.focusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                MainWindow.this.focusLost(evt);
            }
        });
        hopBtn.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                MainWindow.this.keyPressed(evt);
            }
        });

        hslBtn.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        hslBtn.setText(org.openide.util.NbBundle.getMessage(MainWindow.class, "MainWindow.hslBtn.text")); // NOI18N
        hslBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                hslBtnActionPerformed(evt);
            }
        });
        hslBtn.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                MainWindow.this.focusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                MainWindow.this.focusLost(evt);
            }
        });
        hslBtn.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                MainWindow.this.keyPressed(evt);
            }
        });

        showOnStartupCheckbox.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        showOnStartupCheckbox.setText(org.openide.util.NbBundle.getMessage(MainWindow.class, "MainWindow.showOnStartupCheckbox.text")); // NOI18N
        showOnStartupCheckbox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                showOnStartupCheckboxActionPerformed(evt);
            }
        });
        showOnStartupCheckbox.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                MainWindow.this.keyPressed(evt);
            }
        });

        moreHelpBtn.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        moreHelpBtn.setText(org.openide.util.NbBundle.getMessage(MainWindow.class, "MainWindow.moreHelpBtn.text")); // NOI18N
        moreHelpBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                moreHelpBtnActionPerformed(evt);
            }
        });
        moreHelpBtn.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                MainWindow.this.focusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                MainWindow.this.focusLost(evt);
            }
        });
        moreHelpBtn.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                MainWindow.this.keyPressed(evt);
            }
        });

        titleLbl.setAlignment(java.awt.Label.CENTER);
        titleLbl.setFont(new java.awt.Font("Tahoma", 1, 48)); // NOI18N
        titleLbl.setText(org.openide.util.NbBundle.getMessage(MainWindow.class, "MainWindow.titleLbl.text")); // NOI18N

        org.jdesktop.layout.GroupLayout headerPnlLayout = new org.jdesktop.layout.GroupLayout(headerPnl);
        headerPnl.setLayout(headerPnlLayout);
        headerPnlLayout.setHorizontalGroup(
            headerPnlLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, titleLbl, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        headerPnlLayout.setVerticalGroup(
            headerPnlLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, headerPnlLayout.createSequentialGroup()
                .addContainerGap(24, Short.MAX_VALUE)
                .add(titleLbl, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 49, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        titleLbl.getAccessibleContext().setAccessibleName(org.openide.util.NbBundle.getMessage(MainWindow.class, "MainWindow.titleLbl.AccessibleContext.accessibleName")); // NOI18N

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/sodbeans/tutorial/Resources/SodbeansLogoTransparent3.png"))); // NOI18N
        jLabel1.setText(org.openide.util.NbBundle.getMessage(MainWindow.class, "MainWindow.jLabel1.text")); // NOI18N

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .addContainerGap()
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
                        .add(jLabel1)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(headerPnl, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(191, 191, 191))
                    .add(layout.createSequentialGroup()
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(layout.createSequentialGroup()
                                .add(hotkeysBtn, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 350, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .add(18, 18, 18)
                                .add(sodbeansBtn, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 350, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                            .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING, false)
                                .add(org.jdesktop.layout.GroupLayout.LEADING, layout.createSequentialGroup()
                                    .add(moreHelpBtn, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 350, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                    .add(18, 18, 18)
                                    .add(closeBtn, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .add(org.jdesktop.layout.GroupLayout.LEADING, layout.createSequentialGroup()
                                    .add(hopBtn, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 350, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                    .add(18, 18, 18)
                                    .add(hslBtn, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 350, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                                .add(showOnStartupCheckbox)))
                        .addContainerGap(97, Short.MAX_VALUE))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .addContainerGap()
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jLabel1)
                    .add(headerPnl, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(hotkeysBtn, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 100, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(sodbeansBtn, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 100, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(18, 18, 18)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(hopBtn, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 100, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(hslBtn, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 100, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(18, 18, 18)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(moreHelpBtn, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 100, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(closeBtn, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 100, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(showOnStartupCheckbox)
                .addContainerGap())
        );

        sodbeansBtn.getAccessibleContext().setAccessibleName(org.openide.util.NbBundle.getMessage(MainWindow.class, "MainWindow.sodbeansBtn.AccessibleContext.accessibleName")); // NOI18N
        hotkeysBtn.getAccessibleContext().setAccessibleName(org.openide.util.NbBundle.getMessage(MainWindow.class, "MainWindow.hotkeysBtn.AccessibleContext.accessibleName")); // NOI18N
        closeBtn.getAccessibleContext().setAccessibleName(org.openide.util.NbBundle.getMessage(MainWindow.class, "MainWindow.closeBtn.AccessibleContext.accessibleName")); // NOI18N
        hopBtn.getAccessibleContext().setAccessibleName(org.openide.util.NbBundle.getMessage(MainWindow.class, "MainWindow.hopBtn.AccessibleContext.accessibleName")); // NOI18N
        hslBtn.getAccessibleContext().setAccessibleName(org.openide.util.NbBundle.getMessage(MainWindow.class, "MainWindow.hslBtn.AccessibleContext.accessibleName")); // NOI18N
        moreHelpBtn.getAccessibleContext().setAccessibleName(org.openide.util.NbBundle.getMessage(MainWindow.class, "MainWindow.moreHelpBtn.AccessibleContext.accessibleName")); // NOI18N

        getAccessibleContext().setAccessibleName(org.openide.util.NbBundle.getMessage(MainWindow.class, "MainWindow.AccessibleContext.accessibleName")); // NOI18N

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void sodbeansBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sodbeansBtnActionPerformed
        if (TextToSpeechOptions.isScreenReading())
            speech.speak("Opening Sodbeans tutorials window", SpeechPriority.MEDIUM);
        openTutorials(LoadMode.SODBEANS);
}//GEN-LAST:event_sodbeansBtnActionPerformed

    private void hotkeysBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_hotkeysBtnActionPerformed
        openHotkeysReferenceWindow();
}//GEN-LAST:event_hotkeysBtnActionPerformed

    private void closeBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_closeBtnActionPerformed
        leaveWindow();
}//GEN-LAST:event_closeBtnActionPerformed

    private void hopBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_hopBtnActionPerformed
        if (TextToSpeechOptions.isScreenReading())
            speech.speak("Opening Quorum tutorials window", SpeechPriority.MEDIUM);
        openTutorials(LoadMode.QUORUM);
}//GEN-LAST:event_hopBtnActionPerformed

    private void hslBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_hslBtnActionPerformed
        if (TextToSpeechOptions.isScreenReading())
            speech.speak("Opening Quorum Standard Library tutorials window", SpeechPriority.MEDIUM);
        openTutorials(LoadMode.STD_LIB);
}//GEN-LAST:event_hslBtnActionPerformed

    private void moreHelpBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_moreHelpBtnActionPerformed
        startHotkeysTutorial();
}//GEN-LAST:event_moreHelpBtnActionPerformed

    private void keyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_keyPressed
        if (evt.getKeyCode() == KeyEvent.VK_DOWN || evt.getKeyCode() == KeyEvent.VK_RIGHT)
        {
            keyDown(evt.getComponent());
        }
        else if(evt.getKeyCode() == KeyEvent.VK_UP || evt.getKeyCode() == KeyEvent.VK_LEFT)
        {
            keyUp(evt.getComponent());
        }
        else if (evt.getKeyCode() == KeyEvent.VK_1)     // corresponds with closeBtn
        {
            respeakOrFocusButton(buttons[0]);
        }
        else if (evt.getKeyCode() == KeyEvent.VK_2)     // Corresponds with hotkeysBtn
        {
            respeakOrFocusButton(buttons[1]);
        }
        else if (evt.getKeyCode() == KeyEvent.VK_3)     // Corresponds with sodbeansBtn
        {
            respeakOrFocusButton(buttons[2]);
        }
        else if (evt.getKeyCode() == KeyEvent.VK_4)     // Corresponds with hopBtn
        {
            respeakOrFocusButton(buttons[3]);
        }
        else if (evt.getKeyCode() == KeyEvent.VK_5)     // Corresponds with hslBtn for Quorum Standard Library
        {
            respeakOrFocusButton(buttons[4]);
        }
        else if (evt.getKeyCode() == KeyEvent.VK_6)     // Corresponds with moreHelpBtn
        {
            respeakOrFocusButton(buttons[5]);
        }
        else if (evt.getKeyCode() == KeyEvent.VK_F2)
        {
            if (TextToSpeechOptions.isScreenReading()) {
                speech.speak(instructionsString);
            }
        }
        else if (evt.getKeyCode() == KeyEvent.VK_ENTER)
        {
            if (showOnStartupCheckbox.hasFocus())
                showOnStartupCheckbox.doClick();
            else
            {
                for(int i = 0; i< 6; i++)
                    {
                        if (buttons[i].hasFocus())
                            buttons[i].doClick();
                    }
            }
        }
        else if (evt.getKeyCode() == KeyEvent.VK_ESCAPE)
        {
            leaveWindow();
        }
    }//GEN-LAST:event_keyPressed

    private void showOnStartupCheckboxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_showOnStartupCheckboxActionPerformed
        boolean selected = showOnStartupCheckbox.isSelected();
        NbPreferences.forModule(MainWindow.class).putBoolean(MainWindow.LOAD_AT_STARTUP, selected);
    }//GEN-LAST:event_showOnStartupCheckboxActionPerformed

    private void focusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_focusGained
        JButton focused;
        if (this.getFocusOwner().getClass() == JButton.class){
            focused = (JButton)this.getFocusOwner();
        }
        else{
            return;
        }
    }//GEN-LAST:event_focusGained

    private void focusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_focusLost
        // TODO add your handling code here:
    }//GEN-LAST:event_focusLost

    private void respeakOrFocusButton(JButton btn){
        if (btn.hasFocus()){
            if (TextToSpeechOptions.isScreenReading()) {
                speech.speak(btn.getText());
            }
        }else{
            btn.requestFocus();
        }
    }

    private void keyDown(Component c){
        /*if (!searchField.hasFocus() ||
            (searchField.hasFocus() && searchField.getCaretPosition() == searchField.getText().length())){
            c.transferFocus();
        }*/
        
    }

    private void keyUp(Component c){
        /*if (!searchField.hasFocus() || 
            (searchField.hasFocus() && searchField.getCaretPosition() == 0)){
            c.transferFocusBackward();
        }*/
    }

    private void leaveWindow(){
        if (TextToSpeechOptions.isScreenReading())
            speech.speak("Leaving tutorials window", SpeechPriority.MEDIUM);
        TutorialTopComponent.findInstance().close();
        this.dispose();
    }

    private void openTutorials(LoadMode mode){
        this.setVisible(false);
        SelectionWindowSingleton.getInstance(mode).setVisible(true);
    }

    private void openHotkeysReferenceWindow(){
        if (TextToSpeechOptions.isScreenReading())
            speech.speak("Opening Hotkeys Reference window", SpeechPriority.MEDIUM);
        this.setVisible(false);
        HotkeysRefWindowSingleton.getInstance().setVisible(true);
    }

    private void startHotkeysTutorial(){
        // Now actually starts the windows and navigation tutorial
        if (TextToSpeechOptions.isScreenReading())
            speech.speak("Starting Sodbeans Windows and Navigation Tutorial", SpeechPriority.MEDIUM);
        TutorialTopComponent comp = TutorialTopComponent.findInstance();
        comp.open();
        comp.initTutorial("SodbeansWindowsAndNav.xml", LoadMode.MAIN_WINDOW);
        this.setVisible(false);
        comp.startTutorial();
        
    }

    private void searchTutorials(){
        if (TextToSpeechOptions.isScreenReading())
            speech.speak("Opening search tutorials dialog", SpeechPriority.MEDIUM);
        this.setVisible(false);
        //SearchDialogSingleton.getInstance(searchField.getText()).setVisible(true);
    }

    /**
    * @param args the command line arguments
    */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                MainWindow dialog = new MainWindow(new javax.swing.JFrame(), false);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    @Override
    public void setVisible(boolean bln) {
        if (bln)
        {
            Dimension dim = getToolkit().getScreenSize();
            Rectangle abounds = getBounds();
            super.setLocation((dim.width - abounds.width) / 2,
                        (dim.height - abounds.height) / 2);
            if (firstTime)
            {
                if (TextToSpeechOptions.isScreenReading()) {
                    if (System.getProperty("os.name").startsWith("Mac")){
                        speech.speak("Main Tutorials Window. Press Function Key + F2 for help. ");
                    }
                    else{
                        speech.speak("Main Tutorials Window. Press F2 for help. ");
                    }
                }
                firstTime = false;
            }
            else {
                if (TextToSpeechOptions.isScreenReading()) {
                    speech.speak("Main Tutorials Window.");
                }
            }
        }
        super.setVisible(bln);
    }

    //private boolean menuOn = true;
    static TextToSpeech speech = TextToSpeechFactory.getDefaultTextToSpeech();
    String instructionsString;
    private JButton[] buttons;
    private boolean firstTime = true;

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton closeBtn;
    private javax.swing.JPanel headerPnl;
    private javax.swing.JButton hopBtn;
    private javax.swing.JButton hotkeysBtn;
    private javax.swing.JButton hslBtn;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JButton moreHelpBtn;
    private javax.swing.JCheckBox showOnStartupCheckbox;
    private javax.swing.JButton sodbeansBtn;
    private java.awt.Label titleLbl;
    // End of variables declaration//GEN-END:variables

}
