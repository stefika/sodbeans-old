/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sodbeans.tutorial;

/**
 *
 * @author user
 */
public class MainWindowSingleton {
    private static MainWindow instance;

    public MainWindowSingleton(){
        getInstance();
    }

    public static synchronized MainWindow getInstance(){
        if (instance == null)
            instance = new MainWindow(null, true);
        return instance;
    }
}
