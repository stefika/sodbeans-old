/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sodbeans.tutorial;

/**
 *
 * @author user
 */
public class SearchDialogSingleton {
    private static SearchDialog instance;
    private static String term;

    public SearchDialogSingleton(String term){
        getInstance(term);
    }

    public static synchronized SearchDialog getInstance(String t){
        setTerm(t);
        if (instance == null)
            instance = new SearchDialog(null, true, term);
        else
        {
            instance.resetTerm(term);
        }
        return instance;
    }

    public static synchronized SearchDialog getInstance(){
        if (instance == null)
            instance = new SearchDialog(null, true, term);
        else
        {
            instance.resetTerm(term);
        }
        return instance;
    }

    private static void setTerm(String t){
        term = t;
    }
}
