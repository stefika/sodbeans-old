/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sodbeans.tutorial;

/**
 *
 * @author user
 */
public class SelectionWindowSingleton {
    public static SelectionWindow instance;
    private static LoadMode mode;

    public SelectionWindowSingleton(LoadMode mode){
        getInstance(mode);
    }

    public static synchronized SelectionWindow getInstance(LoadMode m){
        setMode(m);
        if (instance == null)
            instance = new SelectionWindow(null, true, mode);
        else
        {
            instance.resetMode(mode);
        }
        return instance;
    }

    public static synchronized SelectionWindow getInstance(){
        if (instance == null)
            instance = new SelectionWindow(null, true, mode);
        else
        {
            instance.resetMode(mode);
        }
        return instance;
    }

    private static void setMode(LoadMode m){
        mode = m;
    }
}
