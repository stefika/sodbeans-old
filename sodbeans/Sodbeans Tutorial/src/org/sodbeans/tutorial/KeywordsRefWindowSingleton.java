/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sodbeans.tutorial;

/**
 *
 * @author Melissa Stefik
 */
public class KeywordsRefWindowSingleton {
    public static HelpWindow instance;

    public KeywordsRefWindowSingleton(){
        getInstance();
    }

    public static synchronized HelpWindow getInstance(){
        if (instance == null)
            instance = new HelpWindow(null, true, HelpWindowMode.KEYWORDS);
        return instance;
    }
}
