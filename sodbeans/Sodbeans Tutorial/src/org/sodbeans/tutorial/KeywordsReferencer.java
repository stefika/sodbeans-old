/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.sodbeans.tutorial;

import java.io.File;
import java.util.ArrayList;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 *
 * @author melissa
 */
class KeywordsReferencer {
    private final String KEYWORDS_REFERENCE_FILENAME = "KeywordsReference.xml";
    private ArrayList<ArrayList<String>> keywords = new ArrayList<ArrayList<String>>();
   
    public KeywordsReferencer() {
        TutorialLoader loader = new TutorialLoader();
        File ref = loader.loadTutorial(KEYWORDS_REFERENCE_FILENAME);
        
        buildKeywordsList(ref);
    }
    
    public int getNumberOfKeywords() {
        return keywords.size();
    }
    
    public String getDescription(int index) {
        return keywords.get(index).get(1);
    }
    
    public String getKeyword(int index) {
        return keywords.get(index).get(0);
    }
    
    private void buildKeywordsList(File ref) {
        
        try {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document doc = db.parse(ref);

            doc.getDocumentElement().normalize();
            NodeList ndlst = doc.getElementsByTagName("KEY");;

            for (int s = 0; s < ndlst.getLength(); s++) {
                Node node = ndlst.item(s);

                if (node.getNodeType() == Node.ELEMENT_NODE){
                    Element elmt = (Element)node;
                    String desc = elmt.getFirstChild().getNodeValue().trim();
                    String keyword = elmt.getAttribute("WORD").trim();
                    
                    ArrayList<String> nodes = new ArrayList<String>();
                    nodes.add(keyword);
                    nodes.add(desc);
                    keywords.add(nodes);
                }
            }
        } catch(Exception e)
        {
            e.printStackTrace();
        }
    }
    
}
