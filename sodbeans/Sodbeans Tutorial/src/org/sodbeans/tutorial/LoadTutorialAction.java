/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.sodbeans.tutorial;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public final class LoadTutorialAction implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent e) {
        MainWindowSingleton.getInstance().setVisible(true);
    }
}
