/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sodbeans.tutorial;

/**
 *
 * @author Kim Slattery
 */
public class HotkeysRefWindowSingleton {
    public static HelpWindow instance;

    public HotkeysRefWindowSingleton(){
        getInstance();
    }

    public static synchronized HelpWindow getInstance(){
        if (instance == null)
            instance = new HelpWindow(null, true, HelpWindowMode.HOTKEYS);
        return instance;
    }
}
