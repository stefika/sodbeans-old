/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.sodbeans.tutorial;

import java.io.File;
import java.util.ArrayList;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


/**
 * Contains a list of 3-tuples containing the shortcuts for Sodbeans Hotkeys
 * on PCs and Macs.
 * @author jeff
 */
public class HotkeysReferencer {
    private final String HOTKEYS_REFERENCE_FILENAME = "HotkeysReference.xml";
    private ArrayList<ArrayList<String>> hotkeys = new ArrayList<ArrayList<String>>();
   
    public HotkeysReferencer() {
        TutorialLoader loader = new TutorialLoader();
        File ref = loader.loadTutorial(HOTKEYS_REFERENCE_FILENAME);
        
        buildHotkeysList(ref);
    }
    
    public int getNumberOfHotkeys() {
        return hotkeys.size();
    }
    
    public String getDescription(int index) {
        return hotkeys.get(index).get(0);
    }
    
    public String getHotkey(int index) {
        return hotkeys.get(index).get(1);
    }
    
    private void buildHotkeysList(File ref) {
        String platform = System.getProperty("os.name");
        
        try {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document doc = db.parse(ref);

            doc.getDocumentElement().normalize();
            NodeList ndlst = doc.getElementsByTagName("KEY");;

            for (int s = 0; s < ndlst.getLength(); s++) {
                Node node = ndlst.item(s);

                if (node.getNodeType() == Node.ELEMENT_NODE){
                    Element elmt = (Element)node;
                    String desc = elmt.getFirstChild().getNodeValue().trim();
                    String macKey = elmt.getAttribute("MAC").trim();
                    String pcKey = elmt.getAttribute("PC").trim();
                    
                    // Is this a Mac system? If it is, and the MAC attribute
                    // is NON-EMPTY, add it to the ArrayList.
                    if (platform.startsWith("Mac") && !macKey.isEmpty())
                    {
                        ArrayList<String> nodes = new ArrayList<String>();
                        nodes.add(desc);
                        nodes.add(macKey);
                        hotkeys.add(nodes);
                    }
                    else
                    {
                        // Add it regardless, but if the PC attribute is empty,
                        // use the mac one because they are apparently identical
                        // on both systems.
                        if (pcKey.isEmpty())
                            pcKey = macKey;
                        
                        ArrayList<String> nodes = new ArrayList<String>();
                        nodes.add(desc);
                        nodes.add(pcKey);
                        hotkeys.add(nodes);
                    }
                }
            }
        } catch(Exception e)
        {
            e.printStackTrace();
        }
    }
}
