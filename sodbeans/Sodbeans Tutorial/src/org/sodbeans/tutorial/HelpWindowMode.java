/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.sodbeans.tutorial;

/**
 * This enum tracks the type of help window to load.
 * 
 * @author Melissa Stefik
 */
public enum HelpWindowMode {
    HOTKEYS,
    KEYWORDS
}
