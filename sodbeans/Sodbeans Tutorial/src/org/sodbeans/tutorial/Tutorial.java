/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sodbeans.tutorial;

import java.util.*;
import java.io.File;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 *
 * @author user
 */
public class Tutorial {

    private int currInst = 0;
    private String platform;
    private ArrayList<String> instructions = new ArrayList();
    private String relativePath = "";

    
    public Tutorial() {
        platform = System.getProperty("os.name");
    }
    
    
    public Tutorial(String file){
        platform = System.getProperty("os.name");
        initInstructions(file);
    }

     /**
     * @return the relativePath
     */
    public String getRelativePath() {
        return relativePath;
    }

    /**
     * @param relativePath the relativePath to set
     */
    public void setRelativePath(String relativePath) {
        this.relativePath = relativePath;
        initInstructions(relativePath);
    }
    
    // make hashmap with functions and function pointers to get instructions for any tutorial
    private void initInstructions(String filename){
        if (filename != null){
            try {
                TutorialLoader loader = new TutorialLoader();
                File file = loader.loadTutorial(filename);
                DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
                DocumentBuilder db = dbf.newDocumentBuilder();
                Document doc = db.parse(file);

                doc.getDocumentElement().normalize();
                NodeList ndlst = doc.getElementsByTagName("INSTRUCTION");

                for (int s = 0; s < ndlst.getLength(); s++) {
                    Node node = ndlst.item(s);

                    if (node.getNodeType() == Node.ELEMENT_NODE){
                        Element elmt = (Element)node;

                        // get system
                        NodeList sysList = elmt.getElementsByTagName("SYSTEM");
                        Element sysElmt = (Element)sysList.item(0);
                        NodeList sys = sysElmt.getChildNodes();

                        // get text
                        NodeList instList = elmt.getElementsByTagName("TEXT");
                        Element instElmt = (Element)instList.item(0);
                        NodeList inst = instElmt.getChildNodes();

                        String sysName = ((Node)sys.item(0)).getNodeValue().trim();
                        if (sysName.equals("All") || platform.startsWith(sysName) || (platform.startsWith("Lin") && sysName.equals("Win")))
                            instructions.add(((Node)inst.item(0)).getNodeValue().trim());
                    }
                }
            } catch(Exception e)
            {
                e.printStackTrace();
            }
        }
    }
    
    /**
     * Returns the next instruction.
     * @return
     */
    public String getNextInstruction(){
        if (currInst < instructions.size() - 1)
        {
            currInst += 1;
            return instructions.get(currInst);
        }
        return null;
    }

    /**
     * Returns the previous instruction.
     * @return
     */
    public String getPrevInstruction(){
        if (currInst > 0 && instructions.size() > 0)
        {
            currInst -= 1;
            return instructions.get(currInst);
        }
        return null;
    }

    /**
     * Returns the size of the instruction arraylist.
     * @return
     */
    public int getNoOfInstructions(){
        return instructions.size();
    }

    /**
     * Returns the instruction at index.
     * @param index
     * @return
     */
    public String getInstruction(int index){
        if (index >= 0 && index < instructions.size()){
            currInst = index;
            return instructions.get(index);
        }
        return null;
    }

    /**
     * Returns the index of the current element.
     * @return
     */
    public int getCurrIndex(){
        return currInst;
    }

    /**
     * Returns the index of the next element. If the iterator is positioned
     * after the last element, the size (or the position "after" the last element)
     * is returned.
     * @return
     */
    public int getNextIndex(){
        if (instructions.listIterator().hasNext())
            return instructions.listIterator().nextIndex();
        else
            return instructions.size();
    }

    /**
     * Returns the index of the previous element. If the iterator is at the
     * first element, -1 is returned.
     * @return
     */
    public int getPrevIndex(){
        if (instructions.listIterator().hasPrevious())
            return instructions.listIterator().previousIndex();
        else
            return -1;
    }

    
}
