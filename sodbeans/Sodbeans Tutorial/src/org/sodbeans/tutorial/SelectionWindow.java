/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * SelectionWindow.java
 *
 * Created on Mar 2, 2011, 12:44:15 PM
 */

package org.sodbeans.tutorial;

import javax.swing.*;
import org.sodbeans.phonemic.TextToSpeechFactory;
import org.sodbeans.phonemic.tts.TextToSpeech;
import org.sodbeans.phonemic.SpeechPriority;
import java.awt.event.*;
import java.awt.*;
import java.util.Map;
import java.util.HashMap;
import java.io.File;
import java.io.FileReader;
import java.io.BufferedReader;
import org.sodbeans.tts.options.api.TextToSpeechOptions;

/**
 *
 * @author Kim Slattery
 */
public class SelectionWindow extends javax.swing.JDialog {

    private LoadMode loadMode = LoadMode.SODBEANS;
    private JButton[] choiceButtons;
    private String[] tutorials;
    private String[] tutorialDescriptions;
    private String menuName;
    private String chosenTutorial;
    private Map tutorialFilenames;
    private int SODBEAN_PADDING = 4;

    static TextToSpeech speech = TextToSpeechFactory.getDefaultTextToSpeech();
    String instructionsString = "To navigate the tutorials, use the number keys 1 through 0, and the arrow keys or tab key. "
                    + "To get information about the selected tutorial, press the number of the tutorial. "
                    + "To start the tutorial, press enter or the spacebar. "
                    + "To go back to the main window, press backspace. "
                    + "To find out where you are in the window, press control + shift + F5. "
                    + "To hear instructions for any window, press F2.";

    /** Creates new form SelectionWindow */
    public SelectionWindow(java.awt.Frame parent, boolean modal, LoadMode mode) {
        super(parent, modal);
        loadMode = mode;
        initComponents();
        initOtherComponents();
    }

    private void initOtherComponents() {
        choiceButtons = new JButton[10];
        choiceButtons[0] = choice1Btn;
        choiceButtons[1] = choice2Btn;
        choiceButtons[2] = choice3Btn;
        choiceButtons[3] = choice4Btn;
        choiceButtons[4] = choice5Btn;
        choiceButtons[5] = choice6Btn;
        choiceButtons[6] = choice7Btn;
        choiceButtons[7] = choice8Btn;
        choiceButtons[8] = choice9Btn;
        choiceButtons[9] = choice10Btn;

        tutorialFilenames = new HashMap<Integer, String>();
        loadFilenames();

        tutorials = new String[10];
        tutorialDescriptions = new String[10];

        if (loadMode == LoadMode.SODBEANS)
            loadSodbeansMode();
        else if (loadMode == LoadMode.QUORUM)
            loadQuorumMode();
        else
            loadStdLibMode();
    }

    private void loadSodbeansMode() {
        menuName = "Sodbeans Tutorials Window. ";
        titleLbl.setText("Tutorials");

        tutorials[0] = "Windows and Navigation";
        tutorials[1] = "Projects and Files";
        tutorials[2] = "Sodbeans Hotkeys";
        tutorials[3] = "Debugging Programs";
        tutorials[4] = "Running Programs";
        tutorials[5] = "";
        tutorials[6] = "";
        tutorials[7] = "";
        tutorials[8] = "";
        tutorials[9] = "";

        tutorialDescriptions[0] = "This tutorial will show you how to navigate and use the windows in the Sodbeans environment.";
        tutorialDescriptions[1] = "This tutorial will show you how to create and open projects and files in Sodbeans.";
        tutorialDescriptions[2] = "This tutorial will give you an overview of the Sodbeans hot keys and their uses.";
        tutorialDescriptions[3] = "This tutorial will show you how to use the debugger for a Quorum program in Sodbeans.";
        tutorialDescriptions[4] = "This tutorial will show you how to run a Quorum program in Sodbeans.";
        tutorialDescriptions[5] = "";
        tutorialDescriptions[6] = "";
        tutorialDescriptions[7] = "";
        tutorialDescriptions[8] = "";
        tutorialDescriptions[9] = "";

        generateButtonLabels();
        //centerTitleLbl();
        instructionsString = menuName + instructionsString;
    }

    private void loadQuorumMode() {
        menuName = "Quorum Tutorials Window. ";
        titleLbl.setText("Quorum Tutorials");

        tutorials[0] = "Variables";
        tutorials[1] = "Doing Math with Quorum";
        tutorials[2] = "User Interaction";
        tutorials[3] = "Conditionals and Loops";
        tutorials[4] = "Actions";
        tutorials[5] = "Classes";
        tutorials[6] = "Inheritance";
        tutorials[7] = "Generics";
        tutorials[8] = "Errors";
        tutorials[9] = "Quorum Keyword Reference";

        tutorialDescriptions[0] = "This tutorial will teach you how to create and use variables in Quorum.";
        tutorialDescriptions[1] = "This tutorial will teach you how to perform mathematical operations in Quorum.";
        tutorialDescriptions[2] = "This tutorial will teach you how to interact with the user of a Quorum program.";
        tutorialDescriptions[3] = "This tutorial will teach you about conditional statements and loops in Quorum.";
        tutorialDescriptions[4] = "This tutorial will teach you about writing and using actions also called methods or functions in Quorum.";
        tutorialDescriptions[5] = "This tutorial will teach you about writing and using Quorum classes.";
        tutorialDescriptions[6] = "This tutorial will teach you how to use inheritance for Quorum classes.";
        tutorialDescriptions[7] = "This tutorial will teach you how to use generics or templating in Quorum.";
        tutorialDescriptions[8] = "This tutorial will teach you about errors and error handling in Quorum.";
        tutorialDescriptions[9] = "This button will take you to a list of keywords available in Quorum.";

        generateButtonLabels();
        //centerTitleLbl();
        instructionsString = menuName + instructionsString;
    }

    private void loadStdLibMode() {
        menuName = "Quorum Standard Library Tutorials Window. ";
        titleLbl.setText("Quorum Standard Library Tutorials");

        tutorials[0] = "Math";
        tutorials[1] = "List and Array";
        tutorials[2] = "Queue and Stack";
        tutorials[3] = "File";
        tutorials[4] = "";
        tutorials[5] = "";
        tutorials[6] = "";
        tutorials[7] = "";
        tutorials[8] = "";
        tutorials[9] = "";

        tutorialDescriptions[0] = "This tutorial will teach you about the Math class available in the Quorum Standard Library.";
        tutorialDescriptions[1] = "This tutorial will teach you about the List and Array classes available in the Quorum Standard Library.";
        tutorialDescriptions[2] = "This tutorial will teach you about the Queue and Stack classes available in the Quprum Standard Library.";
        tutorialDescriptions[3] = "This tutorial will teach you about the File class available in the Quorum Standard Library.";
        tutorialDescriptions[4] = "";
        tutorialDescriptions[5] = "";
        tutorialDescriptions[6] = "";
        tutorialDescriptions[7] = "";
        tutorialDescriptions[8] = "";
        tutorialDescriptions[9] = "";

        generateButtonLabels();
        //centerTitleLbl();
        instructionsString = menuName + instructionsString;
    }

    private void centerTitleLbl(){
        /*int width = titlePnl.getWidth();
        int totalWidth = sodbeanLbl.getWidth() + SODBEAN_PADDING + titleLbl.getWidth();
        //Dimension sodbeanSize = sodbeanLbl.getSize();
        //Dimension lblSize = titleLbl.getSize();
        int sodbeanY = sodbeanLbl.getY();
        int lblY = titleLbl.getY();
        int sodbeanX = titlePnl.getX() + (width - totalWidth)/2;
        int lblX = sodbeanX + SODBEAN_PADDING + sodbeanLbl.getWidth();
        //sodbeanLbl.setLayout(null);
        //titleLbl.setLayout(null);
        sodbeanLbl.setLocation(sodbeanX, sodbeanY);
        titleLbl.setLocation(lblX, lblY);
        //sodbeanLbl.setSize(sodbeanSize);
        //titleLbl.setSize(lblSize);
        this.doLayout();
        //sodbeanLbl.doLayout();
        //titleLbl.doLayout();   */
        int sodbeanX = titleLbl.getX() - (sodbeanLbl.getWidth() + SODBEAN_PADDING);
        sodbeanLbl.setLocation(sodbeanX, sodbeanLbl.getY());
    }

    private void generateButtonLabels(){
        for(int i = 0; i < 10; i++)
        {
            if (tutorials[i].compareTo("") == 0){
                choiceButtons[i].setEnabled(false);
                choiceButtons[i].setText("");
                choiceButtons[i].getAccessibleContext().setAccessibleName("");
            }
            else
            {
                choiceButtons[i].setText("[" + ((i + 1) % 10) + "] " + tutorials[i]);
                if (tutorials[i].contains("Hotkeys")){
                    choiceButtons[i].getAccessibleContext().setAccessibleName(tutorials[i].replace("Hotkeys", "Hot keys")
                                                                               + " [" + ((i + 1) % 10) + "]");
                }
                else{
                    choiceButtons[i].getAccessibleContext().setAccessibleName(tutorials[i] + " [" + ((i + 1) % 10) + "]" );
                }
                choiceButtons[i].setEnabled(true);
            }
        }
    }

    private void loadFilenames() {
        TutorialLoader loader = new TutorialLoader();
        File files = loader.loadIndex();
        FileReader fread;
        BufferedReader reader;
        int i = 0;
        int j = 0;
        String s;

        try{
            fread = new FileReader(files);
            reader = new BufferedReader(fread);

            while (reader.ready())
            {
                s = reader.readLine();
                if (s.equals(""))
                {
                    i++;
                    j = 0;
                }
                else{
                    tutorialFilenames.put(new Integer((getModePrefix(i)) + j), s);
                    j++;
                }
            }
        } catch (Exception ex)
        {
            return;
        }
        return;
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        choice7Btn = new javax.swing.JButton();
        choice1Btn = new javax.swing.JButton();
        choice3Btn = new javax.swing.JButton();
        choice5Btn = new javax.swing.JButton();
        choice9Btn = new javax.swing.JButton();
        choice2Btn = new javax.swing.JButton();
        choice4Btn = new javax.swing.JButton();
        choice6Btn = new javax.swing.JButton();
        backBtn = new javax.swing.JButton();
        choice8Btn = new javax.swing.JButton();
        choice10Btn = new javax.swing.JButton();
        titlePnl = new javax.swing.JPanel();
        sodbeanLbl = new javax.swing.JLabel();
        titleLbl = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setBackground(new java.awt.Color(204, 204, 204));
        setName("Tutorials Selection Window"); // NOI18N

        choice7Btn.setFont(new java.awt.Font("Tahoma", 1, 18));
        choice7Btn.setText(org.openide.util.NbBundle.getMessage(SelectionWindow.class, "SelectionWindow.choice7Btn.text")); // NOI18N
        choice7Btn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                choice7BtnActionPerformed(evt);
            }
        });
        choice7Btn.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                SelectionWindow.this.keyPressed(evt);
            }
        });

        choice1Btn.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        choice1Btn.setText(org.openide.util.NbBundle.getMessage(SelectionWindow.class, "SelectionWindow.choice1Btn.text")); // NOI18N
        choice1Btn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                choice1BtnActionPerformed(evt);
            }
        });
        choice1Btn.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                SelectionWindow.this.keyPressed(evt);
            }
        });

        choice3Btn.setFont(new java.awt.Font("Tahoma", 1, 18));
        choice3Btn.setText(org.openide.util.NbBundle.getMessage(SelectionWindow.class, "SelectionWindow.choice3Btn.text")); // NOI18N
        choice3Btn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                choice3BtnActionPerformed(evt);
            }
        });
        choice3Btn.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                SelectionWindow.this.keyPressed(evt);
            }
        });

        choice5Btn.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        choice5Btn.setText(org.openide.util.NbBundle.getMessage(SelectionWindow.class, "SelectionWindow.choice5Btn.text")); // NOI18N
        choice5Btn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                choice5BtnActionPerformed(evt);
            }
        });
        choice5Btn.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                SelectionWindow.this.keyPressed(evt);
            }
        });

        choice9Btn.setFont(new java.awt.Font("Tahoma", 1, 18));
        choice9Btn.setText(org.openide.util.NbBundle.getMessage(SelectionWindow.class, "SelectionWindow.choice9Btn.text")); // NOI18N
        choice9Btn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                choice9BtnActionPerformed(evt);
            }
        });
        choice9Btn.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                SelectionWindow.this.keyPressed(evt);
            }
        });

        choice2Btn.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        choice2Btn.setText(org.openide.util.NbBundle.getMessage(SelectionWindow.class, "SelectionWindow.choice2Btn.text")); // NOI18N
        choice2Btn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                choice2BtnActionPerformed(evt);
            }
        });
        choice2Btn.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                SelectionWindow.this.keyPressed(evt);
            }
        });

        choice4Btn.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        choice4Btn.setText(org.openide.util.NbBundle.getMessage(SelectionWindow.class, "SelectionWindow.choice4Btn.text")); // NOI18N
        choice4Btn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                choice4BtnActionPerformed(evt);
            }
        });
        choice4Btn.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                SelectionWindow.this.keyPressed(evt);
            }
        });

        choice6Btn.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        choice6Btn.setText(org.openide.util.NbBundle.getMessage(SelectionWindow.class, "SelectionWindow.choice6Btn.text")); // NOI18N
        choice6Btn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                choice6BtnActionPerformed(evt);
            }
        });
        choice6Btn.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                SelectionWindow.this.keyPressed(evt);
            }
        });

        backBtn.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        backBtn.setText(org.openide.util.NbBundle.getMessage(SelectionWindow.class, "SelectionWindow.backBtn.text")); // NOI18N
        backBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backBtnActionPerformed(evt);
            }
        });
        backBtn.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                SelectionWindow.this.keyPressed(evt);
            }
        });

        choice8Btn.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        choice8Btn.setText(org.openide.util.NbBundle.getMessage(SelectionWindow.class, "SelectionWindow.choice8Btn.text")); // NOI18N
        choice8Btn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                choice8BtnActionPerformed(evt);
            }
        });
        choice8Btn.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                SelectionWindow.this.keyPressed(evt);
            }
        });

        choice10Btn.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        choice10Btn.setText(org.openide.util.NbBundle.getMessage(SelectionWindow.class, "SelectionWindow.choice10Btn.text")); // NOI18N
        choice10Btn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                choice10BtnActionPerformed(evt);
            }
        });
        choice10Btn.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                SelectionWindow.this.keyPressed(evt);
            }
        });

        sodbeanLbl.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/sodbeans/tutorial/Resources/SodbeansLogoTransparent4.png"))); // NOI18N
        sodbeanLbl.setText(org.openide.util.NbBundle.getMessage(SelectionWindow.class, "SelectionWindow.sodbeanLbl.text")); // NOI18N

        titleLbl.setFont(new java.awt.Font("Tahoma", 1, 40)); // NOI18N
        titleLbl.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        titleLbl.setText(org.openide.util.NbBundle.getMessage(SelectionWindow.class, "SelectionWindow.titleLbl.text")); // NOI18N
        titleLbl.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        org.jdesktop.layout.GroupLayout titlePnlLayout = new org.jdesktop.layout.GroupLayout(titlePnl);
        titlePnl.setLayout(titlePnlLayout);
        titlePnlLayout.setHorizontalGroup(
            titlePnlLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, titlePnlLayout.createSequentialGroup()
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .add(sodbeanLbl)
                .add(3, 3, 3)
                .add(titleLbl)
                .add(753, 753, 753))
        );
        titlePnlLayout.setVerticalGroup(
            titlePnlLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(sodbeanLbl)
            .add(titlePnlLayout.createSequentialGroup()
                .addContainerGap()
                .add(titleLbl))
        );

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(layout.createSequentialGroup()
                        .add(44, 44, 44)
                        .add(titlePnl, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 751, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                    .add(layout.createSequentialGroup()
                        .addContainerGap()
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                                .add(layout.createSequentialGroup()
                                    .add(choice9Btn, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 400, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                    .add(choice10Btn, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .add(layout.createSequentialGroup()
                                    .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                                        .add(org.jdesktop.layout.GroupLayout.LEADING, choice7Btn, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 400, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                        .add(org.jdesktop.layout.GroupLayout.LEADING, layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING, false)
                                            .add(org.jdesktop.layout.GroupLayout.LEADING, choice1Btn, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .add(org.jdesktop.layout.GroupLayout.LEADING, choice3Btn, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 400, Short.MAX_VALUE)
                                            .add(org.jdesktop.layout.GroupLayout.LEADING, choice5Btn, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 400, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                                    .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                    .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                                        .add(choice8Btn, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .add(choice4Btn, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 400, Short.MAX_VALUE)
                                        .add(choice2Btn, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 400, Short.MAX_VALUE)
                                        .add(choice6Btn, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                            .add(layout.createSequentialGroup()
                                .add(225, 225, 225)
                                .add(backBtn, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 357, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .addContainerGap()
                .add(titlePnl, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(18, 18, 18)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(choice1Btn, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 70, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(choice2Btn, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 70, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(choice3Btn, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 70, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(choice4Btn, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 70, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(choice5Btn, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 70, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(choice6Btn, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 70, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(choice7Btn, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 70, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(choice8Btn, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 70, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(choice9Btn, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 70, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(choice10Btn, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 70, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(backBtn, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 70, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents


    private void keyDown(Component c) {
        c.transferFocus();
    }

    private void keyUp(Component c) {
        c.transferFocusBackward();
    }

    private void speakDetails() {
        for(int i = 0; i < 10; i++)
        {
            if (choiceButtons[i].hasFocus())
            {
                if (TextToSpeechOptions.isScreenReading()) {
                    speech.speak(((i+1) % 10) + ", " + tutorials[i] + ", " + tutorialDescriptions[i]);
                }
                i = 10;
            }
        }
    }

    private void clickButtonWithFocus(Component c){
        if (c.getClass() == JButton.class){
            JButton b = (JButton)c;
            b.doClick();
        }
    }

    private void goBack() {
        if (TextToSpeechOptions.isScreenReading())
            speech.speak("Returning to main tutorials menu", SpeechPriority.MEDIUM);
        choice1Btn.requestFocus();
        this.setVisible(false);
        MainWindowSingleton.getInstance().setVisible(true);
    }

    private void startTutorial(int index){
        chosenTutorial = (String)tutorialFilenames.get(new Integer(getModePrefix(loadMode.ordinal()) + index));
        if (TextToSpeechOptions.isScreenReading())
            speech.speak("Beginning " + tutorials[index] + " tutorial", SpeechPriority.MEDIUM);
        // TODO: change for Sodbeans Hotkeys tutorial
        SelectionWindowSingleton.getInstance(loadMode).setVisible(false);
        TutorialTopComponent comp = TutorialTopComponent.findInstance();
        comp.open();
        comp.initTutorial(chosenTutorial, loadMode);
        comp.startTutorial();
    }

    /*
     *  This function returns the "mode prefix," which is the first part of the
     *  the key for the tutorialFilenames hashmap.  This prefix is based on the
     *  loadMode, which must be passed as an int.
     */
    private int getModePrefix(int mode){
        return mode * 100;
    }

    private void respeakOrFocusButton(JButton btn){
        if (btn.hasFocus())
            speakDetails();
        else
            btn.requestFocus();
    }

    private void backBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backBtnActionPerformed
        goBack();
    }//GEN-LAST:event_backBtnActionPerformed

    private void choice1BtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_choice1BtnActionPerformed
        startTutorial(0);
    }//GEN-LAST:event_choice1BtnActionPerformed

    private void choice2BtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_choice2BtnActionPerformed
        startTutorial(1);
    }//GEN-LAST:event_choice2BtnActionPerformed

    private void choice3BtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_choice3BtnActionPerformed
        startTutorial(2);
    }//GEN-LAST:event_choice3BtnActionPerformed

    private void choice4BtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_choice4BtnActionPerformed
        startTutorial(3);
    }//GEN-LAST:event_choice4BtnActionPerformed

    private void choice5BtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_choice5BtnActionPerformed
        startTutorial(4);
    }//GEN-LAST:event_choice5BtnActionPerformed

    private void choice6BtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_choice6BtnActionPerformed
        startTutorial(5);
    }//GEN-LAST:event_choice6BtnActionPerformed

    private void choice7BtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_choice7BtnActionPerformed
        startTutorial(6);
    }//GEN-LAST:event_choice7BtnActionPerformed

    private void choice8BtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_choice8BtnActionPerformed
        startTutorial(7);
    }//GEN-LAST:event_choice8BtnActionPerformed

    private void choice9BtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_choice9BtnActionPerformed
        startTutorial(8);
    }//GEN-LAST:event_choice9BtnActionPerformed

    private void choice10BtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_choice10BtnActionPerformed
        //resetMode(LoadMode.STD_LIB);
        //choice1Btn.requestFocus();
        openKeywordReferenceWindow();
    }//GEN-LAST:event_choice10BtnActionPerformed

    private void keyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_keyPressed
        if (evt.getKeyCode() == KeyEvent.VK_DOWN || evt.getKeyCode() == KeyEvent.VK_RIGHT)
        {
            keyDown(evt.getComponent());
        }
        else if(evt.getKeyCode() == KeyEvent.VK_UP || evt.getKeyCode() == KeyEvent.VK_LEFT)
        {
            keyUp(evt.getComponent());
        }
        else if (evt.getKeyCode() == KeyEvent.VK_F2)
        {
            if (TextToSpeechOptions.isScreenReading()) {
                speech.speak(instructionsString);
            }
        }
        else if (evt.getKeyCode() == KeyEvent.VK_ESCAPE)
        {
            goBack();
        }if (evt.getKeyCode() == KeyEvent.VK_1)
        {
            respeakOrFocusButton(choiceButtons[0]);
        }
        else if (evt.getKeyCode() == KeyEvent.VK_2)
        {
            respeakOrFocusButton(choiceButtons[1]);
        }
        else if (evt.getKeyCode() == KeyEvent.VK_3)
        {
            respeakOrFocusButton(choiceButtons[2]);
        }
        else if (evt.getKeyCode() == KeyEvent.VK_4)
        {
            respeakOrFocusButton(choiceButtons[3]);
        }
        else if (evt.getKeyCode() == KeyEvent.VK_5)
        {
            respeakOrFocusButton(choiceButtons[4]);
        }
        else if (evt.getKeyCode() == KeyEvent.VK_6)
        {
            respeakOrFocusButton(choiceButtons[5]);
        }
        else if (evt.getKeyCode() == KeyEvent.VK_7)
        {
            respeakOrFocusButton(choiceButtons[6]);
        }
        else if (evt.getKeyCode() == KeyEvent.VK_8)
        {
            respeakOrFocusButton(choiceButtons[7]);
        }
        else if (evt.getKeyCode() == KeyEvent.VK_9)
        {
            respeakOrFocusButton(choiceButtons[8]);
        }
        else if (evt.getKeyCode() == KeyEvent.VK_0)
        {
            respeakOrFocusButton(choiceButtons[9]);
        }
        else if (evt.getKeyCode() == KeyEvent.VK_BACK_SPACE)
        {
            goBack();
            backBtn.requestFocus();
        }
        else if(evt.getKeyCode() == KeyEvent.VK_ENTER/* || evt.getKeyCode() == KeyEvent.VK_SPACE*/)
        {
            clickButtonWithFocus(evt.getComponent());
        }
    }//GEN-LAST:event_keyPressed

    /**
    * @param args the command line arguments
    */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                SelectionWindow dialog = new SelectionWindow(new javax.swing.JFrame(), false, LoadMode.SODBEANS);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    @Override
    public void setVisible(boolean bln) {
        if (bln)
        {
            Dimension dim = getToolkit().getScreenSize();
            Rectangle abounds = getBounds();
            super.setLocation((dim.width - abounds.width) / 2,
                        (dim.height - abounds.height) / 2);
            if (TextToSpeechOptions.isScreenReading()) {
                speech.speak(menuName + " "
                        + choice1Btn.getAccessibleContext().getAccessibleName()
                        + ", button");
            }
        }
        super.setVisible(bln);
    }

    @Override
    public void dispose() {
        SelectionWindowSingleton.getInstance(loadMode).setVisible(false);
        MainWindowSingleton.getInstance().setVisible(true);
        super.dispose();
    }

    public LoadMode getMode() {
        return loadMode;
    }

    public void resetMode(LoadMode m) {
        if (m.equals(LoadMode.SODBEANS)){
            loadMode = LoadMode.SODBEANS;
            loadSodbeansMode();
        }
        else if(m.equals(LoadMode.QUORUM)){
            loadMode = LoadMode.QUORUM;
            loadQuorumMode();
        }
        else{
            loadMode = LoadMode.STD_LIB;
            loadStdLibMode();
        }
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton backBtn;
    private javax.swing.JButton choice10Btn;
    private javax.swing.JButton choice1Btn;
    private javax.swing.JButton choice2Btn;
    private javax.swing.JButton choice3Btn;
    private javax.swing.JButton choice4Btn;
    private javax.swing.JButton choice5Btn;
    private javax.swing.JButton choice6Btn;
    private javax.swing.JButton choice7Btn;
    private javax.swing.JButton choice8Btn;
    private javax.swing.JButton choice9Btn;
    private javax.swing.JLabel sodbeanLbl;
    private javax.swing.JLabel titleLbl;
    private javax.swing.JPanel titlePnl;
    // End of variables declaration//GEN-END:variables

    private void openKeywordReferenceWindow() {
        if (TextToSpeechOptions.isScreenReading()) {
            speech.speak("Opening Keyword Reference window", SpeechPriority.MEDIUM);
        }
        this.setVisible(false);
        KeywordsRefWindowSingleton.getInstance().setVisible(true);
    }

}
