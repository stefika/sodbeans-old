/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.sodbeans.tutorial;

import org.openide.modules.ModuleInstall;
import org.openide.util.NbPreferences;
import org.openide.windows.WindowManager;

/**
 * Manages a module's lifecycle. Remember that an installer is optional and
 * often not needed at all.
 */
public class Installer extends ModuleInstall implements Runnable{
    private boolean loadOnStartup = false;

    @Override
    public void restored() {
        loadOnStartup = NbPreferences.forModule(MainWindow.class).getBoolean(MainWindow.LOAD_AT_STARTUP, false);
        WindowManager manager = WindowManager.getDefault();
        manager.invokeWhenUIReady(this);
    }

    @Override
    public void run() {
         if(loadOnStartup) {
            MainWindowSingleton.getInstance().setVisible(true);
        }
    }
}
