/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.sodbeans.navigator;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;
import javax.swing.AbstractAction;
import javax.swing.JEditorPane;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.JViewport;
import javax.swing.KeyStroke;
import org.openide.cookies.EditorCookie;
import org.openide.cookies.EditorCookie.Observable;
import org.openide.cookies.LineCookie;
import org.openide.explorer.ExplorerManager;
import org.openide.explorer.ExplorerUtils;
import org.openide.explorer.view.BeanTreeView;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectNotFoundException;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Node;
import org.openide.text.Line;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.windows.TopComponent;
import org.sodbeans.compiler.api.descriptors.CompilerFileDescriptor;
import org.sodbeans.compiler.api.descriptors.CompilerNode;
import org.sodbeans.phonemic.SpeechPriority;
import org.sodbeans.phonemic.TextToSpeechFactory;
import org.sodbeans.phonemic.tts.TextToSpeech;
import org.sodbeans.tts.options.api.TextToSpeechOptions;

/**
 * This class represents the view of the navigator window for the Compiler
 * Module.
 * 
 * @author Neelima Sansami and Andreas Stefik
 */
public class CompilerView extends TopComponent implements ExplorerManager.Provider, ActionListener, MouseListener {
    private TextToSpeech speech = TextToSpeechFactory.getDefaultTextToSpeech();
    private org.sodbeans.compiler.api.Compiler compiler;
    private FileObject currentFileInEditor;
    private ExplorerManager mgr = new ExplorerManager();
    protected Node actionNode;

    /** Creates new customizer CompilerView */
    public CompilerView() {
        initComponents();
        compiler = Lookup.getDefault().lookup(org.sodbeans.compiler.api.Compiler.class);
        associateLookup(ExplorerUtils.createLookup(mgr, getActionMap()));
        resetModelToCurrentFile();
        setFocusable(true);
//        KeyStroke upKey = KeyStroke.getKeyStroke(KeyEvent.VK_UP, 0, true);
//        ((BeanTreeView)jScrollPane1).getInputMap(JScrollPane.WHEN_IN_FOCUSED_WINDOW).put(upKey, "up");
//        ((BeanTreeView)jScrollPane1).getActionMap().put("up", new KeyHandler());
//        KeyStroke downKey = KeyStroke.getKeyStroke(KeyEvent.VK_DOWN, 0, true);
//        ((BeanTreeView)jScrollPane1).getInputMap(JScrollPane.WHEN_IN_FOCUSED_WINDOW).put(downKey, "down");
//        ((BeanTreeView)jScrollPane1).getActionMap().put("down", new KeyHandler());
//        KeyStroke leftArrowKey = KeyStroke.getKeyStroke(KeyEvent.VK_LEFT, 0, true);
//        ((BeanTreeView)jScrollPane1).getInputMap(JScrollPane.WHEN_IN_FOCUSED_WINDOW).put(leftArrowKey, "left");
//        ((BeanTreeView)jScrollPane1).getActionMap().put("left", new LeftKeyHandler());
//        KeyStroke rightArrowKey = KeyStroke.getKeyStroke(KeyEvent.VK_RIGHT, 0, true);
//        ((BeanTreeView)jScrollPane1).getInputMap(JScrollPane.WHEN_IN_FOCUSED_WINDOW).put(rightArrowKey, "right");
//        ((BeanTreeView)jScrollPane1).getActionMap().put("right", new RightKeyHandler());
//         KeyStroke enterKey = KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, true);
//        ((BeanTreeView)jScrollPane1).getInputMap(JScrollPane.WHEN_IN_FOCUSED_WINDOW).put(enterKey, "enter");
//        ((BeanTreeView)jScrollPane1).getActionMap().put("enter", new EnterKeyHandler());
//        ((BeanTreeView)jScrollPane1).getViewport().getView().addMouseListener((MouseListener)this);
//
    }

    @Override
    public int getPersistenceType() {
        return TopComponent.PERSISTENCE_NEVER;
    }

    @Override
    public boolean requestFocusInWindow(boolean val) {
        boolean bool = super.requestFocusInWindow(val);
        //kind of a hack, but it forces the focus
        //to be in the correct window, which is easier for a blind user.
        JViewport viewport = jScrollPane1.getViewport();
        if (viewport != null) {
            Component[] components = viewport.getComponents();
            if (components != null && components.length > 0) {
                Component comp = components[0];
                if (comp != null) {
                    if (comp instanceof JTree) {
                        JTree tree = (JTree) comp;
                        if (tree.getSelectionPath() == null) {
                            //select a default node
                            //just ignore anything that goes wrong
                            try {
                                tree.setSelectionRow(0);
                            } catch (Exception e) {
                            }
                        } else {
                            int[] rows = tree.getSelectionRows();
                            if(rows.length >= 1) {
                                tree.setSelectionRow(rows[0]);
                            }

                        }
                    }
                    boolean failed = comp.requestFocusInWindow();
                    int a = 5;
                }
            }
        }
        return bool;
    }

    /**
     * Tells the class to gather information about the current file from
     * the compiler and display it to the user.
     */
    public Node resetModelToCurrentFile() {
        if (currentFileInEditor != null) {
            CompilerFileDescriptor cfd = compiler.getFileDescriptor(currentFileInEditor);
            if (cfd != null) {
                AbstractNode root = cfd.getRootNode();
                if (root != null) {
                    mgr.setRootContext(root);
                    ((BeanTreeView) jScrollPane1).setRootVisible(false);
                }
            }
        }
        return null;
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the FormEditor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new BeanTreeView();

        setLayout(new java.awt.BorderLayout());

        setFocusable(false);
        add(jScrollPane1, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane jScrollPane1;
    // End of variables declaration//GEN-END:variables

    /**
     * Returns the explorer manager that this component uses.
     * @return
     */
    public ExplorerManager getExplorerManager() {
        return mgr;
    }

    /**
     * @return the currentFileInEditor
     */
    public FileObject getCurrentFileInEditor() {
        return currentFileInEditor;
    }

    /**
     * @param currentFileInEditor the currentFileInEditor to set
     */
    public void setCurrentFileInEditor(FileObject currentFileInEditor) {
        this.currentFileInEditor = currentFileInEditor;
    }

    public void actionPerformed(ActionEvent e) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public Node getNodeSelected() {
        Node[] nodes = mgr.getSelectedNodes();
        Node cNode = null;
        if (nodes != null) {
            if (nodes.length != 0) {
                if (nodes[0] instanceof CompilerNode) {
                    cNode = (CompilerNode) nodes[0];
                }
            }
        }
        return cNode;
    }

    public void mouseClicked(MouseEvent e) {
        if (e.getClickCount() >= 2) {
            doubleClickHandler();
        }
    }

    public void mousePressed(MouseEvent e) {
    }

    public void mouseReleased(MouseEvent e) {
    }

    public void mouseEntered(MouseEvent e) {
    }

    public void mouseExited(MouseEvent e) {
    }

    public class KeyHandler extends AbstractAction {

        public void actionPerformed(ActionEvent evt) {
            Node n = getNodeSelected();
            if (n == null) {
                return;
            }
            
            if (TextToSpeechOptions.isScreenReading()) {
                speech.speak(n.getDisplayName() + " " + n.getShortDescription(),
                        SpeechPriority.MEDIUM);
            }
            actionNode = n;
        }
    }

    public class LeftKeyHandler extends AbstractAction {

        public void actionPerformed(ActionEvent evt) {
            Node n = getNodeSelected();
            if (n == null) {
                return;
            }
            if (actionNode == n) {
                if (TextToSpeechOptions.isScreenReading()) {
                    String str1 = "Closing" + n.getDisplayName() + " " + n.getShortDescription();
                    speech.speak(str1, SpeechPriority.MEDIUM);
                }
            } else {
                if (TextToSpeechOptions.isScreenReading()) {
                    String str1 = n.getDisplayName() + " " + n.getShortDescription();
                    speech.speak(str1, SpeechPriority.MEDIUM);
                }
            }
            actionNode = getNodeSelected();
        }
    }

    public class RightKeyHandler extends AbstractAction {

        public void actionPerformed(ActionEvent evt) {
            Node n = getNodeSelected();
            if (n == null) {
                return;
            }
            if (n.getShortDescription().compareTo("Class") == 0) {
                if (TextToSpeechOptions.isScreenReading()) {
                    String str = "Opening" + n.getDisplayName() + " " + n.getShortDescription();
                    speech.speak(str, SpeechPriority.MEDIUM);
                }
            }
            actionNode = getNodeSelected();
        }
    }

    public class EnterKeyHandler extends AbstractAction {

        public void actionPerformed(ActionEvent evt) {
            doubleClickHandler();
            actionNode = getNodeSelected();
        }
    }

    public void doubleClickHandler() {
        int lineNumber = 0;
        CompilerNode n = (CompilerNode) getNodeSelected();

        if (n == null) {
            return;
        }
        try {
            DataObject dob = DataObject.find(currentFileInEditor);

            //Bug fix: actually open the file
            openEditor(dob);

            EditorCookie ck = (EditorCookie) dob.getCookie(EditorCookie.class);
            if (ck != null) {
                ck.openDocument();
                JEditorPane[] p = ck.getOpenedPanes();
                if (p.length > 0) {
                    //Need to do this since we're disabling the window system's
                    //auto focus mechanism
                    p[0].requestFocus();
                    if (dob != null) {
                        LineCookie lc = (LineCookie) dob.getCookie(LineCookie.class);
                        if (lc == null) {
                            return;
                        }
                        lineNumber = n.getDescriptor().getLine() - 1;
                        Line l = lc.getLineSet().getOriginal(lineNumber);
                        l.show(Line.ShowOpenType.OPEN, Line.ShowVisibilityType.FOCUS);

                        if (TextToSpeechOptions.isScreenReading()) {
                        speech.speak("Focussed to " + currentFileInEditor.getName()
                                + " dot " + currentFileInEditor.getExt() + " "
                                + n.getShortDescription() + " " + n.getDisplayName(),
                                SpeechPriority.MEDIUM);
                        }
                    }
                }
            }
        } catch (DataObjectNotFoundException e) {
            Exceptions.printStackTrace(e);
        } catch (IOException ioe) {
            Exceptions.printStackTrace(ioe);
        }
    }

    private void openEditor(DataObject dataObj) {
        final Observable ec = dataObj.getCookie(Observable.class);

        if (ec != null) {
            runInEventDispatchThread(new Runnable() {

                public void run() {
                    ec.open();
                }
            });
        }
    }

    private void runInEventDispatchThread(Runnable runnable) {
        if (javax.swing.SwingUtilities.isEventDispatchThread()) {
            runnable.run();
        } else {
            javax.swing.SwingUtilities.invokeLater(runnable);
        }
    }
}
