/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */

package org.sodbeans.navigator;

import java.util.Collection;
import java.util.Iterator;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComponent;
import org.netbeans.spi.navigator.NavigatorPanel;
import org.openide.filesystems.FileObject;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.sodbeans.compiler.api.CompilerEvent;
import org.sodbeans.compiler.api.CompilerListener;

/**
 *  Thanks to http://bits.netbeans.org/dev/javadoc/org-netbeans-spi-navigator/overview-summary.html
 * @author Andreas Stefik
 */
public class Navigator implements NavigatorPanel, CompilerListener {

    /** holds UI of this panel */
    private CompilerView panelUI = new CompilerView();
    /** template for finding data in given context.
     * Object used as example, replace with your own data source, for example JavaDataObject etc */
    private static final Lookup.Template MY_DATA = new Lookup.Template(FileObject.class);
    
    /** current context to work on */
    private Lookup.Result curContext;
    /** listener to context changes */
    private LookupListener contextL;
    private org.sodbeans.compiler.api.Compiler compiler =
            Lookup.getDefault().lookup(org.sodbeans.compiler.api.Compiler.class);
    private static final Logger logger = Logger.getLogger(Navigator.class.getName());
    

    Node selectedNode = null;
    
    /** public no arg constructor needed for system to instantiate provider well */
    public Navigator() {
        compiler.addListener(this);
    }

    public String getDisplayHint() {
        return "Displays a navigator for the Sodbeans language";
    }

    public String getDisplayName() {
        return "Sodbeans navigator";
    }

    public JComponent getComponent() {
        panelUI.requestFocusInWindow(true);
        return panelUI;
    }

    public void panelActivated(Lookup context) {
        // lookup context and listen to result to get notified about context changes
        curContext = context.lookup(MY_DATA);
        Collection data = curContext.allInstances();
        
        try {
            setNewContent(data);
        } catch (Exception exception) {
            logger.log(Level.INFO, "An exception was thrown when trying to activate the navigator window. Thread: " + Thread.currentThread().getName(), exception);
        }
        
        curContext.addLookupListener(getContextListener());
    }

    public void panelDeactivated() {
        curContext.removeLookupListener(getContextListener());
        curContext = null;
    }

    public Lookup getLookup () {
        // go with default activated Node strategy
        return null;
    }

    /************* non - public part ************/
    private void setNewContent (Collection newData) {
        Iterator it = newData.iterator();
        while(it.hasNext()) {
            Object o = it.next();
            if(o instanceof FileObject) {
                panelUI.setCurrentFileInEditor((FileObject)o);
            }
        }

        Node node = panelUI.resetModelToCurrentFile();
    }

    /** Accessor for listener to context */
    private LookupListener getContextListener () {
        if (contextL == null) {
            contextL = new ContextListener();
        }
        return contextL;
    }

    public void actionPerformed(CompilerEvent event) {
        if(event.isBuildEvent()) {
            panelUI.resetModelToCurrentFile();
        }
    }

    /** Listens to changes of context and triggers proper action */
    private class ContextListener implements LookupListener {
        public void resultChanged(LookupEvent ev) {
            Set set = ((Lookup.Result)ev.getSource()).allClasses();
            Collection data = ((Lookup.Result)ev.getSource()).allInstances();
            setNewContent(data);
        }
    }
}
