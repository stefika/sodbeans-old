 /*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.sodbeans.completion.api;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import javax.swing.JToolTip;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import javax.swing.text.StyledDocument;
import org.netbeans.api.editor.completion.Completion;
import org.netbeans.spi.editor.completion.CompletionItem;
import org.netbeans.spi.editor.completion.CompletionResultSet;
import org.netbeans.spi.editor.completion.CompletionTask;
import org.netbeans.spi.editor.completion.support.AsyncCompletionQuery;
import org.netbeans.spi.editor.completion.support.AsyncCompletionTask;
import org.netbeans.spi.editor.completion.support.CompletionUtilities;
import org.openide.util.Exceptions;
import org.sodbeans.compiler.api.CompilerCodeCompletionType;

/**
 *
 * @author Andreas Stefik and Susanna Siebert
 */
public class QuorumCompletionItem implements CompletionItem {

    private String displayText;
    private String rightDisplayText;
    private String completionText;
    private String documentationText;
    private int caretOffset;
    private int dotOffset;

    private CompilerCodeCompletionType type = CompilerCodeCompletionType.LOCAL_VARIABLE;
    private boolean isBaseClassMethod = false;
    
    public QuorumCompletionItem() {
    }

    public QuorumCompletionItem(String displayText, String rightDisplayText, String completionText, String documentationText, int dotOffset, int caretOffset) {
        this.displayText = displayText;
        this.rightDisplayText = rightDisplayText;
        this.completionText = completionText;
        this.documentationText = documentationText;
        this.dotOffset = dotOffset;
        this.caretOffset = caretOffset;
    }

    public void defaultAction(JTextComponent jTextComponent) {
        try {
            StyledDocument doc = (StyledDocument) jTextComponent.getDocument();
            //Here we remove the characters starting at the start offset
            //and ending at the point where the caret is currently found:
            doc.remove(getDotOffset(), getCaretOffset() - getDotOffset());
            doc.insertString(getDotOffset(), getCompletionText(), null);
            Completion.get().hideAll();
        } catch (BadLocationException ex) {
            Exceptions.printStackTrace(ex);
        }
    }

    public CompletionTask createToolTipTask() {
        return new AsyncCompletionTask(new AsyncCompletionQuery() {
            protected void query(CompletionResultSet completionResultSet, Document document, int i) {
                JToolTip toolTip = new JToolTip();
                toolTip.setTipText("Press Enter to insert \"" + getCompletionText() + "\"");
                completionResultSet.setToolTip(toolTip);
                completionResultSet.finish();
            }
        });
    }
    static int value = 0;
    static String previous = null;
    static String current = null;

    public void processKeyEvent(KeyEvent ke) {
        current = getDisplayText();
        if (previous == null) {
            previous = "";
        }

        if (!previous.equals(current)) {
            previous = current;
        } else { //if it's the second call, just go back to our original state
            //if you don't do this, the user may open the window
            //again, which would cause this not to speak.
            previous = null;
        }
    }

    public int getPreferredWidth(Graphics graphics, Font font) {
        return CompletionUtilities.getPreferredWidth(displayText, rightDisplayText, graphics, font);
    }

    public void render(Graphics g, Font defaultFont, Color defaultColor, Color backgroundColor, int width, int height, boolean selected) {
        Color color;
        if(selected) {
            color = Color.WHITE;
        }
        else {
            color = type.getColor();
        }
        
        if(isBaseClassMethod) {
            defaultFont = defaultFont.deriveFont(Font.BOLD);
        }
        
        CompletionUtilities.renderHtml(type.getImage(), getDisplayText(), getRightDisplayText(), g, defaultFont, color, width, height, selected);
    }

    public CompletionTask createDocumentationTask() {
        return new AsyncCompletionTask(new AsyncCompletionQuery() {
            protected void query(CompletionResultSet completionResultSet, Document document, int i) {
                completionResultSet.setDocumentation(new QuorumCompletionDocumentation(QuorumCompletionItem.this));
                completionResultSet.finish();
            }
        });
    }

    public boolean instantSubstitution(JTextComponent jtc) {
        return false;
    }

    public int getSortPriority() {
        if(type == null) {
            return 0;
        }
        return type.getPriority();
    }

    public CharSequence getSortText() {
        return getDisplayText();
    }

    public CharSequence getInsertPrefix() {
        return getDisplayText();
    }

    String getText() {
        return getDisplayText();
    }

    String getDocumentation() {
        return getDocumentationText();
    }

    /**
     * @return the displayText
     */
    public String getDisplayText() {
        return displayText;
    }

    /**
     * @param displayText the displayText to set
     */
    public void setDisplayText(String displayText) {
        this.displayText = displayText;
    }

    /**
     * @return the completionText
     */
    public String getCompletionText() {
        return completionText;
    }

    /**
     * @param completionText the completionText to set
     */
    public void setCompletionText(String completionText) {
        this.completionText = completionText;
    }

    /**
     * @return the documentationText
     */
    public String getDocumentationText() {
        return documentationText;
    }

    /**
     * @param documentationText the documentationText to set
     */
    public void setDocumentationText(String documentationText) {
        this.documentationText = documentationText;
    }

    /**
     * @return the caretOffset
     */
    public int getCaretOffset() {
        return caretOffset;
    }

    /**
     * @param caretOffset the caretOffset to set
     */
    public void setCaretOffset(int caretOffset) {
        this.caretOffset = caretOffset;
    }

    /**
     * @return the dotOffset
     */
    public int getDotOffset() {
        return dotOffset;
    }

    /**
     * @param dotOffset the dotOffset to set
     */
    public void setDotOffset(int dotOffset) {
        this.dotOffset = dotOffset;
    }

    private String getRightDisplayText() {
        return rightDisplayText;
    }

    void setRightDisplayText(String displayType) {
        this.rightDisplayText = displayType;
    }

    @Override
    public String toString() {
        return getDisplayText() + ": " + getDocumentationText().replaceAll("\\<.*?>", "");
    }

    /**
     * @return the type
     */
    public CompilerCodeCompletionType getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(CompilerCodeCompletionType type) {
        this.type = type;
    }
    
    /**
     * @return the isBaseClassMethod
     */
    public boolean isBaseClassMethod() {
        return isBaseClassMethod;
    }

    /**
     * @param isBaseClassMethod the isBaseClassMethod to set
     */
    public void setIsBaseClassMethod(boolean isBaseClassMethod) {
        this.isBaseClassMethod = isBaseClassMethod;
    }
}
