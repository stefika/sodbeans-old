/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sodbeans.completion.api;

import java.net.URL;
import javax.swing.Action;
import org.netbeans.spi.editor.completion.CompletionDocumentation;

/**
 *
 * @author Susanna
 */
public class QuorumCompletionDocumentation implements CompletionDocumentation {

    private QuorumCompletionItem item;

    public QuorumCompletionDocumentation(QuorumCompletionItem item) {
        this.item = item;
    }

    public String getText() {
        return item.getDocumentation();
    }

    public URL getURL() {
       return null;
    }

    public CompletionDocumentation resolveLink(String string) {
        return null;
    }

    public Action getGotoSourceAction() {
        return null;
    }

}
