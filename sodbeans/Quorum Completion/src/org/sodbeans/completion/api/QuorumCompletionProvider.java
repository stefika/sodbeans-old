/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sodbeans.completion.api;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Iterator;
import java.util.Set;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import org.netbeans.api.editor.mimelookup.MimeRegistration;
import javax.swing.text.Element;
import javax.swing.text.JTextComponent;
import javax.swing.text.StyledDocument;
import org.netbeans.modules.editor.NbEditorUtilities;
import org.netbeans.spi.editor.completion.CompletionProvider;
import org.netbeans.spi.editor.completion.CompletionResultSet;
import org.netbeans.spi.editor.completion.CompletionTask;
import org.netbeans.spi.editor.completion.support.AsyncCompletionQuery;
import org.netbeans.spi.editor.completion.support.AsyncCompletionTask;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.sodbeans.compiler.api.CompilerCodeCompletionRequest;
import org.sodbeans.compiler.api.CompilerCodeCompletionResult;
import org.openide.text.Line;
import org.sodbeans.compiler.api.CompilerCodeCompletionResults;
import org.sodbeans.phonemic.TextToSpeechFactory;
import org.sodbeans.phonemic.tts.TextToSpeech;

/**
 *
 * @author Andreas Stefik and Susanna Siebert
 */
@MimeRegistration(mimeType = "text/x-quorum", service = CompletionProvider.class)
public class QuorumCompletionProvider implements CompletionProvider{
    
    private org.sodbeans.compiler.api.Compiler compiler = Lookup.getDefault().lookup(org.sodbeans.compiler.api.Compiler.class);
    private TextToSpeech speech = TextToSpeechFactory.getDefaultTextToSpeech();
    
    public QuorumCompletionProvider() {
    }

    public CompletionTask createTask(int type, final JTextComponent jtc) {
        if (type != CompletionProvider.COMPLETION_QUERY_TYPE) {
            return null;
        }
        
        jtc.addPropertyChangeListener(new PropertyChangeListener() {
            public void propertyChange(PropertyChangeEvent evt) {
                if(evt.getPropertyName().equals("completion-active")) {
                    Object val = evt.getNewValue();
                    if(val instanceof Boolean) {
                        Boolean b = (Boolean) val;
                        boolean b2 = b;
                        if(!b2) { //the user is closing the window
                            //remove the event
                            jtc.removePropertyChangeListener(this);
                            speech.stop();
                        }
                    }
                }
            }
        });
        
        return new AsyncCompletionTask(new AsyncCompletionQuery() {

            @Override
            protected void query(CompletionResultSet completionResultSet, Document document, int caretOffset) {
                String filter = "";
                try {
                    final StyledDocument styled = (StyledDocument) document;
                    final int lineStartOffset = getRowFirst(styled, caretOffset);

                    Line myLine = NbEditorUtilities.getLine(document, lineStartOffset, false);
                    final String line = styled.getText(lineStartOffset, caretOffset - lineStartOffset);

                    Object titleProperty = styled.getProperty("title");
                    String title = "";
                    if(titleProperty != null) {
                        title = titleProperty.toString();
                    }

                    if(compiler != null) {
                        CompilerCodeCompletionRequest request = new CompilerCodeCompletionRequest();
                        request.setLine(line);
                        request.setFileKey(title);
                        request.setStartOffset(caretOffset - lineStartOffset);
                        request.setLine(myLine.getText());
                        request.setLineNumber(myLine.getLineNumber());
                        CompilerCodeCompletionResults completionResults = compiler.requestCodeCompletionResult(request);
                        Set<CompilerCodeCompletionResult> results = completionResults.getResults();
                        if(results.size() > 0) {
                            filter = completionResults.getFilter();
                        }
                        Iterator<CompilerCodeCompletionResult> iterator = results.iterator();
                        while(iterator.hasNext()) {
                            CompilerCodeCompletionResult next = iterator.next();
                            QuorumCompletionItem res = new QuorumCompletionItem();
                            res.setIsBaseClassMethod(next.isBaseClassMethod());
                            res.setType(next.getType());
                            res.setCaretOffset(caretOffset);
                            res.setDotOffset(caretOffset - filter.length());
                            res.setDocumentationText(next.getDocumentation());
                            res.setCompletionText(next.getCompletion());
                            res.setDisplayText(next.getDisplayName());
                            res.setRightDisplayText(next.getDisplayType());
                            if(!next.getDisplayName().equals("") && next.getDisplayName().startsWith(filter)) {
                                completionResultSet.addItem(res);
                            }
                        }                
                    }

                    } catch (BadLocationException ex) {
                        Exceptions.printStackTrace(ex);
                    }
                completionResultSet.finish();
            }

        }, jtc);
    }

    public int getAutoQueryTypes(JTextComponent jtc, String string) {
        if(string.length() > 0) {
            if(string.startsWith(":") || string.startsWith(".")) {
                return CompletionProvider.COMPLETION_QUERY_TYPE;
            }
        }
        return 0;
    }

    static int getRowFirstNonWhite(StyledDocument doc, int offset)
    throws BadLocationException {
        Element lineElement = doc.getParagraphElement(offset);
        int start = lineElement.getStartOffset();
        while (start + 1 < lineElement.getEndOffset()) {
            try {
                if (doc.getText(start, 1).charAt(0) != ' ') {
                    break;
                }
            } catch (BadLocationException ex) {
                throw (BadLocationException)new BadLocationException(
                        "calling getText(" + start + ", " + (start + 1) +
                        ") on doc of length: " + doc.getLength(), start
                        ).initCause(ex);
            }
            start++;
        }
        return start;
    }
    
    static int getRowFirst(StyledDocument doc, int offset)
    throws BadLocationException {
        Element lineElement = doc.getParagraphElement(offset);
        int start = lineElement.getStartOffset();
        return start;
    }

    static int getRowIndent(StyledDocument doc, int offset)
    throws BadLocationException {
        Element lineElement = doc.getParagraphElement(offset);
        return lineElement.getStartOffset();
    }

    static int indexOfWhite(char[] line){
        int i = line.length;
        while(--i > -1){
            final char c = line[i];
            if(Character.isWhitespace(c)){
                return i;
            }
        }
        return -1;
    }

    private String insertWhitespace(String text, int whitespace)
    {
        int index = 0;
        String WhiteSpace = "";
        for(int i = 0; i < whitespace; i++)
            WhiteSpace = WhiteSpace + " ";

        while(index < text.length() && index != -1)
        {
           index = text.indexOf("\r", index);
           
           if (index > -1)
           {
               text = text.substring(0, index + 1) + WhiteSpace + text.substring(index + 1, text.length());
               index++;
           }

           
        }
        return text;
    }

}
