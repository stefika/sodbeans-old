/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sodbeans.controller.impl.readers;

/**
 *  Provides a default reading when the component is not directly supported.
 * 
 * @author Andreas Stefik
 */
public class DefaultReader extends AbstractScreenReader{

    public void setObject(Object object) {
    }
}
