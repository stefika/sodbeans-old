/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * Portions Copyrighted 2007 Sun Microsystems, Inc.
 */

package org.sodbeans.matching;

import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import org.antlr.runtime.ANTLRStringStream;
import org.antlr.runtime.CommonTokenStream;
import org.antlr.runtime.RecognitionException;
import org.netbeans.spi.editor.bracesmatching.BracesMatcher;
import org.netbeans.spi.editor.bracesmatching.MatcherContext;
import org.openide.util.Exceptions;
import org.sodbeans.matching.parser.QuorumCodeMatcher;
import org.sodbeans.matching.parser.QuorumLexer;
import org.sodbeans.matching.parser.QuorumParser;
import org.sodbeans.matching.parser.MatchKeyword;



/**
 * Matches "braces" for the quorum programming language.
 * 
 * @author Andreas Stefik and Susanna Siebert
 */
public class QuorumEndMatcher implements BracesMatcher{
    private static QuorumCodeMatcher matcher = new QuorumCodeMatcher();
    private final MatcherContext context;
    private final String [] matchingPairs;
    private final int lowerBound;
    private final int upperBound;

    private int matchID;
    private int originStartOffset;
    private int originEndOffset;
    private int matchStartOffset;
    private int matchEndOffset;
    private boolean backward;

    private String currentString = "";
    
    public QuorumEndMatcher(MatcherContext context, int lowerBound, int upperBound, String [] matchingPairs) {
        this.context = context;
        currentString = getDocumentString(context);
        this.lowerBound = lowerBound == -1 ? Integer.MIN_VALUE : lowerBound;
        this.upperBound = upperBound == -1 ? Integer.MAX_VALUE : upperBound;

        
        assert matchingPairs.length % 2 == 0 : "The matchingPairs parameter must contain even number of characters."; //NOI18N
        this.matchingPairs = matchingPairs;
    }
    
    private String getDocumentString(MatcherContext context) {
        String result = "";
        try {
            result = context.getDocument().getText(0, context.getDocument().getLength());
        } catch (BadLocationException ex) {
            Exceptions.printStackTrace(ex);
        }
        return result;
    }

    @Override
    public int [] findOrigin() throws BadLocationException {
        int searchOffset = context.getSearchOffset();
        final String matchingThread = "EditorBracesMatching";
        if(Thread.currentThread().getName().equals(matchingThread)) {
            parse(currentString);
        }
        MatchKeyword origin = matcher.findOrigin(searchOffset);
        if(origin == null) {
            return null;
        }
        int[] findOrigin = origin.translate();
        if (findOrigin != null) {
            matchID = findOrigin[0];
            originStartOffset = findOrigin[1];
            originEndOffset = findOrigin[2];
            backward = findOrigin[3] == 0 ? true : false;
            return new int [] {originStartOffset, originEndOffset};
        }
        else {
            return null;
        }
    }
    
    private synchronized void parse(String text) {
        try {
            ANTLRStringStream ss = new ANTLRStringStream(text);
            QuorumLexer lexer = new QuorumLexer(ss);
            lexer.getNumberOfSyntaxErrors();
            CommonTokenStream tokens = new CommonTokenStream(lexer);
            
            //do a parse to check for matches
            QuorumParser parser = new QuorumParser(tokens);
            parser.setMatcher(matcher);
            matcher.clear();
            parser.start();
        } catch (Exception ex) { //ignore syntax errors
            int a = 0;
        }
    }

    @Override
    public int [] findMatches() throws BadLocationException {
        Document document = context.getDocument();
        MatchKeyword match = matcher.findMatch(matchID, backward);
        if(match == null) {
            return null;
        }
        int[] findMatch = match.translate();
        
        if (findMatch != null && findMatch[2] <= document.getEndPosition().getOffset()) {
            matchStartOffset = findMatch[1];
            matchEndOffset = findMatch[2];
            return new int [] { matchStartOffset, matchEndOffset } ;
        }

        return null;
    }
}
