/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.sodbeans.matching.parser;

import java.util.HashMap;
import java.util.Map.Entry;
import java.util.NavigableMap;
import java.util.TreeMap;

/**
 *
 * @author Andreas Stefik
 */
public class FileMatcher {

    private String fileKey;
    private NavigableMap<Integer, MatchKeyword> matchPrefix;
    private NavigableMap<Integer, MatchKeyword> matchSuffix;
    private HashMap<Integer, MatchKeyword> matchPrefixByID;
    private HashMap<Integer, MatchKeyword> matchSuffixByID;
    
    public FileMatcher(String fileKey) {
        this.fileKey = fileKey;
        matchPrefix = new TreeMap<Integer, MatchKeyword>();
        matchSuffix = new TreeMap<Integer, MatchKeyword>();
        matchPrefixByID = new HashMap<Integer, MatchKeyword>();
        matchSuffixByID = new HashMap<Integer, MatchKeyword>();
    }

    public FileMatcher() {
        this.fileKey = "";
        matchPrefix = new TreeMap<Integer, MatchKeyword>();
        matchSuffix = new TreeMap<Integer, MatchKeyword>();
        matchPrefixByID = new HashMap<Integer, MatchKeyword>();
        matchSuffixByID = new HashMap<Integer, MatchKeyword>();
    }

    public void add(MatchKeyword matchKeyword) {
        if (matchKeyword.getIsPrefix()){
            matchPrefix.put(matchKeyword.getStartOffset(), matchKeyword);
            matchPrefixByID.put(matchKeyword.getMatchID(), matchKeyword);
        }
        else {
            matchSuffix.put(matchKeyword.getStartOffset(), matchKeyword);
            matchSuffixByID.put(matchKeyword.getMatchID(), matchKeyword);
        }
    }

    public MatchKeyword findOrigin(int searchOffset){
        Entry entry = matchPrefix.floorEntry(searchOffset);
        if (entry != null) {
            MatchKeyword keyword = (MatchKeyword)entry.getValue();
            if (searchOffset <= keyword.getEndOffset()){
                return keyword;
            }
            else {
                entry = matchSuffix.floorEntry(searchOffset);
                if (entry != null) {
                    keyword = (MatchKeyword)entry.getValue();
                    if (searchOffset <= keyword.getEndOffset()){
                        return keyword;
                    }
                }
            }
        }
        return null;
    }

    public MatchKeyword findMatch(int matchID, boolean backward){
        if (backward) {
            MatchKeyword keyword = matchPrefixByID.get(matchID);
            return keyword;
        }
        else {
            MatchKeyword keyword = matchSuffixByID.get(matchID);
            return keyword;
        }

    }

    public void clear() {
        matchPrefix.clear();
        matchSuffix.clear();
        matchPrefixByID.clear();
        matchSuffixByID.clear();
    }

    public void remove(int matchID) {
        matchPrefix.remove(matchID);
        matchSuffix.remove(matchID);
        matchPrefixByID.remove(matchID);
        matchSuffixByID.remove(matchID);
    }

    /**
     * @return the fileKey
     */
    public String getFileKey() {
        return fileKey;
    }

    /**
     * @param fileKey the fileKey to set
     */
    public void setFileKey(String fileKey) {
        this.fileKey = fileKey;
    }
}
