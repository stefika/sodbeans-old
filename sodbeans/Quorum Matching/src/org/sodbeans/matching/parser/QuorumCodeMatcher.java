/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.sodbeans.matching.parser;

/**
 *
 * @author Andreas Stefik
 */
public class QuorumCodeMatcher{
    private FileMatcher currentFileMatcher = null;
    private boolean isRun = true;

    public QuorumCodeMatcher() {
        currentFileMatcher = new FileMatcher();
    }

    public void addMatch(MatchKeyword prefix, MatchKeyword suffix){
        if(currentFileMatcher != null) {
            if (suffix != null) {
                currentFileMatcher.add(prefix);
            }
            if (prefix != null) {
                currentFileMatcher.add(suffix);
            }
        }
    }

    public void setIsRun(boolean isRun){
        this.isRun = isRun;
    }

    public MatchKeyword findOrigin(int searchOffset) {
        return currentFileMatcher.findOrigin(searchOffset);
    }

    public MatchKeyword findMatch(int matchID, boolean backward) {
        return currentFileMatcher.findMatch(matchID, backward);
    }

    /**
     * Empties the files matcher
     */
    public void clear() {
        if (currentFileMatcher != null) {
            currentFileMatcher.clear();
        }
        this.setIsRun(true);
    }
}
