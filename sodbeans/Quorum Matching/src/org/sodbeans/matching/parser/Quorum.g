grammar Quorum;

options {
	output=AST;
	ASTLabelType = CommonTree;
}

tokens { //these are imaginary tokens --- they are inserted to help processing. More info in antlr book
	FUNCTION_CALL;
	FUNCTION_CALL_PARENT;
	FUNCTION_CALL_THIS;
	FUNCTION_EXPRESSION_LIST;
	SOLO_FUNCTION_CALL;
	SOLO_FUNCTION_CALL_PARENT;
	SOLO_FUNCTION_CALL_THIS;
	QUALIFIED_NAME;
	EXPRESSION_STATEMENT;
	STATEMENT_LIST;
	CONSTRUCTOR;
	FPARAM;
	UNARY_NOT;
	ELSE_IF_STATEMENT;
	FINAL_ELSE;
	PAREN_WRAPPED_EXPRESSION;
	ROOT_EXPRESSION;
	QUALIFIED_SOLO_EXPRESSION;
	QUALIFIED_SOLO_EXPRESSION_SELECTOR;
	QUALIFIED_SOLO_PARENT_EXPRESSON;
	GENERIC;
}

@header {
package org.sodbeans.matching.parser;
}
@lexer::header {package org.sodbeans.matching.parser;
}

@lexer::members {
	public void reportError(RecognitionException e) {
	}
}

@members{
	QuorumCodeMatcher matcher;
	public void setMatcher(QuorumCodeMatcher match) {
		matcher = match;
	}
	
	public QuorumCodeMatcher getMatcher() {
		return matcher;
	}
	public void reportError(RecognitionException e) {
    	}
}

start	:
		(package_rule reference+ 
	|	reference+ package_rule
	|	package_rule
	|	reference+
	|	)
	class_declaration  EOF//Declaring a class name
	;
	
package_rule :	PACKAGE_NAME qn=qualified_name
	;
	
reference 
	:	
	USE qn = qualified_name 
	;


class_declaration 				
:	
	(
	CLASS ID  
	generic_declaration?
	inherit_stmnts?       
	class_stmnts*
	END
	{
		MatchKeyword keyword1 = new MatchKeyword();
		keyword1.setMatchID(((CommonToken)$CLASS).getStartIndex());
  		keyword1.setStartOffset(((CommonToken)$CLASS).getStartIndex());
  		keyword1.setEndOffset(((CommonToken)$CLASS).getStopIndex() + 1);
  		keyword1.setIsPrefix(true); 
  		MatchKeyword keyword2 = new MatchKeyword();
  		keyword2.setMatchID(((CommonToken)$CLASS).getStartIndex());
  		keyword2.setStartOffset(((CommonToken)$END).getStartIndex());
  		keyword2.setEndOffset(((CommonToken)$END).getStopIndex() + 1);
  		keyword2.setIsPrefix(false); 
  		matcher.addMatch(keyword1, keyword2);
	}
	)
	|
	no_class_stmnts
	;
no_class_stmnts
	:	
	statement+

	|(access_modifier? method_declaration)+ //no special processing needed for this case
	;
inherit_stmnts
	:	INHERITS qn=qualified_name genericList=generic_statement? 
	 (COMMA qn=qualified_name genericList=generic_statement?
	 )*
	;
access_modifier
	:	PUBLIC
	|	PRIVATE
	;	
class_stmnts
	:
	assignment_statement
	|	access_modifier?
	 	method_declaration
	;
method_declaration
	:	ACTION ID 
	//now parameters, which are optional
	(LEFT_PAREN (formal_parameter (COMMA formal_parameter)*)? RIGHT_PAREN)?
	(RETURNS return_type = assignment_declaration )?
		block
	END 
	{
		MatchKeyword keyword1 = new MatchKeyword();
		keyword1.setMatchID(((CommonToken)$ACTION).getStartIndex());
       		keyword1.setStartOffset(((CommonToken)$ACTION).getStartIndex());
       		keyword1.setEndOffset(((CommonToken)$ACTION).getStopIndex() + 1);
       		keyword1.setIsPrefix(true); 
       		MatchKeyword keyword2 = new MatchKeyword();
        	keyword2.setMatchID(((CommonToken)$ACTION).getStartIndex());
       		keyword2.setStartOffset(((CommonToken)$END).getStartIndex());
       		keyword2.setEndOffset(((CommonToken)$END).getStopIndex() + 1);
       		keyword2.setIsPrefix(false); 
       		matcher.addMatch(keyword1, keyword2);
	}
	|	BLUEPRINT ACTION ID 

	//now parameters, which are optional
	(LEFT_PAREN (formal_parameter (COMMA formal_parameter)*)? RIGHT_PAREN)?
	(RETURNS assignment_declaration)?
	|	NATIVE ACTION ID 
	//now parameters, which are optional
	(LEFT_PAREN (formal_parameter (COMMA formal_parameter)*)? RIGHT_PAREN)?
	(RETURNS return_type = assignment_declaration)?
	| ON CREATE
	block END
	{
		MatchKeyword keyword1 = new MatchKeyword();
		keyword1.setMatchID(((CommonToken)$ON_CREATE).getStartIndex());
      		keyword1.setStartOffset(((CommonToken)$ON_CREATE).getStartIndex());
      		keyword1.setEndOffset(((CommonToken)$ON_CREATE).getStopIndex() + 1);
      		keyword1.setIsPrefix(true); 
      		MatchKeyword keyword2 = new MatchKeyword();
       	 	keyword2.setMatchID(((CommonToken)$ON_CREATE).getStartIndex());
      		keyword2.setStartOffset(((CommonToken)$END).getStartIndex());
      		keyword2.setEndOffset(((CommonToken)$END).getStopIndex() + 1);
      		keyword2.setIsPrefix(false); 
      		matcher.addMatch(keyword1, keyword2);
	}
	;
formal_parameter
	:	assignment_declaration ID
	;
	
qualified_name
	:	ID (PERIOD ID)*
	;
block 	:	statement*
	;

statement:
		solo_method_call
	|	if_statement
	|	assignment_statement
	|	loop_statement
	|	return_statement
	|	print_statement
	|	speak_statement
	|	check_statement
	|	alert_statement
	;

solo_method_call 
	:
	qualified_name (COLON ID)? LEFT_PAREN (expression (COMMA expression)*)? RIGHT_PAREN ->
			^(SOLO_FUNCTION_CALL qualified_name (COLON ID)? LEFT_PAREN (expression (COMMA expression)*)? RIGHT_PAREN)
	|	PARENT COLON qualified_name COLON ID LEFT_PAREN (expression (COMMA expression)*)? RIGHT_PAREN ->
			^(SOLO_FUNCTION_CALL_PARENT PARENT COLON qualified_name COLON ID LEFT_PAREN (expression (COMMA expression)*)? RIGHT_PAREN)
	|	ME COLON qualified_name (COLON ID)? LEFT_PAREN (expression (COMMA expression)*)? RIGHT_PAREN ->
			^(SOLO_FUNCTION_CALL_THIS ME COLON qualified_name (COLON ID)? LEFT_PAREN (expression (COMMA expression)*)? RIGHT_PAREN)
	;
	
alert_statement 
	:	ALERT LEFT_PAREN root_expression  RIGHT_PAREN
	-> ^(ALERT LEFT_PAREN root_expression RIGHT_PAREN)
	;
	
check_statement 
@init{
	MatchKeyword keyword1;
	MatchKeyword keyword2;
}
	:   check_start = CHECK block
	{
		keyword1 = new MatchKeyword();
		keyword1.setMatchID(((CommonToken)$check_start).getStartIndex());
       		keyword1.setStartOffset(((CommonToken)$check_start).getStartIndex());
       		keyword1.setEndOffset(((CommonToken)$check_start).getStopIndex() + 1);
       		keyword1.setIsPrefix(true); 
       		keyword2 = new MatchKeyword();
         	keyword2.setMatchID(((CommonToken)$check_start).getStartIndex());
       		keyword2.setIsPrefix(false); 

	}
	    (  (detect_start=DETECT detect_parameter block 
	{
		keyword2.setStartOffset(((CommonToken)$detect_start).getStartIndex());
		keyword2.setEndOffset(((CommonToken)$detect_start).getStopIndex() + 1);
	        matcher.addMatch(keyword1, keyword2);
	       		
		keyword1 = new MatchKeyword();
		keyword1.setMatchID(((CommonToken)$detect_start).getStartIndex());
       		keyword1.setStartOffset(((CommonToken)$detect_start).getStartIndex());
       		keyword1.setEndOffset(((CommonToken)$detect_start).getStopIndex() + 1);
       		keyword1.setIsPrefix(true); 
       		keyword2 = new MatchKeyword();
         	keyword2.setMatchID(((CommonToken)$detect_start).getStartIndex());
       		keyword2.setIsPrefix(false); 
       		
	}
	    )+ 
	    (always_start=ALWAYS block 
	{
	       	keyword2.setStartOffset(((CommonToken)$always_start).getStartIndex());
       		keyword2.setEndOffset(((CommonToken)$always_start).getStopIndex() + 1);
		matcher.addMatch(keyword1, keyword2);
		
		keyword1 = new MatchKeyword();
		keyword1.setMatchID(((CommonToken)$always_start).getStartIndex());
       		keyword1.setStartOffset(((CommonToken)$always_start).getStartIndex());
       		keyword1.setEndOffset(((CommonToken)$always_start).getStopIndex() + 1);
       		keyword1.setIsPrefix(true); 
       		keyword2 = new MatchKeyword();
         	keyword2.setMatchID(((CommonToken)$always_start).getStartIndex());
       		keyword2.setIsPrefix(false); 
       		
	}
	    )? 
	    |   always_start_2=ALWAYS block
	{
		keyword2.setStartOffset(((CommonToken)$always_start_2).getStartIndex());
       		keyword2.setEndOffset(((CommonToken)$always_start_2).getStopIndex() + 1);
		matcher.addMatch(keyword1, keyword2);
		
		keyword1 = new MatchKeyword();
		keyword1.setMatchID(((CommonToken)$always_start_2).getStartIndex());
       		keyword1.setStartOffset(((CommonToken)$always_start_2).getStartIndex());
       		keyword1.setEndOffset(((CommonToken)$always_start_2).getStopIndex() + 1);
       		keyword1.setIsPrefix(true); 
       		keyword2 = new MatchKeyword();
         	keyword2.setMatchID(((CommonToken)$always_start_2).getStartIndex());
       		keyword2.setIsPrefix(false); 
	}
	    )   
	    end=END
	{
	        keyword2.setStartOffset(((CommonToken)$end).getStartIndex());
       		keyword2.setEndOffset(((CommonToken)$end).getStopIndex() + 1);
		matcher.addMatch(keyword1, keyword2);
	} 
	 ;	
    
detect_parameter
	: 	ID 
	(OF_TYPE qualified_name(OR qualified_name)*)?
	;
print_statement 
	:	OUTPUT root_expression
	;

speak_statement 
	:	SAY root_expression
	;

return_statement
	:	RETURN ( root_expression | NOW)
	;
	
generic_declaration 
	:	LESS ID (COMMA ID)* GREATER
	;
generic_statement
	:	LESS 
	assignment_declaration
	(COMMA assignment_declaration
	)* GREATER
	;
class_type
	:	qualified_name
	;
assignment_declaration
	:	qualified_name generic_statement?
	
	|	INTEGER_KEYWORD
	|	NUMBER_KEYWORD
	|	TEXT
	|	BOOLEAN_KEYWORD;
assignment_statement
	:			
		(selector COLON)? ID assign_right_hand_side
		
	|	qualified_name (COLON PARENT COLON qualified_name)? COLON ID assign_right_hand_side
	|	access_modifier? CONSTANT?  assignment_declaration name = ID rhs = assign_right_hand_side?				
	;
assign_right_hand_side
	:	
		(EQUALITY root_expression)
	;

	
if_statement	
@init {
	MatchKeyword keyword1;
	MatchKeyword keyword2;
	String startType;
}
	:
	firstif = IF root_expression block  
	{
		keyword1 = new MatchKeyword();
		keyword1.setMatchID(((CommonToken)$firstif).getStartIndex());
       		keyword1.setStartOffset(((CommonToken)$firstif).getStartIndex());
       		keyword1.setEndOffset(((CommonToken)$firstif).getStopIndex() + 1);
       		keyword1.setIsPrefix(true); 
       		keyword2 = new MatchKeyword();
       		startType = "if";
	}
	((firstelse = ELSE_IF root_expression block 
	{
	       	if(startType.equals("if")){
	       		keyword2 = new MatchKeyword();
         		keyword2.setMatchID(((CommonToken)$firstif).getStartIndex());
         	}else{
         		int oldStartOffset = keyword2.getStartOffset();
         		keyword2 = new MatchKeyword();
         		keyword2.setMatchID(oldStartOffset);
         	}
       		keyword2.setStartOffset(((CommonToken)$firstelse).getStartIndex());
       		keyword2.setEndOffset(((CommonToken)$firstelse).getStopIndex() + 1);
       		keyword2.setIsPrefix(false); 
       		matcher.addMatch(keyword1, keyword2);
       		
       		startType = "elseif";
       		
		keyword1 = new MatchKeyword();
		keyword1.setMatchID(((CommonToken)$firstelse).getStartIndex());
       		keyword1.setStartOffset(((CommonToken)$firstelse).getStartIndex());
       		keyword1.setEndOffset(((CommonToken)$firstelse).getStopIndex() + 1);
       		keyword1.setIsPrefix(true); 
	}
	))*  //else if blocks
	((secondelse = ELSE block
	{
		keyword2 = new MatchKeyword();
	       	if(startType.equals("if")){
         		keyword2.setMatchID(((CommonToken)$firstif).getStartIndex());
         	}else {
         		keyword2.setMatchID(((CommonToken)$firstelse).getStartIndex());
         	}
       		keyword2.setStartOffset(((CommonToken)$secondelse).getStartIndex());
       		keyword2.setEndOffset(((CommonToken)$secondelse).getStopIndex() + 1);
       		keyword2.setIsPrefix(false); 
       		matcher.addMatch(keyword1, keyword2);
       		
       		startType = "else";
       		
	 	keyword1 = new MatchKeyword();
		keyword1.setMatchID(((CommonToken)$secondelse).getStartIndex());
       		keyword1.setStartOffset(((CommonToken)$secondelse).getStartIndex());
       		keyword1.setEndOffset(((CommonToken)$secondelse).getStopIndex() + 1);
       		keyword1.setIsPrefix(true); 
	}
	) )? 
	end=END
	{
		keyword2 = new MatchKeyword();
	       	if(startType.equals("if")){
         		keyword2.setMatchID(((CommonToken)$firstif).getStartIndex());
         	}else if(startType.equals("elseif")){
         		keyword2.setMatchID(((CommonToken)$firstelse).getStartIndex());
         	}else{
         		keyword2.setMatchID(((CommonToken)$secondelse).getStartIndex());
         	}
       		keyword2.setStartOffset(((CommonToken)$end).getStartIndex());
       		keyword2.setEndOffset(((CommonToken)$end).getStopIndex() + 1);
       		keyword2.setIsPrefix(false); 
       		matcher.addMatch(keyword1, keyword2);
	}
	;

loop_statement
	:	
		REPEAT (// (OVER ID)
		//|	((FROM range))
		//|	
		(root_expression TIMES)
		|	((WHILE | UNTIL) root_expression))  block END
		{
			MatchKeyword keyword1 = new MatchKeyword();
			keyword1.setMatchID(((CommonToken)$REPEAT).getStartIndex());
       			keyword1.setStartOffset(((CommonToken)$REPEAT).getStartIndex());
       			keyword1.setEndOffset(((CommonToken)$REPEAT).getStopIndex() + 1);
       			keyword1.setIsPrefix(true); 
       			MatchKeyword keyword2 = new MatchKeyword();
        	 	keyword2.setMatchID(((CommonToken)$REPEAT).getStartIndex());
       			keyword2.setStartOffset(((CommonToken)$END).getStartIndex());
       			keyword2.setEndOffset(((CommonToken)$END).getStopIndex() + 1);
       			keyword2.setIsPrefix(false); 
       			matcher.addMatch(keyword1, keyword2);
       		}
		;

//range	:	(root_expression) TO (root_expression) -> ^(TO root_expression root_expression);

selector	
	:	PARENT COLON qualified_name
	|	ME
	;

root_expression
	:	expression -> ^(ROOT_EXPRESSION expression)
	;
	
expression
	:	or
	;
	
or 	:	and (OR ^ and)*
	;

and 	:	equality (AND ^ equality)*
	;	
equality:	isa_operation ((EQUALITY ^ | NOTEQUALS ^) isa_operation)*
	;
isa_operation
	:	comparison (INHERITS ^ class_type)?
	;
comparison:	add ((GREATER ^| GREATER_EQUAL ^| LESS ^| LESS_EQUAL^) add)*
	;
	

add	:	multiply ((PLUS ^| MINUS^) multiply)*
	;
		
multiply:	combo_expression ((MULTIPLY ^| DIVIDE ^|MODULO^) combo_expression)*
	;
	
combo_expression 
	:	NOT atom -> ^(UNARY_NOT NOT atom)
	|	CAST LEFT_PAREN assignment_declaration COMMA expression RIGHT_PAREN
	|	atom
	;
		
atom 	: 
	qualified_name (COLON ID)? -> ^(QUALIFIED_SOLO_EXPRESSION qualified_name (COLON ID)?)
	|	qualified_name COLON PARENT COLON qualified_name COLON ID -> ^(QUALIFIED_SOLO_PARENT_EXPRESSON qualified_name COLON PARENT COLON qualified_name COLON ID)
	|	qualified_name (COLON ID)? LEFT_PAREN function_expression_list RIGHT_PAREN ->
			^(FUNCTION_CALL qualified_name (COLON ID)? LEFT_PAREN function_expression_list RIGHT_PAREN)
	|	selector COLON qualified_name -> 
			^(QUALIFIED_SOLO_EXPRESSION_SELECTOR selector COLON qualified_name)
	// sdf s |	qualified_name COLON PARENT COLON qualified_name ->
	//sdfsd		^(QUALIFIED_SOLO_PARENT_EXPRESSON qualified_name COLON PARENT COLON qualified_name)
	|	PARENT COLON qualified_name COLON ID LEFT_PAREN function_expression_list RIGHT_PAREN ->
			^(FUNCTION_CALL_PARENT PARENT COLON qualified_name COLON ID LEFT_PAREN function_expression_list RIGHT_PAREN)
	|	ME COLON qualified_name (COLON ID)? LEFT_PAREN function_expression_list RIGHT_PAREN ->
			^(FUNCTION_CALL_THIS ME COLON qualified_name (COLON ID)? LEFT_PAREN function_expression_list RIGHT_PAREN)
	| (MINUS)? INT
	| BOOLEAN
	| (MINUS)? DECIMAL 
	| STRING
	| QUOTE
	| NULL
	| ME
		| INPUT LEFT_PAREN expression RIGHT_PAREN
	| LEFT_PAREN expression RIGHT_PAREN -> ^(expression)
	;

function_expression_list 
	:
	(expression (COMMA expression)*)?	
		-> ^(FUNCTION_EXPRESSION_LIST expression*)
	;
OUTPUT	:	'output';
ON	:	'on';
DESTROY	:	'destroy';
CREATE	:	'create';
QUOTE	:	'quote';
CONSTANT	:	'constant';
ELSE_IF :	'elseif';
ME	:	'me';
UNTIL	:	'until';
OF_TYPE :	'of type';
PUBLIC	:	'public';
PRIVATE	:	'private';	
ALERT	:	'alert';
DETECT	:	'detect';
ALWAYS	:	'always';
CHECK	:	'check';
PARENT	:	'parent';
BLUEPRINT :	'blueprint';
NATIVE :	'system';
INHERITS :	'is a';
CAST	:	'cast';
INPUT	:	'input';
SAY	:	'say';
NOW	:	'now';
WHILE	:	'while';
PACKAGE_NAME :	'package';
// FROM	:	'from';
TIMES	:	'times';
REPEAT	:	'repeat';
OVER	:	'over';
ELSE 	:	'else';
RETURNS :	'returns';
RETURN 	:	'return';
AND	:	'and';
OR 	:	'or';
// TO	:	'to';
NULL	:	'undefined';
ACTION 
	:	'action'
	;
COLON	:	':'
	;
INTEGER_KEYWORD	
	:	'integer';
NUMBER_KEYWORD	
	:	'number';
TEXT	
	:	'text';
BOOLEAN_KEYWORD	
	:	'boolean';	
USE 	:	'use'
	;
NOT	:	'not' | 'Not';
NOTEQUALS
	:	('n' | 'N' ) 'ot=';
PERIOD	:	'.';
COMMA	:	',';
EQUALITY:	'=';
GREATER	:	'>';
GREATER_EQUAL
	:	'>=';
LESS	:	'<';
LESS_EQUAL 
	:	'<=';
PLUS	:	'+';
MINUS	:	'-';
MULTIPLY:	'*';
DIVIDE	:	'/';
MODULO	:	'mod';
LEFT_SQR_BRACE
	:	'[';
RIGHT_SQR_BRACE
	:	']';	
LEFT_PAREN
	:	'(';
RIGHT_PAREN
	:	')';
DOUBLE_QUOTE
	:	'"';
IF	:	'if';
END	:	'end';
CLASS	:	'class';
BOOLEAN	:	'true' | 'false';
INT 	:	'0'..'9'+;
DECIMAL	:	'0'..'9'+ (PERIOD ('0'..'9')*)?;	
ID 	: 	('a'..'z'|'A'..'Z')('a'..'z'|'A'..'Z'|'0'..'9' | '_')*;
STRING	:	DOUBLE_QUOTE ~(DOUBLE_QUOTE)* DOUBLE_QUOTE;


NEWLINE	:	 '\r'?'\n' {$channel = HIDDEN;};
WS	:	(' '|'\t'|'\n'|'\r')+ {$channel = HIDDEN;};

COMMENTS
    :   '//' ~('\n'|'\r')* (('\r'? '\n') | EOF) {$channel=HIDDEN;}
    |   '/*' ( options {greedy=false;} : . )* '*/' {$channel=HIDDEN_DOCUMENTATION;}
    ;

