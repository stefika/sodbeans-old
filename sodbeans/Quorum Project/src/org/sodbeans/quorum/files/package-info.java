/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
@TemplateRegistrations({
        @TemplateRegistration(
                folder = "Quorum", 
                iconBase="org/sodbeans/quorum/files/QuorumFile.png",
                displayName = "Quorum Class", 
                description = "QuorumClass.html",
                content = "Main.quorum",
                scriptEngine = "freemarker"),
        
        @TemplateRegistration(
                folder = "Quorum", 
                iconBase="org/sodbeans/quorum/files/QuorumFile.png",
                displayName = "Empty Quorum File", 
                description = "EmptyQuorumFile.html",
                content = "Empty.quorum",
                scriptEngine = "freemarker")

    })
package org.sodbeans.quorum.files;

import org.netbeans.api.templates.TemplateRegistration;
import org.netbeans.api.templates.TemplateRegistrations;
