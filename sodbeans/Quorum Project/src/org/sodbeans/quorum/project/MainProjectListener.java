/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sodbeans.quorum.project;


import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import org.netbeans.api.project.Project;
import org.netbeans.api.project.ui.OpenProjects;
import org.openide.awt.ToolbarPool;
import org.openide.util.Lookup;
import org.openide.windows.WindowManager;
import org.sodbeans.compiler.api.CompilerEvent;
import org.sodbeans.compiler.api.CompilerListener;

/**
 * TODO: Move this class, and the entire
 * org.teambeans.sodbeans.projects.Toolbar folder,
 * along with the META-INF/services/ entry to
 * the Windows module.
 * @author Andrew
 */
public class MainProjectListener implements PropertyChangeListener, CompilerListener {

    Project mainProject;
    ToolbarPool toolbarPool = ToolbarPool.getDefault();
    org.sodbeans.compiler.api.Compiler compiler =
                Lookup.getDefault().lookup(org.sodbeans.compiler.api.Compiler.class);

    public MainProjectListener() {
        OpenProjects.getDefault().addPropertyChangeListener(this);
        //compiler.addListener(this);
    }

    /***
     * Property change event for the Open Projects
     * @param evt
     */
    public void propertyChange(PropertyChangeEvent evt) {

        // Main project was set
        if (evt.getPropertyName().equalsIgnoreCase("MainProject")) {

            // No main project set
            if (OpenProjects.getDefault().getMainProject() == null) {
                setToolbarPoolConfiguration("Default");
                return;
            }

            // Main project is Quorum
            String mainProjectType = OpenProjects.getDefault().getMainProject().getClass().toString();
            if (mainProjectType.endsWith("QuorumProject")) {
                setToolbarPoolConfiguration("SODBeansDeveloping");
            }

            // Main project is not quorum, so ignore it
            else {
                setToolbarPoolConfiguration("Default");
            }
        }
        else if(evt.getPropertyName().equalsIgnoreCase("openProjects")) { //a quorum project may have been closed
            //until new debugger/build architecture is in place,
            //for now just tell the compiler to
            //abandon any current build it has.
            compiler.clean();
        }
    }

    /***
     * Action performed event for the compiler
     * @param event
     */
    public void actionPerformed(CompilerEvent event) {


        // The debugger is starting
        if (event.isDebuggerStartEvent() && compiler.isDebuggerActive()) {
            setToolbarPoolConfiguration("SODBeansDebugging");
        }

        // The debugger is stopping
        if (event.isDebuggerStopEvent() && !compiler.isDebuggerActive()) {
            setToolbarPoolConfiguration("SODBeansDeveloping");
        }
    }

    /***
     * Safely sets toolbarpool configuration
     * @param configuration The string representation of the
     * configuration you wish to load
     */
    private void setToolbarPoolConfiguration(final String configuration) {

        Runnable setConfiguration = new Runnable() {
            public void run() {
                toolbarPool.setConfiguration(configuration);
            }
        };

        WindowManager.getDefault().invokeWhenUIReady(setConfiguration);
    }
}
