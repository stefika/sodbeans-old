/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sodbeans.quorum.project;

import org.netbeans.spi.project.ui.PrivilegedTemplates;

/**
 *
 * @author Andreas Stefik
 */
public class QuorumPrivilegedTemplates implements PrivilegedTemplates{

    public String[] getPrivilegedTemplates() {
        String[] vals = new String[2];
        vals[0] = "Templates/Quorum/EmptyQuorumFile.quorum";
        vals[1] = "Templates/Quorum/Main.quorum";
        return vals;
    }

}
