/*
 Copyright (c) 2013, Andreas Stefik
 All rights reserved.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met: 

 1. Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer. 
 2. Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution. 

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 The views and conclusions contained in the software and documentation are those
 of the authors and should not be interpreted as representing official policies, 
 either expressed or implied, of the FreeBSD Project.
 */
package org.tod.meta;

import com.sun.jdi.ClassNotLoadedException;
import com.sun.jdi.Method;
import com.sun.jdi.ReferenceType;
import com.sun.jdi.Type;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.tod.meta.impl.TypeUtils;
import tod.core.database.structure.IBehaviorInfo;
import tod.core.database.structure.ITypeInfo;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author jeff
 */
public abstract class ClassInformationProvider {

    /**
     * Retrieves information for a particular class, if available.
     *
     * @param className the name of the class to load; must be a JNI-style name,
     * such as quorum/Main.
     *
     * @return the class information. At a minimum, the returned ClassInfo
     * object will only have an associated name--line number information is not
     * guaranteed.
     */
    public ClassInfo getClassInfo(String className) {
        ClassInfo c = new ClassInfo();
        c.setFullyQualifiedName(className);
        return c;
    }

    public ClassInfo getClassInfo(ITypeInfo type) {
        return getClassInfo(type.getName().replace(".", "/"));
    }

    public ClassInfo getClassInfo(ReferenceType type) {
        return getClassInfo(type.name().replace(".", "/"));
    }

    /**
     * Resets the state of this provider. This method can be optionally
     * implemented to do things such as clear a cache of class data. This method
     * is called when a TOD session ends.
     */
    public void reset() {
        // do nothing
    }

    public MethodInfo getMethodInfo(IBehaviorInfo behavior) {
        MethodInfo m = new MethodInfo();
        m.setLineNumber(behavior.getLineNumber(0));
        m.setName(behavior.getName());
        m.setOwningClass(this.getClassInfo(behavior.getDeclaringType()));
        m.setReturnType(TypeUtils.getTypeInfo(behavior.getReturnType()));

        // Set up parameters
        ITypeInfo[] argumentTypes = behavior.getArgumentTypes();
        for (int i = 0; i < argumentTypes.length; i++) {
            m.addParameter(TypeUtils.getTypeInfo(argumentTypes[i]));
        }

        return m;
    }

    public MethodInfo getMethodInfo(Method method) {
        MethodInfo m = new MethodInfo();
        m.setLineNumber(method.location().lineNumber());
        m.setName(method.name());
        m.setOwningClass(this.getClassInfo(method.declaringType()));
        try {
            m.setReturnType(TypeUtils.getTypeInfo(method.returnType()));
        } catch (ClassNotLoadedException ex) {
            Logger.getLogger(TypeUtils.class.getName()).log(Level.INFO, null, ex);
        }
        try {
            // Set up parameters.
            Iterator<Type> argumentTypes = method.argumentTypes().iterator();
            while (argumentTypes.hasNext()) {
                Type next = argumentTypes.next();
                m.addParameter(TypeUtils.getTypeInfo(next));
            }
        } catch (ClassNotLoadedException ex) {
            Logger.getLogger(TypeUtils.class.getName()).log(Level.INFO, null, ex);
        }

        return m;
    }
}
