/*
 Copyright (c) 2013, Andreas Stefik
 All rights reserved.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met: 

 1. Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer. 
 2. Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution. 

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 The views and conclusions contained in the software and documentation are those
 of the authors and should not be interpreted as representing official policies, 
 either expressed or implied, of the FreeBSD Project.
 */
package org.tod.meta;

/**
 * Information about a breakpoint that has been reached in the debugger.
 *
 * @author jeff
 */
public class BreakpointInfo extends StateInfo {

    /**
     * The count filter for this breakpoint. In most cases, this is -1.
     */
    private int countFilter = -1;

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof BreakpointInfo)) {
            return false;
        }

        BreakpointInfo bpi = (BreakpointInfo) o;

        return (bpi.getClassInfo().getFullyQualifiedName().equals(this.getClassInfo().getFullyQualifiedName())
                && bpi.getLineNumber() == this.getLineNumber());
    }

    @Override
    public int hashCode() {
        StringBuilder hash = new StringBuilder();
        hash.append(this.getClassInfo().getFullyQualifiedName());
        hash.append(":");
        hash.append(this.getLineNumber());

        return hash.toString().hashCode();
    }

    @Override
    public String toString() {
        StringBuilder str = new StringBuilder();
        str.append(this.getClassInfo().getFullyQualifiedName());
        str.append(":");
        str.append(this.getLineNumber());

        return str.toString();
    }

    /**
     * @return the countFilter
     */
    public int getCountFilter() {
        return countFilter;
    }

    /**
     * @param countFilter the countFilter to set
     */
    public void setCountFilter(int countFilter) {
        this.countFilter = countFilter;
    }
}
