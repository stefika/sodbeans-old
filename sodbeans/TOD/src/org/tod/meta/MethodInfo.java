/*
 Copyright (c) 2013, Andreas Stefik
 All rights reserved.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met: 

 1. Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer. 
 2. Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution. 

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 The views and conclusions contained in the software and documentation are those
 of the authors and should not be interpreted as representing official policies, 
 either expressed or implied, of the FreeBSD Project.
 */
package org.tod.meta;

import java.util.ArrayList;
import java.util.Iterator;

/**
 *
 * @author jeff
 */
public class MethodInfo {

    private String name = "";
    private String signature = "";
    private TypeInfo returnType = new TypeInfo(JavaType.VOID);
    private ClassInfo owningClass = null;
    private ArrayList<TypeInfo> parameters = new ArrayList<TypeInfo>();
    private int lineNumber = -1;

    public void addParameter(TypeInfo info) {
        parameters.add(info);
    }

    public Iterator<TypeInfo> getParameters() {
        return parameters.iterator();
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the signature
     */
    public String getSignature() {
        StringBuilder sig = new StringBuilder();
        Iterator<TypeInfo> i = this.getParameters();
        sig.append(this.getName());
        sig.append("(");
        while (i.hasNext()) {
            sig.append(i.next().getSignature());
        }
        sig.append(")");
        sig.append(this.getReturnType().getSignature());

        return sig.toString();
    }

    /**
     * @return the returnType
     */
    public TypeInfo getReturnType() {
        return returnType;
    }

    /**
     * @param returnType the returnType to set
     */
    public void setReturnType(TypeInfo returnType) {
        this.returnType = returnType;
    }

    /**
     * @return the owningClass
     */
    public ClassInfo getOwningClass() {
        return owningClass;
    }

    /**
     * @param owningClass the owningClass to set
     */
    public void setOwningClass(ClassInfo owningClass) {
        this.owningClass = owningClass;
    }

    @Override
    public String toString() {
        StringBuilder b = new StringBuilder();
        b.append(owningClass.getDotName());
        b.append(":");
        b.append(this.getName());
        b.append("()");

        return b.toString();
    }

    /**
     * @return the lineNumber
     */
    public int getLineNumber() {
        return lineNumber;
    }

    /**
     * @param lineNumber the lineNumber to set
     */
    public void setLineNumber(int lineNumber) {
        this.lineNumber = lineNumber;
    }
}
