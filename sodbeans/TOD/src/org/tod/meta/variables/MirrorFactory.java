/*
 Copyright (c) 2013, Andreas Stefik
 All rights reserved.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met: 

 1. Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer. 
 2. Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution. 

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 The views and conclusions contained in the software and documentation are those
 of the authors and should not be interpreted as representing official policies, 
 either expressed or implied, of the FreeBSD Project.
 */
package org.tod.meta.variables;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.tod.api.TODSession;
import org.tod.meta.variables.impl.FieldVariableImpl;
import org.tod.meta.variables.impl.LocalVariableImpl;
import org.tod.meta.variables.impl.MirroredObjectInstanceImpl;
import org.tod.meta.variables.impl.MirroredPrimitiveImpl;
import tod.core.database.event.ICallerSideEvent;
import tod.core.database.event.IWriteEvent;
import tod.core.database.structure.IStructureDatabase.LocalVariableInfo;
import tod.core.database.structure.ObjectId;
import tod.impl.common.event.FieldWriteEvent;
import tod.impl.common.event.LocalVariableWriteEvent;

/**
 * Constructs mirrored objects from TOD and the JVM.
 *
 * @author jeff
 */
public class MirrorFactory {

    public static MirroredValue instanceFromTOD(TODSession session, Object o, String typeSig) {
        return MirrorFactory.instanceFromTOD(session, o, typeSig, Long.MAX_VALUE);
    }

    public static MirroredValue instanceFromTOD(TODSession session, Object o, String typeSig, long validTimestamp) {
        if (o instanceof Boolean || o instanceof Integer
                || o instanceof Double || o instanceof String) {
            if (typeSig.equals("Z") && o instanceof Integer) {
                Integer i = (Integer) o;
                if (i == 0) {
                    return new MirroredPrimitiveImpl(Boolean.FALSE, typeSig);
                } else {
                    return new MirroredPrimitiveImpl(Boolean.TRUE, typeSig);
                }
            } else {
                return new MirroredPrimitiveImpl(o, typeSig);
            }
        } else if (o instanceof ObjectId) {
            // It may be a string.
            Object newObject = session.getTODHandler().getObjectById((ObjectId) o);
            if (newObject instanceof String) {
                return new MirroredPrimitiveImpl(newObject, typeSig);
            } else {
                // It's not a string, but some other object.
                String instantiatedType = ObjectInspector.getTypeSignature(session, (ObjectId) o);
                if (instantiatedType == null) {
                    instantiatedType = typeSig;
                }
                return mirrorNonprimitive(session, (ObjectId) o, instantiatedType, validTimestamp);
            }
        } else {
            // An unknown type was passed.
            return NullReference.getInstance();
        }
    }

    public static Variable variableFromTOD(TODSession session, ICallerSideEvent event) {
        if (event instanceof IWriteEvent) {
            IWriteEvent write = (IWriteEvent) event;
            Object value = write.getValue();
            if (event instanceof LocalVariableWriteEvent) {
                LocalVariableWriteEvent local = (LocalVariableWriteEvent) event;
                return new LocalVariableImpl(session, local.getVariable(), value, local.getTimestamp());
            } else if (event instanceof FieldWriteEvent) {
                FieldWriteEvent field = (FieldWriteEvent) event;
                return new FieldVariableImpl(session, field.getField(), value, field.getTimestamp());
            }
        }

        return null;
    }

    public static Variable variableFromTOD(TODSession session, LocalVariableInfo local, Object value, long validTimestamp) {
        return new LocalVariableImpl(session, local, value, validTimestamp);
    }

    /**
     * Ask the lookup manager for a non-primitive datatype.
     *
     * @param session
     * @param objectId
     * @param typeSig
     * @param validTimestamp
     * @return
     */
    private static MirroredValue mirrorNonprimitive(TODSession session, ObjectId objectId, String typeSig, long validTimestamp) {
        try {
            Class<? extends MirroredObjectInstance> inst = session.getTypeLookupManager().lookup(typeSig);
            if (inst != null) {
                Constructor<? extends MirroredObjectInstance> constructor = inst.getDeclaredConstructor(TODSession.class, ObjectId.class, String.class, long.class);
                if (constructor != null) {
                    MirroredObjectInstance newInstance =
                            constructor.newInstance(session, objectId, typeSig, validTimestamp);
                    if (newInstance != null) {
                        return newInstance;
                    }
                }
            }
        } catch (InstantiationException ex) {
            Logger.getLogger(MirrorFactory.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(MirrorFactory.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalArgumentException ex) {
            Logger.getLogger(MirrorFactory.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvocationTargetException ex) {
            Logger.getLogger(MirrorFactory.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchMethodException ex) {
            Logger.getLogger(MirrorFactory.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SecurityException ex) {
            Logger.getLogger(MirrorFactory.class.getName()).log(Level.SEVERE, null, ex);
        }

        // if we made it here, an error occurred, so just use the default type.
        return new MirroredObjectInstanceImpl(session, objectId, typeSig, validTimestamp);
    }
}
