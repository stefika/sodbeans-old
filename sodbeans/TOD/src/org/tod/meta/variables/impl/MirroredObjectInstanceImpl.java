/*
 Copyright (c) 2013, Andreas Stefik
 All rights reserved.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met: 

 1. Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer. 
 2. Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution. 

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 The views and conclusions contained in the software and documentation are those
 of the authors and should not be interpreted as representing official policies, 
 either expressed or implied, of the FreeBSD Project.
 */
package org.tod.meta.variables.impl;

import org.tod.meta.variables.MirrorFactory;
import java.util.HashMap;
import java.util.Iterator;
import org.tod.api.TODSession;
import org.tod.meta.impl.TypeUtils;
import org.tod.meta.variables.FieldVariable;
import org.tod.meta.variables.MirroredObjectInstance;
import org.tod.meta.variables.Variable;
import tod.core.database.browser.IEventBrowser;
import tod.core.database.event.ILogEvent;
import tod.core.database.structure.ObjectId;
import tod.impl.common.event.FieldWriteEvent;

/**
 * An instance of an object in the live virtual machine that implements lazy
 * loading of fields.
 *
 * @author jeff
 */
public class MirroredObjectInstanceImpl extends MirroredObjectInstance {

    /**
     * The fields for this object.
     */
    private HashMap<String, FieldVariable> fields = new HashMap<String, FieldVariable>();
    /**
     * Have we bothered to load fields yet? We won't do so until asked.
     */
    private boolean loadedFields = false;
    /**
     * The TOD object we came from.
     */
    private ObjectId todObject = null;
    /**
     * The object type signature from the JVM.
     */
    private String typeSig = null;
    private TODSession session = null;
    /**
     * The maximum point in time in which this object is being considered. A
     * value of Long.MAX_VALUE indicates that the most recent event data is to
     * be used when constructing the object's field values.
     */
    private long validTimestamp = Long.MAX_VALUE;

    public MirroredObjectInstanceImpl(TODSession session, ObjectId ref, String typeSig, long validTimestamp) {
        this.init(session, ref, typeSig, validTimestamp);
    }

    private void init(TODSession session, ObjectId ref, String typeSig, long validTimestamp) {
        this.session = session;
        this.todObject = ref;
        this.typeSig = typeSig;
        this.validTimestamp = validTimestamp;
    }

    /**
     * Get all the fields for this instance. This includes both static and
     * non-static fields.
     *
     * @return
     */
    @Override
    public Iterator<FieldVariable> getFields() {
        if (!loadedFields) {
            loadFields();
        }
        return fields.values().iterator();
    }

    /**
     * Get a particular field by name. Note that if the field does not exist, a
     * null value will be returned.
     *
     * @param name the field to grab
     * @return a FieldVariable instance, or null if the variable specified by
     * name doesn't exist.
     */
    @Override
    public FieldVariable getField(String name) {
        if (!loadedFields) {
            loadFields();
        }

        return fields.get(name);
    }

    @Override
    public int getNumberOfFields() {
        if (!loadedFields) {
            loadFields();
        }

        return fields.size();
    }

    /**
     * Load all field information for this class.
     */
    private void loadFields() {
        IEventBrowser allEvents = session.getTODHandler().getObjectBrowser(this.todObject);
        IEventBrowser variableWrites = session.getTODHandler().filterFieldWrites(allEvents);

        if (!variableWrites.hasNext()) {
            return;
        }

        ILogEvent currentEvent;
        do {
            currentEvent = variableWrites.next();

            if (currentEvent instanceof FieldWriteEvent) {
                FieldWriteEvent fw = (FieldWriteEvent) currentEvent;
                this.updateFieldVariable(fw);
            }
        } while (variableWrites.hasNext() && currentEvent.getTimestamp() <= this.validTimestamp);

        this.loadedFields = true;
        // TODO
    }

    private void updateFieldVariable(FieldWriteEvent fw) {
        String varName = fw.getField().getName();
        if (!this.session.getFilter().acceptVariableName(varName)) {
            return;
        }

        Variable variableFromTOD = MirrorFactory.variableFromTOD(session, fw);
        if (!(variableFromTOD instanceof FieldVariable)) {
            return;
        }


        if (this.fields.containsKey(variableFromTOD.getName())) {
            this.fields.remove(variableFromTOD.getName());
        }

        if (variableFromTOD.getTODObjectID() != this.todObject.getId()) {
            this.fields.put(variableFromTOD.getName(), (FieldVariable) variableFromTOD);
        }
    }

    @Override
    public String getTypeSignature() {
        return TypeUtils.formatTypeSignature(this.typeSig);
    }

    @Override
    public String toString() {
        return "#" + this.todObject.hashCode();
    }
}
