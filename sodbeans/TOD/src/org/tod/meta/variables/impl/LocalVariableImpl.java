/*
 Copyright (c) 2013, Andreas Stefik
 All rights reserved.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met: 

 1. Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer. 
 2. Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution. 

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 The views and conclusions contained in the software and documentation are those
 of the authors and should not be interpreted as representing official policies, 
 either expressed or implied, of the FreeBSD Project.
 */
package org.tod.meta.variables.impl;

import org.tod.meta.variables.MirrorFactory;
import org.tod.api.TODSession;
import org.tod.meta.impl.TypeUtils;
import org.tod.meta.variables.LocalVariable;
import org.tod.meta.variables.MirroredValue;
import org.tod.meta.variables.NullReference;
import tod.core.database.structure.IStructureDatabase.LocalVariableInfo;
import tod.core.database.structure.ObjectId;

/**
 *
 * @author jeff
 */
public class LocalVariableImpl extends LocalVariable {

    /**
     * Our field from TOD.
     */
    private LocalVariableInfo todLocal = null;
    /**
     * Only get information when asked.
     */
    private boolean loaded = false;
    private String name = "";
    private String typeSig = "";
    private Object todObject = null;
    private MirroredValue value = null;
    private TODSession session = null;
    private long validTimestamp = 0;

    public LocalVariableImpl(TODSession session, LocalVariableInfo local, Object todObject, long validTimestamp) {
        this.session = session;
        this.todLocal = local;
        this.todObject = todObject;
        this.name = this.todLocal.getVariableName();
        this.typeSig = this.todLocal.getVariableTypeName();
        this.validTimestamp = validTimestamp;
    }

    @Override
    public String getName() {
        if (!loaded) {
            load();
        }
        return this.name;
    }

    @Override
    public MirroredValue getValue() {
        if (!loaded) {
            load();
        }

        return this.value;

    }

    @Override
    public String getTypeSignature() {
        if (!loaded) {
            load();
        }

        return TypeUtils.formatTypeSignature(this.typeSig);
    }

    private void load() {
        this.loaded = true;
        if (this.todObject != null) {
            this.value = MirrorFactory.instanceFromTOD(this.session, this.todObject, this.typeSig, validTimestamp);
        } else {
            this.value = NullReference.getInstance();
        }
    }

    @Override
    public long getTODObjectID() {
        if (!loaded) {
            load();
        }

        if (this.todObject instanceof ObjectId) {
            ObjectId oid = (ObjectId) this.todObject;

            return oid.getId();
        }

        return -1;
    }
}
