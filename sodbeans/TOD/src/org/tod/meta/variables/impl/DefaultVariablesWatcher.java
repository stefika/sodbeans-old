/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.tod.meta.variables.impl;

import org.tod.api.AbstractSessionTracker;
import com.sun.jdi.AbsentInformationException;
import com.sun.jdi.LocalVariable;
import com.sun.jdi.ObjectReference;
import com.sun.jdi.StackFrame;
import com.sun.jdi.ThreadReference;
import com.sun.jdi.Value;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;
import org.tod.api.DebugEventListener;
import org.tod.api.ExecutionTracker;
import org.tod.impl.execution.SearchResult;
import org.tod.meta.MethodInfo;
import org.tod.meta.steps.AssignmentStep;
import org.tod.meta.steps.ProgramStep;
import org.tod.meta.variables.FieldVariable;
import org.tod.meta.variables.VariablesWatcher;
import org.tod.meta.variables.MirroredObjectInstance;
import org.tod.meta.variables.MirroredValue;
import org.tod.meta.variables.NullReference;
import org.tod.meta.variables.Variable;
import org.tod.meta.variables.MirrorFactory;
import tod.core.database.browser.IEventBrowser;
import tod.core.database.browser.IEventFilter;
import tod.core.database.event.IBehaviorCallEvent;
import tod.core.database.event.ICallerSideEvent;
import tod.core.database.event.ILogEvent;
import tod.core.database.event.IMethodCallEvent;
import tod.core.database.event.IParentEvent;
import tod.core.database.structure.IBehaviorInfo;
import tod.core.database.structure.IClassInfo;
import tod.core.database.structure.IStructureDatabase.LocalVariableInfo;
import tod.core.database.structure.ITypeInfo;
import tod.core.database.structure.ObjectId;
import tod.impl.common.event.FieldWriteEvent;
import tod.impl.common.event.LocalVariableWriteEvent;
import tod.impl.dbgrid.event.MethodCallEvent;

/**
 * This class permits observation of variable values at the current scope.
 * By collaborating with the execution tracker, this works in both
 * forward and backward execution modes.
 * @author jeff
 */
public class DefaultVariablesWatcher extends AbstractSessionTracker implements VariablesWatcher {
    /**
     * The name of the "this" object in the JVM.
     */
    public static final String THIS_VARIABLE_NAME = "me";
    
    /**
     * The locals for the current scope.
     */
    private HashMap<String, Variable> locals = new HashMap<String, Variable>();
    
    /**
     * Our "this" object.
     */
    private MirroredObjectInstance thisObject = null;
    
    /**
     * Get the "this" pointer for the current scope.
     * 
     * @return A MirroredObjectInstance, or null, if the current scope is
     * a static method.
     */
    @Override
    public MirroredObjectInstance getThis() {
        return thisObject;
    }

    @Override
    public Iterator<Variable> getVariablesInScope() {
        return locals.values().iterator();
    }
    
    @Override
    public void clear() {
        this.locals.clear();
        this.thisObject = null;
        
        fireLocalsChanged();
    }

    /**
     * Notify all event listeners of changes in the variables view.
     */
    private void fireLocalsChanged() {
        Iterator<DebugEventListener> listeners = this.getTODSession().getEventListeners();
        while (listeners.hasNext()) {
            DebugEventListener next = listeners.next();
            next.localsChanged();
        }
    }
    
    /**
     * Notify all listeners of changes to the call stack window.
     * 
     * @param lastEvent the last event that occurred from TOD
     */
    private void fireStackChanged(SearchResult result, boolean isStepInto, boolean isForward) {
        List<MethodInfo> stack = new LinkedList<MethodInfo>();
        if (result.live) {
            // Work with live callstack instead.
            int n = result.liveStack.size();
            for (int i = 0; i < n; i++) {
                StackFrame sf = result.liveStack.get(i);
                MethodInfo method = getTODSession().getClassInformationProvider().getMethodInfo(sf.location().method());
                method.setLineNumber(sf.location().lineNumber());
                stack.add(method);
            }
        }else if(result.lastEvent != null && result.lastEvent instanceof IMethodCallEvent && isStepInto){
            stack = this.getTODSession().getTODHandler().getCallStack(result.lastEvent, isStepInto, isForward, false);
        }else if(result.lastEvent != null && result.afterLastEvent != null && isForward){
            boolean isLastEvent = false;
            if(!isStepInto && isForward){
                isLastEvent = true;
            }
            stack = this.getTODSession().getTODHandler().getCallStack(result.afterLastEvent, isStepInto, isForward, isLastEvent);
        }else if(result.lastEvent != null){
            boolean isLastEvent = false;
            if(!isStepInto && !isForward){
                isLastEvent = true;
            }
            stack = this.getTODSession().getTODHandler().getCallStack(result.lastEvent, isStepInto, isForward, isLastEvent);
        }else{
            return;
        }
        
        Iterator<DebugEventListener> listeners = this.getTODSession().getEventListeners();
        while (listeners.hasNext()) {
            DebugEventListener next = listeners.next();
            next.callStackChanged(stack);
        }
    }

    @Override
    public void update(SearchResult result, boolean isStepInto, boolean isForward) {
        this.fireStackChanged(result, isStepInto, isForward);

        if (result.lastEvent == null || result.lastEvent.getParent() == null) {
            this.clear();
            return;
        }        
        this.clear();
        updateLocals(result, isStepInto);
        updateThis(result, isStepInto);
        this.fireLocalsChanged();
    }

    private void updateLocals(SearchResult result, boolean isStepInto) {
        ICallerSideEvent parentEvent;
        if (isStepInto) {
            parentEvent = result.lastEvent;
        } else {
            parentEvent = findParent(result);
        }

        // Update the method parameters
        updateParameters(result, parentEvent);
        IEventBrowser methodEvents;
        if (isStepInto) {
            methodEvents = this.getTODSession().getTODHandler().getStepIntoChildrenBrowser(parentEvent);
        } else {
            methodEvents = this.getTODSession().getTODHandler().getChildrenBrowser(parentEvent);
        }
        IBehaviorInfo parentBehavior = parentEvent.getOperationBehavior();
        //System.out.println("Local variable scope: " + parentBehavior.getDeclaringType().getJvmName() + " - " + parentBehavior.getName() + parentBehavior.getSignature());
        IEventBrowser variableWrites = this.getTODSession().getTODHandler().filterLocalVariableWrites(methodEvents);
        ILogEvent target = result.lastEvent;
        
        if (!variableWrites.hasNext()) {
            return;
        }
        
        ILogEvent currentEvent;
        do {
            currentEvent = variableWrites.next();
            
            if (currentEvent instanceof LocalVariableWriteEvent) {
                LocalVariableWriteEvent lvw = (LocalVariableWriteEvent)currentEvent;
                String varName = lvw.getVariable().getVariableName();
                if (eventInScope(lvw, target)) {
                    this.updateLocalVariable(lvw);
                }
            }
        } while (variableWrites.hasNext() && eventComesBefore(currentEvent, result));        
    }
    
    private void updateThis(SearchResult result, boolean isStepInto) {
        // If the current method is static, there is no this object.
        IBehaviorInfo currentBehavior = result.behavior;
        if (currentBehavior.isStatic() || currentBehavior.isStaticInit()) {
            this.thisObject = null;
            return;
        }
        
        // The "target" of the current method is our "this" object.
        ICallerSideEvent parent;
        parent = findParent(result);
        
        // Our parent must be a method call.
        if (!(parent instanceof IBehaviorCallEvent)) {
            return;
        }
        
        IBehaviorCallEvent callEvent = (IBehaviorCallEvent)parent;
        Object target = callEvent.getTarget();
        
        // The target object must be an ObjectId
        if (!(target instanceof ObjectId)) {
            this.thisObject = null;
            return;
        }
        
        // Our current method's "declaring type" will give us the type signature.
        String thisType = currentBehavior.getDeclaringType().getName();
        
        // We want the object to only have data up to our target event's timestamp.
        long validTimestamp = result.lastEvent.getTimestamp();
        // Set the this pointer. It must be a MirroredObjectInstance. and not a NullReference.
        MirroredValue instanceFromTOD = MirrorFactory.instanceFromTOD(session, target, thisType, validTimestamp);
        if ((instanceFromTOD instanceof NullReference) || !(instanceFromTOD instanceof MirroredObjectInstance)) {
            this.thisObject = null;
            return;
        }
        
        this.thisObject = (MirroredObjectInstance)instanceFromTOD;
    }
    
    private boolean eventInScope(LocalVariableWriteEvent event, ILogEvent referenceEvent) {
        if (!(referenceEvent instanceof ICallerSideEvent)) {
            return false;
        }
        ICallerSideEvent reference = (ICallerSideEvent)referenceEvent;
        //LocalVariableInfo localInfo = event.getVariable();
        return reference.getDepth() == event.getDepth() &&
                reference.getOperationBehavior().equals(event.getOperationBehavior());
                
    }
    
    private void updateLocalVariable(LocalVariableWriteEvent lvw) {
        String varName = lvw.getVariable().getVariableName();
        if (!this.getTODSession().getFilter().acceptVariableName(varName)) {
            return;
        }
        Variable variableFromTOD = MirrorFactory.variableFromTOD(session, lvw);
        if (this.locals.containsKey(variableFromTOD.getName())) {
            this.locals.remove(variableFromTOD.getName());
        }
        this.locals.put(variableFromTOD.getName(), variableFromTOD);
    }
    
    private void updateParameter(LocalVariableInfo param, Object value, long timeStamp) {
        String varName = param.getVariableName();
        if (!this.getTODSession().getFilter().acceptVariableName(varName)) {
            return;
        }
        Variable variableFromTOD = MirrorFactory.variableFromTOD(session, param, value, timeStamp);
        if (this.locals.containsKey(variableFromTOD.getName())) {
            this.locals.remove(variableFromTOD.getName());
        }
        this.locals.put(variableFromTOD.getName(), variableFromTOD);
    }

    /**
     * Does this event come before the given event? If the result parameter
     * indicates that we are running "live" from the virtual machine, the
     * currentEvent's timestamp is compared against the result's lastEvent's
     * timestamp and is considered to come "before" if the target's timestamp
     * is less than or equal to the currentEvent's timestamp. Otherwise, in
     * non-live execution, the lastEvent's timestamp must be strictly 
     * lower.
     * @param currentEvent
     * @param result
     * @return 
     */
    private boolean eventComesBefore(ILogEvent currentEvent, SearchResult result) {
        if (result.live) {
            return currentEvent.getTimestamp() <= result.lastEvent.getTimestamp();  
        } else {
            return currentEvent.getTimestamp() < result.lastEvent.getTimestamp();  
        }
    }

    private ICallerSideEvent findParent(SearchResult result) {
        // Find the event that CALLED our current scope.
        IBehaviorInfo current = result.behavior;
        IBehaviorCallEvent call;
        
        // Are we currently on a method call?
        if (result.targetEvent instanceof IBehaviorCallEvent) {
            call = (IBehaviorCallEvent)result.targetEvent;
        } else {
            call = result.targetEvent.getParent();
        }
        
        while (!calledBehavior(call).equals(current)) {
            call = call.getParent();
        }
        
        return call;
    }

    private IBehaviorInfo calledBehavior(IBehaviorCallEvent e) {
        return e.getCalledBehavior() != null ? 
                e.getCalledBehavior() : e.getExecutedBehavior();
    }
    private void updateParameters(SearchResult result, ICallerSideEvent parentEvent) {
        if (!(parentEvent instanceof IBehaviorCallEvent)) {
            return;
        }
        getTODSession().getTODHandler().flush();
        
        IBehaviorCallEvent call = (IBehaviorCallEvent)parentEvent;
        
        int startIdx = 1;
        
        // If we're in a non-static method, there is no "this" pointer, so
        // we need to subtract 1 from each local variable index we receive
        if (call.getCalledBehavior() != null) {
            if (call.getCalledBehavior().isStatic() || call.getCalledBehavior().isStaticInit()) {
                startIdx = 0;
            }
        } else if (call.getExecutedBehavior() != null) {
            if (call.getExecutedBehavior().isStatic() || call.getExecutedBehavior().isStaticInit()) {
                startIdx = 0;
            }
        }
        
        Object[] arguments = call.getArguments();
        List<LocalVariableInfo> localVars = result.behavior.getLocalVariables();
        for (int i = 0; i < localVars.size(); i++) {
            LocalVariableInfo var = localVars.get(i);
            if (var != null) {
                if (var.getIndex() <= arguments.length) {
                    try {
                        Object arg = arguments[var.getIndex() - startIdx];
                        this.updateParameter(var, arg, call.getTimestamp());
                    } catch (ArrayIndexOutOfBoundsException e) {
                    }
                }
            }
        }
    }
}