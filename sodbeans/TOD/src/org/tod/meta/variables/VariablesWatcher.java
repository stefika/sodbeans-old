/*
 Copyright (c) 2013, Andreas Stefik
 All rights reserved.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met: 

 1. Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer. 
 2. Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution. 

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 The views and conclusions contained in the software and documentation are those
 of the authors and should not be interpreted as representing official policies, 
 either expressed or implied, of the FreeBSD Project.
 */
package org.tod.meta.variables;

import java.util.Iterator;
import org.tod.api.SessionTracker;
import org.tod.impl.execution.SearchResult;
import org.tod.meta.variables.MirroredObjectInstance;
import org.tod.meta.variables.Variable;

/**
 * This class permits observation of variable values at the current scope. By
 * collaborating with the execution tracker, this works in both forward and
 * backward execution modes.
 *
 * @author jeff
 */
public interface VariablesWatcher extends SessionTracker {

    /**
     * Get the "this" pointer for the current scope.
     *
     * @return A MirroredObjectInstance, or null, if the current scope is a
     * static method.
     */
    public MirroredObjectInstance getThis();

    public Iterator<Variable> getVariablesInScope();

    /**
     * Empty out any information about locals. This is necessary in any
     * circumstance where the VM (or TOD) may resume for an arbitrary amount of
     * time, as any data in the local variable table is invalidated.
     */
    public void clear();

    public void update(SearchResult result, boolean isStepInto, boolean isForward);
}
