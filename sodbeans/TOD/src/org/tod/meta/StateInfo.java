/*
 Copyright (c) 2013, Andreas Stefik
 All rights reserved.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met: 

 1. Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer. 
 2. Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution. 

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 The views and conclusions contained in the software and documentation are those
 of the authors and should not be interpreted as representing official policies, 
 either expressed or implied, of the FreeBSD Project.
 */
package org.tod.meta;

import java.util.Iterator;
import org.tod.meta.steps.ProgramStep;

/**
 * Information about some step that has occurred, such as stepping over a line.
 *
 * @author jeff
 */
public class StateInfo {

    private MethodInfo methodInfo = null;
    private ClassInfo classInfo = null;
    private int lineNumber = -1;
    private ThreadInfo thread = null;
    private boolean isLiveEvent = true;
    /**
     * The most recent events recorded from the program.
     */
    private Iterator<ProgramStep> steps = null;

    /**
     * @return the lineNumber
     */
    public int getLineNumber() {
        return lineNumber;
    }

    /**
     * @param lineNumber the lineNumber to set
     */
    public void setLineNumber(int lineNumber) {
        this.lineNumber = lineNumber;
    }

    /**
     * @return the thread
     */
    public ThreadInfo getThread() {
        return thread;
    }

    /**
     * @param thread the thread to set
     */
    public void setThread(ThreadInfo thread) {
        this.thread = thread;
    }

    /**
     * Did this event occur from a stepping or breakpoint event in the virtual
     * machine? If not, this event corresponds to stepping through program
     * history.
     *
     * @return the isLiveEvent
     */
    public boolean isIsLiveEvent() {
        return isLiveEvent;
    }

    /**
     * @param isLiveEvent whether or not this event corresponds to real program
     * execution
     */
    public void setIsLiveEvent(boolean isLiveEvent) {
        this.isLiveEvent = isLiveEvent;
    }

    public boolean hasNext() {
        if (this.steps == null) {
            return false;
        }

        return this.steps.hasNext();
    }

    public ProgramStep next() {
        return this.steps.next();
    }

    public void setSteps(Iterator<ProgramStep> steps) {
        this.steps = steps;
    }

    /**
     * @return the classInfo
     */
    public ClassInfo getClassInfo() {
        return classInfo;
    }

    /**
     * @param classInfo the classInfo to set
     */
    public void setClassInfo(ClassInfo classInfo) {
        this.classInfo = classInfo;
    }

    /**
     * @return the methodInfo
     */
    public MethodInfo getMethodInfo() {
        return methodInfo;
    }

    /**
     * @param methodInfo the methodInfo to set
     */
    public void setMethodInfo(MethodInfo methodInfo) {
        this.methodInfo = methodInfo;
    }
}
