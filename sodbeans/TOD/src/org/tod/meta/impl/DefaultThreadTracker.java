/*
 Copyright (c) 2013, Andreas Stefik
 All rights reserved.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met: 

 1. Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer. 
 2. Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution. 

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 The views and conclusions contained in the software and documentation are those
 of the authors and should not be interpreted as representing official policies, 
 either expressed or implied, of the FreeBSD Project.
 */
package org.tod.meta.impl;

import org.tod.api.AbstractSessionTracker;
import com.sun.jdi.ThreadReference;
import java.util.HashMap;
import org.tod.meta.ThreadTracker;
import org.tod.meta.ThreadInfo;
import tod.core.database.structure.IThreadInfo;

/**
 *
 * @author jeff
 */
public class DefaultThreadTracker extends AbstractSessionTracker implements ThreadTracker {

    private HashMap<ThreadInfo, ThreadReference> threadsToRefs = new HashMap<ThreadInfo, ThreadReference>();
    private HashMap<ThreadReference, ThreadInfo> refsToThreads = new HashMap<ThreadReference, ThreadInfo>();
    private ThreadInfo activeThread = null;

    @Override
    public ThreadInfo getActiveThread() {
        return this.activeThread;
    }

    @Override
    public void setActiveThread(ThreadInfo info) {
        this.activeThread = info;

        // Make sure it exists.
        if (!threadsToRefs.containsKey(info)) {
            threadsToRefs.put(info, info.getThreadReference());
            refsToThreads.put(info.getThreadReference(), info);
        }
    }

    @Override
    public ThreadReference get(ThreadInfo info) {
        return threadsToRefs.get(info);
    }

    @Override
    public void put(ThreadInfo info) {
        threadsToRefs.put(info, info.getThreadReference());
        refsToThreads.put(info.getThreadReference(), info);
    }

    @Override
    public ThreadInfo getByReference(ThreadReference reference) {
        ThreadInfo info = refsToThreads.get(reference);
        if (info == null) {
            IThreadInfo todReference = this.getTODSession().getTODHandler().lookupThread(reference);
            info = new ThreadInfo(reference, todReference);
            threadsToRefs.put(info, reference);
            refsToThreads.put(reference, info);
        }

        return info;
    }
}
