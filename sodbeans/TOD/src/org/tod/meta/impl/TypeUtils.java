/*
 Copyright (c) 2013, Andreas Stefik
 All rights reserved.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met: 

 1. Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer. 
 2. Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution. 

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 The views and conclusions contained in the software and documentation are those
 of the authors and should not be interpreted as representing official policies, 
 either expressed or implied, of the FreeBSD Project.
 */
package org.tod.meta.impl;

import com.sun.jdi.Type;
import org.tod.meta.TypeInfo;
import tod.core.database.structure.IBehaviorInfo;
import tod.core.database.structure.IStructureDatabase;
import tod.core.database.structure.ITypeInfo;

/**
 *
 * @author jeff
 */
public class TypeUtils {

    public static TypeInfo getTypeInfo(ITypeInfo type) {
        TypeInfo t = new TypeInfo();
        t.setSignature(type.getName());
        return t;
    }

    public static TypeInfo getTypeInfo(Type type) {
        TypeInfo t = new TypeInfo();
        t.setSignature(type.signature());
        return t;
    }

    public static int calculateLineNumber(IBehaviorInfo behavior, int index) {
        if (behavior == null) {
            return -1;
        }

        IStructureDatabase.LineNumberInfo[] lineNumbers = behavior.getLineNumbers();

        if (lineNumbers == null) {
            // This method has no line numbers.
            return -1;
        } else if (lineNumbers.length == 0) {
            return -1;
        }

        int targetLineIndex = -1;
        for (int i = 0; i < lineNumbers.length; i++) {
            IStructureDatabase.LineNumberInfo line = lineNumbers[i];
            if (line.getStartPc() == index) {
                targetLineIndex = i;
                break;
            } else if (line.getStartPc() > index) {
                targetLineIndex = i - 1;
                break;
            }
        }

        if (targetLineIndex >= 0) {
            return lineNumbers[targetLineIndex].getLineNumber();
        } else {
            // Return the first line
            return lineNumbers[0].getLineNumber();
        }
    }

    public static int getLineBytecodeIndex(IBehaviorInfo behavior, int lineNumber) {
        if (behavior == null) {
            // No valid behavior given.
            return -1;
        }
        IStructureDatabase.LineNumberInfo[] lines = behavior.getLineNumbers();
        // This method has no line numbers.
        if (lines == null) {
            return -1;
        }

        for (int i = 0; i < lines.length; i++) {
            if (lines[i].getLineNumber() >= lineNumber) {
                return lines[i].getStartPc();
            }
        }

        // No such line exists.
        return -1;
    }

    public static int getEndLineBytecodeIndex(IBehaviorInfo behavior, int lineNumber) {
        if (behavior == null) {
            // No valid behavior given.
            return -1;
        }
        IStructureDatabase.LineNumberInfo[] lines = behavior.getLineNumbers();
        // This method has no line numbers.
        if (lines == null) {
            return -1;
        }
        int arrayIndex = -1;
        for (int i = 0; i < lines.length; i++) {
            if (lines[i].getLineNumber() == lineNumber) {
                arrayIndex = i;
            }
        }

        if (arrayIndex != -1) {
            // Is it the last line of the method?
            if (arrayIndex == lines.length - 1) {
                return behavior.getCodeSize();
            } else {
                // Return the NEXT line's starting PC.
                return lines[arrayIndex + 1].getStartPc();
            }
        }

        // No such line exists.
        return -1;
    }

    public static int getPreviousLineBytecodeIndex(IBehaviorInfo behavior, int lineNumber) {
        IStructureDatabase.LineNumberInfo[] lines = behavior.getLineNumbers();
        int arrayIndex = -1;
        for (int i = 0; i < lines.length; i++) {
            if (lines[i].getLineNumber() == lineNumber) {
                arrayIndex = i - 1;
                break;
            }
        }

        if (arrayIndex != -1) {
            // It's not the first line of the method.
            return lines[arrayIndex].getStartPc();
        }

        // There is no previous line.
        return -1;
    }

    /**
     * Format a type signature from TOD or the JVM and ensure that it is of the
     * format Lx/y/z;
     *
     * @param typeSignature
     * @return
     */
    public static String formatTypeSignature(String typeSig) {
        // Leave primitives alone.
        if (typeSig.length() <= 1) {
            return typeSig;
        }

        String newTypeSig = typeSig;
        if (!typeSig.startsWith("[") && !typeSig.startsWith("L")) {
            newTypeSig = "L" + newTypeSig;
        }
        newTypeSig = newTypeSig.replace(".", "/");
        if (!newTypeSig.endsWith(";")) {
            newTypeSig = newTypeSig + ";";
        }

        return newTypeSig;
    }
}
