/*
 Copyright (c) 2013, Andreas Stefik
 All rights reserved.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met: 

 1. Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer. 
 2. Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution. 

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 The views and conclusions contained in the software and documentation are those
 of the authors and should not be interpreted as representing official policies, 
 either expressed or implied, of the FreeBSD Project.
 */
package org.tod.meta.steps.impl;

import org.tod.api.TODSession;
import org.tod.meta.ClassInfo;
import org.tod.meta.MethodInfo;
import org.tod.meta.impl.TypeUtils;
import org.tod.meta.sourcecode.Scope;
import org.tod.meta.sourcecode.BlockScope;
import org.tod.meta.sourcecode.SourceInfo;
import org.tod.meta.steps.AssignmentStep;
import org.tod.meta.steps.MethodCallStep;
import org.tod.meta.steps.MethodReturnStep;
import org.tod.meta.steps.ProgramStep;
import org.tod.meta.steps.StepType;
import org.tod.meta.steps.TestConditionalStep;
import org.tod.meta.steps.TestLoopStep;
import org.tod.meta.variables.MirrorFactory;
import org.tod.meta.variables.Variable;
import tod.core.database.event.ICallerSideEvent;
import tod.core.database.event.IMethodCallEvent;
import tod.core.database.event.IWriteEvent;
import tod.core.database.structure.IBehaviorInfo;
import tod.impl.common.event.BehaviorExitEvent;
import tod.impl.dbgrid.event.InstantiationEvent;

/**
 *
 * @author jeff
 */
public class ProgramStepFactory {

    public static TestConditionalStep conditionalStep(TODSession session, ICallerSideEvent event, int newLine, int lastLineNumber, boolean forward) {
        // Did the conditional evaluate to true or false?
        boolean conditionalTrue;
        BlockScope targetScope;
        if (event == null) {
            return null;
        }

        if (event.getOperationBehavior() == null) {
            return null;
        }
        // Gather information about i) the class we're in,
        // ii) the last scope we were in (from lastLineNumber) and
        // iii) the current scope we are in.
        ClassInfo classInfo = session.getClassInformationProvider().getClassInfo(event.getOperationBehavior().getDeclaringType());
        SourceInfo sourceInfo = classInfo.getSourceInfo();
        BlockScope lastScope = sourceInfo.findScope(lastLineNumber);
        BlockScope newScope = sourceInfo.findScope(newLine);

        if (newScope == null && lastScope == null) {
            return null;
        }

        // We want to generate a test conditional step under the following cases:
        // i) We just entered the body of an if/elseif/else;
        // ii) We skipped over the body of an if/elseif.

        // If we weren't in a scope on lastLine, then don't worry about it--
        // just skip.
        if (lastScope != null) {
            // If the lastLineNumber was the start of an if/elseif...
            if (lastLineNumber == lastScope.getStartLine()) {
                // ...and we're in the same scope now, then it's true.
                if (newScope != null && newScope.getEndLine() <= lastScope.getEndLine()) {
                    conditionalTrue = true;
                    targetScope = lastScope;
                } else {
                    // We're not in that scope, so it was false.
                    // Is our current scope an else? If so, the "else"
                    // has evaluated to true.
                    if (newScope != null && newScope.getType() == Scope.ELSE) {
                        conditionalTrue = true;
                        targetScope = newScope;
                    } else {
                        conditionalTrue = false;
                        targetScope = lastScope;
                    }
                }

                if (targetScope.getType() == Scope.REPEAT) {
                    return null;
                }
                // Generate the conditional step.
                TestConditionalStep s = new TestConditionalStep();
                s.setType(StepType.TEST_CONDITIONAL);
                s.setConditional(targetScope);
                s.setWasTrue(conditionalTrue);
                s.setMethodInfo(session.getClassInformationProvider().getMethodInfo(event.getOperationBehavior()));
                s.setLineNumber(newLine);
                s.setForward(forward);
                return s;
            }
        }

        return null;
    }

    public static TestLoopStep loopStep(TODSession session, ICallerSideEvent event, int newLine, int lastLineNumber, boolean forward) {
        // Did the loop evaluate to true or false?
        boolean loopTrue = false;
        BlockScope targetScope = null;
        if (event == null) {
            return null;
        }

        if (event.getOperationBehavior() == null) {
            return null;
        }
        // Gather information about i) the class we're in,
        // ii) the last scope we were in (from lastLineNumber) and
        // iii) the current scope we are in.
        ClassInfo classInfo = session.getClassInformationProvider().getClassInfo(event.getOperationBehavior().getDeclaringType());
        SourceInfo sourceInfo = classInfo.getSourceInfo();
        BlockScope lastScope = sourceInfo.findScope(lastLineNumber);
        BlockScope newScope = sourceInfo.findScope(newLine);

        if (newScope == null && lastScope == null) {
            return null;
        }

        // We want to generate a test conditional step under the following cases:
        // i) We just entered the body of an if/elseif/else;
        // ii) We skipped over the body of an if/elseif.

        // If we weren't in a scope on lastLine, then don't worry about it--
        // just skip.
        if (lastScope != null) {
            // If the lastLineNumber was the start of a repeat...
            if (lastLineNumber == lastScope.getStartLine() && lastScope.getType() == Scope.REPEAT) {
                // ...and we're in the same scope now, then it's true.
                if (newScope != null && newScope.getEndLine() <= lastScope.getEndLine()) {
                    loopTrue = true;
                    targetScope = lastScope;
                } else {
                    // We're not in that scope, so it was false.
                    loopTrue = false;
                    targetScope = lastScope;
                }
                // If we ended up back at the beginning of the loop, we need to
                // say "loop true restored."
            }/* else if (lastLineNumber != lastScope.getStartLine() && lastScope.equals(newScope) &&
             newLine == newScope.getStartLine()) {
             loopTrue = true;
             targetScope = lastScope;
             }*/

            if (targetScope != null) {
                // Generate the conditional step.
                TestLoopStep s = new TestLoopStep();
                s.setType(StepType.TEST_LOOP);
                s.setConditional(targetScope);
                s.setWasTrue(loopTrue);
                s.setMethodInfo(session.getClassInformationProvider().getMethodInfo(event.getOperationBehavior()));
                s.setLineNumber(newLine);
                s.setForward(forward);
                return s;
            }
        }

        return null;
    }

    public static ProgramStep stepFromTOD(TODSession session, ICallerSideEvent event, boolean forward, boolean live) {
        if (event instanceof IMethodCallEvent) {
            return methodCallTOD(session, (IMethodCallEvent) event, forward, live);
        } else if (event instanceof BehaviorExitEvent) {
            //return methodReturnTOD(session, (BehaviorExitEvent)event, forward, live);
        } else if (event instanceof IWriteEvent) {
            return assignmentFromTOD(session, event, forward, live);
        } else if (event instanceof InstantiationEvent) {
            return instantiationFromTOD(session, (InstantiationEvent) event, forward, live);
        }
        return null;
    }

    private static MethodCallStep methodCallTOD(TODSession session, IMethodCallEvent event, boolean forward, boolean live) {
        MethodCallStep s = new MethodCallStep();
        IBehaviorInfo behavior = event.getOperationBehavior();
        IBehaviorInfo calledBehavior = event.getCalledBehavior();

        if (calledBehavior == null) {
            calledBehavior = event.getExecutedBehavior();
        }
        // If there is no associated behavior, TOD does not have information
        // for the given class. It is likely out of scope.
        if (behavior == null || calledBehavior == null || calledBehavior.isStaticInit()) {
            return null;
        }
        MethodInfo methodInfo = session.getClassInformationProvider().getMethodInfo(behavior);
        MethodInfo targetMethodInfo = session.getClassInformationProvider().getMethodInfo(calledBehavior);
        if (!session.getFilter().acceptMethod(targetMethodInfo)) {
            return null;
        }
        s.setType(StepType.METHOD_CALL);
        s.setLive(live);
        s.setMethodInfo(methodInfo);
        s.setTargetMethodInfo(targetMethodInfo);
        int line = TypeUtils.calculateLineNumber(event.getOperationBehavior(), event.getOperationBytecodeIndex());
        s.setLineNumber(line); // TODO
        s.setTargetLineNumber(0); // TODO
        s.setForward(forward);

        // Set up the parameters.
        Object[] arguments = event.getArguments();
        for (int i = 0; i < arguments.length; i++) {
            Object arg = arguments[i];
            s.addParameter(MirrorFactory.instanceFromTOD(session, arg, calledBehavior.getArgumentTypes()[i].getJvmName()));
        }

        return s;
    }

    private static MethodReturnStep methodReturnTOD(TODSession session, BehaviorExitEvent event, boolean forward, boolean live) {
        MethodReturnStep s = new MethodReturnStep();
        IBehaviorInfo behavior = event.getOperationBehavior();

        // If there is no associated behavior, TOD does not have information
        // for the given class. It is likely out of scope.
        if (behavior == null || behavior.isStaticInit()) {
            return null;
        }
        MethodInfo methodInfo = session.getClassInformationProvider().getMethodInfo(behavior);
        if (!session.getFilter().acceptMethod(methodInfo)) {
            return null;
        }
        s.setType(StepType.METHOD_RETURN);
        s.setLive(live);
        s.setMethodInfo(methodInfo);
        int line = TypeUtils.calculateLineNumber(event.getOperationBehavior(), event.getOperationBytecodeIndex());
        s.setLineNumber(line);
        s.setForward(forward);


        return s;
    }

    private static ProgramStep assignmentFromTOD(TODSession session, ICallerSideEvent event, boolean forward, boolean live) {
        AssignmentStep s = new AssignmentStep();
        IBehaviorInfo behavior = event.getOperationBehavior();
        s.setType(StepType.ASSIGNMENT);
        s.setLive(live);
        s.setMethodInfo(session.getClassInformationProvider().getMethodInfo(behavior));
        int line = TypeUtils.calculateLineNumber(event.getOperationBehavior(), event.getOperationBytecodeIndex());
        s.setLineNumber(line); // TODO
        s.setForward(forward);
        Variable variableFromTOD = MirrorFactory.variableFromTOD(session, event);
        if (!session.getFilter().acceptVariableName(variableFromTOD.getName())) {
            return null;
        }
        s.setVariable(variableFromTOD);
        return s;
    }

    private static ProgramStep instantiationFromTOD(TODSession session, InstantiationEvent instantiationEvent, boolean forward, boolean live) {
        /*AssignmentStep s = new AssignmentStep();
         IBehaviorInfo behavior = event.getOperationBehavior();
         s.setType(StepType.ASSIGNMENT);
         s.setLive(live);
         s.setMethodInfo(session.getClassInformationProvider().getMethodInfo(behavior));
         int line = TypeUtils.calculateLineNumber(event.getOperationBehavior(), event.getOperationBytecodeIndex());
         s.setLineNumber(line); // TODO
         s.setForward(forward);
         Variable variableFromTOD = MirrorFactory.variableFromTOD(session, event);
         if (!session.getFilter().acceptVariableName(variableFromTOD.getName())) {
         return null;
         }
         s.setVariable(variableFromTOD);*/
        return null; // TODO
    }
}
