/*
 Copyright (c) 2013, Andreas Stefik
 All rights reserved.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met: 

 1. Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer. 
 2. Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution. 

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 The views and conclusions contained in the software and documentation are those
 of the authors and should not be interpreted as representing official policies, 
 either expressed or implied, of the FreeBSD Project.
 */
package org.tod.meta;

import java.util.HashMap;

/**
 *
 * @author jeff
 */
public class TypeInfo {

    private JavaType type = JavaType.UNKNOWN;
    private String signature = "";
    private static final HashMap<JavaType, String> typeMap = new HashMap<JavaType, String>();

    static {
        typeMap.put(JavaType.BOOLEAN, "Z");
        typeMap.put(JavaType.DOUBLE, "D");
        typeMap.put(JavaType.INT, "I");
        typeMap.put(JavaType.STRING, "Ljava/lang/String;");
        typeMap.put(JavaType.OBJECT, "Ljava/lang/Object;");
        typeMap.put(JavaType.VOID, "V");
        typeMap.put(JavaType.UNKNOWN, "");
    }

    public TypeInfo() {
    }

    public TypeInfo(JavaType type) {
        this.setType(type);
    }

    /**
     * @return the type
     */
    public JavaType getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(JavaType type) {
        this.type = type;

        // Set the signature to a reasonable default
        this.signature = typeMap.get(type);
    }

    /**
     * @return the signature
     */
    public String getSignature() {
        return signature;
    }

    /**
     * @param signature the signature to set
     */
    public void setSignature(String signature) {
        if (type == JavaType.OBJECT) {
            this.signature = signature;
        }
    }
}
