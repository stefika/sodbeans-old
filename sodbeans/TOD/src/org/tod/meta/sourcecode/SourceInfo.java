/*
 Copyright (c) 2013, Andreas Stefik
 All rights reserved.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met: 

 1. Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer. 
 2. Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution. 

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 The views and conclusions contained in the software and documentation are those
 of the authors and should not be interpreted as representing official policies, 
 either expressed or implied, of the FreeBSD Project.
 */
package org.tod.meta.sourcecode;

import java.util.HashSet;
import java.util.Map.Entry;
import java.util.TreeMap;

/**
 * Encapsulates information about the source code in a running program.
 *
 * @author jeff
 */
public class SourceInfo {

    /**
     * Stores conditionals in the class by starting line number.
     */
    private TreeMap<Integer, BlockScope> scopes = new TreeMap<Integer, BlockScope>();
    /**
     * Stores conditionals in the class by ending line number.
     */
    private TreeMap<Integer, BlockScope> endLines = new TreeMap<Integer, BlockScope>();
    /**
     * Which lines of the class have returns on them?
     */
    private HashSet<Integer> returns = new HashSet<Integer>();
    /**
     * Lines which mark the "end" lines of methods.
     */
    private HashSet<Integer> actionEnds = new HashSet<Integer>();

    /**
     * Mark the given line as a return statement.
     *
     * @param line
     */
    public void addReturn(int line) {
        returns.add(line);
    }

    public void addActionEnd(int line) {
        actionEnds.add(line);
    }

    public boolean isActionEnd(int line) {
        return actionEnds.contains(line);
    }

    /**
     * Is the given line a return statement?
     *
     * @param line
     * @return
     */
    public boolean isReturn(int line) {
        return returns.contains(line);
    }

    /**
     *
     * @param type
     * @param startLine
     * @param endLine
     */
    public void addScope(Scope type, int startLine, int endLine) {
        BlockScope scope = new BlockScope();
        scope.setType(type);
        scope.setStartLine(startLine);
        scope.setEndLine(endLine);
        scopes.put(startLine, scope);
        endLines.put(endLine, scope);

        // If this is an ELSEIF or ELSE, what IF does it belong to?
        if (type == Scope.ELSE || type == Scope.ELSEIF) {
            // Find the conditional with an end line that is closest to the
            // start line of this conditional.
            Entry<Integer, BlockScope> parent = endLines.floorEntry(startLine);
            if (parent != null) {
                scope.setParent(parent.getValue());
            }
        }
    }

    public BlockScope scopeAtLine(int startLine) {
        return scopes.get(startLine);
    }

    public BlockScope scopeBetween(int startLine, int endLine) {
        Entry<Integer, BlockScope> entry = scopes.higherEntry(startLine);
        if (entry == null) {
            return null;
        } else if (entry.getKey() <= endLine) {
            return entry.getValue();
        } else {
            return null;
        }
    }

    public BlockScope findScope(int line) {
        Entry<Integer, BlockScope> entry = scopes.floorEntry(line);
        if (entry == null) {
            return null;
        } else if (entry.getValue().getStartLine() <= line && entry.getValue().getEndLine() >= line) {
            return entry.getValue();
        } else {
            return null;
        }
    }

    public BlockScope scopeCeilingBetween(int startLine, int endLine) {
        Entry<Integer, BlockScope> entry = scopes.ceilingEntry(startLine);
        if (entry == null) {
            return null;
        } else if (entry.getKey() <= endLine) {
            return entry.getValue();
        } else {
            return null;
        }
    }

    public BlockScope scopeFloorBetween(int startLine, int endLine) {
        Entry<Integer, BlockScope> entry = scopes.floorEntry(startLine);
        if (entry == null) {
            return null;
        } else if (endLine <= entry.getValue().getStartLine()) {
            return entry.getValue();
        } else {
            return null;
        }
    }

    public BlockScope findRepeatScope(int currentLine) {
        Entry<Integer, BlockScope> entry = scopes.lowerEntry(currentLine);
        if (entry != null && entry.getValue().getType() == Scope.REPEAT && currentLine < entry.getValue().getEndLine() && currentLine > entry.getValue().getStartLine()) {
            return entry.getValue();
        } else if (entry != null) {
            BlockScope findRepeatScope = findRepeatScope(entry.getValue().getStartLine());
            if (entry.getValue().getType() == Scope.REPEAT && currentLine < findRepeatScope.getEndLine() && currentLine > findRepeatScope.getStartLine()) {
                return findRepeatScope;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }
}
