/*
 Copyright (c) 2013, Andreas Stefik
 All rights reserved.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met: 

 1. Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer. 
 2. Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution. 

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 The views and conclusions contained in the software and documentation are those
 of the authors and should not be interpreted as representing official policies, 
 either expressed or implied, of the FreeBSD Project.
 */
package org.tod.meta.sourcecode;

/**
 *
 * @author jeff
 */
public class BlockScope {

    private int startLine = 0;
    private int endLine = 0;
    private Scope type = Scope.IF;
    private BlockScope parent = null;
    private int loopCounter = 1;

    /**
     * @return the startLine
     */
    public int getStartLine() {
        return startLine;
    }

    /**
     * @param startLine the startLine to set
     */
    public void setStartLine(int startLine) {
        this.startLine = startLine;
    }

    /**
     * @return the endLine
     */
    public int getEndLine() {
        return endLine;
    }

    /**
     * @param endLine the endLine to set
     */
    public void setEndLine(int endLine) {
        this.endLine = endLine;
    }

    /**
     * @return the type
     */
    public Scope getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(Scope type) {
        this.type = type;
    }

    /**
     * For elseif and else statements, this is the conditional that immediately
     * precedes it.
     *
     * @param parent
     */
    public void setParent(BlockScope parent) {
        this.parent = parent;
    }

    public BlockScope getParent() {
        return this.parent;
    }

    /**
     * @return the loopCounter
     */
    public int getLoopCounter() {
        return loopCounter;
    }

    /**
     * @param loopCounter the loopCounter to set
     */
    public void setLoopCounter(int loopCounter) {
        this.loopCounter = loopCounter;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null || !(o instanceof BlockScope)) {
            return false;
        }

        BlockScope other = (BlockScope) o;

        return other.getStartLine() == this.getStartLine()
                && other.getEndLine() == this.getEndLine()
                && other.getType() == this.getType();
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + this.startLine;
        hash = 29 * hash + this.endLine;
        hash = 29 * hash + (this.type != null ? this.type.hashCode() : 0);
        hash = 29 * hash + (this.parent != null ? this.parent.hashCode() : 0);
        return hash;
    }
}
