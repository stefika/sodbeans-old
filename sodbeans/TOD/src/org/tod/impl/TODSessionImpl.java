/*
 Copyright (c) 2013, Andreas Stefik
 All rights reserved.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met: 

 1. Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer. 
 2. Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution. 

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 The views and conclusions contained in the software and documentation are those
 of the authors and should not be interpreted as representing official policies, 
 either expressed or implied, of the FreeBSD Project.
 */
package org.tod.impl;

import org.tod.impl.execution.DefaultExecutionTracker;
import org.tod.meta.variables.impl.DefaultVariablesWatcher;
import org.tod.meta.impl.DefaultThreadTracker;
import org.tod.meta.impl.NullClassInformationProvider;
import org.tod.jvm.impl.DefaultJVMHandler;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import org.tod.api.ActionHandler;
import org.tod.meta.ClassInformationProvider;
import org.tod.api.DebugEventListener;
import org.tod.api.ExecutionTracker;
import org.tod.api.Filter;
import org.tod.jvm.api.JVMHandler;
import org.tod.api.TODHandler;
import org.tod.api.TODSession;
import org.tod.jvm.impl.JVMEventDispatcher;
import org.tod.meta.ThreadTracker;
import org.tod.meta.variables.TypeLookupManager;
import org.tod.meta.variables.VariablesWatcher;

/**
 *
 * @author jwilson
 */
public final class TODSessionImpl implements TODSession {

    private ActionHandler actionHandler = new DefaultActionHandler();
    private JVMHandler jvmHandler = null;
    private TODHandler todHandler = new DefaultTODHandler();
    private ClassInformationProvider classInformationProvider = new NullClassInformationProvider();
    private ThreadTracker threadTracker = new DefaultThreadTracker();
    private DefaultExecutionTracker executionTracker = new DefaultExecutionTracker();
    private DefaultVariablesWatcher variablesWatcher = new DefaultVariablesWatcher();
    private Filter filter = new NullFilter();
    private File mainClassLocation = null;
    private String jar = null;
    private TypeLookupManager typeLookupManager = new TypeLookupManager(); // use the default.
    private ArrayList<DebugEventListener> eventListeners = new ArrayList<DebugEventListener>();
    protected boolean active = false;

    public TODSessionImpl() {
        jvmHandler = new DefaultJVMHandler(this);
        todHandler.setTODSession(this);
        actionHandler.setTODSession(this);
        jvmHandler.setTODSession(this);
        threadTracker.setTODSession(this);
        executionTracker.setTODSession(this);
        variablesWatcher.setTODSession(this);
        jvmHandler.addListener(new JVMEventDispatcher(this));
    }

    @Override
    public void setMainClassLocation(File mainClassLocation) {
        this.mainClassLocation = mainClassLocation;
    }

    @Override
    public void setJar(String jar) {
        this.jar = jar;
    }

    @Override
    public File getMainClassLocation() {
        return this.mainClassLocation;
    }

    @Override
    public String getJar() {
        return this.jar;
    }

    @Override
    public void setActionHandler(ActionHandler handler) {
        handler.setTODSession(this);
        this.actionHandler = handler;
    }

    @Override
    public ActionHandler getActionHandler() {
        return this.actionHandler;
    }

    @Override
    public ExecutionTracker getExecutionTracker() {
        return this.executionTracker;
    }

    @Override
    public void addEventListener(DebugEventListener listener) {
        // Don't add any listener twice.
        if (!this.eventListeners.contains(listener)) {
            listener.setTODSession(this);
            this.eventListeners.add(listener);
        }
    }

    @Override
    public void removeEventListener(DebugEventListener listener) {
        this.eventListeners.remove(listener);
    }

    @Override
    public Iterator<DebugEventListener> getEventListeners() {
        return this.eventListeners.iterator(); // TODO: Dangerous
    }

    @Override
    public JVMHandler getJVMHandler() {
        return this.jvmHandler;
    }

    @Override
    public ThreadTracker getThreadTracker() {
        return this.threadTracker;
    }

    @Override
    public TODHandler getTODHandler() {
        return this.todHandler;
    }

    @Override
    public VariablesWatcher getVarialesWatcher() {
        return this.variablesWatcher;
    }

    @Override
    public boolean isActive() {
        return this.getJVMHandler().isAlive() && this.getTODHandler().isAlive();
    }

    @Override
    public void setClassInformationProvider(ClassInformationProvider info) {
        if (info != null) {
            this.classInformationProvider = info;
        }
    }

    @Override
    public ClassInformationProvider getClassInformationProvider() {
        return this.classInformationProvider;
    }

    @Override
    public Filter getFilter() {
        return this.filter;
    }

    @Override
    public void setFilter(Filter filter) {
        if (filter != null) {
            this.filter = filter;
        }
    }

    @Override
    public TypeLookupManager getTypeLookupManager() {
        return this.typeLookupManager;
    }

    @Override
    public void setTypeLookupManager(TypeLookupManager manager) {
        if (manager != null) {
            this.typeLookupManager = manager;
        }
    }
}
