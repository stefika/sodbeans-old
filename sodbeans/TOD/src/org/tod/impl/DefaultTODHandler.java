/*
 Copyright (c) 2013, Andreas Stefik
 All rights reserved.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met: 

 1. Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer. 
 2. Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution. 

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 The views and conclusions contained in the software and documentation are those
 of the authors and should not be interpreted as representing official policies, 
 either expressed or implied, of the FreeBSD Project.
 */
package org.tod.impl;

import org.tod.api.AbstractSessionTracker;
import com.sun.jdi.ClassNotLoadedException;
import com.sun.jdi.Method;
import com.sun.jdi.ThreadReference;
import com.sun.jdi.Type;
import java.io.File;
import java.net.URI;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.tod.TODUtils;
import org.tod.api.TODHandler;
import org.tod.meta.MethodInfo;
import org.tod.meta.impl.TypeUtils;
import tod.core.config.TODConfig;
import tod.core.database.browser.IEventBrowser;
import tod.core.database.browser.IEventFilter;
import tod.core.database.browser.ILogBrowser;
import tod.core.database.event.ICallerSideEvent;
import tod.core.database.event.ILogEvent;
import tod.core.database.event.IMethodCallEvent;
import tod.core.database.structure.IBehaviorInfo;
import tod.core.database.structure.IClassInfo;
import tod.core.database.structure.IStructureDatabase;
import tod.core.database.structure.IThreadInfo;
import tod.core.database.structure.ITypeInfo;
import tod.core.database.structure.ObjectId;
import tod.core.session.ISession;
import tod.core.session.SessionTypeManager;
import tod.impl.common.event.BehaviorExitEvent;
import tod.impl.dbgrid.DBProcessManager;
import tod.impl.dbgrid.aggregator.GridEventBrowser;

/**
 *
 * @author User
 */
public class DefaultTODHandler extends AbstractSessionTracker implements TODHandler {

    private ISession todSession = null;
    private DBProcessManager processManager = null;
    private TODConfig todConfig = new TODConfig();

    @Override
    public boolean start() {
        try {
            // Start TOD.
            if (this.processManager == null) {
                DBProcessManager.lib = TODUtils.getTODJARPath();
                DBProcessManager.cp += File.pathSeparator + TODUtils.getTODJARPath() + "asm-all-3.2-svn.jar";
                DBProcessManager.cp += File.pathSeparator + TODUtils.getTODJARPath() + "tod-debugger.jar";
                DBProcessManager.cp += File.pathSeparator + TODUtils.getTODJARPath() + "tod-dbgrid.jar";
                DBProcessManager.cp += File.pathSeparator + TODUtils.getTODJARPath() + "tod-evdb1.jar";
                DBProcessManager.cp += File.pathSeparator + TODUtils.getTODJARPath() + "tod-agent-custom.jar";
                DBProcessManager.cp += File.pathSeparator + TODUtils.getTODJARPath() + "zz.utils.jar";
                DBProcessManager.cp += File.pathSeparator + TODUtils.getTODJARPath() + "aspectjrt.jar";

                URI todURI = URI.create("tod-dbgrid-remote:localhost");
                todConfig = new TODConfig();
                //todConfig.set(TODConfig.INSTRUMENTER_CLASSES_DIR, "/Users/jeff/Desktop/quorum2");
                todConfig.set(TODConfig.SCOPE_TRACE_FILTER, "[+quorum.** -com.** -sun.** -java.**]");
                todConfig.set(TODConfig.AGENT_SKIP_CORE_CLASSE, true);
                todConfig.set(TODConfig.SCOPE_GLOBAL_FILTER, "[-java.** -sun.** -com.**]");
                todConfig.set(TODConfig.AGENT_CAPTURE_EXCEPTIONS, true);
                todConfig.set(TODConfig.DB_AUTOFLUSH_DELAY, 100);
                this.processManager = new DBProcessManager(this.todConfig);
                this.processManager.start();
                this.todSession = SessionTypeManager.getInstance().createSession(null, todURI, this.todConfig);
            } else {
                this.processManager.getMaster().clear();
            }
        } catch (Exception ex) {
            Logger logger = Logger.getLogger(this.getClass().getName());
            logger.log(Level.SEVERE, "Exception in TOD Bootup");
        }
        return true;
    }

    @Override
    public boolean stop() {
        if (this.processManager != null && this.todSession != null) {
            try {
                // Stop TOD.
                this.processManager.stop();
                this.processManager = null;
                this.todSession = null;
            } catch (RuntimeException ex) {
                // Disconnect from the VM.
            }
            return true;
        } else {
            return false;
        }
    }

    @Override
    public IThreadInfo lookupThread(ThreadReference reference) {
        Iterator<IThreadInfo> threads = this.todSession.getLogBrowser().getThreads().iterator();

        while (threads.hasNext()) {
            IThreadInfo thread = threads.next();

            if (thread.getJVMId() == reference.uniqueID()) {
                return thread;
            }
        }

        // The thread couldn't be found--this shouldn't happen under normal
        // circumstances.
        return null;
    }

    @Override
    public IBehaviorInfo getMethodBehavior(Method method) {
        try {
            String className = method.declaringType().name();
            IStructureDatabase db = this.todSession.getLogBrowser().getStructureDatabase();
            IClassInfo cls = db.getClass(className, false);
            List<Type> argumentTypes = method.argumentTypes();
            ITypeInfo[] args = new ITypeInfo[argumentTypes.size()];
            for (int i = 0; i < argumentTypes.size(); i++) {
                args[i] = db.getType(argumentTypes.get(i).signature(), false);
            }
            ITypeInfo ret = db.getType(method.returnType().signature(), false);
            IBehaviorInfo behavior = cls.getBehavior(method.name(), args, ret);
            return behavior;
        } catch (ClassNotLoadedException ex) {
            return null;
        }
    }

    @Override
    public Object getObjectById(ObjectId oid) {
        return this.todSession.getLogBrowser().getRegistered(oid);
    }

    @Override
    public IEventBrowser getStepOverEventBrowser() {
        return getStepOverEventBrowser(null);
    }
    @Override
    public IEventBrowser getStepOverEventBrowser(ThreadReference thread) {
        return getStepOverEventBrowser(thread, -1);
    }

    @Override
    public IEventBrowser getStepOverEventBrowser(ThreadReference thread, int depth) {
        try {
            // This helps ensure any new events have safely arrived.
            Thread.sleep(100);
        } catch (InterruptedException ex) {
            Logger.getLogger(DefaultTODHandler.class.getName()).log(Level.SEVERE, null, ex);
        }

        this.processManager.getMaster().flush();
        ILogBrowser browser = this.todSession.getLogBrowser();
        
        if (thread == null) {
            return browser.createBrowser().clone();
        }
        
        // Create a thread filter.
        IThreadInfo todThread = this.getTODSession().getThreadTracker().getByReference(thread).getTODThreadReference();
        if (todThread == null) {
            return null;
        }

        IEventFilter filter = browser.createThreadFilter(todThread);
        /*IEventFilter bcf = browser.createBehaviorCallFilter();
         IEventFilter vwf = browser.createVariableWriteFilter();
         IEventFilter fwf = browser.createFieldWriteFilter();*/
        // If the depth is known, limit our scope to it.
        if (depth != -1) {
            IEventFilter depthFilter = browser.createDepthFilter(depth);
            IEventFilter depthFilter2 = browser.createDepthFilter(depth - 1);
            IEventFilter tmpFilter = browser.createUnionFilter(depthFilter, depthFilter2);
            filter = browser.createIntersectionFilter(filter, tmpFilter);
        }
        //IEventFilter filter2 = browser.createUnionFilter(bcf, vwf, fwf);
        //filter = browser.createIntersectionFilter(filter, filter2);

        IEventBrowser clone = browser.createBrowser(filter).clone();
        return clone;
    }

    @Override
    public List<MethodInfo> getCallStack(ILogEvent event, boolean isStepInto, boolean isForward, boolean isLastEvent) {
        if (!(event instanceof ICallerSideEvent)) {
            return new LinkedList<MethodInfo>();
        }
        
        LinkedList<MethodInfo> stack = new LinkedList<MethodInfo>();
        boolean loggedMethod = false;
        if (event instanceof IMethodCallEvent) {
            IMethodCallEvent mce = (IMethodCallEvent)event;
            if (mce.getCalledBehavior() != null && isForward) {
                IBehaviorInfo b = mce.getCalledBehavior();
                MethodInfo i = getTODSession().getClassInformationProvider().getMethodInfo(b);
                mce.getOperationBytecodeIndex();
                int n = TypeUtils.calculateLineNumber(b, mce.getOperationBytecodeIndex());
                i.setLineNumber(n);
                stack.add(i);
                loggedMethod = true;
            }
        }
        ICallerSideEvent currentEvent = (ICallerSideEvent)event;
        boolean isFirst = true;
        do {
            if (currentEvent.getOperationBehavior() != null) {
                IBehaviorInfo method = currentEvent.getOperationBehavior();
                MethodInfo i = getTODSession().getClassInformationProvider().getMethodInfo(method);
                int n = TypeUtils.calculateLineNumber(method, currentEvent.getOperationBytecodeIndex());
                //we really need the current location not some calculated position...is this possible?
                i.setLineNumber(n);
                if(!isStepInto && isFirst && isForward && event instanceof IMethodCallEvent){
                    isFirst = false;
                }else{
                    stack.add(i);
                }
                
            }
            currentEvent = currentEvent.getParent();
        } while (currentEvent != null);
         
        return stack;
    }

    @Override
    public long getEventCount(ThreadReference thread) {
        if (this.processManager == null) {
            return 0;
        }

        // Perform a flush.
        this.processManager.getMaster().flush();

        // Create a thread filter.
        ILogBrowser browser = this.todSession.getLogBrowser();
        IThreadInfo todThread = this.getTODSession().getThreadTracker().getByReference(thread).getTODThreadReference();
        if (todThread == null) {
            return 0;
        }
        IEventFilter filter = browser.createThreadFilter(todThread);
        IEventBrowser events = browser.createBrowser(filter);
        return events.getEventCount();
    }

    @Override
    public IEventBrowser getChildrenBrowser(ICallerSideEvent parent) {
        ILogBrowser logBrowser = this.todSession.getLogBrowser();
        IEventFilter depth1 = logBrowser.createDepthFilter(parent.getDepth());
        IEventFilter depth2 = logBrowser.createDepthFilter(parent.getDepth() + 1);
        IEventFilter depthFilter = logBrowser.createUnionFilter(depth1, depth2);
        IEventFilter childFilter = logBrowser.createIntersectionFilter(depthFilter,
                logBrowser.createThreadFilter(parent.getThread()));
        IEventBrowser events = logBrowser.createBrowser(childFilter);
        events.setPreviousEvent(parent);

        if (!events.hasNext()) {
            return events;
        }

        if (events instanceof GridEventBrowser) {
            GridEventBrowser gridEvents = (GridEventBrowser) events;

            // The first event here will always be the parent
            ILogEvent firstEvent = events.next();

            ILogEvent lastEvent = firstEvent;
            while (events.hasNext() && !(lastEvent instanceof BehaviorExitEvent)) {
                lastEvent = events.next();
            }

            gridEvents.setBounds(firstEvent, lastEvent);
            gridEvents.setNextEvent(firstEvent);
            return gridEvents;
        }

        return null;
    }

    @Override
    public IEventBrowser getStepIntoChildrenBrowser(ICallerSideEvent parent) {
        ILogBrowser logBrowser = this.todSession.getLogBrowser();
        IEventFilter depthFilter = logBrowser.createDepthFilter(parent.getDepth() + 1);
        IEventFilter childFilter = logBrowser.createIntersectionFilter(depthFilter,
                logBrowser.createThreadFilter(parent.getThread()));
        IEventBrowser events = logBrowser.createBrowser(childFilter);
        events.setPreviousEvent(parent);

        if (!events.hasNext()) {
            return events;
        }

        if (events instanceof GridEventBrowser) {
            GridEventBrowser gridEvents = (GridEventBrowser) events;

            // The first event here will always be the parent
            ILogEvent firstEvent = events.next();

            ILogEvent lastEvent = firstEvent;
            while (events.hasNext() && !(lastEvent instanceof BehaviorExitEvent)) {
                lastEvent = events.next();
            }

            gridEvents.setBounds(firstEvent, lastEvent);
            gridEvents.setNextEvent(firstEvent);
            return gridEvents;
        }

        return null;
    }

    @Override
    public IEventBrowser filterLocalVariableWrites(IEventBrowser browser) {
        IEventFilter localsFilter = browser.getLogBrowser().createVariableWriteFilter();
        IEventBrowser newBrowser = browser.createIntersection(localsFilter);

        // Fixes a bug in TOD where hasNext() incorrectly returns false....
        newBrowser.setNextTimestamp(0);

        return newBrowser;
    }

    @Override
    public IEventBrowser getThreadBrowser(IThreadInfo thread) {
        IEventFilter threadFilter = this.todSession.getLogBrowser().createThreadFilter(thread);

        return this.todSession.getLogBrowser().createBrowser(threadFilter);
    }

    @Override
    public IEventBrowser filterFieldWrites(IEventBrowser browser) {
        IEventFilter fieldFilter = browser.getLogBrowser().createFieldWriteFilter();
        IEventBrowser newBrowser = browser.createIntersection(fieldFilter);

        // Fixes a bug in TOD where hasNext() incorrectly returns false....
        newBrowser.setNextTimestamp(0);

        return newBrowser;
    }

    @Override
    public IEventBrowser filterMethodCalls(IEventBrowser browser) {
        IEventFilter methodFilter = browser.getLogBrowser().createBehaviorCallFilter();
        IEventBrowser newBrowser = browser.createIntersection(methodFilter);

        // Fixes a bug in TOD where hasNext() incorrectly returns false....
        newBrowser.setNextTimestamp(0);

        return newBrowser;
    }

    @Override
    public IEventBrowser getObjectBrowser(ObjectId todObject) {
        IEventFilter objectFilter = this.todSession.getLogBrowser().createObjectFilter(todObject);

        return this.todSession.getLogBrowser().createBrowser(objectFilter);
    }

    @Override
    public IEventBrowser filterInstantiations(IEventBrowser objectBrowser) {
        IEventFilter instFilter = objectBrowser.getLogBrowser().createInstantiationsFilter();
        IEventBrowser newBrowser = objectBrowser.createIntersection(instFilter);

        // Fixes a bug in TOD where hasNext() incorrectly returns false....
        newBrowser.setNextTimestamp(0);

        return newBrowser;
    }

    @Override
    public boolean isAlive() {
        return this.processManager != null;
    }
    
    @Override
    public void flush() {
        this.processManager.getMaster().flush();
    }
}
