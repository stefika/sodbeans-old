/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.tod.impl.execution;

import com.sun.jdi.IncompatibleThreadStateException;
import com.sun.jdi.Location;
import com.sun.jdi.Method;
import com.sun.jdi.ThreadReference;
import com.sun.jdi.event.BreakpointEvent;
import com.sun.jdi.event.StepEvent;
import com.sun.jdi.request.BreakpointRequest;
import com.sun.jdi.request.StepRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.objectweb.asm.Opcodes;
import org.tod.api.AbstractSessionTracker;
import org.tod.api.DebugEventListener;
import org.tod.api.ExecutionTracker;
import org.tod.api.TODSession;
import org.tod.jvm.api.JVMListener;
import org.tod.meta.BreakpointInfo;
import org.tod.meta.ClassInfo;
import org.tod.meta.MethodInfo;
import org.tod.meta.StateInfo;
import org.tod.meta.impl.TypeUtils;
import org.tod.meta.sourcecode.BlockScope;
import org.tod.meta.sourcecode.SourceInfo;
import org.tod.meta.steps.ProgramStep;
import org.tod.meta.steps.impl.ProgramStepFactory;
import tod.core.database.browser.IEventBrowser;
import tod.core.database.event.ExternalPointer;
import tod.core.database.event.IBehaviorCallEvent;
import tod.core.database.event.ICallerSideEvent;
import tod.core.database.event.ILogEvent;
import tod.core.database.event.IMethodCallEvent;
import tod.core.database.structure.IBehaviorInfo;
import tod.core.database.structure.IClassInfo;
import tod.impl.common.event.BehaviorExitEvent;
import tod.impl.common.event.ExceptionGeneratedEvent;
import tod.impl.common.event.OpcodeEvent;
import tod.impl.dbgrid.event.InstantiationEvent;
import tod.impl.dbgrid.event.MethodCallEvent;
import tod.impl.local.event.ConstructorChainingEvent;

/**
 *
 * @author jeff and Melissa Stefik
 */
public class DefaultExecutionTracker extends AbstractSessionTracker implements ExecutionTracker {

    /**
     * Our virtual machine listener.
     */
    private ExecutionListener listener = new ExecutionListener();
    /**
     * The thread we are tracking.
     */
    private ThreadReference thread = null;
    /**
     * This thread's behavior.
     */
    private IBehaviorInfo behavior = null;
    /**
     * The event browser for this thread.
     */
    private IEventBrowser events = null;
    
    /**
     * Our current line number.
     */
    private int lineNumber = -1;
    /**
     * Our last recorded event from TOD.
     */
    private ICallerSideEvent lastEvent = null;
    private boolean runningLive = false;
    private int liveLine = -1;

    /**
     * Stores depth information calculated by {@link DefaultExecutionTracker#computeDepth(tod.core.database.event.ILogEvent)},
     * as this method is very expensive and was resulting in severe slowdowns. Caching depths results in a speedup from
     * 4000ms to about 200ms for a typical step-over.
     */
    private HashMap<ExternalPointer, Integer> depths = new HashMap<ExternalPointer, Integer>();
    private IBehaviorInfo liveBehavior;
    
    @Override
    public boolean stepOver() {
        // Should we ask the virtual machine to step over for us?
        if (runningLive || isLive()) {
            // Ask the virtual machine to step over.
            
            // If we are currently on a line marked as a return (such as a 'return now' line),
            // then we must perform step over TWICE, to get around the odd way in which TOD
            // instruments the bytecode.
            if(this.behavior != null && this.lineNumber >= 0) {
                ClassInfo classInfo = this.getTODSession().getClassInformationProvider().getClassInfo(behavior.getDeclaringType());
                SourceInfo sourceInfo = classInfo.getSourceInfo();
                
                if (sourceInfo.isActionEnd(this.lineNumber) || sourceInfo.isReturn(this.lineNumber)) {
                    // Step over twice.
                    if (this.behavior.getName().equals("Main") || this.behavior.getName().equals("main")) {
                        return false;
                    }
                    this.getTODSession().getJVMHandler().stepOver(this.thread, 2);
                } else {
                    // Step over once.
                    this.getTODSession().getJVMHandler().stepOver(this.thread, 1);
                }
            } else {
                // Step over once.
                this.getTODSession().getJVMHandler().stepOver(this.thread);
            }
        } else {
            SearchResult result = this.stepOverEvents(computeDepth(this.lastEvent));
            this.update(result, false, true);
            this.fireStepOverToListeners(result);
        }
        
        return true;
    }

    /**
     * Step over TOD events, generating a list of program events and a new line
     * number.
     *
     * @param depth the maximum allowable stack depth.
     * @return
     */
    private SearchResult stepOverEvents(int depth) {
        return stepOverEvents(depth, lineNumber, -1);
    }

    /**
     * Step over TOD events, generating a list of program events and a new line
     * number.
     *
     * @param depth the maximum allowable stack depth.
     * @param targetLine the current line number of the active method, or -1 if
     * the active method and line number in the JVM is known
     *
     * @return
     */
    private SearchResult stepOverEvents(int depth, int currentLine, int destinationLine) {
        // Our current (but soon to be last) line number.
        int lastLine = currentLine;
        
        // Store each step we go over.
        LinkedList<ProgramStep> steps = new LinkedList<ProgramStep>();

        // The result we'll send back to our caller.
        SearchResult result = new SearchResult();

        // Get the events from TOD.
        IEventBrowser newEvents = getTODSession().getTODHandler().getStepOverEventBrowser(thread);

        ICallerSideEvent event = null;
        ICallerSideEvent previousEvent = null;
        if (newEvents != null) {
            this.events = newEvents;
            // Move the event pointer past the last event we saw.
            if (lastEvent != null && runningLive) {
                events.setPreviousTimestamp(lastEvent.getTimestamp());
            } else if (lastEvent != null) {
                events.setNextTimestamp(lastEvent.getTimestamp());
            }
        }

        int lineNum = currentLine;
        if (this.events != null && this.events.hasNext()) {
            // Go forward
            do {
                ILogEvent ile = events.next();
                if (skippable(ile) && !ile.getParent().equals(lastEvent)) {
                    if (ile instanceof ICallerSideEvent) {
                        event = (ICallerSideEvent)ile;
                    }
                    continue;
                }
                int ileDepth = computeDepth(ile);
                if (depth != -1 && ileDepth != depth && ileDepth != depth - 1) {
                    continue;
                }
                
                if (ile instanceof ICallerSideEvent) {
                    event = (ICallerSideEvent) ile;
                    if (event.getOperationBehavior() != null) {
                        lineNum = TypeUtils.calculateLineNumber(event.getOperationBehavior(), event.getOperationBytecodeIndex());
                        // Generate the program event.
                        ProgramStep step = ProgramStepFactory.stepFromTOD(getTODSession(), event, true, runningLive);
                        if (step != null && lineNum == currentLine) {
                            steps.addFirst(step);
                        }

                        if (previousEvent == null && event.getOperationBehavior() != null) {
                            previousEvent = event;
                        }
                    }
                }
            } while (lineNum == currentLine && events.hasNext()); //&& (isEventOnLine(event, currentLine) || this.isEventBeforeLine(event, currentLine)));
        }

        if (destinationLine == -1 && event != null) {
            result.newLine = TypeUtils.calculateLineNumber(event.getOperationBehavior(), event.getOperationBytecodeIndex());
        } else {
            result.newLine = destinationLine;
        }

        if (previousEvent == null) {
            result.behavior = this.behavior; // no events were found
            result.targetEvent = event;
            result.lastEvent = event;
        } else {
            result.targetEvent = event;
            result.behavior = event.getOperationBehavior();
            result.lastEvent = previousEvent;
        }
        
        if (events.hasNext()) {
            result.afterLastEvent = events.next();
        }
        
        if (!runningLive && !isLive()) {
            runningLive = (result.lastEvent == null) || (result.lastEvent.equals(lastEvent) && !events.hasNext() && result.newLine == liveLine ) || (lineNumber == result.newLine && !events.hasNext());
            if (runningLive) {
                result.newLine = liveLine;
                result.behavior = this.liveBehavior;
            }
        }
        
        // Generate the conditional test event, if any.
        ProgramStep conditionalStep = ProgramStepFactory.conditionalStep(session, event, result.newLine, lastLine, true);
        if (conditionalStep != null) {
            steps.addLast(conditionalStep);
        }
        
        // Generate the loop conditional test event, if any.
        ProgramStep loopStep = ProgramStepFactory.loopStep(session, event, result.newLine, lastLine, true);
        if (loopStep != null) {
            steps.addLast(loopStep);
        }
        
        result.steps = steps.iterator();
        long t2 = System.currentTimeMillis();
        return result;
    }

    /**
     * Step backwards in program history, one line at a time.
     *
     * @return false if a new location could not be computed.
     */
    @Override
    public boolean stepBackOver() {
        if (this.lastEvent == null) {
            return false; // no event to go back from.
            
        }

        // No longer using live events.
        this.runningLive = false;
        IEventBrowser lastBrowser = this.events;
        SearchResult result = this.stepBackOverEvents(computeDepth(this.lastEvent));
        
        if (result.newLine == -1) {
            this.events.setPreviousEvent(this.lastEvent);
            return false;
        }
        this.update(result, false, false);
        this.fireStepBackOverToListeners(result);
        
        return true;
    }

    private SearchResult stepBackOverEvents(int depth) {
        int lastLine = lineNumber;
        int targetLine = -1;
        // Store each step we go over.
        LinkedList<ProgramStep> steps = new LinkedList<ProgramStep>();

        // The result we'll send back to our caller.
        SearchResult result = new SearchResult();

        ICallerSideEvent event = null;
        ICallerSideEvent previousEvent = null;
        
        ILogEvent ile;
        ICallerSideEvent validEvent = null;
        if (this.events != null && this.events.hasPrevious()) {
            // Go backwards. First, position the event log at the first line
            // we see that is NOT the line we are currently on.
            ILogEvent pointerEvent = null;
            do {
                ile = events.previous();
                if (depth != -1 && (computeDepth(ile) != depth && computeDepth(ile) != depth - 1)) {
                    continue;
                }
                if (ile instanceof ICallerSideEvent && !(ile instanceof BehaviorExitEvent)) {
                    event = (ICallerSideEvent)ile;
                    int lineNum = TypeUtils.calculateLineNumber(event.getOperationBehavior(), event.getOperationBytecodeIndex());
                    if (skippable(event) || lineNum == -1) {
                        continue;
                    }
                    
                    if (targetLine == -1 && lineNum != lineNumber) {
                        targetLine = lineNum;
                        pointerEvent = event;
                    } else if (targetLine != -1 && targetLine != lineNum) {
                        result.lastEvent = event;
                        break;
                    }
                }
            } while (events.hasPrevious());
            // Reset the event pointer to the first event on the target line.
            if (pointerEvent != null) {
                events.setNextTimestamp(pointerEvent.getTimestamp());
                ile = pointerEvent;
            }
            do {
                if (depth != -1 && computeDepth(ile) != depth && computeDepth(ile) != depth - 1) {
                    ile = events.previous();
                    continue;
                }
                if (ile instanceof ICallerSideEvent && !(ile instanceof BehaviorExitEvent)) {
                    if (!skippable(ile) && this.isEventOnLine(ile, targetLine)) {
                        previousEvent = event;
                        event = (ICallerSideEvent) ile;
                        int lineNum = TypeUtils.calculateLineNumber(event.getOperationBehavior(), event.getOperationBytecodeIndex());
                        if (lineNum != -1 ) {
                            validEvent = event;
                            ProgramStep step = ProgramStepFactory.stepFromTOD(this.getTODSession(), event, false, false);
                            if (step != null) {
                                steps.addLast(step);
                            }
                        }
                    } else if (!this.isEventOnLine(ile, targetLine)) {
                        break;
                    }
                }
                if (events.hasPrevious()) {
                    ile = events.previous();
                }
            } while (events.hasPrevious());
            

            // Don't skip any events! If the last event wasn't on this line,
            // go forward so that it will be seen again.
            if (!this.isEventOnLine(ile, targetLine)) {
                events.next();
            }
        }

        if (validEvent == null) {
            result.behavior = this.behavior; // TODO: might break
        } else {
            result.newLine = TypeUtils.calculateLineNumber(validEvent.getOperationBehavior(), validEvent.getOperationBytecodeIndex());
            result.targetEvent = validEvent;
            result.lastEvent = validEvent;
            result.behavior = validEvent.getOperationBehavior();
        }

        // Generate the conditional test event, if any.
        ProgramStep step = ProgramStepFactory.conditionalStep(session, event, lastLine, result.newLine, false);
        if (step != null) {
            steps.addLast(step);
        }        
        // Generate the loop conditional test event, if any.
        ProgramStep loopStep = ProgramStepFactory.loopStep(session, event, lastLine, result.newLine, false);
        if (loopStep != null) {
            steps.addLast(loopStep);
        }
        
        result.forward = false;
        result.steps = steps.iterator();
        return result;
    }

    @Override
    public boolean stepInto() {
        // Should we ask the virtual machine to step into for us?
        if (runningLive || isLive()) {
            // If we are currently on a line marked as a return (such as a 'return now' line),
            // then we must perform step over TWICE, to get around the odd way in which TOD
            // instruments the bytecode.
            if(this.behavior != null && this.lineNumber >= 0) {
                ClassInfo classInfo = this.getTODSession().getClassInformationProvider().getClassInfo(behavior.getDeclaringType());
                SourceInfo sourceInfo = classInfo.getSourceInfo();
                
                if (sourceInfo.isActionEnd(this.lineNumber) || sourceInfo.isReturn(this.lineNumber)) {
                    if (this.behavior.getName().equals("Main") || this.behavior.getName().equals("main")) {
                        return false;
                    }
                    // Step over twice.
                    this.getTODSession().getJVMHandler().stepInto(this.thread, 2);
                } else {
                    // Step over once.
                    this.getTODSession().getJVMHandler().stepInto(this.thread, 1);
                }
            }
        } else {
            SearchResult result = this.stepOverEvents(-1);
            this.update(result, true, true);
            this.fireStepOverToListeners(result);
        }
        
        return true;
    }

    @Override
    public boolean stepBackInto() {
        if (this.lastEvent == null) {
            return false; // no event to go back from.
        }

        // No longer using live events.
        this.runningLive = false;
        SearchResult result = this.stepBackOverEvents(-1);
        if (result.newLine == -1) {
            this.events.setPreviousEvent(this.lastEvent);
            return false;
        }
        this.update(result, false, false);
        this.fireStepBackOverToListeners(result);
        return true;
    }

    @Override
    public void runForwardToLine(String fullyQualifiedClassName, int line, boolean hitBreakpoints) {
        // Should we ask the virtual machine to run there for us?
        if (runningLive || isLive()) {
            // Ask the virtual machine to run forward until it hits the given
            // line.
            this.getTODSession().getJVMHandler().addLineBreakpoint(fullyQualifiedClassName, line, 1);
            this.getTODSession().getJVMHandler().resume();
        } else {
            // We have to do it ourseves using our own step to line
            // algorithm. 
            SearchResult result = this.stepToLine(fullyQualifiedClassName, line, hitBreakpoints);
            
            // BUT, if this algorithm finds nothing, then we will run the
            // virtual machine.
            if (result.newLine == -1) {
                this.getTODSession().getJVMHandler().addLineBreakpoint(fullyQualifiedClassName, line, 1);
                this.getTODSession().getJVMHandler().resume();
            }
            this.update(result,false, true);
            this.fireStepOverToListeners(result);
        }
    }

    @Override
    public void runBackToLine(String fullyQualifiedClassName, int line, boolean hitBreakpoints) {
        if (this.lastEvent == null) {
            return; // no event to go back from.
            // TODO: Alert the user
        }

        // No longer using live events. Use our own step back to line
        // algorithm.
        this.runningLive = false;
        SearchResult result = this.stepBackToLine(fullyQualifiedClassName, line, hitBreakpoints);
        this.update(result, false, false);
        this.fireStepBackOverToListeners(result);  
    }

    private SearchResult stepToLine(String fullyQualifiedClassName, int line, boolean hitBreakpoints) {
        // The result we'll send back to our caller.
        SearchResult result = new SearchResult();

        // Get the events from TOD.
        IEventBrowser newEvents = getTODSession().getTODHandler().getStepOverEventBrowser(thread);

        ICallerSideEvent event = null;
        ICallerSideEvent previousEvent = null;
        if (newEvents != null) {
            this.events = newEvents;
            // Move the event pointer past the last event we saw.
            if (lastEvent != null && runningLive) {
                events.setPreviousTimestamp(lastEvent.getTimestamp());
            } else if (lastEvent != null) {
                events.setNextTimestamp(lastEvent.getTimestamp());
            }
        }

        // Have we found the start of the given location?
        if (this.events != null && this.events.hasNext()) {
            // Go forward
            do {
                ILogEvent ile = events.next();
                if (ile instanceof ICallerSideEvent) {
                    event = (ICallerSideEvent) ile;
                    if (skippable(event)) {
                        continue;
                    }
                    
                    // Is there a breakpoint, and should we stop at it?
                    if (hitBreakpoints && hasBreakpoint(event)) {
                        return makeSearchResult(event);
                    }
                    
                    if (isAtLocation(event, fullyQualifiedClassName, line)) {
                        previousEvent = event;
                    }
                }
            } while (events.hasNext() && !isAtLocation(event, fullyQualifiedClassName, line));
        }

        if (previousEvent == null) {
            result.behavior = null;
            result.newLine = -1;
            result.targetEvent = null;
        } else {
            result.targetEvent = previousEvent;
            result.behavior = previousEvent.getOperationBehavior();
            result.newLine = TypeUtils.calculateLineNumber(previousEvent.getOperationBehavior(), previousEvent.getOperationBytecodeIndex());
            result.lastEvent = event;
        }
        
        result.steps = (new LinkedList<ProgramStep>()).iterator();
        return result;
    }
    
    private SearchResult stepBackToLine(String fullyQualifiedClassName, int line, boolean hitBreakpoints) {
        // The result we'll send back to our caller.
        SearchResult result = new SearchResult();

        ICallerSideEvent event = null;
        ICallerSideEvent previousEvent = null;
        // Have we found the start of the given location?
        if (this.events != null && this.events.hasPrevious()) {
            // Go backward
            do {
                ILogEvent ile = events.previous();
                if (ile instanceof ICallerSideEvent) {
                    event = (ICallerSideEvent) ile;
                    if (skippable(event)) {
                        continue;
                    }

                    // Is there a breakpoint, and should we stop at it?
                    if (hitBreakpoints && hasBreakpoint(event)) {
                        return makeSearchResult(event);
                    }
                    
                    if (isAtLocation(event, fullyQualifiedClassName, line)) {
                        previousEvent = event;
                    }
                }
            } while (events.hasPrevious() && !isAtLocation(event, fullyQualifiedClassName, line));
            
            do {
                ILogEvent ile = events.previous();
                if (ile instanceof ICallerSideEvent) {
                    event = (ICallerSideEvent) ile;
                    if (skippable(event)) {
                        continue;
                    }
                    
                    if (isAtLocation(event, fullyQualifiedClassName, line)) {
                        previousEvent = event;
                    }
                }
            } while (events.hasPrevious() && isAtLocation(event, fullyQualifiedClassName, line));
        }

        if (previousEvent == null) {
            result.behavior = event.getOperationBehavior();
            result.newLine = TypeUtils.calculateLineNumber(event.getOperationBehavior(), event.getOperationBytecodeIndex());
            result.targetEvent = event;
            result.lastEvent = event;
        } else {
            result.targetEvent = previousEvent;
            result.behavior = previousEvent.getOperationBehavior();
            result.newLine = TypeUtils.calculateLineNumber(previousEvent.getOperationBehavior(), previousEvent.getOperationBytecodeIndex());
            result.lastEvent = event;
        }
        
        result.steps = (new LinkedList<ProgramStep>()).iterator();
        return result;
    }
    
    private SearchResult makeSearchResult(ICallerSideEvent event) {
        SearchResult r = new SearchResult();
        r.behavior = event.getOperationBehavior();
        r.newLine = TypeUtils.calculateLineNumber(event.getOperationBehavior(), event.getOperationBytecodeIndex());
        r.steps = (new ArrayList<ProgramStep>()).iterator();
        r.targetEvent = event;
        return r;
    }
    private synchronized void update(SearchResult result, boolean isStepInto, boolean isForward) {
        this.behavior = result.behavior;
        this.lineNumber = result.newLine;
        if (result.targetEvent != null) {
            this.lastEvent = result.targetEvent;
        }

        // Update the variables window.
        this.getTODSession().getVarialesWatcher().update(result, isStepInto, isForward);
    }

    /**
     * Get the "this" object for the current frame.
     *
     * @return
     */
    @Override
    public Object getThisObject() {
        // TODO: Use TOD for this
        return new Object();
    }

    private void fireStepOverToListeners(SearchResult result) {
        StateInfo info = new StateInfo();
        info.setClassInfo(this.getTODSession().getClassInformationProvider().getClassInfo(result.behavior.getDeclaringType()));
        info.setMethodInfo(this.getTODSession().getClassInformationProvider().getMethodInfo(result.behavior));
        info.setLineNumber(result.newLine);
        info.setThread(this.getTODSession().getThreadTracker().getByReference(this.thread));
        info.setSteps(result.steps);
        Iterator<DebugEventListener> eventListeners = this.getTODSession().getEventListeners();
        while (eventListeners.hasNext()) {
            DebugEventListener next = eventListeners.next();
            next.steppedOver(info);
        }
    }

    private void fireStepBackOverToListeners(SearchResult result) {
        StateInfo info = new StateInfo();
        info.setClassInfo(this.getTODSession().getClassInformationProvider().getClassInfo(result.behavior.getDeclaringType()));
        info.setMethodInfo(this.getTODSession().getClassInformationProvider().getMethodInfo(result.behavior));
        info.setLineNumber(result.newLine);
        info.setThread(this.getTODSession().getThreadTracker().getByReference(this.thread));
        info.setSteps(result.steps);
        Iterator<DebugEventListener> eventListeners = this.getTODSession().getEventListeners();
        while (eventListeners.hasNext()) {
            DebugEventListener next = eventListeners.next();
            next.steppedBackOver(info);
        }
    }

    @Override
    public void reset() {
        this.thread = null;
        this.behavior = null;
        this.events = null;
        this.lineNumber = -1;
        this.lastEvent = null;
        this.runningLive = false;
        this.depths.clear();
    }

    private SearchResult readLastEvent() {
        SearchResult result = new SearchResult();
        LinkedList<ProgramStep> steps = new LinkedList<ProgramStep>();

        // Get the events from TOD.
        IEventBrowser newEvents = this.getTODSession().getTODHandler().getStepOverEventBrowser(this.thread);
        if (newEvents == null) {
            return result;
        }

        this.events = newEvents;

        ICallerSideEvent event = null;
        if (!this.events.hasNext()) {
            return result;
        }

        long lastTimestamp = this.events.getLastTimestamp();
        this.events.setNextTimestamp(lastTimestamp);
        event = (ICallerSideEvent)this.events.next();
        result.steps = steps.iterator();
        result.targetEvent = event;
        result.lastEvent = event;
        return result;
    }

    /**
     * Update the position in execution for the given thread. This method is
     * called when the JVM stops due to either a live step event, or a live
     * breakpoint event.
     *
     * @param thread
     * @param location
     */
    private void stateChanged(ThreadReference thread, Location location) {
        synchronized (this) {
            // The thread in the VM.
            this.thread = thread;

            // Set our current behavior.
            this.behavior = this.getTODSession().getTODHandler().getMethodBehavior(location.method());
            this.liveBehavior = this.behavior;
            
            // Set our current line number.
            this.lineNumber = location.lineNumber();

            // We are now running in a "live" state and not using history.
            this.runningLive = true;
        }
    }

    @Override
    public void run() {
        this.getTODSession().getJVMHandler().resume();
    }

    @Override
    public void resume() {
        this.getTODSession().getJVMHandler().clearStepRequests();
        this.getTODSession().getJVMHandler().resume();
    }

    @Override
    public void pause() {
        this.getTODSession().getJVMHandler().pause();
    }

    private boolean isEventOnLine(ILogEvent event, int lineNumber) {
        if (skippable(event)) {
            return true;
        }
        
        if (!(event instanceof ICallerSideEvent)) {
            return false;
        }
        ICallerSideEvent callerEvent = (ICallerSideEvent)event;
        
        return TypeUtils.calculateLineNumber(callerEvent.getOperationBehavior(), callerEvent.getOperationBytecodeIndex()) == lineNumber;
    }

    private boolean isLive() {
        return liveLine == lineNumber && !events.hasNext();
    }

    private boolean skippable(ILogEvent ile) {
        // Behavior event, but not a normal method call.
        if (ile instanceof IBehaviorCallEvent && !(ile instanceof IMethodCallEvent)) {
            return true;
        }
        else if (ile instanceof InstantiationEvent || ile instanceof ConstructorChainingEvent) {
            return true;
        } else if (ile instanceof OpcodeEvent) {
            OpcodeEvent e = (OpcodeEvent)ile;
            return e.getOpcode() == Opcodes.GOTO;
        } else if (ile instanceof IMethodCallEvent) {
            IMethodCallEvent imce = (IMethodCallEvent)ile;
            IBehaviorInfo executedBehavior = imce.getExecutedBehavior();
            IBehaviorInfo calledBehavior = imce.getCalledBehavior();
            if (executedBehavior == null && calledBehavior == null) {
                return true;
            }
            
            // Use either executedBehavior or calledBehavior, in that order, depending on which is available.
            IBehaviorInfo intendedBehavior = executedBehavior != null ? executedBehavior : calledBehavior;
            MethodInfo methodInfo = getTODSession().getClassInformationProvider().getMethodInfo(intendedBehavior);
            return !getTODSession().getFilter().acceptMethod(methodInfo);
        } else if (ile instanceof ICallerSideEvent) {
            ICallerSideEvent callerSideEvent = (ICallerSideEvent)ile;
            IBehaviorInfo operationBehavior = callerSideEvent.getOperationBehavior();
            if (operationBehavior == null) {
                return true;
            }
            
            // If it is something that occurred in java's SDK, ignore it.

            // Do we wish to filter out events from this method?
            MethodInfo methodInfo = getTODSession().getClassInformationProvider().getMethodInfo(operationBehavior);
            return !getTODSession().getFilter().acceptMethod(methodInfo);
        }
        
        return false;
    }
    
    /**
     * Determine if the given event occurs on the line number in the specified class.
     * 
     * @param event
     * @param fullyQualifiedClassName
     * @param line
     * @return 
     */
    private boolean isAtLocation(ICallerSideEvent event, String fullyQualifiedClassName, int line) {
        if (event.getOperationBehavior() == null) {
            return false;
        }
        
        IClassInfo classInfo = event.getOperationBehavior().getDeclaringType();
        boolean inClass = classInfo.getJvmName().equals("L" + fullyQualifiedClassName + ";");
        boolean onLine = TypeUtils.calculateLineNumber(event.getOperationBehavior(), event.getOperationBytecodeIndex()) == line;
        
        return inClass && onLine;
    }
    
    private boolean hasBreakpoint(ICallerSideEvent event) {
        if (event.getOperationBehavior() == null) {
            return false;
        }
        
        IClassInfo classInfo = event.getOperationBehavior().getDeclaringType();
        String jvmName = classInfo.getJvmName();
        String fullyQualifiedClassName = jvmName.substring(1, jvmName.length() - 1);
        int line = TypeUtils.calculateLineNumber(event.getOperationBehavior(), event.getOperationBytecodeIndex());
        BreakpointInfo breakpoint = this.getTODSession().getJVMHandler().getBreakpoint(fullyQualifiedClassName, line);
        System.out.println("Breakpoint at " + fullyQualifiedClassName + ":" + line + "?");
        return breakpoint != null;
    }

    private int computeDepth(ILogEvent event) {
        if (event == null) {
            return 1;
        }
        
        // Have we calculated this event before?
        if (depths.containsKey(event.getPointer())) {
            return depths.get(event.getPointer());
        }
        
        // Have we calculated the parent before?
        if (depths.containsKey(event.getParentPointer())) {
            int newDepth = depths.get(event.getParentPointer()) + 1;
            depths.put(event.getPointer(), newDepth);
            return newDepth;
        }
        
        int depth = 1;
        ICallerSideEvent parent = event.getParent();
        while (parent != null) {
            if (parent instanceof MethodCallEvent || parent instanceof InstantiationEvent || parent instanceof tod.impl.dbgrid.event.ConstructorChainingEvent) {
                if (parent.getOperationBehavior() != null) {
                    depth++;
                }
            }
            parent = parent.getParent();
        }
        
        // Cache it for next time.
        depths.put(event.getPointer(), depth);
        return depth; 
    }

    /**
     * This class listens for events from the virtual machine, such as step
     * overs, to keep the execution tracker up-to-date with the current state of
     * the virtual machine.
     */
    private class ExecutionListener extends JVMListener {

        @Override
        public void stepEvent(StepEvent e) {
            int lastLine = DefaultExecutionTracker.this.lineNumber;
            DefaultExecutionTracker.this.stateChanged(e.thread(), e.location());
            SearchResult stepOverResult;
            try {
                StepRequest r = (StepRequest) e.request();
                if (r.depth() == StepRequest.STEP_OVER) {
                    stepOverResult = DefaultExecutionTracker.this.stepOverEvents(e.thread().frameCount(), lastLine, e.location().lineNumber());
                    liveLine = e.location().lineNumber();
                    stepOverResult.live = true;
                    stepOverResult.liveStack = e.thread().frames();
                    runningLive = true;
                    stepOverResult.behavior = getTODSession().getTODHandler().getMethodBehavior(e.location().method());
                    update(stepOverResult, false, true);
                    fireStepOverToListeners(stepOverResult);
                } else if (r.depth() == StepRequest.STEP_INTO) {
                    stepOverResult = DefaultExecutionTracker.this.stepOverEvents(e.thread().frameCount(), lastLine, e.location().lineNumber());
                    liveLine = e.location().lineNumber();
                    stepOverResult.behavior = getTODSession().getTODHandler().getMethodBehavior(e.location().method());
                    stepOverResult.live = true;
                    stepOverResult.liveStack = e.thread().frames();
                    runningLive = true;
                    update(stepOverResult, true, true);
                    fireStepOverToListeners(stepOverResult);
                }

            } catch (IncompatibleThreadStateException ex) {
                Logger.getLogger(DefaultExecutionTracker.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        @Override
        public void breakpointEvent(BreakpointEvent e) {
            DefaultExecutionTracker.this.stateChanged(e.thread(), e.location());
            // Delay before reading the last event, to ensure the events
            // have arrived.
            try {
                Thread.sleep(100);
            } catch (InterruptedException ex) {
                Logger.getLogger(DefaultExecutionTracker.class.getName()).log(Level.SEVERE, null, ex);
            }
            SearchResult result = readLastEvent();
            liveLine = e.location().lineNumber();
            result.live = true;
            result.behavior = DefaultExecutionTracker.this.behavior;
            result.newLine = DefaultExecutionTracker.this.lineNumber;
            try {
                result.liveStack = e.thread().frames();
            } catch (IncompatibleThreadStateException ex) {
                Logger.getLogger(DefaultExecutionTracker.class.getName()).log(Level.SEVERE, null, ex);
            }
            DefaultExecutionTracker.this.fireStepOverToListeners(result);
            DefaultExecutionTracker.this.lastEvent = result.targetEvent;
            DefaultExecutionTracker.this.getTODSession().getVarialesWatcher().update(result, false, true);
        }
    }

    @Override
    public void setTODSession(TODSession session) {
        this.session = session;

        // Associate our listener with the virtual machine.
        this.session.getJVMHandler().removeListener(listener);
        this.session.getJVMHandler().addListener(listener);
    }
}