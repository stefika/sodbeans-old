/*
 Copyright (c) 2013, Andreas Stefik
 All rights reserved.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met: 

 1. Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer. 
 2. Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution. 

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 The views and conclusions contained in the software and documentation are those
 of the authors and should not be interpreted as representing official policies, 
 either expressed or implied, of the FreeBSD Project.
 */
package org.tod.impl;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.sodbeans.phonemic.SpeechPriority;
import org.sodbeans.phonemic.TextToSpeechFactory;
import org.sodbeans.phonemic.tts.TextToSpeech;
import org.sodbeans.tts.options.api.TextToSpeechOptions;
import org.tod.api.AbstractSessionTracker;
import org.tod.api.ActionHandler;
import org.tod.meta.ClassInfo;
import org.tod.meta.impl.TypeUtils;
import tod.core.database.browser.IEventBrowser;
import tod.core.database.event.IBehaviorCallEvent;
import tod.core.database.event.ICallerSideEvent;
import tod.core.database.event.ILogEvent;
import tod.core.database.event.IMethodCallEvent;
import tod.core.database.structure.IBehaviorInfo;
import tod.core.database.structure.IClassInfo;

/**
 *
 * @author jeff
 */
public class DefaultActionHandler extends AbstractSessionTracker implements ActionHandler {
    // Dispatch each event to a cached thread pool to get them off of the
    // event dispatch thread

    private TextToSpeech speech = TextToSpeechFactory.getDefaultTextToSpeech();
    private ExecutorService executor = Executors.newSingleThreadExecutor();

    @Override
    public void run() {
        executor.submit(new Runnable() {
            @Override
            public void run() {
                getTODSession().getExecutionTracker().run();
            }
        });
    }

    @Override
    public void pause() {
        executor.submit(new Runnable() {
            @Override
            public void run() {
                getTODSession().getExecutionTracker().pause();
            }
        });
    }

    @Override
    public void resume() {
        executor.submit(new Runnable() {
            @Override
            public void run() {
                getTODSession().getExecutionTracker().resume();
            }
        });
    }

    @Override
    public void start(final String[] programArguments) {
        executor.submit(new Runnable() {
            @Override
            public void run() {
                // Launch the TOD database server.
                boolean started = getTODSession().getTODHandler().start();

                if (started) {
                    // Launch the Java Virtual Machine.
                    getTODSession().getJVMHandler().launchJVM(programArguments);
                }
            }
        });
    }

    @Override
    public void stop() {
        executor.submit(new Runnable() {
            @Override
            public void run() {
                // Disconnect TOD from the virtual machine.
                getTODSession().getTODHandler().stop();

                // Terminate the virtual machine.
                getTODSession().getJVMHandler().stop();

                // Reset the class information provider.
                getTODSession().getClassInformationProvider().reset();

                // Set the execution tracker back to its default state.
                getTODSession().getExecutionTracker().reset();
            }
        });

    }

    @Override
    public void rewind(boolean hitBreakpoints) {
        // Find the method and line number that corresponds to a method
        // call to "main" or "Main" and comes from public static void main(...)
        // in a quorum.* class. We search from the beginning of time in execution
        // history, and don't pay any attention to the thread of execution in
        // this case, as quorum is only called from the thread called "main."
        String mainClassName = null;
        int mainFirstLine = -1;

        IEventBrowser browser = getTODSession().getTODHandler().getStepOverEventBrowser();
        while (browser.hasNext()) {
            ILogEvent event = browser.next();
            if (event instanceof IMethodCallEvent) {
                IMethodCallEvent mce = (IMethodCallEvent) event;
                IBehaviorInfo called = mce.getCalledBehavior();
                if (called != null) {
                    String calledSignature = called.getName() + called.getSignature();
                    String calledClass = called.getDeclaringType().getName();
                    if (calledClass.startsWith("quorum.") && (calledSignature.equals("main()V") || calledSignature.equals("Main()V"))) {
                        if (browser.hasNext()) {
                            ILogEvent next = browser.next();
                            if (next instanceof ICallerSideEvent) {
                                ICallerSideEvent e = (ICallerSideEvent) next;
                                IClassInfo c = called.getDeclaringType();
                                ClassInfo ci = getTODSession().getClassInformationProvider().getClassInfo(c);
                                mainClassName = ci.getFullyQualifiedName();
                                mainFirstLine = TypeUtils.calculateLineNumber(called, e.getOperationBytecodeIndex());
                                break;
                            }
                        }
                    }
                }
            }
        }

        // Now, tell the execution tracker to run back, if we found main.
        if (mainClassName != null && mainFirstLine != -1) {
            this.runBackToLine(mainClassName, mainFirstLine, hitBreakpoints);
        }

    }

    @Override
    public void stepOver() {
        executor.submit(new Runnable() {
            @Override
            public void run() {
                boolean stepOver = getTODSession().getExecutionTracker().stepOver();
                if (!stepOver && TextToSpeechOptions.isScreenReading()) {
                    speech.speak("Can't step forward. You are at the end of the execution.", SpeechPriority.MEDIUM_HIGH);
                }
            }
        });
    }

    @Override
    public void stepBackOver() {
        executor.submit(new Runnable() {
            @Override
            public void run() {
                boolean stepBackOver = getTODSession().getExecutionTracker().stepBackOver();

                if (!stepBackOver && TextToSpeechOptions.isScreenReading()) {
                    speech.speak("Can't back up. You are at the beginning of the execution.", SpeechPriority.MEDIUM_HIGH);
                }
            }
        });
    }

    @Override
    public void stepInto() {
        executor.submit(new Runnable() {
            @Override
            public void run() {
                boolean stepInto = getTODSession().getExecutionTracker().stepInto();
                if (!stepInto && TextToSpeechOptions.isScreenReading()) {
                    speech.speak("Can't step forward. You are at the end of the execution.", SpeechPriority.MEDIUM_HIGH);
                }
            }
        });
    }

    @Override
    public void stepBackInto() {
        executor.submit(new Runnable() {
            @Override
            public void run() {
                boolean stepBackInto = getTODSession().getExecutionTracker().stepBackInto();
                if (!stepBackInto && TextToSpeechOptions.isScreenReading()) {
                    speech.speak("Can't back up. You are at the beginning of the execution.", SpeechPriority.MEDIUM_HIGH);
                }
            }
        });
    }

    @Override
    public void runForwardToLine(final String fullyQualifiedClassName, final int lineNumber, final boolean hitBreakpoints) {
        executor.submit(new Runnable() {
            @Override
            public void run() {
                getTODSession().getExecutionTracker().runForwardToLine(fullyQualifiedClassName, lineNumber, hitBreakpoints);
            }
        });
    }

    @Override
    public void runBackToLine(final String fullyQualifiedClassName, final int lineNumber, final boolean hitBreakpoints) {
        executor.submit(new Runnable() {
            @Override
            public void run() {
                getTODSession().getExecutionTracker().runBackToLine(fullyQualifiedClassName, lineNumber, hitBreakpoints);
            }
        });
    }
}
