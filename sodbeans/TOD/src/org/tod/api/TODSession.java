/*
 Copyright (c) 2013, Andreas Stefik
 All rights reserved.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met: 

 1. Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer. 
 2. Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution. 

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 The views and conclusions contained in the software and documentation are those
 of the authors and should not be interpreted as representing official policies, 
 either expressed or implied, of the FreeBSD Project.
 */
package org.tod.api;

import org.tod.meta.ThreadTracker;
import org.tod.meta.variables.VariablesWatcher;
import org.tod.jvm.api.JVMHandler;
import org.tod.meta.ClassInformationProvider;
import java.io.File;
import java.util.Iterator;
import org.tod.meta.variables.TypeLookupManager;

/**
 * This interface represents a debugging session using the TOD
 * (Trace-Oriented-Debugger) architecture. Typically, a "session" begins when
 * the user first clicks the "debug" toolbar button, and ends when either the
 * debugged program exits, or the user clicks the "stop" button. TOD sessions
 * are capable of running execution forwards and simulating backwards execution.
 *
 * This and related interfaces allow control over a Java Virtual Machine running
 * a specified Java application in debug mode. In addition to providing access
 * to common tasks such as stepping forward/backward, functionality is also
 * provided for language-specific requirements, such as filtering out packages
 * and events, controlling auditory queues and so on.
 *
 * TOD sessions require the following information: - Location of the jar and/or
 * class file to be debugged. - The fully qualified name of a class with a main
 * method
 *
 * Additionally, one can: - Run at full speed - Pause the program - Stop the
 * program - Submit/remove a breakpoint for a specific line in a class - Run to
 * a specific line (forward or backward) - Step Over (forward or backward) -
 * Step Into (forward or backward) - Rewind the program to its initial state
 *
 * @author Jeff Wilson
 */
public interface TODSession {

    public void setMainClassLocation(File mainClassLocation);

    public void setJar(String jar);

    public File getMainClassLocation();

    public String getJar();

    public void setActionHandler(ActionHandler handler);

    public ActionHandler getActionHandler();

    public ExecutionTracker getExecutionTracker();

    public VariablesWatcher getVarialesWatcher();

    public JVMHandler getJVMHandler();

    public ThreadTracker getThreadTracker();

    public TODHandler getTODHandler();

    public Filter getFilter();

    public void setFilter(Filter filter);

    public void addEventListener(DebugEventListener listener);

    public void removeEventListener(DebugEventListener listener);

    public Iterator<DebugEventListener> getEventListeners();

    public boolean isActive();

    public void setClassInformationProvider(ClassInformationProvider info);

    public ClassInformationProvider getClassInformationProvider();

    public TypeLookupManager getTypeLookupManager();

    public void setTypeLookupManager(TypeLookupManager manager);
}
