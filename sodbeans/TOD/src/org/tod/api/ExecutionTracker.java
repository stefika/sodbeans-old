/*
 Copyright (c) 2013, Andreas Stefik
 All rights reserved.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met: 

 1. Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer. 
 2. Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution. 

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 The views and conclusions contained in the software and documentation are those
 of the authors and should not be interpreted as representing official policies, 
 either expressed or implied, of the FreeBSD Project.
 */
package org.tod.api;

/**
 * The execution tracker is responsible for determining where execution must go
 * for forward and backward execution when stepping in the debugger.
 *
 * This implementation does not take into account multiple threads of execution.
 *
 * @author jeff
 */
public interface ExecutionTracker extends SessionTracker {

    /**
     * Update the position in execution for the given thread. This method is
     * called when the JVM stops due to either a live step event, or a live
     * breakpoint event.
     *
     * @param thread
     * @param location
     */
    //public void stateChanged(ThreadReference thread, Location location);
    /**
     * Step backwards in program history, one line at a time.
     *
     * @return false if a new location could not be computed.
     */
    public boolean stepBackOver();

    public boolean stepOver();

    public boolean stepInto();

    public boolean stepBackInto();

    public void runForwardToLine(String fullyQualifiedClassName, int line, boolean hitBreakpoints);

    public void runBackToLine(String fullyQualifiedClassName, int line, boolean hitBreakpoints);

    /**
     * Get the "this" object for the current frame.
     *
     * @return
     */
    public Object getThisObject();

    public void reset();

    //public void stepOverFromVM(ThreadReference thread, Location location);
    public void run();

    public void resume();

    public void pause();
}