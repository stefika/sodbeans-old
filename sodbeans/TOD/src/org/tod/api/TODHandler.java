/*
 Copyright (c) 2013, Andreas Stefik
 All rights reserved.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met: 

 1. Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer. 
 2. Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution. 

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 The views and conclusions contained in the software and documentation are those
 of the authors and should not be interpreted as representing official policies, 
 either expressed or implied, of the FreeBSD Project.
 */
package org.tod.api;

import com.sun.jdi.Method;
import com.sun.jdi.ThreadReference;
import java.util.List;
import org.tod.meta.MethodInfo;
import tod.core.database.browser.IEventBrowser;
import tod.core.database.event.ICallerSideEvent;
import tod.core.database.event.ILogEvent;
import tod.core.database.structure.IBehaviorInfo;
import tod.core.database.structure.IThreadInfo;
import tod.core.database.structure.ObjectId;

/**
 * This interface is responsible for communicating with the TOD database server
 * for event information, as well as starting and stopping the database server
 *
 * @author Jeff Wilson
 */
public interface TODHandler extends SessionTracker {

    public boolean isAlive();

    public boolean start();

    public boolean stop();

    public IThreadInfo lookupThread(ThreadReference reference);

    public IEventBrowser getStepOverEventBrowser();

    public IEventBrowser getStepOverEventBrowser(ThreadReference thread);

    public IEventBrowser getStepOverEventBrowser(ThreadReference thread, int depth);

    public IBehaviorInfo getMethodBehavior(Method method);

    public Object getObjectById(ObjectId oid);

    public long getEventCount(ThreadReference thread);

    public List<MethodInfo> getCallStack(ILogEvent event, boolean isStepInto, boolean isForward, boolean isLastEvent);

    public IEventBrowser getChildrenBrowser(ICallerSideEvent parent);

    public IEventBrowser getStepIntoChildrenBrowser(ICallerSideEvent parent);

    public IEventBrowser filterLocalVariableWrites(IEventBrowser browser);

    public IEventBrowser getThreadBrowser(IThreadInfo thread);

    public IEventBrowser filterFieldWrites(IEventBrowser browser);

    public IEventBrowser getObjectBrowser(ObjectId todObject);

    public IEventBrowser filterMethodCalls(IEventBrowser browser);

    public IEventBrowser filterInstantiations(IEventBrowser objectBrowser);

    public void flush();
}
