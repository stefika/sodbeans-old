/*
 Copyright (c) 2013, Andreas Stefik
 All rights reserved.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met: 

 1. Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer. 
 2. Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution. 

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 The views and conclusions contained in the software and documentation are those
 of the authors and should not be interpreted as representing official policies, 
 either expressed or implied, of the FreeBSD Project.
 */
package org.tod.jvm.impl;

import org.tod.api.AbstractSessionTracker;
import com.sun.jdi.Bootstrap;
import com.sun.jdi.ThreadReference;
import com.sun.jdi.VMDisconnectedException;
import com.sun.jdi.VirtualMachine;
import com.sun.jdi.connect.Connector;
import com.sun.jdi.connect.IllegalConnectorArgumentsException;
import com.sun.jdi.connect.LaunchingConnector;
import com.sun.jdi.connect.VMStartException;
import com.sun.jdi.request.EventRequest;
import com.sun.jdi.request.EventRequestManager;
import com.sun.jdi.request.StepRequest;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.tod.TODUtils;
import org.tod.api.TODSession;
import org.tod.jvm.api.JVMHandler;
import org.tod.jvm.api.JVMListener;
import org.tod.meta.BreakpointInfo;

/**
 *
 * @author jwilson
 */
public class DefaultJVMHandler extends AbstractSessionTracker implements JVMHandler {

    private VirtualMachine vm = null;
    private InputStream inputStream = null;
    private InputStream errorStream = null;
    private OutputStream outputStream = null;
    private JVMEventManager jvmEventDispatcher = null;
    private ArrayList<JVMListener> listeners = new ArrayList<JVMListener>();
    private RequestQueueManager requestManager = null;
    private Logger logger = Logger.getLogger(DefaultJVMHandler.class.getName());

    public DefaultJVMHandler(TODSession session) {
        this.setTODSession(session);
        this.requestManager = new RequestQueueManager(this, this.getTODSession());
    }

    @Override
    public void launchJVM(String[] programArguments) {

        String parameters = buildTODParameters();
        String jarFile = new File(this.getTODSession().getMainClassLocation(), this.getTODSession().getJar()).getAbsolutePath();
        LaunchingConnector defaultConnector = Bootstrap.virtualMachineManager().defaultConnector();
        Map arguments = defaultConnector.defaultArguments();
        //temporary hack for windows 7, 64-bit, possibly others?
        //String os = System.getProperty("os.name");
        //String arch = System.getProperty("sun.arch.data.model");


        // if(os.compareTo("Windows 7") == 0 && 
        //    arch.contains("64")
        // ) {


        Connector.Argument home = (Connector.Argument) arguments.get("home");
        home.setValue("\"" + home.value() + "");
        Connector.Argument java = (Connector.Argument) arguments.get("vmexec");
        java.setValue(java.value() + "\"");
        // }

        Connector.Argument mainArg = (Connector.Argument) arguments.get("main");
        mainArg.setValue("-jar \"" + jarFile + "\"");
        Connector.Argument optionsArg = (Connector.Argument) arguments.get("options");
        optionsArg.setValue(parameters);
        logger.log(Level.INFO, "JVM Args: {0}", parameters);
        //System.out.println("JVM Args: " + parameters);
        try {
            this.vm = defaultConnector.launch(arguments);
        } catch (IOException ex) {
            logger.log(Level.SEVERE, ex.getMessage());
            return;
        } catch (IllegalConnectorArgumentsException ex) {
            logger.log(Level.SEVERE, ex.getMessage());
            return;
        } catch (VMStartException ex) {
            logger.log(Level.SEVERE, ex.getMessage());
            return;
        }

        this.inputStream = this.vm.process().getInputStream();
        this.errorStream = this.vm.process().getErrorStream();
        this.outputStream = this.vm.process().getOutputStream();

        // Notify the request queue manager of the new virtual machine.
        this.requestManager.setVM(this.vm);
        this.fireVMStartingEvent();

        // Set up the event dispatcher.
        this.jvmEventDispatcher = new JVMEventManager(this.getTODSession(), this.vm);
        this.jvmEventDispatcher.start();

        // Start the VM.
        this.vm.resume();
    }

    @Override
    public InputStream getJVMInputStream() {
        return this.inputStream;
    }

    @Override
    public InputStream getJVMErrorStream() {
        return this.errorStream;
    }

    @Override
    public OutputStream getJVMOutputStream() {
        return this.outputStream;
    }

    @Override
    public void addLineBreakpoint(String fullyQualifiedClassName, int lineNumber) {
        this.addLineBreakpoint(fullyQualifiedClassName, lineNumber, -1);
    }

    @Override
    public void addLineBreakpoint(String fullyQualifiedClassName, int lineNumber, int count) {
        BreakpointInfo bpi = new BreakpointInfo();
        bpi.setClassInfo(this.getTODSession().getClassInformationProvider().getClassInfo(fullyQualifiedClassName));
        bpi.setLineNumber(lineNumber);
        bpi.setCountFilter(count);
        this.requestManager.add(bpi);
    }

    @Override
    public BreakpointInfo getBreakpoint(String fullyQualifiedClassName, int line) {
        return this.requestManager.getBreakpoint(fullyQualifiedClassName, line);
    }

    @Override
    public void removeLineBreakpoint(String fullyQualifiedClassName, int lineNumber) {
        BreakpointInfo bpi = new BreakpointInfo();
        bpi.setClassInfo(this.getTODSession().getClassInformationProvider().getClassInfo(fullyQualifiedClassName));
        bpi.setLineNumber(lineNumber);
        this.requestManager.remove(bpi);
    }

    @Override
    public Iterator<BreakpointInfo> getActiveBreakpoints() {
        return this.requestManager.getResolvedBreakpoints();
    }

    /**
     * Get the appropriate agent library name. The library loaded depends on the
     * operating system.
     *
     * @return the agent's path
     */
    private String getAgentPath() {
        String agentName;
        String os = System.getProperty("os.name");

        if (os.compareTo("Mac OS X") == 0) {
            agentName = "libtod-agent15.dylib";
        } else if (os.compareTo("Linux") == 0) {
            agentName = "libtod-agent15.so";
        } else if (os.contains("Windows")) {
            if (System.getProperty("sun.arch.data.model").contains("64")) {
                agentName = "tod-agent15-64.dll";
            } else {
                agentName = "tod-agent15.dll";
            }
        } else { //perhaps it is some other flavor of linux we don't know about?
            agentName = "libtod-agent15.so";
        }

        return TODUtils.getTODLibraryPath() + agentName;
    }

    private String getAgentJar() {
        String agentJarName = "tod-agent-custom.jar";

        return TODUtils.getTODJARPath() + agentJarName;
    }

    /**
     * Returns the most commonly used temporary path on the system.
     *
     * @return If windows, "C:\Windows\Temp" is returned. Otherwise, "/var/tmp".
     */
    private String getCachePath() {
        return System.getProperty("java.io.tmpdir");
    }

    /**
     * Build the TOD parameter string for the target JVM.
     *
     * @return the parameters
     */
    private String buildTODParameters() {
        // TODO: Some of these are hardcoded
        String agentPath = getAgentPath();
        String agentJar = getAgentJar();
        String hostName = "localhost";
        int portNumber = 8058;
        String clientName = "tod-1";
        String cachePath = getCachePath();
        int verboseLevel = 0;
        boolean captureAtStart = true;
        String parameters = "-Xverify:none"
                + " \"-agentpath:" + agentPath + "\""
                + " \"-Dcollector-host=" + hostName + "\""
                + " \"-Dcollector-port=" + portNumber + "\""
                + " \"-Dclient-name=" + clientName + "\""
                + " \"-Dagent-cache-path=" + cachePath + "\""
                + " \"-Dagent-verbose=" + verboseLevel + "\""
                + " \"-Dcapture-at-start=" + captureAtStart + "\""
                + " \"-Xbootclasspath/p:" + agentJar + "\"";
        //" -classpath " + this.getTODSession().getMainClassLocation().getAbsolutePath();

        return parameters;
    }

    @Override
    public void stop() {
        if (this.vm != null) {
            try {
                this.vm.exit(0);
            } catch (VMDisconnectedException e) {
            } finally {
                this.vm = null;
                this.requestManager.reset();
            }
            //this.listeners.clear(); ?
        }
    }

    @Override
    public void clearStepRequests() {
        List<StepRequest> list = this.vm.eventRequestManager().stepRequests();
        this.vm.eventRequestManager().deleteEventRequests(list);
    }

    @Override
    public void clearStepRequests(ThreadReference threadReference) {
        EventRequestManager evm = this.vm.eventRequestManager();
        Iterator<StepRequest> iterator = evm.stepRequests().iterator();
        while (iterator.hasNext()) {
            StepRequest next = iterator.next();
            if (next.thread().equals(threadReference)) {
                evm.deleteEventRequest(next);
                break;
            }
        }
    }

    @Override
    public void addListener(JVMListener listener) {
        if (listener != null) {
            listeners.add(listener);
        }
    }

    @Override
    public void removeListener(JVMListener listener) {
        if (listener != null) {
            listeners.remove(listener);
        }
    }

    @Override
    public Iterator<JVMListener> getListeners() {
        return listeners.iterator();
    }

    private void fireVMStartingEvent() {
        Iterator<JVMListener> iterator = this.getListeners();
        while (iterator.hasNext()) {
            iterator.next().VMPrepareEvent();
        }
    }

    @Override
    public void stepOver(ThreadReference thread) {
        stepOver(thread, 1);
    }

    @Override
    public void stepOver(ThreadReference thread, int skipCount) {
        EventRequestManager evm = vm.eventRequestManager();
        this.clearStepRequests(thread);
        StepRequest r = evm.createStepRequest(thread, StepRequest.STEP_LINE, StepRequest.STEP_OVER);
        r.addClassExclusionFilter("java.*"); // TODO: Hardcoded
        r.addClassExclusionFilter("tod.*"); // TODO: Hardcoded
        r.addClassExclusionFilter("quorum.Libraries.Language.Types.*"); // TODO: Hardcoded
        r.addCountFilter(skipCount);
        r.setSuspendPolicy(EventRequest.SUSPEND_EVENT_THREAD);
        r.enable();
        vm.resume();
    }

    @Override
    public void stepInto(ThreadReference thread) {
        stepInto(thread, 1);
    }

    @Override
    public void stepInto(ThreadReference thread, int skipCount) {
        EventRequestManager evm = vm.eventRequestManager();
        this.clearStepRequests(thread);
        StepRequest r = evm.createStepRequest(thread, StepRequest.STEP_LINE, StepRequest.STEP_INTO);
        r.addClassFilter("quorum.*"); // TODO: Hardcoded
        r.addClassExclusionFilter("quorum.Libraries.Language.Types.*");
        r.addCountFilter(skipCount);
        r.setSuspendPolicy(EventRequest.SUSPEND_EVENT_THREAD);
        r.enable();
        vm.resume();
    }

    @Override
    public void runToLine(String fullyQualifiedClassName, int lineNumber) {
        // Request a one-off breakpoint.
        this.clearStepRequests();
        this.addLineBreakpoint(fullyQualifiedClassName, lineNumber, 1);
        this.vm.resume();
    }

    @Override
    public boolean isAlive() {
        return this.vm != null;
    }

    @Override
    public void pause() {
        this.vm.suspend();
    }

    @Override
    public void resume() {
        this.vm.resume();
    }

    @Override
    public void removeAllBreakpoints() {
        this.requestManager.clear();
    }
}
