/*
 Copyright (c) 2013, Andreas Stefik
 All rights reserved.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met: 

 1. Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer. 
 2. Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution. 

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 The views and conclusions contained in the software and documentation are those
 of the authors and should not be interpreted as representing official policies, 
 either expressed or implied, of the FreeBSD Project.
 */
package org.tod.jvm.impl;

import com.sun.jdi.event.BreakpointEvent;
import com.sun.jdi.event.StepEvent;
import com.sun.jdi.request.BreakpointRequest;
import java.util.Iterator;
import org.tod.api.DebugEventListener;
import org.tod.api.TODSession;
import org.tod.jvm.api.JVMListener;
import org.tod.meta.BreakpointInfo;
import org.tod.meta.ThreadInfo;

/**
 *
 * @author jeff
 */
public class JVMEventDispatcher extends JVMListener {

    /**
     * Our associated TOD session.
     */
    private TODSession session = null;

    public JVMEventDispatcher(TODSession session) {
        this.session = session;
    }

    @Override
    public void stepEvent(StepEvent e) {
        /*Iterator<DebugEventListener> listeners = this.session.getEventListeners();
         while (listeners.hasNext()) {
         DebugEventListener listener = listeners.next();
         StepRequest sr = (StepRequest) e.request();
         ThreadInfo thread = this.session.getThreadTracker().getByReference(e.thread());
         StateInfo info = new StateInfo();
         info.setClassInfo(this.session.getClassInformationProvider().getClassInfo(e.location().declaringType()));
         info.setMethodInfo(this.session.getClassInformationProvider().getMethodInfo(e.location().method()));
         info.setLineNumber(e.location().lineNumber());
         info.setThread(thread);
         if (sr.depth() == StepRequest.STEP_OVER) {
         listener.steppedOver(info);
         } else if (sr.depth() == StepRequest.STEP_INTO) {
         listener.steppedInto(info);
         }
         }*/
    }

    @Override
    public void breakpointEvent(BreakpointEvent e) {
        Iterator<DebugEventListener> listeners = this.session.getEventListeners();
        while (listeners.hasNext()) {
            DebugEventListener listener = listeners.next();
            BreakpointRequest bpr = (BreakpointRequest) e.request();
            BreakpointInfo info = new BreakpointInfo();
            info.setClassInfo(this.session.getClassInformationProvider().getClassInfo(e.location().declaringType()));
            info.setMethodInfo(this.session.getClassInformationProvider().getMethodInfo(e.location().method()));
            info.setLineNumber(e.location().lineNumber());
            ThreadInfo threadInfo = this.session.getThreadTracker().getByReference(e.thread());
            this.session.getThreadTracker().setActiveThread(threadInfo);
            info.setThread(threadInfo);
            listener.breakpointReached(info);
            // TODO
            // Is this a "step into" event, or a real breakpoint?
            //if (this.expectingStepInto) {
            //listener.steppedInto((StateInfo)info);
            //    this.expectingStepInto = false;
            //} else {
            //    listener.breakpointReached(info);
            //}
        }
    }
}