/*
 Copyright (c) 2013, Andreas Stefik
 All rights reserved.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met: 

 1. Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer. 
 2. Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution. 

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 The views and conclusions contained in the software and documentation are those
 of the authors and should not be interpreted as representing official policies, 
 either expressed or implied, of the FreeBSD Project.
 */
package org.tod.jvm.impl;

import com.sun.jdi.AbsentInformationException;
import com.sun.jdi.Location;
import com.sun.jdi.ReferenceType;
import com.sun.jdi.VMDisconnectedException;
import com.sun.jdi.VirtualMachine;
import com.sun.jdi.event.ClassPrepareEvent;
import com.sun.jdi.request.BreakpointRequest;
import com.sun.jdi.request.ClassPrepareRequest;
import com.sun.jdi.request.EventRequest;
import com.sun.jdi.request.EventRequestManager;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import org.tod.api.TODSession;
import org.tod.jvm.api.JVMHandler;
import org.tod.jvm.api.JVMListener;
import org.tod.meta.BreakpointInfo;

/**
 * This class is responsible for taking breakpoint requests and resolving them
 * as soon as possible in the target JVM. The Java Virtual Machine requires that
 * any breakpoint requests refer to classes that have already been loaded, but
 * breakpoint requests may come before this occurs (for example, if a user has
 * added a breakpoint to a line before the debugger is even running).
 *
 * @author jeff
 */
public class RequestQueueManager {

    /**
     * All breakpoints that have been requested.
     */
    private List<BreakpointInfo> requestedBreakpoints = new LinkedList<BreakpointInfo>();
    /**
     * The breakpoint requests we need to satisfy.
     */
    private List<BreakpointInfo> unresolvedBreakpoints = new LinkedList<BreakpointInfo>();
    /**
     * The breakpoints we have satisfied.
     */
    private List<BreakpointInfo> resolvedBreakpoints = new LinkedList<BreakpointInfo>();
    /**
     * We map each breakpoint to its original request in the VM so we can
     * disable it later. TODO: Pretty hackish--dont use String, use
     * BreakpointInfo
     */
    private HashMap<BreakpointInfo, BreakpointRequest> infoToRequest = new HashMap<BreakpointInfo, BreakpointRequest>();
    /**
     * Hash breakpoints as fullyQualifiedClassName:lineNumber for fast access
     */
    private HashMap<String, BreakpointInfo> hashedBreakpoints = new HashMap<String, BreakpointInfo>();
    /**
     * The JVM handler this manager is related to.
     */
    private JVMHandler jvmHandler = null;
    /**
     * The session this manager is associated with.
     */
    private TODSession session = null;
    /**
     * The virtual machine we are connected to.
     */
    private VirtualMachine vm = null;

    public RequestQueueManager(JVMHandler handler, TODSession session) {
        this.jvmHandler = handler;
        this.session = session;

        // Add a listener for class prepare events.
        this.jvmHandler.addListener(new BreakpointListener());
    }

    /**
     * Sets up the request manager for a new session by marking all breakpoints
     * as unresolved.
     */
    public void reset() {
        this.unresolvedBreakpoints.clear();
        this.unresolvedBreakpoints.addAll(this.requestedBreakpoints);
        this.resolvedBreakpoints.clear();
        this.vm = null;
    }

    /**
     * Clears all unresolved and requested breakpoints.
     */
    public void clear() {
        Iterator<BreakpointInfo> resolved = this.resolvedBreakpoints.iterator();
        while (resolved.hasNext()) {
            try {
                BreakpointInfo bpi = resolved.next();
                BreakpointRequest bpr = this.infoToRequest.get(bpi);
                if (bpr != null) {
                    bpr.disable();
                }
            } catch (VMDisconnectedException e) {
                // vm went away.
            }
        }

        this.unresolvedBreakpoints.clear();
        this.resolvedBreakpoints.clear();
        this.requestedBreakpoints.clear();
        this.infoToRequest.clear();
    }

    /**
     * Add a breakpoint.
     *
     * @param breakpoint the breakpoint to add
     */
    public void add(BreakpointInfo breakpoint) {
        // Resolve breakpoints with count filters immediately.
        if (breakpoint.getCountFilter() != -1) {
            attemptResolve(breakpoint);
            return;
        }

        this.requestedBreakpoints.add(breakpoint);
        this.hashedBreakpoints.put(breakpoint.toString(), breakpoint);

        boolean resolved = attemptResolve(breakpoint);
        if (!resolved) {
            // Make it known that this breakpoint request wishes to be resolved.
            this.unresolvedBreakpoints.add(breakpoint);
        } else {
            this.resolvedBreakpoints.add(breakpoint);
        }
    }

    /**
     * Attempt to resolve all currently unresolved breakpoints.
     */
    public void attemptResolveAll() {
        Iterator<BreakpointInfo> breakpoints = this.unresolvedBreakpoints.iterator();
        while (breakpoints.hasNext()) {
            BreakpointInfo bpi = breakpoints.next();
            boolean resolved = attemptResolve(bpi);

            if (resolved) {
                // Remove this breakpoint from the unresolved queue.
                this.resolvedBreakpoints.add(bpi);
                breakpoints.remove();
            }
        }
    }

    /**
     * Attempt to resolve the added breakpoint. This is done by looking through
     * the classes loaded in the virtual machine (if it is running) and
     * determining if this request matches.
     *
     * @param breakpoint
     */
    private boolean attemptResolve(BreakpointInfo breakpoint) {
        if (vm == null) {
            return false;
        }
        try {
            List<ReferenceType> classesByName = vm.classesByName(breakpoint.getClassInfo().getDotName());
            if (classesByName.size() >= 1) {
                // Get the bytecode locations of the line number.
                ReferenceType ref = classesByName.get(0);
                try {
                    List<Location> locations = ref.locationsOfLine(breakpoint.getLineNumber());

                    // Set a breakpoint at the first location.
                    if (locations.size() >= 1) {
                        EventRequestManager mgr = vm.eventRequestManager();
                        BreakpointRequest req = mgr.createBreakpointRequest(locations.get(0));
                        req.setSuspendPolicy(EventRequest.SUSPEND_EVENT_THREAD);
                        if (breakpoint.getCountFilter() != -1) {
                            req.addCountFilter(breakpoint.getCountFilter());
                        }
                        req.enable();
                        this.infoToRequest.put(breakpoint, req);
                        return true;
                    }
                } catch (AbsentInformationException ex) {
                }
            }

            // If we make it here, the breakpoint could NOT be resolved, but the JVM
            // is running... so add a class prepare request to handle it.
            ClassPrepareRequest prepRequest = vm.eventRequestManager().createClassPrepareRequest();
            prepRequest.addClassFilter(breakpoint.getClassInfo().getDotName());
            prepRequest.setSuspendPolicy(EventRequest.SUSPEND_EVENT_THREAD);
            prepRequest.enable();
        } catch (VMDisconnectedException e) {
            return false;
        }

        return false;
    }

    /**
     * Resolve any breakpoints associated with the given reference type.
     *
     * @param ref
     */
    public void resolveBreakpointsForClass(ReferenceType ref) {
        if (this.vm == null) {
            return;
        }

        try {
            Iterator<BreakpointInfo> iterator = this.unresolvedBreakpoints.iterator();
            String fullyQualifiedClassName = ref.name();
            while (iterator.hasNext()) {
                BreakpointInfo bpi = iterator.next();
                if (bpi.getClassInfo().getDotName().equals(fullyQualifiedClassName)) {
                    List<Location> locations;
                    try {
                        locations = ref.locationsOfLine(bpi.getLineNumber());
                        // Set a breakpoint at the first location.
                        if (locations.size() >= 1) {
                            EventRequestManager mgr = vm.eventRequestManager();
                            BreakpointRequest req = mgr.createBreakpointRequest(locations.get(0));
                            req.setSuspendPolicy(EventRequest.SUSPEND_EVENT_THREAD);
                            if (bpi.getCountFilter() != -1) {
                                req.addCountFilter(bpi.getCountFilter());
                            }
                            // TODO: bpi.setThread(...)
                            bpi.setMethodInfo(this.session.getClassInformationProvider().getMethodInfo(req.location().method()));
                            req.enable();
                            this.infoToRequest.put(bpi, req);
                            // This breakpoint has been satisfied. Remove it from
                            // the unsatisfied list.
                            this.resolvedBreakpoints.add(bpi);
                            iterator.remove();
                        }
                    } catch (AbsentInformationException ex) {
                    }
                }
            }

            this.vm.resume();
        } catch (VMDisconnectedException e) {
        }
    }

    /**
     * Return all resolved breakpoints.
     *
     * @return
     */
    public Iterator<BreakpointInfo> getResolvedBreakpoints() {
        return this.resolvedBreakpoints.iterator();
    }

    void remove(BreakpointInfo bpi) {
        this.unresolvedBreakpoints.remove(bpi);
        this.resolvedBreakpoints.remove(bpi);
        this.requestedBreakpoints.remove(bpi);
        BreakpointRequest bpr = this.infoToRequest.get(bpi);
        try {
            if (bpr != null) {
                bpr.disable();
                this.infoToRequest.remove(bpi);
            }
        } catch (VMDisconnectedException e) {
            // vm went away.
        }
    }

    public BreakpointInfo getBreakpoint(String fullyQualifiedClassName, int line) {
        StringBuilder b = new StringBuilder();
        b.append(fullyQualifiedClassName);
        b.append(":");
        b.append(line);

        return this.hashedBreakpoints.get(b.toString());
    }

    private class BreakpointListener extends JVMListener {

        @Override
        public void VMPrepareEvent() {
            RequestQueueManager.this.attemptResolveAll();
        }

        @Override
        public void classPrepareEvent(ClassPrepareEvent e) {
            RequestQueueManager.this.resolveBreakpointsForClass(e.referenceType());
        }
    }

    public void setVM(VirtualMachine vm) {
        this.vm = vm;
    }
}
