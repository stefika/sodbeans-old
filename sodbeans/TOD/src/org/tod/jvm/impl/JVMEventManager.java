/*
 Copyright (c) 2013, Andreas Stefik
 All rights reserved.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met: 

 1. Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer. 
 2. Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution. 

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 The views and conclusions contained in the software and documentation are those
 of the authors and should not be interpreted as representing official policies, 
 either expressed or implied, of the FreeBSD Project.
 */
package org.tod.jvm.impl;

import com.sun.jdi.AbsentInformationException;
import com.sun.jdi.VMDisconnectedException;
import com.sun.jdi.VirtualMachine;
import com.sun.jdi.event.BreakpointEvent;
import com.sun.jdi.event.ClassPrepareEvent;
import com.sun.jdi.event.Event;
import com.sun.jdi.event.EventIterator;
import com.sun.jdi.event.EventSet;
import com.sun.jdi.event.StepEvent;
import com.sun.jdi.event.VMDeathEvent;
import com.sun.jdi.event.VMStartEvent;
import com.sun.jdi.request.BreakpointRequest;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.tod.api.TODSession;
import org.tod.jvm.api.JVMListener;

/**
 * This class is responsible for dispatching events received from the running
 * Java Virtual Machine to the TOD Session. Dispatched events are sent to all
 * listening clients.
 *
 * @author Jeff Wilson
 */
class JVMEventManager extends Thread {

    private TODSession session = null;
    private boolean vmConnected = true;
    private boolean expectingStepInto = false;
    private boolean expectingStepOver = false;
    private VirtualMachine vm = null;

    public JVMEventManager(TODSession session, VirtualMachine vm) {
        this.session = session;
        this.vm = vm;
    }

    @Override
    public void run() {
        while (this.vmConnected) {
            try {
                EventSet set = vm.eventQueue().remove();
                EventIterator eventIterator = set.eventIterator();
                while (eventIterator.hasNext()) {
                    Event next = eventIterator.next();
                    Throwable t = null;
                    dispatchEvent(vm, next);
                }
            } catch (InterruptedException ex) {
            } catch (VMDisconnectedException ex) {
                // TODO: Make sure the VM terminates properly!
                this.vmConnected = false;
            }
        }

        // The virtual machine disconnected.
        fireDisconnectEvent();
    }

    private synchronized void dispatchEvent(VirtualMachine vm, Event e) {
        Iterator<JVMListener> listeners = this.session.getJVMHandler().getListeners();
        while (listeners.hasNext()) {
            JVMListener listener = listeners.next();
            if (e instanceof VMStartEvent) {
                listener.VMStartEvent();
            } else if (e instanceof BreakpointEvent) {
                listener.breakpointEvent((BreakpointEvent) e);
            } else if (e instanceof ClassPrepareEvent) {
                listener.classPrepareEvent((ClassPrepareEvent) e);
            } else if (e instanceof StepEvent) {
                listener.stepEvent((StepEvent) e);
            } else if (e instanceof VMDeathEvent) {
                // The virtual machine has terminated.
                listener.VMDeathEvent();
                this.vmConnected = false;
            }
        }

        /*if (e instanceof StepEvent) {
         StepEvent se = (StepEvent)e;
         StepRequest sr = (StepRequest)se.request();
         long firstBytecode = 0;
         try {
         firstBytecode = se.location().method().allLineLocations().get(0).codeIndex();
         } catch (AbsentInformationException ex) {
         }
         if (se.location().codeIndex() == 0) {
         if (sr.depth() == StepRequest.STEP_INTO) {
         // We need to set a breakpoint at the first line in this
         // method to get around a bug in the JVM implementation.
         makeStepIntoBreakpoint(se);
         this.expectingStepInto = true;
         vm.resume();
         return;
         }
         } else if (se.location().codeIndex() < firstBytecode) {
         // ignore
         vm.resume();
         return;
         } else {
         manageStepEvent(se);
         }
         } else if (e instanceof BreakpointEvent) {
         manageBreakpointEvent((BreakpointEvent)e);
         }*/
    }

    private void fireDisconnectEvent() {
        Iterator<JVMListener> listeners = this.session.getJVMHandler().getListeners();
        while (listeners.hasNext()) {
            listeners.next().VMExitEvent();
        }
    }

    /**
     * Create a breakpoint which brings us into the proper location in a method
     * after a "step into" event.
     *
     * @param se
     */
    private synchronized void makeStepIntoBreakpoint(StepEvent se) {
        try {
            BreakpointRequest bpr = vm.eventRequestManager().createBreakpointRequest(se.location().method().allLineLocations().get(0));
            bpr.addCountFilter(1);
            bpr.enable();
        } catch (AbsentInformationException ex) {
            Logger.getLogger(JVMEventManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
