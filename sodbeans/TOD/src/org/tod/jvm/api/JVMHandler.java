/*
 Copyright (c) 2013, Andreas Stefik
 All rights reserved.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met: 

 1. Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer. 
 2. Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution. 

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 The views and conclusions contained in the software and documentation are those
 of the authors and should not be interpreted as representing official policies, 
 either expressed or implied, of the FreeBSD Project.
 */
package org.tod.jvm.api;

import com.sun.jdi.ThreadReference;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Iterator;
import org.tod.api.SessionTracker;
import org.tod.meta.BreakpointInfo;

/**
 * This interface is responsible for providing access to the running Java
 * Virtual Machine (JVM).
 *
 * @author Jeff Wilson
 */
public interface JVMHandler extends SessionTracker {

    public void launchJVM(String[] programArguments);

    public InputStream getJVMInputStream();

    public InputStream getJVMErrorStream();

    public OutputStream getJVMOutputStream();

    public void stop();

    public void clearStepRequests();

    public void stepOver(ThreadReference thread);

    public void stepInto(ThreadReference thread);

    public void stepOver(ThreadReference thread, int skipCount);

    public void stepInto(ThreadReference thread, int skipCount);

    public void runToLine(String fullyQualifiedClassName, int lineNumber);

    public void clearStepRequests(ThreadReference threadReference);

    public void addListener(JVMListener listener);

    public void removeListener(JVMListener listener);

    public Iterator<JVMListener> getListeners();

    public void addLineBreakpoint(String fullyQualifiedClassName, int lineNumber);

    public void addLineBreakpoint(String fullyQualifiedClassName, int lineNumber, int count);

    public void removeLineBreakpoint(String fullyQualifiedClassName, int lineNumber);

    public void removeAllBreakpoints();

    public Iterator<BreakpointInfo> getActiveBreakpoints();

    public BreakpointInfo getBreakpoint(String fullyQualifiedClassName, int line);

    public boolean isAlive();

    public void pause();

    public void resume();
}
