/*
 Copyright (c) 2013, Andreas Stefik
 All rights reserved.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met: 

 1. Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer. 
 2. Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution. 

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 The views and conclusions contained in the software and documentation are those
 of the authors and should not be interpreted as representing official policies, 
 either expressed or implied, of the FreeBSD Project.
 */
package org.tod;

import com.sun.jdi.Method;
import java.io.File;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openide.modules.InstalledFileLocator;

/**
 * A collection of utility functions for TOD.
 *
 * @author jeff
 */
public class TODUtils {

    /**
     * Whether or not TOD should be used in place of the standard interpreted
     * omniscient debugger in Sodbeans.
     */
    private static final boolean ENABLE_TOD = true;

    public static boolean isTODEnabled() {
        return ENABLE_TOD;
    }

    /**
     * The location of TOD's libraries.
     *
     * @return the library path
     */
    public static String getTODLibraryPath() {
        //String userDir;
        //String os = System.getProperty("os.name");
        /*
         if (os.compareTo("Mac OS X") == 0) {
         userDir =  System.getProperty("user.dir") + "/TOD/release/modules/lib/";
         }
         else if (os.compareTo("Linux") == 0) {
         userDir =  System.getProperty("user.dir") + "/TOD/release/modules/lib/";
         }
         else {
         userDir =  System.getProperty("user.dir") + "\\TOD\\release\\modules\\lib\\";
         }
        
         return userDir;*/

        URL url;
        try {
            InstalledFileLocator locator = InstalledFileLocator.getDefault();
            File directory = locator.locate("modules/lib", "org.tod", false);
            return directory.getAbsolutePath() + File.separator;
        } catch (Exception ex) {
            Logger.getLogger(TODUtils.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    /**
     * The location of TOD's JAR files.
     *
     * @return the library path
     */
    public static String getTODJARPath() {
        try {
            InstalledFileLocator locator = InstalledFileLocator.getDefault();
            File directory = locator.locate("modules/ext", "org.tod", false);
            return directory.getAbsolutePath() + File.separator;
        } catch (Exception ex) {
            Logger.getLogger(TODUtils.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    /**
     * Compute the JNI-style signature for the given JVM method.
     *
     * @param method
     * @return the JNI-style signature. for example, "public void Main()"
     * becomes "Main()V".
     */
    public static String computeMethodSignature(Method method) {
        return method.name() + method.signature();
    }
}
