{\rtf1\ansi\ansicpg1252\cocoartf1038\cocoasubrtf360
{\fonttbl\f0\fswiss\fcharset0 Helvetica;}
{\colortbl;\red255\green255\blue255;}
\margl1440\margr1440\vieww9000\viewh8400\viewkind0
\pard\tx720\tx1440\tx2160\tx2880\tx3600\tx4320\tx5040\tx5760\tx6480\tx7200\tx7920\tx8640\ql\qnatural\pardirnatural

\f0\fs24 \cf0 Instructions for moving a new version of Quorum into Sodbeans\
\
Part 1: Grab Quorum\
1. Do a fresh checkout of Quorum (needs to be a clean copy).\
2. "svn export trunk trunk2" from the terminal in the quorum folder.\
3. Open un-versioned Quorum project (in trunk2).\
4. Clean and build.\
\
Part 2: Copy Quorum to Sodbeans\
1. Go to dist folder in Quorum\
2. Update Sodbeans\
3. Clean and build all for Sappy\
4. Clean and build all for Sodbeans\
5. Replace the quorum jar file in Quorum module -> release -> modules -> ext -> quorum.jar (this can only be seen under the Files tab) with the jar in Quorum dist folder (the un-revisioned version).\
6. Open QuorumStandardLibrary module\
7. Open it then in the files tab QuorumStandardLibrary -> release -> quorum and delete the Libraries  folder and the Plugins folder.\
8. Delete quorum.index from QuorumStandardLibrary -> release -> indexes\
9. Commit the release folder.\
10. Open dist folder of Quorum (un-revisioned version) copy the quorum.index file, the Plugins folder and the Libraries folder back into Sodbeans.\
11. Clean and build all of Sappy then Sodbeans\
\pard\tx720\tx1440\tx2160\tx2880\tx3600\tx4320\tx5040\tx5760\tx6480\tx7200\tx7920\tx8640\ql\qnatural\pardirnatural
\cf0 12. Commit\
\
}