/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.sodbeans.tod;

import java.util.HashMap;
import java.util.Iterator;
import org.openide.util.Lookup;
import org.sodbeans.compiler.api.descriptors.CompilerClassDescriptor;
import org.sodbeans.compiler.api.descriptors.CompilerFileDescriptor;

/**
 * A collection of common functions used by the TOD debugging framework, such
 * as converting a quorum class name to a JVM class name, etc.
 * @author jeff
 */
public class TODCompilerUtils {
    private static final HashMap<String, String> commonTypes = new HashMap<String, String>();
    private static final String quorumPrefix = "Lquorum/";
    
    static {
        commonTypes.put("I", "integer");
        commonTypes.put("Z", "boolean");
        commonTypes.put("D", "number");
        commonTypes.put("Ljava/lang/String;", "text");

    }
    public static String friendlyTypeSignature(String typeSignature) {
        if (commonTypes.containsKey(typeSignature)) {
            return commonTypes.get(typeSignature);
        }
        
        // If it is a quorum class, remove "Lquorum/" and the last ";"
        if (typeSignature.startsWith(quorumPrefix)) {
            int lastSlash = typeSignature.lastIndexOf("/");
            String newSig =  typeSignature.substring(lastSlash+1, typeSignature.length() - 1);
            newSig = newSig.replace("$Interface", "");
            return newSig;
        }
        
        // Otherwise, just return the type signature unchanged. (Unknown type)
        return typeSignature;
    }
    
    public static String friendlyClassName(String fullyQualifiedClassName) {
        // If it is a quorum class, remove "quorum/" and the last ";"
        if (fullyQualifiedClassName.startsWith("quorum/")) {
            int lastSlash = fullyQualifiedClassName.lastIndexOf("/");
            return fullyQualifiedClassName.substring(lastSlash+1, fullyQualifiedClassName.length());
        }
        
        // Otherwise, just return the type signature unchanged. (Unknown type)
        return fullyQualifiedClassName;
    }
    
    public static String staticKeyToJVMName(String name) {
        String newName = name.replace('.', '/');
        if (newName.startsWith("/")) {
            return "quorum" + newName;
        }

        return "quorum/" + newName;
    }

    public static String findJVMClassName(CompilerFileDescriptor fileDescriptor, int targetLine) {
        Iterator<CompilerClassDescriptor> classes = fileDescriptor.getClasses();
        while (classes.hasNext()) {
            CompilerClassDescriptor next = classes.next();
            if (targetLine >= next.getLine()) {
                return staticKeyToJVMName(next.getStaticKey());
            }
        }

        return null;
    }
}
