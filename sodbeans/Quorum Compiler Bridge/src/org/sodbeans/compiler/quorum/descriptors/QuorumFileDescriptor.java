/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sodbeans.compiler.quorum.descriptors;

import java.io.File;
import java.util.Iterator;
import java.util.Vector;
import org.openide.nodes.Node;
import org.sodbeans.compiler.api.descriptors.CompilerClassDescriptor;
import org.sodbeans.compiler.api.descriptors.CompilerFileDescriptor;
import org.quorum.symbols.ClassDescriptor;
import org.quorum.symbols.FileDescriptor;

/**
 *  Describes a file that was processed in the Quorum Programming Language.
 * 
 * @author Andreas Stefik
 */
public class QuorumFileDescriptor extends CompilerFileDescriptor{
    private FileDescriptor fileDescriptor;
    
     
    /**
     * @return the fileDescriptor
     */
    public FileDescriptor getFileDescriptor() {
        return fileDescriptor;
    }

    /**
     * @param fileDescriptor the fileDescriptor to set
     */
    public void setFileDescriptor(FileDescriptor fileDescriptor) {
        this.fileDescriptor = fileDescriptor;
    }

    @Override
    public CompilerClassDescriptor getClass(String name) {
        QuorumClassDescriptor hcd = new QuorumClassDescriptor();
        if(fileDescriptor != null) {
            ClassDescriptor cd = fileDescriptor.getClassDescriptor(name);
            hcd.setClassDescriptor(cd);
        }
        return hcd;
    }

    @Override
    public Iterator<CompilerClassDescriptor> getClasses() {
        QuorumClassIterator it = new QuorumClassIterator();
        if(fileDescriptor != null) {
            Iterator<ClassDescriptor> cIt = fileDescriptor.getClassIterator();
            it.setIterator(cIt);
        }
        return it;
    }

    @Override
    public String getName() {
        if(fileDescriptor != null) {
            return fileDescriptor.getStaticKey();
        }
        else {
            return "";
        }
    }
    @Override
    public void addNotify() {
        if(fileDescriptor != null) {
            Iterator<ClassDescriptor> mIt = fileDescriptor.getClassIterator();
            Vector<QuorumClassDescriptor> methods = new Vector<QuorumClassDescriptor>();
            while(mIt.hasNext()) {

                QuorumClassDescriptor desc = new QuorumClassDescriptor();
                desc.setClassDescriptor(mIt.next());
                methods.add(desc);
            }
            setKeys(methods);
        }
    }

    @Override
    public Node[] createNodes(CompilerClassDescriptor key) {
        return new Node[]{key.getRootNode()};
    }

    @Override
    public File getFile() {
        return this.fileDescriptor.getFile();
    }
}
 