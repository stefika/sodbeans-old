/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sodbeans.compiler.quorum.descriptors;

import java.util.Iterator;
import org.sodbeans.compiler.api.descriptors.CompilerMethodDescriptor;
import org.quorum.symbols.MethodDescriptor;

/**
 * An Iterator over a list of methods.
 *
 * @author Andreas Stefik
 */
public class QuorumMethodIterator implements Iterator<CompilerMethodDescriptor>{

    private Iterator<MethodDescriptor> methodIterator;

    @Override
    public boolean hasNext() {
        return methodIterator.hasNext();
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException("This function is not supported.");
    }

    /**
     * Sets an iterator over method descriptor objects.
     * 
     * @param cIt
     */
    protected void setIterator(Iterator<MethodDescriptor> cIt) {
        methodIterator = cIt;
    }

    @Override
    public CompilerMethodDescriptor next() {
        MethodDescriptor cd = methodIterator.next();
        QuorumMethodDescriptor hcd  = new QuorumMethodDescriptor();
        hcd.setMethodDescriptor(cd);
        return hcd;
    }

}
