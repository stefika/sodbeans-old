/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sodbeans.compiler.quorum.descriptors;

import java.util.Iterator;
import java.util.Vector;
import org.openide.nodes.Node;
import org.sodbeans.compiler.api.descriptors.CompilerClassDescriptor;
import org.sodbeans.compiler.api.descriptors.CompilerMethodDescriptor;
import org.sodbeans.controller.api.ScreenReaderInformation;
import org.quorum.symbols.ClassDescriptor;
import org.quorum.symbols.MethodDescriptor;

/**
 * Represents a class in the Quorum Programming language.
 * 
 * @author Andreas Stefik
 */
public class QuorumClassDescriptor extends CompilerClassDescriptor{
    private ClassDescriptor classDescriptor;
    ClassInformation info = new ClassInformation();

    /**
     * Returns the class descriptor being described by this object.
     * 
     * @return the classDescriptor
     */
    public ClassDescriptor getClassDescriptor() {
        return classDescriptor;
    }

    /**
     * Sets the class descriptor being described by this object.
     * 
     * @param classDescriptor the classDescriptor to set
     */
    public void setClassDescriptor(ClassDescriptor classDescriptor) {
        this.classDescriptor = classDescriptor;
    }

    @Override
    public String getName() {
        // Fixes a potential NullPointerException
        if (classDescriptor == null) {
            return "";
        }
        
        return classDescriptor.getName();
    }

    @Override
    public CompilerMethodDescriptor getMethod(String name) {
        QuorumMethodDescriptor hmd = new QuorumMethodDescriptor();
        MethodDescriptor md = classDescriptor.getMethod(name);
        hmd.setMethodDescriptor(md);
        return hmd;
    }

    @Override
    public Iterator<CompilerMethodDescriptor> getMethods() {
        QuorumMethodIterator it = new QuorumMethodIterator();
        Iterator<MethodDescriptor> mIt = classDescriptor.getMethods();
        it.setIterator(mIt);
        return it;
    }

    @Override
    public void addNotify() {
        Iterator<MethodDescriptor> mIt = classDescriptor.getMethods();
        Vector<CompilerMethodDescriptor> methods = new Vector<CompilerMethodDescriptor>();
        while(mIt.hasNext()) {

            QuorumMethodDescriptor desc = new QuorumMethodDescriptor();
            desc.setMethodDescriptor(mIt.next());
            methods.add(desc);
        }
        setKeys(methods);
    }
    @Override
    protected Node[] createNodes(CompilerMethodDescriptor key) {
        return new Node[]{key.getRootNode()};
    }

    @Override
    public int getLine(){
        return classDescriptor.getLineBegin();
    }

    @Override
    public int getColumn() {
        return classDescriptor.getColumnBegin();
    }


    @Override
    public int getEndLine() {
        return classDescriptor.getLineEnd();
    }

    @Override
    public int getEndColumn() {
        return classDescriptor.getColumnEnd();
    }

    public ScreenReaderInformation getScreenReaderInformation() {
        return info;
    }

    @Override
    public String getStaticKey() {
        // Fixes a potential NullPointerException
        if (classDescriptor == null) {
            return "";
        }
        
        return classDescriptor.getStaticKey();
    }

    public class ClassInformation implements ScreenReaderInformation {
        public String getDescription() {
            return classDescriptor.getName() + " class";
        }
    }
}
