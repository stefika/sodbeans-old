/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sodbeans.compiler.quorum.descriptors;

import java.util.Iterator;
import org.sodbeans.compiler.api.descriptors.CompilerClassDescriptor;
import org.quorum.symbols.ClassDescriptor;

/**
 * A general iterator for digging through the various classes available
 * on the system.
 * 
 * @author Andreas Stefik
 */
public class QuorumClassIterator implements Iterator<CompilerClassDescriptor>{

    /**
     * An iterator object of all of the classes available.
     */
    private Iterator<ClassDescriptor> classIterator;

    @Override
    public boolean hasNext() {
        if(classIterator != null) {
            return classIterator.hasNext();
        }
        else {
            return false;
        }
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException("This function is not supported.");
    }

    /**
     * Sets an iterator from the virtual machine and translates it into
     * NetBeans friendly APIs.
     * 
     * @param cIt
     */
    protected void setIterator(Iterator<ClassDescriptor> cIt) {
        classIterator = cIt;
    }

    @Override
    public CompilerClassDescriptor next() {
        if(classIterator != null) {
            ClassDescriptor cd = classIterator.next();
            QuorumClassDescriptor hcd  = new QuorumClassDescriptor();
            hcd.setClassDescriptor(cd);
            return hcd;
        }
        else {
            return null;
        }
    }

}
