/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.sodbeans.compiler.quorum.refactor;

import java.util.Iterator;
import java.util.Set;
import javax.swing.text.Document;
import org.netbeans.modules.csl.api.InstantRenamer;
import org.netbeans.modules.csl.api.OffsetRange;
import org.netbeans.modules.csl.spi.ParserResult;
import org.openide.filesystems.FileUtil;
import org.quorum.symbols.ClassDescriptor;
import org.quorum.symbols.FileDescriptor;
import org.quorum.symbols.MethodDescriptor;
import org.quorum.vm.implementation.QuorumVirtualMachine;
import org.sodbeans.compiler.quorum.impl.QuorumEditorParser;
import org.sodbeans.compiler.quorum.impl.QuorumParserResult;
import org.netbeans.modules.editor.NbEditorUtilities;
import org.openide.text.Line;
import org.quorum.symbols.VariableParameterCommonDescriptor;

/**
 *
 * @author astefik
 */
public class QuorumInstantRenamer implements InstantRenamer{

    public boolean isRenameAllowed(ParserResult pr, int caretPosition, String[] strings) {
        QuorumParserResult qpr = (QuorumParserResult)pr;
        QuorumVirtualMachine vm = qpr.getVirtualMachine();
        boolean identifierFound = false;
        int currentPosition = caretPosition;
        int endPosition = -1;
        int startPosition = -1;
        
        //find end
        identifierFound = Character.isJavaIdentifierPart(qpr.getSnapshot().getText().charAt(currentPosition));
        
        //is it the start or end
        if(!identifierFound && Character.isJavaIdentifierPart(qpr.getSnapshot().getText().charAt(currentPosition + 1))){
            startPosition = caretPosition;
            identifierFound = true;
            //find the end
            while(identifierFound){
                currentPosition++;
                identifierFound = Character.isJavaIdentifierPart(qpr.getSnapshot().getText().charAt(currentPosition));

                if(!identifierFound){
                    endPosition = currentPosition;
                }
            }
        }else if(!identifierFound && Character.isJavaIdentifierPart(qpr.getSnapshot().getText().charAt(currentPosition - 1))){
            endPosition = caretPosition;
            identifierFound = true;
            //find the start
            while(identifierFound){
                currentPosition--;
                identifierFound = Character.isJavaIdentifierPart(qpr.getSnapshot().getText().charAt(currentPosition));

                if(!identifierFound){
                    startPosition = currentPosition;
                }
            }
        }else{
            //if we haven't found it, find the end.
            while(identifierFound){
                currentPosition++;
                identifierFound = Character.isJavaIdentifierPart(qpr.getSnapshot().getText().charAt(currentPosition));

                if(!identifierFound){
                    endPosition = currentPosition;
                }
            }

            //if we haven't found it, find the end.
            currentPosition = caretPosition;
            identifierFound = Character.isJavaIdentifierPart(qpr.getSnapshot().getText().charAt(currentPosition));

            while(identifierFound){
                currentPosition--;
                identifierFound = Character.isJavaIdentifierPart(qpr.getSnapshot().getText().charAt(currentPosition));

                if(!identifierFound){
                    startPosition = currentPosition;
                }
            }
        }
        //get the subSequence that is highlighted
        CharSequence subSequence = pr.getSnapshot().getText().subSequence(startPosition+1, endPosition);
        FileDescriptor file = vm.getSymbolTable().getFileDescriptor(FileUtil.toFile(qpr.getSnapshot().getSource().getFileObject()).getAbsolutePath());
        Iterator<ClassDescriptor> classes = file.getClassIterator();
        ClassDescriptor clazz = null;
        while(classes.hasNext()) {
            clazz = classes.next();
        }
        
        if(clazz != null) {
            Document document = qpr.getSnapshot().getSource().getDocument(true);
            Line line = NbEditorUtilities.getLine(document, startPosition, false);
            MethodDescriptor methodAtLine = clazz.getMethodAtLine(line.getLineNumber() + 1, 0);
            VariableParameterCommonDescriptor variable = methodAtLine.getVariable(subSequence.toString());
            if(variable != null){
                return true;
            }else{ 
                return false;
            }
        }
        
        return false;
    }

    public Set<OffsetRange> getRenameRegions(ParserResult pr, int i) {
        return null;
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
