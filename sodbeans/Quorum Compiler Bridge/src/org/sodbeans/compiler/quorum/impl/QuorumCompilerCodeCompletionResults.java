/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.sodbeans.compiler.quorum.impl;

import java.util.Collection;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;
import org.sodbeans.compiler.api.CompilerCodeCompletionResult;
import org.sodbeans.compiler.api.CompilerCodeCompletionResults;
import org.quorum.vm.interfaces.CodeCompletionItem;
import org.quorum.vm.interfaces.CodeCompletionResult;
import org.quorum.vm.interfaces.CodeCompletionType;
import org.sodbeans.compiler.api.CompilerCodeCompletionType;

/**
 *
 * @author Andreas Stefik
 */
public class QuorumCompilerCodeCompletionResults implements CompilerCodeCompletionResults{
    private CodeCompletionResult virtualMachineResults;
    /**
     * @return the results
     */
    public Set<CompilerCodeCompletionResult> getResults() {
        TreeSet<CompilerCodeCompletionResult> set = new TreeSet<CompilerCodeCompletionResult>();
        Collection<CodeCompletionItem> items = virtualMachineResults.getCompletionItems();
        Iterator<CodeCompletionItem> iterator = items.iterator();
        while(iterator.hasNext()) {
            CodeCompletionItem next = iterator.next();
            CompilerCodeCompletionResult nextResult = new CompilerCodeCompletionResult();
            nextResult.setDisplayName(next.getDisplayName());
            nextResult.setDisplayType(next.getDisplayType());
            nextResult.setCompletion(next.getCompletion());
            nextResult.setDocumentation(next.getDocumentation());
            nextResult.setType(getType(next));
            nextResult.setIsBaseClassMethod(next.isBaseClassMethod());
            set.add(nextResult);
        }
        return set;
    }

    /**
     * This converts the type generated from the virtual machine to a type
     * that can be accessed safely from outside of the module suite.
     * 
     * @param item
     * @return 
     */
    public CompilerCodeCompletionType getType(CodeCompletionItem item) {
        if(item.getCodeCompletionType() == CodeCompletionType.CLASS) {
            return CompilerCodeCompletionType.CLASS;
        } else if(item.getCodeCompletionType() == CodeCompletionType.CONTROL_STRUCTURE) {
            return CompilerCodeCompletionType.CONTROL_STRUCTURE;
        } else if(item.getCodeCompletionType() == CodeCompletionType.LOCAL_VARIABLE) {
            return CompilerCodeCompletionType.LOCAL_VARIABLE;
        } else if(item.getCodeCompletionType() == CodeCompletionType.PARAMETER) {
            return CompilerCodeCompletionType.PARAMETER;
        } else if(item.getCodeCompletionType() == CodeCompletionType.PRIMITIVE) {
            return CompilerCodeCompletionType.PRIMITIVE;
        } else if(item.getCodeCompletionType() == CodeCompletionType.PRIVATE_ACTION) {
            return CompilerCodeCompletionType.PRIVATE_ACTION;
        } else if(item.getCodeCompletionType() == CodeCompletionType.PRIVATE_BLUEPRINT_ACTION) {
            return CompilerCodeCompletionType.PRIVATE_BLUEPRINT_ACTION;
        } else if(item.getCodeCompletionType() == CodeCompletionType.PRIVATE_FIELD_VARIABLE) {
            return CompilerCodeCompletionType.PRIVATE_FIELD_VARIABLE;
        } else if(item.getCodeCompletionType() == CodeCompletionType.PRIVATE_SYSTEM_ACTION) {
            return CompilerCodeCompletionType.PRIVATE_SYSTEM_ACTION;
        } else if(item.getCodeCompletionType() == CodeCompletionType.PUBLIC_ACTION) {
            return CompilerCodeCompletionType.PUBLIC_ACTION;
        } else if(item.getCodeCompletionType() == CodeCompletionType.PUBLIC_BLUEPRINT_ACTION) {
            return CompilerCodeCompletionType.PUBLIC_BLUEPRINT_ACTION;
        } else if(item.getCodeCompletionType() == CodeCompletionType.PUBLIC_FIELD_VARIABLE) {
            return CompilerCodeCompletionType.PUBLIC_FIELD_VARIABLE;
        } else if(item.getCodeCompletionType() == CodeCompletionType.PUBLIC_SYSTEM_ACTION) {
            return CompilerCodeCompletionType.PUBLIC_SYSTEM_ACTION;
        } else if(item.getCodeCompletionType() == CodeCompletionType.PACKAGE) {
            return CompilerCodeCompletionType.PACKAGE;
        }
        return CompilerCodeCompletionType.LOCAL_VARIABLE;
    }
    
    /**
     * @return the virtualMachineResults
     */
    public CodeCompletionResult getVirtualMachineResults() {
        return virtualMachineResults;
    }

    /**
     * @param virtualMachineResults the virtualMachineResults to set
     */
    public void setVirtualMachineResults(CodeCompletionResult virtualMachineResults) {
        this.virtualMachineResults = virtualMachineResults;
    }

    /**
     * @return the filter
     */
    public String getFilter() {
        return virtualMachineResults.getFilter();
    }
}
