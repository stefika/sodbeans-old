/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sodbeans.compiler.quorum.impl;

import org.sodbeans.compiler.api.descriptors.CompilerErrorDescriptorNode;
import java.util.Iterator;
import org.openide.nodes.Children;
import org.sodbeans.compiler.api.descriptors.CompilerErrorDescriptor;
import org.quorum.vm.interfaces.CompilerError;

/**
 *
 * @author Melissa Stefik
 */
public class CompilerErrorIterator implements Iterator<CompilerErrorDescriptor> {

    private Iterator<CompilerError> errors;

    public boolean hasNext() {
        return errors.hasNext();
    }

    public CompilerErrorDescriptor next() {
        CompilerErrorDescriptorNode var = new CompilerErrorDescriptorNode(Children.LEAF);
        CompilerError obj = errors.next();
        var.setCompilerError(obj);
        return var;
    }

    /**
     * This iterator ignores all removal operations.
     */
    public void remove() {
    }

    /**
     * Sets the object to be wrapped by this iterator.
     *
     * @param it
     */
    public void setIterator(Iterator<CompilerError> it) {
        errors = it;
    }

}
