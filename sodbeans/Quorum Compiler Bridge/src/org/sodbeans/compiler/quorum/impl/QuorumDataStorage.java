/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sodbeans.compiler.quorum.impl;

import java.util.ArrayList;
import java.util.Iterator;
import org.netbeans.spi.viewmodel.ModelEvent;
import org.netbeans.spi.viewmodel.ModelListener;
import org.netbeans.spi.viewmodel.UnknownTypeException;
import org.sodbeans.compiler.api.CompilerDataStorage;
import org.quorum.plugins.ArrayPlugin;
import org.quorum.plugins.ArrayPluginInterface;
import org.quorum.vm.interfaces.AbstractVirtualMachine;
import org.quorum.vm.interfaces.Plugin;
import org.quorum.vm.interfaces.PluginManager;
import org.quorum.execution.DataEnvironment;
import org.quorum.execution.DataObject;
import org.quorum.execution.ExpressionValue;
import org.quorum.execution.RuntimeObject;
import org.quorum.execution.RuntimeScope;
import org.quorum.symbols.TypeDescriptor;
import org.quorum.vm.interfaces.DebuggerListener;

/**
 *
 * @author Andreas Stefik
 */
public class QuorumDataStorage implements CompilerDataStorage, DebuggerListener{
    private AbstractVirtualMachine virtualMachine;
    private DataEnvironment data;
    private final String QUORUM_ROOT = "Root";
    private static final String TYPE = "LocalsType";
    private static final String VALUE = "LocalsValue";
    private ArrayList<ModelListener> listeners = new ArrayList<ModelListener>();
    public static final String LOCAL_VARIABLE_IMAGE =
        "org/netbeans/modules/debugger/resources/localsView/LocalVariable";

    public static final String LOCAL_VARIABLE_GROUP_IMAGE =
        "org/netbeans/modules/debugger/resources/localsView/LocalVariablesGroup";

    public static final String SUPER_VARIABLE_IMAGE =
        "org/netbeans/modules/debugger/resources/watchesView/SuperVariable";

    public static final String FIELD_IMAGE =
        "org/netbeans/modules/debugger/resources/watchesView/Field";

    private PluginManager plugins;
    
    public Object getRoot() {
        return QUORUM_ROOT;
    }

    private void addArrayChildren(ArrayList<Object> kids, RuntimeObject object) {
        TypeDescriptor type = object.getClazz().getType();
        if(!type.isArrayClass()) { //only mess with arrays if this is an array
            return;
        }
        Plugin arrayPlugin = this.plugins.get(ArrayPlugin.KEY);
        ArrayPluginInterface ap = (ArrayPluginInterface) arrayPlugin;
        org.quorum.plugins.ArrayInterface array = ap.getArray(object.getHashKey());

        if(array != null) { //something is very wrong, since the object clearly exists
            for(int i = 0; i < array.getSize(); i++) {
                ArrayChild child = new ArrayChild();
                child.index = i;
                ExpressionValue value = array.get(i);
                if(value != null && !value.isNull()) {
                    int hash = value.getObjectHash();
                    RuntimeObject arrayChild = data.getObject(hash);
                    child.object = arrayChild;
                }
                kids.add(child);
            }
        }
    }

    public Object[] getChildren(Object parent, int from, int to) throws UnknownTypeException {
        ArrayList<Object> kids = new ArrayList<Object>();
        if(parent instanceof String) {
            String s = (String) parent;
            if(s.compareTo(QUORUM_ROOT)==0) {
                RuntimeObject ro = data.getThisObject();
                if(ro != null) {
                    ThisObject th = new ThisObject();
                    th.myThis = ro;
                    kids.add(th);
                }
                
                Iterator<DataObject> obs = data.getVariablesInActionScope();
                while(obs.hasNext()) {
                    kids.add(obs.next());
                }
            }
        }
        else if(parent instanceof DataObject) {
            DataObject d = (DataObject) parent;
            String name = d.getName();

            ExpressionValue value = d.getCurrentValue();
            if(value == null){
                value = data.getLocalScope().getVariable(name);
            }
            if(value != null && value.getType() != null && !value.getType().isPrimitiveType()) {
                if(!value.isNull()) { //it's a legal object
                    //request the object of that hash number from the
                    //data environment
                    int hash = value.getObjectHash();
                    RuntimeObject object = data.getObject(hash);

                    if(object != null && object.hasParents()) {
                        InheritanceNode node = new InheritanceNode();
                        node.child = object;
                        kids.add(node);
                    }

                    if(object != null) {
                        Iterator<DataObject> obs = object.getLocalVariables();
                        while(obs.hasNext()) {
                            kids.add(obs.next());
                        }

                        addArrayChildren(kids, object);
                    }
                }
            }


        }
        else if (parent instanceof InheritanceNode){ //an object with parents
            InheritanceNode node = (InheritanceNode) parent;
            RuntimeObject child = node.child;
            Iterator<RuntimeObject> ros = child.getParents();
            while(ros.hasNext()) {
                kids.add(ros.next());
            }
        }
        else if (parent instanceof RuntimeObject){ //a parent of an object
            RuntimeObject node = (RuntimeObject) parent;
            if(node.hasLocalVariables()) {//if it has variables, display them
                Iterator<DataObject> vars = node.getLocalVariables();
                while(vars.hasNext()) {
                    ChildNode cn = new ChildNode();
                    cn.scope = node;
                    cn.data = vars.next();
                    kids.add(cn);
                }
            }
        }
        else if (parent instanceof ThisObject){ //a parent of an object
            ThisObject th = (ThisObject)parent;
            RuntimeObject node = th.myThis;
            if(node.hasLocalVariables()) {//if it has variables, display them
                Iterator<DataObject> vars = node.getLocalVariables();
                while(vars.hasNext()) {
                    ChildNode cn = new ChildNode();
                    cn.scope = node;
                    cn.data = vars.next();
                    kids.add(cn);
                }
            }
        }
        else if (parent instanceof ChildNode){ //a parent of an object
            ChildNode n = (ChildNode) parent;
            DataObject d = n.data;
            String name = d.getName();

            ExpressionValue value = n.scope.getVariable(name);
            if(value != null && value.getType() != null && !value.getType().isPrimitiveType()) {
                if(!value.isNull()) { //it's a legal object
                    //request the object of that hash number from the
                    //data environment
                    int hash = value.getObjectHash();
                    RuntimeObject object = data.getObject(hash);

                    if(object.hasParents()) {
                        InheritanceNode node = new InheritanceNode();
                        node.child = object;
                        kids.add(node);
                    }

                    Iterator<DataObject> obs = object.getLocalVariables();
                    while(obs.hasNext()) {
                        kids.add(obs.next());
                    }
                }
            }
        }
        else if(parent instanceof ArrayChild) {
            ArrayChild child = (ArrayChild) parent;
            RuntimeObject node = child.object;
            if(node.hasLocalVariables()) {//if it has variables, display them
                Iterator<DataObject> vars = node.getLocalVariables();
                while(vars.hasNext()) {
                    ChildNode cn = new ChildNode();
                    cn.scope = node;
                    cn.data = vars.next();
                    kids.add(cn);
                }
            }
        }
           
        return kids.toArray();
    }

    public boolean isLeaf(Object node) throws UnknownTypeException {
        if(node instanceof String) {
            String s = (String) node;
            if(s.compareTo(QUORUM_ROOT)==0) {
                return false;
            }
        }

        if(node instanceof DataObject) {
            DataObject d = (DataObject) node;
            boolean primitive = d.getCurrentValue().getType().isPrimitiveType();
            if(primitive) {
                return true;
            }
            else {
                return false;
            }
        }

        else if (node instanceof InheritanceNode){ //an object with parents
            return false;
        }
        else if (node instanceof RuntimeObject){ //a parent of an object
            return !((RuntimeObject)node).hasLocalVariables(); //this should NOT be node.hasParents()
                          //as parents are flattened in child references
                          //In other words, parents of children cached in a
                          //runtime object do not store additional references
        }
        else if (node instanceof ThisObject){ //a parent of an object
            return !((ThisObject)node).myThis.hasLocalVariables(); //this should NOT be node.hasParents()
                          //as parents are flattened in child references
                          //In other words, parents of children cached in a
                          //runtime object do not store additional references
        }
        else if (node instanceof ChildNode){ //a parent of an object
            ChildNode n = (ChildNode) node;
            DataObject d = n.data;
            boolean primitive = d.getCurrentValue().getType().isPrimitiveType();
            if(primitive) {
                return true;
            }
            else {
                return false;
            }
        }
        else if(node instanceof ArrayChild) {
            ArrayChild child = (ArrayChild)node;
            if(child.object != null) {
                return child.object.getNumberLocalVariables() == 0;
            }
        }
        return true;
    }

    public int getChildrenCount(Object node) throws UnknownTypeException {
        if(node instanceof String) {
            String s = (String) node;
            if(s.compareTo(QUORUM_ROOT)==0) {
                return data.getLocalScope().getNumberLocalVariables() + 1; //save space for "This"
            }
        }
        else if(node instanceof DataObject) {
            DataObject d = (DataObject) node;
            String name = d.getName();

            ExpressionValue value = data.getLocalScope().getVariable(name);
            if(value != null && value.getType() != null && !value.getType().isPrimitiveType()) {
                if(!value.isNull()) { //it's a legal object
                    //request the object of that hash number from the
                    //data environment
                    int hash = value.getObjectHash();
                    RuntimeObject object = data.getObject(hash);
                    int vars = object.getNumberLocalVariables() + object.getNumberParents();
                    return vars;
                }
            }
        }
        else if (node instanceof InheritanceNode){ //an object with parents
            InheritanceNode n = (InheritanceNode) node;
            return n.child.getNumberParents();
        }
        else if (node instanceof RuntimeObject){ //a parent of an object
            return ((RuntimeObject)node).getNumberLocalVariables(); //don't add parents, as they are flattened in the child, not stored here
        }
        else if (node instanceof ThisObject){ //a parent of an object
            return ((ThisObject)node).myThis.getNumberLocalVariables(); //don't add parents, as they are flattened in the child, not stored here
        }
        else if (node instanceof ChildNode){ //a parent of an object
            ChildNode n = (ChildNode) node;
            DataObject d = n.data;
            String name = d.getName();

            ExpressionValue value = n.scope.getVariable(name);
            if(value != null && value.getType() != null && !value.getType().isPrimitiveType()) {
                if(!value.isNull()) { //it's a legal object
                    //request the object of that hash number from the
                    //data environment
                    int hash = value.getObjectHash();
                    RuntimeObject object = data.getObject(hash);
                    int vars = object.getNumberLocalVariables() + object.getNumberParents();
                    return vars;
                }
            }
        }
        else if(node instanceof ArrayChild) {
            ArrayChild child = (ArrayChild)node;
            if(child.object != null) {
                return child.object.getNumberLocalVariables();
            }
            return 0;
        }

        //We are either in an error condition or there are no children.
        //Either way, return 0.
        return 0;
    }

    public void addModelListener(ModelListener l) {
        listeners.add(l);
    }

    public void removeModelListener(ModelListener l) {
        listeners.remove(l);
    }

    public String getDisplayName(Object node) throws UnknownTypeException {
        if(node instanceof DataObject) {
            DataObject d = (DataObject) node;
            return d.getName();
        }
        else if (node instanceof InheritanceNode){ //an object with parents
            return "inherited";
        }
        else if (node instanceof RuntimeObject){ //a parent of an object
            RuntimeObject ro = (RuntimeObject) node;
            return ro.getClazz().getStaticKey();
        }
        else if (node instanceof ThisObject){ //a parent of an object
            return "me";
        }
        else if (node instanceof ChildNode){ //a parent of an object
            ChildNode cn = (ChildNode) node;
            return cn.data.getName();
        }
        else if(node instanceof ArrayChild) {
            return "[" + ((ArrayChild)node).index + "]";
        }
        return "";
    }

    public String getIconBase(Object node) throws UnknownTypeException {
        if(node instanceof DataObject) {
            return LOCAL_VARIABLE_IMAGE;
        }
        else if (node instanceof InheritanceNode){ //an object with parents
            return SUPER_VARIABLE_IMAGE;
        }
        else if (node instanceof RuntimeObject){ //a parent of an object
            return SUPER_VARIABLE_IMAGE;
        }
        else if (node instanceof ThisObject){ //a parent of an object
            return FIELD_IMAGE; //TODO: Change this, perhaps?
        }
        else if (node instanceof ChildNode){ //a parent of an object
            return FIELD_IMAGE;
        }
        else if(node instanceof ArrayChild) {
            return LOCAL_VARIABLE_IMAGE;
        }
        return LOCAL_VARIABLE_IMAGE;
    }

    public String getShortDescription(Object node) throws UnknownTypeException {
        if(node instanceof DataObject) {
            DataObject d = (DataObject) node;
            return d.getName() + ", " + d.getCurrentValue().getType().toString() +
                    ", " + d.getCurrentValue().toString();
        }
        else if (node instanceof InheritanceNode){ //an object with parents
            return "inherited";
        }
        else if (node instanceof RuntimeObject){ //a parent of an object
            return "An object";
        }
        else if (node instanceof ThisObject){ //a parent of an object
            return "The reference to me";
        }
        else if (node instanceof ChildNode){ //a parent of an object
            DataObject d = ((ChildNode) node).data;
            return d.getName() + ", " + d.getCurrentValue().getType().toString() +
                    ", " + d.getCurrentValue().toString();
        }
        else if(node instanceof ArrayChild) {
            return "Array value";
        }
        return "";
    }

    public Object getValueAt(Object node, String columnID) throws UnknownTypeException {
        if(node instanceof DataObject) {
            DataObject d = (DataObject) node;
            if(columnID.compareTo(TYPE) == 0) {
                return d.getCurrentValue().getType().toString();
            }
            if(columnID.compareTo(VALUE) == 0) {
                return d.getCurrentValue().toString();
            }
        }
        else if (node instanceof InheritanceNode){ //an object with parents
            return "";
        }
        else if(node instanceof RuntimeObject) { //a parent
            RuntimeObject ro = (RuntimeObject) node;
            if(columnID.compareTo(TYPE) == 0) {
                return ro.getClazz().getStaticKey();
            }
            if(columnID.compareTo(VALUE) == 0) {
                return "";
            }
        }
        else if(node instanceof ThisObject) { //a parent
            RuntimeObject ro = ((ThisObject) node).myThis;
            if(columnID.compareTo(TYPE) == 0) {
                return ro.getClazz().getStaticKey();
            }
            if(columnID.compareTo(VALUE) == 0) {
                return "#" + ro.getHashKey();
            }
        }
        else if(node instanceof ChildNode) {
            DataObject d = ((ChildNode) node).data;
            if(columnID.compareTo(TYPE) == 0) {
                return d.getCurrentValue().getType().toString();
            }
            if(columnID.compareTo(VALUE) == 0) {
                return d.getCurrentValue().toString();
            }
        }
        else if(node instanceof ArrayChild) {
            ArrayChild ac = (ArrayChild) node;
            if(columnID.compareTo(TYPE) == 0) {
                if(ac.object != null) {
                    return ac.object.getClazz().getType().toString();
                }
                else {
                    return TypeDescriptor.OBJECT;
                }
            }
            if(columnID.compareTo(VALUE) == 0) {
                if(ac.object != null) {
                    return "#" + ac.object.getHashKey();
                }
                else {
                    return "undefined";
                }
            }
        }
        return "";
    }

    public boolean isReadOnly(Object node, String columnID) throws UnknownTypeException {
        return false;
    }

    public void setValueAt(Object node, String columnID, Object value) throws UnknownTypeException {
        
    }

    /**
     * @return the virtualMachine
     */
    public AbstractVirtualMachine getVirtualMachine() {
        return virtualMachine;
    }

    /**
     * @param virtualMachine the virtualMachine to set
     */
    public void setVirtualMachine(AbstractVirtualMachine virtualMachine) {
        this.virtualMachine = virtualMachine;
        data = virtualMachine.getDataEnvironment();
       // this.quorumNativeLibrary = (QuorumLibraryManager) virtualMachine.getNativeLibraries();
        plugins = virtualMachine.getPluginManager();
    }

    public void update() {
        ModelEvent event = new ModelEvent.TreeChanged(this);
        for(ModelListener l : listeners) {
            l.modelChanged(event);
        }
    }

    private class InheritanceNode {
        public RuntimeObject child;
    }

    private class ChildNode { //represents a DataObject that can be found in a particular scope
        DataObject data;
        RuntimeScope scope;
    }

    private class ThisObject {
        RuntimeObject myThis;
    }

    private class ArrayChild {
        int index;
        RuntimeObject object;
    }
}
