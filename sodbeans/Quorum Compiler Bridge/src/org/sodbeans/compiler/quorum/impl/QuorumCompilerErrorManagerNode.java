/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sodbeans.compiler.quorum.impl;

import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.sodbeans.compiler.api.CompilerErrorManager;

/**
 *
 * @author Melissa
 */
public class QuorumCompilerErrorManagerNode extends AbstractNode {

    private CompilerErrorManager compilerErrorManager;
    
    public QuorumCompilerErrorManagerNode(Children children){
        super(children);
    }

    public CompilerErrorManager getCompilerErrorManager(){
        return compilerErrorManager;
    }

    public void setCompilerErrorManager(CompilerErrorManager errorManager){
        this.compilerErrorManager = errorManager;
    }

    @Override
    public String getDisplayName() {
        return "Description";
    }
}
