/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.sodbeans.compiler.quorum.impl;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.netbeans.spi.lexer.Lexer;
import org.netbeans.spi.lexer.LexerRestartInfo;
import org.sodbeans.compiler.api.CompilerLanguageHierarchy;
import org.sodbeans.compiler.api.CompilerTokenId;
import org.sodbeans.compiler.util.AntlrTokenReader;

/**
 * This class represents a general overview of the Quorum Programming language,
 * which serves as an adaptor between the compiler used in the Sodbeans project
 * and the parser and lexer architectures built into the NetBeans platform.
 * @author Andreas Stefik
 */
public class QuorumLanguageHierarchy extends CompilerLanguageHierarchy {

    private static List<CompilerTokenId> tokens;
    private static Map<Integer, CompilerTokenId> idToToken;

    /**
     * Initializes the list of tokens with IDs generated from the ANTLR
     * token file in the Quorum Parser.
     */
    private static void init() {
        AntlrTokenReader reader = new AntlrTokenReader();
        tokens = reader.readTokenFile();
        idToToken = new HashMap<Integer, CompilerTokenId>();
        for (CompilerTokenId token : tokens) {
            idToToken.put(token.ordinal(), token);
        }
        int a = 5;
    }

    /**
     * Returns an actual CompilerTokenId from an id. This essentially allows
     * the syntax highligher to decide the color of specific words.
     * @param id
     * @return
     */
    static synchronized CompilerTokenId getToken(int id) {
        if (idToToken == null) {
            init();
        }
        return idToToken.get(id);
    }

    /**
     * Initializes the tokens in use.
     * 
     * @return
     */
    protected synchronized Collection<CompilerTokenId> createTokenIds() {
        if (tokens == null) {
            init();
        }
        return tokens;
    }

    /**
     * Creates a lexer object for use in syntax highlighting.
     * 
     * @param info
     * @return
     */
    protected synchronized Lexer<CompilerTokenId> createLexer(LexerRestartInfo<CompilerTokenId> info) {
        return new QuorumEditorLexer(info);
    }

    /**
     * Returns the mime type of this programming language ("text/x-quorum). This
     * allows NetBeans to load the appropriate editors and file loaders when
     * a file with the quorum file extension is loaded.
     * 
     * @return
     */
    protected String mimeType() {
        return "text/x-quorum";
    }
}
