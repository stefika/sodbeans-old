/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sodbeans.compiler.quorum.impl;

import java.io.File;
import java.util.Date;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import javax.swing.event.ChangeListener;
import org.netbeans.api.project.FileOwnerQuery;
import org.netbeans.api.project.Project;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.api.Task;
import org.netbeans.modules.parsing.spi.ParseException;
import org.netbeans.modules.parsing.spi.Parser;
import org.netbeans.modules.parsing.spi.SourceModificationEvent;
import org.openide.filesystems.FileObject;
import org.openide.util.Lookup;
import org.quorum.vm.implementation.QuorumVirtualMachine;
import org.quorum.parser.QuorumParser;
import org.quorum.vm.interfaces.CompilerError;
import org.sodbeans.compiler.api.descriptors.CompilerErrorDescriptor;

/**
 * Many thanks to the NetBeans wiki:
 * http://wiki.netbeans.org/New_Language_Support_Tutorial_Antlr
 * 
 * @author Andreas Stefik
 */
public class QuorumEditorParser extends Parser{
    private Snapshot snapshot;
    private QuorumParser parser;
    private static FileObject currentFileInEditor;
    private static final String UI_LOGGER_NAME = "org.netbeans.ui.sodbeansBuild";

    private org.sodbeans.compiler.api.Compiler compiler =
                Lookup.getDefault().lookup(org.sodbeans.compiler.api.Compiler.class);
    
    @Override
    public void parse(Snapshot snapshot, Task task, SourceModificationEvent event)
            throws ParseException {
        QuorumCompiler quorum = (QuorumCompiler) compiler;
        this.snapshot = snapshot;
        currentFileInEditor = snapshot.getSource().getFileObject();
        CharSequence text = this.snapshot.getText();
        String editorText = text.toString();

        File file = org.openide.filesystems.FileUtil.toFile(currentFileInEditor);

        QuorumVirtualMachine machine = (QuorumVirtualMachine) QuorumCompiler.getVirtualMachine();
        
        //don't recompile if the debugger is running
        if(compiler.isDebuggerActive()) {
            return;
        }
        
        Date date = new Date();
        long startTime = date.getTime();
        if(compiler.hasBuiltAllOnce()) {
            machine.buildSingle(file, editorText);
        }
        else {
            Project project = FileOwnerQuery.getOwner(currentFileInEditor);
            compiler.compile(project); //ignore failure
        }
        
        date = new Date();
        long endTime = date.getTime();
        addSuccessfulBuildLog(startTime, endTime, machine.getCompilerErrors().isCompilationErrorFree(), machine.getCompilerErrors().getNumberSyntaxErrors());
        Iterator<CompilerErrorDescriptor> iterator = compiler.getCompilerErrorManager().iterator();
        while(iterator.hasNext()){
                CompilerErrorDescriptor next = iterator.next();
                addCompilerErrorBuildLog(next);
        }
    }

    @Override
    public Result getResult(Task task) throws ParseException {
        return new QuorumParserResult(snapshot, parser);
    }

    /**
     * Returns the current file that is in the editor.
     * 
     * @return
     */
    public static FileObject getCurrentFileInEditor() {
        return currentFileInEditor;
    }

    @Override
    public void addChangeListener(ChangeListener arg0) {
    }

    @Override
    public void removeChangeListener(ChangeListener arg0) {
    }
    
    public static void addSuccessfulBuildLog(long startTime, long endTime, boolean buildSuccess, int compilerErrorCount) {
        LogRecord record = new LogRecord(Level.INFO, "SODBEANS_AUTOBUILD");
        record.setParameters(new Object[]{startTime, endTime, buildSuccess, compilerErrorCount});
        record.setLoggerName(UI_LOGGER_NAME);
        Logger.getLogger(UI_LOGGER_NAME).log(record);
    }
    
    public static void addCompilerErrorBuildLog(CompilerErrorDescriptor error){
        LogRecord record = new LogRecord(Level.INFO, "SODBEANS_COMPILER_ERROR");
        record.setParameters(new Object[]{error.getErrorType().toString(), error.getFileName(), error.getAbsolutePath(), error.getLine(), error.getDescription()});
        record.setLoggerName(UI_LOGGER_NAME);
        Logger.getLogger(UI_LOGGER_NAME).log(record);
    }

}
