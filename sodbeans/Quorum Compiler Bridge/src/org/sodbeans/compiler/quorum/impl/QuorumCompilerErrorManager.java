/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sodbeans.compiler.quorum.impl;

import java.util.*;
import org.openide.nodes.Node;
import org.sodbeans.compiler.api.CompilerErrorManager;
import org.sodbeans.compiler.api.descriptors.CompilerErrorDescriptor;
import org.quorum.vm.interfaces.AbstractVirtualMachine;

/**
 *
 * @author Melissa Stefik
 */
public class QuorumCompilerErrorManager extends CompilerErrorManager{
    private AbstractVirtualMachine machine;
    protected QuorumCompilerErrorManagerNode node = new QuorumCompilerErrorManagerNode(this);
    protected static org.sodbeans.compiler.api.Compiler compiler;
    private Vector<CompilerErrorDescriptor> errors = new Vector<CompilerErrorDescriptor>();
    
    @Override
    public synchronized Node getRootCompilerErrorNode() {
        errors = new Vector<CompilerErrorDescriptor>();

        if(!isEmpty()){
           Iterator<CompilerErrorDescriptor> errorIterator = iterator();

            while(errorIterator.hasNext()){
                CompilerErrorDescriptor errorObject = errorIterator.next();
                errors.add(errorObject);
            }
            node.setCompilerErrorManager(this);

            if(!errors.isEmpty()){
                setKeys(errors);
                errors.clear();
            }
            else{
                setKeys(new Vector<CompilerErrorDescriptor>());
            }

            return node;
        }
        else{
            return null;
        }
    }

    @Override
    protected Node[] createNodes(CompilerErrorDescriptor ed) {
        return new Node[]{ed.getRootNode()};
    }

    /**
     * @return the machine
     */
    public AbstractVirtualMachine getMachine() {
        return machine;
    }

    /**
     * @param machine the machine to set
     */
    public void setMachine(AbstractVirtualMachine machine) {
        this.machine = machine;
    }

    @Override
    public boolean isEmpty() {
        return this.machine.getCompilerErrors().isCompilationErrorFree();
    }

    @Override
    public int getNumberOfSyntaxErrors() {
        return this.machine.getCompilerErrors().getNumberSyntaxErrors();
    }

    @Override
    public Iterator<CompilerErrorDescriptor> iterator() {

        CompilerErrorIterator errorIterator = new CompilerErrorIterator();
        errorIterator.setIterator(this.machine.getCompilerErrors().iterator());

        return errorIterator;
    }

    /**
     * @return the compiler
     */
    public static org.sodbeans.compiler.api.Compiler getCompiler() {
        return compiler;
    }

    /**
     * @param aCompiler the compiler to set
     */
    public static void setCompiler(org.sodbeans.compiler.api.Compiler aCompiler) {
        compiler = aCompiler;
    }

}
