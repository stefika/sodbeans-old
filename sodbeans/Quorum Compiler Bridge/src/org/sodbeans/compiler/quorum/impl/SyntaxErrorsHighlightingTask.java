/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sodbeans.compiler.quorum.impl;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.swing.text.Document;
import org.netbeans.modules.parsing.spi.Parser.Result;
import org.netbeans.modules.parsing.spi.ParserResultTask;
import org.netbeans.modules.parsing.spi.Scheduler;
import org.netbeans.modules.parsing.spi.SchedulerEvent;
import org.netbeans.spi.editor.hints.ErrorDescription;
import org.netbeans.spi.editor.hints.ErrorDescriptionFactory;
import org.netbeans.spi.editor.hints.HintsController;
import org.netbeans.spi.editor.hints.Severity;
import org.quorum.vm.interfaces.CompilerError;
import org.quorum.vm.implementation.QuorumVirtualMachine;

/**
 * Many thanks to the NetBeans wiki:
 * http://wiki.netbeans.org/New_Language_Support_Tutorial_Antlr
 *
 * @author Andreas Stefik
 */
public class SyntaxErrorsHighlightingTask extends ParserResultTask {

    @Override
    public void run(Result result, SchedulerEvent event) {
        try {
            QuorumParserResult res = (QuorumParserResult) result;
            File file = org.openide.filesystems.FileUtil.toFile(
                    res.getSnapshot().getSource().getFileObject());
            QuorumVirtualMachine vm = res.getVirtualMachine();
            Iterator<CompilerError> errors = 
                    vm.getCompilerErrors().iterator(file.getAbsolutePath());
            Document document = result.getSnapshot().getSource().getDocument(false);
            List<ErrorDescription> nbErrors = new ArrayList<ErrorDescription>();
            while(errors.hasNext()) {
                ErrorDescription errorDescription;
                CompilerError error = errors.next();
                errorDescription = ErrorDescriptionFactory.createErrorDescription(
                        Severity.ERROR, error.getError(),
                        document,
                        error.getLineNumber());
                nbErrors.add(errorDescription);
            }
            HintsController.setErrors(document, "quorum", nbErrors);
        } catch (Exception ex) { //there's really no point to logging these
        }
        catch (AssertionError error) {
        }
    }

    @Override
    public int getPriority() {
        return 100;
    }

    @Override
    public Class<? extends Scheduler> getSchedulerClass() {
        return Scheduler.EDITOR_SENSITIVE_TASK_SCHEDULER;
    }

    @Override
    public void cancel() {
    }

}
