/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sodbeans.compiler.quorum.impl;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.logging.Logger;
import org.netbeans.api.project.FileOwnerQuery;
import org.netbeans.api.project.Project;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.modules.InstalledFileLocator;
import org.openide.util.Exceptions;
import org.quorum.debugger.audio.VirtualMachineErrorAction;
import org.sodbeans.compiler.api.CompilerCodeCompletionRequest;
import org.sodbeans.compiler.api.CompilerCodeCompletionResults;
import org.sodbeans.compiler.api.CompilerDataStorage;
import org.sodbeans.compiler.api.CompilerErrorManager;
import org.sodbeans.compiler.api.descriptors.CompilerFileDescriptor;
import org.sodbeans.compiler.quorum.descriptors.QuorumFileDescriptor;
import org.quorum.vm.interfaces.AbstractVirtualMachine;
import org.quorum.vm.implementation.QuorumVirtualMachine;
import org.quorum.symbols.FileDescriptor;
import org.quorum.plugins.*;
import org.quorum.symbols.ClassDescriptor;
import org.quorum.symbols.MethodDescriptor;
import org.quorum.symbols.ParameterDescriptor;
import org.quorum.symbols.Parameters;
import org.quorum.vm.interfaces.CodeCompletionRequest;
import org.quorum.vm.interfaces.CodeCompletionResult;
import org.quorum.vm.implementation.QuorumStandardLibrary;
import org.sodbeans.compiler.api.CompilerLocation;

/**
 * This class implements the general Sodbeans compiler class with the Quorum
 * virtual machine.
 * 
 * @author Andreas Stefik
 */
public class QuorumCompiler extends org.sodbeans.compiler.api.Compiler {
    protected static QuorumDataStorage data;
    private static final String STANDARD_LIBRARY_CODE_BASE_NAME = "org.sodbeans.quorum.libraries";
    private static final String STANDARD_LIBRARY_ROOT_NAME = "Libraries";
    private static final String STANDARD_LIBRARY_INDEX_PATH = "indexes/quorum.index";
    private static final String STANDARD_LIBRARY_ROOT_PATH = "quorum";
    private static final String SERVLET_FOLDER = "servlet";
    private static final String WOVEN_DEPENDENCIES_FOLDER = "dependencies/org";
    private static final String QUORUM_COMPILED_PLUGINS_PATH = "plugins";
    private static final String BUILD_CLASSES_FOLDER = "classes";
    private static final Logger logger = Logger.getLogger(QuorumCompiler.class.getName());
    private FileObject build;
    private FileObject run;
    
    public QuorumCompiler() {
    }

    private static void vmSingleton() {
        if(virtualMachine == null) {
            QuorumStandardLibrary.overrideStandardLibraryPath(getRootFolder(), getIndexFile());
            virtualMachine = new QuorumVirtualMachine();
            virtualMachine.getCodeGenerator().setServletFolder(getServletFolderDefault());
            virtualMachine.getCodeGenerator().setWovenDependenciesFolder(getWovenDependenciesFolderDefault());
            SodbeansConsole console = new SodbeansConsole();
            VirtualMachineErrorAction.setConsole(console);
            ConsolePlugin.setConsole(console);
            addPlugins(virtualMachine);
            
            virtualMachine.setGenerateCode(true);
            File compiledPluginsFolder = getCompiledPluginsFolder();
            
            virtualMachine.setPluginFolder(compiledPluginsFolder);
            
            
            
            compilerErrorManager = new QuorumCompilerErrorManager();
            ((QuorumCompilerErrorManager)compilerErrorManager).setMachine(virtualMachine);
            data = new QuorumDataStorage();
            data.setVirtualMachine(virtualMachine);
            virtualMachine.addListener(data);
        }
    }
    
    @Override
    public FileObject getBuildFolder() {
        return build;
    }

    @Override
    public FileObject getDistributionFolder() {
        return run;
    }
    
    @Override
    public void addDependency(File file) {
        virtualMachine.addDependency(file);
    }
    
    @Override
    public void addDependency(File file, String relativePath) {
        virtualMachine.addDependency(file, relativePath);
    }

    @Override
    public void clearDependencies() {
        virtualMachine.clearDependencies();
    }

    @Override
    public void setBuildFolder(FileObject folder) {
        build = folder;
        File classFile = FileUtil.toFile(build);
        virtualMachine.setBuildFolder(classFile);
    }

    @Override
    public void setDistributionFolder(FileObject folder) {
        run = folder;
        File runFile = FileUtil.toFile(run);
        virtualMachine.setDistributionFolder(runFile);
    }

    private static File getIndexFile() {
        File file = InstalledFileLocator.getDefault().locate(
                STANDARD_LIBRARY_INDEX_PATH, STANDARD_LIBRARY_CODE_BASE_NAME, false);
        return file;
    }
    
    public static File getRootFolder() {
        File file = InstalledFileLocator.getDefault().locate(
            STANDARD_LIBRARY_ROOT_PATH, STANDARD_LIBRARY_CODE_BASE_NAME, false);
        return file;
    }
    
    public static File getServletFolderDefault() {
        File file = InstalledFileLocator.getDefault().locate(
            SERVLET_FOLDER, STANDARD_LIBRARY_CODE_BASE_NAME, false);
        return file;
    }
    
    public static File getWovenDependenciesFolderDefault() {
        File file = InstalledFileLocator.getDefault().locate(
            WOVEN_DEPENDENCIES_FOLDER, STANDARD_LIBRARY_CODE_BASE_NAME, false);
        return file;
    }
    
    public static File getCompiledPluginsFolder() {
        File file = InstalledFileLocator.getDefault().locate(
            QUORUM_COMPILED_PLUGINS_PATH, STANDARD_LIBRARY_CODE_BASE_NAME, false);
        return file;
    }
    
    private static void addPlugins(AbstractVirtualMachine machine) {
        DefaultPluginLoader loader = new DefaultPluginLoader();
        loader.loadIntoVirtualMachine(machine);
        loader.checkConsistency(machine);
    }
    
    protected void createVM() {
        vmSingleton();
    }

    /**
     * Obtains the current Virtual Machine in use.
     *
     * @return
     */
    public static AbstractVirtualMachine getVirtualMachine() {
        vmSingleton();
        return virtualMachine;
    }

    /**
     * Returns the file extension used by this compiler.
     * 
     * @return
     */
    public String getSourceFileExtension() {
        return "quorum";
    }

    @Override
    public CompilerFileDescriptor getFileDescriptor(FileObject file) {
        if(file != null) {
            File ioFile = org.openide.filesystems.FileUtil.toFile(file);
            //ask the virtual machine for a fileDescriptor of a different type
            FileDescriptor fd = virtualMachine.getSymbolTable().getFileDescriptor(ioFile.getAbsolutePath());
            if(fd == null && !hasBuiltAllOnce()) {//try compiling if we haven't already
                Project project = FileOwnerQuery.getOwner(file);
                compile(project);
                fd = virtualMachine.getSymbolTable().getFileDescriptor(ioFile.getAbsolutePath());
            }
            QuorumFileDescriptor cfd = new QuorumFileDescriptor();
            cfd.setFileDescriptor(fd);
            return cfd;
        }
        else {
            return null;
        }
    }

    @Override
    public Iterator<CompilerFileDescriptor> getFileDescriptors() {
        Iterator<FileDescriptor> files = virtualMachine.getSymbolTable().getFileDescriptors();
        QuorumFileDescriptorIterator it = new QuorumFileDescriptorIterator();
        it.setFiles(files);
        return it;
    }

    @Override
    public CompilerErrorManager getCompilerErrorManager() {
        return compilerErrorManager;
    }

    @Override
    public CompilerDataStorage getDataStorage() {
        return data;
    }
    
    public CompilerCodeCompletionResults requestCodeCompletionResult(CompilerCodeCompletionRequest request) {
        QuorumCompilerCodeCompletionResults results = new QuorumCompilerCodeCompletionResults();
        
        //set the request
        CodeCompletionRequest req = new CodeCompletionRequest();
        req.setFileKey(request.getFileKey());
        req.setLine(request.getLine());
        req.setLineNumber(request.getLineNumber());     
        req.setStartOffset(request.getStartOffset());
        
        //adapt it for outside classes to use
        CodeCompletionResult result = this.virtualMachine.requestCodeCompletionResult(req);
        results.setVirtualMachineResults(result);
        return results;
    }
    
    public void setAuditoryDebugging(boolean isSpeaking) {
        getVirtualMachine().setAuditoryDebugging(isSpeaking);
    }
    
    public boolean getAuditoryDebugging() {
        return getVirtualMachine().getAuditoryDebugging();
    }

    @Override
    public String getClassIndex(String staticKey) {
        return this.virtualMachine.getSymbolTable().getClassIndexer(staticKey);
    }
    
    @Override
    public FileObject getContainingFile(String staticKey) {
        ClassDescriptor cd = this.virtualMachine.getSymbolTable().getClassDescriptor(staticKey);
        
        if(cd == null) {
            return null;
        }
        FileDescriptor fd = cd.getFile();
        if(fd == null) {
            return null;
        }
        File file = fd.getFile();
        if(file == null) {
            return null;
        }
        return FileUtil.toFileObject(file);
    }

    @Override
    public CompilerLocation GetLocationAtLine(String staticKey, int line) {
        CompilerLocation location = new CompilerLocation();
        ClassDescriptor clazz = this.virtualMachine.getSymbolTable().getClassDescriptor(staticKey);
        if(clazz != null) {
            MethodDescriptor method = clazz.getMethodAtLine(line, 0);
            if(method != null) {
                location.setName(method.getName());
                Parameters parameters = method.getParameters();
                if(parameters != null) {
                    for(int i = 0; i < parameters.size(); i++) {
                        ParameterDescriptor param = parameters.get(i);
                        location.add(param.getName(), param.getType().getStaticKey());
                    }
                }
                return location;
            }
        }
        return null;
    }

    @Override
    public boolean isGenerateWar() {
        return virtualMachine.getCodeGenerator().isGenerateWar();
    }

    @Override
    public void setGenerateWar(boolean value) {
        virtualMachine.getCodeGenerator().setGenerateWar(value);
    }
    
    public FileObject getServletFolder() {
        return FileUtil.toFileObject(virtualMachine.getCodeGenerator().getServletFolder());
    }
    
    public void setServletFolder(FileObject file) {
        if(file != null) {
            File ioFile = org.openide.filesystems.FileUtil.toFile(file);
            virtualMachine.getCodeGenerator().setServletFolder(ioFile);
        }
    }
}
