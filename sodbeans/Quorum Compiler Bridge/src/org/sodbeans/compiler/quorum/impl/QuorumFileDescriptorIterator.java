/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sodbeans.compiler.quorum.impl;

import java.util.Iterator;
import org.sodbeans.compiler.api.descriptors.CompilerFileDescriptor;
import org.sodbeans.compiler.quorum.descriptors.QuorumFileDescriptor;
import org.quorum.symbols.FileDescriptor;

/**
 * An iterator of the various files currently known about in the compiler.
 * 
 * @author Andreas Stefik
 */
public class QuorumFileDescriptorIterator implements Iterator<CompilerFileDescriptor>{

    private Iterator<FileDescriptor> files;

    @Override
    public boolean hasNext() {
        return files.hasNext();
    }

    @Override
    public CompilerFileDescriptor next() {
        FileDescriptor file = files.next();
        QuorumFileDescriptor hfd = new QuorumFileDescriptor();
        hfd.setFileDescriptor(file);
        return hfd;
    }

    /**
     * Not supported, as the parser and symbol table track this automatically.
     */
    @Override
    public void remove() {
    }

    /**
     * @return the files
     */
    public Iterator<FileDescriptor> getFiles() {
        return files;
    }

    /**
     * @param files the files to set
     */
    public void setFiles(Iterator<FileDescriptor> files) {
        this.files = files;
    }

}
