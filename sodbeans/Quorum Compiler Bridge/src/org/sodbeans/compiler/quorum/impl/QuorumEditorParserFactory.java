/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sodbeans.compiler.quorum.impl;

import java.util.Collection;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.spi.Parser;
import org.netbeans.modules.parsing.spi.ParserFactory;

/**
 * Many thanks to the NetBeans wiki:
 * http://wiki.netbeans.org/New_Language_Support_Tutorial_Antlr
 * @author Andy
 */
public class QuorumEditorParserFactory extends ParserFactory{
    public static QuorumEditorParser parser = new QuorumEditorParser();
    
    @Override
    public Parser createParser(Collection<Snapshot> arg0) {
        return parser;
    }

}
