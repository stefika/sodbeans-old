/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.sodbeans.compiler.quorum.impl;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.event.DocumentEvent;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import org.netbeans.api.editor.fold.Fold;
import org.netbeans.api.editor.fold.FoldHierarchy;
import org.netbeans.api.editor.fold.FoldType;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenHierarchy;
import org.netbeans.api.lexer.TokenSequence;
import org.netbeans.spi.editor.fold.FoldHierarchyTransaction;
import org.netbeans.spi.editor.fold.FoldManager;
import org.netbeans.spi.editor.fold.FoldOperation;
import org.openide.util.Exceptions;
import org.sodbeans.compiler.api.CompilerTokenId;

/**
 *
 * @author stefika
 */
public class QuorumFoldManager implements FoldManager {

    private FoldOperation operation;
    public static final FoldType COMMENT_FOLD_TYPE = new FoldType("Notes");
    public static final FoldType ACTION_FOLD_TYPE = new FoldType("action code");
    public static final FoldType USE_FOLD_TYPE = new FoldType("use statements");
    private static final Logger logger = Logger.getLogger(QuorumFoldManager.class.getName());
    
    public void init(FoldOperation operation) {
        this.operation = operation;
    }

    public void initFolds(FoldHierarchyTransaction transaction) {
        FoldHierarchy hierarchy = operation.getHierarchy();
        Document document = hierarchy.getComponent().getDocument();
        TokenHierarchy<Document> hi = TokenHierarchy.get(document);
        TokenSequence<CompilerTokenId> ts = (TokenSequence<CompilerTokenId>) hi.tokenSequence();

        int actionStack = 0;
        CompilerTokenId actionStart = null;
        int actionOffset = -1;
        int useOffset = -1;
        int endUseOffset = -1;
        
        FoldType type = null;
        int start = 0;
        int offset = 0;
        while (ts.moveNext()) {
            offset = ts.offset();
            Token<CompilerTokenId> token = ts.token();
            CompilerTokenId id = token.id();
            if (id.name().equals(CompilerTokenId.COMMENTS)) {
                start = offset;
                try {
                    operation.addToHierarchy(
                            COMMENT_FOLD_TYPE,
                            COMMENT_FOLD_TYPE.toString(),
                            false,
                            start,
                            offset + token.length(),
                            0,
                            0,
                            hierarchy,
                            transaction);
                } catch (BadLocationException ex) {
                    logger.log(Level.INFO, ex.getMessage());
                }
            } else if (id.name().equals(CompilerTokenId.USE)) {
                useOffset = ts.offset();
                
                ts.moveNext();
                skipWhitespaceComments(ts);
                //skip any number of periods
                boolean done = false;
                while (!done && ts.moveNext()) {
                    if (    !ts.token().id().name().equals(CompilerTokenId.PERIOD)
                        &&  !ts.token().id().name().equals(CompilerTokenId.ID)) {
                        //mark the location and check if there's another use statement
                        endUseOffset = ts.offset();
                        skipWhitespaceNewLinesComments(ts);
                        if (!ts.token().id().name().equals(CompilerTokenId.USE)) {
                            done = true;
                        } else {
                            ts.moveNext();
                            skipWhitespaceNewLinesComments(ts);
                        }
                    }
                }
                
                try {
                    operation.addToHierarchy(
                            USE_FOLD_TYPE,
                            USE_FOLD_TYPE.toString(),
                            false,
                            useOffset,
                            endUseOffset,
                            0,
                            0,
                            hierarchy,
                            transaction);
                } catch (BadLocationException ex) {
                    logger.log(Level.INFO, ex.getMessage());
                }
                ts.movePrevious();
                unskipWhitespaceNewLinesComments(ts);
                
            }//if it's an action, find its end position
            else if (id.name().equals(CompilerTokenId.ACTION)) {
                actionStart = id;
                actionStack = 0;
                ts.moveNext();
                Token<CompilerTokenId> search = null;
                int returnedOffset = skipWhitespaceComments(ts);
                actionOffset = ts.offset() + ts.token().length();
                
                ts.moveNext();
                returnedOffset = skipWhitespaceComments(ts);
                
                search = ts.token();
                if (search.id().name().equals(CompilerTokenId.LEFT_PAREN)) {
                    boolean done = false;
                    while (!done && ts.moveNext()) {
                        Token<CompilerTokenId> rightSide = ts.token();
                        if (rightSide.id().name().equals(CompilerTokenId.RIGHT_PAREN)) {
                            actionOffset = ts.offset() + ts.token().length();
                            ts.moveNext();
                            returnedOffset = skipWhitespaceComments(ts);
                            rightSide = ts.token();
                            if (rightSide.id().name().equals(CompilerTokenId.RETURNS)) {
                                ts.moveNext();
                                returnedOffset = skipWhitespaceComments(ts);
                                actionOffset = ts.offset() + ts.token().length();
                            } else if(rightSide.id().name().equals(CompilerTokenId.END)) {
                                ts.movePrevious();
                            }
                            done = true;
                        }
                        else if (rightSide.id().name().equals(CompilerTokenId.RETURNS)) {
                            done = true;
                            actionOffset = ts.offset() + ts.token().length();
                            ts.moveNext();
                            returnedOffset = skipWhitespaceComments(ts);
                            if(returnedOffset != -1) {
                                actionOffset = returnedOffset;
                            }
                            ts.moveNext();
                            actionOffset = ts.offset() + ts.token().length();
                        }
                    }
                } else if(search.id().name().equals(CompilerTokenId.RETURNS)) {
                    actionOffset = ts.offset() + ts.token().length();
                    ts.moveNext();
                    returnedOffset = skipWhitespaceComments(ts);
                    actionOffset = ts.offset() + ts.token().length();
                }
                else if(search.id().name().equals(CompilerTokenId.END)) {
                    ts.movePrevious();
                }
            } else if (id.name().equals(CompilerTokenId.IF)) {
                actionStack = actionStack + 1;
            } else if (id.name().equals(CompilerTokenId.REPEAT)) {
                actionStack = actionStack + 1;
            } else if (id.name().equals(CompilerTokenId.CHECK)) {
                actionStack = actionStack + 1;
            } else if (id.name().equals(CompilerTokenId.END)) {
                if (actionStack == 0 && actionOffset != -1) { //matching action statement
                    start = actionOffset;
                    try {
                        operation.addToHierarchy(
                                ACTION_FOLD_TYPE,
                                ACTION_FOLD_TYPE.toString(),
                                false,
                                start,
                                offset + token.length(),
                                0,
                                0,
                                hierarchy,
                                transaction);
                    } catch (BadLocationException ex) {
                        logger.log(Level.INFO, ex.getMessage());
                    }
                }
                actionStack = actionStack - 1;
            }
        }
    }
    
    /**
     * Skips all whitepsace tokens and returns the offset of the last one.
     * 
     * @param ts
     * @return an offset of the last whitespace token. If there was no whitespace
     * this method returns -1.
     */
    private int skipWhitespaceComments(TokenSequence<CompilerTokenId> ts) {
        boolean finished = false;
        if(!ts.token().id().name().equals(CompilerTokenId.WS)
           && !ts.token().id().name().equals(CompilerTokenId.COMMENTS)) {
            return -1;
        }
        
        int offset = -1;
        while(!finished && ts.moveNext()) { //either a left paren or a returns
            Token<CompilerTokenId> search = ts.token();
            if (search.id().name().equals(CompilerTokenId.WS)
                || search.id().name().equals(CompilerTokenId.COMMENTS)) {
                offset = ts.offset() + ts.token().length();
            } else {
                finished = true;
            }
        }
        return offset;
    }
    
    private void unskipWhitespaceNewLinesComments(TokenSequence<CompilerTokenId> ts) {
        boolean finished = false;
        if(!ts.token().id().name().equals(CompilerTokenId.WS)
           && !ts.token().id().name().equals(CompilerTokenId.COMMENTS)
           && !ts.token().id().name().equals(CompilerTokenId.NEWLINE)) {
            return;
        }
        
        while(!finished && ts.movePrevious()) { //either a left paren or a returns
            Token<CompilerTokenId> search = ts.token();
            if (search.id().name().equals(CompilerTokenId.WS)
                || search.id().name().equals(CompilerTokenId.COMMENTS)
                || search.id().name().equals(CompilerTokenId.NEWLINE)) {
            } else {
                finished = true;
            }
        }
    }
    
    private int skipWhitespaceNewLinesComments(TokenSequence<CompilerTokenId> ts) {
        boolean finished = false;
        if(!ts.token().id().name().equals(CompilerTokenId.WS)
           && !ts.token().id().name().equals(CompilerTokenId.COMMENTS)
           && !ts.token().id().name().equals(CompilerTokenId.NEWLINE)) {
            return -1;
        }
        
        int offset = -1;
        while(!finished && ts.moveNext()) { //either a left paren or a returns
            Token<CompilerTokenId> search = ts.token();
            if (search.id().name().equals(CompilerTokenId.WS)
                || search.id().name().equals(CompilerTokenId.COMMENTS)
                || search.id().name().equals(CompilerTokenId.NEWLINE)) {
                offset = ts.offset() + ts.token().length();
            } else {
                finished = true;
            }
        }
        return offset;
    }

    public void insertUpdate(DocumentEvent evt, FoldHierarchyTransaction transaction) {
    }

    public void removeUpdate(DocumentEvent evt, FoldHierarchyTransaction transaction) {
    }

    public void changedUpdate(DocumentEvent evt, FoldHierarchyTransaction transaction) {
    }

    public void removeEmptyNotify(Fold epmtyFold) {
    }

    public void removeDamagedNotify(Fold damagedFold) {
    }

    public void expandNotify(Fold expandedFold) {
    }

    public void release() {
    }
}
