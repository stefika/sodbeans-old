/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.sodbeans.compiler.quorum.impl;

import org.netbeans.api.lexer.Language;
import org.netbeans.modules.csl.api.InstantRenamer;
import org.netbeans.modules.csl.spi.DefaultLanguageConfig;
import org.netbeans.modules.csl.spi.LanguageRegistration;
import org.netbeans.modules.parsing.spi.Parser;
import org.sodbeans.compiler.api.CompilerTokenId;
import org.sodbeans.compiler.quorum.refactor.QuorumInstantRenamer;

/**
 *
 * @author astefik
 */
@LanguageRegistration(mimeType = "text/x-quorum")
public class QuorumLanguage extends DefaultLanguageConfig{
    private static final String LINE_COMMENT_PREFIX = "//";
    
    @Override
    public Language getLexerLanguage() {
        return CompilerTokenId.getLanguage();
    }

    @Override
    public Parser getParser() {
        return QuorumEditorParserFactory.parser;
    }
    
    @Override
    public String getLineCommentPrefix() {
        return LINE_COMMENT_PREFIX;
    }

    @Override
    public String getDisplayName() {
        return "Quorum";
    }
    
    @Override
    public boolean isIdentifierChar(char c) {
        return Character.isJavaIdentifierPart(c);
    }

    @Override
    public InstantRenamer getInstantRenamer() {
        return new QuorumInstantRenamer(); //To change body of generated methods, choose Tools | Templates.
    }
}
