/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sodbeans.compiler.quorum.impl;

import java.util.Collection;
import java.util.Collections;
import org.netbeans.api.editor.mimelookup.MimeRegistration;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.spi.SchedulerTask;
import org.netbeans.modules.parsing.spi.TaskFactory;

/**
 * Many thanks to the NetBeans wiki:
 * http://wiki.netbeans.org/New_Language_Support_Tutorial_Antlr
 * 
 * @author Andreas Stefik
 */
@MimeRegistration(mimeType="text/x-quorum",service=TaskFactory.class)
public class SyntaxErrorsHighlightingTaskFactory extends TaskFactory {

    @Override
    public Collection<? extends SchedulerTask> create(Snapshot arg0) {
        return Collections.singleton (new SyntaxErrorsHighlightingTask ());
    }

}
