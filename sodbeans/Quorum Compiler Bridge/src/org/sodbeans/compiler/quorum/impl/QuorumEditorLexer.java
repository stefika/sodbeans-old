/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.sodbeans.compiler.quorum.impl;

import org.netbeans.spi.lexer.Lexer;
import org.netbeans.spi.lexer.LexerRestartInfo;
import org.sodbeans.compiler.api.CompilerTokenId;
import org.quorum.parser.QuorumLexer;

/**
 *  This class represents the lexer supported by the Quorum architecture.
 * 
 * @author Andreas Stefik
 */
public class QuorumEditorLexer implements Lexer<CompilerTokenId> {

    private LexerRestartInfo<CompilerTokenId> info;
    private QuorumLexer lexer;

    QuorumEditorLexer(LexerRestartInfo<CompilerTokenId> info) {
        this.info = info;
        AntlrCharStream charStream = new AntlrCharStream(info.input(), "QuorumEditor");
        lexer = new QuorumLexer(charStream);
    }

    @Override
    public org.netbeans.api.lexer.Token<CompilerTokenId> nextToken() {
        org.antlr.runtime.Token token = lexer.nextToken();
        if (info.input().readLength() < 1) {
            return null;
        } else if (token.getType() == -1) {
            return info.tokenFactory().createToken(QuorumLanguageHierarchy.getToken(10));
        }
        CompilerTokenId id = QuorumLanguageHierarchy.getToken(token.getType());
        org.netbeans.api.lexer.Token<CompilerTokenId> tId = info.tokenFactory().createToken(id);
        return tId;
    }

    @Override
    public Object state() {
        return null;
    }

    @Override
    public void release() {
    }
}
