/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.sodbeans.compiler.quorum.impl;

import org.netbeans.api.editor.mimelookup.MimeRegistration;
import org.netbeans.spi.editor.fold.FoldManager;
import org.netbeans.spi.editor.fold.FoldManagerFactory;

/**
 *
 * @author stefika
 */
@MimeRegistration(mimeType="text/x-quorum",service=FoldManagerFactory.class)
public class QuorumFoldManagerFactory implements FoldManagerFactory{

    public FoldManager createFoldManager() {
        return new QuorumFoldManager();
    }
    
}
