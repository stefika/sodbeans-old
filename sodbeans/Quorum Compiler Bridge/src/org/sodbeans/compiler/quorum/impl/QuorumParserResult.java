/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.sodbeans.compiler.quorum.impl;

import java.util.Collections;
import java.util.List;
import org.netbeans.modules.csl.api.Error;
import org.netbeans.modules.csl.spi.ParserResult;
import org.netbeans.modules.parsing.api.Snapshot;
import org.quorum.parser.QuorumParser;
import org.quorum.vm.implementation.QuorumVirtualMachine;

/**
 * Many thanks to the netbeans wiki:
 * http://wiki.netbeans.org/New_Language_Support_Tutorial_Antlr
 * 
 * @author Andreas Stefik
 */
public class QuorumParserResult extends ParserResult {
    private boolean valid = true;
    private QuorumParser parser;

    QuorumParserResult(Snapshot snapshot, QuorumParser p) {
        super(snapshot);
        parser = p;
    }

    public QuorumVirtualMachine getVirtualMachine() {
        return (QuorumVirtualMachine) QuorumCompiler.getVirtualMachine();
    }

    @Override
    protected void invalidate() {
        valid = false;
    }

    @Override
    public List<? extends Error> getDiagnostics() {
        return Collections.EMPTY_LIST;
    }
}
