/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.sodbeans.compiler.quorum.impl;

import org.openide.util.Lookup;
import org.quorum.plugins.Console;
import org.sodbeans.io.CommandLine;

/**
 * An implementation of the console used in the Sodbeans project. This object
 * is passed to the ConsolePlugin object at bootup so that the Quorum interpreter
 * can pass events to NetBeans.
 * 
 * @author Andreas Stefik
 */
public class SodbeansConsole implements Console{
    private CommandLine console = Lookup.getDefault().lookup(CommandLine.class);
    
    public void post(String post) {
        console.post(post);
    }

    public void unpost() {
        console.unpost();
    }

    public String getInput(String post) {
        return console.getInput(post);
    }

    public void clear() {
        console.clear();
    }
    
}
