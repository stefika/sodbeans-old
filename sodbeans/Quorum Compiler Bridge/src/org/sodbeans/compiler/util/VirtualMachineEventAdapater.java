/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sodbeans.compiler.util;

import java.io.File;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.sodbeans.compiler.api.CompilerEvent;
import org.quorum.vm.interfaces.VirtualMachineEvent;

/**
 * This class translates an event in the VirtualMachine into an event
 * recognized by the NetBeans implementation.
 *
 * @author Andreas Stefik
 */
public class VirtualMachineEventAdapater extends CompilerEvent{
    private VirtualMachineEvent event;
    @Override
    public boolean isExecuteEvent() {
        return event.isExecuteEvent();
    }
    @Override
    public boolean isVirtualMachineStopError() {
        return event.isVirtualMachineStopError();
    }
    @Override
    public boolean isDebuggerStartEvent() {
        return false;
    }
    @Override
    public boolean isDebuggerStopEvent() {
        return event.isDebuggerStopEvent();
    }

    @Override
    public boolean isNaturalTerminationEvent() {
        return event.isNaturalTerminationEvent();
    }

    @Override
    public boolean isBreakpointToggleEvent() {
        return false;
    }
    @Override
    public String getExecutionMessage() {
        return "Stepping forward is not yet implemented into CompilerEvent.";
    }
    @Override
    public String getUnexecutionMessage() {
        return "Stepping Backward is not yet implemented into CompilerEvent.";
    }


    /**
     * @return the event
     */
    protected VirtualMachineEvent getEvent() {
        return event;
    }

    /**
     * @param event the event to set
     */
    protected void setEvent(VirtualMachineEvent event) {
        this.event = event;
    }

    @Override
    public boolean isBuildEvent() {
        return event.isBuildEvent();
    }

    @Override
    public boolean isBuildAllEvent() {
        return event.isBuildAllEvent();
    }
    
    @Override
    public boolean isBuildSuccessful() {
        return event.isBuildSuccessful();
    }
    
    /**
     * Returns the file in which the code is currently being executed.
     * @return
     */
    @Override
    public FileObject getFile() {
        String path = event.getFile();
        FileObject fileObject = FileUtil.toFileObject(new File(path));
        return fileObject;
    }
}
