/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sodbeans.compiler.util;

import org.openide.text.Annotation;

/**
 *
 * @author Andreas Stefik
 */
public class LineAnnotation extends Annotation {

        @Override
        public String getAnnotationType() {
            return "CurrentPC";
        }

        @Override
        public String getShortDescription() {
            return "Current Program Counter";
        }
}
