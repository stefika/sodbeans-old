package org.sodbeans.compiler.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.openide.util.Exceptions;
import org.sodbeans.compiler.api.CompilerTokenId;

/**
 * This class reads in a file generated by ANTLR and uses it to automatically
 * tell NetBeans which words should have colored highlighting. For example,
 * keywords will show up blue, by default.
 * 
 * @author Andreas Stefik
 */
public class AntlrTokenReader {
    private HashMap<String, String> tokenTypes = new HashMap<String, String>();
    private ArrayList<CompilerTokenId> tokens = new ArrayList<CompilerTokenId>();

    public AntlrTokenReader() {
        init();
    }

    /**
     * Initializes the map to include any keywords in the Quorum Programming language.
     */
    private void init() {
        tokenTypes.put("ACTION", "keyword" );
        tokenTypes.put("ALERT", "keyword" );
        tokenTypes.put("NATIVE", "keyword" );
        tokenTypes.put("NULL", "keyword" );
        tokenTypes.put("EQUALITY", "operator" );
        tokenTypes.put("CLASS", "keyword" );
        tokenTypes.put("CONSTANT", "keyword" );
        tokenTypes.put("UNTIL", "keyword" );
        tokenTypes.put("REPEAT", "keyword" );
        tokenTypes.put("ELSE_IF", "keyword" );
        tokenTypes.put("PACKAGE_NAME", "keyword" );
        tokenTypes.put("DOUBLE_QUOTE", "whitespace" );
        tokenTypes.put("NOT", "keyword" );
        tokenTypes.put("NOTEQUALS", "keyword" );
        tokenTypes.put("ID", "identifier" );
        tokenTypes.put("AND", "keyword" );
        tokenTypes.put("IF", "keyword" );
        tokenTypes.put("RIGHT_PAREN", "separator" );
        tokenTypes.put("GREATER", "operator" );
        tokenTypes.put("BOOLEAN", "keyword" );
        tokenTypes.put("COMMENTS", "comment" );
        tokenTypes.put("MULTIPLY", "operator" );
        tokenTypes.put("COMMA", "separator" );
        tokenTypes.put("RETURN", "keyword" );
        tokenTypes.put("LESS", "operator" );
        tokenTypes.put("PLUS", "operator" );
        tokenTypes.put("BLUEPRINT", "keyword" );
        tokenTypes.put("PUBLIC", "keyword" );
        tokenTypes.put("PRIVATE", "keyword" );
        tokenTypes.put("INHERITS", "keyword" );
        tokenTypes.put("ME", "keyword" );
        tokenTypes.put("ALWAYS", "keyword" );
        tokenTypes.put("CHECK", "keyword" );
        tokenTypes.put("DETECT", "keyword" );
        tokenTypes.put("PARENT", "keyword" );
        tokenTypes.put("LEFT_ARROW", "operator" );
        tokenTypes.put("RETURNS", "keyword" );
        tokenTypes.put("DIVIDE", "operator" );
        tokenTypes.put("QUALIFIED_NAME", "whitespace" );
        tokenTypes.put("MODULO", "keyword" );
        tokenTypes.put("PERIOD", "separator" );
        tokenTypes.put("ELSE", "keyword" );
        tokenTypes.put("REFERENCE", "keyword" );
        tokenTypes.put("USE", "keyword" );
        tokenTypes.put("STATEMENT_LIST", "whitespace" );
        tokenTypes.put("INT", "number" );
        tokenTypes.put("INTEGER_KEYWORD", "keyword" );
        tokenTypes.put("NUMBER_KEYWORD", "keyword" );
        tokenTypes.put("BOOLEAN_KEYWORD", "keyword" );
        tokenTypes.put("ARRAY", "keyword" );
        tokenTypes.put("CALL_FUNCTION_TOKEN", "keyword" );
        tokenTypes.put("OUTPUT", "keyword" );
        tokenTypes.put("SAY", "keyword" );
        tokenTypes.put("INPUT", "keyword" );
        tokenTypes.put("TEXT", "keyword" );
        tokenTypes.put("MINUS", "operator" );
        tokenTypes.put("WS", "whitespace" );
        tokenTypes.put("NEWLINE", "whitespace" );
        tokenTypes.put("DECIMAL", "number" );
        tokenTypes.put("OR", "keyword" );
        tokenTypes.put("LEFT_PAREN", "separator" );
        tokenTypes.put("LESS_EQUAL", "operator" );
        tokenTypes.put("EXPRESSION_STATEMENT", "whitespace" );
        tokenTypes.put("END", "keyword" );
        tokenTypes.put("FUNCTION_CALL", "method" );
        tokenTypes.put("CONSTRUCTOR", "method" );
        tokenTypes.put("GREATER_EQUAL", "whitespace" );
        tokenTypes.put("WHILE", "keyword" );
        tokenTypes.put("TIMES", "keyword" );
        tokenTypes.put("NOW", "keyword" );
        tokenTypes.put("STRING", "string" );
        tokenTypes.put("ON", "keyword" );
        tokenTypes.put("CREATE", "keyword" );
    }

    /**
     * Reads the token file from the ANTLR parser and generates
     * appropriate tokens.
     *
     * @return
     */
    public List<CompilerTokenId> readTokenFile() {
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        InputStream inp = classLoader.getResourceAsStream("org/quorum/parser/Quorum.tokens");
        BufferedReader input = new BufferedReader(new InputStreamReader(inp));
        readTokenFile(input);
        return tokens;
    }

    /**
     * Reads in the token file.
     * 
     * @param buff
     */
    private void readTokenFile(BufferedReader buff) {
        String line = null;
        try {
            while ((line = buff.readLine()) != null) {
                String[] splLine = line.split("=");
                String name = splLine[0];
                int tok = Integer.parseInt(splLine[1].trim());
                CompilerTokenId id;
                String tokenCategory = tokenTypes.get(name);
                if (tokenCategory != null) {
                    //if the value exists, put it in the correct category
                    id = new CompilerTokenId(name, tokenCategory, tok);
                } else {
                    //if we don't recognize the token, consider it to a separator
                    id = new CompilerTokenId(name, "separator", tok);
                }
                //add it into the vector of tokens
                tokens.add(id);
            }
        } catch (IOException ex) {
            Exceptions.printStackTrace(ex);
        }
    }
}
