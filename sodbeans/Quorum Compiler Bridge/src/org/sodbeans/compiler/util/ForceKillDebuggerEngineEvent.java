/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sodbeans.compiler.util;

import org.sodbeans.compiler.api.CompilerEvent;

/**
 *
 * @author Andreas Stefik
 */
public class ForceKillDebuggerEngineEvent extends CompilerEvent {

    @Override
    public boolean isForceKillDebuggerEngineEvent() {
        return true;
    }
}
