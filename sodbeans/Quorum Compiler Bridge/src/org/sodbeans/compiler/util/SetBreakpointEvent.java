/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sodbeans.compiler.util;

import org.openide.filesystems.FileObject;
import org.sodbeans.compiler.api.CompilerEvent;

/**
 *
 * @author Andrew Hauck
 */
public class SetBreakpointEvent extends CompilerEvent{

    public SetBreakpointEvent(int line, int caretPos, FileObject fo)
    {
      setBreakpointToggleLine(line);
      setBreakpointToggleCarretPosition(caretPos);
      setFile(fo);
    }

    @Override
    public boolean isBreakpointToggleEvent() {
        return true;
    }
}
