/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.sodbeans.compiler.util;

import org.sodbeans.compiler.api.CompilerEvent;

/**
 * Indicates that the debugger has started.
 *
 * @author Andreas Stefik
 */
public class StartDebuggerEvent extends CompilerEvent {

    @Override
    public boolean isDebuggerStartEvent() {
        return true;
    }
}
