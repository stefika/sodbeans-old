/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.sodbeans.compiler.util;

import org.quorum.vm.interfaces.DebuggerListener;


/**
 * This class is an adaptor converting events fired in org.quorum.vm.interfaces.DebuggerListener
 * to those fired in org.sodbeans.compiler.api.DebuggerListener. This allows
 * the DebuggerListener class to be exposed to NetBeans platform applications, 
 * while allowing the existing Debugger listener interface to continue
 * to be used inside of the virtual machine.
 *
 * @author Andreas Stefik
 */
public class DebuggerListenerAdaptor implements DebuggerListener{

    private org.sodbeans.compiler.api.DebuggerListener listener = null;
    
    public void update() {
        listener.update();
    }

    /**
     * @return the listener
     */
    public org.sodbeans.compiler.api.DebuggerListener getListener() {
        return listener;
    }

    /**
     * @param listener the listener to set
     */
    public void setListener(org.sodbeans.compiler.api.DebuggerListener listener) {
        this.listener = listener;
    }
    
}
