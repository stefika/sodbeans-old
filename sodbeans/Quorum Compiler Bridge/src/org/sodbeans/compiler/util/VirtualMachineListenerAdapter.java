/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sodbeans.compiler.util;

import org.quorum.vm.interfaces.VirtualMachineEvent;
import org.quorum.vm.interfaces.VirtualMachineListener;
import org.sodbeans.compiler.api.CompilerListener;

/**
 * This class translates between the underlying Virtual Machine and the
 * support classes loaded into NetBeans, forming a barrier between the environment
 * and the VM.
 *
 * @author Andreas Stefik
 */
public class VirtualMachineListenerAdapter implements VirtualMachineListener{
    
    private CompilerListener listener;
    private VirtualMachineEventAdapater adapter = new VirtualMachineEventAdapater();

    /**
     * indicates and processes a virtual machine event and converts it into
     * a compiler event for NetBeans applications to process.
     * 
     * @param ev
     */
    public void actionPerformed(VirtualMachineEvent ev) {
        adapter.setEvent(ev);
        listener.actionPerformed(adapter);
    }

    /**
     * @return the listener
     */
    public CompilerListener getListener() {
        return listener;
    }

    /**
     * @param listener the listener to set
     */
    public void setListener(CompilerListener listener) {
        this.listener = listener;
    }
}
