/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.sodbeans.compiler.util;

import org.sodbeans.compiler.api.CompilerEvent;

/**
 * Indicates that the debugger has stopped.
 * 
 * @author Andreas Stefik
 */
public class StopDebuggerEvent extends CompilerEvent {

    @Override
    public boolean isDebuggerStopEvent() {
        return true;
    }
}
