/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.sodbeans.compiler.api;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Vector;
import java.util.prefs.PreferenceChangeEvent;
import java.util.prefs.PreferenceChangeListener;
import org.netbeans.api.actions.Savable;
import org.netbeans.api.project.Project;
import org.netbeans.api.project.ProjectInformation;
import org.netbeans.api.project.ProjectUtils;
import org.netbeans.api.project.ui.OpenProjects;
import org.openide.cookies.SaveCookie;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectNotFoundException;
import org.openide.util.Exceptions;
import org.sodbeans.compiler.api.descriptors.CompilerFileDescriptor;
import org.sodbeans.compiler.util.ForceKillDebuggerEngineEvent;
import org.sodbeans.compiler.util.StartDebuggerEvent;
import org.sodbeans.compiler.util.StopDebuggerEvent;
import org.sodbeans.compiler.util.VirtualMachineListenerAdapter;
import org.quorum.vm.interfaces.AbstractVirtualMachine;
import org.quorum.vm.interfaces.Breakpoint;
import org.quorum.vm.interfaces.VirtualMachineEvent;
import org.quorum.vm.interfaces.VirtualMachineListener;
import org.quorum.listeners.SodbeansAudioListener;
import org.sodbeans.api.quorum.MainFileProvider;
import org.sodbeans.compiler.util.DebuggerListenerAdaptor;

/**
 *  This class represents the Sodbeans compiler that is currently on the system.
 *  
 * @author Andreas Stefik
 */
public abstract class Compiler implements PreferenceChangeListener {

    /**
     * Determines whether or not the debugger is active.
     */
    protected boolean debuggerActive;
    /**
     * Whether or not a program built in Sodbeans is executing.
     */
    protected boolean running = false;
    /**
     * This is the actual virtual machine that compiles and executes computer
     * code on the system.
     */
    protected static AbstractVirtualMachine virtualMachine;

    /**
     * Files related to the project.
     */
    private FileObject[] projectFiles;
    /**
     * A set of adapters that manage the transformation from CompilerListener
     * objects to VirtualMachineListener objects. This is necessary to insulate
     * the developer from putting NetBeans platform code into the actual virtual
     * machine itself.
     */
    private HashMap<CompilerListener, VirtualMachineListenerAdapter> adapters =
            new HashMap<CompilerListener, VirtualMachineListenerAdapter>();
    
    /**
     * Another adaptor, but this time for debugger listeners.
     */
    private HashMap<DebuggerListener, DebuggerListenerAdaptor> debuggerAdaptor = 
            new HashMap<DebuggerListener, DebuggerListenerAdaptor>();
    
    /**
     * A NetBeans code to indicate project type.
     */
    private static final String PROJECT_KEY = "Quorum_Project_Keycode";
    /**
     * Determines the style of auditory cues that are sent out by the system.
     */
    private VirtualMachineListener sodbeansDefaultAudio = new SodbeansAudioListener();
    /**
     * If true, this flag indicates that the system has completely built every
     * file at least once.
     */
    private boolean builtAllOnce = false;
    /**
     * Manages all the compiler errors on the system.
     */
    protected static CompilerErrorManager compilerErrorManager;
    
    private File documenationFolder;
    
    protected boolean dirty = false;
    
    
    public Compiler() {
        createVM();
        virtualMachine.addListener(sodbeansDefaultAudio);
        virtualMachine.addListener(new VirtualMachineListener() {

            public void actionPerformed(VirtualMachineEvent event) {
                if(event.isDebuggerStopEvent()) {
                    StopDebuggerEvent stop = new StopDebuggerEvent();
                    throwEventToListeners(stop);
                }
            }
        });
    }

    protected abstract void createVM();

    /**
     * Sets the files in the user's project, which will be compiled.
     * 
     * @param files
     */
    public void setProjectFiles(FileObject[] files) {
        this.projectFiles = files;
    }

    public FileObject[] getProjectFiles() {
        return this.projectFiles;
    }
    
    public abstract CompilerErrorManager getCompilerErrorManager();

    /**
     * Returns whether the debugger is currently active in this compiler.
     * @return
     */
    public boolean isDebuggerActive() {
        return debuggerActive;
    }

    /**
     * Returns whether a program built in the compiler is currently executing.
     * 
     * @return
     */
    public boolean isExecuting() {
        return running;
    }

    /**
     * Returns a complete description of the file requested on the system.
     * @param file
     * @return
     */
    public abstract CompilerFileDescriptor getFileDescriptor(FileObject file);

    /**
     * Returns a list of all CompilerFileDescriptors on the system.
     * 
     * @return
     */
    public abstract Iterator<CompilerFileDescriptor> getFileDescriptors();

    /**
     * Attempt to compile the program.
     * 
     * @return Compiler message
     */
    public void compile(Project project) {
        Project proj = project;
        if(proj == null) {
            return;
        }

        ProjectInformation info = ProjectUtils.getInformation(proj);
        String name = info.getName();
        if (name.compareTo(PROJECT_KEY) == 0) { //it's a quorum project
            //so build it!
            MainFileProvider provider =
                    proj.getLookup().lookup(MainFileProvider.class);
            if (provider.getMainFile() == null) {
                return;
            } else {
                setMainFile(provider.getMainFile());
            }
            setProjectFiles(provider.getSourceFiles());
            actuallyDoTheCompilation();
        }
    }

    public boolean getGenerateCode() {
        return virtualMachine.isGenerateCode();
    }
    
    public void setGenerateCode(boolean gen) {
        virtualMachine.setGenerateCode(gen);
    }
    
    /**
     * Returns an XML specification for a particular class that is compiled
     * by Quorum.
     * 
     * @param staticKey a key, like Libraries.Container.Array.
     * @return Returns an XML representation of the class. If no class is found
     *                  for this particular name, this function returns null.
     */
    public abstract String getClassIndex(String staticKey);
    
    /**
     * This method returns a FileObject for which a class is contained.
     * 
     * @param staticKey
     * @return 
     */
    public abstract FileObject getContainingFile(String staticKey);
    
    /**
     * This method attempts to create documentation for the Quorum
     * project in question.
     */
    public boolean document() {
        return virtualMachine.generateDocumentation();
    }

    /**
     * This method returns information related to a method, given the static key
     * for a class (e.g., Libraries.Containers.Array, .Main for a class in 
     * the default package) and a line number. Given that information, if
     * a method at that location is found, the compiler will return the method
     * at that location, including a possible constructor. If no method or class
     * can be found, this method will return null.
     * 
     * As one caveat, as users edit a document, it may go out of sync. For each
     * correct compile, this method grabs information from the most recent cache.
     * This may cause programs that depend on it to go out of sync, a problem 
     * many debuggers (for example) have.
     * 
     * @param staticKey
     * @param line
     * @return 
     */
    public abstract CompilerLocation GetLocationAtLine(String staticKey, int line);
    
    /**
     * This method returns information to be placed in code completion, given
     * a particular request. 
     * 
     * @param request
     * @return 
     */
    public abstract CompilerCodeCompletionResults requestCodeCompletionResult(CompilerCodeCompletionRequest request);
    
    private String actuallyDoTheCompilation() {
        builtAllOnce = true; //A flag indicating whether modules are built

        Vector<File> files = new Vector<File>();
        for (int i = 0; i < projectFiles.length; i++) {
            String name = projectFiles[i].getNameExt();
            String extension = stripFileExtension(name);
            if (extension != null) {
                if (extension.compareToIgnoreCase("quorum") == 0) {
                    files.add(org.openide.filesystems.FileUtil.toFile(projectFiles[i]));
                }
            }
        }
        File[] filesArray = new File[files.size()];
        for (int i = 0; i < files.size(); i++) {
            filesArray[i] = files.elementAt(i);

            try {
                FileObject fo = FileUtil.toFileObject(filesArray[i]);
                DataObject dataObj = DataObject.find(fo);
                if (dataObj != null) {
                    SaveCookie cookie = dataObj.getLookup().lookup(SaveCookie.class);
                    if (cookie != null) {
                        cookie.save();
                    }
                }
            } catch (DataObjectNotFoundException ex) {
                Exceptions.printStackTrace(ex);
            } catch (IOException ex) {
                Exceptions.printStackTrace(ex);
            }

        }
        virtualMachine.build(filesArray);

        if (virtualMachine.getCompilerErrors().isCompilationErrorFree()) {            
            return "Build Successful. Focus to editor.";
        } else {
            return virtualMachine.getCompilerErrors().toString();
        }
    }

    protected String stripFileExtension(String name) {
        int i = name.lastIndexOf('.');

        if (i > 0 && i < name.length() - 1) {
            return name.substring(i + 1).toLowerCase();
        }

        return null;
    }

    /**
     * This method determines whether the debugger should talk.
     * 
     * @param isSpeaking 
     */
    public abstract void setAuditoryDebugging(boolean isSpeaking);
    
    /**
     * Returns true if the debugger is set to talk.
     * 
     * @return 
     */
    public abstract boolean getAuditoryDebugging();
    
    /**
     * Returns the file extension for the source files used
     * in this compiler implementation.
     * @return
     */
    public abstract String getSourceFileExtension();
    
    /**
     * Returns the current location in which build information
     * will be generated.
     * 
     * @return 
     */
    public abstract FileObject getBuildFolder();
    
    /**
     * Returns the current location in which final projects distribution
     * materials will be compiled.
     * 
     * @return 
     */
    public abstract FileObject getDistributionFolder();
    
    /**
     * Sets the location in which build information will be generated.
     * 
     * @param folder 
     */
    public abstract void setBuildFolder(FileObject folder);
    
    /**
     * Sets the location in which a final distribution of a project will
     * be placed after building.
     * 
     * @param folder 
     */
    public abstract void setDistributionFolder(FileObject folder);
    
    /**
     * Adds a raw file or folder on disk as a dependency.
     * 
     * @param file 
     */
    public abstract void addDependency(File file);
    
    /**
     * Adds a dependency with the individual file, file, and places at 
     * a path relative to the distribution and build folders. If a folder
     * is passed, an error will occur.
     * 
     * @param file 
     */
    public abstract void addDependency(File file, String relativePath);
    
    /**
     * Clears all dependencies from the compiler.
     * 
     */
    public abstract void clearDependencies();

    /***
     * Checks to see if the last build was successful
     * @return True = build successful, False = build failed
     */
    public boolean wasBuildSuccessful() {
        return virtualMachine.wasBuildSuccessful();
    }

    /**
     * Tells the compiler to start the debugger from the beginning of the
     * source code.
     * 
     */
    public void startDebugger() {
        debuggerActive = true;
        StartDebuggerEvent event = new StartDebuggerEvent();
        throwEventToListeners(event);
        //VirtualMachineEvent ev = new VirtualMachineEvent(null, this.virtualMachine, false);
        //virtualMachine.fireAnnotationEvent(ev);
    }

    /**
     * Tells the compiler to stop the debugger.
     */
    public void stopDebugger(boolean forceKill) {
        debuggerActive = false;
        running = false;
        CompilerEvent event;
        if(forceKill) {
            event = new ForceKillDebuggerEngineEvent();
        }
        else {
            event = new StopDebuggerEvent();
        }
        throwEventToListeners(event);
        virtualMachine.stop();
    }

    /**
     * Performs a "clean" on the build, effectively clearing out any
     * previous build information.
     */
    public void clean() {
        stopDebugger(false);
        debuggerActive = false;
        StopDebuggerEvent event = new StopDebuggerEvent();
        throwEventToListeners(event);
        virtualMachine.clean();
    }

    /** This method is equivalent to "stepInto into" in most debuggers"
     * 
     */
    public void stepInto() {
        virtualMachine.stepInto();
    }

    /**
     * Does a step over call in the debugger.
     */
    public void stepOver() {
        virtualMachine.stepOver();
    }

    /**
     * Executes the code until a breakpoint is hit in the source.
     */
    public void stepToBreakpoint() {
        virtualMachine.stepToBreakpoint();
    }

    /**
     * Does a step into in reverse.
     */
    public void stepBackInto() {
        virtualMachine.stepBackInto();
    }

    /**
     * Does a step over in reverse.
     */
    public void stepBackOver() {
        virtualMachine.stepBackOver();
    }

    /**
     * Backs up until the previously hit breakpoint.
     */
    public void stepBackToBreakpoint() {
        virtualMachine.unstepToBreakpoint();
    }
    
    /**
     * Returns true if the compiler is set to generate war files instead of
     * jar files.
     * 
     * @return 
     */
    public abstract boolean isGenerateWar();
    
    /**
     * Set this to true if the Quorum compiler should generate .war files instead
     * of .jar files. These files are compatible with Tomcat and glassfish 
     * Java EE containers.
     * 
     * @param value 
     */
    public abstract void setGenerateWar(boolean value);

    /**
     *
     * Toggle a breakpoint on the system.
     *
     * @param line
     * @param fo
     * @return returns whether a breakpoint was added or removed at the current
     *         location.
     */
    public boolean toggleBreakpoint(int line, FileObject fo) {
        File f = FileUtil.toFile(fo);
        String path = f.getAbsolutePath();

        Breakpoint point = new Breakpoint(path, line);

        //SetBreakpointEvent event = new SetBreakpointEvent(line, caretPos, fo);
        //throwEventToListeners(event);

        //does the VM already have a breakpoint here?
        //if so, murder it ruthlessly
        if (!virtualMachine.getBreakpointManager().isBreakpointAtPosition(point)) {
            virtualMachine.getBreakpointManager().addBreakpoint(point);
            return true;
        } else {
            virtualMachine.getBreakpointManager().removeBreakpoint(point);
            return false;
        }
    }

    /**
     * Executes the program from beginning to end (if any), without
     * stopping.
     */
    public void run() {
        if (virtualMachine.wasBuildSuccessful()) {
            running = true;
            virtualMachine.run();
        }
    }

    /**
     * Runs the program until the cursor. If the program never hits the file
     * or line that the cursor is at, it continues until either it halts
     * or crashes, if either.
     * @param file
     * @param line
     */
    public void runToCursor(FileObject file, long line) {
        String foString = org.openide.filesystems.FileUtil.toFile(file).getAbsolutePath();
        virtualMachine.runToCursor(foString, line + 1);
    }

    /**
     * Runs the program backward to the cursor. If the program never hits the 
     * file or line that the cursor is at, it continues until either it halts
     * or crashes, if either.
     * @param file
     * @param line
     */
    public void runBackToCursor(FileObject file, long line) {
        String foString = org.openide.filesystems.FileUtil.toFile(file).getAbsolutePath();
        virtualMachine.runBackToCursor(foString, line + 1);
    }
    
    /**
     * This method causes the entire system to rollback to a particular
     * point in time. The point to which it rolls back is a machine time stamp
     * (an op-code number), not a date and time. Date and time rollback
     * might be added at a later date. This function is guaranteed to halt.
     * Worst case, this function will rollback to the beginning of the
     * program.
     *
     * @param stamp
     */
    public void rollback(int stamp) {
        virtualMachine.rollBackward(stamp);
    }

    /**
     * This function rolls the execution forward to a particular point
     * in time. The point to which it rolls back is a machine time stamp
     * (an op-code number), not a date and time. Date and time rollback
     * might be added at a later date. This function is guaranteed to
     * halt. In cases like this, this is similar to never reaching a breakpoint.
     *
     * @param stamp
     */
    public void rollForward(int stamp) {
        virtualMachine.rollForward(stamp);
    }

    public boolean hasBreakPoint(FileObject file, int caretPos, int line) {
        File f = FileUtil.toFile(file);
        String path = f.getAbsolutePath();
        Breakpoint point = new Breakpoint(path, caretPos, line);
        return virtualMachine.getBreakpointManager().isBreakpointAtPosition(point);
    }

    /**
     * Adds a listener to the compiler, allowing components to listen to
     * what the compiler is doing (e.g., parsing, debugging).
     *
     * @param listener
     */
    public void addListener(CompilerListener listener) {
        VirtualMachineListenerAdapter adapt = new VirtualMachineListenerAdapter();
        adapt.setListener(listener);
        adapters.put(listener, adapt);
        virtualMachine.addListener(adapt);
    }

    public void addListener(DebuggerListener listener) {
        DebuggerListenerAdaptor adapt = new DebuggerListenerAdaptor();
        adapt.setListener(listener);
        virtualMachine.addListener(adapt);
        debuggerAdaptor.put(listener, adapt);
    }

    public void removeListener(DebuggerListener listener) {
        DebuggerListenerAdaptor adapt = new DebuggerListenerAdaptor();
        adapt.setListener(listener);
        virtualMachine.removeListener(adapt);
        if(debuggerAdaptor.containsKey(listener)) {
            debuggerAdaptor.remove(listener);
        }
    }

    /**
     * Removes a listener from the compiler's list, which stops the compiler
     * from sending it messages regarding its behavior.
     * 
     * @param listener
     */
    public void removeListener(CompilerListener listener) {
        VirtualMachineListenerAdapter adapt = adapters.remove(listener);
        virtualMachine.removeListener(adapt);
    }

    /**
     * If the debugger is being executed, this tells the debugger what line of
     * code is currently being executed.
     *
     * @return
     */
    public int getLineNumber() {
        return virtualMachine.getLineNumber();
    }

    /**
     * Returns a FileObject representing the main file that was compiled.
     * 
     * @return 
     */
    public FileObject getMainClassFile() {
        String fileKey = virtualMachine.getMain();
        if(fileKey == null) {
            return null;
        }
        
        File file = new File(fileKey);
        return FileUtil.toFileObject(file);
    }
    
    /** Returns a FileObject representing the file that is currently
     * being executed in the debugger. If the debugger is not running,
     * it returns null.
     * @return
     */
    public FileObject getCurrentFileBeingExecuted() {
        String fileKey = virtualMachine.getCurrentFileBeingExecuted();
        if(fileKey == null) {
            return null;
        }
        
        File file = new File(fileKey);
        return FileUtil.toFileObject(file);
    }

    /**
     * Returns whether the execution in the debugger (if active), is at
     * the end.
     * 
     * @return
     */
    public boolean isExecutionFinished() {
        return virtualMachine.getExecution().isExecutionFinished();
    }

    /**
     * Returns whether the execution sequence in the debugger (if active)
     * is at the beginning.
     * 
     * @return
     */
    public boolean isExecutionAtBeginning() {
        return virtualMachine.getExecution().isExecutionAtBeginning();
    }

    /**
     * Throws an event to all items currently listening to events thrown
     * by the compiler.
     * 
     * @param event
     */
    public void throwEventToListeners(CompilerEvent event) {
        try {
            Iterator<CompilerListener> it = adapters.keySet().iterator();
            while (it.hasNext()) {
                CompilerListener listener = it.next();
                listener.actionPerformed(event);
            }
        } catch (Exception exception) {//just ignore errors that any listeners might have.
        }
    }

    /**
     * Tells the compiler that a preference has changed on the system.
     * 
     * @param evt
     */
    public void preferenceChange(PreferenceChangeEvent evt) {

        // Cue Mode SODBeans
        if (evt.getKey().equalsIgnoreCase("cueModeSodBeans")) {
            // Cue Mode SODBeans has just been set to true
            if (evt.getNewValue().equalsIgnoreCase("true")) {
                virtualMachine.addListener(sodbeansDefaultAudio);
            } // Cue Mode SODBeans has just been set to false
            else {
                virtualMachine.removeListener(sodbeansDefaultAudio);
            }
        } // Cue Mode Visual Studio
        else if (evt.getKey().equalsIgnoreCase("cueModeVisualStudios")) {
            // Cue Mode Visual Studio has just been set to true
            if (evt.getNewValue().equalsIgnoreCase("true")) {
                virtualMachine.addListener(sodbeansDefaultAudio);
            } // Cue Mode Visual Studio has just been set to false
            else {
                virtualMachine.removeListener(sodbeansDefaultAudio);
            }
        }
    }

    /**
     * @return the builtOnce
     */
    public boolean hasBuiltAllOnce() {
        return builtAllOnce;
    }

    /**
     * Sets the main file that will be executed when the compiler builds
     * and links the final project.
     * @param mainFile
     */
    private void setMainFile(FileObject mainFile) {
        String name = org.openide.filesystems.FileUtil.toFile(mainFile).getAbsolutePath();
        virtualMachine.setMain(name);
    }

    /**
     * @return the dataStorage
     */
    public abstract CompilerDataStorage getDataStorage();

    /**
     * @return the documenationFolder
     */
    public File getDocumenationFolder() {
        return documenationFolder;
    }

    /**
     * @param documenationFolder the documenationFolder to set
     */
    public void setDocumenationFolder(File documenationFolder) {
        this.documenationFolder = documenationFolder;
        this.virtualMachine.setDocumentationPath(documenationFolder.getAbsolutePath() + "/");
    }
    
    /**
     * Sets the folder where the servlet folder is placed.
     * 
     * @return 
     */
    public abstract FileObject getServletFolder();
    
    /**
     * Sets the location of the servlet folder.
     * 
     * @param file 
     */
    public abstract void setServletFolder(FileObject file);
}
