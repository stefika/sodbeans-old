/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sodbeans.compiler.api;

import org.netbeans.api.lexer.Language;
import org.sodbeans.compiler.quorum.impl.QuorumLanguageHierarchy;

/**
 * This class allows for a set of static getter methods for returning various
 * lexers and parsers supported by Sodbeans.
 * 
 * @author Andreas Stefik
 */
public class LanguageChoices {
    private static final Language<CompilerTokenId> language = new QuorumLanguageHierarchy().language();

    /**
     * Returns a representation of the Quorum programming language.
     * 
     * @return
     */
    public static final Language<CompilerTokenId> getQuorumLanguage() {
        return language;
    }
}
