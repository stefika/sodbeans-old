/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.sodbeans.compiler.api;

/**
 *
 * @author Andreas Stefik
 */
public class CompilerCodeCompletionResult implements Comparable{
    private String documentation = "";
    private String completion = "";
    private String displayName = "";
    private String typeName = "";
    private CompilerCodeCompletionType type;
    private boolean isBaseClassMethod = false;
    
    /**
     * @return the isBaseClassMethod
     */
    public boolean isBaseClassMethod() {
        return isBaseClassMethod;
    }

    /**
     * @param isBaseClassMethod the isBaseClassMethod to set
     */
    public void setIsBaseClassMethod(boolean isBaseClassMethod) {
        this.isBaseClassMethod = isBaseClassMethod;
    }
    
    /**
     * @return the documentation
     */
    public String getDocumentation() {
        return documentation;
    }

    /**
     * @param documentation the documentation to set
     */
    public void setDocumentation(String documentation) {
        this.documentation = documentation;
    }

    /**
     * @return the completion
     */
    public String getCompletion() {
        return completion;
    }

    /**
     * @param completion the completion to set
     */
    public void setCompletion(String completion) {
        this.completion = completion;
    }

    /**
     * @return the displayName
     */
    public String getDisplayName() {
        return displayName;
    }

    /**
     * @param displayName the displayName to set
     */
    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }
    
    /**
     * @return the displayName
     */
    public String getDisplayType() {
        return typeName;
    }

    /**
     * @param displayName the displayName to set
     */
    public void setDisplayType(String typeName) {
        this.typeName = typeName;
    }

    public int compareTo(Object o) {
        if(o instanceof CompilerCodeCompletionResult) {
            CompilerCodeCompletionResult right = (CompilerCodeCompletionResult) o;
            return this.getDisplayName().compareTo(right.getDisplayName());
        } 
        
        return -1;
    }

    /**
     * @return the type
     */
    public CompilerCodeCompletionType getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(CompilerCodeCompletionType type) {
        this.type = type;
    }
    
}
