/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sodbeans.compiler.api;

import org.netbeans.spi.lexer.Lexer;

/**
 * The Compiler lexer class represents an adaptor between the NetBeans platform
 * and the sodbeans lexical architecture. This architecture is not lexer
 * dependent, meaning it does not rely on a particular architecture (e.g., JavaCC, ANTLR)
 * for doing its lexical analysis.
 *
 * @author Andreas Stefik
 */
public interface CompilerLexer extends Lexer<CompilerTokenId>{

}
