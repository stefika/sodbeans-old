/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sodbeans.compiler.api;

import org.openide.filesystems.FileObject;

/**
 *
 * @author Andreas Stefik
 */
public interface CompilerCodeMatcher{
    public int[] findOrigin(FileObject obj, int cursorOffset);
    public int[] findMatch(FileObject obj, int matchID, boolean backward);
}
