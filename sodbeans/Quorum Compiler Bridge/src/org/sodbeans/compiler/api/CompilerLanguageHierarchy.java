/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sodbeans.compiler.api;

import org.netbeans.spi.lexer.LanguageHierarchy;

/**
 * The CompilerLanguageHierarchy represents an adaptor between
 * the sodbeans virtual machine and the NetBeans platform.
 * 
 * @author Andreas Stefik
 */
public abstract class CompilerLanguageHierarchy extends LanguageHierarchy<CompilerTokenId>{

}
