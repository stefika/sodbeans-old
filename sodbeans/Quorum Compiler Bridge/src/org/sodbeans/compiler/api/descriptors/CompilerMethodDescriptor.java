/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sodbeans.compiler.api.descriptors;

import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;

/**
 *  This class represents a method in the compiler.
 *
 * @author Neelima Samsani and Andreas Stefik
 */
public abstract class CompilerMethodDescriptor implements CompilerDescriptor{

    /**
     * Returns a node object that represents the static representation of the
     * method on the system.
     * 
     * @return
     */
    public AbstractNode getRootNode() {
        CompilerNode node = new CompilerNode(Children.LEAF);
        node.setDescriptor(this);
        node.setDisplayName(this.getName());
        node.setShortDescription("Action");
        node.setIconBaseWithExtension("org/sodbeans/resources/methodicon.png");
        return node;
    }
}
