/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.sodbeans.compiler.api.descriptors;

import java.lang.reflect.InvocationTargetException;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.nodes.PropertySupport;
import org.openide.nodes.Sheet;
import org.quorum.vm.interfaces.CompilerError;
import org.quorum.vm.interfaces.ErrorType;

/**
 * A Node object that represents compiler errors in the debugger.
 * Specifically, this node object represents all of the possible
 * properties that can be displayed in the output window for
 * compiler errors.
 * 
 * @author Melissa Stefik
 */
public class CompilerErrorDescriptorNode extends AbstractNode implements CompilerErrorDescriptor{

    private CompilerError error;

    /**
     * Creates a Compiler error node to display an error.
     *
     * @param children
     */
    public CompilerErrorDescriptorNode(Children children) {
        super(children);
    }

    /**
     * Set the compiler error in the quorum compiler error descriptor
     * @param error
     */
    public void setCompilerError(CompilerError error) {
        this.error = error;
    }

    @Override
    public Node getRootNode(){
        this.setIconBaseWithExtension("org/sodbeans/resources/compilererror.png");
        this.setShortDescription("Error");
        this.setDisplayName(getDescription());
        return this;
    }

    @Override
    public int getLine() {
        return error.getLineNumber();
    }

    @Override
    public String getDescription() {
        return error.getError();
    }

    @Override
    public String getFileName() {
       return error.getFile();
    }

    @Override
    public String getAbsolutePath() {
        return error.getAbsolutePath();
    }

    @Override
    protected Sheet createSheet() {
        Sheet sheet = Sheet.createDefault();
        Sheet.Set set = sheet.get(Sheet.PROPERTIES);
        if (set == null) {
            set = Sheet.createPropertiesSet();
            sheet.put(set);
        }

        set.put(new LineNumberProperty(this));
        set.put(new LocationProperty(this));
        set.put(new PathProperty(this));

        sheet.put(set);
        return sheet;
    }

    public String getErrorType() {
        return error.getErrorType().toString();
    }

    /**
     * A Node property representing values of compiler errors on the system.
     */
    public class LocationProperty extends PropertySupport.ReadWrite<String> {

        private final CompilerErrorDescriptorNode node;

        public LocationProperty(CompilerErrorDescriptorNode node) {
            super("Location", String.class, "File", "Error Location Value");
            this.node = node;
        }

        @Override
        public String getValue() throws IllegalAccessException, InvocationTargetException {
            return error.getFile();
        }

        @Override
        public void setValue(String arg0) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
            throw new UnsupportedOperationException("Not supported yet.");
        }
    }

    /**
     * A Node property representing values of compiler erros on the system.
     */
    public class PathProperty extends PropertySupport.ReadWrite<String> {

        private final CompilerErrorDescriptorNode node;

        public PathProperty(CompilerErrorDescriptorNode node) {
            super("Path", String.class, "Path", "Path Value");
            this.node = node;
        }

        @Override
        public String getValue() throws IllegalAccessException, InvocationTargetException {
            return error.getAbsolutePath();
        }

        @Override
        public void setValue(String arg0) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
            throw new UnsupportedOperationException("Not supported yet.");
        }
    }

    /**
     * A Node property representing values of compiler errors on the system.
     */
    public class LineNumberProperty extends PropertySupport.ReadWrite<String> {

        private final CompilerErrorDescriptorNode node;

        public LineNumberProperty(CompilerErrorDescriptorNode node) {
            super("Line", String.class, "Line", "Line Number Value");
            this.node = node;
        }

        @Override
        public String getValue() throws IllegalAccessException, InvocationTargetException {
            return error.getLineNumber() + "";
        }

        @Override
        public void setValue(String arg0) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
            throw new UnsupportedOperationException("Not supported yet.");
        }
    }
}
