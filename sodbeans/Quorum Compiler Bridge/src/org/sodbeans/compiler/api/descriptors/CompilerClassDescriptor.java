/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sodbeans.compiler.api.descriptors;

import java.util.Iterator;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;

/**
 * A class that represents a class object represented within the compiler. From
 * this object, methods and other attributes can be retrieved from anywhere
 * on the system.
 * 
 * @author Andreas Stefik
 */
public abstract class CompilerClassDescriptor extends Children.Keys<CompilerMethodDescriptor> implements CompilerDescriptor{
    /**
     * Returns the name of the class.
     * 
     * @return
     */
    public abstract String getName();
    
    /**
     * Returns the static key for the class.
     * 
     * @return 
     */
    public abstract String getStaticKey();
    
    /**
     * Returns a method in the class of a certain name. TODO: Fix this 
     * for methods with other signatures (e.g., parameters).
     * 
     * @param name
     * @return
     */
    public abstract CompilerMethodDescriptor getMethod(String name);

    /**
     * Returns all methods currently in this class.
     * @return
     */
    public abstract Iterator<CompilerMethodDescriptor> getMethods();

    /**
     * Returns a node object that can be displayed on the system, which
     * contains appropriate subnodes for all classes.
     *
     * @return
     */
    public AbstractNode getRootNode() {
        CompilerNode node = new CompilerNode(this);
        node.setDescriptor(this);
        node.setDisplayName(this.getName());
        node.setShortDescription("Class");
        node.setIconBaseWithExtension("org/sodbeans/resources/Class.png");
        return node;
    }
}

    
