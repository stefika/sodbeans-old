/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sodbeans.compiler.api.descriptors;

import java.io.File;
import java.util.Iterator;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;


/**
 * This class represents a source code file in the user's project. While
 * it corresponds to a physical file on disk, it is annotated with symbolic
 * information on what is in the file (e.g., classes, methods).
 *
 * @author Andreas Stefik
 */
public abstract class CompilerFileDescriptor extends Children.Keys<CompilerClassDescriptor> {
    /**
     * Returns a class of a specific name.
     * @param name
     * @return
     */
    public abstract CompilerClassDescriptor getClass(String name);

    /**
     * Returns all classes defined in this file.
     * @return
     */
    public abstract Iterator<CompilerClassDescriptor> getClasses();

    /**
     * Returns the name of this file.
     * @return
     */
    public abstract String getName();

    /**
     * Returns a node object that represents everything that is defined
     * in this file. This node represents only static analysis, not runtime
     * information that is in the debugger.
     * @return
     */
    public AbstractNode getRootNode() {
        AbstractNode node = new AbstractNode(this);
        node.setDisplayName(this.getName());
        return node;
    }
    
    public abstract File getFile();
}



