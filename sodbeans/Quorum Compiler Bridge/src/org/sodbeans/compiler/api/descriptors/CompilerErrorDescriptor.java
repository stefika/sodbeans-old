/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sodbeans.compiler.api.descriptors;

import org.openide.nodes.Node;

/**
 * This descriptor represents the information a compiler error on
 * the system will have.
 *
 * @author Melissa Stefik
 */
public interface CompilerErrorDescriptor {

    public Node getRootNode();
    
    /**
     * Returns the line number the compiler error starts at.
     * @return
     */
    public int getLine();

    /**
     * Returns the description of the compiler error.
     * @return
     */
    public String getDescription();

    /**
     * Returns the name of the file containing the compiler error.
     * @return
     */
    public String getFileName();

    /**
     * Returns the file path which contains the compiler error.
     * @return
     */
    public String getAbsolutePath();

    public String getErrorType();

}
