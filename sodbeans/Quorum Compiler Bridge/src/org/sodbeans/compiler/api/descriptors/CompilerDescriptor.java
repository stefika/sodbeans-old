/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sodbeans.compiler.api.descriptors;

import org.sodbeans.controller.api.ScreenReaderInformation;

/**
 * This descriptor represents generic information any item on the system
 * will have.
 * 
 * @author Andreas Stefik
 */

public abstract interface CompilerDescriptor{
    /**
     * Returns the line number where the item is first defined.
     * @return
     */
    public int getLine();
    /**
     * Returns the column number where this item is first defined.
     * @return
     */
    public int getColumn();
    /**
     * Returns the last line on which this item is defined.
     * @return
     */
    public int getEndLine();
    /**
     * Returns the last column number on which this item is defined.
     * @return
     */
    public int getEndColumn();

    /**
     * Returns the name of the item.
     * @return
     */
    public abstract String getName();

    public ScreenReaderInformation getScreenReaderInformation();
}
