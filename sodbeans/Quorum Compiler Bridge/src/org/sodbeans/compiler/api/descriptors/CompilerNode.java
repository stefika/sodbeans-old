/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sodbeans.compiler.api.descriptors;

import org.openide.nodes.Children;
import org.sodbeans.controller.api.AccessibleNode;
import org.sodbeans.controller.api.ScreenReaderInformation;

/**
 * Represents a general Node object that NetBeans can use to
 * output compiler information to the user.
 * 
 * @author Neelima Samsani and Andreas Stefik
 */
public class CompilerNode extends AccessibleNode{
    
    /**
     * A descriptor object with general information regarding an object.
     */
    private CompilerDescriptor descriptor;

    public CompilerNode(Children children){
        super(children);
    }

    /**
     * Returns a CompilerDescriptor object.
     * 
     * @return the descriptor
     */
    public CompilerDescriptor getDescriptor() {
        return descriptor;
    }

    /**
     * Sets a CompilerDescriptor object.
     * @param descriptor the descriptor to set
     */
    public void setDescriptor(CompilerDescriptor descriptor) {
        this.descriptor = descriptor;
    }

    @Override
    public ScreenReaderInformation getScreenReaderInformation() {
        return getDescriptor().getScreenReaderInformation();
    }
}
    
