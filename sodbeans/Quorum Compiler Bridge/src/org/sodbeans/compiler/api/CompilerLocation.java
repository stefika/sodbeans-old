/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.sodbeans.compiler.api;

import java.util.ArrayList;

/**
 *
 * @author stefika
 */
public class CompilerLocation {
    private String name = "";
    
    private ArrayList<CompilerParameter> parameters = new ArrayList<CompilerParameter>();
    
    public void add(CompilerParameter param) {
        parameters.add(param);
    }
    
    public CompilerParameter getParameter(int i) {
        return parameters.get(i);
    }
    
    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public void add(String name, String type) {
        CompilerParameter parameter = new CompilerParameter();
        parameter.setName(name);
        parameter.setType(type);
        parameters.add(parameter);
    }
    
    public int getNumParameters() {
        return parameters.size();
    }
}
