/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sodbeans.compiler.api;

import org.netbeans.spi.viewmodel.NodeModel;
import org.netbeans.spi.viewmodel.TableModel;
import org.netbeans.spi.viewmodel.TreeModel;

/**
 *
 * @author Andreas Stefik
 */
public interface CompilerDataStorage extends TreeModel, NodeModel, TableModel{
}
