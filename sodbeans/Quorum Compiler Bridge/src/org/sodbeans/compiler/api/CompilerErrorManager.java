/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sodbeans.compiler.api;

import org.openide.nodes.Children;
import org.sodbeans.compiler.api.descriptors.CompilerErrorDescriptor;
import java.util.*;
import org.openide.nodes.Node;

/**
 *
 * @author Melissa Stefik
 */
public abstract class CompilerErrorManager extends Children.Keys<CompilerErrorDescriptor>{

    /**
     * Gets the root node for compiler errors.
     * @return
     */
    public abstract Node getRootCompilerErrorNode();
    /**
     * If this is empty there are no compiler errors.
     * 
     * @return
     */
    public abstract boolean isEmpty();

    /**
     * Get the number of syntax errors.
     * @return
     */
    public abstract int getNumberOfSyntaxErrors();

    /**
     * Returns all of the compiler errors that are present.
     * @return
     */
    public abstract Iterator<CompilerErrorDescriptor> iterator();

}
