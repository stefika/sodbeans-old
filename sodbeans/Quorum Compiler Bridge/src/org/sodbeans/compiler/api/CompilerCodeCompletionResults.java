/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.sodbeans.compiler.api;

import java.util.Set;

/**
 *
 * @author Andreas Stefik
 */
public interface CompilerCodeCompletionResults {
    /**
     * @return the results
     */
    public Set<CompilerCodeCompletionResult> getResults();

    /**
     * @return the filter
     */
    public String getFilter();
}
