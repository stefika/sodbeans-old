/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.sodbeans.compiler.api;

import java.awt.Color;
import javax.swing.ImageIcon;
import org.openide.util.ImageUtilities;

/**
 *
 * @author astefik
 */
public enum CompilerCodeCompletionType {
    
    
    LOCAL_VARIABLE ("org/sodbeans/completion/api/resources/variablePublic.png", Color.decode("0x009900"), 20), 
    PARAMETER ("org/sodbeans/completion/api/resources/variablePublic.png", Color.decode("0x009900"), 20),
    
    PRIVATE_FIELD_VARIABLE("org/sodbeans/completion/api/resources/fieldPrivate.png", Color.decode("0x0000B2"), 30), 
    PUBLIC_FIELD_VARIABLE ("org/sodbeans/completion/api/resources/fieldPublic.png", Color.decode("0x0000B2"), 30),
    
    PRIVATE_ACTION ("org/sodbeans/completion/api/resources/methodPrivate.png", Color.decode("0x000000"), 50), 
    PUBLIC_ACTION ("org/sodbeans/completion/api/resources/methodPublic.png", Color.decode("0x000000"), 50),
    
    PRIVATE_SYSTEM_ACTION ("org/sodbeans/completion/api/resources/methodPrivate.png", Color.decode("0x000000"), 50), 
    PUBLIC_SYSTEM_ACTION ("org/sodbeans/completion/api/resources/methodPublic.png", Color.decode("0x000000"), 50),
    
    PRIVATE_BLUEPRINT_ACTION ("org/sodbeans/completion/api/resources/methodPrivate.png", Color.decode("0x000000"), 50),
    PUBLIC_BLUEPRINT_ACTION ("org/sodbeans/completion/api/resources/methodPublic.png", Color.decode("0x000000"), 50),
    
    CLASS ("org/sodbeans/completion/api/resources/class.png", Color.decode("0x701107"), 75),
    PRIMITIVE ("org/sodbeans/completion/api/resources/quorum.png", Color.decode("0x0E39E7"), 90),
    CONTROL_STRUCTURE ("org/sodbeans/completion/api/resources/quorum.png", Color.decode("0x0E39E7"), 90),
    PACKAGE ("org/sodbeans/completion/api/resources/package.png", Color.decode("0x076912"), 100);
    ImageIcon icon;
    Color color;
    int priority;
    
    CompilerCodeCompletionType(String imageLocation, Color c, int p) {
        icon = new ImageIcon(ImageUtilities.loadImage(imageLocation));
        color = c;
        priority = p;
    }
    public ImageIcon getImage() {
        return icon;
    }
    
    public Color getColor() {
        return color;
    }
    
    public int getPriority() {
        return priority;
    }
}
