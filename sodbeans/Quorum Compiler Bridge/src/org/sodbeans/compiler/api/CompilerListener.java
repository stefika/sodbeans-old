/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sodbeans.compiler.api;


/**
 * By implementing this listener and sending a copy to the compiler,
 * you receive a copy of all events the virtual machine fires (e.g.,
 * build events, debugger events).
 * @author Andreas Stefik
 */
public interface CompilerListener {

    /**
     * Passes a CompilerEventObject fired by the virtual machine.
     * 
     * @param event
     */
    public void actionPerformed(CompilerEvent event);
}
