/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sodbeans.compiler.api;

import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.quorum.execution.DataObject;

/**
 * This class represents a variable in the compiler.
 *
 * @author Neelima Samsani and Andreas Stefik
 */
public abstract class CompilerVariable extends Children.Keys<CompilerVariable>
    implements Comparable<CompilerVariable>{
    protected boolean topMost = false;

    /**
     * Returns the variables name.
     * @return
     */
    public abstract String getName();

    /**
     * Returns a node used by netbeans for displaying the variable.
     * @return
     */
    public abstract AbstractNode getRootNode();

    /**
     * Returns a string representing the value of a given variable.
     * @return
     */
    public abstract String getVarValue();

    /**
     * Returns a string representing the type of the variable.
     * @return
     */
    public abstract String getVarType();


    /**
     * This returns the exact op-code number in which this
     * particular variable was set while the machine was executing.
     * @return
     */
    public abstract String getMachineTimeStamp();

    /**
     * This returns the actual date and time that this variable was set.
     * 
     * @return
     */
    public abstract String getTimeStamp();

    /**
     * Returns whether this variable is a top most variable, meaning
     * that in a local or watch window it is flush left on the system (not already
     * a history node, for example.).
     * @return
     */
    public boolean isTopMost() {
        return topMost;
    }

    /**
     * Sets the object that represents what is displayed to the user.
     * @param dataObject
     */
    public abstract void setDataObject(DataObject dataObject);

    /**
     * Gets a data object representing what is displayed to the user.
     * @return
     */
    public abstract DataObject getDataObject();

    /**
     * Compares one compiler variable to another.
     *
     * @param hcVar
     * @return
     */
    public int compareTo(CompilerVariable var) {
        if((this.getName()).compareTo(var.getName())< 0)
        {
            return -1;
        }
        else if((this.getName()).compareTo(var.getName())> 0)
        {
            return 1;
        }
        else
            return 0;
    }
}
