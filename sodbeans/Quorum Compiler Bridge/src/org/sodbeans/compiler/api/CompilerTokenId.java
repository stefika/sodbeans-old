/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.sodbeans.compiler.api;

import org.netbeans.api.lexer.Language;
import org.netbeans.api.lexer.TokenId;
import org.sodbeans.compiler.quorum.impl.QuorumLanguageHierarchy;

/**
 * This class represents a token for the Quorum programming language. Its name
 * is an artifact of legacy code.
 * 
 * @author Andreas Stefik
 */
public class CompilerTokenId implements TokenId {
    public static final String ACTION = "ACTION";
    public static final String ALERT = "ALERT";
    public static final String ALWAYS = "ALWAYS";
    public static final String AND = "AND";
    public static final String BLUEPRINT = "BLUEPRINT";
    public static final String BOOLEAN = "BOOLEAN";
    public static final String BOOLEAN_KEYWORD = "BOOLEAN_KEYWORD";
    public static final String CAST = "CAST";
    public static final String CAST_EXPRESSION = "CAST_EXPRESSION";
    public static final String CHECK = "CHECK";
    public static final String CLASS = "CLASS";
    public static final String COLON = "COLON";
    public static final String COMMA = "COMMA";
    public static final String COMMENTS = "COMMENTS";
    public static final String CONSTANT = "CONSTANT";
    public static final String CONSTRUCTOR = "CONSTRUCTOR";
    public static final String CREATE = "CREATE";
    public static final String DECIMAL = "DECIMAL";
    public static final String DESTROY = "DESTROY";
    public static final String DETECT = "DETECT";
    public static final String DIVIDE = "DIVIDE";
    public static final String DOUBLE_QUOTE = "DOUBLE_QUOTE";
    public static final String ELSE = "ELSE";
    public static final String ELSE_IF = "ELSE_IF";
    public static final String ELSE_IF_STATEMENT = "ELSE_IF_STATEMENT";
    public static final String END = "END";
    public static final String EQUALITY = "EQUALITY";
    public static final String EXPRESSION_STATEMENT = "EXPRESSION_STATEMENT";
    public static final String FINAL_ELSE = "FINAL_ELSE";
    public static final String FPARAM = "FPARAM";
    public static final String FUNCTION_CALL = "FUNCTION_CALL";
    public static final String FUNCTION_CALL_PARENT = "FUNCTION_CALL_PARENT";
    public static final String FUNCTION_CALL_THIS = "FUNCTION_CALL_THIS";
    public static final String FUNCTION_EXPRESSION_LIST = "FUNCTION_EXPRESSION_LIST";
    public static final String GENERIC = "GENERIC";
    public static final String GREATER = "GREATER";
    public static final String GREATER_EQUAL = "GREATER_EQUAL";
    public static final String ID = "ID";
    public static final String IF = "IF";
    public static final String INHERITS = "INHERITS";
    public static final String INPUT = "INPUT";
    public static final String INPUT_EXPRESSION = "INPUT_EXPRESSION";
    public static final String INT = "INT";
    public static final String INTEGER_KEYWORD = "INTEGER_KEYWORD";
    public static final String LEFT_PAREN = "LEFT_PAREN";
    public static final String LEFT_SQR_BRACE = "LEFT_SQR_BRACE";
    public static final String LESS = "LESS";
    public static final String LESS_EQUAL = "LESS_EQUAL";
    public static final String ME = "ME";
    public static final String MINUS = "MINUS";
    public static final String MODULO = "MODULO";
    public static final String MULTIPLY = "MULTIPLY";
    public static final String NATIVE = "NATIVE";
    public static final String NEWLINE = "NEWLINE";
    public static final String NOT = "NOT";
    public static final String NOTEQUALS = "NOTEQUALS";
    public static final String NOW = "NOW";
    public static final String NULL = "NULL";
    public static final String NUMBER_KEYWORD = "NUMBER_KEYWORD";
    public static final String ON = "ON";
    public static final String OR = "OR";
    public static final String OUTPUT = "OUTPUT";
    public static final String OVER = "OVER";
    public static final String PACKAGE_NAME = "PACKAGE_NAME";
    public static final String PARENT = "PARENT";
    public static final String PAREN_EXPRESSION = "PAREN_EXPRESSION";
    public static final String PAREN_WRAPPED_EXPRESSION = "PAREN_WRAPPED_EXPRESSION";
    public static final String PERIOD = "PERIOD";
    public static final String PLUS = "PLUS";
    public static final String PRIVATE = "PRIVATE";
    public static final String PUBLIC = "PUBLIC";
    public static final String QUALIFIED_NAME = "QUALIFIED_NAME";
    public static final String QUALIFIED_SOLO_EXPRESSION = "QUALIFIED_SOLO_EXPRESSION";
    public static final String QUALIFIED_SOLO_EXPRESSION_SELECTOR = "QUALIFIED_SOLO_EXPRESSION_SELECTOR";
    public static final String QUALIFIED_SOLO_PARENT_EXPRESSON = "QUALIFIED_SOLO_PARENT_EXPRESSON";
    public static final String REPEAT = "REPEAT";
    public static final String RETURN = "RETURN";
    public static final String RETURNS = "RETURNS";
    public static final String RIGHT_PAREN = "RIGHT_PAREN";
    public static final String RIGHT_SQR_BRACE = "RIGHT_SQR_BRACE";
    public static final String ROOT_EXPRESSION = "ROOT_EXPRESSION";
    public static final String SAY = "SAY";
    public static final String SOLO_FUNCTION_CALL = "SOLO_FUNCTION_CALL";
    public static final String SOLO_FUNCTION_CALL_PARENT = "SOLO_FUNCTION_CALL_PARENT";
    public static final String SOLO_FUNCTION_CALL_THIS = "SOLO_FUNCTION_CALL_THIS";
    public static final String STATEMENT_LIST = "STATEMENT_LIST";
    public static final String STRING = "STRING";
    public static final String TEXT = "TEXT";
    public static final String TIMES = "TIMES";
    public static final String UNARY_NOT = "UNARY_NOT";
    public static final String UNTIL = "UNTIL";
    public static final String USE = "USE";
    public static final String WHILE = "WHILE";
    public static final String WS = "WS";

    private final String name;
    private final String primaryCategory;
    private final int id;

    public CompilerTokenId(
            String name,
            String primaryCategory,
            int id) {
        this.name = name;
        this.primaryCategory = primaryCategory;
        this.id = id;
    }

    public static Language<CompilerTokenId> getLanguage() {
        return new QuorumLanguageHierarchy().language();
    }
    
    @Override
    public String primaryCategory() {
        return primaryCategory;
    }

    @Override
    public int ordinal() {
        return id;
    }

    @Override
    public String name() {
        return name;
    }
}
