/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sodbeans.compiler.api;

/**
 * Implement this interface if you want to receive events from the compiler
 * when heavyweight debugger functions have finished executing.
 * 
 * @author Andreas Stefik
 */
public interface DebuggerListener {
    public void update();
}
