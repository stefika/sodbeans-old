As Sodbeans is a research instrument created by Andreas Stefik and
others from Southern Illinois University Edwardsville, it is used in
part to track data on how individuals use the environment. By default,
when Sodbeans loads for the first time, it begins tracking user data,
especially in projects that use the Quorum programming language. For
example, Sodbeans automatically tracks whether it internally throws an
error or crashes, which windows and features are used and how often,
and the types of errors that users make when using Quorum.

By agreeing to this license, you are allowing Sodbeans to initially
track data on the usage of Sodbeans. If you prefer that Sodbeans does
not track such data, you have two options: 1) do not install Sodbeans,
or 2) in the preferences window of Sodbeans, turn off data collection
by deselecting, under the tab labeled "General," the check box
related to anonymous usage data.

Additionally, if you prefer to have your data associated with your email, which would allow us to better understand those people using Sodbeans, members of the public may optionally
login to Sodbeans. Members of the public are not required to login to use Sodbeans and your private information will never be intentionally shared with a third party.