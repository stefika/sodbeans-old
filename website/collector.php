<?php include("include/header.html"); ?>
<?php include("include/menu_eng.html"); ?>
<script type="text/javascript">
    document.title += ' Get Help with Sodbeans';
    document.getElementById("help_button").className = "HereButton";
</script>

<h1 class="nomargin">UI Gesture Collector</h1>
<p align="justify">Welcome to UI Gestures Collector</p>
<p align="justify">You can now submit data about the UI actions you performed.</p>
<form action="http://localhost:8888/analytics/upload.jsp" method="post">
  <input name="submit" value="&Submit Data" type="hidden">
  <input name="exit" value="&Cancel" type="hidden">
</form>
<?php include("include/footer.html"); ?>
