<?php include("include/header.html"); ?>
<?php include("include/menu_eng.html"); ?>
<script type="text/javascript">
    document.title += ' The Sodbeans Community';
    document.getElementById("community_button").className = "HereButton";
</script>

<h1 class="nomargin">Community</h1>

<p align="justify">To help keep the team organized, we have setup a number of support utilities
on <a href="https://sourceforge.net/">Source Forge</a>:

<UL>
    <LI><a href = "http://sourceforge.net/apps/trac/sodbeans/wiki">Getting Help</a>:
    The most complete and up to date source for getting help with Sodbeans is done our
    community wiki, called Trac.
    </LI>
    <LI><a href = "https://lists.sourceforge.net/lists/listinfo/sodbeans-developer">Mailing List</a>:
    Join the mailing list if you want to get in touch with the development team about
    using Sodbeans.
    </LI>
    <LI><a href = "https://sourceforge.net/forum/?group_id=256336">Forums</a>: The forums are
    another way of getting in touch with the development team.
    </LI>
    <LI><a href = "http://sourceforge.net/apps/trac/sodbeans/report">Bug Tracker</a>:
    Have a problem with Sodbeans? Post it in the bug tracker or check to see if we
    already know about it.
    </LI>
    <LI><a href = "http://sourceforge.net/apps/trac/sodbeans/timeline">Daily Progress</a>:
    You can also track our daily progress and activities.
    </LI>
    <LI><a href = "http://sourceforge.net/apps/trac/sodbeans/roadmap">See the project goals and roadmap</a>:
    We also post our project goals and roadmap online, including how far we are along toward
    the next release.
    </LI>
</UL>
</p>
<?php include("include/footer.html"); ?>