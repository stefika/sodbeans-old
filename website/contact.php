<?php include("include/header.html"); ?>
<?php include("include/menu_eng.html"); ?>
<script type="text/javascript">
    document.title += ' Contact the Developers';
    document.getElementById("contact_button").className = "HereButton";
</script>
<h1 class="nomargin">Contact Us</h1>
<p>For more information on getting in touch with the Sodbeans development
team or to learn more about the Sodbeans project, please join the Sodbeans
<a href = "https://lists.sourceforge.net/lists/listinfo/sodbeans-developer">Mailing List</a>
(preferred), or contact: <br /><br />
Dr. Andreas Stefik. <br />
<strong>Email:</strong> stefika "at" gmail "dot" com<br />
<strong>Office Phone:</strong> 618-692-7393</p>

<p>Go to our community wiki to see a list of the <a href="http://sourceforge.net/apps/trac/sodbeans/wiki/ActiveTeamMembers">contributors to the Sodbeans project</a>.</p>

<?php include("include/footer.html"); ?>