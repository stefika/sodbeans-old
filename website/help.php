<?php include("include/header.html"); ?>
<?php include("include/menu_eng.html"); ?>
<script type="text/javascript">
    document.title += ' Get Help with Sodbeans';
    document.getElementById("help_button").className = "HereButton";
</script>

<h1 class="nomargin">Help</h1>

<p align="justify">The best place to get help with Sodbeans is to go to our
    <a href="http://sourceforge.net/apps/trac/sodbeans/wiki">community run wiki page</a>, hosted by
    <a href="https://sourceforge.net">Sourceforge</a>. We store all of the latest documentation and tutorials related to
    Sodbeans and the Quorum Programming Language there.
</p>
<?php include("include/footer.html"); ?>
