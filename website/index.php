<?php include("include/header.html"); ?>
<?php include("include/menu_eng.html"); ?>
<script type="text/javascript">
    document.title += ' Welcome to the Sodbeans Website';
    document.getElementById("home_button").className = "HereButton";
</script>
<div style="width:968px; height:600px;">
    <h1 class="nomargin">What is Sodbeans?</h1>

    <p align = "justify"> Sodbeans is a NetBeans module suite designed to enhance
    accessibility for the blind in modern programming environments. It is
    currently being developed by students and researchers at Southern Illinois
    University Edwardsville and Washington State University. Below is a short movie
    of Sodbeans in action with children at the Washington State School
    for the Blind. <a href="http://netbeans.dzone.com/news/sodbeans-netbeans-module-duke-2011-winner">Sodbeans was one of the winners of the 2011 Java Innovation Award, also known 
    as the Duke's choice Award!</a> </p>

    <p >
        <object width="640" height="385">
            <param name="movie" value="http://www.youtube.com/v/lC1mOSdmzFc?fs=1&amp;hl=en_US"></param>
            <param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param>
            <embed src="http://www.youtube.com/v/lC1mOSdmzFc?fs=1&amp;hl=en_US" type="application/x-shockwave-flash"
                   allowscriptaccess="always" allowfullscreen="true" width="640" height="385"></embed>
        </object>
    </p>
</div>
<?php include("include/footer.html"); ?>