<?php include("include/header.html"); ?>
<?php include("include/menu_eng.html"); ?>
<script type="text/javascript">
    document.title += ' Download Sodbeans';
    document.getElementById("download_button").className = "HereButton";
</script>

<h1 class="nomargin">Download Sodbeans 3.5 Full Version or <a href="http://sodbeans.sourceforge.net/downloadAll.php">Download Sodbeans 3.5 Student Version</a> </h1>
<p>Sodbeans 3.5 includes an interpreter for the Quorum programming language and significantly
expanded accessibility features. This version of Sodbeans is built on the
NetBeans 7.2 release by Oracle.</p>
<h2>Sodbeans 3.5 Windows Installer</h2>
<p><a href="https://sourceforge.net/projects/sodbeans/files/Sodbeans/Sodbeans_3_5/Public/Sodbeans%203.5.exe/download" 
      onClick="_gaq.push(['_trackEvent', 'Downloads', 'Software', 'Sodbeans 3.5 Windows']);"><img src="images/download.png"
                 alt="Click this image to download the Sodbeans 3.5 Windows installer."/></a>
</p>

<h2>Sodbeans 3.5 Mac OS X Installer</h2>
<p><a href="https://sourceforge.net/projects/sodbeans/files/Sodbeans/Sodbeans_3_5/Public/sodbeans-macosx.tgz/download" 
      onClick="_gaq.push(['_trackEvent', 'Downloads', 'Software', 'Sodbeans 3.5 Mac']);"><img src="images/download.png"
                 alt="Click this image to download the Sodbeans 3.5 Mac OS X installer."/></a>
</p>

<h2>Sodbeans 3.5 Linux Installer</h2>
<p><a href="https://sourceforge.net/projects/sodbeans/files/Sodbeans/Sodbeans_3_5/Public/sodbeans-linux.sh/download" 
      onClick="_gaq.push(['_trackEvent', 'Downloads', 'Software', 'Sodbeans 3.5 Linux']);"><img src="images/download.png"
                 alt="Click this image to download the Sodbeans 3.5 Linux installer."/></a>
</p>

<h2>Sodbeans 3.5 Binaries (Windows, Mac OS X, or Linux, no installer)</h2>
<p><a href="https://sourceforge.net/projects/sodbeans/files/Sodbeans/Sodbeans_3_5/Public/sodbeans%203.5.zip/download" 
      onClick="_gaq.push(['_trackEvent', 'Downloads', 'Software', 'Sodbeans 3.5 Binaries']);"><img src="images/download.png"
                 alt="Click this image to download the Sodbeans 3.5 binaries with no installer."/></a>
</p>

<h2>Sodbeans 3.5 Source Code</h2>
<p><a href="https://sourceforge.net/projects/sodbeans/files/Sodbeans/Sodbeans_3_5/Public/Sodbeans%203.5%20Source.zip/download" 
      onClick="_gaq.push(['_trackEvent', 'Downloads', 'Software', 'Sodbeans 3.5 Source']);"><img src="images/download.png"
                 alt="Click this image to download the Sodbeans 3.5 binaries with no installer."/></a>
</p>
<br />
<p>Please feel free to join the
<a href = "https://lists.sourceforge.net/lists/listinfo/sodbeans-developer">Mailing List</a>
or check the <a href="https://sourceforge.net/apps/trac/sodbeans/wiki/Sodbeans/CompileSodbeans">
    wiki</a> for more information.
</p>
<?php include("include/footer.html"); ?>