/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 2010 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 *
 * Contributor(s):
 *
 * Portions Copyrighted 2010 Sun Microsystems, Inc.
 */

package org.netbeans.server.uihandler.api;

import java.util.Collection;

/**
 *
 * @author Jindrich Sedek
 */
public interface Submit {

    public boolean isInBugzilla();

    public int getBugzillaId();

    public String getCommentString();

    public Collection<Submit> getDuplicates();

    public Reporter getReporter();

    public String getVM();

    public String getOS();

    public String getProductVersion();

    public String getBugzillaVersion();

    public String getAdditionalComment();

    public String getComponent();

    public String getSubcomponent();

    public int getId();

    public int getReportId();

    public String getSummary();

    public String getStatusWhiteboard();

    public String getKeywords();

    public Attachment getAttachment();

    public static final class Attachment {
        private final String fileNameFormat;
        private final String description;
        private final String type;
        private final byte[] data;

        public Attachment(String fileNameFormat, String description, String type, byte[] data) {
            this.fileNameFormat = fileNameFormat;
            this.description = description;
            this.type = type;
            this.data = data;
        }

        public byte[] getData() {
            return data;
        }

        public String getDescription() {
            return description;
        }

        public String getFileNameFormat() {
            return fileNameFormat;
        }

        public String getType() {
            return type;
        }
        
    }

    public static final class Reporter {

        private final String username;
        private final boolean isDirectUser;
        private final String directUserPrefix;

        public Reporter(String username, boolean isDirectUser, String directUserPrefix) {
            this.username = username;
            this.isDirectUser = isDirectUser;
            this.directUserPrefix = directUserPrefix;
        }

        public String getDirectUserPrefix() {
            return directUserPrefix;
        }

        public boolean isIsDirectUser() {
            return isDirectUser;
        }

        public String getUserName() {
            return username;
        }
    }
}
