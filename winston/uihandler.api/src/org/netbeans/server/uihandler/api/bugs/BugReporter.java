/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */
package org.netbeans.server.uihandler.api.bugs;

import java.util.Collection;
import java.util.logging.Logger;
import org.netbeans.server.uihandler.api.Issue;
import org.netbeans.server.uihandler.api.Submit;

/**
 *
 * @author Jan Horvath
 */
public interface BugReporter {

    public static final String GUEST_USER = "GUEST";// NOI18N
    public static final Logger LOG = Logger.getLogger("org.netbeans.modules.exceptions.bugs.reporter");

    /**
     * Reports exception to the bug tracking system
     * 
     * @param submit Submit implementation that should be reported
     * @param password password that should be used to authenticate submit reporter with BZ tracking
     * @param extraUserToCC extra user that should be added at cc, might be null
     * @param extraComment extra comment that should be added to the bug, might be null
     * @return id of newly created bug
     *
     */
    public int reportSubmit(Submit submit, String password, String extraComment, String extraUserToCC) throws BugReporterException;


    public void createAttachment(Submit submit, String password) throws BugReporterException;
    /**
     * Add text comment to the issue
     * 
     * @param issueId Issue ID
     * @param text text to add
     * @param username name of user that should add the comment
     * @param password password that should be used to authenticate the user with BZ tracking
     */
    public void postTextComment(int issueId, String text, String username, String password) throws BugReporterException;

    /**
     * Set the issue priority
     * 
     * @param issueId Issue ID
     * @param priority P1, P2, ...
     * @param username name of user that should change the priority
     * @param password password that should be used to authenticate the user with BZ tracking
     */
    public void setIssuePriority(int issueId, String priority, String username, String password) throws BugReporterException;

    /**
     * Add Username to the issue's CC list
     * 
     * @param issueId issue id
     * @param username name of user that should be added to the list
     * @param password password that should be used to authenticate the user with BZ tracking
     */
    public void addToCCList(int issueId, String username, String password) throws BugReporterException;

    /**
     * Reopen issue
     * 
     * @param issueId Issue ID
     * @param comment comment that should be added to the issued during the reopen action
     * @param username name of user that should invoke the reopen action
     * @param password password that should be used to authenticate the user with BZ tracking
     *
     */
    public void reopenIssue(int issueId, String comment, String username, String password) throws BugReporterException;

    /**
     * Get Issue entity
     * 
     * @param issueId issue id that should be found
     */
    public Issue getIssue(int issueId);

    /**
     * Go through the list and remove all already closed bugs
     * 
     * @param ids collection of bugs' ids to filter
     * @return collecion of open bugs' ids as a subset of ids
     */
    public Collection<Integer> filterOpen(Collection<Integer> ids);
}
