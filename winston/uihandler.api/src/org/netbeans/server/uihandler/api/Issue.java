/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 2010 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 *
 * Contributor(s):
 *
 * Portions Copyrighted 2010 Sun Microsystems, Inc.
 */

package org.netbeans.server.uihandler.api;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jindrich Sedek
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Issue implements Serializable {

    private Integer id;
    private Integer reporterSubmitId;
    private String issueStatus;
    private String priority;
    private String resolution;
    private String component;
    private String subcomponent;
    private String targetMilestone;
    private String issueType;
    private String statusWhiteboard;
    private String shortDesc;
    private String keywords;
    private String reporter;
    private Date lastResolutionChange;
    private List<String> ccUsers;
    private Integer duplicateOf;
    public static final String TBD_TM = "TBD";

    public Issue() {
    }

    public Issue(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getIssueStatus() {
        return issueStatus;
    }

    public void setIssueStatus(String issueStatus) {
        this.issueStatus = issueStatus;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getResolution() {
        return resolution;
    }

    public void setResolution(String resolution) {
        this.resolution = resolution;
    }

    public String getComponent() {
        return component;
    }

    public void setComponent(String component) {
        this.component = component;
    }

    public String getSubcomponent() {
        return subcomponent;
    }

    public void setSubcomponent(String subcomponent) {
        this.subcomponent = subcomponent;
    }

    public String getTargetMilestone() {
        return targetMilestone;
    }

    public void setTargetMilestone(String targetMilestone) {
        this.targetMilestone = targetMilestone;
    }

    public String getReporter() {
        return reporter;
    }

    public void setReporter(String reporter) {
        this.reporter = reporter;
    }

    public String getIssueType() {
        return issueType;
    }

    public void setIssueType(String issueType) {
        this.issueType = issueType;
    }

    public String getStatusWhiteboard() {
        return statusWhiteboard;
    }

    public void setStatusWhiteboard(String statusWhiteboard) {
        this.statusWhiteboard = statusWhiteboard;
    }

    public String getShortDesc() {
        return shortDesc;
    }

    public void setShortDesc(String shortDesc) {
        this.shortDesc = shortDesc;
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public Date getLastResolutionChange() {
        return lastResolutionChange;
    }

    public void setLastResolutionChange(Date lastResolutionChange) {
        this.lastResolutionChange = lastResolutionChange;
    }

    public Integer getReporterSubmitId() {
        return reporterSubmitId;
    }

    public void setReporterSubmitId(Integer reporterSubmitId) {
        this.reporterSubmitId = reporterSubmitId;
    }

    public List<String> getCcUsers() {
        return ccUsers;
    }

    public void setCcUsers(List<String> ccUsers) {
        this.ccUsers = ccUsers;
    }

    public Integer getDuplicateOf() {
        return duplicateOf;
    }

    public void setDuplicateOf(Integer duplicateOf) {
        this.duplicateOf = duplicateOf;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null
                ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        /*
         * TODO: Warning - this method won't work in the case the id fields are not set
         */
        if (!(object instanceof Issue)) {
            return false;
        }
        Issue other = (Issue) object;
        if (this.id != other.id && (this.id == null || !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "test.Issue[issueId=" + id + "]";
    }

    public String getTMNumber() {
        String issueTM = getTargetMilestone();
        if (issueTM == null) {
            return null;
        }
        if (issueTM.contains(TBD_TM)) {
            return TBD_TM;
        }
        int i = 0;
        boolean isDigitOrDot;
        do {
            Character c = issueTM.charAt(i);
            isDigitOrDot = (Character.isDigit(c) || c.equals('.'));
            if (!(isDigitOrDot)) {
                break;
            }
            i++;
        } while (i < issueTM.length());
        if (i == 0) {
            return null;
        }
        return issueTM.substring(0, i);
    }

    public boolean isOpen() {
        boolean isClosed = "RESOLVED".equals(getIssueStatus()) || // NOI18N
                "VERIFIED".equals(getIssueStatus()) || // NOI18N
                "CLOSED".equals(getIssueStatus());
        return !isClosed;
    }

    public boolean isWorksForMe() {
        if (!isOpen() && "WORKSFORME".equals(getResolution())) {// NOI18N
            return true;
        }
        return false;
    }

    public boolean isFixed() {
        if (!isOpen() && "FIXED".equals(getResolution())) {// NOI18N
            return true;
        }
        return false;
    }

    public boolean isTBD() {
        return TBD_TM.equals(getTargetMilestone());
    }
}
