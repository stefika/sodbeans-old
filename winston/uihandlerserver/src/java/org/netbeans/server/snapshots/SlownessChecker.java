/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2009 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */
package org.netbeans.server.snapshots;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;
import javax.persistence.EntityManager;
import org.netbeans.modules.exceptions.entity.Logfile;
import org.netbeans.modules.exceptions.entity.Report;
import org.netbeans.modules.exceptions.entity.Slowness;
import org.netbeans.modules.exceptions.entity.Slowness.SlownessType;
import org.netbeans.modules.exceptions.utils.PersistenceUtils;
import org.netbeans.server.componentsmatch.Component;
import org.netbeans.server.componentsmatch.Matcher;

/**
 *
 * @author Jindrich Sedek
 */
public class SlownessChecker implements Comparator<Report>{

    private static final String UNKNOWN_SUSP_METHOD = "unknown";
    private final EntityManager em;
    private final String latestAction;
    private final SlownessType slownessType;
    private final SnapshotManager manager;
    private Component component;

    public SlownessChecker(EntityManager em, String latestAction, Logfile logInfo) {
        this(em, latestAction, null, logInfo);
    }
    
    public SlownessChecker(EntityManager em, String latestAction, SlownessType slownessType, Logfile logInfo) {
        this.em = em;
        this.latestAction = latestAction;
        this.slownessType = slownessType;
        this.manager = SnapshotManager.loadSnapshot(logInfo);
    }

    public Report handleCCSlowness(){
        List<Slowness> candidatesByType = PersistenceUtils.executeNamedQuery(em, "Slowness.findBySlownessType",
                    Collections.singletonMap("type", slownessType.toString()));     
        List<Slowness> suspiciousMethodCandidates = getSnapshotCandidates();
        TreeSet<Slowness> intersecion = new TreeSet<Slowness>(candidatesByType);
        intersecion.retainAll(suspiciousMethodCandidates);
        if (!intersecion.isEmpty()){
            return getLatestReport(intersecion);
        }
        if (!suspiciousMethodCandidates.isEmpty()){
            return getLatestReport(suspiciousMethodCandidates);
        }
        return null;
    }
    
    public Report checkSlowness() {
        if (slownessType != null ){
            if(SlownessType.CodeCompletion == slownessType){
                return handleCCSlowness();
            }
            List<Report> candidates = PersistenceUtils.executeNamedQuery(em, "Slowness.findReportsBySlownessType",
                    Collections.singletonMap("type", slownessType.toString()));
            for (Report report : candidates) {
                if (!report.isInIssuezilla()){
                    return report;
                }
                if (PersistenceUtils.issueIsOpen(report.getIssueId())){
                    return report;
                }
            }
            return null;
        }
        List<Slowness> lastActionCandidates = filterNullType(getLastActionCandidates());
        List<Slowness> suspiciousMethodCandidates = filterNullType(getSnapshotCandidates());
        TreeSet<Slowness> intersecion = new TreeSet<Slowness>(lastActionCandidates);
        intersecion.retainAll(suspiciousMethodCandidates);
        if (!intersecion.isEmpty()){
            return getLatestReport(intersecion);
        }
        if (!suspiciousMethodCandidates.isEmpty()){
            return getLatestReport(suspiciousMethodCandidates);
        }
        return null;
    }
    
    private List<Slowness> filterNullType(List<Slowness> input){
        //filter reports with no slowness type
        List<Slowness> result = new ArrayList<Slowness>();
        for (Slowness slowness : input) {
            if(slowness.getSlownessType()==null){
                result.add(slowness);
            }
        }
        return result;
    }

    private Report getLatestReport(Collection<Slowness> sl){
        TreeSet<Report> reportCandidates = new TreeSet<Report>(this);
        for (Slowness slowness : sl) {
            reportCandidates.add(slowness.getReportId());
        }
        return reportCandidates.last();
    }

    private List<Slowness> getLastActionCandidates() {
        if (latestAction != null) {
            return PersistenceUtils.executeNamedQuery(em,
                    "Slowness.findByLatestAction", Collections.singletonMap("action", latestAction), Slowness.class);
        }else{
            return Collections.<Slowness>emptyList();
        }
    }

    private List<Slowness> getSnapshotCandidates() {
        Map<String, Object> param = Collections.singletonMap("methodName", (Object)getSuspiciousMethodName());
        List<Slowness> candidates = PersistenceUtils.executeNamedQuery(em, "Slowness.findBySuspiciousMethodName", param, Slowness.class);
        return candidates;
    }

    public Component getComponentForSlowness(){
        if (component == null){
            if (manager != null){
                component = manager.getComponent(em);
            }
            if ((component == null || Component.UNKNOWN.equals(component)) && latestAction !=null){
                Component compFromAction = Matcher.getDefault().matchMethod(em, latestAction);
                if (compFromAction != null){
                    component = compFromAction;
                }
            }
        }
        return Matcher.getDefault().getRealComponent(em, component);
    }

    public String getSuspiciousMethodName(){
        if (manager == null){
            return UNKNOWN_SUSP_METHOD;
        }
        MethodItem mi = null;
        //for CC do not search AWT thread, but CodeCompletion one
        if(slownessType == slownessType.CodeCompletion){
            mi = manager.getSuspiciousMethodItemForCC(em);
        } else {
            mi = manager.getSuspiciousMethodItem(em);
        }
        if (mi == null){
            return UNKNOWN_SUSP_METHOD;
        }
        return mi.getMethodName();
    }

    public int compare(Report o1, Report o2) {
        return o1.getId().compareTo(o2.getId());
    }
}


