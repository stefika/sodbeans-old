/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2007 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */

package org.netbeans.server.componentsmatch;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import org.netbeans.modules.exceptions.entity.Exceptions;
import org.netbeans.modules.exceptions.entity.InnocentClass;
import org.netbeans.modules.exceptions.entity.Method;
import org.netbeans.modules.exceptions.entity.SourcejarMapping;
import org.netbeans.modules.exceptions.utils.PersistenceUtils;
import org.netbeans.server.services.beans.Components;
import org.openide.util.lookup.ServiceProvider;

/**
 * @author Jindrich Sedek
 */
@ServiceProvider(service=Matcher.class)
public final class SourcejarMatcher extends Matcher {

    private List<InnocentClass> innocents;
    private List<SourcejarMapping> mappings;
    
    private static final String PARAMETERS_CLASS_NAME = "org.openide.util.Parameters";

    public SourcejarMatcher() {
        reload();
    }

    public void reload(){
        innocents = PersistenceUtils.getInstance().getAll(InnocentClass.class);
        mappings = PersistenceUtils.getInstance().getAll(SourcejarMapping.class);
    }

    private Component getComponentFromSourceJar(EntityManager em, StackTraceElement stackTraceElement) {
        SourcejarMapping bestMapping = null;
        String methodName = Method.getMethodNameFromSTE(stackTraceElement);
        Method method = (Method) PersistenceUtils.getExist(em, "Method.findByName", methodName);
        if ((method != null) && (method.getSourcejar() != null)){
            for (SourcejarMapping mapping: mappings){
                if (!mapping.matches(method.getSourcejar())){
                    continue;
                }
                if ((bestMapping == null) || (mapping.getSourcejar().length() > bestMapping.getSourcejar().length())){
                    bestMapping = mapping;
                }
            }
        }
        if (bestMapping != null){
          return new Component(bestMapping.getComponent().toLowerCase(), bestMapping.getSubcomponent());
        }
        return null;
    }
        
    private Component fromJar(String jarFileName){
        if (jarFileName == null){
            return null;
        }
        if (jarFileName.startsWith("${java.home}")){
            return null;
        }
        if (jarFileName.startsWith("$")){
            int position = jarFileName.indexOf('}');
            String comp =jarFileName.substring(2, position);
            String subComp = jarFileName.substring(jarFileName.lastIndexOf('/')+1, jarFileName.length());
            if ("user.home".equals(comp)&& "tools.jar".equals(subComp)){
                return null;
            }
            return new Component(comp, subComp);
        }
        return null;
    }

    /** @return component or null if no component was found
     * @param stes exception stacktrace
     */
    public Component match(EntityManager em, StackTraceElement[] stes){
        String classToSkip = null;
        for (int i=0; i<stes.length; i++){
            if (PARAMETERS_CLASS_NAME.equals(stes[i].getClassName())){
                classToSkip = stes[i+1].getClassName();
                continue;
            }
            if ((classToSkip != null) && (stes[i].getClassName().equals(classToSkip))){
                continue;
            }
            String methodName = Method.getMethodNameFromSTE(stes[i]);
            if (isInnocent(methodName)) {
                continue;
            }
            Component comp = getComponentFromSourceJar(em, stes[i]);
            if (comp == null){
                comp = fromJar(stes[i].getFileName());
                if (comp != null){
                    String jarName = comp.getSubComponent();
                    SourcejarMapping bestMapping = null;
                    for (SourcejarMapping mapping: mappings){
                        if (!mapping.matches(jarName)){
                            continue;
                        }
                        if ((bestMapping == null) || (mapping.getSourcejar().length() > bestMapping.getSourcejar().length())){
                            bestMapping = mapping;
                        }
                    }
                    if (bestMapping != null){
                      comp = new Component(bestMapping.getComponent(), bestMapping.getSubcomponent());
                    }
                }
            }
            if (comp != null){
                return comp;
            }
        }
        return null;
    }
    
    private boolean isInnocent(String className){
        for (InnocentClass innocent : innocents) {
            if (className.contains(innocent.getClassname())){
                return true;
            }
        }
        return false;
    }
    
    public Component getRealComponent(EntityManager em, Component expectedComponent){
        if (expectedComponent == null){
            return Component.UNKNOWN;
        }
        Component result = new Component(expectedComponent.getComponent().toLowerCase(), expectedComponent.getSubComponent());
        if (isNetbeansDir(expectedComponent.getComponent()) && isNBModule(expectedComponent.getSubComponent())) {
            Components componentsObj = PersistenceUtils.getInstance().getComponents();
            List<String> componentsList = new ArrayList<String>(componentsObj.getComponentsSet());
            Collections.sort(componentsList, new Comparator<String>(){

                public int compare(String o1, String o2) {
                    return o2.length() - o1.length();
                }

            });
        } else if (isNetbeansDir(expectedComponent.getComponent())) {
            result = Component.UNKNOWN;
        }
        if (!isValid(result)){
            Logger.getLogger(Matcher.class.getName()).log(Level.WARNING, 
                    "Trying to set invalid component and subcomponent: {0}", result.toString());
            result = Component.UNKNOWN;
        }
        return result;
    }

    private boolean isNetbeansDir(String component){
        return "netBeansDir".equalsIgnoreCase(component);
    }

    private boolean isNBModule(String jarName){
        return jarName.startsWith("org-netbeans-modules");
    }

    private boolean isValid(Component comp) {
//        if (Arrays.asList(Exceptions.generatedComponents).contains(comp.getComponent())){
//            return true;
//        }
        return PersistenceUtils.isExistingComponent(comp.getComponent(), comp.getSubComponent());
    }
}
