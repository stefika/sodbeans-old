/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2007 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */
package org.netbeans.server.componentsmatch;

/**
 *  Issuezilla component with assigned numbers of issues
 * @author Petr Zajac
 * @author Jindrich Sedek
 */
public final class Component implements Comparable<Component> {

    public static Component UNKNOWN = new Component("ide", "Code");
    public static Component UNKNOWN_SLOWNESS = new Component("performance", "Code");
    private final String component;
    private final String subcomponent;

    public Component(String component, String subcomponent) {
        this.component = component;
        this.subcomponent = subcomponent;
    }

    public int compareTo(Component comp) {
        int ret = component.compareTo(comp.component);
        if (ret == 0) {
            ret = subcomponent.compareTo(comp.subcomponent);
        }
        return ret;
    }

    public String getComponent() {
        return component;
    }

    public String getSubComponent() {
        return subcomponent;
    }

    @Override
    public String toString() {
        return component + "/" + subcomponent;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Component) {
            Component c2 = (Component) obj;
            return equalStrings(component, c2.component) && equalStrings(subcomponent, c2.subcomponent);
        } else {
            return false;
        }
    }

    private boolean equalStrings(String s1, String s2) {
        if (s1 == null) {
            return s2 == null;
        } else {
            return s1.equals(s2);
        }
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 31 * hash + (this.component != null ? this.component.hashCode() : 0);
        hash = 31 * hash + (this.subcomponent != null ? this.subcomponent.hashCode() : 0);
        return hash;
    }
}

