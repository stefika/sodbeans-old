/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */
package org.netbeans.server.usages;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.net.SocketTimeoutException;
import java.util.Calendar;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.netbeans.lib.uihandler.MultiPartHandler;
import org.netbeans.server.uihandler.RequestFacadeImpl;
import org.netbeans.server.uihandler.Utils;

/**
 *
 * @author Jindrich Sedek
 */
public class UsagesUpload extends HttpServlet {

    private static final Logger LOG = Logger.getLogger(UsagesUpload.class.getName());
    private static final String PROPERTIES_FILE_SUFFIX = ".properties";
    private static String usagesDirPath;

    @Override
    public void init() throws ServletException {
        usagesDirPath = Utils.getVariable("usages", String.class);
        if (usagesDirPath == null) {
            LOG.severe("usages dir is not set");
            return;
        }
        File usagesDir = new File(usagesDirPath);
        if (!usagesDir.isDirectory() && !usagesDir.mkdirs()){
            LOG.severe("usagesDir is not a directory");
            usagesDirPath = null;
        }
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LOG.fine("accepting usages file");
        if (usagesDirPath == null) {
            LOG.severe("usagesDir is not set");
            response.sendRedirect(request.getContextPath() + "/error.jsp");
            return;
        }
        

        File appFile = null;
        String contentType = request.getContentType();
        if (contentType != null && contentType.toLowerCase().startsWith("multipart/form-data")) {

            RequestFacadeImpl facade = new RequestFacadeImpl(request);
            MultiPartHandler multiReq = new MultiPartHandler(facade, usagesDirPath, Utils.getMaxUploadSize());
            try{
                multiReq.parseMultipartUpload();
                // the file is uploaded as arguments "logs"
                appFile = multiReq.getFile("logs"); // NOI18N
            }catch(SocketTimeoutException ste){
                LOG.severe(ste.getMessage());
                response.sendError(HttpServletResponse.SC_REQUEST_TIMEOUT);
                return;
            }finally{
                facade.getInput().getInputStream().close();
            }
        }
        if (appFile != null){
            String ip = request.getHeader("x-forwarded-for");
            if (ip == null){
                ip = request.getRemoteHost();// no Lighttpd
            }
            File properties = new File(usagesDirPath, appFile.getName() + PROPERTIES_FILE_SUFFIX);
            PrintStream os = new PrintStream(new FileOutputStream(properties));
            os.println("IP="+ip);
            os.println("DATE="+Calendar.getInstance().getTimeInMillis());
            os.close();
        }
        request.setAttribute("redirect",  Boolean.TRUE);
        LOG.finer("sending response id");
        request.getRequestDispatcher("/usages_redirect.jsp").forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "NetBeans usages upload servlet";
    }// </editor-fold>
}
