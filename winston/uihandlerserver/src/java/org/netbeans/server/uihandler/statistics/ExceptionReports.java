/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */
package org.netbeans.server.uihandler.statistics;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;
import javax.persistence.EntityManager;
import javax.servlet.jsp.PageContext;
import org.netbeans.modules.exceptions.entity.Nbversion;
import org.netbeans.modules.exceptions.utils.PersistenceUtils;
import org.netbeans.server.uihandler.LogsManager;
import org.netbeans.server.uihandler.Statistics;
import org.netbeans.server.uihandler.statistics.ExceptionReports.Data;
import org.netbeans.web.Persistable;
import org.netbeans.web.Utils;
import org.openide.util.RequestProcessor;
import org.openide.util.Task;
import org.openide.util.lookup.ServiceProvider;

/**
 *
 * @author Jindrich Sedek
 */
@ServiceProvider(service=Statistics.class)
public class ExceptionReports extends Statistics<ExceptionReports.Data> implements Runnable, Persistable {

    private static final int HISTORY_LENGHT = 2; //in years
    private static final Logger LOG = Logger.getLogger(ExceptionReports.class.getName());
    private static final int HOURS_IN_MILISECONDS = 1000 * 3600;
    private static final int RELOAD_INTERVAL = HOURS_IN_MILISECONDS * 17;
    private static int MIN_COUNT = 300;
    private static Date BASE_DATE = null;
    private static final String START = "start";
    private static Data globalData;
    private static Task refreshTask;

    public ExceptionReports() {
        super("ExceptionReports");
        if ((refreshTask == null) || (refreshTask.isFinished())) {
            refreshTask = RequestProcessor.getDefault().post(this, HOURS_IN_MILISECONDS, Thread.MIN_PRIORITY);
        }
    }

    protected Data newData() {
        return null;
    }

    protected Data process(LogRecord rec) {
        return null;
    }

    protected Data join(Data one, Data two) {
        return null;
    }

    protected Data finishSessionUpload(String userId, int sessionNumber, boolean initialParse, Data data) {
        return null;
    }

    public void run() {
        LOG.info("Refreshing ExceptionReports statistics");
        refreshTask = RequestProcessor.getDefault().post(this, RELOAD_INTERVAL, Thread.MIN_PRIORITY);
        Utils.processPersistable(this);
    }

    static void setMinCountAndBaseDate(int min_count, Date baseDate) {
        MIN_COUNT = min_count;
        BASE_DATE = baseDate;
    }

    private synchronized void loadData() {
        if (globalData == null) {
            Utils.processPersistable(this);
        }
    }

    public TransactionResult runQuery(EntityManager em) {
        Data dat = new Data();
        Map<String, Integer> components = new TreeMap<String, Integer>();
        List<ReportItem> monthStatistics = new ArrayList<ReportItem>();
        List<ReportItem> reportDateStatistics = new ArrayList<ReportItem>();
        try {
            dat.reportsCount = PersistenceUtils.executeCount(em, "Exceptions.countAll", null);
            dat.slownessCount = PersistenceUtils.executeCount(em, "Slowness.countAll", null);
            components = reportsComponentsDivision(em, dat.reportsCount);
            Map<String, Object> map = new HashMap<String, Object>(3);
            Map<String, Object> dateMap = new HashMap<String, Object>(3);
            Calendar cald = Calendar.getInstance();
            if (BASE_DATE != null){
                cald.setTime(BASE_DATE);
            }
            long realYear = cald.get(Calendar.YEAR);
            long year = cald.get(Calendar.YEAR) % 100 - HISTORY_LENGHT;
            long month = cald.get(Calendar.MONTH);
            Long border = year * 100 + month % 12 + 1;
            Date dateBorder = new Date((int) (realYear - 1901 + month / 12), (int) (month % 12 + 1), 1);
            map.put(START, border * 100);
            dateMap.put( START,dateBorder);
            List<Nbversion> versions = PersistenceUtils.executeNamedQuery(em, "Nbversion.all", null, Nbversion.class);
            LOG.fine("versions count = " + versions.size());
            for (Nbversion nbversion : versions) {
                for (long i = month + 1; i <= month + 1 + HISTORY_LENGHT * 12; i++) {
                    int begin_border = border.intValue();
                    border = (year + i / 12) * 100 + i % 12 + 1;
                    dateBorder = new Date((int) (realYear - 1901 + i / 12), (int) (i % 12 + 1), 1);
                    dateMap.put("end", dateBorder);
                    dateMap.put("version", nbversion);
                    map.put("end", border * 100);
                    map.put("version", nbversion);
                    int count = PersistenceUtils.executeCount(em, "Exceptions.buildDateQuery", map);
                    if (count > MIN_COUNT) {
                        monthStatistics.add(new ReportItem(begin_border % 100, begin_border / 100, count, nbversion));
                    }
                    count = PersistenceUtils.executeCount(em, "Exceptions.reportDateQuery", dateMap);
                    if (count > MIN_COUNT) {
                        reportDateStatistics.add(new ReportItem(border.intValue() % 100, border.intValue() / 100 + 1, count, nbversion));
                    }
                    map.put(START, border * 100);
                    dateMap.put( START,dateBorder);
                }
            }

        } catch (Exception ex) {
            // requires database and that is not present
            System.out.println(ex);
            ex.printStackTrace();
            LOG.log(Level.WARNING, ex.getMessage(), ex);
        }
        dat.setComponents(components);
        dat.setMonthStatistics(monthStatistics);
        dat.setReportDateStatistics(reportDateStatistics);
        globalData = dat;
        return TransactionResult.NONE;
    }

    private Map<String, Integer> reportsComponentsDivision(EntityManager em, int globalReportsCount) {
        Map<String, Integer> componentsBugs = new TreeMap<String, Integer>();
        List<String> list = PersistenceUtils.executeNamedQuery(em, "Exceptions.distinctComponents", null, String.class);
        Iterator<String> iter = list.iterator();
        int minimumToShow = globalReportsCount / 50; // 2%
        while (iter.hasNext()) {
            String str = iter.next();
            int count = PersistenceUtils.executeCount(em, "Exceptions.countByComponent",
                    Collections.<String, Object>singletonMap("component", str));
            if ((str != null) && (count > minimumToShow)) {
                componentsBugs.put(str, count);
            }
        }
        return componentsBugs;
    }

    @Override
    protected void registerPageContext(PageContext page, String name, Data xxx) {
        LOG.fine("starting registering");
        if (globalData == null) {
            loadData();
        }
        super.registerPageContext(page, name, globalData);
    }

    public static final class Data {

        int reportsCount;
        int slownessCount;
        Map<String, Integer> components;
        /**
         * get(71100) returns number of reports from
         * 2007/10 to 2007/11 
         */
        private List<ReportItem> monthStatistics;
        private List<ReportItem> reportDateStatistics;

        public Data() {
            reportsCount = 0;
            slownessCount = 0;
        }

        public int getReportsCount() {
            return reportsCount;
        }

        public int getSlownessCount() {
            return slownessCount;
        }

        public int getGesturesCount(){
            return LogsManager.getDefault().getNumberOfLogs() - reportsCount - slownessCount;
        }

        public Map<String, Integer> getComponents() {
            if (components != null) {
                return components;
            } else {
                return Collections.emptyMap();
            }
        }

        private void setComponents(Map<String, Integer> components) {
            this.components = components;
        }

        public List<ReportItem> getMonthStats() {
            LOG.finer("monthStats:" + monthStatistics.size());
            return monthStatistics;
        }

        public void setMonthStatistics(List<ReportItem> monthStatistics) {
            Collections.sort(monthStatistics);
            this.monthStatistics = monthStatistics;
        }

        public List<ReportItem> getReportDateStats() {
            LOG.fine("reportStats:" + reportDateStatistics.size());
            return reportDateStatistics;
        }

        public void setReportDateStatistics(List<ReportItem> reportDateStatistics) {
            Collections.sort(reportDateStatistics);
            this.reportDateStatistics = reportDateStatistics;
        }

        public List<String> getSortedKeys() {
            List<String> list = new ArrayList<String>(components.keySet());
            Collections.sort(list, new ReportsComparator());
            return list;
        }

        private final class ReportsComparator implements Comparator<String> {

            public int compare(String o1, String o2) {
                return components.get(o2).compareTo(components.get(o1));
            }
        }
    }

    public static class ReportItem implements Comparable<ReportItem> {

        private static final String[] MONTHS_NAMES = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
        private Integer month,  year;
        private Integer count;
        private Nbversion version;

        public ReportItem(Integer month, Integer year, Integer count, Nbversion version) {
            this.month = month;
            this.year = year;
            this.count = count;
            this.version = version;
        }

        public String getSerie() {
            return version.getVersion();
        }

        public String getKey() {
            return MONTHS_NAMES[month - 1] + year % 10;
        }

        public Integer getValue() {
            return count;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj instanceof ReportItem) {
                ReportItem ri = (ReportItem) obj;
                return month.equals(ri.month) && year.equals(ri.year) && count.equals(ri.count);
            } else {
                return false;
            }
        }

        @Override
        public int hashCode() {
            int hash = 5;
            hash = 17 * hash + (this.month != null ? this.month.hashCode() : 0);
            hash = 17 * hash + (this.year != null ? this.year.hashCode() : 0);
            hash = 17 * hash + (this.count != null ? this.count.hashCode() : 0);
            return hash;
        }

        public int compareTo(ReportItem o) {
            if (year != o.year) {
                return year - o.year;
            }
            return month - o.month;
        }
    }

    protected void write(Preferences pref, Data d) throws BackingStoreException {
        // data are saved in DB for this statistics
    }

    protected Data read(Preferences pref) throws BackingStoreException {
        return null; // nothing is needed to be done
    }

    public boolean needsTransaction() {
        return false;
    }
}
