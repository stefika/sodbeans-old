/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2007 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */
package org.netbeans.server.uihandler.statistics;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;
import javax.servlet.jsp.PageContext;
import org.netbeans.server.uihandler.Statistics;
import org.openide.util.lookup.ServiceProvider;

/**
 *
 * @author Jindrich Sedek
 */
@ServiceProvider(service=Statistics.class)
public class TimeToFailture extends Statistics<List<TimeToFailture.TTFData>> {

    private static Logger LOG = Logger.getLogger(TimeToFailture.class.getName());
    protected static String TTFLoggingMessage = "org.netbeans.modules.uihandler.TimeToFailure:";

    public TimeToFailture() {
        super("TimeToFailture");
    }

    @Override
    protected List<TTFData> newData() {
        return null;
    }

    @Override
    protected List<TimeToFailture.TTFData> process(LogRecord rec) {
        if (rec.getLevel().equals(Level.CONFIG) && (rec.getMessage() != null)) {
            if (!rec.getMessage().contains(TTFLoggingMessage)) {
                return null;
            }
            String number = rec.getMessage().replaceAll(TTFLoggingMessage, "");
            if (number.length() == 0){
                return null;
            }
            try {
                List<TimeToFailture.TTFData> result = new ArrayList<TTFData>(1);
                long ttf = Long.parseLong(number);
                if (ttf > 0) {
                    result.add(new TTFData(ttf, 1));
                    return result;
                }
            } catch (NumberFormatException numberFormExc) {
                LOG.log(Level.SEVERE, "PARSING ERROR:" + number+":"+rec.getMessage(), numberFormExc);
            }
        }
        return null;
    }

    @Override
    protected List<TimeToFailture.TTFData> join(List<TimeToFailture.TTFData> one,
            List<TimeToFailture.TTFData> two) {
        if ((one == null)||(one.size()== 0)) {
            return two;
        }
        if ((two == null)||(two.size()== 0)) {
            return one;
        }
        /* MERGE ARRAYS */
        List<TimeToFailture.TTFData> result = new ArrayList<TimeToFailture.TTFData>(one.size() + two.size());
        Iterator<TTFData> oneIterator = one.iterator();
        Iterator<TTFData> twoIterator = two.iterator();
        TTFData oneItem = oneIterator.next(), twoItem = twoIterator.next();
        while ((oneItem != null) && (twoItem != null)) {//merge data
            if (oneItem.getTrimmedTime() < twoItem.getTrimmedTime()) {
                result.add(new TTFData(oneItem));
                oneItem = getNext(oneIterator);
            } else if (oneItem.getTrimmedTime() > twoItem.getTrimmedTime()) {
                result.add(new TTFData(twoItem));
                twoItem = getNext(twoIterator);
            } else {
                long middleTime = (oneItem.getTime() + twoItem.getTime()) / 2;
                result.add(new TTFData(middleTime, oneItem.getCount() + twoItem.getCount()));
                twoItem = getNext(twoIterator);
                oneItem = getNext(oneIterator);
            }
        }
        while (oneItem != null) {//add tail
            result.add(new TTFData(oneItem));
            oneItem = getNext(oneIterator);
        }
        while (twoItem != null) {//add tail
            result.add(new TTFData(twoItem));
            twoItem = getNext(twoIterator);
        }
        return result;
    }

    private TTFData getNext(Iterator<TTFData> it) {
        if (it.hasNext()) {
            return it.next();
        } else {
            return null;
        }
    }

    @Override
    protected List<TTFData> finishSessionUpload(String userId, int sessionNumber, boolean initialParse, List<TTFData> d) {
        return d;
    }

    @Override
    protected void registerPageContext(PageContext page, String name, List<TTFData> data) {
        if ((data != null)&&(data.size() > 0)){
            long MTTF_SUM = 0;
            long itemsCount = 0;
            List<TTFData> newData = new ArrayList<TTFData>(data.size() / 2);
            for (TimeToFailture.TTFData tTFData : data) {
                if (tTFData.getTime() >= TTFData.ROUND_CONST){
                    MTTF_SUM += tTFData.getTrimmedTime() * tTFData.getCount();
                    itemsCount += tTFData.getCount();
                    if (tTFData.getTrimmedTime() < TTFData.MAX_TIME){
                        newData.add(tTFData);
                    }
                }
            }
            page.setAttribute(name, newData);
            if (itemsCount != 0){
                page.setAttribute(name+"MTTF", MTTF_SUM / itemsCount);
            }
        }
    }

    @Override
    protected void write(Preferences pref, List<TTFData> d) throws BackingStoreException {
        if (d != null) {
            for (TTFData data : d) {
                pref.putInt(Long.toString(data.getTime()), data.getCount());
            }
        }
    }

    @Override
    protected List<TTFData> read(Preferences pref) throws BackingStoreException {
        String[] keys = pref.keys();
        int expected_size = 0;
        if (keys != null){
            expected_size = keys.length;
        }
        List<TTFData> times = new ArrayList<TTFData>(expected_size);
        for (String str : keys) {
            times.add(new TTFData(Integer.parseInt(str), pref.getInt(str, 0)));
        }
        Collections.sort(times);
        return times;
    }

    public static List<TTFData> groupData(List<TTFData> list, int factor){
        List<TTFData> result = new ArrayList<TTFData>();
        int leftBorder = 0;
        int rightBorder = 1;
        while (rightBorder <= list.size()){
            int count = list.get(leftBorder).getCount();
            for (int i = leftBorder+1; i < rightBorder; i++){
                count += list.get(i).getCount();
            }
            result.add(new TTFData(list.get(rightBorder - 1).getTime(), count));
            leftBorder = rightBorder;
            rightBorder = rightBorder * factor;
        }
        return result;
    }

    public static class TTFData implements Comparable<TTFData>{

        static int ROUND_CONST = 1000 * 60;//ms -> 1 min interval
        private static final int MAX_TIME = 1000;
        private int count;
        private long time;

        TTFData(TTFData d) {
            this.count = d.count;
            this.time = d.time;
        }

        TTFData(long time, int count) {
            this.count = count;
            this.time = time;
        }

        public int getCount() {
            return count;
        }

        public long getTime() {
            return time;
        }

        public String getSerie(){
            return "Time To Failure";
        }

        public long getTrimmedTime() {
            long trimmedTime = time / ROUND_CONST;
            return trimmedTime;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj instanceof TimeToFailture.TTFData) {
                TTFData data = (TTFData) obj;
                return (data.count == this.count) && (data.getTrimmedTime() == this.getTrimmedTime());
            } else {
                return false;
            }
        }

        @Override
        public int hashCode() {
            int hash = 3;
            hash = 11 * hash + this.count;
            hash = 11 * hash + new Long(this.time).intValue();
            return hash;
        }

        public int compareTo(TTFData o) {
            return new Long(getTrimmedTime()).compareTo(o.getTrimmedTime());
        }
    }
}
