/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * Copyright 2007 Sun Microsystems, Inc. All rights reserved.
 */

package org.netbeans.server.uihandler;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.logging.LogRecord;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;
import javax.servlet.jsp.PageContext;
import org.netbeans.lib.uihandler.LogRecords;

/** A provider of one kind of statistics for the UI handler server.
 * The provider should be a subclass of this call, with a statically 
 * defined <code>Data</code> parameter to hold the actual statistics.
 * For example:
 * <pre>
 * public class CountRecords extends Statistics&lt;Integer&gt; {
 *   public CountRecords() {
 *     super("CountRecords");
 *   }
 *   // etc.
 * }
 * </pre>
 * This class should be registered in 
 * <code>META-INF/services/org.netbeans.server.uihandler.Statistics</code>
 * file. Then it is picked by the processing server up and its methods
 * are called, and feeded with <code>LogRecord</code>s. When a request
 * for a page is made via {@link LogRecords#preparePage} three parameters
 * are inserted into <code>PageContext</code>, for the CountRecords example
 * that would be:
 * <ul>
 *   <li>globalCountRecords
 *   <li>userCountRecords
 *   <li>lastCountRecords
 * </ul>
 * Each of them contains a pointer to the "Data" - e.g. <code>Integer</code>
 * in case of <code>CountRecords</code>. These beans can then be used
 * from any JSP page.
 * <p>
 * The <code>Data</code> object is supposed to be immutable and shall not
 * be modified. Instead new copy should be created in each method of the
 * Statistics class if necessary.
 *
 * @param Data type of data this statistic produces and consumes
 * @author Jaroslav Tulach
 */
public abstract class Statistics<Data> {
    /** in case we need to rebuild our database, it is possible to 
     * increase this variable, so all revisions for all existing
     * statistics are updated.
     */
    private static final int BASE_REVISION = 1;
    
    final String name;
    final int revision;

    /** Creates new statistics with given name. Constructor for subclasses.
     * @param name name of the statistics
     * @param revision identifies the revision of the persistent data,
     *   if a statistic changes its format of persistence datas in a way
     *   that it can no longer understand its previous format, bump the 
     *   revision number, the system will discard old info a produce new
     */
    protected Statistics(String name, int revision) {
        this.name = name == null ? getClass().getSimpleName() : name;
        this.revision = BASE_REVISION + revision;
    }

    /** Creates new statistics with given name and revision 0.
     * 
     * @param name name of the statistics
     */
    protected Statistics(String name) {
        this(name, 0);
    }

    protected Statistics(int revision) {
        this(null, revision);
    }
    protected Statistics() {
        this(null);
    }

    /** Creates new, empty data for statistics.
     * @return the data, not null
     */
    protected abstract Data newData();

    /** Reads data from a record and converts them into
     * reasonable data representation
     * 
     * @param rec the record to analyse
     * @return data representing the values in the record, not null
     */
    protected abstract Data process(LogRecord rec);

    /** Called when all records in one session have been processed.
     * 
     * @param userId identification of the user
     * @param sessionNumber the id of the session for the user
     * @param initialParse is this initial upload or just a reparse from 
     *   data existing on a disk
     * @param d the data collected from the log records found in the session
     * @return new data that should represent the session
     */
    protected abstract Data finishSessionUpload(String userId, int sessionNumber, boolean initialParse, Data d);

    /** Merges values of two data and produces a concatation
     * or an average, etc. representing the merged data.
     * 
     * @param one input data
     * @param two input data
     * @return merged values, not null
     */
    protected abstract Data join(Data one, Data two);

    
    /** Registers given data into page context for consumption by associated
     * JSP page. The default implementation just assigns the data to the
     * name.
     * 
     * @param page page context to fill
     * @param name name of the variable to assign there
     * @param data the data to assign to the variable
     */
    protected void registerPageContext(PageContext page, String name, Data data) {
        page.setAttribute(name, data);
    }

    /** Method that stores currently obtained data into a persistant 
     * storage.
     * 
     * @param d data
     * @param pref the storage
     */
    protected abstract void write(Preferences pref, Data d) throws BackingStoreException;

    /** Method that restores data from a persistant 
     * storage.
     * 
     * @param pref the storage
     * @return the data read from the storage
     */
    protected abstract Data read(Preferences pref) throws BackingStoreException;
    
    
    //
    // Useful helper methods
    //

    protected final void writeMap(Preferences node, Map<? extends Object, Integer> map) {
        if (map == null){
            return;
        }
        for (Map.Entry<? extends Object, Integer> entry : map.entrySet()) {
            String key = entry.getKey().toString();
            if (key.length() > Preferences.MAX_KEY_LENGTH){
                key = key.substring(key.length() - Preferences.MAX_KEY_LENGTH);
            }
            node.putInt(key, entry.getValue());
        }
    }

    protected final Map<Integer, Integer> readCounts(Preferences node) throws BackingStoreException {
        String[] keys = node.keys();
        Map<Integer, Integer> map = new HashMap<Integer, Integer>(keys.length);
        for (String string : keys) {
            map.put(Integer.parseInt(string), node.getInt(string, 0));
        }
        return map;
    }

    protected final <T> void addAllCounts(Map<T, Integer> base, Map<T, Integer> additions) {
        for (Map.Entry<T, Integer> entry : additions.entrySet()) {
            T key = entry.getKey();
            if (base.containsKey(entry.getKey())) {
                Integer sum = entry.getValue() + base.get(key);
                base.put(key, sum);
            } else {
                base.put(key, entry.getValue());
            }
        }
    }

    protected static void putSet(Statistics statistics, Preferences pref, String key, Set<String> set) throws BackingStoreException {
        assert pref instanceof DbPreferences;
        DbPreferences dbPref = (DbPreferences) pref;
        dbPref.putSetIntoByteArray(key, statistics, set);
    }

    protected static Set<String> getSet(Statistics statistics, Preferences pref, String key) throws BackingStoreException {
        assert pref instanceof DbPreferences;
        DbPreferences dbPref = (DbPreferences) pref;
        return dbPref.readSetFromByteArray(key, statistics);
    }

    protected static void putMap(Statistics statistics, Preferences pref, String key, Map<String, Integer> map) throws BackingStoreException {
        assert pref instanceof DbPreferences;
        DbPreferences dbPref = (DbPreferences) pref;
        dbPref.putMapIntoByteArray(key, statistics, map);
    }

    protected static Map<String, Integer> getMap(Statistics statistics, Preferences pref, String key) throws BackingStoreException {
        assert pref instanceof DbPreferences;
        DbPreferences dbPref = (DbPreferences) pref;
        return dbPref.readMapFromByteArray(key, statistics);
    }
}
