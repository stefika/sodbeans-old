/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * Copyright 2007 Sun Microsystems, Inc. All rights reserved.
 */

package org.netbeans.server.uihandler;

import java.util.Collections;
import java.util.concurrent.ExecutionException;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;

/** A JSP tag to initialize data from a statistic. Locates given statistic
 * and associates its values to the page context.
 *
 * @author Jaroslav Tulach
 */
public final class StatisticsTag extends SimpleTagSupport {
    private String statistic;

    /** Name of the statistic to initialize
     * @param statistic name of the statistic
     */
    public void setName(String statistic) {
        this.statistic = statistic;
    }
    
    /**
    * Registers the beans.
    */
    @Override
    public void doTag() throws JspException {
        try {
            LogsManager logsManager = LogsManager.getDefault();
            PageContext context = (PageContext) getJspContext();
            if (context.getRequest().getParameter("submit_netbeans_version") != null){
                context.getSession().setAttribute("netbeans_version", context.getRequest().getParameter("netbeans_version"));
            }
            context.getRequest().setAttribute("stat_versions", logsManager.getPossibleVersions());

            logsManager.preparePageContext(context, Collections.singleton(statistic));

        } catch (InterruptedException ex) {
            throw new JspException(ex);
        } catch (ExecutionException ex) {
            throw new JspException(ex);
        }
    }

}