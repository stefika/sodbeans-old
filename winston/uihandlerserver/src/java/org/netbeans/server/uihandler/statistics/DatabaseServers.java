/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * Copyright 2008 Sun Microsystems, Inc. All rights reserved.
 */

package org.netbeans.server.uihandler.statistics;

import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.TreeMap;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;
import javax.servlet.jsp.PageContext;
import org.netbeans.server.uihandler.Statistics;
import org.netbeans.server.uihandler.statistics.DatabaseServers.Server;
import org.openide.util.lookup.ServiceProvider;

/**
 *
 * @author Andrei Badea
 */
@ServiceProvider(service=Statistics.class)
public class DatabaseServers extends Statistics<Map<Server, Integer>>{
    
    private static Logger LOG = Logger.getLogger(DatabaseServers.class.getName());

    public DatabaseServers() {
        super(1);
    }

    @Override
    protected Map<Server, Integer> newData() {
        return Collections.emptyMap();
    }
    
    private Map<Server, Integer> newMutableData() {
        return new EnumMap<Server, Integer>(Server.class);
    }

    @Override
    protected Map<Server, Integer> process(LogRecord rec) {
        Map<Server, Integer> result = newMutableData();
        if (!"UI_CONNECT_DB".equals(rec.getMessage())) {
            return result;
        }
        Object[] params = rec.getParameters();
        if (params != null && params.length > 0) {
            String clazz = params[0].toString();
            result.put(Server.forDriverClass(clazz), 1);
        }
        return result;
    }

    @Override
    protected Map<Server, Integer> finishSessionUpload(String userId, int sessionNumber, boolean initialParse, Map<Server, Integer> d) {
        return d;
    }

    @Override
    protected Map<Server, Integer> join(Map<Server, Integer> one, Map<Server, Integer> two) {
        // Do not use EnumSet.copyOf() when not sure if argument is non-empty.
        Set<Server> servers = EnumSet.noneOf(Server.class);
        servers.addAll(one.keySet());
        servers.addAll(two.keySet());
        Map<Server, Integer> result = newMutableData();
        for (Server server : servers) {
            Integer value1 = one.get(server);
            Integer value2 = two.get(server);
            int value = (value1 != null ? value1 : 0) + (value2 != null ? value2 : 0);
            result.put(server, value);
        }
        return result;
    }

    @Override
    protected void write(Preferences pref, Map<Server, Integer> d) throws BackingStoreException {
        for (Map.Entry<Server, Integer> entry : d.entrySet()) {
            pref.putInt(entry.getKey().toString(), entry.getValue());
        }
    }

    @Override
    protected Map<Server, Integer> read(Preferences pref) throws BackingStoreException {
        Map<Server, Integer> result = newMutableData();
        for (String name : pref.keys()) {
            Server server;
            try {
                server = Server.valueOf(name);
            } catch (IllegalArgumentException e) {
                LOG.log(Level.SEVERE, "Skipping unknown server {0}", name);
                continue;
            }
            result.put(server, pref.getInt(name, 0));
        }
        return result;
    }

    @Override
    protected void registerPageContext(PageContext page, String name, Map<Server, Integer> data) {
        Map<Server, Integer> percentages = new EnumMap<Server, Integer>(Server.class);
        long total = 0L;
        for (Map.Entry<Server, Integer> entry : data.entrySet()) {
            Integer value = percentages.get(entry.getKey());
            if (value == null) {
                value = 0;
            }
            value += entry.getValue();
            total += value;
            percentages.put(entry.getKey(), value);
        }
        for (Map.Entry<Server, Integer> entry : percentages.entrySet()) {
            Integer value = entry.getValue();
            if (value != null) {
                entry.setValue((int) (value * 100 / total));
            }
        }
        Map<String, Integer> result = new TreeMap<String, Integer>();
        for (Map.Entry<Server, Integer> entry : percentages.entrySet()) {
            Integer value = entry.getValue();
            if (value > 0) {
                result.put(entry.getKey().getDisplayName(), value);
            }
        }
        page.setAttribute(name, result);
    }

    public enum Server {
        
        DERBY("Derby and Java DB", 
                "org.apache.derby.jdbc.ClientDriver",
                "org.apache.derby.jdbc.EmbeddedDriver"
        ),
        MYSQL("MySQL", 
                "com.mysql.jdbc.Driver", 
                "org.gjt.mm.mysql.Driver"
        ),
        POSTGRESQL("PostgreSQL", 
                "postgresql.Driver",                                 // PostgreSQL 6.5 and earlier.
                "org.postgresql.Driver"
        ),
        ORACLE("Oracle", 
                "oracle.jdbc.OracleDriver",
                "oracle.jdbc.driver.OracleDriver"
        ),
        MSSQL("MS SQL Server", 
                "net.sourceforge.jtds.jdbc.Driver",                  // jTDS.
                "com.microsoft.jdbc.sqlserver.SQLServerDriver",      // SQL Server 2000.
                "com.microsoft.sqlserver.jdbc.SQLServerDriver"       // SQL Server 2005.
        ),
        DB2("DB2",
                "COM.ibm.db2.jdbc.app.DB2Driver",
                "COM.ibm.db2.jdbc.net.DB2Driver",
                "com.ibm.db2.jcc.DB2Driver"
        ),
        JDBCODBC("Other (JDBC-ODBC bridge)",
                "sun.jdbc.odbc.JdbcOdbcDriver"
        ),
        HSQLDB("HSQLDB",
                "org.hsqldb.jdbcDriver"),
        OTHER("Other");
        
        private static Map<String, Server> class2Server = new HashMap<String, Server>();
        
        private final String displayName;
        private final String[] classes;
        
        static {
            for (Server server : Server.values()) {
                for (String clazz : server.classes) {
                    class2Server.put(clazz, server);
                }
            }
        }
        
        public static Server forDriverClass(String clazz) {
            Server server = class2Server.get(clazz);
            if (server == null) {
                LOG.log(Level.FINE, "Unknown driver class {0}", clazz);
                server = OTHER;
            }
            return server;
        }

        Server(String displayName, String... classes) {
            this.displayName = displayName;
            this.classes = classes;
        }

        public String getDisplayName() {
            return displayName;
        }
    }
}
