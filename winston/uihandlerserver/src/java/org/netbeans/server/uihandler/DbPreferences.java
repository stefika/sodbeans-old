/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * Portions Copyrighted 2007 Sun Microsystems, Inc.
 */

package org.netbeans.server.uihandler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.prefs.AbstractPreferences;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.netbeans.modules.exceptions.entity.Logfile;
import org.netbeans.modules.exceptions.entity.Preference;
import org.netbeans.modules.exceptions.entity.Prefname;
import org.netbeans.modules.exceptions.entity.Prefset;
import org.netbeans.modules.exceptions.entity.Statistic;
import org.netbeans.modules.exceptions.utils.PersistenceUtils;

/** A preferences that do store objects into database.
 *
 * @author Jaroslav Tulach
 * @author Jindrich Sedek
 */
public final class DbPreferences extends AbstractPreferences {
    private Statistic statistic;
    private Logfile log;
    private String prefix;
    private EntityManager persist;
    private transient volatile List<Preference> preferences;

    private static final int BYTES_IN_INT = Integer.SIZE / Byte.SIZE;
    private static final int MAX_BYTE_ARRAY_ITEMS = Preferences.MAX_VALUE_LENGTH * 3/4 / BYTES_IN_INT;
    private static final int MAX_BYTE_ARRAY_LENGTH = MAX_BYTE_ARRAY_ITEMS * BYTES_IN_INT ;
    private static final int DEFAULT_CACHE_SIZE = 1000;
    private static final Map<String, Map<Integer, Integer> > prefSetCache = 
            new HashMap<String, Map<Integer, Integer>>(4);
    private static final Map<String, Map<Integer, String> > indexCache = 
            new HashMap<String, Map<Integer, String>>(4);
    
    private DbPreferences(
        AbstractPreferences parent, String name, 
        Statistic statistic, Logfile log, String prefix,
        EntityManager em
    ) {
        super(parent, name);
        this.statistic = statistic;
        this.log = log;
        this.prefix = prefix;
        this.persist = em;
    }

    public static Preferences root(Logfile log, Statistics<?> stat, EntityManager em) {
        Statistic statist = Statistic.getExists(stat.name);
        return new DbPreferences(null, "", statist, log, null, em);
    }

    protected void putSpi(String key, String value) {
        Preference p = null;
        for (Preference current : getPreferences()) {
            if (key.equals(current.getKey())) {
                p = current;
                p.setValue(persist, value);
                break;
            }
        }
        if (p == null) {
            p = new Preference(persist, statistic, log, prefix, key);
            p.setValue(persist, value);
            preferences.add(p);
            persist.persist(p);
        } else {
            persist.merge(p);
        }
    }

    protected String getSpi(String key) {
        for (Preference p : getPreferences()) {
            if (key.equals(p.getKey())) {
                    return p.getValue();
                }
            }
        return null;
    }

    void putSetIntoByteArray(String key, Statistics statistics, Set<String> set) throws BackingStoreException {
        byte[] array = new byte[MAX_BYTE_ARRAY_LENGTH];
        for (String str : set) {
            Integer index = getIndex(persist, statistics.name, str);
            array[index] = 1;
        }
        try {
            putByteArray(key, array);
        } catch (IllegalArgumentException exc) {
            throw new BackingStoreException(exc);
        }
    }

    protected Set<String> readSetFromByteArray(String key, Statistics statistics) throws BackingStoreException {
        byte[] array = getByteArray(key, null);
        Set<String> result = new HashSet<String>();
        if (array != null) {
            for (int index = 0; index < array.length; index++) {
                if (array[index] != 0) {
                    result.add(getValue(persist, statistics.name, index));
                }
            }
        }
        return result;
    }

    void putMapIntoByteArray(String key, Statistics statistics, Map<String, Integer> map) throws BackingStoreException {
        byte[] data = new byte[MAX_BYTE_ARRAY_LENGTH];
        for (Entry<String, Integer> entry : map.entrySet()) {
            Integer index = getIndex(persist, statistics.name, entry.getKey());
            byte[] valueArray =  integerToByteArray(entry.getValue());
            int possition = index * BYTES_IN_INT;
            if (possition >= MAX_BYTE_ARRAY_LENGTH){
                throw new BackingStoreException("VALUE ARRAY IS TWO LONG");
            }
            System.arraycopy(valueArray, 0, data, possition, BYTES_IN_INT);
        }
        try {
            putByteArray(key, data);
        } catch (IllegalArgumentException exc) {
            throw new BackingStoreException(exc);
        }
    }

    Map<String, Integer> readMapFromByteArray(String key, Statistics statistics) throws BackingStoreException {
        Map<String, Integer> result = new HashMap<String, Integer>();
        byte[] array = getByteArray(key, null);
        if (array != null) {
            Statistic stat = Statistic.getExists(statistics.name);
            for (int index = 1; index <= Prefset.getMaxIndex(persist, stat); index++) {
                String mapKey = getValue(persist, statistics.name, index);
                int possition = index * BYTES_IN_INT;
                byte[] valueArray = new byte[BYTES_IN_INT];
                System.arraycopy(array, possition, valueArray, 0, BYTES_IN_INT);
                Integer value = byteArrayToInteger(valueArray);
                if (value != 0){
                    result.put(mapKey, value);
                }
            }
        }
        return result;
    }

    private static String getValue(EntityManager em, String statName, Integer index)throws BackingStoreException {
        Map<Integer, String> cache = getIndexCache(statName);
        String result = cache.get(index);
        if (result == null){    
            Query query = em.createNamedQuery("Prefset.findByStatisticAndIndex");
            Statistic stat = Statistic.getExists(statName);
            query.setParameter("statistic", stat.getId());
            query.setParameter("index", index);
            Prefset prefset = (Prefset) query.getSingleResult();
            result = prefset.getPrefname().getName();
        }
        return result;
    }
    
    private static Map<Integer, String> getIndexCache(String statName){
        Map<Integer, String> map = indexCache.get(statName);
        if (map == null){
            map = new HashMap<Integer, String>(DEFAULT_CACHE_SIZE);
            indexCache.put(statName, map);
        }
        return map;
    }

    private static Integer getIndex(EntityManager em, String statName, String name) throws BackingStoreException {
        Map<Integer, Integer> cache = getCache(statName);
        Integer hashCode = name.hashCode();
        Integer index = cache.get(hashCode);
        if (index == null) {
            Statistic stat = Statistic.getExists(statName);
            index = Prefset.getByteArrayIndex(em, stat, name);
            if (cache.containsKey(hashCode)){
                throw new BackingStoreException("HashCode collision for " + name);
            }
            cache.put(hashCode, index);
            getIndexCache(statName).put(index, name);
        }
        return index;
    }
    
    private static Map<Integer, Integer> getCache(String statName){
        Map<Integer, Integer> map = prefSetCache.get(statName);
        if (map == null){
            map = new HashMap<Integer, Integer>(DEFAULT_CACHE_SIZE);
            prefSetCache.put(statName, map);
        }
        return map;
    }

    private static byte[] integerToByteArray(Integer value){
        byte[] result = new byte[BYTES_IN_INT];
        for (int i = 0; i < BYTES_IN_INT; i++){
            result[i] = (byte) (value / (1 << i * Byte.SIZE));
        }
        return result;
    }
    
    private static Integer byteArrayToInteger(byte[] array){
        int result = 0;
        for (int i = BYTES_IN_INT - 1; i >= 0; i--){
            int addition = array[i];
            if (addition < 0) {
                addition = addition + 256;
            }
            result <<= Byte.SIZE;
            result += addition; 
        }
        return result;
    }
    
    /** Loads current preferences from the database.
     * @return always non-null list of objects
     */
    private List<Preference> getPreferences() {
        List<Preference> prefs = preferences;
        if (prefs == null) {
            prefs = new ArrayList<Preference>();
            Query q;
            if (prefix != null){
                q = persist.createNamedQuery("Preference.findWithoutKey").
                        setParameter("prefix", Prefname.getExistsOrNew(persist, prefix)).
                        setParameter("statistic", statistic).
                        setParameter("logfile", log);
            } else {
                q = persist.createNamedQuery("Preference.findWithoutKeyNullPrefix").
                        setParameter("statistic", statistic).
                        setParameter("logfile", log);
            }
            for (Object o : q.getResultList()) { // NOI18N
                Preference p = (Preference) o;
                prefs.add(p);
            }

            preferences = new CopyOnWriteArrayList<Preference>(prefs);
        }
        return prefs;
    }

    protected void removeSpi(String key) {
        for (Preference p : getPreferences()) {
            if (key.equals(p.getKey())) {
                persist.remove(p);
                getPreferences().remove(p);
            }
        }
    }

    protected void removeNodeSpi() throws BackingStoreException {
        for (Preference p : getPreferences()) { // NOI18N
            persist.remove(p);
        }
        preferences = null;
    }

    protected String[] keysSpi() throws BackingStoreException {
        List<String> keys = new ArrayList<String>();
        for (Preference p : getPreferences()) {
            keys.add(p.getKey());
        }
        
        return keys.toArray(new String[0]);
    }

    protected String[] childrenNamesSpi() throws BackingStoreException {
        Map<String, Object> params = new HashMap<String, Object>(4);
        if (prefix != null){
            params.put("prefix", this.prefix + "%");
        }else{
            params.put("prefix", "%");
        }
        params.put("statistic", statistic);
        params.put("logfile", this.log);
        List<Preference> resultList = PersistenceUtils.executeNamedQuery(persist, "Preference.findAllPrefixes", params, Preference.class);
        Set<String> subnames = new LinkedHashSet<String>();
        for (Preference p : resultList) { // NOI18N
            
            String n;
            if (prefix != null){
                assert p.getPrefix().getName().startsWith(this.prefix);
                n = p.getPrefix().getName().substring(this.prefix.length());
            }else{
                n = p.getPrefix().getName();
            }
            int idx = n.indexOf('/');
            if (idx > 0) {
                n = n.substring(0, idx);
            }
            if (n.length() == 0) {
                continue;
            }
            subnames.add(n);
        }
        
        return subnames.toArray(new String[0]);
        
    }

    protected AbstractPreferences childSpi(String name) {
        if (prefix != null){
            return new DbPreferences(this, name, statistic, log, prefix + name + '/', persist);
        }else{
            return new DbPreferences(this, name, statistic, log, name + '/', persist);
        }
    }

    protected void syncSpi() throws BackingStoreException {
    }

    protected void flushSpi() throws BackingStoreException {
        boolean active = persist.getTransaction().isActive();
        assert active;
        if (!active) {
            persist.getTransaction().begin();
        }
        persist.flush();
        if (!active) {
            persist.getTransaction().commit();
        }
    }

}
