/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */


package org.netbeans.server.uihandler;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.logging.Level;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import org.netbeans.modules.exceptions.utils.PersistenceUtils;
import org.netbeans.modules.exceptions.utils.SynchronizeIssues;
import org.netbeans.web.mail.BugsNoWatchDog;
import org.openide.util.RequestProcessor;

/**
 *
 * @author Jindrich Sedek
 */
public class OnDeploy implements ServletContextListener{

    /** Creates a new instance of OnDeploy */
    public OnDeploy() {
    }
    
    public void contextInitialized(ServletContextEvent ev) {
        try {
            initialize();
        } catch (Throwable t) {
            LogsManager.LOG.log(Level.SEVERE, t.getMessage(), t);
        }
    }
    
    private void initialize() {
        System.setProperty("java.awt.headless", "true");
        if (LogsManager.LOG.isLoggable(Level.INFO)) {
            try {
                LogsManager.LOG.warning("printing bindings"); // NOI18N
                Utils.printContext("", null);
            } catch (Exception ex) {
                LogsManager.LOG.log(Level.SEVERE, ex.getMessage(), ex);
            } finally {
                LogsManager.LOG.warning("end of bindings"); // NOI18N
            }
        }

        String persistenceUnit = Utils.getVariable("pu", String.class);
        if (persistenceUnit != null) {
            PersistenceUtils.setDefault(persistenceUnit);
        }
        
        String settingsDir = Utils.getVariable("settings", String.class);
        if (settingsDir != null) {
            PersistenceUtils.setSettingsDirectory(settingsDir);
        }
        
        String privateKeyFileName = Utils.getVariable("privKey", String.class);
        File privateKeyFile = new File(settingsDir, privateKeyFileName);
        try{
            FileInputStream inputStream = new FileInputStream(privateKeyFile);
            Authenticate.loadPrivateKey(inputStream);
        }catch(IOException exc){
            LogsManager.LOG.log(Level.SEVERE, "Impossible to open privKey file", exc);
        }
        
        String dir = Utils.getVariable("dir", String.class);
        if (dir == null){
            LogsManager.LOG.severe("Analytics dir is not set correctly");
            return;
        }
        
        try {
            LogsManager.getDefault();
        } catch (Exception ex) {
            LogsManager.LOG.log(Level.SEVERE, "Failure initializing data", ex);
        }
        LogsManager.LOG.finer("contextInitialized");
        
        SynchronizeIssues.startSynchronization();
        BugsNoWatchDog.start();
    }
    
    public void contextDestroyed(ServletContextEvent ev) {
        LogsManager.LOG.finer("contextDestroyed");
        LogsManager.closeInstance();
        BugsNoWatchDog.stop();
        RequestProcessor.getDefault().stop();
    }
}
