/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */
package org.netbeans.server.uihandler;

import org.openide.util.NbBundle;
import org.netbeans.server.uihandler.builds.BuildProvider;
import org.netbeans.server.uihandler.api.Issue;
import org.netbeans.server.snapshots.SlownessChecker;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import org.netbeans.modules.exceptions.utils.PersistenceUtils;
import org.netbeans.server.componentsmatch.Component;
import org.netbeans.server.componentsmatch.Matcher;
import org.netbeans.modules.exceptions.entity.*;
import org.netbeans.modules.exceptions.entity.Slowness.SlownessType;
import org.netbeans.modules.exceptions.utils.BugReporterFactory;
import org.netbeans.server.services.beans.Components;
import static org.netbeans.server.uihandler.Utils.*;

/**
 *
 * @author Jindrich Sedek
 */
final class DbInsertion {
    private static final Logger LOG = Logger.getLogger(DbInsertion.class.getName());
    private static final int MAXMESSAGELENGTH = 1024;
    private static final int CHANGESET_RADIX = 16;

    private static final int CHANGESET_IDX = 5;
    private static final String UNKNOWN = "unknown";// NOI18N
    private final LogRecord userMetaData;
    private final LogRecord buildInfo;
    private final Throwable thrown;
    private final LogFileTask thisTask;
    private final Long slownessTime;
    private final SlownessType slownessType;
    private final String latestAction;
    private Integer submitId;
    private Report duplicatesReport;
    private PreparedParams preparedParams;

    DbInsertion(LogRecord data, Throwable thrown, LogRecord buildInfo, Long slownessTime, SlownessType slownessType, String latestAction) {
        this(data, thrown, buildInfo, null, slownessTime, slownessType, latestAction);
    }

    DbInsertion(LogRecord data, Throwable thrown, LogFileTask task) {
        this(data, thrown, null, task, null, null, null);
    }
    
    DbInsertion(LogRecord data, Throwable thrown, LogRecord buildInfo, LogFileTask task) {
        this(data, thrown, buildInfo, task, null, null, null);
    }
    
    DbInsertion(LogRecord data, Throwable thrown, LogRecord buildInfo, LogFileTask task, Long slownessTime, SlownessType slownessType, String latestAction) {
        this.thrown = thrown;
        this.userMetaData = data;
        this.buildInfo = buildInfo;
        this.slownessTime = slownessTime;
        this.latestAction = latestAction;
        this.slownessType = slownessType;
        if (task == null) {
            this.thisTask = LogFileTask.getRunningTask();
        } else {
            this.thisTask = task;
        }
    }

    /** issueId is an ID of newly created issue if not duplicate
     *  if duplicate issueId is an ID of issue to be duplicate of
     *  issuezillaId is 0 if not assigned, otherwise an issuezilla Id
     * @return {int issueId, bool isDuplicate, int issuezillaId} or null if it's not an error report
     *
     */
    public void start(ExceptionsData data) {
        long time = System.currentTimeMillis();
        preparedParams = PreparedParams.prepare(userMetaData.getParameters(), thrown);
        submitId = getNextId(org.netbeans.modules.exceptions.entity.Submit.class);
        data.setSubmitId(submitId);
        insertLogFileInformation();
        Submit submit = null;
        if (slownessTime != null){
            data.setDataType(ExceptionsData.DataType.SLOWNESS_REPORT);
            SlownessChecker checker = new SlownessChecker(thisTask.getTaskEntityManager(), latestAction, slownessType, thisTask.getLogInfo());
            duplicatesReport = checker.checkSlowness();
            submit = insertSlownessData(checker);
        } else if (thrown != null) {
            data.setDataType(ExceptionsData.DataType.EXC_REPORT);
            duplicatesReport = Utils.checkIssue(thisTask.getTaskEntityManager(), thrown);
            submit = insertException();
        } else {// only a log file insertion
            data.setDataType(ExceptionsData.DataType.STATISTIC);
        }
        if (submit != null){
            Integer beforeReopenId = null;
            if (duplicatesReport != null){
                beforeReopenId = duplicatesReport.getId();
            }
            boolean reopen = checkDuplicateReportForReopen(submit);
            if (!reopen){
                new IZInsertion(submit, preparedParams.getUserToken()).start();
            } else {
                data.setReportBeforeReopenId(beforeReopenId);
            }
        }
        if (duplicatesReport != null) {
            data.setIssuezillaId(duplicatesReport.getIssueId());
            data.setReportId(duplicatesReport.getId());
        }
        if (!isGuest(preparedParams.getUserName())) {
            data.setUserName(preparedParams.getUserName());
        }
        time = System.currentTimeMillis() - time;
        LOG.log(Level.FINE, "submit db insertion finished in {0}ms", time);  // NOI18N
    }

    private boolean checkDuplicateReportForReopen(Submit sbmt){
        if (!sbmt.getReportId().isInIssuezilla()){
            LOG.fine("Not in issuezilla");
            return false;
        }
        Integer issueId = sbmt.getReportId().getIssueId();
        Issue issue = BugReporterFactory.getDefaultReporter().getIssue(issueId);
        if (issue == null){
            LOG.log(Level.SEVERE, "Impossible to find issue for issueId = {0}", issueId);
            return false;
        } else {
            LOG.fine("Issue is not null");
        }
        if ((issue.isFixed()||issue.isWorksForMe()) && !Utils.isGuest(sbmt.getNbuserId().getName())) {
            if (sbmt.getCommentCollection() == null || sbmt.getCommentCollection().isEmpty()){
                LOG.fine("No comments");
                return false;
            }
            Long fixedBuildNumber = BuildProvider.getInstance().getBuild(issueId);
            if (org.netbeans.modules.exceptions.utils.Utils.isDailyBuild(sbmt.getLogfileId().getProductVersionId()) && fixedBuildNumber != null){
                Long excBuildNumber = sbmt.getLogfileId().getBuildnumber();
                if ((excBuildNumber != null) && (fixedBuildNumber <= excBuildNumber)){
                    Object[] params = new Object[]{sbmt.getId(), excBuildNumber, fixedBuildNumber};
                    LOG.log(Level.INFO, "Calling reopen of issue {0} excBuild: {1}; fixed build: {2}", params);
                    handleReopen(sbmt);
                    return true;
                }
            }
            Date date = issue.getLastResolutionChange();
            if ((date != null) && (sbmt.isBuildNewerThan(date)) && isIssueTMBeforeExcProductVersion(sbmt, issue)){
                Object[] params = new Object[]{sbmt.getId(), sbmt.getBuild(), date, sbmt.getLogfileId().getProductVersionId().getProductVersion(), issue.getTMNumber()};
                LOG.log(Level.INFO, "Calling reopen of issue {0} excBuild: {1}; date: {2} ; excVersion: {3}; issueTM: {4}", params);
                handleReopen(sbmt);
                return true;
            } else {
                LOG.log(Level.FINE, "Is not newer {0}, {1},{2}", new Object[]{date, issue.getTMNumber(), issue.getTargetMilestone()});
            }
        } else {
            LOG.fine("Not fixed or is guest");
        }
        return false;
    }

    private void handleReopen(Submit submit){
        LOG.log(Level.INFO, "Trying to reopen issue exc id: {0}", submit.getId());
        int issuezillaId = submit.getReportId().getIssueId();
        EntityManager em = thisTask.getTaskEntityManager();
        Submit afterSplit = ReopenHandler.createHandler(submit).process(em);
        em.getTransaction().commit(); // commit reopen handler changes for IZ Insertion
        em.getTransaction().begin();
        duplicatesReport = afterSplit.getReportId();
        LOG.log(Level.INFO, "Trying to report after reopen split the exception id: {0}", submit.getId());
        String reopenMessage = NbBundle.getMessage(IZInsertion.class, "originally_reported", Integer.toString(issuezillaId));
        new IZInsertion(afterSplit, preparedParams.getUserToken(), null, reopenMessage).startReopenInsert();
    }

    private void insertLogFileInformation() {
        LOG.fine("Inserting logfile information into DB");   // NOI18N
        EntityManager em = thisTask.getTaskEntityManager();
        ProductVersion productVersion = null;

        if (preparedParams.getVersion() != null) {
            productVersion = getExistProductVersion(em, preparedParams.getVersion());
        } else {
            List<ProductVersion> list = PersistenceUtils.executeQuery(em, "SELECT p FROM ProductVersion p WHERE p.productVersion is null", null);
            if (!list.isEmpty()) {
                productVersion = list.get(0);
            }
        }
        if (productVersion == null) {
            productVersion = new ProductVersion(getNextId(ProductVersion.class));
            productVersion.setProductVersion(preparedParams.getVersion());
            String nbVersionName = org.netbeans.modules.exceptions.utils.Utils.getNbVersion(preparedParams.getVersion());
            Nbversion nbVersion = getExistNbVersion(em, nbVersionName);
            if (nbVersion == null) {
                nbVersion = new Nbversion(getNextId(Nbversion.class));
                nbVersion.setVersion(nbVersionName);
                em.persist(nbVersion);
            }
            productVersion.setNbversionId(nbVersion);
            em.persist(productVersion);
        }
        Long changeset = parseBuildInfo(buildInfo);
        Logfile logFile = thisTask.getLogInfo();
        assert logFile != null;
        logFile.setProductVersionId(productVersion);
        logFile.setChangeset(changeset);
        logFile.setBuildnumber(preparedParams.getBuildNumber());
        logFile = em.merge(logFile);
    }

    private Nbuser prepareNBUser(EntityManager em){
        Nbuser nbuser = getExistUser(em, preparedParams.getUserName());
        if (nbuser == null) {//Adding NbUser into DB
            nbuser = createUser(em, preparedParams.getUserName());
        }
        return nbuser;
    }

    private Component doComponentChecking(EntityManager em){
        // get component from comment
        Component comp = getComponentFromComment(preparedParams.getComment());
        // use unknown component for STACK OVERFLOW ISSUE 112156
        if ((comp == null) && (thrown.getMessage().contains("StackOverflowError"))) {// NOI18N
            comp = Component.UNKNOWN;
        }else if (comp == null) {
            comp = Matcher.getDefault().match(em, thrown);
        }
        return comp;
    }

    private Nbuser createUser(EntityManager em, String userName) {
        Nbuser nbuser = new Nbuser(getNextId(Nbuser.class));
        nbuser.setName(userName);
        em.persist(nbuser);
        return nbuser;
    }

    protected static Component getComponentFromComment(String comment) {
        int startIndex = comment.indexOf("[");
        int slashIndex = comment.indexOf("/");
        int endIndex = comment.indexOf("]");
        int newLineIndex = comment.indexOf("\n");
        if (newLineIndex == -1) {
            newLineIndex = comment.length();
        }
        if ((startIndex == 0) && (startIndex < slashIndex) &&
                (slashIndex < endIndex) && (endIndex < newLineIndex)) {
            String comp = comment.substring(1, slashIndex);
            String subcomp = comment.substring(slashIndex + 1, endIndex);

            Components realComps = PersistenceUtils.getInstance().getComponents();
            if (realComps.getComponentsSet().contains(comp) && realComps.getSubcomponentsSet(comp).contains(subcomp)) {
                return new Component(comp, subcomp);
            }
        }
        return null;
    }

    private Submit insertSlownessData(SlownessChecker checker) {
        EntityManager em = thisTask.getTaskEntityManager();

        Nbuser user = prepareNBUser(em);
        Slowness sn = new Slowness(submitId);
        sn.setSummary(preparedParams.getSummary());
        sn.setLogfileId(thisTask.getLogInfo());
        sn.setVmId(getVm(em, preparedParams.getVm()));
        sn.setOperatingsystemId(getOperatingsystem(em, preparedParams.getOs()));
        sn.setNbuserId(user);
        sn.setReportdate(new Date());
        sn.setLatestAction(latestAction);
        sn.setActionTime(slownessTime);
        sn.setSlownessType(slownessType);
        String fullMethodName = checker.getSuspiciousMethodName();
        Method method = getExistingMethod(em, fullMethodName);
        if (method == null){
            method = new Method();
            method.setId(getNextId(Method.class));
            method.setName(fullMethodName);
            em.persist(method);
        }
        sn.setSuspiciousMethod(method);
        Report report = prepareReport(em, sn);

        // get component from comment
        Component comp = getComponentFromComment(preparedParams.getComment());
        if (comp == null){
            comp = checker.getComponentForSlowness();
        }
        if (comp == null || comp.equals(Component.UNKNOWN)) {
            comp = Component.UNKNOWN_SLOWNESS;
        }
        report.setComponent(comp);
        sn.setReportId(report);
        em.persist(sn);
        em.merge(report);
        sn = mergeComment(em, sn, user);

        em.getTransaction().commit();
        em.getTransaction().begin();
        return sn;
    }

    private <T extends Submit> T mergeComment(EntityManager em, T submit, Nbuser user){
        // adding comment
        if (!preparedParams.getComment().isEmpty()){
            Comment comment = new Comment();
            comment.generateId();
            comment.setCommentDate(new Date());
            comment.setSubmitId(submit);
            comment.setNbuserId(user);
            comment.setComment(preparedParams.getComment());
            em.persist(comment);
            Collection<Comment> commentList = new ArrayList<Comment>();
            commentList.add(comment);
            submit.setCommentCollection(commentList);
            return em.merge(submit);
        }else{
            return submit;
        }
    }

    private Report prepareReport(EntityManager em, Submit submit){
        if (duplicatesReport != null) {
            duplicatesReport.preloadSubmitCollection(em);
            duplicatesReport.getSubmitCollection().add(submit);
            return duplicatesReport;
        } else {
            Report report = createReport(em);
            List<Submit> list = new ArrayList<Submit>();
            list.add(submit);
            report.setSubmitCollection(list);
            return report;
        }
    }
    
    private Submit insertException() {
        LOG.log(Level.FINE, "Inserting exception into DB {0}", Integer.toString(submitId));   // NOI18N
        final EntityManager em = thisTask.getTaskEntityManager();

        // adding stacktrace
        Stacktrace stacktrace = addStackTrace(em, thrown);
        Nbuser nbuser = prepareNBUser(em);
        
        org.netbeans.modules.exceptions.entity.Exceptions exc = new org.netbeans.modules.exceptions.entity.Exceptions();
        Logfile logFile = thisTask.getLogInfo();
        exc.setId(submitId);
        exc.setSummary(preparedParams.getSummary());
        exc.setStacktrace(stacktrace);
        exc.setLogfileId(logFile);
        exc.setVmId(getVm(em, preparedParams.getVm()));
        exc.setOperatingsystemId(getOperatingsystem(em, preparedParams.getOs()));
        exc.setBuild(logFile.getBuildnumber());
        exc.setNbuserId(nbuser);
        Report report = prepareReport(em, exc);
        Component comp = doComponentChecking(em);
        report.setComponent(comp);
        exc.setReportId(report);
        exc.setReportdate(new Date());
        em.persist(exc);
        em.merge(report);
        stacktrace.setExceptions(exc);
        stacktrace = em.merge(stacktrace);

        exc = mergeComment(em, exc, nbuser);

        //add hashCode
        Hashcodes hashcode = new Hashcodes(submitId);
        hashcode.setCode(countHash(thrown.getStackTrace()));
        hashcode.setExceptions(exc);
        em.persist(hashcode);
        exc.setHashcodes(hashcode);
        em.merge(exc);

        em.getTransaction().commit();
        em.getTransaction().begin();
        assert exc.getCommentCollection() != null;
        return exc;
    }

    private Stacktrace addStackTrace(EntityManager em, Throwable thrown) {
        Stacktrace stacktrace = new Stacktrace();
        Line line;
        LinePK linePK;
        Method method;
        Jarfile jarFile;

        int stackTraceId = getNextId(Stacktrace.class);
        Stacktrace cause = null;

        if (thrown.getCause() != null) {
            cause = addStackTrace(em, thrown.getCause());//add recursively
        }
        stacktrace.setId(stackTraceId);
        String message = thrown.getMessage();
        String stackClass = message;
        if (message != null) {
            if (message.length() > MAXMESSAGELENGTH) {
                message = message.substring(0, MAXMESSAGELENGTH);
            }
            stacktrace.setMessage(message);
            stackClass = FilteringUtils.getClassOfMessage(message);
            stacktrace.setClass1(stackClass);
        }
        if (cause != null) {
            stacktrace.setAnnotation(cause);
        }
        //add stack trace lines
        em.persist(stacktrace);
        if (cause != null) {
            cause.setStacktrace(stacktrace);
            cause = em.merge(cause);
        }
        Integer lineNumber;
        String linesConcatenation = null;
        Collection<Line> lineCollection = new ArrayList<Line>(thrown.getStackTrace().length);
        for (int i = 0; i < thrown.getStackTrace().length; i++) {
            StackTraceElement element = thrown.getStackTrace()[i];
            String jarFileName = element.getFileName();
            if ("".equals(jarFileName)) {
                jarFileName = null;
            }
            if (linesConcatenation == null) {
                linesConcatenation = Method.getMethodNameFromSTEWithoutNumbers(element);
            } else {
                linesConcatenation = linesConcatenation.concat(Method.getMethodNameFromSTEWithoutNumbers(element));
            }
            String fullMethodName = Method.getMethodNameFromSTE(element);
            method = getExistingMethod(em, fullMethodName);
            if (method == null) {//adding method into table
                method = new Method();
                method.setId(getNextId(Method.class));
                method.setName(fullMethodName);
                if (jarFileName != null) {
                    method.setSourcejar(getSourceJar(em, jarFileName));
                }
                em.persist(method);
            } else {
                if ((method.getSourcejar() == null) && (jarFileName != null)) {
                    method.setSourcejar(getSourceJar(em, jarFileName));
                    method = em.merge(method);
                }
            }
            if (jarFileName == null) {
                jarFileName = UNKNOWN;
            }
            jarFile = getExistingFile(em, jarFileName);
            if (jarFile == null) {
                jarFile = new Jarfile(getNextId(Jarfile.class));
                jarFile.setName(jarFileName);
                em.persist(jarFile);
            }
            if (element.getLineNumber() >= 0) {
                lineNumber = element.getLineNumber();
            } else {
                lineNumber = 0;
            }
            line = new Line();
            linePK = new LinePK();
            linePK.setLinenumber(lineNumber);
            linePK.setMethodId(method.getId());
            linePK.setStacktraceId(stackTraceId);
            linePK.setLineOrder(i);
            line.setStacktrace(stacktrace);
            line.setMethod(method);
            line.setLineHashcode(linesConcatenation.hashCode());
            lineCollection.add(line);
            line.setJarfileId(jarFile);
            line.setLinePK(linePK);
            line.setGeneratecomponent(0);
            em.persist(line);
        }
        stacktrace.setLineCollection(lineCollection);
        stacktrace = em.merge(stacktrace);
        return stacktrace;
    }

    private static Sourcejar getSourceJar(EntityManager em, String jarFileName) {
        String[] parts = jarFileName.split("/");
        String jarName = parts[parts.length - 1];
        Sourcejar sjar = (Sourcejar) PersistenceUtils.getExist(em, "Sourcejar.findByName", jarName);
        if (sjar == null) {
            sjar = new Sourcejar(Utils.getNextId(Sourcejar.class));
            sjar.setName(jarName);
            em.persist(sjar);
        }
        return sjar;
    }

    private Report createReport(EntityManager em) {
        Report report = new Report();
        report.setId(getNextId(Report.class));
        em.persist(report);
        return report;
    }

    private Operatingsystem getOperatingsystem(EntityManager em, String osName) {
        String [] s = osName.split(",");
        String name = "";
        String version = "";
        String arch = "";
        if (s.length == 3) {
            name = s[0].trim();
            version = s[1].trim();
            arch = s[2].trim();
        } else {
            name = osName;
        }
        Operatingsystem os = Operatingsystem.getExists(em, name, version, arch);
        if (os == null) {
            os = new Operatingsystem();
            os.setId(getNextId(Operatingsystem.class));
            os.setName(name);
            os.setVersion(version);
            os.setArch(arch);
            em.persist(os);
        }
        return os;
    }
    private Vm getVm(EntityManager em, String vmName) {
        Vm vm = (Vm) PersistenceUtils.getExist(em, "Vm.findByName", vmName);
        if (vm == null) {
            vm = new Vm();
            vm.setId(getNextId(Vm.class));
            vm.setName(vmName);
            vm.setShortName(getShortVM(vmName));
            em.persist(vm);
        }
        return vm;
    }

    private String getShortVM(String vm) {
        String result = "";
        if (vm != null) {
            for (int i = 4; i < 8; i++) {
                String v = "1." + i;
                if (vm.contains(v)) {
                    result = v;
                    break;
                }
            }
        }
        return result;
    }

    static Long parseBuildInfo(LogRecord buildInfoRec) {
        if (buildInfoRec != null){
            Object[] infoParams = buildInfoRec.getParameters();
            if ((infoParams != null) && (infoParams.length >= CHANGESET_IDX + 1)){
                String param = infoParams[CHANGESET_IDX].toString();
                if (param.length() == 0){
                    return null;
                }
                int space = param.indexOf(' ');
                if (space != -1){
                    param = param.substring(0, space);
                }
                param = param.replaceAll("\\+", "");
                try {
                    return Long.parseLong(param, CHANGESET_RADIX);
                }catch (NumberFormatException nfe){
                    LOG.log(Level.WARNING, "Wrong changeset format: {0}", infoParams[CHANGESET_IDX].toString());
                }
            }
        }
        return null;
    }

    private Jarfile getExistingFile(EntityManager em, String fileName) {
        return (Jarfile) PersistenceUtils.getExist(em, "Jarfile.findByName", fileName);// NOI18N
    }

    private Method getExistingMethod(EntityManager em, String methodName) {
        return (Method) PersistenceUtils.getExist(em, "Method.findByName", methodName);
    }

    private Nbuser getExistUser(EntityManager em, String userName) {
        return (Nbuser) PersistenceUtils.getExist(em, "Nbuser.findByName", userName);// NOI18N
    }

    private ProductVersion getExistProductVersion(EntityManager em, String versionName) {
        return (ProductVersion) PersistenceUtils.getExist(em, "ProductVersion.findByProductVersion", versionName);// NOI18N
    }

    private Nbversion getExistNbVersion(EntityManager em, String versionName) {
        return (Nbversion) PersistenceUtils.getExist(em, "Nbversion.findByVersion", versionName);// NOI18N
    }
}
