/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2009 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */
package org.netbeans.server.uihandler.statistics;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;
import javax.servlet.jsp.PageContext;
import org.netbeans.server.uihandler.Statistics;
import org.netbeans.server.uihandler.statistics.OSAndJVMTypes.Info;
import org.openide.util.lookup.ServiceProvider;

/**
 *
 * @author Jaroslav Tulach
 */
@ServiceProvider(service=Statistics.class)
public class OSAndJVMTypes extends Statistics<Info> {
    private static final Logger LOG = Logger.getLogger(OSAndJVMTypes.class.getName());
    
    private static final Info EMPTY = new Info(
        Collections.<Env, Integer>emptyMap()
    );
    private static final String MESSAGE = "UI_USER_CONFIGURATION";

    public OSAndJVMTypes() {
        super("OSAndJVM", 2);
    }

    @Override
    protected Info newData() {
        return EMPTY;
    }

    @Override
    protected Info process(LogRecord rec) {
        if (MESSAGE.equals(rec.getMessage())) {
            Object[] params = rec.getParameters();
            if (
                params != null && params.length >= 2 && 
                !params[1].equals("text")
            ) {
                return new Info(params);
            }
        }
        return newData();
    }

    @Override
    protected Info finishSessionUpload(String userId, int sessionNumber, boolean initialParse, Info d) {
        return d;
    }

    @Override
    protected void registerPageContext(PageContext page, String name, Info data) {
        int count = 10;
        String countTxt = (String) page.getAttribute("Count", PageContext.REQUEST_SCOPE); // NOI18N
        if (countTxt != null) {
            count = Integer.parseInt(countTxt);
        }
        
        String includes = (String) page.getAttribute("Includes", PageContext.REQUEST_SCOPE); // NOI18N
        
        Map<String, Integer> res = new HashMap<String, Integer>();
        data.select(res, count, includes);
        
        page.setAttribute(name, res);
    }

    @Override
    protected Info join(Info one, Info two) {
        if (EMPTY.equals(one)){
            return two;
        }
        if (EMPTY.equals(two)){
            return one;
        }
        Map<Env,Integer> env = new HashMap<Env, Integer>(one.env);
        addAllCounts(env, two.env);
        return new Info(env);
    }

    @Override
    protected void write(Preferences pref, Info d) throws BackingStoreException {
        if (d.env != null && !d.env.isEmpty()) {
            for (Map.Entry<Env, Integer> entry : d.env.entrySet()) {
                pref.putInt(entry.getKey().toString(), entry.getValue());
            }
        }
    }

    @Override
    public Info read(Preferences pref) throws BackingStoreException {
        final String[] keys = pref.keys();
        if (keys.length == 0) {
            return EMPTY;
        }
        Map<Env,Integer> env = new HashMap<Env, Integer>(keys.length* 3 / 2);
        for (String k : keys) {
            Env e = Env.parse(k);
            if (e == null) {
                continue;
            }
            env.put(e, pref.getInt(k, 0));
        }
        return new Info(env);
    }
    
    private static class Env {
        private static final Pattern VER = Pattern.compile(".*(1\\.[5-9]+\\.[0-9]+)[_-].*");
        
        private final String text;
        public Env(String os, String jvm) {
            int comma = os.indexOf(",");
            if (comma >= 0) {
                os = os.substring(0, comma);
            }
            os = os.replace(":", " ");
            
            jvm = jvm.toLowerCase().replace(":", " ");
            String type;
            if (jvm.contains("hotspot")) {
                type = "HotSpot";
            } else if (jvm.contains("openjdk") || jvm.contains("icedtea")) {
                type = "OpenJDK";
            } else {
                type = "Unknown";
                LOG.log(Level.INFO, "Unknown type of JDK: {0}", jvm);
            }
            
            String arch;
            if (jvm.contains("64")) {
                arch = "64bit";
            } else {
                arch = "32bit";
            }
            String ver;
            Matcher m = VER.matcher(jvm);
            if (m.matches()) {
                ver = m.group(1);
            } else {
                LOG.log(Level.INFO, "Unknown version of JDK: {0}", jvm);
                ver = "unknown";
            }
            this.text = os.trim() + ":" + type + ":" + arch + ":" + ver;
        }
        
        private Env(String t) {
            this.text = t;
        }

        @Override
        public String toString() {
            return text.toString();
        }
        
        public static Env parse(String s) {
            return new Env(s);
        }

        @Override
        public boolean equals(Object obj) {
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final Env other = (Env) obj;
            if ((this.text == null) ? (other.text != null) : !this.text.equals(other.text)) {
                return false;
            }
            return true;
        }

        @Override
        public int hashCode() {
            int hash = 7;
            hash = 73 * hash + (this.text != null ? this.text.hashCode() : 0);
            return hash;
        }
    }

    public static class Info {
        private final Map<Env, Integer> env;

        private Info(Map<Env, Integer> env) {
            this.env = env;
        }

        private Info(Object[] params) {
            Env e = new Env(
                (String)params[0],
                (String)params[1]
            );
            env = Collections.singletonMap(e, 1);
        }

        private void select(
            Map<String, Integer> map, 
            int count, String includes
        ) {
            Pattern p = null;
            try {
                if (includes != null) {
                    p = Pattern.compile(includes);
                }
            } catch (PatternSyntaxException ex) {
                LOG.log(Level.WARNING, "Cannot parse {0}", includes);
            }
            SortedSet<C> top = new TreeSet<C>();
            for (Map.Entry<Env, Integer> entry : env.entrySet()) {
                C toAdd;
                if (p == null) {
                    toAdd = new C(entry.getKey().toString(), entry.getValue());
                } else {
                    Matcher m = p.matcher(entry.getKey().toString());
                    if (!m.matches()) {
                        continue;
                    }
                    if (m.groupCount() >= 1) {
                        StringBuilder sb = new StringBuilder();
                        String sep = "";
                        for (int i = 1; i <= m.groupCount(); i++) {
                            sb.append(sep);
                            sb.append(m.group(i));
                            sep = " ";
                        }
                        toAdd = new C(sb.toString(), entry.getValue());
                    } else {
                        toAdd = new C(entry.getKey().toString(), entry.getValue());
                    }
                }
                
                if (top.size() < count) {
                    top.add(toAdd);
                } else {
                    if (toAdd.compareTo(top.first()) >= 0) {
                        top.remove(top.first());
                        top.add(toAdd);
                    }
                }
            }
            
            for (C entry : top) {
                map.put(entry.text, entry.count);
            }
        }
    }
    
    private static final class C implements Comparable<C> {
        final String text;
        final int count;

        public C(String text, int count) {
            this.text = text;
            this.count = count;
        }

        public int compareTo(C o) {
            if (count != o.count) {
                return count - o.count;
            }
            return text.compareTo(o.text);
        }
    }
    
}
