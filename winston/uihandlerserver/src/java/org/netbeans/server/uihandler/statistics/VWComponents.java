/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2008 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * Copyright 2008 Sun Microsystems, Inc. All rights reserved.
 */

package org.netbeans.server.uihandler.statistics;

import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;
import javax.servlet.jsp.PageContext;
import org.netbeans.server.uihandler.*;
import java.util.Collections;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import org.netbeans.server.uihandler.statistics.VWComponents.VWComponent;
import org.openide.util.lookup.ServiceProvider;

/** Counts Web Frameworks usage
 *
 * @author Jan Horvath
 */
@ServiceProvider(service=Statistics.class)
public final class VWComponents extends Statistics<Map<VWComponent, Integer>> {
    static final Logger LOG = Logger.getLogger(ProjectTypes.class.getName());
    private static final Integer ONE = new Integer(1);
    private static final Integer ZERO = new Integer(0);
    private static final String UNKNOWN = "unknown";
    
    public VWComponents() {
        super("VWComponents", 43);
    }

    protected Map<VWComponent, Integer> newData() {
        return Collections.emptyMap();
    }
    
    /*
     */
    protected Map<VWComponent, Integer> process(LogRecord rec) {
        if ("COMPONENT_NAME".equals(rec.getMessage())) {
            VWComponent comp = new VWComponent(getStringParam(rec, 1, UNKNOWN),
                    getStringParam(rec, 0, UNKNOWN));
            return Collections.singletonMap(comp, ONE);
        }
        else {
            return newData();
        }
    }
    
    private static String getStringParam(LogRecord rec, int index, String def) {
        if (rec == null) {
            return def;
        }
        Object[] params = rec.getParameters();
        if (params == null || params.length <= index) {
            return def;
        }
        if (params[index] instanceof String) {
            return (String)params[index];
        }
        return def;
    }
    
    protected Map<VWComponent, Integer> join(
            Map<VWComponent, Integer> one, Map<VWComponent, Integer> two) {
        Map<VWComponent, Integer> merge = new TreeMap<VWComponent, Integer>(one);
        for (Map.Entry<VWComponent, Integer> entry: two.entrySet()) {
            if (merge.containsKey(entry.getKey())) {
                Integer count = merge.get(entry.getKey());
                count = count + entry.getValue();
                merge.put(entry.getKey(), count);
            }
            else {
                merge.put(entry.getKey(), entry.getValue());
            }
        }
        return merge;
    }

    @Override
    protected Map<VWComponent, Integer> finishSessionUpload(
        String userId,
        int sessionNumber,
        boolean initialParse,
        Map<VWComponent, Integer> data
    ) {
        return data;
    }

    @Override
    protected void registerPageContext(PageContext page, String name, Map<VWComponent, Integer> data) {
        super.registerPageContext(page, name, data);
    }
    
    protected void write(Preferences pref, Map<VWComponent, Integer> d) {
        for (Map.Entry<VWComponent, Integer> entry : d.entrySet()) {
            String key = entry.getKey().toString();
            while (key.length() > Preferences.MAX_KEY_LENGTH){
                int index = key.indexOf('.');
                if (index == -1){
                    key = key.substring(key.length() - Preferences.MAX_KEY_LENGTH);
                }else{
                    key = key.substring(index + 1);
                }
            }
            pref.putInt(key, entry.getValue());
        }
    }

    protected Map<VWComponent, Integer> read(Preferences pref) throws BackingStoreException {
        Map<VWComponent, Integer> result = new TreeMap<VWComponent, Integer> ();
        for (String n : pref.keys()) {
            Integer value = pref.getInt(n, ZERO);
            result.put(new VWComponent(n), value);
        }
        return result;
    }
    
    public static class VWComponent implements Comparable<VWComponent>{
        String name;
        String clazz;
        
        public VWComponent(String clazz, String name) {
            this.name = name;
            this.clazz = clazz;
        }
        
        public VWComponent(String t) {
            int i = t.indexOf(":");
            if (i >= 0) {
                this.clazz = t.substring(0, i);
                this.name = t.substring(i + 1, t.length());
            } else {
                this.clazz = UNKNOWN;
                this.name = t;
            }
        }

        public String getClazz() {
            return clazz;
        }

        public String getName() {
            return name;
        }
        
        @Override
        public String toString() {
            return clazz + ":" + name;
        }

        public int compareTo(VWComponent o) {
            return toString().compareTo(o.toString());
        }
    }

}
