/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */


package org.netbeans.server.uihandler;

import java.io.File;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NameClassPair;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.persistence.EntityManager;
import org.netbeans.modules.exceptions.entity.Line;
import org.netbeans.modules.exceptions.entity.Nbversion;
import org.netbeans.modules.exceptions.entity.Report;
import org.netbeans.modules.exceptions.entity.Slowness;
import org.netbeans.modules.exceptions.entity.Stacktrace;
import org.netbeans.modules.exceptions.entity.Submit;
import org.netbeans.modules.exceptions.utils.PersistenceUtils;
import org.netbeans.server.uihandler.CheckingFilter.WholeStackTraceFilter;
import org.netbeans.server.uihandler.CheckingFilter.LinesCheckingFilter;
import org.netbeans.server.uihandler.CheckingFilter.OutOfMemoryFilter;
import org.netbeans.server.uihandler.CheckingFilter.StackOverflowCheckingFilter;
import org.netbeans.server.uihandler.api.Authenticator;
import org.netbeans.server.uihandler.api.Issue;
import org.netbeans.server.uihandler.api.bugs.BugReporter;
import org.openide.util.Lookup;

/**
 *
 * @author Jindrich Sedek
 */
public final class Utils {
    private static final Logger LOG = Logger.getLogger(Utils.class.getName());
    public static final String PASSWORD_PROPERTY = "password";
    public static final String USERNAME_PROPERTY = "username";

    private static final String MESSAGES_DIR_SUFFIX = "_messages";
    private static final String NPS_DIR_SUFFIX = "_nps";
    private static final String HEAP_DIR_SUFFIX = "_heap";
    private static List<CheckingFilter> filters;
    private final static Map<Class, Integer> maxIds = new HashMap<Class, Integer>();
    private static String rootUploadDir;
    private static final int LINES_CHECKING_DEPTH = 4;

    /** Creates a new instance of Utils */
    private Utils() {
    }
 
    private static boolean checkIs173645(EntityManager em, Throwable thrown){
        //173645 has inner AE
        Throwable inner = thrown.getCause();
        if( inner == null) {
            return false;
        }
        //check message
        String msg173645 = "org.openide.util.RequestProcessor$SlowItem: task failed due to: java.lang.AssertionError";
        if (!msg173645.equals(thrown.getMessage())){
            return false;
        }
        //check inner stacktrace (use stacktrace #504914)
        Map<String, Object> params = Collections.singletonMap("stacktraceId", (Object) 504914);
        List<Line> lines = PersistenceUtils.executeNamedQuery(em, "Line.findByStacktraceId", params);
        if(lines.isEmpty()){
            return false;
        }
        if(!FilteringUtils.verifyTheSameStracktraces(inner, lines, 5)){
            return false;
        }
        //its duplicate of 173645!
        return true;
    }
    
    private static Report getReport(EntityManager em, int id){
            Map<String, Object> params = Collections.singletonMap("reportId", (Object) id);
            List<Report> reports = PersistenceUtils.executeNamedQuery(em, "Report.findByID", params);
            return reports.isEmpty() ?  null :  reports.get(0);        
    }

    static boolean isNbLine(StackTraceElement element) {
        if (element.getClassName().startsWith("java")){
            return false;
        }
        return true;
    }

    public static Report checkOpenIssues(EntityManager em, Throwable thrown){
        return checkIssueImpl(em, thrown, true);
    }

    public static Report checkIssue(EntityManager em, Throwable thrown){
        return checkIssueImpl(em, thrown, false);
    }
    
    private static Report checkIssueImpl(EntityManager em, Throwable thrown, boolean searchOnlyOpens){
        if (thrown == null){
            return null;
        }
        //do cheap checking for duplicity with report #173645
        if(checkIs173645(em, thrown)){
            LOG.log(Level.INFO, "checkIs173645=TRUE for thrown:" + thrown.toString());
            Report report173645 = getReport(em, 173645);
            if(report173645!= null) {
                LOG.log(Level.INFO, "Since checkIs173645=TRUE, its duplicate of:"+ report173645.getId());
                return report173645; 
            }
        }
        if (filters == null) {
            filters = new LinkedList<CheckingFilter>();
            filters.add(new WholeStackTraceFilter());
            //filters.add(new OutOfMemoryFilter());
            filters.add(new StackOverflowCheckingFilter());
            filters.add(new LinesCheckingFilter(LINES_CHECKING_DEPTH));
        }
        org.netbeans.modules.exceptions.entity.Exceptions resultExc = null;
        List<Stacktrace> candidates;
        LinkedList<Throwable> inners = new LinkedList<Throwable>();
        Throwable cause = thrown.getCause();
        boolean hasInerStacktrace = (cause != null) && (cause.getStackTrace() != null);
        if (hasInerStacktrace && (cause.getStackTrace().length >= LINES_CHECKING_DEPTH)){
            Throwable inner = thrown.getCause();
            inners.addFirst(thrown);
            while ((inner.getCause() != null) && (inner.getCause().getStackTrace().length >= LINES_CHECKING_DEPTH)){ // get the deepest one
                inners.addFirst(inner);
                inner = inner.getCause();
            }
            candidates = getCandidates(em, inner, searchOnlyOpens);
            if (candidates == null){
                return null;
            }
            while ((inners.size() > 0) && (candidates.size() > 0)){//go to the lowest one
                inner = inners.removeFirst();
                candidates = reduceCandidates(em, candidates, inner);
            }
            if (candidates.size() > 0){
                resultExc = FilteringUtils.getRootExceptions(candidates.get(0));
            }
        }else{
            Iterator<CheckingFilter> iterator = filters.iterator();
            while ((resultExc == null)&&(iterator.hasNext())){
                resultExc = iterator.next().check(em, thrown, searchOnlyOpens);
            }
        }
        //check if we found duplicate && duplicate is not on ignore list
        if (resultExc != null && !CheckingFilter.IgnoredReports.isIgnored(resultExc.getReportId())){
            return resultExc.getReportId();
        }
        return null;
    }

    /**
     *
     * @param em
     * @param candidates
     * @param innerThrowable
     * @return list of parents of candidates corresponding to innerThrowable 
     */
    private static List<Stacktrace> reduceCandidates(EntityManager em, List<Stacktrace> candidates, Throwable innerThrowable) {
        List<Stacktrace> accepted = new LinkedList<Stacktrace>();
        List<Stacktrace> acceptedParents = new LinkedList<Stacktrace>();
        for (Stacktrace stack : candidates) {
            Stacktrace parent = stack.getStacktrace();
            if (parent == null){
                accepted.add(stack);
            }else if (FilteringUtils.verifyTheSameStacktraces(em, innerThrowable, parent, 5)){
                acceptedParents.add(parent);
            }
        }
        FilteringUtils.removeWrongClasses(innerThrowable, acceptedParents);
        accepted.addAll(acceptedParents);
        return accepted;
    }

    private static List<Stacktrace> getCandidates(EntityManager em, Throwable thrown, boolean searchOnlyOpens){
        CheckingFilter.LinesCheckingFilter filter = new CheckingFilter.LinesCheckingFilter(LINES_CHECKING_DEPTH);
        return filter.getSimilarStacktraces(em, thrown, searchOnlyOpens);
    }

    public static int countHash(StackTraceElement[] throwable) {
        if (throwable.length == 0) {
            return 0;
        }
        String str = throwable[0].getClassName();
        str = str.concat(throwable[0].getMethodName());
        for (int i = 1; i < throwable.length; i++) {
            str = str.concat(throwable[i].getClassName());
            str = str.concat(throwable[i].getMethodName());
        }
        int result = str.hashCode();
        return result;
    }

    //
    // Working with JNDI
    //

    static void printContext(String space, Context c) {
        try     {
            printContextImpl(space, c);
        } catch (NamingException ex) {
            org.openide.util.Exceptions.printStackTrace(ex);
        }
    }

    private static void printContextImpl(String space, Context c) throws NamingException {
        if (c == null) {
            Context initCtx = new InitialContext();
            c = (Context) initCtx.lookup("java:comp/env"); // NOI18N
        }

        NamingEnumeration<NameClassPair> tips;
        tips = c.list("");
        while (tips.hasMore()) {
            NameClassPair pair = tips.next();

            Object o = c.lookup(pair.getName());
            if (pair.getClassName().indexOf("Context") >= 0) {
                if (o instanceof Context) {
                    LogsManager.LOG.log(Level.FINE, "binding {0}{1} class: {2}", new Object[]{space, pair.getName(), pair.getClassName()});
                    printContext(space + pair.getName() + "/", (Context)o);
                    continue;
                }
            }
            LogsManager.LOG.log(Level.WARNING, "  value {0}{1} = {2}", new Object[]{space, pair.getName(), o});
        }

    }

    /**
     * Reads value for a variable defined in the "env-entry" section of web.xml
     *
     * @param name name of the variabl
     * @param type the expected type of the variable
     * @return value for the variable or null if there is no, or there is not the right type
     */
    public static <T> T getVariable(String name, Class<T> type) {
        try {
            Context initCtx = new InitialContext();
            Context c = (Context) initCtx.lookup("java:comp/env"); // NOI18N
            c = (Context)c.lookup("analytics"); // NOI18N
            return type.cast(c.lookup(name));
        } catch (NamingException ex) {
            LogsManager.LOG.log(Level.WARNING, "no env variable for " + name, ex);
            printContext("", null);
            return null;
        } catch (ClassCastException ex) {
            LogsManager.LOG.log(Level.WARNING, "wrong env variable for " + name + " we need " + type.getName(), ex);
            return null;
        }
    }

    public static Integer getNextId(Class entity){
        Integer maxId = null;
        synchronized (maxIds) {
            maxId = maxIds.get(entity);
            if (maxId == null) {
                if (entity.equals(Submit.class)){
                    maxId = Math.max(PersistenceUtils.getInstance().max(Exceptions.class, "id"),
                             PersistenceUtils.getInstance().max(Slowness.class, "id"));
                } else {
                    maxId = PersistenceUtils.getInstance().max(entity, "id");
                }
            }
            ++maxId;
            maxIds.put(entity, maxId);
        }
        return maxId;
    }

    public static int getMaxUploadSize(){
        Integer maxUploadSize = getVariable("maxSize", Integer.class); // NOI18N
        if (maxUploadSize == null){
            maxUploadSize = 1024 * 1024;
        }
        return maxUploadSize;
    }

    private static final int DIRECTORY_DEPTH = 2;
    private static final int DIR_INT_COUNT = 3;
    private static String getSubDirName(String id) {
        String hash = Integer.toString(Math.abs(id.hashCode()));
        String name = File.separator;
        for (int depth = 0; depth < DIRECTORY_DEPTH; depth++){
            int length = Math.min(hash.length(), DIR_INT_COUNT);
            name = name + hash.substring(0, length) + File.separator;
            hash = hash.substring(length);
            if (hash.length() == 0){
                break;
            }
        }
        return name;
    }

    public static String getRootUploadDirPath(){
        if (rootUploadDir == null){
            rootUploadDir = Utils.getVariable("dir", String.class);
        }
        return rootUploadDir;
    }

    public static String getMessagesRootUploadDir() {
        return getRootUploadDir(MESSAGES_DIR_SUFFIX);
    }

    public static String getSlownessRootUploadDir() {
        return getRootUploadDir(NPS_DIR_SUFFIX);
    }

    public static String getHeapRootUploadDir() {
        return getRootUploadDir(HEAP_DIR_SUFFIX);
    }

    private static String getRootUploadDir(String suffix){
        File uploadDir = new File(getRootUploadDirPath());
        File parentFile = uploadDir.getParentFile();
        File targetUploadDir = new File(parentFile, uploadDir.getName() + suffix);
        targetUploadDir.mkdir();
        return targetUploadDir.getAbsolutePath();
    }
    
    public static String getUploadDirPath(String rootUploadDir, String id) {
        if (id == null){
            return rootUploadDir;
        }
        File uploadDir = new File(rootUploadDir, getSubDirName(id));
        uploadDir.mkdirs();
        return uploadDir.getAbsolutePath();
    }

    static void setRootUploadDir(String rootUploadDirPath){
        Utils.rootUploadDir = rootUploadDirPath;
    }

    private static Integer parsingTimeout = null;
    public static Integer getParsingTimeout() {
        if (parsingTimeout == null) {
            parsingTimeout = Utils.getVariable("parsingTimeout", Integer.class);
        }
        if (parsingTimeout == null) {//no environement value

            parsingTimeout = new Integer(1000);// default value

        }
        return parsingTimeout;
    }

    //just for tests -> no read from configuration file
    public static void setParsingTimeout(Integer timeout) {
        parsingTimeout = timeout;
    }

    static Long getThreeMonthsEarlierDate(Date date) {
        Calendar cd = Calendar.getInstance();
        cd.setTime(date);
        cd.add(Calendar.MONTH, -3);
        long year = cd.get(Calendar.YEAR) % 100;
        long month = cd.get(Calendar.MONTH) + 1;
        long day = cd.get(Calendar.DAY_OF_MONTH);
        return new Long(year * 10000+month*100+day);
    }

    public static String getReporterUsername(){
        return Lookup.getDefault().lookup(Authenticator.class).getDefaultReporterUsername();
    }

    public static boolean isGuest(String userName){
        return BugReporter.GUEST_USER.equals(userName);
    }
    
    static boolean isIssueTMBeforeExcProductVersion(Submit sbmt, Issue issue) {
        String excVersion = sbmt.getLogfileId().getProductVersionId().getNbversionId().getVersionNumber();
        String issueTMVersion = issue.getTMNumber();
        if ((excVersion == null)||(issueTMVersion == null)){
            return false;
        }
        if (issueTMVersion.equals(Issue.TBD_TM)){
            return true;
        }
        if (excVersion.equals(Nbversion.DEV_VERSION)){
            return true;
        }
        return excVersion.compareTo(issueTMVersion) >= 0;
    }

}

