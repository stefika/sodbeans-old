/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * Copyright 2007 Sun Microsystems, Inc. All rights reserved.
 */

package org.netbeans.server.uihandler.statistics;

import java.util.Map;
import java.util.TreeMap;
import java.util.logging.LogRecord;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;
import javax.servlet.jsp.PageContext;
import java.util.logging.Logger;
import java.util.Collection;
import org.netbeans.server.uihandler.Statistics;
import org.openide.util.lookup.ServiceProvider;

/** Counts the number of used projecs and project types.
 *
 * @author Jan Zyka
 */

/*
   StatRecord tady znamena tridu dat kterou budu brat, u me asi StatRecord
 */
@ServiceProvider(service=Statistics.class)
public final class CodeCompletion extends Statistics<CodeCompletionData> {
    static final Logger LOG = Logger.getLogger(CodeCompletion.class.getName());
    private static final String INDEX_ = "Index ";
    private static final String UNKNOWN = "Unknown";
    
    public CodeCompletion() {
        super("CodeCompletion", 2);
    }
    
    protected CodeCompletionData newData() {
        return new CodeCompletionData();
    }

    protected CodeCompletionData process(LogRecord rec) {
        
        CodeCompletionData tmpData = new CodeCompletionData();
        
        if ("COMPL_INVOCATION".equals(rec.getMessage())) {
            tmpData.incTotalRecords();
            String invocationType = (String)rec.getParameters()[0];
            if (invocationType.equals("true")) {
                tmpData.incImplicitInvocations();
            } else {
                tmpData.incExplicitInvocations();
            }
        } else if ("COMPL_CANCEL".equals(rec.getMessage())) {
            tmpData.incCancelledInvocations();
        } else if ("COMPL_KEY_SELECT_DEFAULT".equals(rec.getMessage())) {
            tmpData.incDefaultSelect();
            tmpData.incKeyboardCompletion();
            tmpData.addIndex(0);
        } else if ("COMPL_KEY_SELECT".equals(rec.getMessage())) {
            tmpData.incKeyboardCompletion();
            String indexOfItem = (String)rec.getParameters()[1];
            try {
                tmpData.addIndex(Integer.parseInt(indexOfItem));
                if (tmpData.getSelectedIndexes().containsKey(INDEX_ + indexOfItem)) {
                    int currentVal = tmpData.getSelectedIndexes().get(INDEX_ + indexOfItem);
                    tmpData.getSelectedIndexes().put(INDEX_ + indexOfItem, currentVal+1);
                } else {
                    tmpData.getSelectedIndexes().put(INDEX_ + indexOfItem, 1);
                }
            } catch (NumberFormatException e) {
                tmpData.addIndex(0);
                if (tmpData.getSelectedIndexes().containsKey(UNKNOWN)) {
                    int currentVal = tmpData.getSelectedIndexes().get(UNKNOWN);
                    tmpData.getSelectedIndexes().put(UNKNOWN, currentVal+1);
                } else {
                    tmpData.getSelectedIndexes().put(UNKNOWN, 1);
            }
            }
        } else if ("COMPL_MOUSE_SELECT".equals(rec.getMessage())) {
            tmpData.incMouseCompletion();
            String indexOfItem = (String)rec.getParameters()[1];
            try {
                tmpData.addIndex(Integer.parseInt(indexOfItem));
                if (tmpData.getSelectedIndexes().containsKey(INDEX_ + indexOfItem)) {
                    int currentVal = tmpData.getSelectedIndexes().get(INDEX_ + indexOfItem);
                    tmpData.getSelectedIndexes().put(INDEX_ + indexOfItem, currentVal+1);
                } else {
                    tmpData.getSelectedIndexes().put(INDEX_ + indexOfItem, 1);
                }
            } catch (NumberFormatException e) {
                tmpData.addIndex(0);
                if (tmpData.getSelectedIndexes().containsKey(UNKNOWN)) {
                    int currentVal = tmpData.getSelectedIndexes().get(UNKNOWN);
                    tmpData.getSelectedIndexes().put(UNKNOWN, currentVal+1);
                } else {
                    tmpData.getSelectedIndexes().put(UNKNOWN, 1);
            }
        } 
        } 
        
        return tmpData;
    }

    protected CodeCompletionData finishSessionUpload(String userId, int sessionNumber, boolean initialParse, CodeCompletionData d) {
        return d;
    }

    protected CodeCompletionData join(CodeCompletionData one, CodeCompletionData two) {
        if (one == null) {
            return two;
        }
        
        if (two == null) {
            return one;
        }
        
        CodeCompletionData tmpData = new CodeCompletionData();
        
        tmpData.setTotalRecords(one.getTotalRecords()+two.getTotalRecords());
        tmpData.setImplicitInvocations(one.getImplicitInvocations()+two.getImplicitInvocations());
        tmpData.setExplicitInvocations(one.getExplicitInvocations()+two.getExplicitInvocations());
        tmpData.setCancelledInvocations(one.getCancelledInvocations()+two.getCancelledInvocations());
        tmpData.setDefaultSelect(one.getDefaultSelect()+two.getDefaultSelect());
        tmpData.setSelectionIndexesSum(one.getSelectionIndexesSum()+two.getSelectionIndexesSum());
        tmpData.setTotalSelections(one.getTotalSelections()+two.getTotalSelections());
        tmpData.setMouseCompletion(one.getMouseCompletion()+two.getMouseCompletion());
        tmpData.setKeyboardCompletion(one.getKeyboardCompletion()+two.getKeyboardCompletion());
        
        tmpData.setSelectedIndexes(one.getSelectedIndexes());
        
        Collection<String> keys = two.getSelectedIndexes().keySet();
        
        for (String key:keys) {
            if (tmpData.getSelectedIndexes().containsKey(key)) {
                int currentVal = tmpData.getSelectedIndexes().get(key);
                tmpData.getSelectedIndexes().put(key, currentVal+two.getSelectedIndexes().get(key));
            } else {
                tmpData.getSelectedIndexes().put(key, two.getSelectedIndexes().get(key));
            }
        }
                
        return tmpData;
    }
    
    @Override
    protected void registerPageContext(PageContext page, String name,
                                       CodeCompletionData data) {
        super.registerPageContext(page, name, data);
        
        if (name.equals("globalCodeCompletion")) {
            Map<String,Integer> keyboardMouseUsage = new TreeMap<String,Integer>();
            keyboardMouseUsage.put("Keyboard completion", data.getKeyboardCompletion());
            keyboardMouseUsage.put("Mouse completion", data.getMouseCompletion());
            page.setAttribute("keyboardMouseUsage", keyboardMouseUsage);
            
            Map<String,Integer> complCancel = new TreeMap<String,Integer>();
            complCancel.put("Completed tasks", data.getKeyboardCompletion()+data.getMouseCompletion());
            complCancel.put("Cancelled tasks", data.getCancelledInvocations());
            complCancel.put(UNKNOWN, data.getTotalRecords()-(data.getKeyboardCompletion()+data.getMouseCompletion())-data.getCancelledInvocations());
            page.setAttribute("complCancel", complCancel);
            
            Map<String,Integer> implicitExplicit = new TreeMap<String,Integer>();
            implicitExplicit.put("Implicit", data.getImplicitInvocations());
            implicitExplicit.put("Explicit", data.getExplicitInvocations());
            page.setAttribute("implicitExplicit", implicitExplicit);
            
            Map<String,Integer> selectedIndex = new TreeMap<String,Integer>();
            selectedIndex.put("Default completions", data.getDefaultSelect());
            selectedIndex.put("Others", data.getKeyboardCompletion()+data.getMouseCompletion()-data.getDefaultSelect());
            page.setAttribute("selectedIndex", selectedIndex);
            
            page.setAttribute("selectedIndexes", data.getTopSelectedIndexes(20));
        }
    }

    protected void write(Preferences pref, CodeCompletionData d) {
        d.write(pref);
    }

    protected CodeCompletionData read(Preferences pref) throws BackingStoreException {
        CodeCompletionData d = newData();
        d.read(pref);
        return d;
    }
    
}
