/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */
package org.netbeans.server.uihandler.statistics;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;
import javax.servlet.jsp.PageContext;
import org.netbeans.server.uihandler.Statistics;
import org.netbeans.server.uihandler.statistics.ProjectSize.ProjectSizeBean;
import org.openide.util.lookup.ServiceProvider;

/**
 *
 * @author Jindrich Sedek
 */
@ServiceProvider(service=Statistics.class)
public class ProjectSize extends Statistics<ProjectSizeBean> {
    public static final int MINIMUM_DEF = 0;
    public static final int MAXIMUM_DEF = 500;
    public static final int COLUMNS_DEF = 10;
    private static final ProjectSizeBean EMPTY = new ProjectSizeBean();
    private static final String MESSAGE = "org.netbeans.modules.java.source.usages.RepositoryUpdater";
    static final String JAVA_FILES = "JAVA_FILES";
    static final String VIRTUAL_FILES = "VIRTUAL_FILES";
    static final String USER_LOG_JAVA_FILES = "USER_LOG_JAVA_FILES";
    static final String USER_LOG_VIRTUAL_FILES = "USER_LOG_VIRTUAL_FILES";

    public ProjectSize() {
        super("ProjectSize", 2);
    }

    @Override
    protected ProjectSizeBean newData() {
        return EMPTY;
    }

    @Override
    protected ProjectSizeBean process(LogRecord rec) {
        if (MESSAGE.equals(rec.getMessage())) {
            Object[] params = rec.getParameters();
            if ((params != null) && (params.length > 1)) {
                return new ProjectSizeBean(params);
            }
        }
        return newData();
    }

    @Override
    protected ProjectSizeBean join(ProjectSizeBean one, ProjectSizeBean two) {
        if (EMPTY.equals(one)) {
            return two;
        }
        if (EMPTY.equals(two)) {
            return one;
        }
        assert (one.isStandardized() == two.isStandardized());
        ProjectSizeBean result = new ProjectSizeBean();
        if (one.isStandardized()) {
            result.javaFilesCounts = new HashMap<Integer, Integer>(one.javaFilesCounts);
            result.virtFilesCounts = new HashMap<Integer, Integer>(one.virtFilesCounts);
            result.userLogJavaFilesCounts = new HashMap<Integer, Integer>(one.userLogJavaFilesCounts);
            result.userLogVirtFilesCounts = new HashMap<Integer, Integer>(one.userLogVirtFilesCounts);
            addAllCounts(result.javaFilesCounts, two.javaFilesCounts);
            addAllCounts(result.virtFilesCounts, two.virtFilesCounts);
            addAllCounts(result.userLogJavaFilesCounts, two.userLogJavaFilesCounts);
            addAllCounts(result.userLogVirtFilesCounts, two.userLogVirtFilesCounts);
        } else {
            result.sRootJavaFile = new HashMap<String, Integer>(one.sRootJavaFile);
            result.sRootVirtualFile = new HashMap<String, Integer>(one.sRootVirtualFile);
            result.sRootJavaFile.putAll(two.sRootJavaFile);
            result.sRootVirtualFile.putAll(two.sRootVirtualFile);
        }
        return result;
    }

    @Override
    protected ProjectSizeBean finishSessionUpload(String userId, int sessionNumber, boolean initialParse, ProjectSizeBean bean) {
        if (bean.equals(EMPTY)){
            return bean;
        }
        assert (!bean.isStandardized());
        ProjectSizeBean result = new ProjectSizeBean();
        result.javaFilesCounts = new HashMap<Integer, Integer>();
        result.virtFilesCounts = new HashMap<Integer, Integer>();
        int jfAll = 0;
        for (Map.Entry<String, Integer> entry : bean.sRootJavaFile.entrySet()) {
            add(result.javaFilesCounts, entry.getValue());
            jfAll += entry.getValue();
        }
        if (jfAll != 0){
            result.userLogJavaFilesCounts = Collections.singletonMap(jfAll, 1);
        }else{
            result.userLogJavaFilesCounts = Collections.emptyMap();
        }
        int vfAll = 0;
        for (Map.Entry<String, Integer> entry : bean.sRootVirtualFile.entrySet()) {
            add(result.virtFilesCounts, entry.getValue());
            vfAll += entry.getValue();
        }
        if (vfAll != 0){
            result.userLogVirtFilesCounts = Collections.singletonMap(vfAll, 1);
        }else{
            result.userLogVirtFilesCounts = Collections.emptyMap();
        }
        assert (result.isStandardized());
        return result;
    }

    private void add(Map<Integer, Integer> countsMap, Integer addedCount) {
        if (countsMap.containsKey(addedCount)) {
            countsMap.put(addedCount, countsMap.get(addedCount) + 1);
        } else {
            countsMap.put(addedCount, 1);
        }
    }

    @Override
    protected void registerPageContext(PageContext page, String name, ProjectSizeBean data) {
        register(page, name + JAVA_FILES, data.javaFilesCounts);
        register(page, name + VIRTUAL_FILES, data.virtFilesCounts);
        register(page, name + USER_LOG_JAVA_FILES, data.userLogJavaFilesCounts);
        register(page, name + USER_LOG_VIRTUAL_FILES, data.userLogVirtFilesCounts);
    }

    private void register(PageContext page, String name, Map<Integer, Integer> filesCount) {
        if (filesCount == null || (filesCount.size() == 0)){
            return;
        }
        int min = getNumber(page, "minimum", MINIMUM_DEF);
        int max = getNumber(page, "maximum", MAXIMUM_DEF);
        int cnt = getNumber(page, "columns", COLUMNS_DEF);
        List<ProjectSizeItem> items = convertToDataset(filesCount, min, max, cnt);
        Collections.sort(items);
        page.setAttribute(name, items);
    }

    private int getNumber(PageContext page, String attrName, int def) {
        String count = (String)page.getAttribute(attrName, PageContext.REQUEST_SCOPE);
        if (count == null){
            return def;
        }
        return Integer.parseInt(count);
    }

    private static List<ProjectSizeItem> convertToDataset(Map<Integer, Integer> logs, int minValue, int maxValue, int columnCount) {
        if (columnCount < 2) {
            columnCount = 2;
        }
        if (minValue < 0) {
            minValue = 0;
        }
        if (maxValue < minValue) {
            maxValue = MAXIMUM_DEF;
        }
        int step = (maxValue - minValue) / columnCount;

        int[] counts = new int[columnCount];

        for (Map.Entry<Integer, Integer> entry: logs.entrySet()) {
            long value = entry.getKey();

            int index;
            if (value < minValue) {
                continue;
            } else if (value >= maxValue) {
                continue;
            } else {
                index = (int)((value - minValue) / step);
            }
            if (index > columnCount - 1){ // rounding
                index = columnCount - 1;
            }
            counts[index] += entry.getValue();
        }

        List<ProjectSizeItem> res = new ArrayList<ProjectSizeItem>(columnCount);
        for (int i = 0; i < columnCount; i++) {
            int value = minValue + step * i + step / 2;
            res.add(new ProjectSizeItem(value, counts[i]));
        }
        return res;
    }

    @Override
    protected void write(Preferences pref, ProjectSizeBean data) throws BackingStoreException {
        writeMap(pref.node(JAVA_FILES), data.javaFilesCounts);
        writeMap(pref.node(VIRTUAL_FILES), data.virtFilesCounts);
        writeMap(pref.node(USER_LOG_JAVA_FILES), data.userLogJavaFilesCounts);
        writeMap(pref.node(USER_LOG_VIRTUAL_FILES), data.userLogVirtFilesCounts);
    }

    @Override
    protected ProjectSizeBean read(Preferences pref) throws BackingStoreException {
        ProjectSizeBean bean = new ProjectSizeBean();
        bean.javaFilesCounts = readCounts(pref.node(JAVA_FILES));
        bean.userLogJavaFilesCounts = readCounts(pref.node(USER_LOG_JAVA_FILES));
        bean.virtFilesCounts = readCounts(pref.node(VIRTUAL_FILES));
        bean.userLogVirtFilesCounts = readCounts(pref.node(USER_LOG_VIRTUAL_FILES));
        return bean;
    }

    public static class ProjectSizeBean {

        private Map<String, Integer> sRootJavaFile;
        private Map<String, Integer> sRootVirtualFile;
        private Map<Integer, Integer> javaFilesCounts;
        private Map<Integer, Integer> virtFilesCounts;
        private Map<Integer, Integer> userLogJavaFilesCounts;
        private Map<Integer, Integer> userLogVirtFilesCounts;

        private ProjectSizeBean() {
        }

        private ProjectSizeBean(Object[] params) {
            int count = (params.length - 1) / 2;
            sRootJavaFile = new HashMap<String, Integer>(count);
            sRootVirtualFile = new HashMap<String, Integer>(count);
            for (int i = 0; i < count; i++) {
                String sRoot = params[2 * i + 1].toString();
                String counts = params[2 * i + 2].toString();
                String[] jv = counts.split(":");
                Integer javaFiles = Integer.parseInt(jv[0]);
                Integer virtFiles = Integer.parseInt(jv[1]);
                if (javaFiles != 0){
                    sRootJavaFile.put(sRoot, javaFiles);
                }
                if (virtFiles!= 0){
                    sRootVirtualFile.put(sRoot, virtFiles);
                }
            }
        }

        private boolean isStandardized() {
            if ((sRootJavaFile == null) != (sRootVirtualFile == null)){
                Logger.getLogger(ProjectSize.class.getName()).warning("incorrect standardization state");
            }
            if ((virtFilesCounts == null) != (javaFilesCounts == null)){
                Logger.getLogger(ProjectSize.class.getName()).warning("incorrect standardization state");
            }
            return (sRootJavaFile == null);
        }
    }
    
    public static class ProjectSizeItem implements Comparable<ProjectSizeItem>{
        private Integer filescount;
        private Integer count;

        public ProjectSizeItem(int files_count, int count) {
            this.filescount = files_count;
            this.count = count;
        }

        @Override
        public boolean equals(Object obj) {
            if (!(obj instanceof ProjectSizeItem)){
                return false;
            }
            ProjectSizeItem item = (ProjectSizeItem) obj;
            return ((item.getFilescount().equals(getFilescount())) && (item.getCount().equals(getCount())));
        }

        @Override
        public int hashCode() {
            int hash = 3;
            hash = 97 * hash + this.getFilescount();
            hash = 37 * hash + this.getCount();
            return hash;
        }

        public int compareTo(ProjectSizeItem o) {
            return getFilescount() - o.getFilescount();
        }

        public void setFilescount(Integer filescount) {
            this.filescount = filescount;
        }

        public void setCount(Integer count) {
            this.count = count;
        }

        public Integer getFilescount() {
            return filescount;
        }

        public Integer getCount() {
            return count;
        }

        public String getSerie(){
            return "";
        }
    }
}

