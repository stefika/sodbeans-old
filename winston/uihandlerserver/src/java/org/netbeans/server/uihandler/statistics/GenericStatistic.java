/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 2010 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 *
 * Contributor(s):
 *
 * Portions Copyrighted 2010 Sun Microsystems, Inc.
 */
package org.netbeans.server.uihandler.statistics;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.logging.LogRecord;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;
import java.util.regex.Pattern;
import javax.servlet.jsp.PageContext;
import org.netbeans.server.uihandler.Statistics;
import org.openide.util.lookup.ServiceProvider;

/**
 *
 * @author Jindrich Sedek
 */
@ServiceProvider(service = Statistics.class)
public class GenericStatistic extends Statistics<GenericMap> {
    private static final GenericMap EMPTY_MAP = new GenericMap();
    private static final Pattern URL_CHARS = Pattern.compile("[$<>'#&|+,:;=?@/]");
    
    static{
        EMPTY_MAP.absoluteCounts = Collections.<String,Map<String, Integer>>emptyMap();
        EMPTY_MAP.logAvareCounts = Collections.<String,Map<String, Integer>>emptyMap();
    }
    private static final List<String> DEFAULT_MESSAGES = Arrays.asList(ResourceBundle.getBundle("org.netbeans.server.uihandler.statistics.GenericStatistic").
            getString("SUPPORTED_MESSAGES").split(":"));
    private static final String MESSAGE_PREFIX = "GENERIC_";
    private final List<String> messages;

    public GenericStatistic() {
        this(DEFAULT_MESSAGES);
    }

    public GenericStatistic(List<String> acceptedMessages){
        super("GenericStatistic", 1);
        this.messages = acceptedMessages;
    }

    @Override
    protected GenericMap newData() {
        return null;
    }

    @Override
    protected GenericMap process(LogRecord rec) {
        String message = rec.getMessage();
        if (message == null) {
            return null;
        }
        Object[] params = rec.getParameters();
        if ((params == null) || params.length == 0) {
            return null;
        }
        String firstParam = filterValueNameFromURLChars(params[0].toString());
        if (messages.contains(message)) {
            return new GenericMap(message, firstParam);
        } else if (message.startsWith(MESSAGE_PREFIX)) {
            return new GenericMap(message.substring(MESSAGE_PREFIX.length()), firstParam);
        }
        return null;
    }

    @Override
    protected GenericMap join(GenericMap one, GenericMap two) {
        if (one != null && two != null) {
            return one.join(two);
        } else if (one == null) {
            return two;
        } else {
            return one;
        }
    }

    @Override
    protected GenericMap finishSessionUpload(String userId, int sessionNumber, boolean initialParse, GenericMap d) {
        if (d == null) {
            return d;
        }
        d.logAvareCounts = new HashMap<String, Map<String, Integer>>(d.absoluteCounts.size());
        for (Map.Entry<String, Map<String, Integer>> entry : d.absoluteCounts.entrySet()) {
            Map<String, Integer> value = entry.getValue();
            Map<String, Integer> logAvareValue = new HashMap<String, Integer>(value.size());
            for (String key : value.keySet()) {
                logAvareValue.put(key, 1);
            }
            d.logAvareCounts.put(entry.getKey(), logAvareValue);
        }
        return d;
    }

    @Override
    protected void write(Preferences pref, GenericMap d) throws BackingStoreException {
        if (d == null){
            return;
        }
        write(pref.node("A"), d.absoluteCounts);
        write(pref.node("L"), d.logAvareCounts);
    }

    @Override
    protected GenericMap read(Preferences pref) throws BackingStoreException {
        Map<String, Map<String, Integer>> absoluteCounts = readMap(pref.node("A"));
        if (absoluteCounts == null){
            return null;
        }
        GenericMap result = new GenericMap();
        result.absoluteCounts = absoluteCounts;
        result.logAvareCounts = readMap(pref.node("L"));
        return result;
    }

    private static void write(Preferences pref, Map<String, Map<String, Integer>> data){
        for (Map.Entry<String, Map<String, Integer>> entry : data.entrySet()) {
            Preferences entryPreferences = pref.node(entry.getKey());
            for (Map.Entry<String, Integer> value : entry.getValue().entrySet()) {
                entryPreferences.putInt(value.getKey(), value.getValue());
            }
        }
    }

    @Override
    protected void registerPageContext(PageContext page, String name, GenericMap data) {
        if (data != null){
            super.registerPageContext(page, name, data);
        }else{
            super.registerPageContext(page, name, EMPTY_MAP);
        }
    }

    private static Map<String, Map<String, Integer>> readMap(Preferences pref) throws BackingStoreException {
        String[] children = pref.childrenNames();
        if (children.length == 0){
            return null;
        }
        Map<String, Map<String, Integer>> result = new HashMap<String, Map<String, Integer>>(children.length);
        for (String child : children) {
            Preferences childPref = pref.node(child);
            String[] keys = childPref.keys();
            Map<String, Integer> values = new HashMap<String, Integer>(keys.length);
            for (String key : keys) {
                values.put(key, childPref.getInt(key, 1));
            }
            result.put(child, values);
        }
        return result;
    }

    static String filterValueNameFromURLChars(String toFilter){
        return URL_CHARS.matcher(toFilter).replaceAll("");
    }
}
