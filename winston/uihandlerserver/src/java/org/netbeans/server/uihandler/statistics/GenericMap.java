/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 2010 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 *
 * Contributor(s):
 *
 * Portions Copyrighted 2010 Sun Microsystems, Inc.
 */
package org.netbeans.server.uihandler.statistics;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.Preferences;

/**
 *
 * @author Jindrich Sedek
 */
public class GenericMap {

    Map<String, Map<String, Integer>> absoluteCounts;
    Map<String, Map<String, Integer>> logAvareCounts;

    public GenericMap() {
    }

    GenericMap(String key, String value) {
        if (key.length() > Preferences.MAX_KEY_LENGTH) {
            key = key.substring(0, Preferences.MAX_KEY_LENGTH);
        }
        if (value.length() > Preferences.MAX_KEY_LENGTH) {
            value = value.substring(0, Preferences.MAX_KEY_LENGTH);
        }
        Map<String, Integer> valueCount = Collections.singletonMap(value, 1);
        absoluteCounts = Collections.singletonMap(key, valueCount);
    }

    GenericMap join(GenericMap two) {
        assert two != null;
        GenericMap result = new GenericMap();
        result.absoluteCounts = joinKeyMaps(absoluteCounts, two.absoluteCounts);
        if (logAvareCounts == null) {
            result.logAvareCounts = two.logAvareCounts;
        } else if (two.logAvareCounts == null) {
            result.logAvareCounts = logAvareCounts;
        } else {
            result.logAvareCounts = joinKeyMaps(logAvareCounts, two.logAvareCounts);
        }
        return result;
    }

    static Map<String, Map<String, Integer>> joinKeyMaps(Map<String, Map<String, Integer>> m1, Map<String, Map<String, Integer>> m2) {
        Map<String, Map<String, Integer>> result = new HashMap<String, Map<String, Integer>>();
        for (Map.Entry<String, Map<String, Integer>> entry : m1.entrySet()) {
            Map<String, Integer> twoValue = m2.get(entry.getKey());
            if (twoValue == null) {
                result.put(entry.getKey(), entry.getValue());
            } else {
                result.put(entry.getKey(), joinMaps(entry.getValue(), twoValue));
            }
        }
        for (Map.Entry<String, Map<String, Integer>> entry : m2.entrySet()) {
            if (!m1.containsKey(entry.getKey())) {
                result.put(entry.getKey(), entry.getValue());
            }
        }
        return result;
    }

    private static Map<String, Integer> joinMaps(Map<String, Integer> m1, Map<String, Integer> m2) {
        assert m1 != null;
        assert m2 != null;
        Map<String, Integer> sum = new HashMap<String, Integer>();
        for (Map.Entry<String, Integer> entry : m1.entrySet()) {
            Integer twoVal = m2.get(entry.getKey());
            if (twoVal != null) {
                // only in m1
                sum.put(entry.getKey(), entry.getValue() + twoVal);
            } else {
                // in both
                sum.put(entry.getKey(), entry.getValue());
            }
        }
        for (Map.Entry<String, Integer> entry : m2.entrySet()) {
            if (!m1.containsKey(entry.getKey())) {
                // only in m2
                sum.put(entry.getKey(), entry.getValue());
            }
        }
        return sum;
    }

    public Collection<String> getCategories() {
        return absoluteCounts.keySet();
    }

    public Map<String, Integer> getCounts(String category, boolean encode) {
        return sort(absoluteCounts.get(category), encode);
    }

    public Map<String, Integer> getLogAvareCounts(String category, boolean encode) {
        return sort(logAvareCounts.get(category), encode);
    }

    private static Map<String, Integer> sort(Map<String, Integer> data, boolean encode) {
        if (data == null) {
            return null;
        }
        String[] keys = data.keySet().toArray(new String[data.size()]);
        Arrays.sort(keys, new ValueComparator(data));
        Map<String, Integer> result = new LinkedHashMap<String, Integer>(data.size());
        for (String key : keys) {
            String resultKey = key;
            if (encode){
                resultKey = encode(key);
            }
            result.put(resultKey, data.get(key));
        }
        return result;
    }

    private static String encode(String value){
        try {
            return URLEncoder.encode(value, "UTF-8");
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(GenericMap.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
        }
        return "Unformattable";
    }

    private static class ValueComparator implements Comparator<String> {

        private final Map<String, Integer> data;

        private ValueComparator(Map<String, Integer> data) {
            this.data = data;
        }

        public int compare(String o1, String o2) {
            return data.get(o2).compareTo(data.get(o1)); // descending
        }
    }
}
