/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */

package org.netbeans.server.uihandler;

import java.util.prefs.Preferences;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import org.openide.util.lookup.ServiceProvider;

/**LogRecord number 0 contains userData(operating system, virtual machine,
 * NetBeans product version,  user name, summary and comment
 * LogRecord number 1 contains lastly thrown exception
 * LogRecord number 2 contains issueId if duplicate
 *
 * @author Jindrich Sedek
 */
@ServiceProvider(service=Statistics.class, position=0)
public class Exceptions extends Statistics<ExceptionsData> {
    private static final String ACTION_NAME = "ActionName";
    private static final String DATA_TYPE = "dataType";
    private static final String ISSUE_ID = "issueId";
    private static final String REPORT_ID = "reportId";
    private static final String REPORT_BEFORE_REOPEN_ID = "reportBeforeReopenId";
    private static final String ISSUEZILLA = "issuezilla";
    private static final String SLOWNESS_TYPE = "slowness_type";
    private static final String TIME = "TIME";
    private static final String USER_NAME = "username";
    private static final int NON_VALUE = -1;
    private static final Logger LOG = Logger.getLogger(Exceptions.class.getName());
    public static final String USER_CONFIGURATION = "UI_USER_CONFIGURATION";   // NOI18N
    public static final String BUILD_INFO_FILE = "build_info";
    public static final String DONT_INSERT = "NB_REPORTER_IGNORE";
    public static final String SLOWNESS_DATA = "SlownessData";
    /** Creates a new instance of Exceptions */
    
    public Exceptions() {
        super("Exceptions");
    }

    @Override
    protected ExceptionsData newData() {
        return new ExceptionsData();
    }

    @Override
    protected ExceptionsData process(LogRecord rec) {
        ExceptionsData data = new ExceptionsData();
        if (USER_CONFIGURATION.equals(rec.getMessage())){
            data.setUserDataLog(rec);
        }
        if ((rec.getThrown()!= null)&&(rec.getLevel().intValue() >= Level.WARNING.intValue())){
            if ((rec.getMessage() == null) ||(!rec.getMessage().startsWith(DONT_INSERT))){
                data.setThrownLog(rec);
            }
        }
        if (BUILD_INFO_FILE.equals(rec.getMessage())){
            data.setBuildInfo(rec);
        }
        if (SLOWNESS_DATA.equals(rec.getMessage())){
            Object[] params = rec.getParameters();
            assert params != null && params.length >= 2;
            Long time = Long.parseLong(params[0].toString());
            data.setSlownessTime(time);
            if (params.length > 1 && !"null".equals(params[1])){
                data.setLatestAction(params[1].toString());
            }
            if ((params.length == 3) && (!"null".equals(params[2]))){
                data.setSlownessType(params[2].toString());
            }
        }
        return data;
    }

    @Override
    protected ExceptionsData join(ExceptionsData one, ExceptionsData two) {
        ExceptionsData result = new ExceptionsData();
        // there is only one user data in one report
        result.setUserDataLog(getSecondIfNotNull(one.getUserDataLog(), two.getUserDataLog()));
        result.setBuildInfo(getSecondIfNotNull(one.getBuildInfo(), two.getBuildInfo()));
        result.setSlownessTime(getSecondIfNotNull(one.getSlownessTime(), two.getSlownessTime()));
        result.setSlownessType(getSecondIfNotNull(one.getSlownessType(), two.getSlownessType()));
        result.setLatestAction(getSecondIfNotNull(one.getLatestAction(), two.getLatestAction()));
        //user last exception you get
        result.setThrownLog(getSecondIfNotNull(one.getThrownLog(), two.getThrownLog()));
        return result;
    }

    private static <T> T getSecondIfNotNull(T first, T second) {
        return (second != null) ? second : first;
    }

    @Override
    protected ExceptionsData finishSessionUpload(String userId, int sessionNumber, boolean initialParse, ExceptionsData data) {
        if (initialParse){
            LogRecord metaData = data.getUserDataLog();
            if (metaData==null){
                LOG.info("METADATA = NULL");    // NOI18N
            }else if (metaData.getParameters() == null){
                LOG.info("PARAMETERS = NULL"); // NOI18N
            }else{
                int length = metaData.getParameters().length;
                Throwable thrown = null;
                if ((data.getThrownLog() != null)&&(length>=6)){// summary and comment are send => report
                    thrown = data.getThrownLog().getThrown();
                }
                DbInsertion insertion;
                insertion = new DbInsertion(metaData, thrown, data.getBuildInfo(), data.getSlownessTime(), data.getSlownessType(), data.getLatestAction());
                insertion.start(data);
            }
        }else{
            data.setDataType(ExceptionsData.DataType.STATISTIC);
        }
        LOG.finest("EXCEPTIONS UPLOAD FINISHED");
        if (LogFileTask.getRunningTask() != null){
            LogFileTask.getRunningTask().setExceptionsData(data);
        }
        return data;
    }

    @Override
    protected void write(Preferences pref, ExceptionsData d){
        pref.put(DATA_TYPE, d.getDataType().name());
        if (d.isExceptionReport() || d.isSlownessReport()){//exc report + inital parse
            pref.putInt(ISSUE_ID, d.getSubmitId());
            pref.putInt(REPORT_ID, d.getReportId());
            if (d.getReportBeforeReopenId() != null){
                pref.putInt(REPORT_BEFORE_REOPEN_ID, d.getReportBeforeReopenId());
            }
            if (d.getIssuezillaId() != null){
                pref.putInt(ISSUEZILLA, d.getIssuezillaId());
            }
            if (d.getSlownessTime() != null){
                pref.putLong(TIME, d.getSlownessTime());
                if (d.getLatestAction() != null){
                    pref.put(ACTION_NAME, d.getLatestAction());
                }
                if (d.getSlownessType() != null){
                    pref.put(SLOWNESS_TYPE, d.getSlownessType().toString());
                }
            }
        }
        if (d.getUserName() != null){
            pref.put(USER_NAME, d.getUserName());
        }
    }

    @Override
    protected ExceptionsData read(Preferences pref){
        ExceptionsData data = new ExceptionsData();
        ExceptionsData.DataType type = ExceptionsData.DataType.valueOf(pref.get(DATA_TYPE, ExceptionsData.DataType.STATISTIC.name()));
        data.setDataType(type);
        data.setSubmitId(pref.getInt(ISSUE_ID, 0));
        data.setReportId(pref.getInt(REPORT_ID, 0));
        int reportBeforeReopen = pref.getInt(REPORT_BEFORE_REOPEN_ID, NON_VALUE);
        if (reportBeforeReopen != NON_VALUE){
            data.setReportBeforeReopenId(reportBeforeReopen);
        }
        int issuezilla = pref.getInt(ISSUEZILLA, NON_VALUE);
        if (issuezilla != NON_VALUE){
            data.setIssuezillaId(issuezilla);
        }
        if (data.isSlownessReport()){
            data.setSlownessTime(pref.getLong(TIME, 0));
            data.setLatestAction(pref.get(ACTION_NAME, null));
            data.setSlownessType(pref.get(SLOWNESS_TYPE, null));
        }
        data.setUserName(pref.get(USER_NAME, null));
        return data;
    }
    
}
