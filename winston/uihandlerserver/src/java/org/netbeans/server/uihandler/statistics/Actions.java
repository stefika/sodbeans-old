/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * Portions Copyrighted 2007 Sun Microsystems, Inc.
 */

package org.netbeans.server.uihandler.statistics;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.servlet.jsp.PageContext;
import org.netbeans.server.uihandler.Statistics;
import org.netbeans.server.uihandler.statistics.Actions.Calls;
import org.openide.util.lookup.ServiceProvider;

/** Counts numbers of action invocations.
 *
 * @author Jaroslav Tulach
 */
@ServiceProvider(service=Statistics.class)
public class Actions extends Statistics<Actions.Calls> {
    private static final String UI_ACTION_BUTTON_PRESS = "UI_ACTION_BUTTON_PRESS";
    private static final String UI_ACTION_EDITOR = "UI_ACTION_EDITOR";
    private static final String UI_ACTION_KEY_PRESS = "UI_ACTION_KEY_PRESS";
    private static final Logger LOG = Logger.getLogger(Actions.class.getName());
    static final Pattern DELEGATE = Pattern.compile(".*delegate=([^,]+)[,@].*");
    static final Pattern PROJECT = Pattern.compile(".*Action\\[(.*)\\]");
    static final Pattern DEBUG = Pattern.compile(".*DebuggerAction\\[(.*)\\]");
    static final Pattern LOCALIZED = Pattern.compile(".*CookieDelegateAction\\[(.*)\\]");
    static final Pattern ALL = Pattern.compile("(.*)");
    static final Pattern COMMAND = Pattern.compile(".*cmd=([^,]*),.*");

    public Actions() {
        super("Actions", 1); // NOI18N
    }

    protected Calls newData() {
        return Calls.EMPTY;
    }

    protected Calls process(LogRecord rec) {
        return MapCalls.process(rec);
    }

    protected Calls finishSessionUpload(String userId, int sessionNumber, boolean initialParse, Calls d) {
        return CompressedCalls.compress(d);
    }

    @Override
    protected void registerPageContext(PageContext page, String name, Calls data) {
        page.setAttribute(name, data);
        page.setAttribute("allInvocations", Invocations.values());
        int count = 10;
        String countTxt = (String) page.getAttribute("ActionsCount", PageContext.REQUEST_SCOPE); // NOI18N
        if (countTxt != null) {
            count = Integer.parseInt(countTxt);
        }
        
        Object invObj = page.getAttribute("ActionsInvocation", PageContext.REQUEST_SCOPE); // NOI18N
        Invocations inv = Invocations.ALL;
        if (invObj != null){
            inv = Invocations.valueOf(invObj.toString());
        }
        page.setAttribute(
            name + "Top10", // NOI18N
            data.getTopTen(
                count, 
                (String)page.getAttribute("ActionsIncludes", PageContext.REQUEST_SCOPE),// NOI18N
                (String)page.getAttribute("ActionsExcludes", PageContext.REQUEST_SCOPE), // NOI18N
                inv
            )
        );
    }

    protected Calls join(Calls one, Calls two) {
        if (one == null || one.isEmpty()) {
            return two;
        }
        if (two == null || two.isEmpty()) {
            return one;
        }
        
        HashMap<Name,Integer> join = new HashMap<Name,Integer>(one.getCounts());

        for (Map.Entry<Name, Integer> entry : two.getCounts().entrySet()) {
            int oneValue = entry.getValue() == null ? 0 : entry.getValue();
            
            Integer sndInt = join.get(entry.getKey());
            int twoValue = sndInt == null ? 0 : sndInt;
            
            join.put(entry.getKey(), oneValue + twoValue);
        }
        
        return new MapCalls(join);
    }

    
    static final Invocations invocationWay(LogRecord rec) {
        if (UI_ACTION_EDITOR.equals(rec.getMessage())) {
            String action = param(rec, 0);
            if (action == null){
                return null;
            }
            if (action.matches(".*Toolbar.*")) {
                return Invocations.TOOLBAR;
            } else if (action.matches(".*MenuItem.*")) {
                return Invocations.MENU;
            } else {
                Matcher m = COMMAND.matcher(action);
                if (m.find()) {
                    String cmd = m.group(1);
                    if (cmd.contains("Ctrl") || cmd.contains("Shift") || cmd.contains("Alt")) {
                        return Invocations.SHORTCUT;
                    }
                }
            }
        } else if (UI_ACTION_BUTTON_PRESS.equals(rec.getMessage())) {
            String action = param(rec, 0);
            if (action == null){
                return null;
            }
            if (action.matches(".*Toolbar.*")) {
                return Invocations.TOOLBAR;
            } else if (action.matches(".*MenuItem.*")) {
                return Invocations.MENU;
            }
        } else if (UI_ACTION_KEY_PRESS.equals(rec.getMessage())) {
            return Invocations.SHORTCUT;
        }
        return null;
    }

    public static final String actionName(LogRecord rec) {
        if (UI_ACTION_EDITOR.equals(rec.getMessage())) { // NOI18N
            String name = param(rec, 4);
            if (name != null && name.equals("insert-break")) { // NOI18N
                return null;
            }
            if (name != null && name.length() > 0) {
                return "org.netbeans.editor." + name; // NOI18N
            }
        }


        if (UI_ACTION_BUTTON_PRESS.equals(rec.getMessage()) || // NOI18N
                UI_ACTION_KEY_PRESS.equals(rec.getMessage())) { // NOI18N
            String className = param(rec, 3);
            if ("org.netbeans.modules.project.ui.actions.ProjectAction".equals(className) || // NOI18N
                    "org.netbeans.modules.project.ui.actions.FileCommandAction".equals(className) || // NOI18N
                    "org.netbeans.modules.project.ui.actions.MainProjectAction".equals(className) // NOI18N
                    ) { // NOI18N
                Matcher m = PROJECT.matcher(param(rec, 2));
                if (m.find()) {
                    className = "org.netbeans.modules.project.actions." + m.group(1).replaceAll("&", "").replaceAll(" ", "");
                } else {
                    className = findLocalizedName(rec, 4, ALL);
                }
            }
            if ("org.netbeans.modules.debugger.ui.actions.DebuggerAction".equals(className) // NOI18N
                    ) { // NOI18N
                Matcher m = DEBUG.matcher(param(rec, 2));
                if (m.find()) {
                    className = "org.netbeans.modules.debugger.actions." + m.group(1).replaceAll("&", "").replaceAll(" ", "");
                } else {
                    rec.setLevel(Level.FINE);
                    rec.setMessage("Unknown delegate action: " + className + " msg: " + param(rec, 2));
                    LOG.log(rec);
                    className = null;
                }
            }
            if ("org.openide.util.actions.NodeAction$DelegateAction".equals(className) || // NOI18N
                    "org.openide.util.actions.CallbackSystemAction$DelegateAction".equals(className) || // NOI18N
                    "org.openide.util.actions.CookieAction$CookieDelegateAction".equals(className) // NOI18N
                    ) { // NOI18N
                Matcher m = DELEGATE.matcher(param(rec, 2));
                if (m.find()) {
                    className = m.group(1);
                } else {
                    className = findLocalizedName(rec, 2, LOCALIZED);
                }
            }
            if (className != null) {
                return className;
            }
        }
        return null;
    }
            
    private static String param(LogRecord rec, int index) {
        if (rec.getParameters() == null) {
            return null;
        }
        if (rec.getParameters().length <= index) {
            return null;
        }
        if (! (rec.getParameters()[index] instanceof String)) {
            return null;
        }
        return (String)rec.getParameters()[index];
    }
    private static String findLocalizedName(LogRecord rec, int index, Pattern pat) {
        Matcher m = pat.matcher(param(rec,index));
        if (m.find()) {
            ResourceBundle rb = ResourceBundle.getBundle("org.netbeans.server.uihandler.statistics.ActionsConvert");
            try {
                return rb.getString(m.group(1).replace("&", "").replaceAll(" ", "")); // NOI18N
            } catch (MissingResourceException missingResourceException) {
                // ignore
            }
        }

        rec.setLevel(Level.FINE);
        rec.setMessage("Unknown delegate action: " + param(rec, index));
        LOG.log(rec);

        return null;
    }
    
    public static abstract class Calls {
        static final Calls EMPTY = new MapCalls();

        protected abstract boolean isEmpty();
        protected abstract Map<Name,Integer> getCounts();
        
        /** Computes a map mapping names of each action to number of its
         * invocations. Includes at most <code>max</code> number of elements,
         * and only those that match the includes and do not match the excludes.
         * 
         * @param max maximum number of elements in the result map
         * @param includes a regular expression on the FQ class names defining what to include, null if include everything
         * @param excludes a regular expression on the FQ class names defining what to exclude, null if there are no excludes
         * @param inv  an item of invocations that should be filtered out
         * @return map from class names to number of their invocations sorted by the number of invocations
         */
        public Map<String,Integer> getTopTen(int max, String includes, String excludes, Invocations inv) {
            
            Pattern pExcludes = null;
            if (excludes != null) {
                try {
                    pExcludes = Pattern.compile(excludes);
                } catch (java.util.regex.PatternSyntaxException ex) {
                    LOG.log(Level.WARNING, ex.getMessage(), ex);
                }
            }
            Pattern pIncludes = null;
            if (includes != null) {
                try {
                    pIncludes = Pattern.compile(includes);
                } catch (java.util.regex.PatternSyntaxException ex) {
                    LOG.log(Level.WARNING, ex.getMessage(), ex);
                }
            }

            Map<String, Integer> cupples = new TreeMap<String, Integer>();
            for (Map.Entry<Name, Integer> entry : this.getCounts().entrySet()) {
                String actionName = entry.getKey().fullName();
                if (pExcludes != null && pExcludes.matcher(actionName).matches()) {
                    continue;
                }
                if (pIncludes != null && !pIncludes.matcher(actionName).matches()) {
                    continue;
                }
                if (!(inv.equals(Invocations.ALL) || inv.equals(entry.getKey().invocation))) {
                    continue;
                }
                Integer previous = cupples.get(actionName);
                if (previous == null){
                    cupples.put(actionName, entry.getValue());
                }else{
                    cupples.put(actionName, entry.getValue() + previous);
                }
            }

            LinkedList<String> names = new LinkedList<String>();
            LinkedList<Integer> counts = new LinkedList<Integer>();
            for (Map.Entry<String, Integer> entry : cupples.entrySet()) {
                if (counts.size() == max && entry.getValue() <= counts.getLast()) {
                    continue;
                }
                String actionName = entry.getKey();

                ListIterator<String> itNames = names.listIterator();
                ListIterator<Integer> itCounts = counts.listIterator();
                for(;;) {
                    if (!itCounts.hasNext()) {
                        // last insert
                        names.add(actionName);
                        counts.add(entry.getValue());
                        break;
                    }

                    Integer cnt = itCounts.next();
                    itNames.next();

                    if (cnt < entry.getValue()) {
                        itCounts.previous();
                        itNames.previous();

                        itCounts.add(entry.getValue());
                        itNames.add(actionName);
                        break;
                    }
                }

                if (counts.size() > max) {
                    counts.removeLast();
                    names.removeLast();
                }
            }


            LinkedHashMap<String,Integer> res = new LinkedHashMap<String, Integer>();
            ListIterator<String> itNames = names.listIterator();
            ListIterator<Integer> itCounts = counts.listIterator();
            while(itCounts.hasNext()) {
                Integer integer =  itCounts.next();
                if (integer == 0) {
                    continue;
                }
                String name = itNames.next();
                
                res.put(name, integer);
            }

            return res;
        }


    } // end of Calls

    private static final class MapCalls extends Calls {
        final Map<Name,Integer> counts;
        
        MapCalls() {
            counts = Collections.emptyMap();
        }
        MapCalls(String className, Invocations invocation) {
            if (invocation == null){
                invocation = Invocations.ALL;
            }
            counts = Collections.singletonMap(Name.create(className, invocation), 1);
        }
        MapCalls(Map<Name,Integer> c) {
            counts = c;
        }
        
        static Calls process(LogRecord rec) {
            String name = actionName(rec);
            if (name != null){
                Invocations invocationWay = invocationWay(rec);
                return new MapCalls(name, invocationWay);
            }
            return Calls.EMPTY;
        }
        
        
        protected final boolean isEmpty() {
            return counts.isEmpty();
        }
        
        protected final Map<Name,Integer> getCounts() {
            return counts;
        }
    } // end of MapCalls
    
    static final class CompressedCalls extends Calls {
        static final Map<Name,Integer> indexer = new HashMap<Name,Integer>();
        static final List<Name> deindexer = new ArrayList<Actions.Name>();
        private final Object counts;

        private CompressedCalls(Object counts) {
            this.counts = counts;
        }
        
        private int getLength() {
            if (counts instanceof byte[]) {
                return ((byte[])counts).length;
            }
            if (counts instanceof short[]) {
                return ((short[])counts).length;
            }
            if (counts instanceof int[]) {
                return ((int[])counts).length;
            }
            throw new IllegalStateException("Wrong array: " + counts); // NOI18N
        }
        
        private int getAt(int index) {
            if (counts instanceof byte[]) {
                return ((byte[])counts)[index];
            }
            if (counts instanceof short[]) {
                return ((short[])counts)[index];
            }
            if (counts instanceof int[]) {
                return ((int[])counts)[index];
            }
            throw new IllegalStateException("Wrong array: " + counts); // NOI18N
        }
        
        
        protected final boolean isEmpty() {
            return getLength() == 0;
        }
        
        protected final Map<Name,Integer> getCounts() {
            HashMap<Name,Integer> map = new HashMap<Name,Integer>();
            
            int cnt = getLength();
            for (int i = 0; i < cnt; i++) {
                map.put(deindexer.get(i), getAt(i));
            }
            return map;
        }

        public static synchronized Calls compress(Calls calls) {
            if (calls == null || calls.isEmpty()) {
                return Calls.EMPTY;
            } else {
                int maxCounts = 0;
                int maxIndex = -1;
                Map<Name,Integer> mapCounts = calls.getCounts();
                for (Map.Entry<Name, Integer> entry : mapCounts.entrySet()) {
                    if (entry.getValue() != null && entry.getValue() > maxCounts) {
                        maxCounts = entry.getValue();
                    }
                    Integer indx = indexer.get(entry.getKey());
                    if (indx == null) {
                        indx = indexer.size();
                        indexer.put(entry.getKey(), indx);
                        deindexer.add(entry.getKey());
                        assert deindexer.size() == indx + 1;
                    }
                    if (indx > maxIndex) {
                        maxIndex = indx;
                    }
                }

                Object arr;
                if (maxCounts < Byte.MAX_VALUE) {
                    arr = new byte[maxIndex + 1];
                } else if (maxIndex < Short.MAX_VALUE) {
                    arr = new short[maxIndex + 1];
                } else {
                    arr = new int[maxIndex + 1];
                }
                for (Map.Entry<Name, Integer> entry : mapCounts.entrySet()) {
                    Integer indx = indexer.get(entry.getKey());
                    assert indx != null;
                    if (arr instanceof byte[]) {
                        ((byte[])arr)[indx] = (byte)entry.getValue().intValue();
                        continue;
                    }
                    if (arr instanceof short[]) {
                        ((short[])arr)[indx] = (short)entry.getValue().intValue();
                        continue;
                    }
                    if (arr instanceof int[]) {
                        ((int[])arr)[indx] = entry.getValue().intValue();
                        continue;
                    }
                    throw new IllegalStateException("Wrong array: " + arr);
                }
                
                return new CompressedCalls(arr);
            }
        }
    }
    
    protected static final class Name {
        private final String pkg;
        private final String clazz;
        private final Invocations invocation;
        
        private Name(String pkg, String clazz, Invocations invocation) {
            assert (invocation != null);
            this.pkg = pkg;
            this.clazz = clazz;
            this.invocation = invocation;
        }
        
        public static Name parse(String value) {
            int last = value.lastIndexOf(':');
            return create(value.substring(0, last), Invocations.valueOf(value.substring(last+1)));
        }

        public static Name create(String fullClassName, Invocations invocation) {
            int last = fullClassName.lastIndexOf('.');
            if (last <= 0) {
                return new Name("", fullClassName.intern(), invocation);
            } else {
                return new Name(fullClassName.substring(0, last).intern(), fullClassName.substring(last + 1).intern(), invocation);
            }
        }

        @Override
        public boolean equals(Object obj) {
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final Name other = (Name) obj;
            if (this.pkg != other.pkg && (this.pkg == null || !this.pkg.equals(other.pkg))) {
                return false;
            }
            if (this.clazz != other.clazz && (this.clazz == null || !this.clazz.equals(other.clazz))) {
                return false;
            }
            if (!this.invocation.equals(other.invocation)){
                return false;
            }
            return true;
        }

        @Override
        public int hashCode() {
            int hash = 5;
            hash = 67 * hash + this.clazz != null ? this.clazz.hashCode() : 0;
            return hash;
        }

        public String fullName(){
            if (pkg == null) {
                return clazz;
            }
            return pkg + '.' + clazz;
        }

        @Override
        public String toString() {
            if (pkg == null) {
                return clazz;
            }
            return pkg + '.' + clazz +":"+ invocation.toString();
        }
    }

    protected void write(Preferences pref, Calls d) throws BackingStoreException {
        if (d.isEmpty()) {
            return;
        }
        for (Map.Entry<Name, Integer> entry : d.getCounts().entrySet()) {
            String actionName = entry.getKey().toString();
            if (actionName.length() > Preferences.MAX_KEY_LENGTH) {
                actionName = actionName.substring(actionName.length() - Preferences.MAX_KEY_LENGTH);
            }
            pref.putInt(actionName, entry.getValue());
        }
    }

    protected Calls read(Preferences pref) throws BackingStoreException {
        Map<Name, Integer> counts = new HashMap<Actions.Name, Integer>();
        for (String k : pref.keys()) {
            int cnt = pref.getInt(k, 0);
            if (cnt > 0) {
                counts.put(Name.parse(k), cnt);
            }
        }
        return counts.isEmpty() ? Calls.EMPTY : new MapCalls(counts);
    }

    protected static enum Invocations {
        MENU, TOOLBAR, SHORTCUT, /*QUICK_SEARCH, */ALL
    };
}
