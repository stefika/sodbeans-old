/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * Copyright 2007 Sun Microsystems, Inc. All rights reserved.
 */

package org.netbeans.server.uihandler.statistics;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.logging.LogRecord;
import java.util.prefs.Preferences;
import javax.servlet.jsp.PageContext;
import org.netbeans.server.uihandler.*;
import java.util.logging.Logger;
import java.util.prefs.BackingStoreException;
import org.netbeans.modules.exceptions.entity.Logfile;
import org.netbeans.server.uihandler.statistics.Help.Invocations;
import org.openide.util.lookup.ServiceProvider;

/** Counts the number of used projecs and project types.
 *
 * @author Jaroslav Tulach
 */
@ServiceProvider(service=Statistics.class)
public final class Help extends Statistics<Help.Invocations> {
    static final Logger LOG = Logger.getLogger(Help.class.getName());
    
    public Help() {
        super("Help", 2);
    }
    
    protected Invocations newData() {
        return new Invocations(Collections.<String,Integer>emptyMap());
    }

    protected Invocations process(LogRecord rec) {
        if ("LOG_SHOWING_HELP".equals(rec.getMessage())) { // NOI18N
            String id = (String)rec.getParameters()[0];
            return new Invocations(Collections.<String,Integer>singletonMap(id, 1));
        }
        return null;
    }

    protected Invocations finishSessionUpload(String userId, int sessionNumber,
                                        boolean initialParse, Help.Invocations d) {
        Logfile l = LogFileTask.getRunningTask().getLogInfo();
        Long build = l == null ? null : l.getBuildnumber();
        if (build == null || build <= 70416L) {
            // ignore old builds, those created before April 16, 2007
            return null;
        }
        
        
        if (d.amounts.isEmpty()) {
            return new Invocations(d.amounts, 1, 0);
        } else {
            return new Invocations(d.amounts, 1, 1);
        }
    }

    protected Invocations join(Help.Invocations one, Help.Invocations two) {
        if (one == null) {
            return two;
        }
        if (two == null) {
            return one;
        }
        
        HashMap<String,Integer> sum = new HashMap<String, Integer>(one.amounts);
        for (Map.Entry<String, Integer> en : two.amounts.entrySet()) {
            Integer prev = sum.get(en.getKey());
            int future = prev == null ? en.getValue() : prev + en.getValue();
            sum.put(en.getKey(), future);
        }
        return new Invocations(sum, one.allSessions + two.allSessions, one.thoseThatUsedHelp + two.thoseThatUsedHelp);
    }

    @Override
    protected void registerPageContext(PageContext page, String name,
                                       Invocations data) {
        super.registerPageContext(page, name, data);
        
        if (name.equals("globalHelp")) {
            Map<String,Integer> list = new TreeMap<String,Integer>();
            list.put("Help Lovers", data.thoseThatUsedHelp);
            list.put("Ignores Help", data.allSessions - data.thoseThatUsedHelp);
            page.setAttribute("globalHelpRatio", list);
        }
    }
    
    public static final class Invocations {
        private final Map<String,Integer> amounts;
        private final int allSessions;
        private final int thoseThatUsedHelp;
        
        private Invocations(Map<String,Integer> amounts) {
            this.amounts = amounts;
            this.allSessions = 0;
            this.thoseThatUsedHelp = 0;
        }

        public Invocations(Map<String, Integer> amounts, int allSessions, int thoseThatUsedHelp) {
            this.amounts = amounts;
            this.allSessions = allSessions;
            this.thoseThatUsedHelp = thoseThatUsedHelp;
        }
        
        public Set<One> getTopTen() {
            TreeSet<One> set = new TreeSet<One>(Collections.reverseOrder());
            for (Map.Entry<String, Integer> en : amounts.entrySet()) {
                set.add(new One(en.getKey(), en.getValue()));
                if (set.size() == 11) {
                    set.remove(set.last());
                }
            }
            return set;
        }
        
        public static final class One implements Comparable<One> {
            private final String id;
            private final int cnt;

            One(String id, int cnt) {
                this.id = id;
                this.cnt = cnt;
            }

            public String getId() {
                return id;
            }

            public int getCnt() {
                return cnt;
            }

            public int compareTo(One other) {
                if (cnt != other.cnt) {
                    return cnt - other.cnt; 
                }
                return id.compareTo(other.id);
            }
        }
    }

    protected void write(Preferences pref, Invocations d) {
        if (d != null) {
            pref.putInt("allSessions", d.allSessions);
            pref.putInt("thoseThatUsedHelp", d.thoseThatUsedHelp);
            for (Map.Entry<String, Integer> entry : d.amounts.entrySet()) {
                pref.putInt(s32(entry.getKey()), entry.getValue());
            }
        }
    }
    
    private static String s32(String s) {
        if (s.length() <= Preferences.MAX_KEY_LENGTH) {
            return s;
        } else {
            return s.substring(s.length() - Preferences.MAX_KEY_LENGTH);
        }
    }

    protected Invocations read(Preferences pref) throws BackingStoreException {
        int allSessions = pref.getInt("allSessions", -1);
        if (allSessions == -1) {
            return null;
        }
        
        Map<String,Integer> amounts = new HashMap<String, Integer>();
        for (String k : pref.keys()) {
            if ("allSessions".equals(k) || "thoseThatUsedHelp".equals(k)) {
                continue;
            }
            
            int cnt = pref.getInt(k, 0);
            if (cnt > 0) {
                amounts.put(k, cnt);
            }
        }
        return new Invocations(
            amounts, 
            allSessions, 
            pref.getInt("thoseThatUsedHelp", 0)
        );
    }

}
