/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * Copyright 2007 Sun Microsystems, Inc. All rights reserved.
 */

package org.netbeans.server.uihandler.statistics;

import java.util.ArrayList;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;
import org.netbeans.server.uihandler.*;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import javax.servlet.jsp.PageContext;
import org.netbeans.lib.uihandler.ProjectOp;
import org.netbeans.server.uihandler.statistics.CoProjects.Sheet;
import org.openide.util.lookup.ServiceProvider;

/** Which projects types are used together with each other.
 *
 * @author Jaroslav Tulach
 */
@ServiceProvider(service=Statistics.class)
public class CoProjects extends Statistics<CoProjects.Sheet> {
    static final Logger LOG = Logger.getLogger(CoProjects.class.getName());
    
    public CoProjects() {
        super(1);
    }
    
    protected Sheet newData() {
        return Sheet.EMPTY;
    }

    protected Sheet process(LogRecord rec) {
        ProjectOp op = ProjectOp.valueOf(rec);
        if (op != null) {
            ProjectChange ch = new ProjectChange(op, rec.getMillis());
            List<ProjectChange> list = Collections.singletonList(ch);
            return new Sheet(list);
        }
        return Sheet.EMPTY;
    }

    protected Sheet finishSessionUpload(
        String userId, int sessionNumber, boolean initialParse, Sheet d
    ) {
        // just keep the technology counts, no sequences of operations
        return new Sheet(d.getTechnologies());
    }

    protected Sheet join(Sheet one, Sheet two) {
        List<ProjectChange> oneOp = one.getOperations();
        List<ProjectChange> twoOp = two.getOperations();
        
        if (oneOp != null && twoOp != null) {

            if (oneOp.isEmpty()) {
                return new Sheet(twoOp);
            }
            if (twoOp.isEmpty()) {
                return new Sheet(oneOp);
            }

            List<ProjectChange> both = new ArrayList<ProjectChange>(oneOp);
            both.addAll(twoOp);
            return new Sheet(both);
        } else {
            return new Sheet(mergeCountMaps(one.getTechnologies(), two.getTechnologies()));
        }
    }
    
    @Override
    protected void registerPageContext(PageContext page, String name, Sheet data) {
        page.setAttribute(name, data);
        
        int minimum = 0;
        String countTxt = (String)page.getAttribute("minimum", PageContext.REQUEST_SCOPE); // NOI18N
        if (countTxt != null) {
            minimum = Integer.parseInt(countTxt);
        }
        
        String excludes = (String)page.getAttribute("excludes", PageContext.REQUEST_SCOPE); // NOI18N
        
        page.setAttribute(
            name, // NOI18N
            data.getPercentages(
                minimum, 
                (String)page.getAttribute("includes", PageContext.REQUEST_SCOPE),// NOI18N
                excludes
            )
        );
    }

    protected void write(Preferences pref, Sheet d) {
        for (Map.Entry<String, Integer> entry : d.getTechnologies().entrySet()) {
            String key = entry.getKey();
            while (key.length() > Preferences.MAX_KEY_LENGTH){
                int index = key.indexOf(' ');
                key = key.substring(index + 1);
            }
            pref.putInt(key, entry.getValue());
        }
    }

    protected Sheet read(Preferences pref) throws BackingStoreException {
        Map<String,Integer> amounts = new HashMap<String, Integer>();
        for (String k : pref.keys()) {
            int cnt = pref.getInt(k, 0);
            if (cnt > 0) {
                amounts.put(k, cnt);
            }
        }
        return new Sheet(amounts);
    }
    
    private static Map<String,Integer> mergeCountMaps(Map<String,Integer> one, Map<String,Integer> two) {
        TreeMap<String,Integer> both = new TreeMap<String, Integer>();
        both.putAll(one);
        for (Map.Entry<String,Integer> entry : two.entrySet()) {
            Integer prev = both.get(entry.getKey());
            if (prev == null) {
                both.put(entry.getKey(), entry.getValue());
            } else {
                both.put(entry.getKey(), entry.getValue() + prev);
            }
        }
        return both;
    }
    
    public static final class Sheet {
        static final Sheet EMPTY = new Sheet(Collections.<ProjectChange>emptyList());
        static final int DELTA = 60 * 1000; // 60s
        
        private final List<ProjectChange> operations;
        private Map<String,Integer> technologies;
        
        Sheet(List<ProjectChange> operations) {
            assert operations != null;
            this.operations = operations;
            this.technologies = null;
        }
        Sheet(Map<String,Integer> technologies) {
            assert technologies != null;
            this.operations = null;
            this.technologies = technologies;
        }

        List<ProjectChange> getOperations() {
            return operations;
        }

        Map<String,Integer> getTechnologies() {
            if (technologies != null) {
                return technologies;
            }

            TreeMap<String,Integer> set = new TreeMap<String,Integer>();
            Map<String,int[]> projects = new TreeMap<String,int[]>();
            Boolean initializing = null;

            long last = 0L;
            for (ProjectChange un : operations) {
                    if (un.op.isStartup()) {
                        if (Boolean.FALSE.equals(initializing)) {
                            projects.clear();
                        }
                        initializing = Boolean.TRUE;
                    } else {
                        initializing = Boolean.FALSE;
                    }

                    if (Boolean.FALSE.equals(initializing) && last + DELTA < un.time) {
                        countChange(set, projects);
                        last = un.time;
                    }

                    String prjName = ProjectTypes.projectName(un.op);
                    int[] cnt = projects.get(prjName);
                    if (cnt == null) {
                        cnt = new int[1];
                        projects.put(prjName, cnt);
                    }
                    cnt[0] += un.op.getDelta();
                    if (cnt[0] <= 0) {
                        projects.remove(prjName);
                    }
            }
            countChange(set, projects);
            technologies = set;
            return set;
        }

        private void countChange(TreeMap<String, Integer> set, Map<String,int[]> projects) {
            if (projects.isEmpty()) {
                return;
            }

            // some used technology
            StringBuffer prjNames = new StringBuffer();
            String sep = "";
            for (String prj : projects.keySet()) {
                prjNames.append(sep).append(prj);
                sep = " ";
            }

            set.put(prjNames.toString().intern(), 1);
        }

        /** Computes a map mapping names of each action to number of its
         * invocations. Includes at most <code>max</code> number of elements,
         * and only those that match the includes and do not match the excludes.
         * 
         * @param max maximum number of elements in the result map
         * @param includes a regular expression on the FQ class names defining what to include, null if include everything
         * @param excludes a regular expression on the FQ class names defining what to exclude, null if there are no excludes
         * @return map from class names to number of their invocations sorted by the number of invocations
         */
        public Map<String,Integer> getPercentages(int minimum, String includes, String excludes) {
            LinkedList<String> names = new LinkedList<String>();
            LinkedList<Integer> counts = new LinkedList<Integer>();
            
            Pattern pExcludes = null;
            if (excludes != null) {
                try {
                    pExcludes = Pattern.compile(excludes);
                } catch (java.util.regex.PatternSyntaxException ex) {
                    LOG.log(Level.WARNING, ex.getMessage(), ex);
                }
            }
            Pattern pIncludes = null;
            if (includes != null) {
                try {
                    pIncludes = Pattern.compile(includes);
                } catch (java.util.regex.PatternSyntaxException ex) {
                    LOG.log(Level.WARNING, ex.getMessage(), ex);
                }
            }

            int sum = 0;
            for (Map.Entry<String, Integer> entry : this.getTechnologies().entrySet()) {
                String actionName = entry.getKey();
                if (pExcludes != null && pExcludes.matcher(actionName).matches()) {
                    continue;
                }
                if (pIncludes != null && !pIncludes.matcher(actionName).matches()) {
                    continue;
                }
                
                sum += entry.getValue();
                
                ListIterator<String> itNames = names.listIterator();
                ListIterator<Integer> itCounts = counts.listIterator();
                for(;;) {
                    if (!itCounts.hasNext()) {
                        // last insert
                        names.add(actionName);
                        counts.add(entry.getValue());
                        break;
                    }
                    
                    Integer cnt = itCounts.next();
                    itNames.next();
                    
                    if (cnt < entry.getValue()) {
                        itCounts.previous();
                        itNames.previous();
                        
                        itCounts.add(entry.getValue());
                        itNames.add(actionName);
                        break;
                    }
                }
            }

            
            Map<String,Integer> res = new LinkedHashMap<String, Integer>();
            ListIterator<String> itNames = names.listIterator();
            ListIterator<Integer> itCounts = counts.listIterator();
            while(itCounts.hasNext()) {
                Integer integer = itCounts.next();
                String name = itNames.next();
                int percent = integer * 100 / sum;
                if (percent < minimum) {
                    continue;
                }
                
                res.put(name, percent);
            }

            return res;
        }
    } // end of Sheet

    private static final class ProjectChange {
        final ProjectOp op;
        final long time;

        public ProjectChange(ProjectOp op, long time) {
            this.op = op;
            this.time = time;
        }
    }


}

