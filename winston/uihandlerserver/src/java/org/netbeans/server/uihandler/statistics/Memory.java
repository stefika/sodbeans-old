/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * Copyright 2007 Sun Microsystems, Inc. All rights reserved.
 */

package org.netbeans.server.uihandler.statistics;

import java.text.MessageFormat;
import java.util.Arrays;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;
import javax.servlet.jsp.PageContext;
import org.netbeans.server.uihandler.*;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.LogRecord;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.openide.util.lookup.ServiceProvider;

/** Computes how much physical memory is used.
 *
 * @author Radim Kubacki
 */
@ServiceProvider(service=Statistics.class)
public final class Memory extends Statistics<Map<Long,Integer>> {
    
    private static List<MemBean> convertToDataset(Map<Long, Integer> logs, long min, long max, int cnt) {
        if (cnt < 2) {
            cnt = 2;
        }
        if (min < 0) {
            min = 0;
        }
        if (max < min) {
            max = min + 1024 * 1024;
        }
        long step = (max - min) / (cnt - 1);
        
        int[] counts = new int[cnt];
        
        for (Map.Entry<Long, Integer> entry: logs.entrySet()) {
            long mem = entry.getKey();
            
            int index;
            if (mem < min) {
                index = 0;
            } else if (mem >= max) {
                index = cnt - 1;
            } else {
                index = (int)((mem - min + step / 2) / step);
            }
            assert index >= 0 && index < cnt;
            counts[index] += entry.getValue();
        }
        
        MemBean[] res = new MemBean[cnt];
        for (int i = 0; i < cnt; i++) {
            long mem = min + step * i;
            MemBean mb = new MemBean(mem);
            res[i] = mb;
            String memLabel = "";
            if (mem > 1024 * 1024 * 1024) {
                memLabel = MessageFormat.format("{0,number,###0.###} GB", mem / 1024.0 / 1024.0 / 1024.0);
            } else if (mem > 1024 * 1024) {
                memLabel = MessageFormat.format("{0,number,###0.###} MB", mem / 1024.0 / 1024.0);
            } else {
                memLabel = MessageFormat.format("{0,number,###0} KB", mem / 1024.0);
            }
            mb.setMemory(memLabel);
            mb.setCount(counts[i]);
        }
        
        return Arrays.asList(res);
    }
    
    public Memory() {
        super("Memory", 5);
    }

    protected Map<Long, Integer> newData() {
        return Collections.emptyMap();
    }

    protected Map<Long, Integer> process(LogRecord rec) {
        if ("MEMORY".equals(rec.getMessage())) {
            Long memory = Long.valueOf((String)rec.getParameters()[0]);
            memory = (memory / 1024) * 1024; // round by KB
            return Collections.singletonMap(memory, 1);
        }else {
            return Collections.emptyMap();
        }
    }

    protected Map<Long, Integer> join(
            Map<Long, Integer> one, Map<Long, Integer> two) {
        
        Map<Long, Integer> merge = new TreeMap<Long, Integer>(one);
        for (Map.Entry<Long, Integer> entry: two.entrySet()) {
            Long key = entry.getKey();
            if (merge.containsKey(key)) {
                merge.put(key, entry.getValue() + merge.get(key));
            }else {
                merge.put(entry.getKey(), entry.getValue());
            }
        }
        return merge;
    }

    protected Map<Long, Integer> finishSessionUpload(String userId, int sessionNumber, boolean initialParse, Map<Long, Integer> d) {
        return d;
    }

    @Override
    protected void write(Preferences pref, Map<Long, Integer> d) throws BackingStoreException {
        for (Map.Entry<Long, Integer> entry : d.entrySet()) {
            pref.putLong(entry.getKey().toString(), entry.getValue());
        }
    }

    @Override
    public Map<Long, Integer> read(Preferences pref) throws BackingStoreException {
        Map<Long, Integer> map = new TreeMap<Long, Integer>();
        for (String key : pref.keys()) {
            map.put(Long.parseLong(key), pref.getInt(key, 0));
        }
        return map;
    }

    @Override
    protected void registerPageContext(PageContext page, String name, Map<Long, Integer> data) {
        long min = toNumber(page, "minimum", 0);
        long max = toNumber(page, "maximum", 0);
        int cnt = (int)toNumber(page, "columns", 9);
        if (max <= min) {
            max = min + 4L * 1024 * 1024 * 1024;
        }
        
        page.setAttribute(name, convertToDataset(data, min, max, cnt));
    }
    
    private static Pattern SIZE = Pattern.compile(" *([0-9\\.]*) *(MB|KB|GB|TB|) *", Pattern.CASE_INSENSITIVE);
    private static long toNumber(PageContext page, String name, long def) {
        String count = (String)page.getAttribute(name, PageContext.REQUEST_SCOPE); // NOI18N
        if (count == null) {
            return def;
        }
        Matcher m = SIZE.matcher(count);
        if (!m.matches()) {
            return def;
        }
        long value = Long.parseLong(m.group(1));
        long multi = 1;
        if ("KB".equalsIgnoreCase(m.group(2))) {
            multi = 1024;
        }
        if ("MB".equalsIgnoreCase(m.group(2))) {
            multi = 1024 * 1024;
        }
        if ("GB".equalsIgnoreCase(m.group(2))) {
            multi = 1024 * 1024 * 1024;
        }
        if ("TB".equalsIgnoreCase(m.group(2))) {
            multi = 1024 * 1024 * 1024 * 1024;
        }
        return value * multi;
    }

    public static class MemBean implements Comparable<MemBean> {
        private long mem;
        private int count;
        private String memory;
        
        public MemBean(long mem) {
            this.mem = mem;
        }
        
        public String getMemory() { return memory; }
        void setMemory(String mem) { memory = mem; }
        
        public int getCount() { return count; }
        void setCount(int count) { this.count = count; }
        
        public String getSerie() { return ""; }

        @Override
        public String toString() {
            return "Memory: "+memory+" # of users: "+count;
        }

        public int compareTo(MemBean o) {
            if (mem == o.mem) {
                return 0;
            }
            return mem < o.mem ? -1 : 1;
        }
    }
}
    
