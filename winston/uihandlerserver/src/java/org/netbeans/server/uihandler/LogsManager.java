/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */
package org.netbeans.server.uihandler;

import java.io.File;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import org.netbeans.modules.exceptions.entity.Logfile;
import java.util.concurrent.ExecutionException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.concurrent.Semaphore;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;
import javax.persistence.EntityManager;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.PageContext;
import org.netbeans.modules.exceptions.entity.LogfileParsed;
import org.netbeans.modules.exceptions.entity.Nbversion;
import org.netbeans.modules.exceptions.entity.Prefname;
import org.netbeans.modules.exceptions.entity.Prefvalue;
import org.netbeans.modules.exceptions.entity.ProductVersion;
import org.netbeans.modules.exceptions.entity.Statistic;
import org.netbeans.modules.exceptions.utils.PersistenceUtils;
import org.netbeans.web.Persistable;
import org.netbeans.web.Persistable.TransactionResult;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.NbCollections;
import org.openide.util.RequestProcessor;
import org.openide.util.Task;
import org.openide.util.TaskListener;

/**
 *
 * @author Jaroslav Tulach
 * @author Jindrich Sedek
 */
public final class LogsManager extends Object {

    private static final int LATEST_TASKS_MAX_SIZE = 20;
    private static final String ALL_VERSIONS = "All";
    private static LogsManager DEFAULT;
    static final Logger LOG = Logger.getLogger(LogsManager.class.getName());
    private final Map<String, VersionData> versionStatistics;
    private File dir;
    private transient RequestProcessor.Task initTask;
    /** map from userdir to it's latest task */
    private final Map<String, LogFileTask> latestTasks;
    private transient volatile boolean closed = false;
    /** statistics to work on and their global values */
    private List<Value> statistics;
    /** how many data has already been parsed */
    private LogCounts logCounts;
    /** executor to use for changing data */
    private final RequestProcessor EXEC;
    /** executor for load/store global and version data*/
    private final RequestProcessor globalRP = new RequestProcessor("global data load/store");

    private LogsManager(File userDir) {
        EXEC = new RequestProcessor("LogsManager: " + userDir);
        latestTasks = Collections.synchronizedMap(new LinkedHashMap<String, LogFileTask>());
        versionStatistics = Collections.synchronizedMap(new TreeMap<String, VersionData>());
        this.dir = userDir;
        this.logCounts = new LogCounts(dir);
        this.statistics = new ArrayList<LogsManager.Value>();
        if (dir == null){
            return ;
        }
        LogFileTask.closed = false;
        for (Statistics<?> s : Lookup.getDefault().lookupAll(Statistics.class)) {
            addStatisticsData(this.statistics, s);
            Statistic.loadIntoCache(s.name);
        }
        if (!closed){
            this.initTask = postVerificationTask(new Verify());
        }


        DEFAULT = this;
    }

    /** For use from tests 
     * @param userDir the dir with log files
     * @return new instance of manager
     */
    static LogsManager createManager(File userDir) {
        if (DEFAULT != null){
            DEFAULT.close();
        }
        return new LogsManager(userDir);
    }

    /** Getter for the default LogsManager. Location of directories it shall operate
     * on is looked up as "java:comp/env/uilogger/dir" from initial naming context.
     *
     * @return an instance of default manager
     * @throws IllegalStateException if the directory cannot be obtained
     */
    public static synchronized LogsManager getDefault() {
        if (DEFAULT == null) {
            String dir = Utils.getVariable("dir", String.class); // NOI18N
            if (dir == null){
                DEFAULT = createManager(null);
            }else{
                DEFAULT = createManager(new java.io.File(dir));
            }
        }
        return DEFAULT;
    }

    public static void closeInstance(){
        if (DEFAULT != null){
            DEFAULT.close();
        }
        DEFAULT = null;
    }
    /** Close the LogsManager, stop parsing, etc. get ready for shutdown.
     */
    public void close() {
        LOG.info("Shutdown");
        LogFileTask.closed = true;
        this.closed = true;
        EXEC.stop();
        if (initTask != null){
            boolean b = this.initTask.cancel();
            LOG.log(java.util.logging.Level.INFO, "Cancel of init task: {0}", b);
        }
        try {
            this.initTask.waitFinished(10000);
        } catch (Exception ex) {
            // ignore, we just need to wait a bit till the task finishes
            LOG.log(Level.INFO, "Timeout while waiting for parsing task to finish");
        }
        Statistic.dropCache();
        Prefvalue.dropCache();
        Prefname.dropCache();
        LOG.log(java.util.logging.Level.INFO, "shutdown");
    }

    private boolean isDirInitialized(){
        if (dir == null) {
            LOG.warning("Specify dir attribute, otherwise the server cannot work");
            return false;
        }
        return true;
    }
    public void addLog(final File f, final String remoteHost) {
        if (!isDirInitialized()){
            return;
        }

        LOG.log(Level.INFO, "request for parsing {0}", f);
        LogFileTask newFileTask = new LogFileTask(f, remoteHost, new LogFileTask.StatisticsGetter() {

            public List<Value> getStatistics() {
                return statistics;
            }
        });
        newFileTask.addTaskListener(new TaskListener() {
            public void taskFinished(Task arg0) {
                logCounts.addParsedLog();
            }
        });

        String userdirId = LogFileTask.getIdSes(f.getName());
        if (latestTasks.containsKey(userdirId)){
            latestTasks.remove(userdirId);
        }
        latestTasks.put(userdirId, newFileTask);
        while (latestTasks.size() > LATEST_TASKS_MAX_SIZE){
            Iterator<Entry<String, LogFileTask>> it = latestTasks.entrySet().iterator();
            for (int i = 0; i < LATEST_TASKS_MAX_SIZE / 2; i++){
                it.next();
                it.remove();
            }
        }

        logCounts.addNewLog();
        initTask = EXEC.post(newFileTask, 0, Thread.MAX_PRIORITY);
    }

    /**
     * Getter for number of all submitted logs.
     * @return number of log files in the logs directory.
     */
    public int getNumberOfLogs(){
        return logCounts.getLogsCount();
    }

    /** 
     * Getter for the amount of parsed logs
     * @return count of parsed logs
     */
    public int getParsedLogs() {
        return logCounts.getParsedLogs();
    }

    RequestProcessor.Task postVerificationTask(Runnable runnable){
        if (closed) {
            return null;
        }
        RequestProcessor.Task result = EXEC.post(runnable, Utils.getParsingTimeout(), Thread.MIN_PRIORITY);
        initTask = result;
        return result;
    }

    void setVersionStatistics(Nbversion version, List<Value> values, int counts){
        VersionData data = new VersionData(values, counts);
        versionStatistics.put(version.getVersion(), data);
    }

    VersionData getVersionStatistics(Nbversion version){
        return versionStatistics.get(version.getVersion());
    }

    String[] getPossibleVersions() {
        Collection<String> vers = new ArrayList<String>(versionStatistics.keySet());
        vers.add(ALL_VERSIONS);
        return vers.toArray(new String[0]);
    }

    /**
     * Takes a page context and fills it with informations about all statistics
     * known to the system.
     *
     * @param context the context to fill
     * @param statistics names of statistics to include in the page
     * @throws java.lang.InterruptedException
     * @throws java.util.concurrent.ExecutionException
     */
    public void preparePageContext(PageContext context, Set<String> statistics)
            throws InterruptedException, ExecutionException {
        ServletRequest request = context.getRequest();
        String id = getIdFromContext(context);
        if (request != null) {
            Enumeration<String> en = NbCollections.checkedEnumerationByFilter(request.getParameterNames(), String.class, true);
            while (en.hasMoreElements()) {
                String name = en.nextElement();
                String value = request.getParameter(name);
                if (value == null || value.length() == 0) {
                    continue;
                }
                context.setAttribute(name, value, PageContext.REQUEST_SCOPE);
            }
        }

        preparePageContext(context, id, statistics);
    }

    private String getIdFromContext(PageContext context){
        ServletRequest request = context.getRequest();
        HttpSession session = context.getSession();

        String id = null;
        if (request != null) {
            Object o = request.getAttribute("id");
            if (o instanceof String) {
                id = (String) o;
            }
        }
        if (id == null && request != null) {
            id = request.getParameter("id");
        }
        if (id == null && session != null) {
            id = (String) session.getAttribute("id");
        } else {
            if (id != null && session != null) {
                session.setAttribute("id", id);
            }
        }
        return id;
    }

    /**
     * Takes a page context and fills it with informations about all statistics
     * known to the system.
     *
     * @param context the context to fill
     * @param id session ID identifying the user (and its userdir)
     * @param statistics names of statistics to include in the page
     * @throws java.lang.InterruptedException
     * @throws java.util.concurrent.ExecutionException
     */
    public void preparePageContext(PageContext context, String id, Set<String> statNames)
            throws InterruptedException, ExecutionException {
        context.setAttribute("manager", this, PageContext.REQUEST_SCOPE);

        if (!isDirInitialized()){
            return;
        }

        if (id == null) {
            prepareGlobalData(context, statNames);
            return;
        }

        if (!initTask.isFinished()) {
            int timeout = Integer.getInteger("uihandlerserver.timeout", 600); // 10 mins

            boolean finished = initTask.waitFinished(1000 * timeout);
            if (!finished) {
                Object[] params = {timeout, id};
                LOG.log(Level.SEVERE, "Timeout ({0} s) waiting for page context id={1}", params); // NOI18N

                try {
                    //context.getSession().setAttribute("post.init.url", HttpUtils.getRequestURL(context.getRequest()));
                    context.getRequest().getRequestDispatcher("/index.jsp").forward(context.getRequest(), context.getResponse());
                    return;
                } catch (ServletException ex2) {
                    Exceptions.printStackTrace(ex2);
                } catch (IOException ex2) {
                    Exceptions.printStackTrace(ex2);
                }
            }
        }

        prepareGlobalData(context, statNames);

        EntityManager em = PersistenceUtils.getInstance().createEntityManager();
        try {
            preparePageContext(em, context, id, statNames);
        } catch (RuntimeException ex) {
            Exceptions.printStackTrace(ex);
            throw ex;
        } finally {
            em.close();
        }
    }

    private void prepareGlobalData(PageContext context, Set<String> statNames){
        int cnt = 0;
        HttpSession session = context.getSession();
        String version = null;
        if (session != null){
            version = (String) session.getAttribute("netbeans_version");
        }
        List<Value> data = null;
        if ((version == null) || (ALL_VERSIONS.equals(version))){
            data = this.statistics;
        } else {
            VersionData versionData = versionStatistics.get(version);
            data = versionData.getData();
            context.setAttribute("versionLogsCount",versionData.getLogsCount());
        }
        assert(data != null);
        for (Value<?> value : data) {
            if (statNames != null && !statNames.contains(value.statistics.name)) {
                continue;
            }
            registerValue(context, value, "global");
            cnt++;
        }

        if (statNames != null && cnt < statNames.size()) {
            HashSet<String> unknown = new HashSet<String>(statNames);
            for (Value<?> value : this.statistics) {
                unknown.remove(value.statistics.name);
            }
            throw new IllegalArgumentException("Unknown statistic: " + unknown); // NOI18N`

        }
    }

    private void preparePageContext(EntityManager em, PageContext context, String id, Set<String> statNames)
            throws InterruptedException, ExecutionException {

        List<Value> users = new ArrayList<Value>(statistics.size());
        for (Value<?> v : this.statistics) {
            addStatisticsData(users, v.statistics);
        }
        List<Value> lasts = new ArrayList<Value>(Collections.<Value>nCopies(statistics.size(), null));
        int lastLog = -1;
        Map<String, Object> params = Collections.singletonMap("userdir", (Object) id); // NOI18N

        List<Logfile> list = PersistenceUtils.executeNamedQuery(em, "Logfile.findByUserdir", params);
        context.setAttribute("userLogsCount", list.size());
        for (Logfile log : list) {

            boolean isLatest = lastLog < log.getUploadNumber();
            if (isLatest) {
                lastLog = log.getUploadNumber();
            }
            int index = 0;
            for (Value<?> value : users) {
                if (value == null) {
                    continue;
                }
                if (statNames != null && !statNames.contains(value.statistics.name)) {
                    continue;
                }
                Preferences prefs = DbPreferences.root(log, value.statistics, em);
                Value<?> readValue = readAndAddStatisticsData(value, prefs);
                if (isLatest) {
                    lasts.set(index, readValue);
                }
                index++;
            }
        }

        for (Value<?> value : lasts) {
            if (value == null) {
                continue;
            }
            if (statNames != null && !statNames.contains(value.statistics.name)) {
                continue;
            }
            if (value != null) {
                registerValue(context, value, "last");
            }
        }


        for (Value<?> value : users) {
            if (statNames != null && !statNames.contains(value.statistics.name)) {
                continue;
            }
            registerValue(context, value, "user");
        }
    }

    public ExceptionsData getUploadDoneExceptions(PageContext context){
        String id = getIdFromContext(context);
        if (id == null){
            return null;
        }
        ExceptionsData result = null;
        LogFileTask task = latestTasks.get(id);
        if (task != null){
            task.waitFinished();
            result = task.getExceptionsData();
        }
        if (result == null){
            LOG.fine("No data in latest tasks cache.");
        }
        return result;
    }

    private <Data> void registerValue(PageContext context, Value<Data> data, String prefix) {
        data.statistics.registerPageContext(context, prefix + data.statistics.name, data.value);
    }

    static <Data> void addStatisticsData(List<Value> toAdd, Statistics<Data> s) {
        toAdd.add(new Value<Data>(s));
    }

    static <Data> void addDataForLog(Value<Data> value, Logfile file, EntityManager em) {
        try {
            Preferences prefs = DbPreferences.root(file, value.statistics, em);
            Data logData = value.statistics.read(prefs);
            value.value = value.statistics.join(value.value, logData);
        } catch (BackingStoreException ex) {
            LOG.log(Level.WARNING, "Cannot read data", ex);
        }
    }

    static <Data> void clearData(Value<Data> v) {
        v.value = v.statistics.newData();
    }

    static <Data> Value<Data> readAndAddStatisticsData(Value<Data> v, Preferences prefs) {
        Data d;
        try {
            d = v.statistics.read(prefs);
        } catch (BackingStoreException ex) {
            LOG.log(Level.WARNING, ex.getMessage(), ex);
            d = v.statistics.newData();
        }
        v.value = v.statistics.join(v.value, d);
        Value<Data> ret = new Value<Data>(v.statistics);
        ret.value = d;
        return ret;
    }

    static final class Value<Data> {

        final Statistics<Data> statistics;
        Data value;

        public Value(Statistics<Data> statistics) {
            this.statistics = statistics;
            this.value = statistics.newData();
        }

        public void persist(Logfile logfile, EntityManager em) {
            Preferences prefs = DbPreferences.root(logfile, statistics, em);
            try {
                statistics.write(prefs, value);
                LogfileParsed lfp = new LogfileParsed(statistics.name, logfile, statistics.revision);
                em.persist(lfp);
            } catch (BackingStoreException ex) {
                LOG.log(Level.WARNING, ex.getMessage(), ex);
            }
        }
    }

    static final class VersionData{
        private int logsCount;
        private final List<Value> data;

        public VersionData(List<Value> data, int logsCount) {
            this.logsCount = logsCount;
            this.data = data;
        }

        public int getLogsCount() {
            return logsCount;
        }

        public List<Value> getData() {
            return data;
        }

        void addLog() {
            logsCount++;
        }
    }

    private final class Verify implements Runnable {

        private int cnt;
        private List<Value> sum;
        private boolean initted;
        private FilesProvider filesProvider;
        List<VersionRecount> preparedVersionRecounts = new ArrayList<VersionRecount>();
        private List<String> releaseVersions = null;
        private Long firstDevBuild = null;

        private void init() {
            initted = true;
            if (!isDirInitialized()){
                return;
            }

            cnt = 0;
            if (!(dir.exists() && dir.isDirectory())) {
                LOG.log(Level.WARNING, "Not a directory: {0}", dir);
            }else{
                filesProvider = new FilesProvider(dir);
            }
        }

        public void run() {
            if (closed) {
                LOG.info("Stopping all processing, already closed");
                return;
            }

            if (!initted){
                init();
                initTask = globalRP.post(new GlobalOperation(this, true));
                return;
            }

            if (!isDirInitialized() || filesProvider == null){
                return;
            }

            LOG.log(Level.FINE, "Checking status of {0}", dir);

            if (cnt == 0) {
                sum = new ArrayList<Value>();
                for (Value<?> v : statistics) {
                    addStatisticsData(sum, v.statistics);
                }
            }

            int batchSize = 5;
            if (Utils.getParsingTimeout() == 0) {
                batchSize = Integer.MAX_VALUE;
            }

            final EntityManager em = PersistenceUtils.getInstance().createEntityManager();
            for (int i = 0; i < batchSize; i++) {
                final File f = filesProvider.getNextFile();
                if (f == null) {
                    break;
                }
                
                cnt++;
                logCounts.addParsedLog();

                if (closed || DEFAULT != LogsManager.this) {
                    LOG.log(Level.INFO, "Stopping processing at {0} files", cnt);
                    break;
                }

                Logfile tempLog = LogFileTask.getLogInfo(f.getName(), em);
                if (tempLog == null){
                    tempLog = new Logfile(LogFileTask.getIdSes(f.getName()), LogFileTask.getSessionNumber(f.getName()));
                    em.persist(tempLog);
                } else if (isOldBuild(tempLog)){
                    LOG.log(Level.FINER, "Skipping old build.", f);
                    continue;
                }
                final Logfile log = tempLog;
                try {
                    assert log != null;
                    final List<Value> recompute = statisticsToRecompute(em, log);
                    if (recompute.isEmpty()) {
                        LOG.log(Level.FINER, "File {0} is up to date.", f);
                    } else {
                        if (LOG.isLoggable(Level.FINER)) {
                            StringBuilder sb = new StringBuilder();
                            sb.append("File ");
                            sb.append(f.getPath());
                            sb.append(" needs reparse for ");
                            String sep = "";
                            for (Value<?> value : recompute) {
                                sb.append(sep);
                                sb.append(value.statistics.name);
                                sep = ", ";
                            }
                            LOG.finer(sb.toString());
                        }
                        class RecomputeTask extends LogFileTask{

                            public RecomputeTask() {
                                super(f, null, null);
                            }

                            @Override
                            public void run() {
                                if (closed) {
                                    LOG.info("Processing cancelled");
                                    return;
                                }
                                runningTask = this;
                                handleAddNewLog(f, recompute, false, log, em);
                                notifyFinished();
                            }
                        }
                        em.getTransaction().begin();
                        new RecomputeTask().run();
                        em.getTransaction().commit();
                    }

                    LOG.log(Level.FINE, "Recomputing {0}", log.getFileName());
                    for (Value<?> v : sum) {
                        addDataForLog(v, log, em);
                    }
                } catch (ThreadDeath td) {
                    throw td;
                } catch (Throwable t) {
                    if (log == null){
                        LOG.log(Level.SEVERE, "Exception when processing: " , t);
                    }else{
                        LOG.log(Level.SEVERE, "Exception when processing: " + log.getFileName(), t);
                    }
                    em.getTransaction().rollback();
                    break;
                }
            }

            em.close();
            if (!filesProvider.isEmpty() || closed) {
                LOG.log(Level.FINE, "Breaking the parsing at {0}", getParsedLogs());
                postVerificationTask(this);
            }else{
                logCounts.markVerificationFinished();
                statistics = sum;
                initTask = globalRP.post(new GlobalOperation(this, false));
            }
        }

        private boolean isOldBuild(Logfile tempLog) {
            ProductVersion pv = tempLog.getProductVersionId();
            if (pv != null){
                Nbversion nbv = pv.getNbversionId();
                if (nbv != null){
                    if (Nbversion.DEV_VERSION.equals(nbv.getVersion())){
                        if (tempLog.getBuildnumber() < getFirstDevBuild()){
                            return true;
                        }
                    } else if(!getReleaseVersions().contains(nbv.getVersion())){
                        return true;
                    }
                }
            }
            return false;
        }

        private Long getFirstDevBuild(){
            if (firstDevBuild == null){
                firstDevBuild = Utils.getThreeMonthsEarlierDate(new Date());
            }
            return firstDevBuild;
        }

        private List<String> getReleaseVersions(){
            if (releaseVersions == null){
                String releaseVersionsList = NbBundle.getMessage(LogsManager.class, "RELEASE_VERSIONS");
                releaseVersions = Arrays.asList(releaseVersionsList.split(":"));
            }
            return releaseVersions;
        }

        private class GlobalOperation extends Persistable.Transaction implements Runnable {
            private final boolean read;
            private final Verify verify;
            public GlobalOperation(Verify verify, boolean read) {
                this.verify = verify;
                this.read = read;
            }

            public TransactionResult runQuery(EntityManager em) {
                if (read) {
                    LOG.log(Level.FINE, "reading previously stored globals");
                    Logfile global = getGlobalLog(em);
                    if (global == null) {
                        LOG.log(Level.WARNING, "no stored globals");
                    } else {
                        List<Value> globalsToRecompute = statisticsToRecompute(em, global);
                        if (!globalsToRecompute.isEmpty()){
                            LOG.log(Level.WARNING, "global data are not actual - there some new statistics or some version has rised");
                        }else{
                            List<Value> loadedStatistics = new ArrayList<Value>(statistics.size());
                            for (Value<?> v : statistics) {
                                addStatisticsData(loadedStatistics, v.statistics);
                            }
                            for (Value<?> v : loadedStatistics) {
                                addDataForLog(v, global, em);
                            }
                            //replace global data after loading whole global data
                            statistics = loadedStatistics;
                            LOG.log(Level.INFO, "done reading previously stored globals");
                            prepareVersionRecount(em);
                        }
                    }
                    postVerificationTask(verify);
                    return TransactionResult.COMMIT;
                } else {//write
                    LOG.log(Level.FINE, "storing globals");
                    Logfile global = getGlobalLog(em);
                    for (Value<?> v : sum) {
                        if (closed){
                            break;
                        }
                        v.persist(global, em);
                    }
                    LOG.log(Level.FINE, "done storing globals");
                    LOG.log(Level.INFO, "Sessions created for {0}", dir);

                    filesProvider = null;
                    initted = false;
                    if (preparedVersionRecounts.isEmpty()){
                        prepareVersionRecount(em);
                    }
                    for (VersionRecount versionRecount : preparedVersionRecounts) {
                        initTask = EXEC.post(versionRecount, 0, Thread.MIN_PRIORITY);
                    }
                    verificationFinished.release();
                    return TransactionResult.COMMIT;
                }
            }

            public void run() {
                org.netbeans.web.Utils.processPersistable(this);
            }

            private void prepareVersionRecount(EntityManager em) {
                for (String versionName : getReleaseVersions()) {
                    Nbversion version = (Nbversion) PersistenceUtils.getExist(em, "Nbversion.findByVersion", versionName);// NOI18N
                    if (version != null){
                        preparedVersionRecounts.add(new VersionRecount(dir, version));
                    }
                }
            }

            private Logfile getGlobalLog(EntityManager em) {
                Logfile globalLogFile = LogFileTask.getLogInfo("global", 0, em);
                if (globalLogFile == null) {
                    globalLogFile = new Logfile("global", 0);
                    em.persist(globalLogFile);
                }
                return globalLogFile;
            }
        }

        private List<Value> statisticsToRecompute(EntityManager em, Logfile log) {
            List<Value> recompute = new ArrayList<Value>();
            Map<String, Object> params = new HashMap<String, Object>(3);
            params.put("logfile", log); // NOI18N
            List<LogfileParsed> list = PersistenceUtils.executeNamedQuery(em, "LogfileParsed.findByLogfile", params);

            for (Value<?> v : statistics) {
                boolean ok = false;

                for (LogfileParsed rec : list) {
                    if ((rec.getStatistic().getStatname().equals(v.statistics.name)) && (rec.getRevision() == v.statistics.revision)) {
                        ok = true;
                        break;
                    }
                }
                if (!ok) {
                    LOG.log(Level.FINER, "  need reparse: {0} log: {1} revision: {2} no stored info", new Object[]{v.statistics.name, log.getFileName(), v.statistics.revision});
                    recompute.add(v);
                }
            }
            return recompute;
        }
    }
    
    private static class LogCounts implements Runnable{
        private int all;
        private int parsed;
        private File dir;

        private LogCounts(File dir) {
            this.dir = dir;
            parsed = 0;
            all = 0;
            if (dir != null){
                RequestProcessor.getDefault().post(this);
            }
        }
        
        synchronized void addNewLog(){
            all++;
        }
        synchronized void addParsedLog(){
            parsed++;
            //some logs might be waiting on parsing during verification finish
            if (all < parsed){
                all = parsed;
            }
        }
        
        public int getParsedLogs(){
            return parsed;
        }
        
        public int getLogsCount(){
            return all;
        }

        private void markVerificationFinished() {
            // correct number of all logs in directory
            all = parsed;
        }

        public void run() {
            long time = System.currentTimeMillis();
            FilesProvider provider = new FilesProvider(dir);
            while (provider.getNextFile() != null){
                all ++;
            }
            LOG.log(Level.FINE, "Counting logs took {0} ms.", (System.currentTimeMillis() - time));
            dir = null;
        }
    }

    //----- ONLY FOR TESTS  ---//
    public void waitParsingFinished() throws InterruptedException{
        initTask.waitFinished();
    }

    private Semaphore verificationFinished = new Semaphore(0);
    public void waitVerificationFinished() throws InterruptedException{
        verificationFinished.acquire();
        verificationFinished.release();
    }

}
