/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */
package org.netbeans.server.uihandler;

import org.netbeans.modules.exceptions.utils.BugReporterFactory;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.servlet.ServletRequest;
import org.netbeans.modules.exceptions.entity.Report;
import org.netbeans.modules.exceptions.entity.ReportComment;
import org.netbeans.modules.exceptions.entity.Slowness.SlownessType;
import org.netbeans.server.uihandler.api.Issue;
import org.netbeans.web.Persistable;
import org.netbeans.web.Persistable.TransactionResult;

/**
 *
 * @author Jindrich Sedek
 */
public class ExceptionsData {

    private LogRecord userDataLog,  thrownLog, buildInfo;
    private Integer issuezillaId;
    private Integer submitId;//exceptins or slowness
    private Integer reportId = 0, reportBeforeReopenId = null;
    private DataType dataType = DataType.STATISTIC;
    private String userName;

    // slowness data
    private Long slownessTime = null;
    private String latestAction;
    private SlownessType slownessType;
    
    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof ExceptionsData)) {
            return false;
        }
        ExceptionsData other = (ExceptionsData) obj;
        boolean result = equals(getThrownLog(), other.getThrownLog());
        result = result && equals(getUserDataLog(), other.getUserDataLog());
        return result;
    }

    private boolean equals(LogRecord a, LogRecord b) {
        if (a != null) {
            return a.equals(b);
        } else {
            return b == null;
        }
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + (this.getUserDataLog() != null ? this.getUserDataLog().hashCode() : 0);
        hash = 67 * hash + (this.getThrownLog() != null ? this.getThrownLog().hashCode() : 0);
        return hash;
    }

    public void prepareUploadDoneContext(final ServletRequest request) {
        if (DataType.STATISTIC.equals(getDataType())) {
            request.setAttribute("statSubmit", true);
            return;
        }else{
            request.setAttribute("statSubmit", false);
            request.setAttribute("issueId", submitId);
            request.setAttribute("isDuplicate", isDuplicateReport());
            if (reportBeforeReopenId != null){
                request.setAttribute("reopen", true);
                request.setAttribute("reportBeforeReopenId", reportBeforeReopenId);
                request.setAttribute("reportId", getReportId());
            } else if (isDuplicateReport()) {
                request.setAttribute("reportId", getReportId());
                if (issuezillaId != null && issuezillaId != 0) {
                    request.setAttribute("inIssuezilla", true);
                    request.setAttribute("issuezillaId", issuezillaId);
                    Issue issue = BugReporterFactory.getDefaultReporter().getIssue(issuezillaId);
                    if (issue != null) {
                        request.setAttribute("isClosed", !issue.isOpen());
                        boolean isFixed = issue.isFixed();
                        request.setAttribute("isFixed", isFixed);
                        if (isFixed && !issue.isTBD()) {
                            request.setAttribute("TM", issue.getTargetMilestone());
                        }
                    }
                } else {
                    request.setAttribute("inIssuezilla", false);
                }
                org.netbeans.web.Utils.processPersistable(new Persistable.Query() {

                    public TransactionResult runQuery(EntityManager em) throws Exception {
                        Report r = em.getReference(Report.class, getReportId());
                        Collection<ReportComment> comments = r.getCommentCollection();
                        if (comments.isEmpty()){
                            return TransactionResult.NONE;
                        }
                        String[] commentArr = new String[comments.size()];
                        int i = 0;
                        for (ReportComment reportComment : comments) {
                            commentArr[i] = reportComment.getComment();
                            i++;
                        }
                        request.setAttribute("additionalComments", commentArr);
                        return TransactionResult.NONE;
                    }
                });

            }
            request.setAttribute("username",getUserName());
        }
    }

    public boolean isExceptionReport() {
        return DataType.EXC_REPORT.equals(dataType);
    }

    public boolean isSlownessReport() {
        return DataType.SLOWNESS_REPORT.equals(dataType);
    }

    boolean isDuplicateReport(){
        return reportId != 0;
    }

    /**
     * @return the userDataLog
     */
    LogRecord getUserDataLog() {
        return userDataLog;
    }

    /**
     * @param userDataLog the userDataLog to set
     */
    void setUserDataLog(LogRecord userDataLog) {
        this.userDataLog = userDataLog;
    }

    /**
     * @return the thrownLog
     */
    LogRecord getThrownLog() {
        return thrownLog;
    }

    /**
     * @param thrownLog the thrownLog to set
     */
    void setThrownLog(LogRecord thrownLog) {
        this.thrownLog = thrownLog;
    }

    /**
     * @return the issuezillaId
     */
    Integer getIssuezillaId() {
        return issuezillaId;
    }

    /**
     * @param issuezillaId the issuezillaId to set
     */
    void setIssuezillaId(Integer issuezillaId) {
        this.issuezillaId = issuezillaId;
    }

    public Integer getSubmitId() {
        return submitId;
    }

    void setSubmitId(Integer exceptionId) {
        this.submitId = exceptionId;
    }

    public void setDataType(DataType dataType) {
        this.dataType = dataType;
    }

    DataType getDataType() {
        return dataType;
    }

    /**
     * @return the userName
     */
    public String getUserName() {
        return userName;
    }

    /**
     * @param userName the userName to set
     */
    void setUserName(String userName) {
        this.userName = userName;
    }

    public LogRecord getBuildInfo() {
        return buildInfo;
    }

    public void setBuildInfo(LogRecord buildInfo) {
        this.buildInfo = buildInfo;
    }

    /**
     * @return the reportId
     */
    public Integer getReportId() {
        return reportId;
    }

    /**
     * @param reportId the reportId to set
     */
    public void setReportId(Integer reportId) {
        this.reportId = reportId;
    }

    public Integer getReportBeforeReopenId() {
        return reportBeforeReopenId;
    }

    public void setReportBeforeReopenId(Integer reportBeforeReopenId) {
        this.reportBeforeReopenId = reportBeforeReopenId;
    }

    /**
     * @return the time
     */
    public Long getSlownessTime() {
        return slownessTime;
    }

    /**
     * @param time the time to set
     */
    public void setSlownessTime(Long time) {
        this.slownessTime = time;
    }

    /**
     * @return the latestAction
     */
    public String getLatestAction() {
        return latestAction;
    }

    /**
     * @param latestAction the latestAction to set
     */
    public void setLatestAction(String latestAction) {
        this.latestAction = latestAction;
    }

    public void setSlownessType(String slownessType) {
        if (slownessType != null){
            try{
                this.slownessType = SlownessType.valueOf(slownessType);
            }catch (IllegalArgumentException iae){
                Logger.getLogger(ExceptionsData.class.getName()).log(Level.WARNING, "Invalid Slowness Type: " + slownessType, iae);
            }
        }
    }

    public void setSlownessType(SlownessType slownessType) {
        this.slownessType = slownessType;
    }

    public SlownessType getSlownessType() {
        return slownessType;
    }

    public static enum DataType {
        STATISTIC, EXC_REPORT, SLOWNESS_REPORT
    }
}
