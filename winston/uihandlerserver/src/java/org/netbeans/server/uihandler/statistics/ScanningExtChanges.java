/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2010 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * Copyright 2010 Sun Microsystems, Inc. All rights reserved.
 */

package org.netbeans.server.uihandler.statistics;

import java.util.Map;
import java.util.TreeMap;
import java.util.logging.LogRecord;
import java.util.prefs.Preferences;
import javax.servlet.jsp.PageContext;
import org.netbeans.server.uihandler.*;
import java.util.logging.Logger;
import java.util.prefs.BackingStoreException;
import org.netbeans.modules.exceptions.entity.Logfile;
import org.openide.util.lookup.ServiceProvider;

/** Counts the number of switched on/off scanning for external changes.
 *
 * @author Tomas Musil
 */
@ServiceProvider(service=Statistics.class)
public final class ScanningExtChanges extends Statistics<ScanningExtChanges.Counts> {
    public static String USING = "Using";
    public static String TURNED_OFF = "Turned off";
    
    static final Logger LOG = Logger.getLogger(Help.class.getName());
    private static boolean wasFoundWindowActivatedInLog = false;
    private static boolean wasFoundWindowDectivatedInLog = false;

    public ScanningExtChanges() {
        super("ScanningExtChanges", 1);
    }

    protected Counts newData() {
        return new Counts(0,0);
    }

    protected Counts process(LogRecord rec) {
        if ("LOG_WINDOW_DEACTIVATED".equals(rec.getMessage())) { // NOI18
            wasFoundWindowDectivatedInLog = true;
        }
        if ("LOG_WINDOW_ACTIVATED".equals(rec.getMessage())) { // NOI18
            wasFoundWindowActivatedInLog = true;
        }
        return new Counts(0,0);
    }

    @Override
    protected Counts finishSessionUpload(String userId, int sessionNumber, boolean initialParse, Counts d) {
        Logfile l = LogFileTask.getRunningTask().getLogInfo();
        Long build = l == null ? null : l.getBuildnumber();
        if (build == null || build <= 100607L) {
            // ignore old builds, those created before June 07, 2010
            return new Counts(0,0);
        }
        
        Counts c;
        if (wasFoundWindowDectivatedInLog && !wasFoundWindowActivatedInLog){
            //user had turned off in options
            c = new Counts(d.getTotal()+1, d.getTurnedOff()+1);
        } else {
            // user is using scanning
            c = new Counts(d.getTotal()+1, d.getTurnedOff());
        }
        wasFoundWindowActivatedInLog = false;
        wasFoundWindowDectivatedInLog = false;
        return c;
    }

    @Override
    protected Counts join(Counts one, Counts two) {
        return new Counts(one.getTotal()+two.getTotal(), one.getTurnedOff()+two.getTurnedOff());
    }

    public static final class Counts {
        private final int total;
        private final int turnedOff;
        
        public Counts(int total, int turnedOff){
            this.total=total;
            this.turnedOff=turnedOff;
        }
        
        public int getTotal() {
            return total;
        }
        
        public int getTurnedOff() {
            return turnedOff;
        }
    }
    

    @Override
    protected void registerPageContext(PageContext page, String name,
                                       Counts data) {
        super.registerPageContext(page, name, data);

        if (name.equals("globalScanningExtChanges")) {
            Map<String,Integer> list = new TreeMap<String,Integer>();
            list.put(TURNED_OFF, data.getTurnedOff());
            list.put(USING, data.getTotal()-data.getTurnedOff());
            page.setAttribute("globalScanningExtChangesRatio", list);
        }
    }


    protected void write(Preferences pref, Counts d) {
        if (d != null) {
            pref.putInt("total", d.getTotal());
            pref.putInt("turnedOff", d.getTurnedOff());
        }
    }

    protected Counts read(Preferences pref) throws BackingStoreException {
        int total = pref.getInt("total", -1);
        int turnedOff = pref.getInt("turnedOff", -1);
        if (total == -1 || turnedOff == -1 ) {
            return new Counts(0,0);
        }
        return new Counts(total,turnedOff);
    }

}
