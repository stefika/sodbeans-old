/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */
package org.netbeans.server.uihandler;

import org.netbeans.modules.exceptions.utils.BugReporterFactory;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.Queue;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import org.netbeans.server.uihandler.api.bugs.BugReporterException;
import org.netbeans.modules.exceptions.entity.Directuser;
import org.netbeans.modules.exceptions.entity.Exceptions;
import org.netbeans.modules.exceptions.entity.Nbuser;
import org.netbeans.modules.exceptions.entity.Nbversion;
import org.netbeans.modules.exceptions.entity.Slowness;
import org.netbeans.modules.exceptions.entity.Submit;
import org.netbeans.modules.exceptions.utils.BugzillaReporter;
import org.netbeans.modules.exceptions.utils.PersistenceUtils;
import org.netbeans.server.uihandler.api.Authenticator.AuthToken;
import org.netbeans.server.uihandler.api.Issue;
import org.netbeans.web.Persistable;
import org.netbeans.web.Persistable.TransactionResult;
import org.openide.util.RequestProcessor;
import org.openide.util.Task;
import org.openide.util.TaskListener;

/**
 *
 * @author Jindrich Sedek
 */
public final class IZInsertion {

    private static final Logger LOG = Logger.getLogger(IZInsertion.class.getName());
    private static Date OLD_REPORTS_DATE = new Date("10/01/09");
    private static int REPORTER_TIMEOUT = 3 * 60 * 1000; // 3min
    private static final int MESSAGE_AMOUNT = 1;
    private static final int MESSAGE_2_AMOUNT = 2;
    private static final int P2_PRIORITY_AMOUNT = 4;
    private static final int P1_PRIORITY_AMOUNT = 10;
    private static final long MINIMAL_REPORT_TIME = 20000; // 20s
    private static final String P1_PRIORITY_STR = "P1";// NOI18N
    private static final String P2_PRIORITY_STR = "P2";// NOI18N
    private static final String OBSOLETE_VERSIONS = "6.7";
    private static final Queue<IssuezillaInsertionRequest> requestQueue = new LinkedList<IssuezillaInsertionRequest>();
    private static final AtomicBoolean isIZProcessing = new AtomicBoolean(false);
    private static final RequestProcessor izRequestProcessor = new RequestProcessor("IZ Insertion", 1);
    private static final Timer timer = new Timer("IZ Insertion");
    private static FilterMap filters;
    private static FilterMap reopenFilters;
    private static Long duplicatesNo;
    private static BugzillaReporter bugReporter;
    private final Submit sbm;
    private final AuthToken userToken;
    private final String extraCC, extraMessage;

    IZInsertion(Submit sbm, AuthToken userToken) {
        this(sbm, userToken, null, null);
    }

    public IZInsertion(Submit sbm, AuthToken userToken, String extraCC, String extraMessage) {
        this.sbm = sbm;
        bugReporter = BugReporterFactory.getDefaultReporter();
        this.extraCC = extraCC;
        this.extraMessage = extraMessage;
        if (isGuest(sbm.getNbuserId())) {
            this.userToken = null;
        } else {
            this.userToken = userToken;
        }
        if (!isIZProcessing.getAndSet(true)){
            timer.schedule(new IZInsertionTask(), 0, REPORTER_TIMEOUT);
        }
    }

    public void start() {
        InsertionResult result = InsertionResult.NONE;
        EntityManager em = LogFileTask.getRunningTask().getTaskEntityManager();
        for (IssueZillaInsertionFilter filter : getFilters(sbm.getClass())) {
            result = filter.isInsertable(em, sbm);
            LOG.log(Level.FINE, "Filter + {0} returned {1}", new Object[]{filter.getClass().getName(), result.name()});
            if (result.equals(InsertionResult.CANCEL)){
                return;
            }
            if (!result.equals(InsertionResult.NONE)){
                break;
            }
        }
        if (!result.equals(InsertionResult.NONE)){
            final IssuezillaInsertionRequest request =  doStart(em, result);
            if (request != null){
                LogFileTask.getRunningTask().addTaskListener(new TaskListener() {

                    public void taskFinished(Task arg0) {
                        requestQueue.add(request);
                    }
                });
            }
        }
    }

    void startReopenInsert() {
        EntityManager em = LogFileTask.getRunningTask().getTaskEntityManager();
        for (IssueZillaInsertionFilter filter : getReopenFilters(sbm.getClass())) {
            if ((filter.isInsertable(em, sbm)).equals(InsertionResult.CANCEL)){
                LOG.log(Level.WARNING, "Canceling reopen based on result of {0}", filter.getClass().getName());
                return;
            }
        }
        IssuezillaInsertionRequest request = doStart(em, InsertionResult.INSERT);
        if (request != null){
            requestQueue.add(request);
        }
    }

    public void startForceInsert(EntityManager em) {
        IssuezillaInsertionRequest request = doStart(em, InsertionResult.INSERT);
        if (request != null){
            requestQueue.add(request);
        }
    }
    
    private synchronized  IssuezillaInsertionRequest doStart(EntityManager em, InsertionResult result){
        if (sbm.getReportId().inIssuezillaTransfer() && InsertionResult.INSERT.equals(result)){
            return null;
        }
        sbm.getReportId().setIssuezillaTransfer(true);
        Submit mergedSbm = em.merge(sbm);
        return new IssuezillaInsertionRequest(userToken, mergedSbm, result, extraMessage, extraCC);
    }

    private static Long getDuplicatesNo() {
        if (duplicatesNo == null) {
            duplicatesNo = Utils.getVariable("duplicatesNo", Long.class);// NOI18N
        }
        return duplicatesNo;
    }

    private static boolean isGuest(Nbuser user){
        return Utils.isGuest(user.getName());
    }

    private static <T extends Submit> IssueZillaInsertionFilter<T>[] getReopenFilters(Class<T> clazz) {
        if (reopenFilters == null) {
            synchronized (IZInsertion.class) {
                reopenFilters = new FilterMap();
                IssueZillaInsertionFilter<Exceptions>[] excFilters = new IssueZillaInsertionFilter[]{
                new OldVersionsFilter(),
                new EmptyStacktraceFilter(),
                new IncorrectComponentFilter<Exceptions>(),
                };
                reopenFilters.put(Exceptions.class, excFilters);
                IssueZillaInsertionFilter<Slowness>[] slownFilters = new IssueZillaInsertionFilter[]{
                new OldVersionsFilter(),
                new OldReportsFilter<Slowness>(),
                new IncorrectComponentFilter<Slowness>(),
                };
                reopenFilters.put(Slowness.class, slownFilters);
            }
        }
        return reopenFilters.get(clazz);
    }
    
    private static <T extends Submit> IssueZillaInsertionFilter<T>[] getFilters(Class<T> clazz) {
        if (filters == null) {
            synchronized (IZInsertion.class) {
                filters = new FilterMap();
                IssueZillaInsertionFilter<Exceptions>[] excFilters = new IssueZillaInsertionFilter[]{
                new OldVersionsFilter(),
                new EmptyStacktraceFilter(),
                new IncorrectComponentFilter<Exceptions>(),
                new IssueClosedFilter<Exceptions>(),// first priority
                new ManyDuplicatesFromOneUserFilter<Exceptions>(),
                new DirectUserFilter<Exceptions>(),
                new NoClassDefFoundFilter(),
                new ExceptionClassNameFilter(),
                new DuplicatesNoFilter<Exceptions>(),
                new ManyDuplicatesFilter<Exceptions>(),
                new AddToCCFilter<Exceptions>(),
                };
                filters.put(Exceptions.class, excFilters);
                IssueZillaInsertionFilter<Slowness>[] slownFilters = new IssueZillaInsertionFilter[]{
                new OldVersionsFilter(),
                new OldReportsFilter<Slowness>(),
                new IncorrectComponentFilter<Slowness>(),
                new IssueClosedFilter<Slowness>(),// first priority
                new ManyDuplicatesFromOneUserFilter<Slowness>(),
                new LongTimeSlownessFilter(),
                new DuplicatesNoFilter<Slowness>(),
                new AddToCCFilter<Slowness>(),
                };
                filters.put(Slowness.class, slownFilters);
            }
        }
        return filters.get(clazz);
    }

    protected static void setDuplicatesNo(Long duplicates) {
        duplicatesNo = duplicates;
    }

    interface IssueZillaInsertionFilter<T extends Submit> {

        public InsertionResult isInsertable(EntityManager em,  T exc);
    }

    enum InsertionResult {

        INSERT, ATTACHMENT, INCREASE_PRIORITY, CANCEL, ADDTOCC, NONE
    }

    private static class DuplicatesNoFilter<T extends Submit> implements IssueZillaInsertionFilter<T> {

        public InsertionResult isInsertable(EntityManager em, Submit exc) {
            int duplicates = exc.getReportId().getDuplicates(em);
            if ((duplicates > getDuplicatesNo() && !exc.getReportId().isInIssuezilla())){
                return InsertionResult.INSERT;
            }
            if ((duplicates > getDuplicatesNo()) && (duplicates < getDuplicatesNo() + 3)) {
                return InsertionResult.ATTACHMENT;
            }
            return InsertionResult.NONE;
        }
    }

    private static class ExceptionClassNameFilter implements IssueZillaInsertionFilter<Exceptions> {
        private static final Class[] directlyInsertedClasses = new Class[]{
            NullPointerException.class,
            OutOfMemoryError.class,
            AssertionError.class,
            MissingResourceException.class,
            IllegalArgumentException.class
        };

        public InsertionResult isInsertable(EntityManager em, Exceptions exc) {
            if (exc.getReportId().isInIssuezilla() || isGuest(exc.getNbuserId())) {
                return InsertionResult.NONE;
            }
            String className = exc.getStacktrace().getClass1();
            for (Class directClass : directlyInsertedClasses) {
                if (className.contains(directClass.getSimpleName())){
                    return InsertionResult.INSERT;// NOI18N
                }
            }
            return InsertionResult.NONE;
        }
    }

    private static class NoClassDefFoundFilter implements IssueZillaInsertionFilter<Exceptions> {
        private static final Class[] CLASS_LOADING_CLASSES = new Class[]{
            NoClassDefFoundError.class,
            ClassNotFoundException.class
        };

        public InsertionResult isInsertable(EntityManager em, Exceptions exc) {
            String className = exc.getStacktrace().getClass1();
            for (Class classLoadingErrorClass : CLASS_LOADING_CLASSES) {
                if (className.contains(classLoadingErrorClass.getSimpleName())){
                    int duplicates = exc.getReportId().getDuplicates(em);
                    if (duplicates < getDuplicatesNo().intValue()){
                        return InsertionResult.CANCEL;
                    }
                }
            }
            return InsertionResult.NONE;
        }
    }

    private static class DirectUserFilter<T extends Submit> implements IssueZillaInsertionFilter<T> {

        public InsertionResult isInsertable(EntityManager em, T exc) {
            Object object = exc.getNbuserId().getName();
            Map<String, Object> params = Collections.singletonMap("name", object);
            List<Directuser> directUsers = PersistenceUtils.executeNamedQuery(em, "Directuser.findByName", params, Directuser.class);
            if (directUsers.size() > 0) {
                if (exc.getReportId().isInIssuezilla()){
                    return InsertionResult.ATTACHMENT;
                }else{
                    return InsertionResult.INSERT;
                }
            } else {
                return InsertionResult.NONE;
            }
        }
    }

    private static class IssueClosedFilter<T extends Submit> implements IssueZillaInsertionFilter<T> {

        public InsertionResult isInsertable(EntityManager em, T exc) {
            if (!exc.getReportId().isInIssuezilla()){
                LOG.fine("Not in issuezilla");
                return InsertionResult.NONE;
            }
            Integer issueId = exc.getReportId().getIssueId();
            Issue issue = bugReporter.getIssue(issueId);
            if ((issue == null) || (!issue.isOpen())) {
                return InsertionResult.CANCEL;
            }
            return InsertionResult.NONE;
        }
    }

    private static class ManyDuplicatesFilter<T extends Submit> implements IssueZillaInsertionFilter<T> {

        public InsertionResult isInsertable(EntityManager em, T submit) {
            if (!submit.getReportId().isInIssuezilla()){
                return InsertionResult.NONE;
            }
            Issue issue = bugReporter.getIssue(submit.getReportId().getIssueId());
            if (issue == null || (!issue.isOpen())) {
                return InsertionResult.NONE;
            }
            int duplicatesCount = submit.getReportId().getDuplicates(em);
            int amount = getDuplicatesAmount(duplicatesCount);
            if (duplicatesCount % getDuplicatesNo() == 0){
                if ((amount == MESSAGE_AMOUNT) || (amount == MESSAGE_2_AMOUNT) || (amount == P2_PRIORITY_AMOUNT)) {
                    return InsertionResult.INCREASE_PRIORITY;
                }
                if ((amount > 0) && (amount % P1_PRIORITY_AMOUNT) == 0){
                    return InsertionResult.INCREASE_PRIORITY;
                }
            }

            return InsertionResult.NONE;
        }

    }

    private static class ManyDuplicatesFromOneUserFilter<T extends Submit> implements IssueZillaInsertionFilter<T> {

        public InsertionResult isInsertable(EntityManager em, T exc) {
            Nbuser user = exc.getNbuserId();
            int duplicates = exc.getReportId().getDuplicatesFromOneUser(em, user);
            if (duplicates > 3) {
                return InsertionResult.CANCEL;
            }
            return InsertionResult.NONE;
        }
    }

    static class AddToCCFilter<T extends Submit> implements IssueZillaInsertionFilter<T>{

        public InsertionResult isInsertable(EntityManager em, T exc) {
            if (exc.getReportId().isInIssuezilla() && !isGuest(exc.getNbuserId())){
                Issue issue = bugReporter.getIssue(exc.getReportId().getIssueId());
                if ((issue != null) && (!exc.getNbuserId().getName().equals(issue.getReporter())) && (issue.isOpen())){
                    return InsertionResult.ADDTOCC;
                }
            }
            return InsertionResult.NONE;
        }
    }

    private static int getDuplicatesAmount(int duplicatesCount) {
        return duplicatesCount / getDuplicatesNo().intValue();
    }

    private static class IssuezillaInsertionRequest extends Persistable.Query implements Runnable {
        private static final int MAX_IZ_INVOCATIONS = 4;
        private int invocationCount;
        private final AuthToken userToken;
        private final Submit submit;
        private final InsertionResult insertionResult;
        private final String extraCC, extraMessage;

        public IssuezillaInsertionRequest(AuthToken userToken, Submit sbm, InsertionResult insertionResult, String extraMessage, String extraCC) {
            this.userToken = userToken;
            this.submit = sbm;
            this.insertionResult = insertionResult;
            this.invocationCount = 0;
            this.extraCC = extraCC;
            this.extraMessage = extraMessage;
        }

        private String getPasswd(){
            if (userToken != null){
                return userToken.getPasswd();
            }else{
                return null;
            }
        }

        public void run() {
            invocationCount++;
            try {
                switch (insertionResult) {
                    case INSERT:
                        LOG.log(Level.INFO, "Trying to report exception id: {0}", submit.getId());
                        bugReporter.reportSubmit(submit, getPasswd(), extraMessage, extraCC);
                        break;
                    case ATTACHMENT:
                        LOG.log(Level.INFO, "Trying to add attachement to exception id: {0}", submit.getId());
                        bugReporter.createAttachment(submit, getPasswd());
                        checkPriority();
                        break;
                    case ADDTOCC:
                        addReporterToCC();
                        checkPriority();
                        break;
                    case INCREASE_PRIORITY:
                        checkPriority();
                        break;
                    default:
                        assert false; // should never happen
                }
            }catch (BugReporterException e){
                if (invocationCount < MAX_IZ_INVOCATIONS){
                    Object[] params = new Object[]{submit.getId().toString(), Integer.toString(invocationCount), e.getMessage()};
                    LOG.log(Level.WARNING, "Filling issue {0} failed for {1} times, beacause of {2}", params);
                    izRequestProcessor.post(new Runnable() {

                        public void run() {
                            requestQueue.add(IssuezillaInsertionRequest.this);
                        }
                    }, REPORTER_TIMEOUT * 2);
                }else{
                    Object[] params = new Object[]{submit.getId().toString(), Integer.toString(invocationCount)};
                    LOG.log(Level.SEVERE, "Filling issue {0} failed for {1} times", params);
                    throw e;
                }
            }
        }

        private void checkPriority(){
            org.netbeans.web.Utils.processPersistable(this);
        }

        private void addReporterToCC(){
            if (!userToken.getUserName().equals(Utils.getReporterUsername())){
                LOG.log(Level.INFO, "Trying to add to cc for issue id:{0}", submit.getId());
                bugReporter.addToCCList(submit.getReportId().getIssueId(), userToken.getUserName(), userToken.getPasswd());
            }
        }

        // increase priority if needed
        public TransactionResult runQuery(EntityManager em) {
            Submit merged = Submit.getById(em, submit.getId());
            if (!merged.getReportId().isInIssuezilla()){
                // nothing to do
                return TransactionResult.NONE;
            }
            int issuezillaId = merged.getReportId().getIssueId();
            if (!PersistenceUtils.issueIsOpen(issuezillaId)){
                return TransactionResult.NONE;
            }
            int duplicatesCount = merged.getReportId().getDuplicates(em);
            int duplicatesAmount = getDuplicatesAmount(duplicatesCount);
            if (duplicatesAmount == 0){
                return TransactionResult.NONE;
            }
            if (merged instanceof Exceptions &&
                    ((Exceptions)merged).getStacktrace().getClass1().contains(AssertionError.class.getName())){
                return TransactionResult.NONE;
            }
            if (merged instanceof Exceptions && duplicatesCount % getDuplicatesNo() == 0){
                if ((duplicatesAmount % P1_PRIORITY_AMOUNT) == 0) {
                    setIssuePriority(em, merged, issuezillaId, P1_PRIORITY_STR);
                } else if (duplicatesAmount == P2_PRIORITY_AMOUNT) {
                    Issue issue = bugReporter.getIssue(issuezillaId);
                    if (!P1_PRIORITY_STR.equals(issue.getPriority())) {
                        setIssuePriority(em, merged, issuezillaId, P2_PRIORITY_STR);
                    }
                } else if (duplicatesAmount == MESSAGE_AMOUNT || duplicatesAmount == MESSAGE_2_AMOUNT) {
                    String commentMessage = getManyDuplicatesComment(em, merged);
                    bugReporter.postTextComment(issuezillaId, commentMessage);
                }
            }
            return TransactionResult.NONE;
        }

        private void setIssuePriority(EntityManager em, Submit exc, int issuezillaId, String priorityStr){
            Issue issue = bugReporter.getIssue(issuezillaId);
            String commentMessage = getManyDuplicatesComment(em, exc);
            if ("RESOLVED".equals(issue.getIssueStatus()) && "LATER".equals(issue.getResolution())){
                bugReporter.reopenIssue(issuezillaId, commentMessage);
            }else{
                bugReporter.postTextComment(issuezillaId, commentMessage);
            }
            bugReporter.setIssuePriority(issuezillaId, priorityStr);
        }
    }

    private static class IncorrectComponentFilter<T extends Submit> implements IssueZillaInsertionFilter<T> {
        public InsertionResult isInsertable(EntityManager em, Submit exc) {
            if (Arrays.asList(Submit.generatedComponents).contains(exc.getReportId().getComponent())){
                return InsertionResult.CANCEL;
            }
            return InsertionResult.NONE;
        }
    }

    private static class EmptyStacktraceFilter implements IssueZillaInsertionFilter<Exceptions> {
        public InsertionResult isInsertable(EntityManager em, Exceptions exc) {
            if (exc.getRelevantCause().getLineCollection().isEmpty()){
                return InsertionResult.CANCEL;
            }
            return InsertionResult.NONE;
        }
    }

    private static class OldVersionsFilter implements IssueZillaInsertionFilter<Submit> {

        public InsertionResult isInsertable(EntityManager em, Submit sbm) {
            String versionNumber = sbm.getLogfileId().getProductVersionId().getNbversionId().getVersionNumber();
            if (versionNumber != null
                    && (!versionNumber.equals(Nbversion.DEV_VERSION))
                    && versionNumber.compareTo(OBSOLETE_VERSIONS) <= 0) {
                return InsertionResult.CANCEL;
            }
            return InsertionResult.NONE;
        }
    }

    private static class LongTimeSlownessFilter implements IssueZillaInsertionFilter<Slowness> {

        public InsertionResult isInsertable(EntityManager em, Slowness slow) {
            if ((slow.getActionTime() > MINIMAL_REPORT_TIME)
                    && (slow.getReportId().getDuplicates(em) > 1)
                    && (!slow.getReportId().isInIssuezilla())) {
                return InsertionResult.INSERT;
            }
            return InsertionResult.NONE;
        }
    }

    private static class OldReportsFilter<T extends Submit> implements IssueZillaInsertionFilter<T> {

        public InsertionResult isInsertable(EntityManager em, T exc) {
            if (exc.getReportdate() == null){
                return InsertionResult.NONE;
            }
            if (!exc.isBuildNewerThan(OLD_REPORTS_DATE)){
                return InsertionResult.CANCEL;
            }
            return InsertionResult.NONE;
        }
    }

    /**
     * !!!ONLY FOR TESTS!!!
     */
    static void setReporterTimeout(int timeout) {
        REPORTER_TIMEOUT = timeout;
    }
    static void setOldReportsDate(Date date){
        OLD_REPORTS_DATE = date;
    }
    static void waitIZInsertionFinished() throws InterruptedException{
        final AtomicBoolean isEmpty = new AtomicBoolean(false);
        while (!isEmpty.get()){
            izRequestProcessor.post(new Runnable() {

                public void run() {
                    isEmpty.set(requestQueue.isEmpty());
                }
            }, REPORTER_TIMEOUT).waitFinished();
        }
    }

    private static class IZInsertionTask extends TimerTask {

        public void run() {
            IssuezillaInsertionRequest firstRequest = requestQueue.poll();
            if (firstRequest != null) {
                izRequestProcessor.post(firstRequest);
            }
        }
    }

    private static String getManyDuplicatesComment(EntityManager em, Submit exc) {
        int duplicatesCount = exc.getReportId().getDuplicates(em);
        String message = "This bug already has " + duplicatesCount + " duplicates \n"; // NOI18N
        message = message.concat("see " + org.netbeans.web.Utils.EXCEPTION_DETAIL + exc.getReportId().getId());
        return message;
    }

    private static class FilterMap {
        private Map<Class, IssueZillaInsertionFilter[]> mapa = new HashMap<Class, IssueZillaInsertionFilter[]>();

        <Q extends Submit> void put(Class<Q> cls, IssueZillaInsertionFilter<Q>[] filters){
            mapa.put(cls, filters);
        }

        <Q extends Submit> IssueZillaInsertionFilter<Q>[] get(Class<Q> cls){
            return (IssueZillaInsertionFilter<Q>[]) mapa.get(cls);
        }

    }
}
