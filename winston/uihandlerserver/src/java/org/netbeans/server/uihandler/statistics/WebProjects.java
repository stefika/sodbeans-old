/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * Copyright 2007 Sun Microsystems, Inc. All rights reserved.
 */

package org.netbeans.server.uihandler.statistics;

import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;
import javax.servlet.jsp.PageContext;
import org.netbeans.server.uihandler.*;
import java.util.Collections;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import org.openide.util.lookup.ServiceProvider;

/** Counts Web Frameworks usage
 *
 * @author Jan Horvath
 */
@ServiceProvider(service=Statistics.class)
public final class WebProjects extends Statistics<Map<WebProjects.WebProjectItem, Integer>> {
    static final Logger LOG = Logger.getLogger(ProjectTypes.class.getName());
    private static final Integer ONE = new Integer(1);
    private static final String UNKNOWN = "unknown";
    private static final Integer ZERO = new Integer(0);
    
    public WebProjects() {
        super("WebProjects", 6);
    }

    protected Map<WebProjects.WebProjectItem, Integer> newData() {
        return Collections.emptyMap();
    }
    
    /*
     * http://wiki.netbeans.org/UILoggingInJ2EE 
     */
    protected Map<WebProjects.WebProjectItem, Integer> process(LogRecord rec) {
        if ("UI_WEB_PROJECT_CREATE".equals(rec.getMessage())) {
            Map<WebProjects.WebProjectItem, Integer> result = new TreeMap<WebProjects.WebProjectItem, Integer> ();
            result.put(WebProjectItem.newServer(getStringParam(rec, 0,UNKNOWN)), ONE);
            result.put(WebProjectItem.newJ2ee(getStringParam(rec, 2,UNKNOWN)), ONE);
            if (rec.getParameters().length > 5) {
                for (int i = 5; i < rec.getParameters().length; i++) {
                    result.put(WebProjectItem.newFramework(getStringParam(rec, i,UNKNOWN)), ONE);
                }
            }
            return result;
        }
        if ("UI_WEB_PROJECT_SERVER_CHANGED".equals(rec.getMessage())) {
            Map<WebProjects.WebProjectItem, Integer> result = new TreeMap<WebProjects.WebProjectItem, Integer> ();
            result.put(WebProjectItem.newServer(getStringParam(rec, 2,UNKNOWN)), ONE);
            return result;
        }
        if ("UI_WEB_PROJECT_FRAMEWORK_ADDED".equals(rec.getMessage())) {
            Map<WebProjects.WebProjectItem, Integer> result = new TreeMap<WebProjects.WebProjectItem, Integer> ();
            if (rec.getParameters().length > 0) {
                for (int i = 0; i < rec.getParameters().length; i++) {
                    result.put(WebProjectItem.newFramework(getStringParam(rec, i,UNKNOWN)), ONE);
                }
            }
            return result;
        }
        else {
            return newData();
        }
    }
    
    private static String getStringParam(LogRecord rec, int index, String def) {
        if (rec == null) {
            return def;
        }
        Object[] params = rec.getParameters();
        if (params == null || params.length <= index) {
            return def;
        }
        if (params[index] instanceof String) {
            return (String)params[index];
        }
        return def;
    }
    
    protected Map<WebProjects.WebProjectItem, Integer> join(
            Map<WebProjects.WebProjectItem, Integer> one, Map<WebProjects.WebProjectItem, Integer> two) {
        Map<WebProjects.WebProjectItem, Integer> merge = new TreeMap<WebProjects.WebProjectItem, Integer>(one);
        for (Map.Entry<WebProjects.WebProjectItem, Integer> entry: two.entrySet()) {
            if (merge.containsKey(entry.getKey())) {
                Integer count = merge.get(entry.getKey());
                count = count + entry.getValue();
//                merge.remove(entry.getKey());
                merge.put(entry.getKey(), count);
            }
            else {
                merge.put(entry.getKey(), entry.getValue());
            }
        }
        return merge;
    }

    @Override
    protected Map<WebProjectItem, Integer> finishSessionUpload(
        String userId,
        int sessionNumber,
        boolean initialParse,
        Map<WebProjectItem, Integer> data
    ) {
        return data;
    }

    protected void write(Preferences pref, Map<WebProjects.WebProjectItem, Integer> d) {
        for (Map.Entry<WebProjects.WebProjectItem, Integer> entry : d.entrySet()) {
            pref.putInt(entry.getKey().toString(), entry.getValue());
        }
    }

    @Override
    protected void registerPageContext(PageContext page, String name, Map<WebProjectItem, Integer> data) {
        super.registerPageContext(page, name, data);
    }

    protected Map<WebProjects.WebProjectItem, Integer> read(Preferences pref) throws BackingStoreException {
        Map<WebProjects.WebProjectItem, Integer> result = new TreeMap<WebProjects.WebProjectItem, Integer> ();
        for (String n : pref.keys()) {
            WebProjectItem item = new WebProjectItem(n);
            Integer value = pref.getInt(n, ZERO);
            result.put(item, value);
        }
        return result;
    }
    
    public static class WebProjectItem implements Comparable<WebProjectItem> {
        public enum Type {
            SERVER, FRAMEWORK, J2EE, UNKNOWN
        };
        private final Type type;
        private String name;
        
        private WebProjectItem(Type type, String name) {
            this.type = type;
            this.name = name;
            stripName();
        }
        
        protected WebProjectItem(String s) {
            int i = s.indexOf(":");
            if (i >= 0) {
                this.type = Type.valueOf(s.substring(0, i));
                this.name = s.substring(i + 1, s.length());
            } else {
                this.type = Type.UNKNOWN;
                this.name = s;
            }
            stripName();
        }

        private void stripName(){
            String text = this.toString();
            while (text.length() >= Preferences.MAX_KEY_LENGTH){
                int index = name.indexOf('.');
                name = name.substring(index + 1);
                text = this.toString();
            }
        }

        public static WebProjectItem newServer(String name) {
            return new WebProjectItem(Type.SERVER, name);
        }
        
        public static WebProjectItem newFramework(String name) {
            return new WebProjectItem(Type.FRAMEWORK, name);
        }

        public static WebProjectItem newJ2ee(String name) {
            return new WebProjectItem(Type.J2EE, name);
        }
        
        public String getName() {
            return name;
        }

        public Type getType() {
            return type;
        }
        
        public String toString() {
            return type.toString() + ":" + name;
        }

        public int compareTo(WebProjectItem o) {
            if (o == null) {
                return -1;
            }
            if (this.type.compareTo(o.type) != 0) {
                return this.type.compareTo(o.type);
            }
            return name.compareTo(o.name);
        }

        @Override
        public boolean equals(Object obj) {
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final WebProjectItem other = (WebProjectItem) obj;
            if (this.type != other.type) {
                return false;
            }
            if (this.name != other.name && (this.name == null || !this.name.equals(other.name))) {
                return false;
            }
            return true;
        }

        @Override
        public int hashCode() {
            int hash = 5;
            hash = 17 * hash + (this.type != null ? this.type.hashCode() : 0);
            hash = 17 * hash + (this.name != null ? this.name.hashCode() : 0);
            return hash;
        }
    }

    public static class ChartBean implements Comparable {

        String name;
        Integer value;
        String serie;

        public ChartBean(String name, Integer value, String serie) {
            this.name = name;
            this.value = value;
            this.serie = serie;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getSerie() {
            return serie;
        }

        public void setSerie(String serie) {
            this.serie = serie;
        }

        public Integer getValue() {
            return value;
        }

        public void setValue(Integer value) {
            this.value = value;
        }

        public int compareTo(Object o) {
            if (o instanceof ChartBean) {
                return ((ChartBean) o).getValue() - value;
            }
            return 0;
        }
    }
}
