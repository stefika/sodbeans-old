/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * Copyright 2007 Sun Microsystems, Inc. All rights reserved.
 */

package org.netbeans.server.uihandler.statistics;

import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;
import javax.servlet.jsp.PageContext;
import java.util.Collections;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import org.netbeans.server.uihandler.Statistics;
import org.openide.util.lookup.ServiceProvider;

/** Counts the number of used maven projecs and their packaging.
 *
 * @author Milos Kleint
 */
@ServiceProvider(service=Statistics.class)
public final class MavenTypes extends Statistics<Map<MavenTypes.Item, Integer>> {
    static final Logger LOG = Logger.getLogger(MavenTypes.class.getName());
    private static final Integer ONE = new Integer(1);
    private static final Integer ZERO = new Integer(0);
    
    public MavenTypes() {
        super("MavenTypes", 7);
    }

    protected Map<MavenTypes.Item, Integer> newData() {
        return Collections.emptyMap();
    }
    
    /*
     * http://wiki.netbeans.org/UILoggingInJ2EE 
     */
    protected Map<MavenTypes.Item, Integer> process(LogRecord rec) {
        if ("UI_MAVEN_PROJECT_OPENED".equals(rec.getMessage())) {
            Map<MavenTypes.Item, Integer> result = new TreeMap<MavenTypes.Item, Integer> ();
            result.put(Item.newPackaging(getStringParam(rec, 0, "unknown")), ONE);
            return result;
        }
        else {
            return newData();
        }
    }
    
    private static String getStringParam(LogRecord rec, int index, String def) {
        if (rec == null) {
            return def;
        }
        Object[] params = rec.getParameters();
        if (params == null || params.length <= index) {
            return def;
        }
        if (params[index] instanceof String) {
            return (String)params[index];
        }
        return def;
    }
    
    protected Map<MavenTypes.Item, Integer> join(
            Map<MavenTypes.Item, Integer> one, Map<MavenTypes.Item, Integer> two) {
        Map<MavenTypes.Item, Integer> merge = new TreeMap<MavenTypes.Item, Integer>(one);
        for (Map.Entry<MavenTypes.Item, Integer> entry: two.entrySet()) {
            if (merge.containsKey(entry.getKey())) {
                Integer count = merge.get(entry.getKey());
                count = count + entry.getValue();
//                merge.remove(entry.getKey());
                merge.put(entry.getKey(), count);
            }
            else {
                merge.put(entry.getKey(), entry.getValue());
            }
        }
        return merge;
    }

    @Override
    protected Map<MavenTypes.Item, Integer> finishSessionUpload(
        String userId,
        int sessionNumber,
        boolean initialParse,
        Map<MavenTypes.Item, Integer> data
    ) {
        return data;
    }

    protected void write(Preferences pref, Map<MavenTypes.Item, Integer> d) {
        for (Map.Entry<MavenTypes.Item, Integer> entry : d.entrySet()) {
            pref.putInt(entry.getKey().toString(), entry.getValue());
        }
    }

    @Override
    protected void registerPageContext(PageContext page, String name, Map<MavenTypes.Item, Integer> data) {
        super.registerPageContext(page, name, data);
    }

    protected Map<MavenTypes.Item, Integer> read(Preferences pref) throws BackingStoreException {
        Map<MavenTypes.Item, Integer> result = new TreeMap<MavenTypes.Item, Integer> ();
        for (String n : pref.keys()) {
            MavenTypes.Item item = new Item(n);
            Integer value = pref.getInt(n, ZERO);
            result.put(item, value);
        }
        return result;
    }
    
    public static class Item implements Comparable<Item> {
        private String name;
        
        private Item() {
        }
        
        protected Item(String pack) {
            this.name = pack;
        }
        
        public static Item newPackaging(String name) {
            return new Item().setName(name);
        }
        
        private Item setName(String name) {
            this.name = name;
            return this; 
        }
        
        public String getName() {
            return name;
        }

        
        public String toString() {
            return name;
        }

        public int compareTo(Item o) {
            if (o == null) {
                return -1;
            }
            return name.compareTo(o.name);
        }

        @Override
        public boolean equals(Object obj) {
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final Item other = (Item) obj;
            if (this.name != other.name && (this.name == null || !this.name.equals(other.name))) {
                return false;
            }
            return true;
        }

        @Override
        public int hashCode() {
            int hash = 5;
            hash = 17 * hash + (this.name != null ? this.name.hashCode() : 0);
            return hash;
        }
    }

    public static class ChartBean implements Comparable {

        String name;
        Integer value;
        String serie;

        public ChartBean(String name, Integer value, String serie) {
            this.name = name;
            this.value = value;
            this.serie = serie;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getSerie() {
            return serie;
        }

        public void setSerie(String serie) {
            this.serie = serie;
        }

        public Integer getValue() {
            return value;
        }

        public void setValue(Integer value) {
            this.value = value;
        }

        public int compareTo(Object o) {
            if (o instanceof ChartBean) {
                return ((ChartBean) o).getValue() - value;
            }
            return 0;
        }
    }
}
