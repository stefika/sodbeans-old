package org.netbeans.server.uihandler.statistics;

import java.util.*;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

public class CodeCompletionData {
	private int totalRecords;
	private int implicitInvocations;
	private int explicitInvocations;
	private int cancelledInvocations;
	private int defaultSelect;
	private int mouseCompletion;
	private int keyboardCompletion;
	private int totalSelections;
	private int selectionIndexesSum;
        private int totalCompletions;
	
        private HashMap<String, Integer> selectedIndexes;
	
	
	public CodeCompletionData() {
		totalRecords = 0;
		implicitInvocations = 0;
		explicitInvocations = 0;
		cancelledInvocations = 0;
		defaultSelect = 0;
		selectionIndexesSum = 0;
		totalSelections = 0;
                mouseCompletion = 0;
                keyboardCompletion = 0;
                totalCompletions = 0;
                selectedIndexes = new HashMap<String, Integer>();
	}

    public void setTotalRecords(int totalRecords) {
        this.totalRecords = totalRecords;
    }

    public void setSelectedIndexes(HashMap<String, Integer> h) {
        this.selectedIndexes = h;
    }
    
    public HashMap<String, Integer> getSelectedIndexes() {
        return this.selectedIndexes;
    }
    
    public HashMap<String, Integer> getTopSelectedIndexes(int howMany) {
        HashMap<String, Integer> tmp = new HashMap<String, Integer>();
        
        List<String> mapKeys = new ArrayList<String>(selectedIndexes.keySet());
        List<Integer> mapValues = new ArrayList<Integer>(selectedIndexes.values());
        
        TreeSet<Integer> sortedSet = new TreeSet<Integer>(mapValues);
        
        Integer[] sortedArray = sortedSet.toArray(new Integer[sortedSet.size()]);
        
        int size = sortedArray.length;
        
        for (int i=size; i>0;) {
            
            tmp.put(mapKeys.get(mapValues.indexOf(sortedArray[--i])), sortedArray[i]);
            
            if ((size-i-1)>howMany){
                break;
            }
            
        }
        
        return tmp;
    }
    
    public void setImplicitInvocations(int implicitInvocations) {
        this.implicitInvocations = implicitInvocations;
    }

    public void setExplicitInvocations(int explicitInvocations) {
        this.explicitInvocations = explicitInvocations;
    }

    public void setCancelledInvocations(int cancelledInvocations) {
        this.cancelledInvocations = cancelledInvocations;
    }

    public void setDefaultSelect(int defaultSelect) {
        this.defaultSelect = defaultSelect;
    }

    public void setMouseCompletion(int mouseCompletion) {
        this.mouseCompletion = mouseCompletion;
        this.totalCompletions = this.mouseCompletion + this.keyboardCompletion;
    }

    public void setKeyboardCompletion(int keyboardCompletion) {
        this.keyboardCompletion = keyboardCompletion;
        this.totalCompletions = this.mouseCompletion + this.keyboardCompletion;
    }

    public void setTotalSelections(int totalSelections) {
        this.totalSelections = totalSelections;
    }

    public void setSelectionIndexesSum(int selectionIndexesSum) {
        this.selectionIndexesSum = selectionIndexesSum;
    }
	
	public float getAverageSelectionIndex() {
		return (getKeyboardCompletion()+getMouseCompletion()==0)?0:(float)selectionIndexesSum/(getKeyboardCompletion()+getMouseCompletion());
	}
	
        public float getAverageNonDefaultSelectionIndex() {
		return (getKeyboardCompletion()+getMouseCompletion()==0)?0:((float)selectionIndexesSum)/(getKeyboardCompletion()+getMouseCompletion()-getDefaultSelect());
	}
        
	public float getDefaultSelectP() {
		return (getKeyboardCompletion()+getMouseCompletion()==0)?0:(float)100*((float)getDefaultSelect()/(getKeyboardCompletion()+getMouseCompletion()));
	}
	
	public void incTotalSelections() {
		totalSelections++;
	}
	
	public int getTotalSelections() {
		return totalSelections;
	}
	
	public void addIndex(int index) {
		selectionIndexesSum+=index;
		incTotalSelections();
	}
	
	public int getSelectionIndexesSum() {
		return selectionIndexesSum;
	}
	
	public float getKeyboardCompletionP() {
		return (getKeyboardCompletion()+getMouseCompletion()==0)?0:((float)100*((float)getKeyboardCompletion()/(getKeyboardCompletion()+getMouseCompletion())));
	}
	
	public float getMouseCompletionP() {
		return (getKeyboardCompletion()+getMouseCompletion()==0)?0:((float)100*((float)getMouseCompletion()/(getKeyboardCompletion()+getMouseCompletion())));
	}
	
	public void incKeyboardCompletion() {
		keyboardCompletion++;
	}
	
	public int getKeyboardCompletion() {
		return keyboardCompletion;
	}
	
	public void incMouseCompletion() {
		mouseCompletion++;
	}
	
	public int getMouseCompletion() {
		return mouseCompletion;
	}
	
	public void incDefaultSelect() {
		defaultSelect++;
	}
	
	public int getDefaultSelect() {
		return defaultSelect;
	}
	
	public void incCancelledInvocations() {
		cancelledInvocations++;
	}
	
	public int getCancelledInvocations() {
		return cancelledInvocations;
	}
	
	public int getTotalRecords() {
		return this.totalRecords;
	}
	
	public int getExplicitInvocations() {
		return explicitInvocations;
	}
	
	public void incExplicitInvocations() {
		explicitInvocations++;
	}
	
	public int getImplicitInvocations() {
		return implicitInvocations;
	}
	
	public void incImplicitInvocations() {
		implicitInvocations++;
	}
	
	public void incTotalRecords() {
		totalRecords++;
	}
	
	public float getCancelledInvocationsP() {
		return (getTotalRecords()==0)?0:((float)100.0*getCancelledInvocations()/getTotalRecords());
	}
	
	public float getCompletedInvocationsP() {
		return (getTotalRecords()==0)?0:((float)100.0*this.totalCompletions/getTotalRecords());
	}
        
        public float getImplicitInvocationsP() {
		return (getTotalRecords()==0)?0:((float)100.0*getImplicitInvocations()/getTotalRecords());
}
        
         public float getExplicitInvocationsP() {
		return (getTotalRecords()==0)?0:((float)100.0*getExplicitInvocations()/getTotalRecords());
	}
         
        final void write(Preferences p) {
            p.putInt("totalRecords", totalRecords);
            p.putInt("implicitInvocations", implicitInvocations);
            p.putInt("explicitInvocations", explicitInvocations);
            p.putInt("cancelledInvocations", cancelledInvocations);
            p.putInt("defaultSelect", defaultSelect);
            p.putInt("mouseCompletion", mouseCompletion);
            p.putInt("keyboardCompletion", keyboardCompletion);
            p.putInt("totalSelections", totalSelections);
            p.putInt("selectionIndexesSum", selectionIndexesSum);
            p.putInt("totalCompletions", totalCompletions);
            if (selectedIndexes != null) {
                Preferences si = p.node("selectedIndexes");
                for (Map.Entry<String, Integer> e : selectedIndexes.entrySet()) {
                    si.putInt(e.getKey(), e.getValue());
                }
            }
        }
        final void read(Preferences p) throws BackingStoreException {
            totalRecords = p.getInt("totalRecords", 0);
            implicitInvocations = p.getInt("implicitInvocations", 0);
            explicitInvocations = p.getInt("explicitInvocations", 0);
            cancelledInvocations = p.getInt("cancelledInvocations", 0);
            defaultSelect = p.getInt("defaultSelect", 0);
            mouseCompletion = p.getInt("mouseCompletion", 0);
            keyboardCompletion = p.getInt("keyboardCompletion", 0);
            totalSelections = p.getInt("totalSelections", 0);
            selectionIndexesSum = p.getInt("selectionIndexesSum", 0);
            totalCompletions = p.getInt("totalCompletions", 0);
            Preferences si = p.node("selectedIndexes");
            if (si != null) {
                selectedIndexes = new HashMap<String, Integer>();
                for (String k : si.keys()) {
                    selectedIndexes.put(k, si.getInt(k, 0));
                }
            }
        }
}       
        
