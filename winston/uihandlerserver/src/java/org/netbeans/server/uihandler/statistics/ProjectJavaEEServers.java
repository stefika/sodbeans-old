/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * Copyright 2007 Sun Microsystems, Inc. All rights reserved.
 */

package org.netbeans.server.uihandler.statistics;

import java.io.Serializable;
import java.util.Collections;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.LogRecord;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;
import org.netbeans.server.uihandler.Statistics;
import org.netbeans.server.uihandler.statistics.ProjectJavaEEServers.ProjectServer;
import org.openide.util.lookup.ServiceProvider;

/**
 * Statistics for Java EE Servers used in projects.
 * {@link http://wiki.netbeans.org/UILoggingInJ2EE}
 *
 * @author Petr Hejl
 */
@ServiceProvider(service=Statistics.class)
public class ProjectJavaEEServers extends Statistics<Map<ProjectServer,Integer>> {

    public ProjectJavaEEServers() {
        super("ProjectJavaEEServers"); // NOI18N
    }

    public static Map<ProjectServer, Integer> filter(ProjectType type, Map<ProjectServer, Integer> data) {
        Map<ProjectServer, Integer> result = new TreeMap<ProjectServer, Integer>();
        for (Map.Entry<ProjectServer, Integer> entry : data.entrySet()) {
            if (entry.getKey().type.equals(type)) {
                result.put(entry.getKey(), entry.getValue());
            }
        }
        return result;
    }

    @Override
    protected Map<ProjectServer, Integer> newData() {
        return Collections.emptyMap();
    }

    @Override
    protected Map<ProjectServer, Integer> process(LogRecord rec) {
        ProjectServer server = ProjectServer.valueOf(rec);
        if (server == null) {
            return Collections.emptyMap();
        } else {
            return Collections.singletonMap(server, 1);
        }
    }

    @Override
    protected Map<ProjectServer, Integer> finishSessionUpload(String userId, int sessionNumber,
            boolean initialParse, Map<ProjectServer, Integer> d) {
        return d;
    }

    @Override
    protected Map<ProjectServer, Integer> join(Map<ProjectServer, Integer> one, Map<ProjectServer, Integer> two) {
        Map<ProjectServer, Integer> result = new TreeMap<ProjectServer, Integer>(one);
        for (Map.Entry<ProjectServer, Integer> entryTwo : two.entrySet()) {
            Integer countOne = result.get(entryTwo.getKey());
            if (countOne == null) {
                result.put(entryTwo.getKey(), entryTwo.getValue());
            } else {
                result.put(entryTwo.getKey(), Integer.valueOf(countOne.intValue() + entryTwo.getValue().intValue()));
            }
        }
        return result;
    }

    @Override
    protected Map<ProjectServer, Integer> read(Preferences pref) throws BackingStoreException {
        Map<ProjectServer, Integer> result = new TreeMap<ProjectServer, Integer>();
        for (String key : pref.keys()) {
            int cnt = pref.getInt(key, 0);
            if (cnt > 0) {
                result.put(ProjectServer.valueOf(key), Integer.valueOf(cnt));
            }
        }
        return result;
    }

    @Override
    protected void write(Preferences pref, Map<ProjectServer, Integer> d) throws BackingStoreException {
        for (Map.Entry<ProjectServer, Integer> entry : d.entrySet()) {
            pref.putInt(ProjectServer.toString(entry.getKey()), entry.getValue());
        }
    }

    public static final class ProjectServer implements Comparable<ProjectServer>, Serializable {

        private final ProjectType type;

        private final String name;

        private ProjectServer(ProjectType type, String name) {
            this.name = name;
            this.type = type;
        }

        public static String toString(ProjectServer server) {
            return server.type + ":" + server.name; // NOI18N
        }

        public static ProjectServer valueOf(String str) {
            String[] parts = str.split(":"); // NOI18N
            if (parts.length != 2) {
                return null;
            }
            return new ProjectServer(ProjectType.valueOf(parts[0]), parts[1]);
        }

        public static ProjectServer valueOf(LogRecord record) {
            ProjectType type = ProjectType.valueOf(record);
            if (type != null) {
                return new ProjectServer(type, getStringParam(record, 0, "Unknown"));
            }
            return null;
        }

        public String getDisplayName() {
            return name;
        }

        @Override
        public String toString() {
            return getDisplayName();
        }

        @Override
        public boolean equals(Object obj) {
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final ProjectServer other = (ProjectServer) obj;
            if (this.type != other.type) {
                return false;
            }
            if (this.name != other.name && (this.name == null || !this.name.equals(other.name))) {
                return false;
            }
            return true;
        }

        @Override
        public int hashCode() {
            int hash = 7;
            hash = 83 * hash + (this.type != null ? this.type.hashCode() : 0);
            hash = 83 * hash + (this.name != null ? this.name.hashCode() : 0);
            return hash;
        }

        public int compareTo(ProjectServer o) {
            if (o == null) {
                return -1;
            }

            int cmp = type.compareTo(o.type);
            if (cmp != 0) {
                return cmp;
            }
            return name.compareTo(o.name);
        }

        private static String getStringParam(LogRecord rec, int index, String def) {
            if (rec == null) {
                return def;
            }

            Object[] params = rec.getParameters();
            if (params == null || params.length <= index) {
                return def;
            }
            if (params[index] instanceof String) {
                String value = (String) params[index];
                return "null".equals(value) ? def : value; // NOI18N
            }
            return def;
        }
    }

    public static enum ProjectType {

        WEB,

        EJB,

        EAR,

        APP;

        public static ProjectType valueOf(LogRecord record) {
            String message = record.getMessage();
            if ("UI_WEB_PROJECT_OPENED".equals(message)) { // NOI18N
                return WEB;
            } else if ("UI_EJB_PROJECT_OPENED".equals(message)) { // NOI18N
                return EJB;
            } else if ("UI_EAR_PROJECT_OPENED".equals(message)) { // NOI18N
                return EAR;
            } else if ("UI_APP_CLIENT_PROJECT_OPENED".equals(message)) { // NOI18N
                return APP;
            } else {
                return null;
            }
        }
    }
}
