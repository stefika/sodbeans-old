/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */

package org.netbeans.server.uihandler;

import java.io.File;
import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Jaroslav Tulach
 * @version
 */
public class UploadLogs extends HttpServlet {
    private static Logger LOG = Logger.getLogger(UploadLogs.class.getName());

    @Override
    public void init() throws ServletException {
        LOG.info("Initializing UploadLogs servlet");
        try {
            LogsManager.getDefault();
        }
        catch (Exception ex) {
            LOG.log(Level.SEVERE, ex.getMessage(), ex);
        }
        LOG.info("Done Initializing UploadLogs servlet");
    }

    
    /** Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        LOG.finer("processing request");
        
        if (Utils.getRootUploadDirPath() == null) {
            String msg = "UploadServlet.doPost: dir is null."; 
            LOG.warning(msg);
            request.setAttribute("error", msg);
            getServletContext().getRequestDispatcher("/error.jsp").forward(request, response);
            return;
        }
        File appFile =  null;
        try{
            appFile = RequestFacadeImpl.upload(request);
        }catch(SocketTimeoutException ste){
            LOG.severe(ste.getMessage());
            response.sendError(HttpServletResponse.SC_REQUEST_TIMEOUT);
            return;
        }
        if (appFile == null){
            String msg = "UploadServlet.doPost: no data submitted";
            LOG.warning(msg);
            request.setAttribute("error", msg);
            getServletContext().getRequestDispatcher("/error.jsp").forward(request, response);
            return;
        }
        LogsManager.getDefault().addLog(appFile, getIP(request));

        String id = appFile.getName().split("\\.")[0];
        request.setAttribute("redirect",  Boolean.TRUE);
        request.setAttribute("id",  id);
        LOG.finer("sending response id:" + id);
        request.getRequestDispatcher("/uploadredirect.jsp").forward(request, response);
    }
    
    protected String getIP(HttpServletRequest request){
        String headerIP = request.getHeader("x-forwarded-for");
        if (headerIP == null){
            return request.getRemoteHost();// no Lighttpd
        }
        if (headerIP != null) {
            String[] array = headerIP.split(",");
            if (array.length > 1) {
                headerIP = array[array.length - 1].trim();
            }
        }
        return headerIP;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }
    
    /** Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }
    
    /** Returns a short description of the servlet.
     */
    public String getServletInfo() {
        return "Uploads Logs Generated By UI Logger - http://logger.netbeans.org/welcome/";
    }
    // </editor-fold>
}
