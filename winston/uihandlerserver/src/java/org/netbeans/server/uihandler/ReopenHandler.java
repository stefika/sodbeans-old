/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2009 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */
package org.netbeans.server.uihandler;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import javax.persistence.EntityManager;
import org.netbeans.server.uihandler.api.bugs.BugReporter;
import org.netbeans.modules.exceptions.entity.Exceptions;
import org.netbeans.modules.exceptions.entity.Report;
import org.netbeans.modules.exceptions.entity.Slowness;
import org.netbeans.modules.exceptions.entity.Stacktrace;
import org.netbeans.modules.exceptions.entity.Submit;
import org.netbeans.server.componentsmatch.Component;
import org.netbeans.server.componentsmatch.Matcher;
import org.netbeans.server.snapshots.SlownessChecker;

/**
 *
 * @author Jindrich Sedek
 */
abstract class ReopenHandler {
    protected Submit result;

    static ReopenHandler createHandler(Submit sbm){
        if (sbm instanceof Exceptions) {
            return new ExceptionReopenHandler((Exceptions)sbm);
        } else if (sbm instanceof Slowness) {
            return new SlownessReopenHandler((Slowness)sbm);
        } else {
            throw new UnsupportedOperationException("only Exceptions and Slowness classes are supported");
        }
    }

    public abstract Submit process(EntityManager em);

    private static class ExceptionReopenHandler extends ReopenHandler {

        private final Exceptions excParam;
        private Exceptions actual;

        public ExceptionReopenHandler(Exceptions excParam) {
            this.excParam = excParam;
        }

        public Submit process(EntityManager em) {
            actual = em.merge(excParam);
            BugReporter.LOG.log(Level.INFO, "reopening issue based on exception {0} from {1}",
                    new Object[]{actual.getId().toString(), actual.getReportId().getId().toString()});
            Report openDuplicateCandidate = Utils.checkOpenIssues(em, actual.getMockThrowable());
            if (openDuplicateCandidate != null) {
                actual.setReportId(openDuplicateCandidate);
                return em.merge(actual);
                
            }
            Report newReport = new Report();
            newReport.setId(Utils.getNextId(Report.class));
            Component comp = Matcher.getDefault().match(em, actual.getMockThrowable());
            newReport.setComponent(comp);
            em.persist(newReport);

            moveSimilarExceptions(em, actual.getReportId(), actual, newReport);

            actual.setReportId(newReport);
            return em.merge(actual);
        }

        /**
         * All candidates should be check how similar are they with exc
         * and the most similar exceptions should be marked as duplicates of report.
         *
         * @param report groups a list of candidates for splitting
         * @param master exception to decide
         */
        private void moveSimilarExceptions(EntityManager em, Report candidatesReport, Exceptions master, Report newReport) {
            // protect the first exception report
            candidatesReport.preloadSubmitCollection(em);
            List<Exceptions> candidates = new ArrayList(candidatesReport.getSubmitCollection());
            assert(!candidates.isEmpty());
            Exceptions minimal = candidates.iterator().next();
            for (Exceptions exceptions : candidates) {
                if (exceptions.getId() < minimal.getId()){
                    minimal = exceptions;
                }
            }
            candidates.remove(master);
            candidates.remove(minimal);

            //move exactly the same stacktraces
            Integer mashterHash = master.getHashcodes().getCode();
            Iterator<Exceptions> it = candidates.iterator();
            while (it.hasNext()) {
                Exceptions next = it.next();
                if (next.getHashcodes().getCode().equals(mashterHash)) {
                    it.remove();

                    next.setReportId(newReport);
                    em.merge(next);
                }
            }

            //move other very similar stacktraces
            CheckingFilter.LinesCheckingFilter filter = new CheckingFilter.LinesCheckingFilter(8);
            for (Exceptions exceptions : candidates) {
                List<Stacktrace> stacks = filter.getSimilarStacktraces(em, exceptions.getMockThrowable(), false);
                if (stacks.contains(master.getStacktrace()) && ! stacks.contains(excParam.getStacktrace())){
                    exceptions.setReportId(newReport);
                    em.merge(exceptions);
                }
            }
        }
    }

    private static class SlownessReopenHandler extends ReopenHandler {

        private final Slowness slownParam;
        private Slowness actual;

        private SlownessReopenHandler(Slowness slown) {
            this.slownParam = slown;
        }

        public Submit process(EntityManager em) {
            actual = em.merge(slownParam);
            BugReporter.LOG.log(Level.INFO, "reopening issue based on slowness{0} from {1}",
                    new Object[]{actual.getId().toString(), actual.getReportId().getId().toString()});
            Report newReport = new Report();
            newReport.setId(Utils.getNextId(Report.class));
            Component comp = recountComponent(em);
            newReport.setComponent(comp);
            em.persist(newReport);

            actual.setReportId(newReport);
            return em.merge(actual);
        }

        private Component recountComponent(EntityManager em) {
            SlownessChecker checker = new SlownessChecker(em, actual.getLatestAction(), actual.getLogfileId());
            return checker.getComponentForSlowness();
        }
    }
}


