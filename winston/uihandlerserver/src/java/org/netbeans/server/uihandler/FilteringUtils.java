/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */
package org.netbeans.server.uihandler;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.persistence.EntityManager;
import org.netbeans.modules.exceptions.entity.Exceptions;
import org.netbeans.modules.exceptions.entity.Line;
import org.netbeans.modules.exceptions.entity.Method;
import org.netbeans.modules.exceptions.entity.Stacktrace;
import org.netbeans.modules.exceptions.utils.LineComparator;
import org.netbeans.modules.exceptions.utils.PersistenceUtils;
import org.openide.util.Parameters;

/**
 *
 * @author Jindrich Sedek
 */
public final class FilteringUtils {
    private static final Logger LOG = Logger.getLogger(FilteringUtils.class.getName());

    public static boolean verifyTheSameStracktraces(Throwable thrown, List<Line> linesOfST, int depth){
        StackTraceElement[] stackTrace = thrown.getStackTrace();
        return compareStacktaces(linesOfST, stackTrace, depth);
    }
    
    public static boolean verifyTheSameStacktraces(EntityManager em, Throwable thrown, Stacktrace stack, int depth) {
        StackTraceElement[] stackTrace = thrown.getStackTrace();
        Map<String, Object> params = Collections.singletonMap("stacktraceId", (Object) stack.getId());
        //DEBUG INFO
        long start = System.currentTimeMillis();
        List<Line> lines = PersistenceUtils.executeNamedQuery(em, "Line.findByStacktraceId", params);
        long time = System.currentTimeMillis() - start;
        if(time > 5000 ) {
            LOG.log(Level.SEVERE, "Query for lines in verifySameStacktaces took:{0}ms. stacktraceId:"+stack.getId(), time);
        }
        return compareStacktaces(lines, stackTrace, depth);
    }
        
    private static boolean compareStacktaces(List<Line> lines, StackTraceElement[] stackTrace, int depth){
        Collections.sort(lines, new LineComparator());
        if (depth == -1) {// compare whole stacktraces
            if (stackTrace.length != lines.size()) {// different lines count
                return false;
            }
            depth = lines.size();
        } else {
            int min = Math.min(lines.size(), stackTrace.length);
            if (depth > min) {
                depth = min;
            }
        }
        for (int i = 0; i < depth; i++) {
            if (!areEquals(lines.get(i), stackTrace[i])){
                return false;
            }
        }
        return true;
    }

    static boolean areEquals(Line line, StackTraceElement element){
        String steMethod = Method.getMethodNameFromSTEWithoutNumbers(element);
        String lineMethod = line.getMethod().getNameWithoutNumbers();
        return lineMethod.equals(steMethod);
    }

    public static Exceptions getRootExceptions(Stacktrace stacktrace) {
        Parameters.notNull("stacktrace must be not null", stacktrace);
        /* go to root stacktrace */
        while (stacktrace.getStacktrace() != null) {
            stacktrace = stacktrace.getStacktrace();
        }
        return stacktrace.getExceptions();
    }

    public static void removeWrongClasses(Throwable thrown, Collection<Stacktrace> results) {
        String message = thrown.getMessage();
        if ((message == null) || (results == null)) {
            return;
        }
        Set<Stacktrace> removed = new HashSet<Stacktrace>();
        for (Stacktrace stacktrace : results) {
            //remove all stacktraces with different message
            if (!sameMessages(thrown, stacktrace)) {
                removed.add(stacktrace);
            }
        }
        results.removeAll(removed);
    }

    public static boolean sameMessages(Throwable thrown, Stacktrace stacktrace) {
        String message = thrown.getMessage();
        String stackTraceMessage = getMessageOfTheOriginalExceptionStacktrace(stacktrace);
        if (message == null){
            return (stackTraceMessage == null);
        }
        String classFromMessage = getClassOfMessage(message);
        if (classFromMessage.contains("ClassNotFoundException")) {
            message = removeLoader(message);
            if (stackTraceMessage != null) {
                stackTraceMessage = removeLoader(stackTraceMessage);
                if (!message.equals(stackTraceMessage)) {
                    return false;
                }
            }
        } else if (classFromMessage.contains("MissingResourceException")){
                String nonObjectedMessage = filterObjects(message);
                String nonObjectedStackTraceMessage = filterObjects(stackTraceMessage);
                if (!nonObjectedMessage.equals(nonObjectedStackTraceMessage)) {
                    return false;
                }
        } else {
            if (stackTraceMessage != null) {
                String stackTraceClass = getClassOfMessage(stackTraceMessage);
                if (!classFromMessage.equals(stackTraceClass)) {
                    return false;
                }
            }
        }
        return true;
    }

    private static String getMessageOfTheOriginalExceptionStacktrace(Stacktrace stacktrace){
        Exceptions exc = stacktrace.getExceptions();
        if ((exc == null) || (stacktrace.getAnnotation() != null)){// has inner exception
            return stacktrace.getMessage();
        }
        return exc.getStacktrace().getMessage();
    }

    private static final Pattern OBJECT_ID_PATTERN = Pattern.compile("@[0-9a-f]{3,10}");
    private static String filterObjects(String message){
        Matcher m = OBJECT_ID_PATTERN.matcher(message);
        message = m.replaceAll("");
        return message;
    }

    public static String getClassOfMessage(String message) {
        int index = message.indexOf(':');
        /*there is no message - just exception class*/
        if (index == -1) {
            index = message.length();
        }
        return message.substring(0, index);
    }

    private static String removeLoader(String message) {
        int begin = message.indexOf('[');
        if (begin != -1) {
            return message.substring(0, begin);
        }
        return message;

    }
}
