/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */

package org.netbeans.server.uihandler;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import org.netbeans.modules.exceptions.entity.Exceptions;
import org.netbeans.modules.exceptions.entity.Hashcodes;
import org.netbeans.modules.exceptions.entity.Line;
import org.netbeans.modules.exceptions.entity.Method;
import org.netbeans.modules.exceptions.entity.Report;
import org.netbeans.modules.exceptions.entity.Stacktrace;
import org.netbeans.modules.exceptions.utils.LineComparator;
import org.netbeans.modules.exceptions.utils.PersistenceUtils;

/**
 *
 * @author Jindrich Sedek
 */
interface CheckingFilter {
    static final String OOME = "java.lang.OutOfMemoryError";
    
    /**this function checks a stacktrace to duplicates
     *
     * @param thrown thrown is a stacktrace to check
     * @return Exception class that this is a duplicate of or null if unable to decide
     */
    public org.netbeans.modules.exceptions.entity.Exceptions check(EntityManager em, Throwable thrown, boolean searchOnlyOpens);
    
    // gather report ids that we don't want submits to duplicate to
    public class IgnoredReports{
        public static Set<Integer> ignored = new HashSet<Integer>();
        
        static {
             ignored.add(67149); // #67149 - OOME: Java heap space
        }
        
        public static boolean isIgnored(Report report){
            return ignored.contains(report.getId());
        }
    }
    
    
    public class OutOfMemoryFilter implements CheckingFilter{

        public Exceptions check(EntityManager em, Throwable thrown, boolean searchOnlyOpens) {
            if ((thrown.getMessage() != null) && thrown.getMessage().startsWith(OOME)){
                List<Exceptions> candidates = PersistenceUtils.executeNamedQuery(em, "Exceptions.findReportByLikeStackMessage", Collections.singletonMap("message", OOME + "%"), Exceptions.class);
                for (Exceptions exc : candidates) {
                    if (PersistenceUtils.exceptionIsOpen(exc)){
                        return exc;
                    }
                }
            }
            return null;
        }
    }

    public class WholeStackTraceFilter implements CheckingFilter{
        
        public org.netbeans.modules.exceptions.entity.Exceptions check(EntityManager em, Throwable thrown, boolean searchOnlyOpens) {
            int code = Utils.countHash(thrown.getStackTrace());
            if (code == 0){
                return null;
            }
            TreeMap<String, Object> params = new TreeMap<String, Object>();
            params.put("code", code);
            List<Hashcodes> resultHashCodes = PersistenceUtils.executeNamedQuery(em, "Hashcodes.findByCode", params, Hashcodes.class);
            for (Hashcodes hashcode : resultHashCodes) {
                Exceptions exc = hashcode.getExceptions();
                if (searchOnlyOpens && !PersistenceUtils.exceptionIsOpen(exc)){
                    continue;
                }
                if (verify(em, thrown, exc) && FilteringUtils.sameMessages(thrown, exc.getStacktrace())){
                    return exc;
                }
            }
            return null;
        }

        // verify the same stacktraces - hashcode is not sure for it
        private boolean verify(EntityManager em, Throwable thrown, org.netbeans.modules.exceptions.entity.Exceptions exception){
            return FilteringUtils.verifyTheSameStacktraces(em, thrown, exception.getStacktrace(), -1);
        }
    }
    
    public class LinesCheckingFilter implements CheckingFilter{
        private final int depth;
        
        public LinesCheckingFilter(int depth){
            this.depth = depth;
        }
        
        public org.netbeans.modules.exceptions.entity.Exceptions check(EntityManager em, Throwable thrown, boolean searchOnlyOpens) {
            List<Stacktrace> stacks = getSimilarStacktraces(em, thrown, searchOnlyOpens);
            if (stacks.isEmpty()){
                return null;
            }
            return FilteringUtils.getRootExceptions(stacks.get(0));
        }
        
        public List<Stacktrace> getSimilarStacktraces(EntityManager em, Throwable thrown, boolean searchOnlyOpens){
            // no exact matching found - try to go through lines
            TreeMap<String, Object> params = new TreeMap<String, Object>();
            String linesConcatenation = null, methodName;
            List<Stacktrace>  stacktracesResult = null;
            StackTraceElement element;
            Set<Stacktrace> stackTraceIds = null, nextIds = null;
            List<Stacktrace> last = null;
            int nbLinesCount = 0;
            for (int i = 0; i < thrown.getStackTrace().length; i++) {
                element = thrown.getStackTrace()[i];
                methodName = Method.getMethodNameFromSTEWithoutNumbers(element);// NOI18N
                if (linesConcatenation == null){
                    linesConcatenation = methodName;
                }else{
                    linesConcatenation = linesConcatenation.concat(methodName);
                }
                if (nbLinesCount >= depth - 2){ // skip query for few first lines - QUERY 'SELECT l.stacktrace FROM Line l WHERE l.lineHashcode = :code' REACHED MAX RESULT
                    params.put("code", linesConcatenation.hashCode());//rewrite previous code
                    stacktracesResult = PersistenceUtils.executeNamedQuery(em, "Line.findStacktraceByLineHash", params);
                    if (stackTraceIds == null){//fill all results of top lines matching
                        stackTraceIds = new HashSet<Stacktrace>(stacktracesResult);
                        FilteringUtils.removeWrongClasses(thrown, stackTraceIds);
                        if (searchOnlyOpens){
                            removeClosed(stackTraceIds);
                        }
                    }else{
                        nextIds = new HashSet<Stacktrace>(stacktracesResult);
                    }
                    if (nextIds != null) {
                        stackTraceIds.retainAll(nextIds); //in stackTraceIds is now an intersection with nextIds
                    }
                    if ((stackTraceIds.isEmpty())||(i == thrown.getStackTrace().length - 1)) {
                        if ((last != null) && (last.size() > 0) && ((nbLinesCount >= depth)||(i == thrown.getStackTrace().length - 1))) {
                            return last;
                        }
                        break; // break allways
                    }
                    if (stackTraceIds.size() > 0){
                       last = new ArrayList<Stacktrace>(stackTraceIds);
                    }
                }
                if (Utils.isNbLine(element)){
                    nbLinesCount++;
                }
            }
            return Collections.emptyList();
        }

        private void removeClosed(Set<Stacktrace> stackTraceIds) {
            Iterator<Stacktrace> it = stackTraceIds.iterator();
            while (it.hasNext()){
                Stacktrace next = it.next();
                Exceptions exc = FilteringUtils.getRootExceptions(next);
                if (exc == null){
                    Logger.getLogger(CheckingFilter.class.getName()).log(Level.SEVERE, "stacktrace {0} has null exception", next.getId());
                }
                if (!PersistenceUtils.exceptionIsOpen(exc)){
                    it.remove();
                }
            }
        }
    }
    
    public class StackOverflowCheckingFilter implements CheckingFilter{
        private static final String STACK_OVERFLOW_MESSAGE = "StackOverflowError";
        private static final String SELECT_STACKTRACE_BY_NAME_FROM_LINE = 
                "SELECT distinct l.stacktrace FROM Line l, Method m WHERE l.linePK.methodId = m.id AND m.name = :name";  //NOI18N;
        public Exceptions check(EntityManager em, Throwable thrown, boolean searchOnlyOpens) {
            if (thrown.getMessage() == null){
                return null;
            }
            String className = FilteringUtils.getClassOfMessage(thrown.getMessage());
            if (className.contains(STACK_OVERFLOW_MESSAGE)){
                StackTraceElement[] elements = thrown.getStackTrace();
                int centeralLine = elements.length /2;
                boolean found = false;
                StackTraceElement firstLine = elements[centeralLine];
                int cycleEnd;
                for (cycleEnd = centeralLine + 1; cycleEnd < elements.length; cycleEnd++){
                    if (firstLine.equals(elements[cycleEnd])){
                        found = true;
                        break;
                    }
                }
                if (found==false){
                    Logger.getLogger(StackOverflowCheckingFilter.class.getName())
                            .log(Level.SEVERE, "NO RECURSION FOUND", thrown);
                    return null;
                }
                String shortestLine = Method.getMethodNameFromSTEWithoutNumbers(elements[centeralLine]);
                int shortestLineIndex = centeralLine;
                for (int j = centeralLine + 1; j < cycleEnd; j++){
                    String concatenation = Method.getMethodNameFromSTEWithoutNumbers(elements[j]);
                    if (shortestLine.compareTo(concatenation) < 0){
                        shortestLine = concatenation;
                        shortestLineIndex = j;
                    }
                }
                Map<String, Object> params = Collections.singletonMap("name", (Object)shortestLine);
                List<Stacktrace>  stacktracesResult;//candidates for duplicates
                stacktracesResult = PersistenceUtils.executeQuery(em, SELECT_STACKTRACE_BY_NAME_FROM_LINE, params);
                for (Stacktrace stacktrace : stacktracesResult) {
                    if (!stacktrace.getClass1().contains(STACK_OVERFLOW_MESSAGE)){
                        continue;
                    }
                    Exceptions exc = FilteringUtils.getRootExceptions(stacktrace);
                    if (searchOnlyOpens && !PersistenceUtils.exceptionIsOpen(exc)){
                        continue;
                    }
                    List<Line> lines = new ArrayList<Line>(stacktrace.getLineCollection());
                    Collections.sort(lines, new LineComparator());
                    Iterator<Line> iterator = lines.iterator();
                    while (iterator.hasNext()){
                        Line line = iterator.next();
                        int j = 0;
                        while (FilteringUtils.areEquals(line, elements[shortestLineIndex+j])){
                            line = iterator.next();
                            j++;
                            if (j >= centeralLine - cycleEnd){
                                return exc;
                            }
                        }
                    }
                }
            }
            return null;
        }
        
    } 
}

