/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */
package org.netbeans.server.uihandler;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Logger;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import org.netbeans.lib.uihandler.MultiPartHandler;

public final class RequestFacadeImpl implements MultiPartHandler.RequestFacade {
    private static final File tmpDir = new File(System.getProperty("java.io.tmpdir"));
    private static final Logger LOG = Logger.getLogger(RequestFacadeImpl.class.getName());
    private final HttpServletRequest request;
    private InputFacadeImpl in;

    public RequestFacadeImpl(HttpServletRequest req) {
        super();
        this.request = req;
    }

    public int getContentLength() {
        return request.getContentLength();
    }

    public String getContentType() {
        return request.getContentType();
    }

    public MultiPartHandler.InputFacade getInput() throws IOException {
        if (in == null) {
            in = new InputFacadeImpl(request.getInputStream());
        }
        return in;
    }

    private static final class InputFacadeImpl implements MultiPartHandler.InputFacade {

        private final ServletInputStream in;

        public InputFacadeImpl(ServletInputStream in) {
            super();
            this.in = in;
        }

        public int readLine(byte[] arr, int off, int len) throws IOException {
            return in.readLine(arr, off, len);
        }

        public InputStream getInputStream() {
            return in;
        }
    }

    public static File upload(HttpServletRequest request) throws IOException {
        String contentType = request.getContentType();
        if (contentType == null || !contentType.toLowerCase().startsWith("multipart/form-data")) {
            return null;
        }

        RequestFacadeImpl facade = new RequestFacadeImpl(request);
        MultiPartHandler multiReq = new MultiPartHandler(facade, tmpDir.getAbsolutePath(), Utils.getMaxUploadSize());
        try{
            multiReq.parseMultipartUpload();
        }finally{
            facade.getInput().getInputStream().close();
        }

        // the file is uploaded as arguments "logs"
        File appFile = multiReq.getFile("logs"); // NOI18N
        if (appFile == null) {
            return null;
        }
        File result =  move(appFile, Utils.getRootUploadDirPath());

        File messagesFile = multiReq.getFile("messages");
        if (messagesFile != null){
            String messagesRootUploadDir = Utils.getMessagesRootUploadDir();
            String messagesUploadDir = Utils.getUploadDirPath(messagesRootUploadDir, getId(appFile));
            File targetFile = new File(messagesUploadDir, result.getName());
            if (!messagesFile.renameTo(targetFile)) {
                LOG.severe("moving " + messagesFile.getPath() + " to " + targetFile.getPath() + " failed");
            } else {
                LOG.fine("moving " + messagesFile.getPath() + " to " + targetFile.getPath()  + " done");
            }
        }
        moveAdditional(multiReq.getFile("slowness"), appFile, result.getName(), Utils.getSlownessRootUploadDir());
        moveAdditional(multiReq.getFile("heapdump"), appFile, result.getName(), Utils.getHeapRootUploadDir());
        
        return result;
    }

    static void moveAdditional(File additionalFile, File appFile, String targetFilename, String rootUploadDir){
        if (additionalFile != null){
            if (!additionalFile.exists()){
                throw new IllegalStateException("heap file is not null, but doesn't exists");
            }
            if (additionalFile.length() == 0L){
                throw new IllegalStateException("heap file exists, but is empty");
            }
            String heapUploadDir = Utils.getUploadDirPath(rootUploadDir, getId(appFile));
            File targetFile = new File(heapUploadDir, targetFilename);
            if (!additionalFile.renameTo(targetFile)) {
                LOG.severe("moving " + additionalFile.getPath() + " to " + targetFile.getPath() + " failed");
            }else{
                LOG.fine("moving " + additionalFile.getPath() + " to " + targetFile.getPath() + " done");
            }
        }
    }

    static File move(File appFile, String rootUploadDir) {
        File result = null;
        String id = getId(appFile);
        String uploadDir = Utils.getUploadDirPath(rootUploadDir, id);
        if (new File(uploadDir, id).exists()) {
            int i = 0;
            while (new File(uploadDir, id + "." + i).exists()) {
                i++;
            }
            result = new File(uploadDir, id + "." + i);
        } else {
            result = new File(uploadDir, id);
        }
        if (!appFile.renameTo(result)) {
            LOG.severe("moving " + appFile.getPath() + " to " + result.getPath() + " failed");
        }
        return result;
    }

    private static String getId(File file){
        String[] idAndSer = file.getName().split("\\.");
        return idAndSer[0];
    }
}
