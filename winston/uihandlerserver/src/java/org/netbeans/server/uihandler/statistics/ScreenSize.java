/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */
package org.netbeans.server.uihandler.statistics;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.LogRecord;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;
import javax.servlet.jsp.PageContext;
import org.netbeans.server.uihandler.Statistics;
import org.netbeans.server.uihandler.statistics.ScreenSize.ScreenSizeBean;
import org.openide.util.lookup.ServiceProvider;

/**
 *
 * @author Jindrich Sedek
 */
@ServiceProvider(service=Statistics.class)
public class ScreenSize extends Statistics<ScreenSizeBean> {

    private static final ScreenSizeBean EMPTY = new ScreenSizeBean();
    private static final String MESSAGE = "SCREEN SIZE";
    private static final String RESOLUTION = "RESOLUTION";
    private static final String MONITORS = "MONITORS";

    public ScreenSize() {
        super("ScreenSize");
    }

    @Override
    protected ScreenSizeBean newData() {
        return EMPTY;
    }

    @Override
    protected ScreenSizeBean process(LogRecord rec) {
        if (MESSAGE.equals(rec.getMessage())) {
            Object[] params = rec.getParameters();
            if (params != null) {
                return new ScreenSizeBean(params);
            }
        }
        return newData();
    }

    @Override
    protected ScreenSizeBean finishSessionUpload(String userId, int sessionNumber, boolean initialParse, ScreenSizeBean d) {
        return d;
    }

    @Override
    protected void registerPageContext(PageContext page, String name, ScreenSizeBean data) {
        page.setAttribute(name + RESOLUTION, data.screenResolution);
        page.setAttribute(name + MONITORS, data.monitors);
    }

    @Override
    protected ScreenSizeBean join(ScreenSizeBean one, ScreenSizeBean two) {
        if (EMPTY.equals(one)){
            return two;
        }
        if (EMPTY.equals(two)){
            return one;
        }
        ScreenSizeBean result = new ScreenSizeBean();
        result.monitors = new HashMap<Integer, Integer>(one.monitors);
        addAllCounts(result.monitors, two.monitors);
        result.screenResolution = new HashMap<Dimension, Integer>(one.screenResolution);
        addAllCounts(result.screenResolution, two.screenResolution);
        return result;
    }

    @Override
    protected void write(Preferences pref, ScreenSizeBean d) throws BackingStoreException {
        writeMap(pref.node(RESOLUTION), d.screenResolution);
        writeMap(pref.node(MONITORS), d.monitors);
    }

    private Map<Dimension, Integer> readResolution(Preferences node) throws BackingStoreException {
        String[] keys = node.keys();
        Map<Dimension, Integer> map = new HashMap<Dimension, Integer>(keys.length);
        for (String string : keys) {
            map.put(Dimension.parse(string), node.getInt(string, 0));
        }
        return map;
    }

    @Override
    protected ScreenSizeBean read(Preferences pref) throws BackingStoreException {
        ScreenSizeBean bean = new ScreenSizeBean();
        bean.monitors = readCounts(pref.node(MONITORS));
        bean.screenResolution = readResolution(pref.node(RESOLUTION));
        return bean;
    }

    public static class ScreenSizeBean {

        Map<Dimension, Integer> screenResolution;
        Map<Integer, Integer> monitors;

        private ScreenSizeBean() {
        }

        private ScreenSizeBean(Object[] params) {
            Dimension key = new Dimension(intParam(params[0]), intParam(params[1]));
            screenResolution = Collections.singletonMap(key, 1);
            monitors = Collections.singletonMap(intParam(params[2]), 1);
        }

        private int intParam(Object param) {
            return Integer.parseInt(param.toString());
        }
    }

    public static class Dimension {

        private static final String SEPARATOR = "x";
        final int width;
        final int height;

        public Dimension(int width, int height) {
            this.width = width;
            this.height = height;
        }

        @Override
        public String toString() {
            return width + SEPARATOR + height;
        }

        private static Dimension parse(String string) {
            String[] pieces = string.split(SEPARATOR);
            return new Dimension(Integer.parseInt(pieces[0]), Integer.parseInt(pieces[1]));
        }

        @Override
        public boolean equals(Object obj) {
            if (!(obj instanceof Dimension)){
                return false;
            }
            Dimension dim = (Dimension) obj;
            return (width == dim.width) && (height == dim.height);
        }

        @Override
        public int hashCode() {
            int hash = 3;
            hash = 97 * hash + this.width;
            hash = 97 * hash + this.height;
            return hash;
        }

    }
}
