/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.netbeans.server.uihandler.statistics;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.LogRecord;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;
import javax.servlet.jsp.PageContext;
import org.netbeans.server.uihandler.Statistics;
import org.netbeans.server.uihandler.statistics.CompilerErrorStatistics.DataBean;
import org.openide.util.lookup.ServiceProvider;

/**
 *
 * @author Melissa Stefik
 */
@ServiceProvider(service = Statistics.class)
public class CompilerErrorStatistics extends Statistics<DataBean> {
    private static final DataBean.ErrorBean EMPTY_BEAN = new DataBean.ErrorBean(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
    private static final DataBean EMPTY = new DataBean(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ,0, 0, 0, 0, 0, 0, EMPTY_BEAN);

    public static final String STATISTIC_NAME = "CompilerErrorStatistics";

    public CompilerErrorStatistics() {
        super(STATISTIC_NAME);
    }

    @Override
    protected DataBean newData() {
        return EMPTY;
    }

    @Override
    protected DataBean process(LogRecord rec) {
        if ("SODBEANS_BUILD".equals(rec.getMessage())){
            //get the start time
            String id = (String)rec.getParameters()[0];
            long start = Long.parseLong(id);
            
            //get the end time
            id = (String)rec.getParameters()[1];
            long end = Long.parseLong(id);
            
            //get the compiled flag
            id = (String)rec.getParameters()[2];
            boolean compiled = Boolean.parseBoolean(id);
            
            //get the number of errors
            id = (String)rec.getParameters()[3];
            int numbErrors = Integer.parseInt(id);
            
            if(compiled){
                return new DataBean(1, end - start, 1, start, end, 0, 0,0,0,0,0,0,0,0,0,0,0,0,0, EMPTY_BEAN);
            }else{
                return new DataBean(1, end - start, 0, start, end, 0, 0,0,0,0,0,0,0,0,0,0,0,0,0, EMPTY_BEAN);
            }
            
        }else if("SODBEANS_COMPILER_ERROR".equals(rec.getMessage())){
            //get the type
            String type = (String)rec.getParameters()[0];
            DataBean.ErrorBean err;
            if(type.equals("Cannot find symbol - variable")){
                err= new DataBean.ErrorBean(1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
            }else if(type.equals("Expected closure")){
                err = new DataBean.ErrorBean(1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
            }else if(type.equals("Missing - return")){
                err = new DataBean.ErrorBean(1,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
            }else if(type.equals("Cannot find symbol - method")){
                err = new DataBean.ErrorBean(1,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
            }else if(type.equals("Missing main method")){
                err = new DataBean.ErrorBean(1,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
            }else if(type.equals("Incompatible types")){
                err = new DataBean.ErrorBean(1,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
            }else if(type.equals("Identifier expected")){
                err = new DataBean.ErrorBean(1,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
            }else if(type.equals("Cannot find symbol - class")){
                err = new DataBean.ErrorBean(1,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
            }else if(type.equals("Missing - if")){
                err = new DataBean.ErrorBean(1,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
            }else if(type.equals("Missing - then")){
                err = new DataBean.ErrorBean(1,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
            }else if(type.equals("Cannot find symbol - parent")){
                err = new DataBean.ErrorBean(1,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
            }else if(type.equals("Cannot find symbol - package")){
                err = new DataBean.ErrorBean(1,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
            }else if(type.equals("Invalid operator")){
                err = new DataBean.ErrorBean(1,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
            }else if(type.equals("Unreachable statements")){
                err = new DataBean.ErrorBean(1,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
            }else if(type.equals("Already defined")){
                err = new DataBean.ErrorBean(1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
            }else if(type.equals("End of file error")){
                err = new DataBean.ErrorBean(1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
            }else if(type.equals("Circular inheritance")){
                err = new DataBean.ErrorBean(1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
            }else if(type.equals("Cannot inherit a from null")){
                err = new DataBean.ErrorBean(1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
            }else if(type.equals("Cannot override a private method")){
                err = new DataBean.ErrorBean(1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
            }else if(type.equals("Ambiguous inheritance")){
                err = new DataBean.ErrorBean(1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0);
            }else if(type.equals("Invalid overriding return type")){
                err = new DataBean.ErrorBean(1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0);
            }else if(type.equals("Ambigous method call")){
                err = new DataBean.ErrorBean(1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0);
            }else if(type.equals("Ambiguous used package")){
                err = new DataBean.ErrorBean(1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0);
            }else if(type.equals("Class already defined")){
                err = new DataBean.ErrorBean(1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0);
            }else if(type.equals("Invalid if expression")){
                err = new DataBean.ErrorBean(1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0);
            }else if(type.equals("Mismatched class template")){
                err = new DataBean.ErrorBean(1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0);
            }else if(type.equals("Cannot instantiate abstract class")){
                err = new DataBean.ErrorBean(1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0);
            }else if(type.equals("Cannot instantiate 'me'")){
                err = new DataBean.ErrorBean(1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0);
            }else if(type.equals("Cannot instantiate generic object")){
                err = new DataBean.ErrorBean(1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0);
            }else if(type.equals("Invalid error type")){
                err = new DataBean.ErrorBean(1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0);
            }else if(type.equals("Invalid return now")){
                err = new DataBean.ErrorBean(1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0);
            }else if(type.equals("Method already defined")){
                err = new DataBean.ErrorBean(1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0);
            }else{
                err = new DataBean.ErrorBean(1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1);
            }
            
            
            return new DataBean(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,err);
        }else if("IDE_STARTUP".equals(rec.getMessage())){
            return new DataBean(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1, EMPTY_BEAN);
        }else {
            DataBean EMPTY = new DataBean(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, EMPTY_BEAN);
            return EMPTY;
        }
    }

    @Override
    protected DataBean finishSessionUpload(String userId, int sessionNumber, boolean initialParse, DataBean d) {
        return new DataBean(d.getDebugActionCount(), d.getAvgCompileTime(), d.getNumberOfErrorFreeSessions(), d.getStartTime(), d.getEndTime(), d.getTenGroup(), d.getTwentyGroup(),
                d.getThirtyGroup(), d.getFourtyGroup(), d.getFiftyGroup(), d.getSixtyGroup(), d.getSeventyGroup(), d.getEightyGroup(), d.getNinetyGroup(), d.getOneHundredGroup(), 
                d.getOneHundredTenGroup(), d.getOneHundredTwentyGroup(), d.getGreaterGroup(),0, d.getCompilerErrors());
    }

    @Override
    protected DataBean join(DataBean one, DataBean two) {
        long timeBetweenCompile = two.getStartTime() - one.getEndTime();
        DataBean dataBean = null;
        
        ArrayList<Integer> list = new ArrayList<Integer>();
        DataBean.ErrorBean err =new DataBean.ErrorBean(one.getCompilerErrors().getTotal() + two.getCompilerErrors().getTotal(), one.getCompilerErrors().getVarDef() + two.getCompilerErrors().getVarDef(), one.getCompilerErrors().getClosure() + two.getCompilerErrors().getClosure(), one.getCompilerErrors().getRet() + two.getCompilerErrors().getRet(),
                    one.getCompilerErrors().getMethDef() + two.getCompilerErrors().getMethDef(), one.getCompilerErrors().getMissingMain() + two.getCompilerErrors().getMissingMain(), one.getCompilerErrors().getTypes() + two.getCompilerErrors().getTypes(), one.getCompilerErrors().getIdentExp() + two.getCompilerErrors().getIdentExp(),
                    one.getCompilerErrors().getExpClassType() + two.getCompilerErrors().getExpClassType(), one.getCompilerErrors().getMissingIf() + two.getCompilerErrors().getMissingIf(), one.getCompilerErrors().getMissingThen() + two.getCompilerErrors().getMissingThen(), one.getCompilerErrors().getMissingParent() + two.getCompilerErrors().getMissingParent(), 
                    one.getCompilerErrors().getMissingPackage() + two.getCompilerErrors().getMissingPackage(), one.getCompilerErrors().getInvalidOp() + two.getCompilerErrors().getInvalidOp(), one.getCompilerErrors().getUnreachable() + two.getCompilerErrors().getUnreachable(), one.getCompilerErrors().getDuplicate() + two.getCompilerErrors().getDuplicate(), one.getCompilerErrors().getEof() + two.getCompilerErrors().getEof(),
                    one.getCompilerErrors().getCircularInh() + two.getCompilerErrors().getCircularInh(), one.getCompilerErrors().getInhNull() + two.getCompilerErrors().getInhNull(), one.getCompilerErrors().getOverridePrivate() + two.getCompilerErrors().getOverridePrivate(), one.getCompilerErrors().getAmbiguousInh() + two.getCompilerErrors().getAmbiguousInh(), 
                    one.getCompilerErrors().getInvalidRet() + two.getCompilerErrors().getInvalidRet(), one.getCompilerErrors().getAmbiguousCall() + two.getCompilerErrors().getAmbiguousCall(), one.getCompilerErrors().getAmbiguousUse() + two.getCompilerErrors().getAmbiguousUse(), one.getCompilerErrors().getClassDup() + two.getCompilerErrors().getClassDup(),
                    one.getCompilerErrors().getIfExpr() + two.getCompilerErrors().getIfExpr(), one.getCompilerErrors().getTemplates() + two.getCompilerErrors().getTemplates(), one.getCompilerErrors().getInstAbstract() + two.getCompilerErrors().getInstAbstract(), one.getCompilerErrors().getInstMe() + two.getCompilerErrors().getInstMe(),
                    one.getCompilerErrors().getInstGeneric() + two.getCompilerErrors().getInstGeneric(), one.getCompilerErrors().getInvalidError() + two.getCompilerErrors().getInvalidError(), one.getCompilerErrors().getInvalidReturn() + two.getCompilerErrors().getInvalidReturn(), one.getCompilerErrors().getMethodDup() + two.getCompilerErrors().getMethodDup(),
                    one.getCompilerErrors().getOther() + two.getCompilerErrors().getOther());
        
        if(two.isNewSession() == 1){
            dataBean = EMPTY;
        }else if(one.startTime > 0 && two.startTime == 0){
            dataBean = new DataBean(one.getDebugActionCount() + two.getDebugActionCount(),
                                    (one.getAvgCompileTime() + two.getAvgCompileTime())/2,
                                    one.getNumberOfErrorFreeSessions() + two.getNumberOfErrorFreeSessions(), 
                                    one.getStartTime() + two.getStartTime(), one.getStartTime() + two.getEndTime(),
                                    one.getTenGroup() + two.getTenGroup(), one.getTwentyGroup() + two.getTwentyGroup(),
                                    one.getThirtyGroup() + two.getThirtyGroup(), one.getFourtyGroup() + two.getFourtyGroup(), one.getFiftyGroup() + two.getFiftyGroup(),
                                    one.getSixtyGroup() + two.getSixtyGroup(), one.getSeventyGroup() + two.getSeventyGroup(), one.getEightyGroup() + two.getEightyGroup(),
                                    one.getNinetyGroup() + two.getNinetyGroup(), one.getOneHundredGroup() + two.getOneHundredGroup(), one.getOneHundredTenGroup() + two.getOneHundredTenGroup(),
                                    one.getOneHundredTwentyGroup() + two.getOneHundredTwentyGroup(), one.getGreaterGroup() + two.getGreaterGroup(),0,err);
        }else{
            if(timeBetweenCompile == 0 || one.getEndTime() == 0){
                dataBean = new DataBean(one.getDebugActionCount() + two.getDebugActionCount(),
                                    (one.getAvgCompileTime() + two.getAvgCompileTime())/2,
                                    one.getNumberOfErrorFreeSessions() + two.getNumberOfErrorFreeSessions(), 
                                    two.getStartTime(), two.getEndTime(),
                                    one.getTenGroup() + two.getTenGroup(), one.getTwentyGroup() + two.getTwentyGroup(),
                                    one.getThirtyGroup() + two.getThirtyGroup(), one.getFourtyGroup() + two.getFourtyGroup(), one.getFiftyGroup() + two.getFiftyGroup(),
                                    one.getSixtyGroup() + two.getSixtyGroup(), one.getSeventyGroup() + two.getSeventyGroup(), one.getEightyGroup() + two.getEightyGroup(),
                                    one.getNinetyGroup() + two.getNinetyGroup(), one.getOneHundredGroup() + two.getOneHundredGroup(), one.getOneHundredTenGroup() + two.getOneHundredTenGroup(),
                                    one.getOneHundredTwentyGroup() + two.getOneHundredTwentyGroup(), one.getGreaterGroup() + two.getGreaterGroup(),0,err);   
            } else if (timeBetweenCompile <= 10000) {
                dataBean = new DataBean(one.getDebugActionCount() + two.getDebugActionCount(),
                                    (one.getAvgCompileTime() + two.getAvgCompileTime())/2,
                                    one.getNumberOfErrorFreeSessions() + two.getNumberOfErrorFreeSessions(), 
                                    two.getStartTime(), two.getEndTime(),
                                    one.getTenGroup() + 1, one.getTwentyGroup() + two.getTwentyGroup(),
                                    one.getThirtyGroup() + two.getThirtyGroup(), one.getFourtyGroup() + two.getFourtyGroup(), one.getFiftyGroup() + two.getFiftyGroup(),
                                    one.getSixtyGroup() + two.getSixtyGroup(), one.getSeventyGroup() + two.getSeventyGroup(), one.getEightyGroup() + two.getEightyGroup(),
                                    one.getNinetyGroup() + two.getNinetyGroup(), one.getOneHundredGroup() + two.getOneHundredGroup(), one.getOneHundredTenGroup() + two.getOneHundredTenGroup(),
                                    one.getOneHundredTwentyGroup() + two.getOneHundredTwentyGroup(), one.getGreaterGroup() + two.getGreaterGroup(),0,err);
            } else if (timeBetweenCompile <= 20000) {
                dataBean = new DataBean(one.getDebugActionCount() + two.getDebugActionCount(),
                                    (one.getAvgCompileTime() + two.getAvgCompileTime())/2,
                                    one.getNumberOfErrorFreeSessions() + two.getNumberOfErrorFreeSessions(), 
                                    two.getStartTime(), two.getEndTime(),
                                    one.getTenGroup() + two.getTenGroup(), one.getTwentyGroup() + 1,
                                    one.getThirtyGroup() + two.getThirtyGroup(), one.getFourtyGroup() + two.getFourtyGroup(), one.getFiftyGroup() + two.getFiftyGroup(),
                                    one.getSixtyGroup() + two.getSixtyGroup(), one.getSeventyGroup() + two.getSeventyGroup(), one.getEightyGroup() + two.getEightyGroup(),
                                    one.getNinetyGroup() + two.getNinetyGroup(), one.getOneHundredGroup() + two.getOneHundredGroup(), one.getOneHundredTenGroup() + two.getOneHundredTenGroup(),
                                    one.getOneHundredTwentyGroup() + two.getOneHundredTwentyGroup(), one.getGreaterGroup() + two.getGreaterGroup(),0,err);
            }else if (timeBetweenCompile <= 30000){
                                dataBean = new DataBean(one.getDebugActionCount() + two.getDebugActionCount(),
                                    (one.getAvgCompileTime() + two.getAvgCompileTime())/2,
                                    one.getNumberOfErrorFreeSessions() + two.getNumberOfErrorFreeSessions(), 
                                    two.getStartTime(), two.getEndTime(),
                                    one.getTenGroup() + two.getTenGroup(), one.getTwentyGroup() + two.getTwentyGroup(),
                                    one.getThirtyGroup() + 1, one.getFourtyGroup() + two.getFourtyGroup(), one.getFiftyGroup() + two.getFiftyGroup(),
                                    one.getSixtyGroup() + two.getSixtyGroup(), one.getSeventyGroup() + two.getSeventyGroup(), one.getEightyGroup() + two.getEightyGroup(),
                                    one.getNinetyGroup() + two.getNinetyGroup(), one.getOneHundredGroup() + two.getOneHundredGroup(), one.getOneHundredTenGroup() + two.getOneHundredTenGroup(),
                                    one.getOneHundredTwentyGroup() + two.getOneHundredTwentyGroup(), one.getGreaterGroup() + two.getGreaterGroup(),0,err);
                
            }else if (timeBetweenCompile <= 40000){
                                dataBean = new DataBean(one.getDebugActionCount() + two.getDebugActionCount(),
                                    (one.getAvgCompileTime() + two.getAvgCompileTime())/2,
                                    one.getNumberOfErrorFreeSessions() + two.getNumberOfErrorFreeSessions(), 
                                    two.getStartTime(), two.getEndTime(),
                                    one.getTenGroup() + two.getTenGroup(), one.getTwentyGroup() + two.getTwentyGroup(),
                                    one.getThirtyGroup() + two.getThirtyGroup(), one.getFourtyGroup() + 1, one.getFiftyGroup() + two.getFiftyGroup(),
                                    one.getSixtyGroup() + two.getSixtyGroup(), one.getSeventyGroup() + two.getSeventyGroup(), one.getEightyGroup() + two.getEightyGroup(),
                                    one.getNinetyGroup() + two.getNinetyGroup(), one.getOneHundredGroup() + two.getOneHundredGroup(), one.getOneHundredTenGroup() + two.getOneHundredTenGroup(),
                                    one.getOneHundredTwentyGroup() + two.getOneHundredTwentyGroup(), one.getGreaterGroup() + two.getGreaterGroup(),0,err);
                
            }else if (timeBetweenCompile <= 50000){
                                dataBean = new DataBean(one.getDebugActionCount() + two.getDebugActionCount(),
                                    (one.getAvgCompileTime() + two.getAvgCompileTime())/2,
                                    one.getNumberOfErrorFreeSessions() + two.getNumberOfErrorFreeSessions(), 
                                    two.getStartTime(), two.getEndTime(),
                                    one.getTenGroup() + two.getTenGroup(), one.getTwentyGroup() + two.getTwentyGroup(),
                                    one.getThirtyGroup() + two.getThirtyGroup(), one.getFourtyGroup() + two.getFourtyGroup(), one.getFiftyGroup() + 1,
                                    one.getSixtyGroup() + two.getSixtyGroup(), one.getSeventyGroup() + two.getSeventyGroup(), one.getEightyGroup() + two.getEightyGroup(),
                                    one.getNinetyGroup() + two.getNinetyGroup(), one.getOneHundredGroup() + two.getOneHundredGroup(), one.getOneHundredTenGroup() + two.getOneHundredTenGroup(),
                                    one.getOneHundredTwentyGroup() + two.getOneHundredTwentyGroup(), one.getGreaterGroup() + two.getGreaterGroup(),0,err);
                
            }else if (timeBetweenCompile <= 60000){
                                dataBean = new DataBean(one.getDebugActionCount() + two.getDebugActionCount(),
                                    (one.getAvgCompileTime() + two.getAvgCompileTime())/2,
                                    one.getNumberOfErrorFreeSessions() + two.getNumberOfErrorFreeSessions(), 
                                    two.getStartTime(), two.getEndTime(),
                                    one.getTenGroup() + two.getTenGroup(), one.getTwentyGroup() + two.getTwentyGroup(),
                                    one.getThirtyGroup() + two.getThirtyGroup(), one.getFourtyGroup() + two.getFourtyGroup(), one.getFiftyGroup() + two.getFiftyGroup(),
                                    one.getSixtyGroup() + 1, one.getSeventyGroup() + two.getSeventyGroup(), one.getEightyGroup() + two.getEightyGroup(),
                                    one.getNinetyGroup() + two.getNinetyGroup(), one.getOneHundredGroup() + two.getOneHundredGroup(), one.getOneHundredTenGroup() + two.getOneHundredTenGroup(),
                                    one.getOneHundredTwentyGroup() + two.getOneHundredTwentyGroup(), one.getGreaterGroup() + two.getGreaterGroup(),0,err);
                
            }else if (timeBetweenCompile <= 70000){
                                dataBean = new DataBean(one.getDebugActionCount() + two.getDebugActionCount(),
                                    (one.getAvgCompileTime() + two.getAvgCompileTime())/2,
                                    one.getNumberOfErrorFreeSessions() + two.getNumberOfErrorFreeSessions(), 
                                    two.getStartTime(), two.getEndTime(),
                                    one.getTenGroup() + two.getTenGroup(), one.getTwentyGroup() + two.getTwentyGroup(),
                                    one.getThirtyGroup() + two.getThirtyGroup(), one.getFourtyGroup() + two.getFourtyGroup(), one.getFiftyGroup() + two.getFiftyGroup(),
                                    one.getSixtyGroup() + two.getSixtyGroup(), one.getSeventyGroup() + 1, one.getEightyGroup() + two.getEightyGroup(),
                                    one.getNinetyGroup() + two.getNinetyGroup(), one.getOneHundredGroup() + two.getOneHundredGroup(), one.getOneHundredTenGroup() + two.getOneHundredTenGroup(),
                                    one.getOneHundredTwentyGroup() + two.getOneHundredTwentyGroup(), one.getGreaterGroup() + two.getGreaterGroup(),0,err);
                
            }else if (timeBetweenCompile <= 80000){
                                dataBean = new DataBean(one.getDebugActionCount() + two.getDebugActionCount(),
                                    (one.getAvgCompileTime() + two.getAvgCompileTime())/2,
                                    one.getNumberOfErrorFreeSessions() + two.getNumberOfErrorFreeSessions(), 
                                    two.getStartTime(), two.getEndTime(),
                                    one.getTenGroup() + two.getTenGroup(), one.getTwentyGroup() + two.getTwentyGroup(),
                                    one.getThirtyGroup() + two.getThirtyGroup(), one.getFourtyGroup() + two.getFourtyGroup(), one.getFiftyGroup() + two.getFiftyGroup(),
                                    one.getSixtyGroup() + two.getSixtyGroup(), one.getSeventyGroup() + two.getSeventyGroup(), one.getEightyGroup() + 1,
                                    one.getNinetyGroup() + two.getNinetyGroup(), one.getOneHundredGroup() + two.getOneHundredGroup(), one.getOneHundredTenGroup() + two.getOneHundredTenGroup(),
                                    one.getOneHundredTwentyGroup() + two.getOneHundredTwentyGroup(), one.getGreaterGroup() + two.getGreaterGroup(),0,err);
                
            }else if (timeBetweenCompile <= 90000){
                                dataBean = new DataBean(one.getDebugActionCount() + two.getDebugActionCount(),
                                    (one.getAvgCompileTime() + two.getAvgCompileTime())/2,
                                    one.getNumberOfErrorFreeSessions() + two.getNumberOfErrorFreeSessions(), 
                                    two.getStartTime(), two.getEndTime(),
                                    one.getTenGroup() + two.getTenGroup(), one.getTwentyGroup() + two.getTwentyGroup(),
                                    one.getThirtyGroup() + two.getThirtyGroup(), one.getFourtyGroup() + two.getFourtyGroup(), one.getFiftyGroup() + two.getFiftyGroup(),
                                    one.getSixtyGroup() + two.getSixtyGroup(), one.getSeventyGroup() + two.getSeventyGroup(), one.getEightyGroup() + two.getEightyGroup(),
                                    one.getNinetyGroup() + 1, one.getOneHundredGroup() + two.getOneHundredGroup(), one.getOneHundredTenGroup() + two.getOneHundredTenGroup(),
                                    one.getOneHundredTwentyGroup() + two.getOneHundredTwentyGroup(), one.getGreaterGroup() + two.getGreaterGroup(),0,err);
                
            }else if (timeBetweenCompile <= 100000){
                                dataBean = new DataBean(one.getDebugActionCount() + two.getDebugActionCount(),
                                    (one.getAvgCompileTime() + two.getAvgCompileTime())/2,
                                    one.getNumberOfErrorFreeSessions() + two.getNumberOfErrorFreeSessions(), 
                                    two.getStartTime(), two.getEndTime(),
                                    one.getTenGroup() + two.getTenGroup(), one.getTwentyGroup() + two.getTwentyGroup(),
                                    one.getThirtyGroup() + two.getThirtyGroup(), one.getFourtyGroup() + two.getFourtyGroup(), one.getFiftyGroup() + two.getFiftyGroup(),
                                    one.getSixtyGroup() + two.getSixtyGroup(), one.getSeventyGroup() + two.getSeventyGroup(), one.getEightyGroup() + two.getEightyGroup(),
                                    one.getNinetyGroup() + two.getNinetyGroup(), one.getOneHundredGroup() + 1, one.getOneHundredTenGroup() + two.getOneHundredTenGroup(),
                                    one.getOneHundredTwentyGroup() + two.getOneHundredTwentyGroup(), one.getGreaterGroup() + two.getGreaterGroup(),0,err);
                
            }else if (timeBetweenCompile <= 110000){
                                dataBean = new DataBean(one.getDebugActionCount() + two.getDebugActionCount(),
                                    (one.getAvgCompileTime() + two.getAvgCompileTime())/2,
                                    one.getNumberOfErrorFreeSessions() + two.getNumberOfErrorFreeSessions(), 
                                    two.getStartTime(), two.getEndTime(),
                                    one.getTenGroup() + two.getTenGroup(), one.getTwentyGroup() + two.getTwentyGroup(),
                                    one.getThirtyGroup() + two.getThirtyGroup(), one.getFourtyGroup() + two.getFourtyGroup(), one.getFiftyGroup() + two.getFiftyGroup(),
                                    one.getSixtyGroup() + two.getSixtyGroup(), one.getSeventyGroup() + two.getSeventyGroup(), one.getEightyGroup() + two.getEightyGroup(),
                                    one.getNinetyGroup() + two.getNinetyGroup(), one.getOneHundredGroup() + two.getOneHundredGroup(), one.getOneHundredTenGroup() + 1,
                                    one.getOneHundredTwentyGroup() + two.getOneHundredTwentyGroup(), one.getGreaterGroup() + two.getGreaterGroup(),0,err);
                
            }else if (timeBetweenCompile <= 120000){
                                dataBean = new DataBean(one.getDebugActionCount() + two.getDebugActionCount(),
                                    (one.getAvgCompileTime() + two.getAvgCompileTime())/2,
                                    one.getNumberOfErrorFreeSessions() + two.getNumberOfErrorFreeSessions(), 
                                    two.getStartTime(), two.getEndTime(),
                                    one.getTenGroup() + two.getTenGroup(), one.getTwentyGroup() + two.getTwentyGroup(),
                                    one.getThirtyGroup() + two.getThirtyGroup(), one.getFourtyGroup() + two.getFourtyGroup(), one.getFiftyGroup() + two.getFiftyGroup(),
                                    one.getSixtyGroup() + two.getSixtyGroup(), one.getSeventyGroup() + two.getSeventyGroup(), one.getEightyGroup() + two.getEightyGroup(),
                                    one.getNinetyGroup() + two.getNinetyGroup(), one.getOneHundredGroup() + two.getOneHundredGroup(), one.getOneHundredTenGroup() + two.getOneHundredTenGroup(),
                                    one.getOneHundredTwentyGroup() + 1, one.getGreaterGroup() + two.getGreaterGroup(),0,err);
                
            }else{
                dataBean = new DataBean(one.getDebugActionCount() + two.getDebugActionCount(),
                                    (one.getAvgCompileTime() + two.getAvgCompileTime())/2,
                                    one.getNumberOfErrorFreeSessions() + two.getNumberOfErrorFreeSessions(), 
                                    two.getStartTime(), two.getEndTime(),
                                    one.getTenGroup() + two.getTenGroup(), one.getTwentyGroup() + two.getTwentyGroup(),
                                    one.getThirtyGroup() + two.getThirtyGroup(), one.getFourtyGroup() + two.getFourtyGroup(), one.getFiftyGroup() + two.getFiftyGroup(),
                                    one.getSixtyGroup() + two.getSixtyGroup(), one.getSeventyGroup() + two.getSeventyGroup(), one.getEightyGroup() + two.getEightyGroup(),
                                    one.getNinetyGroup() + two.getNinetyGroup(), one.getOneHundredGroup() + two.getOneHundredGroup(), one.getOneHundredTenGroup() + two.getOneHundredTenGroup(),
                                    one.getOneHundredTwentyGroup() + two.getOneHundredTwentyGroup(), one.getGreaterGroup() + 1, 0,err);
            }

        }
        return dataBean;
    }

    @Override
    protected void write(Preferences pref, DataBean d) throws BackingStoreException {
        pref.putInt("all_build", d.getDebugActionCount());
        pref.putLong("compile_time", d.getAvgCompileTime());
        pref.putInt("error_free_sessions", d.getNumberOfErrorFreeSessions());
        pref.putLong("start_time", d.getStartTime());
        pref.putLong("end_time", d.getEndTime());
        pref.putInt("tenGroup", d.getTenGroup());
        pref.putInt("twentyGroup", d.getTwentyGroup());
        pref.putInt("newSession", d.isNewSession());
        pref.putInt("total_error", d.getCompilerErrors().getTotal());
        pref.putInt("var_error", d.getCompilerErrors().getVarDef());
        pref.putInt("closure_error", d.getCompilerErrors().getClosure());
        pref.putInt("ret_error", d.getCompilerErrors().getRet());
        pref.putInt("method_error", d.getCompilerErrors().getMethDef());
        pref.putInt("main_error", d.getCompilerErrors().getMissingMain());
        pref.putInt("types_error", d.getCompilerErrors().getTypes());
        pref.putInt("identifier_error", d.getCompilerErrors().getIdentExp());
        pref.putInt("class_error", d.getCompilerErrors().getExpClassType());
        pref.putInt("else_if_error", d.getCompilerErrors().getMissingIf());
        pref.putInt("then_error", d.getCompilerErrors().getMissingThen());
        pref.putInt("parent_error", d.getCompilerErrors().getMissingParent());
        pref.putInt("package_error", d.getCompilerErrors().getMissingPackage());
        pref.putInt("invalid_op_error", d.getCompilerErrors().getInvalidOp());
        pref.putInt("unreachable_error", d.getCompilerErrors().getUnreachable());
        pref.putInt("duplicate_error", d.getCompilerErrors().getDuplicate());
        pref.putInt("eof_error", d.getCompilerErrors().getEof());
        pref.putInt("circular_inheritance_error", d.getCompilerErrors().getCircularInh());
        pref.putInt("inherit_null_error", d.getCompilerErrors().getInhNull());
        pref.putInt("override_private_error", d.getCompilerErrors().getOverridePrivate());
        pref.putInt("amb_inherit_error", d.getCompilerErrors().getAmbiguousInh());
        pref.putInt("invalid_ret_error", d.getCompilerErrors().getInvalidRet());
        pref.putInt("amb_call_error", d.getCompilerErrors().getAmbiguousCall());
        pref.putInt("amb_use_error", d.getCompilerErrors().getAmbiguousUse());
        pref.putInt("class_dup_error", d.getCompilerErrors().getClassDup());
        pref.putInt("if_expression_error", d.getCompilerErrors().getIfExpr());
        pref.putInt("template_error", d.getCompilerErrors().getTemplates());
        pref.putInt("inst_abstract_error", d.getCompilerErrors().getInstAbstract());
        pref.putInt("inst_me_error", d.getCompilerErrors().getInstMe());
        pref.putInt("inst_generic_error", d.getCompilerErrors().getInstGeneric());
        pref.putInt("invalid_error", d.getCompilerErrors().getInvalidError());
        pref.putInt("invalid_return_error", d.getCompilerErrors().getInvalidReturn());
        pref.putInt("method_dup_error", d.getCompilerErrors().getMethodDup());
        pref.putInt("other", d.getCompilerErrors().getOther());
        
    }

    @Override
    protected DataBean read(Preferences pref) throws BackingStoreException {
        DataBean.ErrorBean err = new DataBean.ErrorBean(pref.getInt("total_error", 0), pref.getInt("var_error", 0), pref.getInt("closure_error", 0),
                pref.getInt("ret_error", 0), pref.getInt("method_error", 0),pref.getInt("main_error", 0), pref.getInt("types_error", 0), 
                pref.getInt("identifier_error", 0), pref.getInt("class_error", 0), pref.getInt("else_if_error", 0), pref.getInt("then_error", 0), 
                pref.getInt("parent_error", 0), pref.getInt("package_error", 0), pref.getInt("invalid_op_error", 0), pref.getInt("unreachable_error", 0),
                pref.getInt("duplicate_error", 0), pref.getInt("eof_error", 0), pref.getInt("circular_inheritance_error", 0), pref.getInt("inherit_null_error", 0), 
                pref.getInt("override_private_error", 0), pref.getInt("amb_inherit_error", 0), pref.getInt("invalid_ret_error", 0), pref.getInt("amb_call_error", 0), 
                pref.getInt("amb_use_error", 0), pref.getInt("class_dup_error", 0), pref.getInt("if_expression_error", 0), pref.getInt("template_error", 0), 
                pref.getInt("inst_abstract_error", 0), pref.getInt("inst_me_error", 0), pref.getInt("inst_generic_error", 0), pref.getInt("invalid_error", 0),
                pref.getInt("invalid_return_error", 0), pref.getInt("method_dup_error", 0), pref.getInt("other", 0));
        
        return new DataBean(pref.getInt("all_build", 0), pref.getLong("compile_time", 0), pref.getInt("error_free_sessions", 0),
                pref.getLong("start_time", 0), pref.getLong("end_time", 0), pref.getInt("tenGroup", 0), pref.getInt("twentyGroup", 0),
                pref.getInt("thirtyGroup", 0), pref.getInt("fourtyGroup", 0), pref.getInt("fiftyGroup", 0), pref.getInt("sixtyGroup", 0),
                pref.getInt("seventyGroup", 0), pref.getInt("eightyGroup", 0), pref.getInt("ninetyGroup", 0), pref.getInt("oneHundreadGroup", 0),
                pref.getInt("oneHundreadTenGroup", 0), pref.getInt("oneHundreadTwentyGroup", 0), pref.getInt("greaterGroup", 0), pref.getInt("newSession", 0), err);
    }

    @Override
    protected void registerPageContext(PageContext page, String name, DataBean data) {
        page.setAttribute(name + "Usages", data.getUsages());
        page.setAttribute(name + "Avg", data.getAvgData());
        page.setAttribute(name + "Errors", data.getErrorData());
    }

    public static final class DataBean {

        private final int debugActionCount;
        private final long avgCompileTime;
        private final int numberOfErrorFreeSessions;
        private final long startTime;
        private final long endTime;
        private final int tenGroup;
        private final int twentyGroup;
        private final int thirtyGroup;
        private final int fourtyGroup;
        private final int fiftyGroup;
        private final int sixtyGroup;
        private final int seventyGroup;
        private final int eightyGroup;
        private final int ninetyGroup;
        private final int oneHundredGroup;
        private final int oneHundredTenGroup;
        private final int oneHundredTwentyGroup;
        private final int greaterGroup;
        private final int newSession;
        
        private final ErrorBean compilerErrors;
        
        

        public DataBean(int debugActionCount, long avgCompileTime, int numberOfErrorFreeSessions, 
                long startTime, long endTime, int tenGroup, int twentyGroup, int thirtyGroup, int fourtyGroup,
                int fiftyGroup, int sixtyGroup, int seventyGroup, int eightyGroup, int ninetyGroup, int oneHundredGroup,
                int oneHundredTenGroup, int oneHundredTwentyGroup, int greaterGroup, int newSession, ErrorBean compilerErrors) {
            this.debugActionCount = debugActionCount;
            this.avgCompileTime = avgCompileTime;
            this.numberOfErrorFreeSessions = numberOfErrorFreeSessions;
            this.startTime = startTime;
            this.endTime = endTime;
            this.tenGroup = tenGroup;
            this.twentyGroup = twentyGroup;
            this.thirtyGroup = thirtyGroup;
            this.fourtyGroup = fourtyGroup;
            this.fiftyGroup = fiftyGroup;
            this.sixtyGroup = sixtyGroup;
            this.seventyGroup = seventyGroup;
            this.eightyGroup = eightyGroup;
            this.ninetyGroup = ninetyGroup;
            this.oneHundredGroup = oneHundredGroup;
            this.oneHundredTenGroup = oneHundredTenGroup;
            this.oneHundredTwentyGroup = oneHundredTwentyGroup;
            this.greaterGroup = greaterGroup;
            this.newSession = newSession;
            this.compilerErrors = compilerErrors;
        }

        public int getDebugActionCount() {
            return debugActionCount;
        }

        public long getAvgCompileTime() {
            return avgCompileTime;
        }
        
        public int getNumberOfErrorFreeSessions(){
            return numberOfErrorFreeSessions;
        }
        
        public long getStartTime(){
            return startTime;
        }
        
        public long getEndTime(){
            return endTime;
        }
        
        public int getTenGroup(){
            return tenGroup;
        }
        
        public int getTwentyGroup(){
            return twentyGroup;
        }
        
        public ErrorBean getCompilerErrors(){
            return compilerErrors;
        }

        public Map getUsages() {
            Map usages = new HashMap();
            usages.put("Successful build", numberOfErrorFreeSessions);
            usages.put("Build with compiler errors", debugActionCount - numberOfErrorFreeSessions);
            return usages;
        }

        private Collection<ViewBean> getAvgData() {
            List<ViewBean> vb = new ArrayList<ViewBean>();
            vb.add(new ViewBean("1-10", tenGroup ));
            vb.add(new ViewBean("11-20", twentyGroup));
            vb.add(new ViewBean("21-30", thirtyGroup));
            vb.add(new ViewBean("31-40", fourtyGroup));
            vb.add(new ViewBean("41-50", fiftyGroup));
            vb.add(new ViewBean("51-60", sixtyGroup));
            vb.add(new ViewBean("61-70", seventyGroup));
            vb.add(new ViewBean("71-80", eightyGroup));
            vb.add(new ViewBean("81-90", ninetyGroup));
            vb.add(new ViewBean("91-90", oneHundredGroup));
            vb.add(new ViewBean("101-110", oneHundredTenGroup));
            vb.add(new ViewBean("111-120", oneHundredTwentyGroup));
            vb.add(new ViewBean("120<", greaterGroup));
            return vb;
        }
        
        private Collection<ViewBean> getErrorData(){
            List<ViewBean> eb = new ArrayList<ViewBean>();
            eb.add(new ViewBean("Cannot find symbol - variable", compilerErrors.getVarDef()));//undefined 
            eb.add(new ViewBean("Missing ( or ) or end", compilerErrors.getClosure()));//missing closure
            eb.add(new ViewBean("Missing - return", compilerErrors.getRet()));//missing return
            eb.add(new ViewBean("Cannot find symbol - method", compilerErrors.getMethDef()));//undefined method
            eb.add(new ViewBean("Missing main method", compilerErrors.getMissingMain()));//missing main
            eb.add(new ViewBean("Incompatible types", compilerErrors.getTypes()));//incompatible types
            eb.add(new ViewBean("<identifier> expected", compilerErrors.getIdentExp()));//identifier expected
            eb.add(new ViewBean("Cannot find symbol - class", compilerErrors.getExpClassType()));//undefined class
            eb.add(new ViewBean("Missing - if", compilerErrors.getMissingIf()));// missing if
            eb.add(new ViewBean("Missing - then", compilerErrors.getMissingThen())); //missing then
            eb.add(new ViewBean("Cannot find symbol - parent", compilerErrors.getMissingParent()));//missing parent
            eb.add(new ViewBean("Cannot find symbol - package", compilerErrors.getMissingPackage()));//missing package
            eb.add(new ViewBean("Invalid operator", compilerErrors.getInvalidOp()));//invalid op
            eb.add(new ViewBean("Unreachable statements", compilerErrors.getUnreachable()));//unreachable
            eb.add(new ViewBean("Already defined", compilerErrors.getDuplicate()));//already defined
            eb.add(new ViewBean("End of file error", compilerErrors.getEof()));//end of file
            eb.add(new ViewBean("Circular inheritance", compilerErrors.getCircularInh()));//circular inharitance
            eb.add(new ViewBean("Cannot inherit a from null", compilerErrors.getInhNull()));//null inheritance
            eb.add(new ViewBean("Cannot override a private method", compilerErrors.getOverridePrivate()));//override private
            eb.add(new ViewBean("Ambiguous inheritance", compilerErrors.getAmbiguousInh()));//ambiguous inheritance
            eb.add(new ViewBean("Invalid overriding return type", compilerErrors.getInvalidRet()));//override return type
            eb.add(new ViewBean("Ambiguous method call", compilerErrors.getAmbiguousCall()));//ambiguous method call
            eb.add(new ViewBean("Ambiguous used package", compilerErrors.getAmbiguousUse()));//ambiguous use
            eb.add(new ViewBean("Class already defined", compilerErrors.getClassDup()));//class duplicate
            eb.add(new ViewBean("Invalid if expression", compilerErrors.getIfExpr()));//invalid if expression
            eb.add(new ViewBean("Mismatched class template", compilerErrors.getTemplates()));//invalid template
            eb.add(new ViewBean("Cannot instantiate abstract class", compilerErrors.getInstAbstract()));//instantiate abstract
            eb.add(new ViewBean("Cannot instantiate 'me'", compilerErrors.getInstMe()));//instantiate me
            eb.add(new ViewBean("Cannot instantiate generic object", compilerErrors.getInstGeneric()));//instantiate generic
            eb.add(new ViewBean("Invalid error type", compilerErrors.getInvalidError()));//invalid error type
            eb.add(new ViewBean("Invalid return now", compilerErrors.getInvalidReturn()));//invalid return now
            eb.add(new ViewBean("Method already defined", compilerErrors.getMethodDup()));//duplicate method
            eb.add(new ViewBean("other", compilerErrors.getOther()));//other
            return eb;
        }

        /**
         * @return the thirtyGroup
         */
        public int getThirtyGroup() {
            return thirtyGroup;
        }

        /**
         * @return the fourtyGroup
         */
        public int getFourtyGroup() {
            return fourtyGroup;
        }

        /**
         * @return the fiftyGroup
         */
        public int getFiftyGroup() {
            return fiftyGroup;
        }

        /**
         * @return the sixtyGroup
         */
        public int getSixtyGroup() {
            return sixtyGroup;
        }

        /**
         * @return the seventyGroup
         */
        public int getSeventyGroup() {
            return seventyGroup;
        }

        /**
         * @return the eightyGroup
         */
        public int getEightyGroup() {
            return eightyGroup;
        }

        /**
         * @return the ninetyGroup
         */
        public int getNinetyGroup() {
            return ninetyGroup;
        }

        /**
         * @return the oneHundredGroup
         */
        public int getOneHundredGroup() {
            return oneHundredGroup;
        }

        /**
         * @return the oneHundredTenGroup
         */
        public int getOneHundredTenGroup() {
            return oneHundredTenGroup;
        }

        /**
         * @return the oneHundredTwentyGroup
         */
        public int getOneHundredTwentyGroup() {
            return oneHundredTwentyGroup;
        }

        /**
         * @return the greaterGroup
         */
        public int getGreaterGroup() {
            return greaterGroup;
        }

        private int isNewSession() {
            return newSession;
        }

        public static final class ViewBean {

            private final String name;
            private final int value;

            public ViewBean(String name, int value) {
                this.name = name;
                this.value = value;
            }

            public String getName() {
                return name;
            }

            public int getValue() {
                return value;
            }
        }
        
        public static final class ErrorBean {
            private final int varDef;
            private final int closure;
            private final int ret;
            private final int methDef;
            private final int missingMain;
            private final int types;
            private final int identExp;
            private final int expClassType;
            private final int missingIf;
            private final int missingThen;
            private final int total;
            private final int missingParent;
            private final int missingPackage;
            private final int invalidOp;
            private final int unreachable;
            private final int duplicate;
            private final int eof;
            private final int circularInh;
            private final int inhNull;
            private final int overridePrivate;
            private final int ambiguousInh;
            private final int invalidRet;
            private final int ambiguousCall;
            private final int ambiguousUse;
            private final int classDup;
            private final int ifExpr;
            private final int templates;
            private final int instAbstract;
            private final int instMe;
            private final int instGeneric;
            private final int invalidError;
            private final int invalidReturn;
            private final int methodDup;
            private final int other;
            
            public ErrorBean(int total, int varDef, int closure, int ret, int methDef, int missingMain, int types, int indentExp, int expClassType, int missingIf, 
                    int missingThen, int missingParent, int missingPackage, int invalidOp, int unreachable, int duplicate, int eof, int circularInh, 
                    int inhNull, int overridePrivate, int ambiguousInh, int invalidRet, int ambiguousCall, int ambiguousUse, int classDup, int ifExpr, int templates,
                    int instAbstract, int instMe, int instGeneric, int invalidError, int invalidReturn, int methodDup, int other){
                this.total = total;
                this.varDef = varDef;
                this.closure = closure;
                this.ret = ret;
                this.methDef = methDef;
                this.missingMain = missingMain;
                this.types = types;
                this.identExp = indentExp;
                this.expClassType = expClassType;
                this.missingIf = missingIf;
                this.missingThen = missingThen;
                this.missingParent = missingParent;
                this.missingPackage = missingPackage;
                this.invalidOp = invalidOp;
                this.unreachable = unreachable;
                this.duplicate = duplicate;
                this.eof = eof;
                this.circularInh = circularInh;
                this.inhNull = inhNull;
                this.overridePrivate = overridePrivate;
                this.ambiguousInh = ambiguousInh;
                this.invalidRet = invalidRet;
                this.ambiguousCall = ambiguousCall;
                this.ambiguousUse = ambiguousUse;
                this.classDup = classDup;
                this.ifExpr = ifExpr;
                this.templates = templates;
                this.instAbstract = instAbstract;
                this.instMe = instMe;
                this.instGeneric = instGeneric;
                this.invalidError = invalidError;
                this.invalidReturn = invalidReturn;
                this.methodDup = methodDup;
                this.other = other;
            }

            public int getOther(){
                return other;
            }
            public int getTotal(){
                return total;
            }
            public int getVarDef() {
                return varDef;
            }

            public int getClosure() {
                return closure;
            }

            public int getRet() {
                return ret;
            }

            public int getMethDef() {
                return methDef;
            }

            public int getMissingMain() {
                return missingMain;
            }

            public int getTypes() {
                return types;
            }

            public int getIdentExp() {
                return identExp;
            }

            public int getExpClassType() {
                return expClassType;
            }

            public int getMissingIf() {
                return missingIf;
            }

            public int getMissingThen() {
                return missingThen;
            }

            public int getMissingParent() {
                return missingParent;
            }

            public int getMissingPackage() {
                return missingPackage;
            }

            public int getInvalidOp() {
                return invalidOp;
            }

            public int getUnreachable() {
                return unreachable;
            }

            public int getDuplicate() {
                return duplicate;
            }

            public int getEof() {
                return eof;
            }

            public int getCircularInh() {
                return circularInh;
            }

            public int getInhNull() {
                return inhNull;
            }

            public int getOverridePrivate() {
                return overridePrivate;
            }

            public int getAmbiguousInh() {
                return ambiguousInh;
            }

            public int getInvalidRet() {
                return invalidRet;
            }

            public int getAmbiguousCall() {
                return ambiguousCall;
            }

            public int getAmbiguousUse() {
                return ambiguousUse;
            }

            public int getClassDup() {
                return classDup;
            }

            public int getIfExpr() {
                return ifExpr;
            }

            public int getTemplates() {
                return templates;
            }

            public int getInstAbstract() {
                return instAbstract;
            }

            public int getInstMe() {
                return instMe;
            }

            public int getInstGeneric() {
                return instGeneric;
            }

            public int getInvalidError() {
                return invalidError;
            }

            public int getInvalidReturn() {
                return invalidReturn;
            }

            public int getMethodDup() {
                return methodDup;
            }
        }
        
    }
    
}