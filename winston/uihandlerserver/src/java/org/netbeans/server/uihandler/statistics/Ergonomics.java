/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * Copyright 2007 Sun Microsystems, Inc. All rights reserved.
 */

package org.netbeans.server.uihandler.statistics;

import java.util.ArrayList;
import java.util.Collection;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;
import org.netbeans.server.uihandler.*;
import java.util.Collections;
import java.util.List;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import javax.servlet.jsp.PageContext;

/** Collects data about use behaviour with respect to Ergonomics
 * features of the IDE.
 *
 * @author Jaroslav Tulach
 */
//@ServiceProvider(service=Statistics.class)
public class Ergonomics extends Statistics<Collection<Ergonomics.Matrix>> {
    static final Logger LOG = Logger.getLogger(Ergonomics.class.getName());
    
    public Ergonomics() {
        super(1);
    }
    
    protected Collection<Matrix> newData() {
        return Collections.emptySet();
    }

    protected Collection<Matrix> process(LogRecord rec) {
        if (rec.getMessage() != null && rec.getMessage().startsWith("ERGO_")) {
            return Collections.singleton(new Matrix(rec));
        }
        return Collections.emptySet();
    }

    protected Collection<Matrix> join(Collection<Matrix> one, Collection<Matrix> two) {
        if (one.isEmpty()) {
            return two;
        }
        if (two.isEmpty()) {
            return one;
        }

        if (two.size() == 1) {
            Matrix m1 = lastItem(one);
            Matrix m2 = two.iterator().next();

            if (m1.isOpen() && m2.isClosing()) {
                Matrix join = new Matrix(m1.reason, m1.cluster, m2.accepted);
                ArrayList<Matrix> m = new ArrayList<Matrix>();
                m.addAll(one);
                m.remove(m.size() - 1);
                m.add(join);
                return m;
            }
            if (m2.isClosing()) {
                return one;
            }
        }

        ArrayList<Matrix> m = new ArrayList<Matrix>();
        m.addAll(one);
        m.addAll(two);
        return m;
    }
    private static Matrix lastItem(Collection<Matrix> c) {
        if (c instanceof List) {
            List<?> l = (List<?>)c;
            Object last = l.get(l.size() - 1);
            return (Matrix)last;
        } else {
            if (c.size() > 1) {
                throw new IllegalStateException("" + c);
            }
            return c.iterator().next();
        }
    }
    @Override
    protected void registerPageContext(PageContext page, String name, Collection<Matrix> data) {
        int minimum = 0;
        String countTxt = (String)page.getAttribute("minimum", PageContext.REQUEST_SCOPE); // NOI18N
        if (countTxt != null) {
            minimum = Integer.parseInt(countTxt);
        }
        
        String excludes = (String)page.getAttribute("excludes", PageContext.REQUEST_SCOPE); // NOI18N

        page.setAttribute(
            name, // NOI18N
            data
        );
    }
    protected void write(Preferences pref, Collection<Matrix> d) {
        if (d == null) {
            throw new NullPointerException();
        }
        for (Matrix m : d) {
            if (m.accepted != null) {
                Preferences node = pref.node(m.cluster).node(m.reason);
                int prev = node.getInt(m.accepted.toString(), 0);
                node.putInt(m.accepted.toString(), prev + 1);
            }
        }
    }

    protected Collection<Matrix> read(Preferences pref) throws BackingStoreException {
        ArrayList<Matrix> arr = new ArrayList<Matrix>();
        for (String cluster : pref.childrenNames()) {
            Preferences clusterNode = pref.node(cluster);
            for (String reason : clusterNode.childrenNames()) {
                Preferences reasonNode = clusterNode.node(reason);
                int countTrue = reasonNode.getInt(Boolean.TRUE.toString(), 0);
                int countFalse = reasonNode.getInt(Boolean.FALSE.toString(), 0);

                if (countTrue > 0) {
                    arr.add(new Matrix(reason, cluster, Boolean.TRUE, countTrue));
                }
                if (countFalse > 0) {
                    arr.add(new Matrix(reason, cluster, Boolean.FALSE, countFalse));
                }
            }
        }
        return arr;
    }

    @Override
    protected Collection<Matrix> finishSessionUpload(String userId, int sessionNumber, boolean initialParse, Collection<Matrix> d) {
        return d;
    }
    
    public static final class Matrix {
        final String reason;
        final String cluster;
        final Boolean accepted;
        final int count;
        
        Matrix(LogRecord rec) {
            if ("ERGO_PROJECT_OPEN".equals(rec.getMessage())) { // NOI18N
                reason = "Project Open"; // NOI18N
                cluster = (String)rec.getParameters()[0];
                accepted = true;
                count = 1;
                return;
            }
            if ("ERGO_QUESTION".equals(rec.getMessage())) { // NOI18N
                reason = (String)rec.getParameters()[1];
                cluster = (String)rec.getParameters()[0];
                accepted = null;
                count = 0;
                return;
            }
            if ("ERGO_DOWNLOAD".equals(rec.getMessage())) { // NOI18N
                reason = null;
                cluster = null;
                accepted = true;
                count = 1;
                return;
            }
            if ("ERGO_CLOSE".equals(rec.getMessage())) { // NOI18N
                reason = null;
                cluster = null;
                accepted = false;
                count = 1;
                return;
            }

            reason = null;
            cluster = null;
            accepted = null;
            count = 0;
        }

        private Matrix(String reason, String cluster, Boolean accepted) {
            this(reason, cluster, accepted, 1);
        }

        private Matrix(String reason, String cluster, Boolean accepted, int count) {
            this.reason = reason;
            this.cluster = cluster;
            this.accepted = accepted;
            this.count = count;
        }

        boolean isOpen() {
            return reason != null && cluster != null && accepted == null;
        }

        boolean isClosing() {
            return reason == null && cluster == null && accepted != null;
        }

        boolean isFinished() {
            return reason != null && cluster != null && accepted != null;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final Matrix other = (Matrix) obj;
            if ((this.reason == null) ? (other.reason != null) : !this.reason.equals(other.reason)) {
                return false;
            }
            if ((this.cluster == null) ? (other.cluster != null) : !this.cluster.equals(other.cluster)) {
                return false;
            }
            return true;
        }

        public boolean getAccepted() {
            return accepted == null ? false : accepted.booleanValue();
        }

        public String getCluster() {
            return cluster;
        }

        public int getCount() {
            return count;
        }

        public String getReason() {
            return reason;
        }

        @Override
        public int hashCode() {
            int hash = 7;
            hash = 37 * hash + (this.reason != null ? this.reason.hashCode() : 0);
            hash = 37 * hash + (this.cluster != null ? this.cluster.hashCode() : 0);
            hash = 37 * hash + (this.accepted != null ? this.accepted.hashCode() : 0);
            return hash;
        }

        @Override
        public String toString() {
            return this.reason + ":" + this.cluster + "[" + this.accepted + "," + this.count + "]";
        }
        
    } // end of Matrix
}

