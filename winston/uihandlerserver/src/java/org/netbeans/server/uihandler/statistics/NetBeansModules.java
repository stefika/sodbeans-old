/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2007 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */
package org.netbeans.server.uihandler.statistics;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;
import org.netbeans.server.uihandler.Statistics;

/**
 *
 * @author Jindrich Sedek
 */
//@ServiceProvider(service=Statistics.class)
final public class NetBeansModules extends Statistics<NetBeansModules.NBMData> {

    private boolean rememberDistinctVersions = false;
    private static final String ENABLED_MESSAGE = "UI_ENABLED_MODULES";
    private static final String DISABLED_MESSAGE = "UI_DISABLED_MODULES";
    private static final String ENABLED = "EN";
    private static final String DISABLED = "DIS";

    public NetBeansModules() {
        super("NetBeansModules");
    }

    public void setRememberDistinctVersions(boolean b) {
        rememberDistinctVersions = b;
    }

    public class NBMData implements Comparator<String> {

        Map<String, Integer> allEnabled;
        Map<String, Integer> allDisabled;
        Map<String, ModuleInfo> enabled;
        Map<String, ModuleInfo> disabled;

        public ModuleInfo getEnabled(String moduleName) {
            assert rememberDistinctVersions;
            return enabled.get(moduleName);
        }

        private Set<String> getModuleNames() {
            Set<String> result = new HashSet<String>(allEnabled.keySet());
            result.addAll(allDisabled.keySet());
            return result;
        }

        public String getSorted(String param) {
            int parameter = 0;
            List<String> list = new ArrayList<String>(getModuleNames());
            try {
                if (param != null) {
                    parameter = Integer.parseInt(param);
                }
            } catch (Exception exc) {
                LogRecord log = new LogRecord(Level.INFO, "wrong parameter");
                Object[] params = new Object[1];
                params[0] = param;
                log.setParameters(params);
                log.setThrown(exc);
                Logger.getLogger(NetBeansModules.class.getName()).info("wrong parameter");
            }
            if (parameter == 0) {
                Collections.sort(list);
            } else if (parameter == 1) {
                Collections.sort(list, this);
            } else if (parameter == 2) {
                Collections.sort(list, new DisabledComparator());
            }
            return print(list).toString();
        }

        private StringBuffer print(Collection<String> moduleNames) {
            StringBuffer buffer = new StringBuffer(100 * moduleNames.size() + 30);
            for (String str : moduleNames) {
                buffer.append("\t<tr>");
                buffer.append("<td class='tbltd1'>");
                buffer.append(str);
                buffer.append("</td>");
                buffer.append("<td class='tbltd1'>");
                if (allEnabled.get(str) != null) {
                    buffer.append(allEnabled.get(str));
                } else {
                    buffer.append(0);
                }
                buffer.append("</td>");
                buffer.append("<td class='tbltd1'>");
                if (allDisabled.get(str) != null) {
                    buffer.append(allDisabled.get(str));
                } else {
                    buffer.append(0);
                }
                buffer.append("</td>");
                buffer.append("</tr>\n");
            }
            return buffer;
        }

        private class DisabledComparator implements Comparator<String> {

            public int compare(String o1, String o2) {
                Integer first = value(allDisabled, o1);
                Integer second = value(allDisabled, o2);
                return first.compareTo(second);
            }
        }

        private Integer value(Map<String, Integer> map, String key) {
            Integer result = map.get(key);
            if (result != null) {
                return result;
            }
            return 0;
        }

        public int compare(String o1, String o2) {
                Integer first = value(allEnabled, o1);
                Integer second = value(allEnabled, o2);
                return first.compareTo(second);
        }
    }

    public static class Version {

        Integer[] data;

        public Version(Integer[] version) {
            data = version;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj instanceof Version) {
                Version ver = (Version) obj;
                return comp(ver.data, this.data);
            } else if (obj instanceof Integer[]) {
                Integer[] ver = (Integer[]) obj;
                return comp(ver, this.data);
            } else {
                return false;
            }
        }

        @Override
        public int hashCode() {
            int result = 0;
            for (int i = 0; i < data.length; i++) {
                result += data[i];
                result <<= 4; // * 16
            }
            return result;
        }

        private boolean comp(Integer[] ver, Integer[] data) {
            if (ver.length != data.length) {
                return false;
            }
            for (int i = 0; i < data.length; i++) {
                if (ver[i] != data[i]) {
                    return false;
                }
            }
            return true;
        }
    }

    public class ModuleInfo {

        private Map<Version, Integer> info;// version -> count

        public ModuleInfo() {//no versions
        }

        public ModuleInfo(ModuleInfo old) {
            if (old.info != null) {
                info = new HashMap<Version, Integer>();
                info.putAll(old.info);
            }
        }

        public ModuleInfo(Integer[] version) {
            this(version, 1);
        }

        public ModuleInfo(Integer[] version, Integer count) {
            if (rememberDistinctVersions) {
                info = new HashMap<Version, Integer>();
                info.put(new Version(version), count);
            }
        }

        private void summarize(Version version, Integer count) {
            if (!info.containsKey(version)) {
                info.put(version, count);
            } else {
                info.put(version, info.get(version) + count);
            }
        }

        public void merge(ModuleInfo merged) {
            if (merged == null) {
                return;
            }
            if ((this.info == null) && (merged.info != null)) {
                this.info = new HashMap<Version, Integer>(merged.info);
            } else if (merged.info != null) {
                for (Entry<Version, Integer> entry : merged.info.entrySet()) {
                    summarize(entry.getKey(), entry.getValue());
                }
            }
        //parameter has null info => nothing to merge
        }

        public Integer count(Integer[] version) {
            return info.get(new Version(version));
        }
    }

    public NBMData process(LogRecord rec) {
        if ((rec.getLevel() != Level.CONFIG) || (rec.getParameters() == null) || (rec.getParameters().length == 0)) {
            return null;
        } else if (rec.getMessage().equals(ENABLED_MESSAGE)) {
            NBMData data = new NBMData();
            data.allEnabled = new HashMap<String, Integer>(rec.getParameters().length);
            data.enabled = getModules(rec.getParameters(), data.allEnabled);
            return data;
        } else if (rec.getMessage().equals(DISABLED_MESSAGE)) {
            NBMData data = new NBMData();
            data.allDisabled = new HashMap<String, Integer>(rec.getParameters().length);
            data.disabled = getModules(rec.getParameters(), data.allDisabled);
            return data;
        }
        return null;
    }

    public Map<String, ModuleInfo> getModules(Object[] parameters, Map<String, Integer> all) {
        Map<String, ModuleInfo> miMap = null;
        if (rememberDistinctVersions) {
            miMap = new HashMap<String, ModuleInfo>(parameters.length);
        }
        for (Object object : parameters) {
            String parameter = (String) object;
            int index = parameter.indexOf('[');
            String moduleName = parameter;
            if (index != -1) {
                moduleName = parameter.substring(0, index);
            }
            moduleName = normalizeModuleName(moduleName.trim());
            all.put(moduleName, 1);
            if (rememberDistinctVersions) {
                Integer[] version = null;
                if (index != -1) {
                    version = getModuleVersion(parameter);
                }
                if (version != null) {
                    miMap.put(moduleName, new ModuleInfo(version));
                } else {
                    miMap.put(moduleName, new ModuleInfo());
                }
            }
        }
        return miMap;
    }

    public static String normalizeModuleName(String moduleName) {
        int index = moduleName.indexOf('/');
        if (index != -1) {
            moduleName = moduleName.substring(0, index);
        }
        return moduleName;
    }

    public static Integer[] getModuleVersion(String moduleNameLine) {
        String version = null;
        int index = moduleNameLine.indexOf('[');
        int index2 = moduleNameLine.indexOf(']');
        if (index2 == -1) {
            index2 = moduleNameLine.length();
        }
        version = moduleNameLine.substring(index + 1, index2).trim();
        String[] strs = version.split("\\.");
        Integer[] digits = new Integer[strs.length];
        int i = 0;
        for (String s : strs) {
            try {
                digits[i++] = Integer.parseInt(s);
            } catch (NumberFormatException e) {
                System.err.println(moduleNameLine);
                throw e;
            }
        }
        return digits;
    }

    @Override
    protected NBMData newData() {
        return null;
    }

    @Override
    protected NBMData finishSessionUpload(String userId, int sessionNumber, boolean initialParse, NBMData d) {
        return d;
    }

    @Override
    public NBMData join(NBMData one, NBMData two) {
        if (one == null) {
            return two;
        } else if (two == null) {
            return one;
        }
        // merge data
        NBMData result = new NBMData();
        result.enabled = merge(one.enabled, two.enabled);
        result.disabled = merge(one.disabled, two.disabled);
        result.allEnabled = mergeInts(one.allEnabled, two.allEnabled);
        result.allDisabled = mergeInts(one.allDisabled, two.allDisabled);
        return result;
    }

    private Map<String, Integer> mergeInts(Map<String, Integer> one, Map<String, Integer> two) {
        if (one == null) {
            return two;
        } else if (two == null) {
            return one;
        }
        Map<String, Integer> result = new HashMap<String, Integer>(one);
        Iterator<Entry<String, Integer>> it = two.entrySet().iterator();
        while (it.hasNext()) {
            Entry<String, Integer> entry = it.next();
            if (result.containsKey(entry.getKey())) {
                Integer newValue = result.get(entry.getKey()) + entry.getValue();
                result.put(entry.getKey(), newValue);
            } else {
                result.put(entry.getKey(), entry.getValue());
            }
        }
        return result;
    }

    private Map<String, ModuleInfo> merge(Map<String, ModuleInfo> one, Map<String, ModuleInfo> two) {
        if (one == null) {
            return two;
        } else if (two == null) {
            return one;
        }
        Map<String, ModuleInfo> result = new HashMap<String, NetBeansModules.ModuleInfo>(one);
        Iterator<Entry<String, ModuleInfo>> it = two.entrySet().iterator();
        while (it.hasNext()) {
            Entry<String, ModuleInfo> entry = it.next();
            if (result.containsKey(entry.getKey())) {
                ModuleInfo entryVal = entry.getValue();
                ModuleInfo newInfo = null;
                if (entryVal != null) {
                    newInfo = new ModuleInfo(entryVal);
                    newInfo.merge(result.get(entry.getKey()));
                }
                result.put(entry.getKey(), newInfo);
            } else {
                result.put(entry.getKey(), entry.getValue());
            }
        }
        return result;
    }

    @Override
    protected void write(Preferences pref, NBMData d) throws BackingStoreException {
        if (d != null) {
            if (d.allEnabled != null) {
                writeMap(pref.node(ENABLED), d.allEnabled);
            }
            if (d.allDisabled != null) {
                writeMap(pref.node(DISABLED), d.allDisabled);
            }
        }
    }

    @Override
    protected NBMData read(Preferences pref) throws BackingStoreException {
        NBMData result = new NBMData();
        result.allEnabled = readMap(pref.node(ENABLED));
        result.allDisabled = readMap(pref.node(DISABLED));
        return result;
    }

    private Map<String, Integer> readMap(Preferences node) throws BackingStoreException {
        Map<String, Integer> map = new HashMap<String, Integer>(node.keys().length);
        for (String key : node.keys()) {
            Integer count = node.getInt(key, 0);
            map.put(key, count);
        }
        return map;
    }
}
