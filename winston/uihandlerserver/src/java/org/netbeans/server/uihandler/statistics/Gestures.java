/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * Copyright 2007 Sun Microsystems, Inc. All rights reserved.
 */

package org.netbeans.server.uihandler.statistics;

import java.util.prefs.Preferences;
import org.netbeans.server.uihandler.*;
import java.util.Collections;
import java.util.EnumMap;
import java.util.Map;
import java.util.logging.LogRecord;
import org.netbeans.lib.uihandler.InputGesture;
import org.openide.util.lookup.ServiceProvider;

/** Counts numbers of keyboard, toolbar and menu actions.
 *
 * @author Jaroslav Tulach
 */
@ServiceProvider(service=Statistics.class)
public final class Gestures extends Statistics<Map<InputGesture,Integer>> {
    public Gestures() {
        super("Gestures");
    }

    protected Map<InputGesture, Integer> newData() {
        return Collections.emptyMap();
    }

    protected Map<InputGesture, Integer> process(LogRecord rec) {
        InputGesture ig = InputGesture.valueOf(rec);
        if (ig == null) {
            return Collections.emptyMap();
        } else {
            return Collections.singletonMap(ig, 1);
        }
    }

    protected Map<InputGesture, Integer> join(
        Map<InputGesture, Integer> one,
        Map<InputGesture, Integer> two
    ) {
        Map<InputGesture, Integer> counts = new EnumMap<InputGesture,Integer>(InputGesture.class);
        for (InputGesture ig : InputGesture.values()) {
            Integer i1 = one.get(ig);
            Integer i2 = two.get(ig);
            int int1 = i1 == null ? 0 : i1.intValue();
            int int2 = i2 == null ? 0 : i2.intValue();
            if (int1 + int2 > 0) {
                counts.put(ig, int1 + int2);
            }
        }
        return counts;
    }

    protected Map<InputGesture, Integer> finishSessionUpload(
        String userId,
        int sessionNumber,
        boolean initialParse,
        Map<InputGesture, Integer> data
    ) {
        return data;
    }

    protected void write(Preferences pref, Map<InputGesture, Integer> d) {
        for (Map.Entry<InputGesture, Integer> entry : d.entrySet()) {
            pref.putInt(entry.getKey().name(), entry.getValue());
        }
    }

    protected Map<InputGesture, Integer> read(Preferences pref) {
        Map<InputGesture, Integer> counts = new EnumMap<InputGesture,Integer>(InputGesture.class);
        for (InputGesture ig : InputGesture.values()) {
            int cnt = pref.getInt(ig.name(), 0);
            if (cnt > 0) {
                counts.put(ig, cnt);
            }
        }
        return counts;
    }
}
