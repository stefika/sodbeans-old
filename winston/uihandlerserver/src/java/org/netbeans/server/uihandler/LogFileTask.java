/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */
package org.netbeans.server.uihandler;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import org.netbeans.lib.uihandler.LogRecords;
import org.netbeans.modules.exceptions.entity.Logfile;
import org.netbeans.modules.exceptions.entity.Nbversion;
import org.netbeans.modules.exceptions.entity.ProductVersion;
import org.netbeans.modules.exceptions.utils.PersistenceUtils;
import org.netbeans.server.uihandler.LogsManager.Value;
import org.netbeans.server.uihandler.LogsManager.VersionData;
import org.openide.util.Task;

/**
 *
 * @author Jindrich Sedek
 */
public class LogFileTask extends Task {

    private static final Logger LOG = Logger.getLogger(LogFileTask.class.getName());
    protected static LogFileTask runningTask;
    static boolean closed = false;
    private File f;
    private String remoteHost;
    private StatisticsGetter statisticsGetter;
    private EntityManager taskEm;
    private Logfile logFile;
    private ExceptionsData resultData;

    LogFileTask(File f, String remoteHost, StatisticsGetter statistics) {
        this.f = f;
        this.remoteHost = remoteHost;
        this.statisticsGetter = statistics;
    }

    EntityManager getTaskEntityManager() {
        return taskEm;
    }

    public Logfile getLogInfo() {
        return logFile;
    }

    public static LogFileTask getRunningTask() {
        return runningTask;
    }

    @Override
    public void run() {
        notifyRunning();
        if (closed) {
            LOG.info("Processing cancelled");
            notifyFinished();
            return;
        }
        try {
            runningTask = this;
            long time = System.currentTimeMillis();
            taskEm = PersistenceUtils.getInstance().createEntityManager();
            taskEm.getTransaction().begin();

            LOG.fine("processing parsing " + f);
            logFile = new Logfile(getIdSes(f.getName()), getSessionNumber(f.getName()));
            logFile.setIpAddr(remoteHost);
            taskEm.persist(logFile);
            LOG.fine("IP address stored as " + remoteHost + " for " + f);
            handleAddNewLog(f, statisticsGetter.getStatistics(), true, logFile, taskEm);

            taskEm.getTransaction().commit();
            time = System.currentTimeMillis() - time;
            LOG.info("done processing parsing in " + time + "ms");
        } catch (Throwable t) {
            t.printStackTrace();
            LOG.log(Level.SEVERE, "failure during parsing " + f + " from " + remoteHost, t);
            if (taskEm.getTransaction().isActive()){
                taskEm.getTransaction().rollback();
            }
            if (t instanceof ThreadDeath) {
                throw (ThreadDeath) t;
            }
        } finally {
            notifyFinished();
            taskEm.clear();
            taskEm.close();
            taskEm = null;
            statisticsGetter = null;
        }
    }

    ExceptionsData getExceptionsData() {
        return resultData;
    }

    void setExceptionsData(ExceptionsData data) {
        this.resultData = data;
    }

    void handleAddNewLog(
            File f,
            List<Value> statistics,
            boolean initialParse,
            Logfile logInfo,
            EntityManager em) {
        String idSes = getIdSes(f.getName());
        int ses = getSessionNumber(f.getName());

        LOG.log(Level.FINE, "handleAddNewLog for {0}", f);

        SessionInfo info = new SessionInfo(idSes, ses, f, logInfo, statistics);

        for (Value<?> v : info.getValues()) {
            finishUploadOpen(info, v, initialParse);
        }

        LOG.log(Level.FINE, "Persisting data for {0}", f);

        info.persist(em);

        LOG.log(Level.FINE, "Finished processing {0}", f);

        if (initialParse) { // add statistics data to globals

            info.addValues(statistics);

            ProductVersion  pVersion = logInfo.getProductVersionId();
            if (pVersion == null){
                return;
            }
            Nbversion version = pVersion.getNbversionId();
            VersionData versionData = LogsManager.getDefault().getVersionStatistics(version);
            if (versionData != null){
                info.addValues(versionData.getData());
                versionData.addLog();
            }
        }
    }

    private static <Data> void finishUploadOpen(SessionInfo info, Value<Data> v, boolean initialParse) {
        v.value = v.statistics.finishSessionUpload(info.id, info.session, initialParse, v.value);
    }

    public static String getIdSes(String fileName) {
        String[] idSes = fileName.split("\\.");
        return idSes[0];
    }

    /** Getter for info about log.
     * @param log name of the log
     * @return information about the log file
     */
    public static Logfile getLogInfo(String log, EntityManager em) {
        String userdir = getIdSes(log);
        int uploadnumber = getSessionNumber(log);
        return getLogInfo(userdir, uploadnumber, em);
    }

    /** Getter for info about log.
     * @param userdir userdir id
     * @param uploadnumber uploaded log number from this userdir id
     * @return information about the log file
     */
    public static Logfile getLogInfo(String userdir, int uploadnumber, EntityManager em) {
        Map<String, Object> params = new HashMap<String, Object>(3);
        params.put("userdir", userdir);
        params.put("uploadnumber", uploadnumber);
        return (Logfile) PersistenceUtils.executeNamedQuerySingleResult(em, "Logfile.findByFile", params);
    }

    public static int getSessionNumber(String fileName) {
        String[] idSes = fileName.split("\\.");
        /* ses == 0 - a file on disk without suffix like: NB2123128636
        ses != 0 - a file on disk without suffix like: NB2123128636.x
         * where ses = x+1
         */
        return idSes.length == 1 ? 0 : Integer.parseInt(idSes[1]) + 1;
    }

    static final class SessionInfo {

        private final String id;
        private final int session;
        private final File log;
        private final Logfile logfile;
        private volatile List<Value> values;
        private volatile boolean computed;

        SessionInfo(String id, int session, File log, Logfile logfile, Collection<Value> stats) {
            this.log = log;
            this.id = id;
            this.session = session;
            this.logfile = logfile;
            this.values = new ArrayList<Value>(stats.size());
            for (Value<?> v : stats) {
                LogsManager.addStatisticsData(this.values, v.statistics);
            }
        }

        public List<Value> getValues() {
            if (computed) {
                return values;
            }

            class H extends Handler {

                public void publish(LogRecord rec) {
                    try{
                        for (LogsManager.Value<?> value : values) {
                            addRecord(value, rec);
                        }
                    }catch(RuntimeException exc){
                        LOG.log(Level.SEVERE, "Record processing failed", exc);
                        throw exc;
                    }
                }

                private <Data> void addRecord(Value<Data> value, LogRecord rec) {
                    Data d = value.statistics.process(rec);
                    value.value = value.statistics.join(value.value, d);
                }

                public void flush() {
                }

                public void close() throws SecurityException {
                }
            }
            H h = new H();

            InputStream is = null;
            try {
                is = new BufferedInputStream(new FileInputStream(log));
                LogRecords.scan(is, h);
            } catch (IOException ex) {
                LOG.log(Level.SEVERE, "Cannot read records: " + log, ex);
                if (ex.getCause() != null){
                    Throwable cause = ex.getCause();
                    if ((cause instanceof InternalError) && "processing event: -1".equals(cause.getMessage())){
                        // the file is not parsable -> let's delete it
                        log.delete();
                    }
                }
                log.delete();
            } finally {
                if (is != null) {
                    try {
                        is.close();
                    } catch (IOException ex) {
                        LOG.log(Level.INFO, "Cannot close stream: " + log, ex);
                    }
                }
            }

            computed = true;

            return values;
        }

        @SuppressWarnings("unchecked")
        final void addValues(List<Value> sum) {
            assert sum.size() == getValues().size();

            for (int i = 0; i < sum.size(); i++) {
                Value ses = getValues().get(i);
                Value user = sum.get(i);
                assert ses.statistics == user.statistics;

                user.value = user.statistics.join(user.value, ses.value);
            }

        }

        public void persist(EntityManager em) {
            getValues();
            for (Value<?> value : values) {
                value.persist(logfile, em);
            }

        }
    } // end of SessionInfo

    static interface StatisticsGetter{
        List<Value> getStatistics();
    }
}
