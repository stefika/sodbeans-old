/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2009 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2009 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */
package org.netbeans.server.uihandler.statistics;

import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.LogRecord;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;
import javax.servlet.jsp.PageContext;
import org.netbeans.server.uihandler.Statistics;
import org.netbeans.server.uihandler.statistics.SnapshotStatistics.SnapshotData;
import org.openide.util.lookup.ServiceProvider;

/**
 *
 * @author Jindrich Sedek
 */
@ServiceProvider(service=Statistics.class)
public class SnapshotStatistics extends Statistics<SnapshotData> {

    private static final String MESSAGE = "Snapshot statistics";
    private static final String SAMPLES = "SAMPLES";
    private static final String AVG = "AVG";
    private static final String MAXIMUM = "MAXIMUM";
    private static final String MINIMUM = "MINIMUM";
    private static final String STDDEV = "STDDEV";

    public SnapshotStatistics() {
        super("SnapshotStatistics", 1);
    }

    @Override
    protected SnapshotData newData() {
        return null;
    }

    @Override
    protected SnapshotData process(LogRecord rec) {
        if (MESSAGE.equals(rec.getMessage()) && (rec.getParameters() != null) && (rec.getParameters().length > 10)) {
            return new SnapshotData(rec);
        }
        return null;
    }

    @Override
    protected SnapshotData finishSessionUpload(String userId, int sessionNumber, boolean initialParse, SnapshotData d) {
        if (d == null) {
            return d;
        }
        return d.makeSingle();
    }

    @Override
    protected SnapshotData join(SnapshotData one, SnapshotData two) {
        if (one == null){
            return two;
        }else if (two == null){
            return one;
        }
        SnapshotData result = new SnapshotData(one);
        addAllCounts(result.samples, two.samples);
        addAllCounts(result.avg, two.avg);
        addAllCounts(result.minimum, two.minimum);
        addAllCounts(result.maximum, two.maximum);
        addAllCounts(result.stddev, two.stddev);
        return result;
    }

    @Override
    protected void write(Preferences pref, SnapshotData d) throws BackingStoreException {
        if (d == null){
            return;
        }
        writeMap(pref.node(SAMPLES), d.samples);
        writeMap(pref.node(AVG), d.avg);
        writeMap(pref.node(MINIMUM), d.minimum);
        writeMap(pref.node(MAXIMUM), d.maximum);
        writeMap(pref.node(STDDEV), d.stddev);
    }

    @Override
    public SnapshotData read(Preferences pref) throws BackingStoreException {
        return new SnapshotData(
                readCounts(pref.node(SAMPLES)),
                readCounts(pref.node(AVG)),
                readCounts(pref.node(MINIMUM)),
                readCounts(pref.node(MAXIMUM)),
                readCounts(pref.node(STDDEV)));
    }

    @Override
    protected void registerPageContext(PageContext page, String name, SnapshotData data) {
        if (data == null){
            return;
        }
        page.setAttribute(name + SAMPLES, data.samples);
        page.setAttribute(name + AVG, data.avg);
        page.setAttribute(name + MINIMUM, data.minimum);
        page.setAttribute(name + MAXIMUM, data.maximum);
        page.setAttribute(name + STDDEV, data.stddev);
    }

    public static class SnapshotData {
        private final Map<Integer, Integer> samples;
        private final Map<Integer, Integer> avg;
        private final Map<Integer, Integer> minimum;
        private final Map<Integer, Integer> maximum;
        private final Map<Integer, Integer> stddev;

        public SnapshotData(Map<Integer, Integer> samples, 
                Map<Integer, Integer> avg,
                Map<Integer, Integer> minimum,
                Map<Integer, Integer> maximum,
                Map<Integer, Integer> stddev) {
            this.samples = samples;
            this.avg = avg;
            this.maximum = maximum;
            this.minimum = minimum;
            this.stddev = stddev;
        }

        private SnapshotData(LogRecord rec) {
            Object[] params = rec.getParameters();
            samples = getSingltonMap(Integer.parseInt(params[2].toString()));
            avg = getSingltonMap(getDecimal(params, 4, 1));
            minimum = getSingltonMap(getDecimal(params, 6, 1));
            maximum = getSingltonMap(getDecimal(params, 8, 1));
            stddev = getSingltonMap(getDecimal(params, 10, 1));
        }

        private SnapshotData(SnapshotData one) {
            this.samples = new LinkedHashMap<Integer, Integer>(one.samples);
            this.avg = new LinkedHashMap<Integer, Integer>(one.avg);
            this.minimum = new LinkedHashMap<Integer, Integer>(one.minimum);
            this.maximum = new LinkedHashMap<Integer, Integer>(one.maximum);
            this.stddev = new LinkedHashMap<Integer, Integer>(one.stddev);
        }

        private Map<Integer, Integer> getSingltonMap(int key){
            return Collections.singletonMap(key, 1);
        }
        private int getDecimal(Object[] params, int index, int decimalNumbers){
            return (int) (Double.parseDouble(params[index].toString()) * Math.pow(10, decimalNumbers));
        }

        public Number getSingleSample(){
            return getSingleValue(samples, 0);
        }

        public Number getSingleAVG(){
            return getSingleValue(avg, 1);
        }

        public Number getSingleMinimum(){
            return getSingleValue(minimum, 1);
        }

        public Number getSingleMaximum(){
            return getSingleValue(maximum, 1);
        }

        public Number getSingleStdDev(){
            return getSingleValue(stddev, 1);
        }

        private Number getSingleValue(Map<Integer, Integer> samples, int decimalNumbers) {
            assert samples.size() == 1;
            Collection<Integer> values = samples.keySet();
            return values.iterator().next() / Math.pow(10, decimalNumbers);
        }

        public boolean isSingleData() {
            return samples.size() == 1l;
        }

        private SnapshotData makeSingle() {
            return new SnapshotData(
                    getSingltonMap(getLastKey(samples)),
                    getSingltonMap(getLastKey(avg)),
                    getSingltonMap(getLastKey(minimum)),
                    getSingltonMap(getLastKey(maximum)),
                    getSingltonMap(getLastKey(stddev))
                    );
        }

        public int getLastKey(Map<Integer, Integer> value){
            Iterator<Entry<Integer, Integer>> it = value.entrySet().iterator();
            Entry<Integer, Integer> lastEntry = null;
            while (it.hasNext()){
                lastEntry = it.next();
            }
            return lastEntry.getKey();
        }
    }
}
