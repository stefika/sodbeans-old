/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.netbeans.server.uihandler.statistics;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.LogRecord;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;
import javax.servlet.jsp.PageContext;
import org.netbeans.server.uihandler.Statistics;
import org.netbeans.server.uihandler.statistics.SodbeansOutputStatistics.DataBean;
import org.openide.util.lookup.ServiceProvider;

/**
 *
 * @author melissa
 */
@ServiceProvider(service = Statistics.class)
public class SodbeansOutputStatistics extends Statistics<DataBean> {

    private static final DataBean EMPTY = new DataBean(0, 0, 0, 0);
    public static final String STATISTIC_NAME = "SodbeansOutputStatistics";

    public SodbeansOutputStatistics() {
        super(STATISTIC_NAME);
    }

    @Override
    protected DataBean newData() {
        return EMPTY;
    }

    @Override
    protected DataBean process(LogRecord rec) {
        if ("SODBEANS_OUTPUT_SELECTED".equals(rec.getMessage())) {
            String id = (String)rec.getParameters()[0];
            int errors = Integer.parseInt(id);
            if(errors > 0){
                return new DataBean(1, 0, 0, 1);
            }else{
                return new DataBean(1, 0, 0, 0);
            }
        } else {
            return EMPTY;
        }
    }

    @Override
    protected DataBean finishSessionUpload(String userId, int sessionNumber, boolean initialParse, DataBean d) {
        int nonNullSessions = 0;
        if (d.getActionsCount() > 0) {
            nonNullSessions = 1;
        }
        return new DataBean(d.getActionsCount(), 1, nonNullSessions, d.getNumberOfActionsWithErrors());
    }

    @Override
    protected DataBean join(DataBean one, DataBean two) {
        return new DataBean(one.getActionsCount() + two.getActionsCount(),
                one.getNumberOfSessions() + two.getNumberOfSessions(),
                one.getNumberOfNonNullSessions() + two.getNumberOfNonNullSessions(),
                one.getNumberOfActionsWithErrors() + two.getNumberOfActionsWithErrors());
    }

    @Override
    protected void write(Preferences pref, DataBean d) throws BackingStoreException {
        pref.putInt("all", d.getActionsCount());
        pref.putInt("sessions", d.getNumberOfSessions());
        pref.putInt("non_null_sessions", d.getNumberOfNonNullSessions());
        pref.putInt("compiler_errors", d.getNumberOfActionsWithErrors());
    }

    @Override
    protected DataBean read(Preferences pref) throws BackingStoreException {
        return new DataBean(pref.getInt("all", 0), pref.getInt("sessions", 0), pref.getInt("non_null_sessions", 0), pref.getInt("compiler_errors", 0));
    }

    @Override
    protected void registerPageContext(PageContext page, String name, DataBean data) {
        page.setAttribute(name + "Usages", data.getUsages());
        page.setAttribute(name + "Avg", data.getAvgData());
    }

    public static final class DataBean {

        private final int actionsCount;
        private final int numberOfSessions;
        private final int numberOfNonNullSessions;
        private final int numberOfActionsWithErrors;

        public DataBean(int actionsCount, int numberOfSessions, int numberOfNonNullSessions, int compilerErrors) {
            this.actionsCount = actionsCount;
            this.numberOfSessions = numberOfSessions;
            this.numberOfNonNullSessions = numberOfNonNullSessions;
            this.numberOfActionsWithErrors = compilerErrors;
        }

        public int getActionsCount() {
            return actionsCount;
        }

        public int getNumberOfSessions() {
            return numberOfSessions;
        }

        public int getNumberOfNonNullSessions() {
            return numberOfNonNullSessions;
        }
        
        public int getNumberOfActionsWithErrors(){
            return numberOfActionsWithErrors;
        }

        public Map getUsages() {
            Map usages = new HashMap();
            usages.put("Using Sodbeans output", numberOfNonNullSessions);
            usages.put("Sessions not using the Sodbeans Output", numberOfSessions - numberOfNonNullSessions);
            return usages;
        }

        private Collection<ViewBean> getAvgData() {
            List<ViewBean> vb = new ArrayList<ViewBean>();
            vb.add(new ViewBean("Used for Compiler errors",  numberOfActionsWithErrors));
            vb.add(new ViewBean("Other", actionsCount - numberOfActionsWithErrors));
            return vb;
        }

        public static final class ViewBean {

            private final String name;
            private final Integer value;

            public ViewBean(String name, Integer value) {
                this.name = name;
                this.value = value;
            }

            public String getName() {
                return name;
            }

            public Integer getValue() {
                return value;
            }
        }
        
    }
    
}