/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2009 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */
package org.netbeans.server.uihandler.statistics;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.LogRecord;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;
import javax.servlet.jsp.PageContext;
import org.netbeans.server.uihandler.Statistics;
import org.netbeans.server.uihandler.statistics.CPUInfo.CPUInfoBean;
import org.openide.util.lookup.ServiceProvider;

/**
 *
 * @author Jindrich Sedek
 */
@ServiceProvider(service=Statistics.class)
public class CPUInfo extends Statistics<CPUInfoBean> {

    private static final CPUInfoBean EMPTY = new CPUInfoBean();
    private static final String MESSAGE = "CPU INFO";
    private static final String CPU_COUNT = "CPU_COUNT";

    public CPUInfo() {
        super("CPUInfo");
    }

    @Override
    protected CPUInfoBean newData() {
        return EMPTY;
    }

    @Override
    protected CPUInfoBean process(LogRecord rec) {
        if (MESSAGE.equals(rec.getMessage())) {
            Object[] params = rec.getParameters();
            if (params != null) {
                return new CPUInfoBean(params);
            }
        }
        return newData();
    }

    @Override
    protected CPUInfoBean finishSessionUpload(String userId, int sessionNumber, boolean initialParse, CPUInfoBean d) {
        return d;
    }

    @Override
    protected void registerPageContext(PageContext page, String name, CPUInfoBean data) {
        page.setAttribute(name, data.cpuCount);
    }

    @Override
    protected CPUInfoBean join(CPUInfoBean one, CPUInfoBean two) {
        if (EMPTY.equals(one)){
            return two;
        }
        if (EMPTY.equals(two)){
            return one;
        }
        CPUInfoBean result = new CPUInfoBean();
        result.cpuCount = new HashMap<Integer, Integer>(one.cpuCount);
        addAllCounts(result.cpuCount, two.cpuCount);

        return result;
    }

    @Override
    protected void write(Preferences pref, CPUInfoBean d) throws BackingStoreException {
        writeMap(pref.node(CPU_COUNT), d.cpuCount);
    }

    @Override
    public CPUInfoBean read(Preferences pref) throws BackingStoreException {
        CPUInfoBean bean = new CPUInfoBean();
        bean.cpuCount = readCounts(pref.node(CPU_COUNT));
        return bean;
    }

    public static class CPUInfoBean {

        Map<Integer, Integer> cpuCount;

        private CPUInfoBean() {
        }

        private CPUInfoBean(Object[] params) {
            int count = Integer.parseInt(params[0].toString());
            cpuCount = Collections.singletonMap(count, 1);
        }

        public Map<Integer, Integer> getCounts(){
            return cpuCount;
        }
    }
}
