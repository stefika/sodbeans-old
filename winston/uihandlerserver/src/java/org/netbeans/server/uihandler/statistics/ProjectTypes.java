/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * Copyright 2007 Sun Microsystems, Inc. All rights reserved.
 */

package org.netbeans.server.uihandler.statistics;

import java.util.ArrayList;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;
import javax.servlet.jsp.PageContext;
import org.netbeans.server.uihandler.*;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import org.netbeans.lib.uihandler.ProjectOp;
import org.netbeans.server.uihandler.statistics.ProjectTypes.Counts;
import org.openide.util.lookup.ServiceProvider;

/** Counts the number of used projecs and project types.
 *
 * @author Jaroslav Tulach
 */
@ServiceProvider(service=Statistics.class)
public class ProjectTypes extends Statistics<ProjectTypes.Counts> {
    static final Logger LOG = Logger.getLogger(ProjectTypes.class.getName());
    
    public ProjectTypes() {
        super("ProjectTypes", 1);
    }
    
    ProjectTypes(String n) {
        super(n);
    }
    
    protected Counts newData() {
        return Counts.EMPTY;
    }

    protected Counts process(LogRecord rec) {
        ProjectOp projectOp = ProjectOp.valueOf(rec);
        if (projectOp != null) {
            return new Counts(projectOp);
        } else {
            return Counts.EMPTY;
        }
    }

    protected Counts finishSessionUpload(String userId, int sessionNumber,
                                        boolean initialParse, Counts d) {
        if (d.counts.isEmpty()){
            d.logsCount = 0;
        }else{
            d.logsCount = 1;
            for (String typeName : d.counts.keySet()) {
                d.counts.put(typeName, 1);
            }
        }
        return d;
    }

    protected Counts join(Counts one, Counts two) {
        Counts join = new Counts();
        join.add(one);
        join.add(two);
        return join;
    }
    
    @Override
    protected void registerPageContext(PageContext page, String name, Counts data) {
        int minimal = 5;
        
        String countTxt = (String)page.getAttribute("minimal", PageContext.REQUEST_SCOPE); // NOI18N
        if (countTxt != null) {
            try {
                minimal = Integer.parseInt(countTxt);
            } catch (NumberFormatException ex) {
                LOG.log(Level.WARNING, "cannot parse" + countTxt, ex);
            }
        }
        if (minimal < 1) {
            minimal = 1;
        }
        if (minimal > 100) {
            minimal = 100;
        }
        
        int sum = 0;
        for (int i : data.counts.values()) {
            sum += i;
        }

        int others = 0;
        TreeMap<String,Object> percentages = new TreeMap<String,Object>();
        for (Map.Entry<String, Integer> entry : data.getUsages()) {
            if (entry.getValue() * 100 < sum * minimal) {
                others += entry.getValue();
            } else {
                int percent = entry.getValue() * 100 / sum;
                percentages.put(entry.getKey(), percent);
            }
        }
        
        if (others > 0) {
            int percent = others * 100 / sum;
            percentages.put("Others", percent);
        }
        
        if (!percentages.isEmpty()) {
            page.setAttribute(name, percentages);
        }else{
            return;
        }

        List<Item> itemsList = new ArrayList<Item>(data.getUsages().size());
        if (data.logsCount != 0){
            for (Entry<String, Integer> entry : data.getUsages()) {
                if (entry.getValue() * 100 >= sum * minimal) {
                    itemsList.add(new Item(entry.getKey(), entry.getValue() * 100 / data.logsCount));
                }
            }
            page.setAttribute(name + "MaxScale", 100);
            page.setAttribute(name + "List", itemsList);
        }
    }

    static String projectName(ProjectOp projectOp) {
        String name = projectOp.getProjectDisplayName();
        if (name.endsWith("Project")) {
            name = name.substring(0, name.length() - 7);
        }
        return name;
    }

    public static final class Item {
        private final String key;
        private final int value;

        public Item(String key, int value) {
            this.key = key;
            this.value = value;
        }

        public String getKey() {
            return key;
        }

        public int getValue() {
            return value;
        }
        
        public String getSerie(){
            return key;
        }
    }

    public static final class Counts {
        static final Counts EMPTY = new Counts();
        
        final Map<String,Integer> counts = new TreeMap<String, Integer>();
        int logsCount = 0;
        
        Counts() {
        }
        
        Counts(Map<String,Integer> in) {
            counts.putAll(in);
        }
        
        Counts(ProjectOp projectOp) {
            counts.put(projectName(projectOp), Math.abs(projectOp.getDelta()));
        }

        public final Set<Map.Entry<String,Integer>> getUsages() {
            Set<Entry<String, Integer>> t = counts.entrySet();
            return Collections.unmodifiableSet(t);
        }
        
        final void add(Counts toAdd) {
            logsCount += toAdd.logsCount;
            for (Map.Entry<String,Integer> entry : toAdd.counts.entrySet()) {
                Integer prev = counts.get(entry.getKey());
                if (prev == null) {
                    counts.put(entry.getKey(), entry.getValue());
                } else {
                    counts.put(entry.getKey(), entry.getValue() + prev);
                }
            }
        }

        final Counts countAsOne() {
            TreeMap<String, Integer> tm = new TreeMap<String, Integer>(counts);
            for (Entry<String, Integer> entry : tm.entrySet()) {
                entry.setValue(1);
            }
            return new Counts(tm);
        }
    } // end of Counts
    
    
    protected void write(Preferences pref, Counts d) {
        pref.putInt("logsCount", d.logsCount);
        for (Map.Entry<String, Integer> entry : d.counts.entrySet()) {
            pref.putInt(entry.getKey(), entry.getValue());
        }
    }

    protected Counts read(Preferences pref) throws BackingStoreException {
        int logsCount = 0;
        Map<String,Integer> amounts = new HashMap<String, Integer>();
        for (String k : pref.keys()) {
            int cnt = pref.getInt(k, 0);
            if ("logsCount".equals(k)){
                logsCount = cnt;
            }else if (cnt > 0) {
                amounts.put(k, cnt);
            }
        }
        Counts result = new Counts(amounts);
        result.logsCount = logsCount;
        return result;
    }
}
