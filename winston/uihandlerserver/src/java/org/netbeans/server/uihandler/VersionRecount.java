/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */
package org.netbeans.server.uihandler;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import org.netbeans.modules.exceptions.entity.Logfile;
import org.netbeans.modules.exceptions.entity.Nbversion;
import org.netbeans.modules.exceptions.utils.PersistenceUtils;
import org.netbeans.server.uihandler.LogsManager.Value;
import org.netbeans.web.Persistable;
import org.netbeans.web.Persistable.TransactionResult;
import org.openide.util.Lookup;

/**
 *
 * @author Jindrich Sedek
 */
final class VersionRecount implements Runnable {

    private static final Logger LOG = Logger.getLogger(VersionRecount.class.getName());
    private static final int RECOUNT_BATCH_SIZE = 10;
    private final File dir;
    private final Nbversion version;
    private final List<Value> sum = new ArrayList<Value>();
    private final FilesProvider filesProvider;
    int logsCount = 0;
    
    public VersionRecount(File dir, Nbversion version) {
        this.dir = dir;
        this.version = version;
        this.filesProvider = new FilesProvider(dir);
        for (Statistics<?> s : Lookup.getDefault().lookupAll(Statistics.class)) {
            LogsManager.addStatisticsData(sum, s);
        }
        List<Value> stored = new ArrayList<Value>(sum.size());
        for (Value<?> v : sum) {
            LogsManager.addStatisticsData(stored, v.statistics);
        }
        loadFromDB(stored);
    }

    public void run() {
        LOG.log(Level.FINE, "Checking status of {0} for {1} version", new Object[]{dir, version.getVersion()});

        int batchSize = RECOUNT_BATCH_SIZE;
        if (Utils.getParsingTimeout() == 0) {
            batchSize = Integer.MAX_VALUE;
        }
        final EntityManager em = PersistenceUtils.getInstance().createEntityManager();
        try {
            em.getTransaction().begin();
            for (int i = 0; i < batchSize; i++) {
                final File f = filesProvider.getNextFile();
                if (f == null) {
                    break;
                }

                final Logfile log = LogFileTask.getLogInfo(f.getName(), em);
                if (log == null) {
                    continue;
                }
                if (log.getProductVersionId() == null) {
                    continue;
                }
                if (!version.equals(log.getProductVersionId().getNbversionId())) {
                    continue;
                }
                LOG.log(Level.FINE, "Recomputing {0} for {1}", new Object[]{log.getFileName(), version.getVersion()});
                for (Value<?> v : sum) {
                    LogsManager.addDataForLog(v, log, em);
                }
                logsCount++;
            }
            if (filesProvider.isEmpty()) {
                persistResult(em);
                LogsManager.getDefault().setVersionStatistics(version, sum, logsCount);
            } else {
                LogsManager.getDefault().postVerificationTask(this);
            }
            em.getTransaction().commit();
        } catch (ThreadDeath td) {
            throw td;
        } catch (Throwable t) {
            LOG.log(Level.SEVERE, "Exception when processing: ", t);
            t.printStackTrace();
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
    }

    private void persistResult(EntityManager em) {
        LOG.log(Level.FINE, "Persisting data for {0}", version.getVersion());
        String userdir = "global" + version.getVersion();
        Logfile log = LogFileTask.getLogInfo(userdir, 0, em);
        if (log == null) {
            log = new Logfile(userdir, 0);
            em.persist(log);
        }
        for (Value<?> v : sum) {
            v.persist(log, em);
        }
    }

    private void loadFromDB(final List<Value> statisticsList) {
        LOG.log(Level.FINE, "reading previously stored globals for version {0}", version.getVersion());
        final String userdir = "global" + version.getVersion();
        org.netbeans.web.Utils.processPersistable(new Persistable.Query() {

            public TransactionResult runQuery(EntityManager em) {
                Logfile log = LogFileTask.getLogInfo(userdir, 0, em);
                if (log == null) {
                    LOG.log(Level.FINE, "no stored globals");
                } else {
                    for (Value<?> v : statisticsList) {
                        LogsManager.addDataForLog(v, log, em);
                    }
                    LogsManager.getDefault().setVersionStatistics(version, statisticsList, 0);
                    LOG.log(Level.INFO, "done reading previously stored globals for version {0}", version.getVersion());
                }
                return Persistable.TransactionResult.NONE;
            }
        });
    }
}
