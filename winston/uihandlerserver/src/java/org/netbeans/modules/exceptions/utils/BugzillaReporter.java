/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 2010 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 *
 * Contributor(s):
 *
 * Portions Copyrighted 2010 Sun Microsystems, Inc.
 */

package org.netbeans.modules.exceptions.utils;

import java.util.Collection;
import java.util.logging.Level;
import javax.persistence.EntityManager;
import org.netbeans.modules.exceptions.entity.Report;
import org.netbeans.modules.exceptions.entity.Submit;
import org.netbeans.server.uihandler.api.Issue;
import org.netbeans.server.uihandler.api.bugs.BugReporter;
import org.netbeans.server.uihandler.api.bugs.BugReporterException;
import org.netbeans.web.Persistable;
import org.netbeans.web.Persistable.TransactionResult;

/**
 *
 * @author Jindrich Sedek
 */
public class BugzillaReporter {

    private final BugReporter delegate;
    private static final ShortTimeCache<Integer, Issue> issueCache = new ShortTimeCache<Integer, Issue>();
    
    public BugzillaReporter (BugReporter delegate) {
        this.delegate = delegate;
    }

    public void reportSubmit(Submit submit, String password) {
        reportSubmit(submit, password, null, null);
    }

    public void reportSubmit(Submit submit, String password, String extraComment) {
        reportSubmit(submit, password, extraComment, null);
    }

    // this class is Singleton thanks to BugReporterFactory and thus a report will be reported only once
    public synchronized void reportSubmit(final Submit submit, final String password, final String extraComment, final String extraCC) {
        final int id = submit.getId();
        org.netbeans.web.Utils.processPersistable(new Persistable.Transaction() {

            public TransactionResult runQuery(EntityManager em) {
                try {
                    Submit thisSubmit = em.find(submit.getClass(), id);
                    if (thisSubmit == null) {
                        throw new IllegalStateException("Submit id " + id + " was not found");
                    }
                    Report report = thisSubmit.getReportId();
                    if (report.isInIssuezilla()){
                        // stop insertion
                        return TransactionResult.ROLLBACK;
                    }
                    int bugId = delegate.reportSubmit(thisSubmit.getSubmit(em), password, extraComment, extraCC);
                    report.setIssueId(bugId, BugzillaReporter.class);
                    report = em.merge(report);
                    return TransactionResult.COMMIT;
                } catch (Exception e) {
                    String message = "Bug was not created for report #" + id;
                    BugReporter.LOG.log(Level.SEVERE, message, e);
                    throw new BugReporterException(message, e);
                }
            }
        });
    }

    public void createAttachment(Submit submit, String password) throws BugReporterException{
        delegate.createAttachment(submit.getSubmit(null), password);
    }

    public void postTextComment(int issueId, String text, String username, String password) throws BugReporterException{
        delegate.postTextComment(issueId, text, username, password);
    }

    public void setIssuePriority(int issueId, String priority, String username, String password) throws BugReporterException{
        delegate.setIssuePriority(issueId, priority, username, password);
    }

    public void addToCCList(int issueId, String username, String password) throws BugReporterException{
        delegate.addToCCList(issueId, username, password);
    }

    public void reopenIssue(int issueId, String comment, String username, String password) throws BugReporterException{
        delegate.reopenIssue(issueId, comment, username, password);
    }

    public Issue getIssue(int issueId){
        Issue is = issueCache.get(issueId);
        if (is == null){
            is = delegate.getIssue(issueId);
            issueCache.put(issueId, is);
        }
        return is;
    }

    public Collection<Integer> filterOpen(Collection<Integer> ids){
        return delegate.filterOpen(ids);
    }

    public void postTextComment(int suezillaId, String commentMessage) {
        postTextComment(suezillaId, commentMessage, null, null);
    }

    public void reopenIssue(int suezillaId, String commentMessage) {
        reopenIssue(suezillaId, commentMessage, null, null);
    }

    public void setIssuePriority(int suezillaId, String priorityStr) {
        setIssuePriority(suezillaId, priorityStr, null, null);
    }

    public void clearCache(){
        issueCache.clear();
    }
}
