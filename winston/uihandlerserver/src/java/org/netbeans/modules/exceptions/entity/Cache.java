/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */

package org.netbeans.modules.exceptions.entity;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

/**
 *
 * @author Jindrich Sedek
 */
final class Cache<T, Q> {

    private static final int DEFAULT_CACHE_SIZE = 5000;
    private static final int MIN_ITEM_USAGE = 50;
    private final Map<T, CacheEntry> cache;

    public Cache() {
        this(DEFAULT_CACHE_SIZE);
    }

    public Cache(int size){
        cache = new HashMap<T, CacheEntry>(size);
    }
    
    public Q get(T key) {
        CacheEntry entry = cache.get(key);
        if (entry == null){
            return null;
        }
        entry.usageCount++;
        return entry.data;
    }

    public void put(T key, Q value) {
        if (cache.size() == DEFAULT_CACHE_SIZE - 1){
            dropUnusedItems();
        }
        cache.put(key, new CacheEntry(value));
    }

    private void dropUnusedItems() {
        Iterator<Entry<T, CacheEntry>> iterator = cache.entrySet().iterator();
        while (iterator.hasNext()){
            Entry<T, CacheEntry> value = iterator.next();
            if (value.getValue().usageCount < MIN_ITEM_USAGE){
                iterator.remove();
            }
            value.getValue().usageCount = 0;
        }
    }
    
    public void clear(){
        cache.clear();
    }
    
    int size(){
        return cache.size();
    }
    
    private class CacheEntry{
        Q data;
        int usageCount;
        public CacheEntry(Q data) {
            this.data = data;
            this.usageCount = 0;
        }
    }
}
