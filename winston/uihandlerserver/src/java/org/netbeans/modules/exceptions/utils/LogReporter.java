/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */
package org.netbeans.modules.exceptions.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import org.netbeans.server.uihandler.api.Issue;
import org.netbeans.server.uihandler.api.Submit;
import org.netbeans.server.uihandler.api.bugs.BugReporter;
import org.netbeans.server.uihandler.api.bugs.BugReporterException;

/**
 *
 * @author Jindrich Sedek
 */
public final class LogReporter implements BugReporter {

    static Map<Integer, Issue> issues = new HashMap<Integer, Issue>();

    private static int issuesId = 0;

    public int reportSubmit(Submit submit, String password, String extraComment, String extraUserToCC) throws BugReporterException {
        reportSubmit(submit, password, extraComment);
        return ++issuesId;
    }

    public void createAttachment(Submit submit, String password) throws BugReporterException {
        logMessage(submit.getSummary(), submit.getReporter().getUserName(), password,
                submit.getAttachment().getFileNameFormat() + ":" + submit.getAttachment().getFileNameFormat());
    }

    public void reportSubmit(Submit submit, String password, String extraComment) {
        logMessage(submit.getSummary(), submit.getReporter().getUserName(), password, extraComment);
    }

    @Override
    public void postTextComment(int issueId, String text, String username, String password) {
        logMessage(Integer.toString(issueId), username, password, text);
    }

    @Override
    public void setIssuePriority(int issueId, String priority, String username, String password) {
        logMessage(Integer.toString(issueId), username, password, priority);
    }

    @Override
    public void addToCCList(int issueId, String username, String password) {
        LogRecord log = new LogRecord(Level.INFO, "BUG REPORTER TO CC");
        List<String> parameters = new ArrayList<String>(4);
        parameters.add(Integer.toString(issueId));
        parameters.add(username);
        parameters.add(password);
        log.setParameters(parameters.toArray());
        BugReporter.LOG.log(log);
    }

    @Override
    public void reopenIssue(int issueId, String comment, String username, String password) {
        logMessage(Integer.toString(issueId), username, password, "REOPEN" + comment);
    }

    private void logMessage(String firstParam, String username, String passwd, String lastParam) {
        LogRecord log = new LogRecord(Level.INFO, "BUG REPORTER");
        List<String> parameters = new ArrayList<String>(5);
        parameters.add(firstParam);
        parameters.add(username);
        parameters.add(passwd);
        if (lastParam != null) {
            parameters.add(lastParam);
        }
        log.setParameters(parameters.toArray());
        BugReporter.LOG.log(log);
    }

    @Override
    public Issue getIssue(int issueId) {
        if (!issues.containsKey(issueId)) {
            issues.put(issueId, new Issue(issueId));
        }
        return issues.get(issueId);
    }

    @Override
    public Collection<Integer> filterOpen(Collection<Integer> data) {
        data = new HashSet<Integer>(data);
        Iterator<Integer> it = data.iterator();
        while (it.hasNext()) {
            Integer bugId = it.next();
            Issue bug = getIssue(bugId);
            if (bug != null && !bug.isOpen()){
                it.remove();
            }
        }
        return data;
    }

}
