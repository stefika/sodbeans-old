/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */
package org.netbeans.modules.exceptions.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author Jan Horvath
 */
@Embeddable
public class LinePK implements Serializable {
    @Column(name = "STACKTRACE_ID", nullable = false)
    private int stacktraceId;
    @Column(name = "METHOD_ID", nullable = false)
    private int methodId;
    @Column(name = "LINENUMBER", nullable = false)
    private int linenumber;
    @Column(name = "LINE_ORDER", nullable = false)
    private int lineOrder;

    public LinePK() {
    }

    public LinePK(int stacktraceId, int methodId, int linenumber, int lineOrder) {
        this.stacktraceId = stacktraceId;
        this.methodId = methodId;
        this.linenumber = linenumber;
        this.lineOrder = lineOrder;
    }

    public int getStacktraceId() {
        return stacktraceId;
    }

    public void setStacktraceId(int stacktraceId) {
        this.stacktraceId = stacktraceId;
    }

    public int getMethodId() {
        return methodId;
    }

    public void setMethodId(int methodId) {
        this.methodId = methodId;
    }

    public int getLinenumber() {
        return linenumber;
    }

    public void setLinenumber(int linenumber) {
        this.linenumber = linenumber;
    }

    public int getLineOrder() {
        return lineOrder;
    }

    public void setLineOrder(int lineOrder) {
        this.lineOrder = lineOrder;
    }

    @Override
    public int hashCode() {
        int hash = 0;

        hash += (int) stacktraceId;
        hash += (int) methodId;
        hash += (int) linenumber;
        hash += (int) lineOrder;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LinePK)) {
            return false;
        }
        LinePK other = (LinePK) object;

        if (this.stacktraceId != other.stacktraceId) {
            return false;
        }
        if (this.methodId != other.methodId) {
            return false;
        }
        if (this.linenumber != other.linenumber) {
            return false;
        }
        if (this.lineOrder != other.lineOrder) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.netbeans.modules.exceptions.entity.LinePK[stacktraceId=" +
               stacktraceId + ", methodId=" + methodId + ", linenumber=" +
               linenumber + ", lineOrder=" + lineOrder + "]";
    }

}
