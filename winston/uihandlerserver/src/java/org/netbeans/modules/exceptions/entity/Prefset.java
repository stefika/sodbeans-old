/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */

package org.netbeans.modules.exceptions.entity;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.prefs.Preferences;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.Table;

/**
 *
 * @author Jindrich Sedek
 */
@Entity
@Table(name = "PREFSET")
@NamedQueries({@NamedQuery(name = "Prefset.findByPrefname", query = "SELECT p FROM Prefset p WHERE p.prefsetPK.prefname = :prefname"), 
@NamedQuery(name = "Prefset.findByStatistic", query = "SELECT p FROM Prefset p WHERE p.prefsetPK.statistic = :statistic"), 
@NamedQuery(name = "Prefset.findByPrefnameNameAndStat", query = "SELECT p FROM Prefset p WHERE p.prefname.name = :name AND p.statistic = :statistic"), 
@NamedQuery(name = "Prefset.findByPrefnameAndStat", query = "SELECT p FROM Prefset p WHERE p.prefsetPK.prefname = :prefname AND p.prefsetPK.statistic = :statistic"), 
@NamedQuery(name = "Prefset.findByStatisticAndIndex", query = "SELECT p FROM Prefset p WHERE p.item_index = :index AND p.prefsetPK.statistic = :statistic"),
@NamedQuery(name = "Prefset.findByIndex", query = "SELECT p FROM Prefset p WHERE p.item_index = :index"),
@NamedQuery(name = "Prefset.getMaxIndex", query = "SELECT max(p.item_index) FROM Prefset p WHERE p.prefsetPK.statistic = :statistic")
})
public class Prefset implements Serializable {
    private static Map<Statistic, Integer> maxIndexes = new HashMap<Statistic, Integer>(4);
    private static final long serialVersionUID = 1L;

    @EmbeddedId
    protected PrefsetPK prefsetPK;
    @JoinColumn(name = "PREFNAME", referencedColumnName = "ID", insertable = false, updatable = false)
    @ManyToOne
    private Prefname prefname;
    @JoinColumn(name = "STATISTIC", referencedColumnName = "ID", insertable = false, updatable = false)
    @ManyToOne
    private Statistic statistic;
    @Column(name = "ITEM_INDEX", nullable = false)
    private int item_index;

    public Prefset() {
    }

    private Prefset(EntityManager em, Statistic statist, String name) {
        Prefname existPref = Prefname.getExistsOrNew(em, name);
        this.statistic = statist;
        this.prefname = existPref;
        this.prefsetPK = new PrefsetPK(existPref.getId(), statist.getId());
        this.item_index = getNextIndex(em, statist);
    }

    public PrefsetPK getPrefsetPK() {
        return prefsetPK;
    }

    public void setPrefsetPK(PrefsetPK prefsetPK) {
        this.prefsetPK = prefsetPK;
    }

    public Prefname getPrefname() {
        return prefname;
    }

    public void setPrefname(Prefname prefname1) {
        this.prefname = prefname1;
    }

    public Statistic getStatistic() {
        return statistic;
    }

    public void setStatistic(Statistic statistic1) {
        this.statistic = statistic1;
    }

    public static Integer getByteArrayIndex(EntityManager em, Statistic statist, String name){
        return getExistsOrNew(em, statist, name).getIndex();
    }
    
    private static Prefset getExistsOrNew(EntityManager em, Statistic statist, String name) {
        assert (name != null);
        assert (statist != null);
        Query query = em.createNamedQuery("Prefset.findByPrefnameNameAndStat");
        if (name.length() > Preferences.MAX_KEY_LENGTH) {
            name = name.substring(name.length() - Preferences.MAX_KEY_LENGTH);
        }
        query.setParameter("name", (Object) name);
        query.setParameter("statistic", statist);
        try {
            Object result = query.getSingleResult();
            return (Prefset) result;
        } catch (NoResultException exc) {
            Prefset prefset = new Prefset(em, statist, name);
            em.persist(prefset);
            return prefset;
        }
    }
        
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (prefsetPK != null ? prefsetPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Prefset)) {
            return false;
        }
        Prefset other = (Prefset) object;
        if (this.item_index != other.item_index){
            return false;
        }
        if ((this.prefsetPK == null && other.prefsetPK != null) || (this.prefsetPK != null && !this.prefsetPK.equals(other.prefsetPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.netbeans.modules.exceptions.entity.Prefset[prefsetPK=" + prefsetPK + "]";
    }
    
    public static Integer getMaxIndex(EntityManager em, Statistic statistic){
        Integer maxIndex = maxIndexes.get(statistic);
        if (maxIndex == null){
            Query query = em.createNamedQuery("Prefset.getMaxIndex");
            query.setParameter("statistic", statistic.getId());
            Object obj = query.getSingleResult();
            if (obj instanceof Integer){
                maxIndex = (Integer) obj;
            }else if (obj instanceof Long){
                Long lObj = (Long) obj;
                maxIndex = lObj.intValue();
            }else{
                maxIndex = 0;
            }
        }
        return maxIndex;
    }
    
    private static synchronized int getNextIndex(EntityManager em, Statistic statistic) {
        Integer maxIndex = getMaxIndex(em, statistic);
        maxIndex++;
        maxIndexes.put(statistic, maxIndex);
        return maxIndex;
    }

    public int getIndex() {
        return item_index;
    }

    public void setIndex(int index) {
        this.item_index = index;
    }
}
