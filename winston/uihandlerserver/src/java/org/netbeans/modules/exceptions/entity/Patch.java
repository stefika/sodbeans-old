/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */
package org.netbeans.modules.exceptions.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import org.netbeans.server.uihandler.Utils;

/**
 *
 * @author Jindrich Sedek
 */
@Entity
@Table(name = "PATCH")
@NamedQueries({@NamedQuery(name = "Patch.findByPatchname", query = "SELECT p FROM Patch p WHERE p.patchname = :patchname"),
@NamedQuery(name = "Patch.findByModulename", query = "SELECT p FROM Patch p WHERE p.modulename = :modulename"), 
@NamedQuery(name = "Patch.findBySpecversion", query = "SELECT p FROM Patch p WHERE p.specversion = :specversion"),
@NamedQuery(name = "Patch.findNbVersion", query = "SELECT p FROM Patch p WHERE p.nbversionId = :nbversion")})
public class Patch implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "ID", nullable = false)
    private Integer id;
    @Column(name = "PATCHNAME")
    private String patchname;
    @Column(name = "MODULENAME")
    private String modulename;
    @Column(name = "SPECVERSION")
    private String specversion;
    @JoinColumn(name = "NBVERSION_ID", referencedColumnName = "ID")
    @ManyToOne
    private Nbversion nbversionId;

    public Patch() {
    }

    public Patch(String patchname, String modulename, String specversion, Nbversion nbversionId) {
        this.id = Utils.getNextId(Patch.class);
        this.patchname = patchname;
        this.modulename = modulename;
        this.specversion = specversion;
        this.nbversionId = nbversionId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPatchname() {
        return patchname;
    }

    public void setPatchname(String patchname) {
        this.patchname = patchname;
    }

    public String getModulename() {
        return modulename;
    }

    public void setModulename(String modulename) {
        this.modulename = modulename;
    }

    public String getSpecversion() {
        return specversion;
    }

    public void setSpecversion(String specversion) {
        this.specversion = specversion;
    }

    public Nbversion getNbversionId() {
        return nbversionId;
    }

    public void setNbversionId(Nbversion nbversionId) {
        this.nbversionId = nbversionId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Patch)) {
            return false;
        }
        Patch other = (Patch) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.netbeans.modules.exceptions.entity.Patch[id=" + id + "]";
    }

}
