/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2007 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */
package org.netbeans.modules.exceptions.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author Jindrich Sedek
 */
@Entity
@Table(name = "SOURCEJAR_MAPPING")
@NamedQueries({@NamedQuery(name = "SourcejarMapping.findBySourcejar", query = "SELECT s FROM SourcejarMapping s WHERE s.sourcejar = :name"), 
        @NamedQuery(name = "SourcejarMapping.findByComponent", query = "SELECT s FROM SourcejarMapping s WHERE s.component = :component"), 
        @NamedQuery(name = "SourcejarMapping.findBySubcomponent", query = "SELECT s FROM SourcejarMapping s WHERE s.subcomponent = :subcomponent"),
        @NamedQuery(name = "SourcejarMapping.getAll", query = "SELECT s FROM SourcejarMapping s ORDER BY s.sourcejar")
})
public class SourcejarMapping implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "SOURCEJAR", nullable = false)
    private String sourcejar;
    @Column(name = "COMPONENT")
    private String component;
    @Column(name = "SUBCOMPONENT")
    private String subcomponent;

    public SourcejarMapping() {
    }

    public SourcejarMapping(String sourcejar) {
        this.sourcejar = sourcejar;
    }

    public String getSourcejar() {
        return sourcejar;
    }

    public boolean matches(Sourcejar matchedJar) {
        return matches(matchedJar.getName());
    }

    public boolean matches(String jarName) {
        return jarName.contains(sourcejar);
    }

    public void setSourcejar(String sourcejar) {
        this.sourcejar = sourcejar;
    }

    public String getComponent() {
        return component;
    }

    public void setComponent(String component) {
        this.component = component;
    }

    public String getSubcomponent() {
        return subcomponent;
    }

    public void setSubcomponent(String subcomponent) {
        this.subcomponent = subcomponent;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (sourcejar != null ? sourcejar.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SourcejarMapping)) {
            return false;
        }
        SourcejarMapping other = (SourcejarMapping) object;
        if ((this.sourcejar == null && other.sourcejar != null) || (this.sourcejar != null && !this.sourcejar.equals(other.sourcejar))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.netbeans.modules.exceptions.entity.SourcejarMapping[sourcejar=" + sourcejar + "]";
    }

}
