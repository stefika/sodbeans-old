/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */
package org.netbeans.modules.exceptions.entity;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import org.netbeans.modules.exceptions.utils.PersistenceUtils;

/**
 *
 * @author Jan Horvath
 */
@Entity
@Table(name = "line")
@NamedQueries({@NamedQuery(name = "Line.findByStacktraceId", query = "SELECT l FROM Line l WHERE l.linePK.stacktraceId = :stacktraceId ORDER BY l.linePK.lineOrder"),
    @NamedQuery(name = "Line.findByStacktrace", query = "SELECT l FROM Line l WHERE l.stacktrace = :stacktrace"),
    @NamedQuery(name = "Line.findByMethodId", query = "SELECT l FROM Line l WHERE l.linePK.methodId = :methodId"),
    @NamedQuery(name = "Line.findByLinenumber", query = "SELECT l FROM Line l WHERE l.linePK.linenumber = :linenumber"),
    @NamedQuery(name = "Line.findByLineOrder", query = "SELECT l FROM Line l WHERE l.linePK.lineOrder = :lineOrder"),
    @NamedQuery(name = "Line.findByLineHashcode", query = "SELECT l FROM Line l WHERE l.lineHashcode = :lineHashcode"),
    @NamedQuery(name = "Line.findByGeneratecomponent", query = "SELECT l FROM Line l WHERE l.generatecomponent = :generatecomponent"),
    @NamedQuery(name = "Line.findByStacktraceAndOrder", query = "SELECT l FROM Line l WHERE l.stacktrace.id = :stacktraceId AND l.linePK.lineOrder = :order"),
    @NamedQuery(name = "Line.findStacktraceByLineHash", query = "SELECT l.stacktrace FROM Line l WHERE l.lineHashcode = :code ORDER BY l.stacktrace.id DESC")
})
public class Line implements Serializable {

    @EmbeddedId
    protected LinePK linePK;
    @Column(name = "LINE_HASHCODE")
    private Integer lineHashcode;
    @Column(name = "GENERATECOMPONENT")
    private Integer generatecomponent;
    @JoinColumn(name = "STACKTRACE_ID", referencedColumnName = "ID", insertable = false, updatable = false)
    @ManyToOne
    private Stacktrace stacktrace;
    @JoinColumn(name = "METHOD_ID", referencedColumnName = "ID", insertable = false, updatable = false)
    @ManyToOne
    private Method method;
    @JoinColumn(name = "JARFILE_ID", referencedColumnName = "ID")
    @ManyToOne
    private Jarfile jarfileId;

    public Line() {
    }

    public Line(LinePK linePK) {
        this.linePK = linePK;
    }

    public Line(int stacktraceId, int methodId, int linenumber, int lineOrder) {
        this.linePK = new LinePK(stacktraceId, methodId, linenumber, lineOrder);
    }

    public LinePK getLinePK() {
        return linePK;
    }

    public void setLinePK(LinePK linePK) {
        this.linePK = linePK;
    }

    public Integer getLineHashcode() {
        return lineHashcode;
    }

    public void setLineHashcode(Integer lineHashcode) {
        this.lineHashcode = lineHashcode;
    }

    public Integer getGeneratecomponent() {
        return generatecomponent;
    }

    public void setGeneratecomponent(Integer generatecomponent) {
        this.generatecomponent = generatecomponent;
    }

    public Stacktrace getStacktrace() {
        return stacktrace;
    }

    public void setStacktrace(Stacktrace stacktrace) {
        this.stacktrace = stacktrace;
    }

    public Method getMethod() {
        return method;
    }

    public void setMethod(Method method) {
        this.method = method;
    }

    public Jarfile getJarfileId() {
        return jarfileId;
    }

    public void setJarfileId(Jarfile jarfileId) {
        this.jarfileId = jarfileId;
    }

    @Override
    public int hashCode() {
        int hash = 0;

        hash += (linePK != null ? linePK.hashCode()
                : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Line)) {
            return false;
        }
        Line other = (Line) object;

        if (this.linePK != other.linePK &&
                (this.linePK == null || !this.linePK.equals(other.linePK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.netbeans.modules.exceptions.entity.Line[linePK=" + linePK +
                "]";
    }
    private static final String NAME_DELIMITTER = "_";

    public String getLineName() {
        return getLinePK().getStacktraceId() + NAME_DELIMITTER + getLinePK().getLineOrder();
    }

    public static Line getLineByName(EntityManager em, String name) {
        int index = name.indexOf(NAME_DELIMITTER);
        if (index == -1) {
            throw new AssertionError("No line name");
        }
        String stacktraceIdString = name.substring(0, index);
        String orderStr = name.substring(index + 1);
        Integer stackId = Integer.parseInt(stacktraceIdString);
        Integer lineOrder = Integer.parseInt(orderStr);
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("stacktraceId", stackId);
        params.put("order", lineOrder);
        return (Line) PersistenceUtils.executeNamedQuerySingleResult(em, "Line.findByStacktraceAndOrder", params);
    }
}
