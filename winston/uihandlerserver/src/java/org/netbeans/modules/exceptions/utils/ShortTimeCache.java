/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 2009 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 *
 * Contributor(s):
 *
 * Portions Copyrighted 2009 Sun Microsystems, Inc.
 */
package org.netbeans.modules.exceptions.utils;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import org.openide.util.RequestProcessor;

/**
 *
 * @author Jindrich Sedek
 */
public class ShortTimeCache<Key, Value> {

    private RequestProcessor rp = new RequestProcessor("Short time cache");
    private static final int DEFAULT_TIMEOUT = 30000; // 30sec
    private Map<Key, Value> data = Collections.synchronizedMap(new HashMap<Key, Value>(3));
    private final int timeout;

    public ShortTimeCache() {
        this(DEFAULT_TIMEOUT);
    }

    public ShortTimeCache(int timeout) {
        this.timeout = timeout;
    }

    public Value put(Key key, Value value) {
        rp.post(new Clearer(key), timeout);
        return data.put(key, value);
    }

    public Value get(Key key) {
        return data.get(key);
    }

    public void clear(){
        data.clear();
    }
    
    public boolean contains(Key key) {
        return data.containsKey(key);
    }

    private class Clearer implements Runnable {

        private Key key;

        public Clearer(Key key) {
            this.key = key;
        }

        public void run() {
            data.remove(key);
        }
    }
}
