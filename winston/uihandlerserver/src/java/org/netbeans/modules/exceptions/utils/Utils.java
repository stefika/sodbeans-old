/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */
package org.netbeans.modules.exceptions.utils;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.netbeans.modules.exceptions.entity.Exceptions;
import org.netbeans.modules.exceptions.entity.Line;
import org.netbeans.modules.exceptions.entity.Nbversion;
import org.netbeans.modules.exceptions.entity.ProductVersion;
import org.netbeans.modules.exceptions.entity.Stacktrace;

/**
 *
 * @author Jan Horvath
 */
public class Utils {

    private static final String EXP = "[^0-9]*([0-9]{6,})[^0-9].*";
    private static final String HUDSON_BUILD_EXP = "[^0-9]*([0-9]{4}-[0-9]{2}-[0-9]{2})_.*";
    private static final Pattern BUILD_NUMBER_PATTERN = Pattern.compile(EXP);
    private static final Pattern HUDSON_BUILD_NUMBER_PATTERN = Pattern.compile(HUDSON_BUILD_EXP);
    private static final String NETBEANS_IDE_PREFIX = "NetBeans IDE ";
    private static final String NETBEANS_PLATFORM_PREFIX = "NetBeans Platform ";
    private static final Logger LOG = Logger.getLogger(Utils.class.getName());

    public static byte[] format(Stacktrace stacktrace) {
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        doPrintStackTrace(new PrintStream(os), stacktrace);
        return os.toByteArray();
    }

    private static void doPrintStackTrace(PrintStream printStream, Stacktrace stacktrace) {
        Stacktrace lower = stacktrace.getAnnotation();
        if (lower != null) {
            doPrintStackTrace(printStream, lower);
            printStream.print("Caused: ");                  //NOI18N
        }
        String summary = stacktrace.getMessage();
        if (lower != null) {
            String suffix = ": " + lower;
            if (summary.endsWith(suffix)) {
                summary = summary.substring(0, summary.length() - suffix.length());
            }
        }
        printStream.println(summary);
        List<Line> lines = stacktrace.getSortedLines();
        int end = lines.size();
        Stacktrace higher = stacktrace.getStacktrace();
        if (higher != null) {
            List<Line> higherLines = higher.getSortedLines();
            while (end > 0) {
                int higherEnd = end + higherLines.size() - lines.size();
                if (higherEnd <= 0 || !linesAreEqual(higherLines.get(higherEnd - 1), lines.get(end - 1))) {
                    break;
                }
                end--;
            }
        }
        for (int i = 0; i < end; i++) {
            printStream.println(Utils.formatStacktraceLine(lines.get(i)));
        }
    }

    /**
     * 
     * @param line 
     * @return 
     */
    public static String formatStacktraceLine(Line line) {
        String methodName = line.getMethod().getName();
        Pattern pat = Pattern.compile("\\.");
        String[] items = pat.split(methodName);
        int i = items.length-2;
        if (i < 0) {
            i = 0;
        }
        StringBuffer fileName = new StringBuffer();
        int s = items[i].indexOf("$");
        if (s >= 0) {
            fileName.append(items[i].substring(0, s));
        } else {
            fileName.append(items[i]);
        }
        fileName.append(".java");     
        return "   at " + methodName + "(" + fileName + ":" + line.getLinePK().getLinenumber() + ")";
    }
    
    public static String getBuildNumber(String line) {        
        Matcher m = BUILD_NUMBER_PATTERN.matcher(line);
        if (m.find()) {
            return m.group(1);
        } else {
            m = HUDSON_BUILD_NUMBER_PATTERN.matcher(line);
            if (m.find()) {
                return m.group(1).replaceAll("-", "");
            }
        }
        return "0";
    }
    
    public static String getCustomBuildFormat(String line) {        
        if (line.length() == 12) {
            return line.substring(2, 8);//daily build
        }
        if (line.length() == 14) {
            return line.substring(2, 8);//strange build number
        }
        if (line.length() == 8) {
            return line.substring(2);//continous build
        }
        return line;
    }

    public static boolean isDailyBuild(ProductVersion pv){
        String buildNumber = getBuildNumber(pv.getProductVersion());
        if (buildNumber.length() == 12){
            return true;
        }
        return false;
    }

    public static String getNbVersion(String productVersion){
        String result = productVersion.replaceAll("\\(.*\\)", "");
        result = result.replaceAll("\\d{6,}", "").trim();
        if (result.startsWith(NETBEANS_IDE_PREFIX)) {
            result = result.substring(NETBEANS_IDE_PREFIX.length(), result.length());
        }
        if (result.startsWith(NETBEANS_PLATFORM_PREFIX)) {
            result = result.substring(NETBEANS_PLATFORM_PREFIX.length(), result.length());
        }
        return result.trim();
    }

    private static boolean linesAreEqual(Line l1, Line l2) {
        return l1.getMethod().getName().equals(l2.getMethod().getName());
    }

    /// prepare first n lines of stacktrace
    public static String getStacktraceLines(Exceptions exceptions, int n, String lineDelim) {
        StringBuilder stacktrace = new StringBuilder();
        Stacktrace cause = exceptions.getRelevantCause();
        if (exceptions.getStacktrace() != null) {
            stacktrace.append(cause.getMessage()).append(lineDelim);
            List<Line> lines = new ArrayList<Line>(cause.getLineCollection());
            Collections.sort(lines, new LineComparator());
            int i = n;
            for (Line line : lines) {
                stacktrace.append(formatStacktraceLine(line)).append(lineDelim);
                if (i-- < 1) {
                    break;
                }
            }
        }
        return stacktrace.toString();
    }
    
    public static String getBugzillaVersion(Nbversion nbversion) {
        LOG.log(Level.FINEST, "NbVersion.getId():" + nbversion.getId() + ";");
        LOG.log(Level.FINEST, "NbVersion.getVersion():" + nbversion.getVersion() + ";");
        LOG.log(Level.FINEST, "NbVersion.getVersionNumber():" + nbversion.getVersionNumber() + ";");
        if (Nbversion.DEV_VERSION.equals(nbversion.getVersion())) {
            return Nbversion.DEFAULT_VERSION;
        } else if (nbversion.getVersion().matches("\\d\\.\\d\\.\\d")){
            return  nbversion.getVersion().substring(0, 3);
        } else {
            return nbversion.getVersionNumber();
        }
    }
}

