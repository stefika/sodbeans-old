/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */
package org.netbeans.modules.exceptions.entity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.netbeans.modules.exceptions.utils.LineComparator;
import org.netbeans.modules.exceptions.utils.Utils;
import org.netbeans.server.uihandler.api.Submit.Attachment;
import org.netbeans.web.Persistable;
import org.openide.util.RequestProcessor;

/**
 *
 * @author Jan Horvath
 */
@Entity
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE, include="non-lazy")
@Table(name = "exceptions")
@NamedQueries({
@NamedQuery(name = "Exceptions.countByInterval", query = "SELECT COUNT(e) FROM Exceptions e WHERE e.reportdate BETWEEN :start AND :end"),
@NamedQuery(name = "Exceptions.countByBuildInterval", query = "SELECT COUNT(e) FROM Exceptions e WHERE e.build BETWEEN :start AND :end"),
@NamedQuery(name = "Exceptions.countByBuild", query = "SELECT COUNT(e) FROM Exceptions e WHERE e.build = :build"),
@NamedQuery(name = "Exceptions.groupByBuilds", query = "SELECT e.build, COUNT(e) FROM Exceptions e WHERE e.logfileId.productVersionId.nbversionId = :version GROUP BY e.build ORDER BY e.build"),
@NamedQuery(name = "Exceptions.selectBuilds", query = "SELECT DISTINCT e.build FROM Exceptions e WHERE e.build > :from ORDER BY e.build"),
@NamedQuery(name = "Exceptions.maxBuild", query = "SELECT MAX(e.build) FROM Exceptions e WHERE e.reportId = :reportId"),
@NamedQuery(name = "Exceptions.duplicatesFromOneUser", query = "SELECT COUNT(e) FROM Exceptions e WHERE e.reportId = :reportId AND e.nbuserId = :nbuser"),
@NamedQuery(name = "Exceptions.duplicatesFromDistinctUsersCount", query = "SELECT COUNT(DISTINCT e.nbuserId) FROM Exceptions e WHERE e.reportId = :reportId"),
//@NamedQuery(name = "Exceptions.duplicatesCount", query = "SELECT COUNT(e) FROM Exceptions e WHERE e.duplicateof = :duplicateof"),
@NamedQuery(name = "Exceptions.distinctComponents", query = "SELECT DISTINCT e.reportId.component from Exceptions e"),
@NamedQuery(name = "Exceptions.countByComponent", query = "SELECT COUNT(e) from Exceptions e WHERE e.reportId.component = :component"),
@NamedQuery(name = "Exceptions.countAll", query = "SELECT COUNT(e) from Exceptions e"),
@NamedQuery(name = "Exceptions.reportDateQuery", query = "SELECT COUNT(e) FROM Exceptions e WHERE e.logfileId.productVersionId.nbversionId = :version AND e.reportdate BETWEEN :start AND :end"),
@NamedQuery(name = "Exceptions.buildDateQuery", query = "SELECT COUNT(e) FROM Exceptions e WHERE e.logfileId.productVersionId.nbversionId = :version AND e.build BETWEEN :start AND :end"),
@NamedQuery(name = "Exceptions.findByUserId", query = "SELECT  e FROM Exceptions e WHERE e.logfileId.userdir = :id"),
@NamedQuery(name = "Exceptions.findByUserName", query = "SELECT e FROM Exceptions e WHERE e.nbuserId.name = :name"),
@NamedQuery(name = "Exceptions.findByLikeLine", query = "SELECT DISTINCT e FROM Exceptions e, Line l WHERE e.stacktrace = l.stacktrace AND l.method.name LIKE :line"),
@NamedQuery(name = "Exceptions.findLatestByReport", query = "SELECT e FROM Exceptions e WHERE e.reportId = :report AND e.id >= ALL(SELECT e2.id FROM Exceptions e2 WHERE e2.reportId = e.reportId)"),
@NamedQuery(name = "Exceptions.findReportByLikeStackMessage", query = "SELECT s.exceptions FROM Stacktrace s WHERE s.message LIKE :message ORDER BY s.exceptions DESC"),
@NamedQuery(name = "Exceptions.countByReport", query = "SELECT COUNT(e) FROM Exceptions e WHERE e.reportId = :reportId")
})
                                                        
public class Exceptions extends Submit {

    @Column(name = "BUILD")
    private Long build;
    @JoinColumn(name = "STACKTRACE", referencedColumnName = "ID")
    @OneToOne(fetch=FetchType.LAZY)
    private Stacktrace stacktrace;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "exceptions", fetch=FetchType.LAZY)
    private Hashcodes hashcodes;
    
    private static RequestProcessor.Task dupChangesTask = null;
    public Exceptions() {
    }
    
    public Exceptions(Integer id) {
        super(id);
    }

    public Long getBuild() {
        return build;
    }
    
    public void setBuild(Long build) {
        this.build = build;
    }
    
    public Stacktrace getStacktrace() {
        return stacktrace;
    }
    
    public void setStacktrace(Stacktrace stacktrace) {
        this.stacktrace = stacktrace;
    }
    
    public Hashcodes getHashcodes() {
        return hashcodes;
    }
    
    public void setHashcodes(Hashcodes hashcodes) {
        this.hashcodes = hashcodes;
    }
   
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (getId() != null ? getId().hashCode() : 0);
        return hash;
    }
    
    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Exceptions)) {
            return false;
        }
        Exceptions other = (Exceptions) object;
        
        if (getId() != other.getId() &&
                (getId() == null || !getId().equals(other.getId()))) {
            return false;
        }
        return true;
    }
    
    @Override
    public String toString() {
        return "Exceptions[id=" + getId() +
                ", build=" + getBuild() +
                "]";
    }

    public Throwable getMockThrowable(){
        return new MockThrowable(getStacktrace());
    }

    @Override
    public String getMessage() {
        return getStacktrace().getMessage();
    }

    public Stacktrace getRelevantCause(){
        Stacktrace st = getStacktrace();
        while (st != null && st.getAnnotation() != null){
            Stacktrace cause = st.getAnnotation();
            if (cause.getLineCollection().size() < 2) {
                break;
            }
            if (cause.getMessage() != null && cause.getMessage().contains("org.openide.util.RequestProcessor$Item")){
                break;
            }
            st = cause;
        }
        return st;

    }

    @Override
    public String getTitleName() {
        return "Exception";
    }

    @Override
    public boolean containsMethod(String methodName) {
        for (Line line : getStacktrace().getLineCollection()) {
            if (line.getMethod().getName().startsWith(methodName)){
                return true;
            }
        }
        return false;
    }

    private static class MockThrowable extends Throwable {
        StackTraceElement[] preparedStacktrace = null;
        final Stacktrace stack;

        public MockThrowable(Stacktrace stack) {
            this.stack = stack;
        }

        @Override
        public String getMessage() {
            return stack.getMessage();
        }

        @Override
        public Throwable getCause() {
            if (stack.getAnnotation() != null) {
                return new MockThrowable(stack.getAnnotation());
            }
            return null;
        }

        @Override
        public StackTraceElement[] getStackTrace() {
            if (preparedStacktrace == null){
                List<Line> lines = new ArrayList<Line>(stack.getLineCollection());
                Collections.sort(lines, new LineComparator());
                List<StackTraceElement> stacktrace = new ArrayList<StackTraceElement>(lines.size());
                for (Line line : lines) {
                    stacktrace.add(line.getMethod().getStackTraceElement());
                }
                preparedStacktrace = stacktrace.toArray(new StackTraceElement[0]);
            }
            return preparedStacktrace;
        }
    }

    @Override
    public void setReportId(Report reportId) {
        if ((reportId == null) || (reportId.equals(this.getReportId()))){
            return;
        }
        if (this.getReportId() != null){
            dupChangesTask = RequestProcessor.getDefault().post(new DupChangesLogger(), 0, Thread.MIN_PRIORITY);
        }
        super.setReportId(reportId);
    }

    private class DupChangesLogger implements Runnable, Persistable{

        private final Report oldValue;

        private DupChangesLogger() {
            this.oldValue = Exceptions.this.getReportId();
        }

        public void run() {
            org.netbeans.web.Utils.processPersistable(this);
        }

        public TransactionResult runQuery(EntityManager em) throws Exception {
            Integer duplicateLogId = org.netbeans.server.uihandler.Utils.getNextId(DuplicateLog.class);
            DuplicateLog dupLog = new DuplicateLog(duplicateLogId, oldValue, Exceptions.this);
            em.persist(dupLog);
            return TransactionResult.COMMIT;
        }

        public boolean needsTransaction() {
            return true;
        }

    }

    /** used in tests!!! */
    static void waitDupChanges(){
        if (dupChangesTask != null){
            dupChangesTask.waitFinished();
        }
    }

    // abstract methods
    public String getAdditionalComment() {
        return "\n\nStacktrace: \n" + Utils.getStacktraceLines(this, 5, "\n");
    }

    public String getStatusWhiteboard() {
        return "EXCEPTIONS_REPORT";
    }

    public String getKeywords() {
        return null;
    }

    @Override
    public Attachment getAttachment() {
        byte[] sb = Utils.format(getStacktrace());
        return new Attachment("stacktrace-%1$s.txt", "stacktrace", "text/plain", sb);
    }
}
