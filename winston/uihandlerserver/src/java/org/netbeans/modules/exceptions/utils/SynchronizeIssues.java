/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 2009 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 *
 * Contributor(s):
 *
 * Portions Copyrighted 2009 Sun Microsystems, Inc.
 */
package org.netbeans.modules.exceptions.utils;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import org.netbeans.server.uihandler.bugs.BugzillaConnector;
import org.netbeans.modules.exceptions.entity.Exceptions;
import org.netbeans.modules.exceptions.entity.Nbuser;
import org.netbeans.modules.exceptions.entity.Report;
import org.netbeans.modules.exceptions.entity.ReportComment;
import org.netbeans.modules.exceptions.entity.Submit;
import org.netbeans.server.services.beans.Mappings;
import org.netbeans.server.uihandler.api.Issue;
import org.netbeans.web.Persistable;
import org.netbeans.web.Persistable.TransactionResult;
import org.netbeans.web.Utils;
import org.openide.util.RequestProcessor;

/**
 *
 * @author honza
 */
public class SynchronizeIssues implements Runnable {

    private static final RequestProcessor SYNCHRONIZATION_RP = new RequestProcessor("SYNCHRONIZATION");
    private static final int SYNCHRONIZATION_TIMEOUT = 1000 * 60 * 5;//5mins
    //every DEEP_SYNCHRONIZATION_INDEXth synchronization should be deep
    private static final int DEEP_SYNCHRONIZATION_INDEX = 300;
    private static final String QUERY = "SELECT r.issueId FROM Report r WHERE "
            + "r.issueId IS NOT NULL AND r.issueId <> 0 AND "
            + "((SELECT COUNT(e) FROM Exceptions e WHERE e.reportId = r) > 0 "
            + "OR (SELECT COUNT(s) FROM Slowness s WHERE s.reportId = r) > 0)"
            + "GROUP BY r.issueId HAVING COUNT(id) > 1";
    private static final String MARKED_AS_DUPLICATE = "This report was marked as a duplicate of <a href='"
            + Utils.EXCEPTION_DETAIL + "%1$s'>report %1$s</a>";
    static final Logger SYNCHRONIZATION_LOGGER = Logger.getLogger(SynchronizeIssues.class.getName());
    private final AtomicBoolean isRunning = new AtomicBoolean(false);
    // reports whose "in transport" flag should be cleaned next round 
    private final Set<Integer> reportsToClean = new HashSet<Integer>();
    private int synchronizationCount = 0;

    SynchronizeIssues() {
    }

    /**
     * All issues that were marked as duplicates in issuezilla should be merged also
     * into one report. The history of changes is held in LogDuplicates entities
     * for possible split during issuezilla reopen.
     *
     */
    void mergeReportsWithOneIssueId() {
        Utils.processPersistable(new Persistable.Transaction() {

            public TransactionResult runQuery(EntityManager em) throws Exception {
                BugzillaReporter reporter = BugReporterFactory.getDefaultReporter();
                List<Integer> issueIds = PersistenceUtils.executeQuery(em, QUERY, null);
                javax.persistence.Query reportQuery = em.createNamedQuery("Report.findByIssueId");
                for (Integer issueId : issueIds) {
                    Report rootReport = null;
                    reportQuery.setParameter("issueId", issueId);
                    Issue issue = reporter.getIssue(issueId);
                    List<Report> reports = reportQuery.getResultList();

                    //find report referenced from IZ
                    if (issue == null) {
                        SYNCHRONIZATION_LOGGER.log(Level.SEVERE, "Impossible to find issue #{0}", issueId.toString());
                        continue;
                    }
                    Integer reporterSubmitId = issue.getReporterSubmitId();
                    if (reporterSubmitId != null) {
                        for (Report report : reports) {
                            if (reporterSubmitId.equals(report.getId())) {
                                rootReport = report;
                                break;
                            }
                        }
                        if (rootReport == null) {
                            //workaround for old links from issuezilla
                            if (reporterSubmitId < 146000) {
                                Exceptions exc = em.getReference(Exceptions.class, reporterSubmitId);
                                if (exc != null) {
                                    rootReport = exc.getReportId();
                                } else {
                                    SYNCHRONIZATION_LOGGER.log(Level.SEVERE, "Impossible to find root report {0}", reporterSubmitId.toString());
                                    continue;
                                }
                            }
                        }
                    } else {
                        // the root report was not filled using exception reporter
                        rootReport = reports.iterator().next();
                    }
                    reports.remove(rootReport);

                    // move all exceptions to the referenced report
                    for (Report report : reports) {
                        SYNCHRONIZATION_LOGGER.log(Level.INFO, "merging report {0} to report {1}", new Object[]{report.getId().toString(), rootReport.getId().toString()});
                        report.preloadSubmitCollection(em);
                        for (Submit sub : report.getSubmitCollection()) {
                            sub.setReportId(rootReport);
                            em.merge(sub);
                        }
                        ReportComment rc = new ReportComment();
                        rc.generateId();
                        rc.setReportId(report);
                        String message = String.format(MARKED_AS_DUPLICATE, rootReport.getId());
                        rc.setComment(message);

                        rc.setNbuserId(getReporterUser(em));
                        em.persist(rc);
                    }
                }
                return TransactionResult.COMMIT;
            }
        });
    }

    private Nbuser getReporterUser(EntityManager em) {
        Nbuser reporterUser = (Nbuser) PersistenceUtils.executeNamedQuerySingleResult(em, "Nbuser.findByName",
                Collections.singletonMap("name", (Object) org.netbeans.server.uihandler.Utils.getReporterUsername()));
        if (reporterUser == null) {
            reporterUser = new Nbuser(org.netbeans.server.uihandler.Utils.getNextId(Nbuser.class));
            reporterUser.setName(org.netbeans.server.uihandler.Utils.getReporterUsername());
            em.persist(reporterUser);
        }
        return reporterUser;
    }

    void updateInTransferReports() {
        SYNCHRONIZATION_LOGGER.fine("cleaning in transfer");
        Mappings mappings = getMappings();
        if (mappings == null) {
            return;
        }
        Map<Integer, Integer> mappingMap = mappings.getMappings();
        //** reportId -> bugId  */
        final Map<Integer, Integer> reverseMap = new HashMap<Integer, Integer>();
        for (Entry<Integer, Integer> entry : mappingMap.entrySet()) {
            reverseMap.put(entry.getValue(), entry.getKey());
        }
        Utils.processPersistable(new Persistable.Transaction() {

            public TransactionResult runQuery(EntityManager em) throws Exception {
                List<Integer> l;
                if (PersistenceUtils.DEFAULT_PU.equals(PersistenceUtils.TEST_PU)){
                    l = PersistenceUtils.executeQuery(em, "SELECT r.id from Report r WHERE status <> 0", Collections.<String, Object>emptyMap());
                }else{
                    String query = "SELECT id FROM report WHERE (status & " + Report.Status.ISSUEZILLA_TRANSFER.intValue() + ") <> 0";
                    l = em.createNativeQuery(query).getResultList();
                }
                for (Iterator<Integer> it = l.iterator(); it.hasNext();) {
                    Integer id = it.next();
                    Report report = em.find(Report.class, id);
                    boolean changed = false;
                    if (!report.isInIssuezilla()) {
                        Integer bugId = reverseMap.get(report.getId());
                        if (bugId != null) {
                            report.setIssueId(bugId, SynchronizeIssues.class);
                            changed = true;
                            SYNCHRONIZATION_LOGGER.log(Level.INFO, "setting bugId {0} for report id {1}", new Object[]{bugId.toString(), id.toString()});
                        }
                    }
                    if (report.getIssueId() != null || reportsToClean.contains(report.getId())) {
                        report.setIssuezillaTransfer(false);
                        changed = true;
                        reportsToClean.remove(report.getId());
                        SYNCHRONIZATION_LOGGER.log(Level.INFO, "cleaning in transfer for report id {0}", id.toString());
                    } else {
                        reportsToClean.add(report.getId());
                    }
                    if (changed) {
                        em.merge(report);
                    }
                }
                return TransactionResult.COMMIT;
            }
        });
    }

    void updateComponentAndDuplicates() {
        SYNCHRONIZATION_LOGGER.fine("Synchronizing components");
        Utils.processPersistable(new Persistable.Transaction() {

            public TransactionResult runQuery(EntityManager em) throws Exception {
                List<Report> reports = em.createNamedQuery("Report.findAllInIZ").getResultList();
                int count = 0;
                BugzillaReporter bzr = BugReporterFactory.getDefaultReporter();
                for (Report report : reports) {
                    assert (report.isInIssuezilla());
                    count++;
                    if (count % 1000 == 0) {
                        em.getTransaction().commit();
                        em.getTransaction().begin();
                    }
                    Issue iss = bzr.getIssue(report.getIssueId());
                    if (iss == null) {
                        SYNCHRONIZATION_LOGGER.log(Level.SEVERE, "not found issue for report{0}", report.getId().toString());
                        continue;
                    }
                    if (iss.getComponent() == null || iss.getSubcomponent() == null) {
                        SYNCHRONIZATION_LOGGER.log(Level.SEVERE, "null component of subcomponent for issue {0}", iss.getId().toString());
                        continue;
                    }
                    if (!iss.getComponent().equals(report.getComponent()) || !iss.getSubcomponent().equals(report.getSubcomponent())) {
                        SYNCHRONIZATION_LOGGER.log(Level.INFO, "changing report {0} FROM {1}/{2} TO {3}/{4}", new Object[]{report.getId().toString(), report.getComponent(), report.getSubcomponent(), iss.getComponent(), iss.getSubcomponent()});
                        report.setComponent(iss.getComponent());
                        report.setSubcomponent(iss.getSubcomponent());
                        em.merge(report);
                    }
                    if (iss.getDuplicateOf() != null) {
                        SYNCHRONIZATION_LOGGER.log(Level.INFO, "issue {0} was marked as duplicate of {1}", new Object[]{report.getIssueId().toString(), iss.getDuplicateOf().toString()});
                        report.setIssueId(iss.getDuplicateOf(), SynchronizeIssues.class);
                        em.merge(report);
                    }
                }
                return TransactionResult.COMMIT;
            }
        });
    }

    private static Mappings getMappings(){
        try{
            return BugzillaConnector.getInstance().getMappings();
        }catch(RuntimeException re){
            SYNCHRONIZATION_LOGGER.log(Level.WARNING, "Getting BZ mappings failed");
            SYNCHRONIZATION_LOGGER.log(Level.FINE, "Getting BZ mappings failed", re);
        }
        return null;
    }

    /** in case someone add Reporter number into Bugzilla directly */
    void updateMappings() {
        SYNCHRONIZATION_LOGGER.fine("updating mappings");
        Utils.processPersistable(new Persistable.Transaction() {

            public TransactionResult runQuery(EntityManager em) throws Exception {
                Mappings mappings = getMappings();
                if (mappings == null){
                    return TransactionResult.ROLLBACK;
                }
                for (Entry<Integer, Integer> mappingEntry : mappings.getMappings().entrySet()) {
                    Integer bugId = mappingEntry.getKey();
                    Integer cfAutoreporterId = mappingEntry.getValue();
                    Report rep = em.find(Report.class, cfAutoreporterId);
                    if (rep == null) {
                        continue;
                    }
                    if (rep.getIssueId() == null) {
                        SYNCHRONIZATION_LOGGER.log(Level.INFO, "updating report {0} from {1} to {2}", new Object[]{String.valueOf(cfAutoreporterId), String.valueOf(rep.getIssueId()), String.valueOf(bugId)});
                        rep.setIssueId(bugId, SynchronizeIssues.class);
                        em.merge(rep);
                    }
                }
                return TransactionResult.COMMIT;
            }
        });
    }

    public static void startSynchronization() {
        SYNCHRONIZATION_RP.post(new SynchronizeIssues(), SYNCHRONIZATION_TIMEOUT, Thread.MIN_PRIORITY);
    }

    public void run() {
        SYNCHRONIZATION_RP.post(this, SYNCHRONIZATION_TIMEOUT, Thread.MIN_PRIORITY);
        if (isRunning.getAndSet(true)) {
            SYNCHRONIZATION_LOGGER.warning("Synchronize issues was started twice!");
            return;
        }
        synchronizationCount++;
        try {
            updateMappings();
            updateInTransferReports();
            if (synchronizationCount % DEEP_SYNCHRONIZATION_INDEX == 0) {
                SYNCHRONIZATION_RP.post(new DeepSynchronization());
            }
        } finally {
            isRunning.set(false);
        }
    }

    private class DeepSynchronization implements Runnable{

        public void run() {
            updateComponentAndDuplicates();
            mergeReportsWithOneIssueId();
        }

    }
}
