/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 2010 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 *
 * Contributor(s):
 *
 * Portions Copyrighted 2010 Sun Microsystems, Inc.
 */
package org.netbeans.modules.exceptions.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author Jindrich Sedek
 */
@Entity
@Table(name = "ISSUE_BUILD")
@NamedQueries({
    @NamedQuery(name = "IssueBuild.findAll", query = "SELECT i FROM IssueBuild i"),
    @NamedQuery(name = "IssueBuild.findById", query = "SELECT i FROM IssueBuild i WHERE i.id = :id"),
    @NamedQuery(name = "IssueBuild.findByBuildNumber", query = "SELECT i FROM IssueBuild i WHERE i.buildNumber = :buildNumber")})
public class IssueBuild implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "ISSUE_NUMBER")
    private Integer issueNumber;
    @Column(name = "BUILD_NUMBER")
    private Long buildNumber;

    public IssueBuild() {
    }

    public IssueBuild(Integer issueNumber, Long buildNumber) {
        this.issueNumber = issueNumber;
        this.buildNumber = buildNumber;
    }

    public Integer getIssueNumber() {
        return issueNumber;
    }

    public void setIssueNumber(Integer issueNumber) {
        this.issueNumber = issueNumber;
    }

    public Long getBuildNumber() {
        return buildNumber;
    }

    public void setBuildNumber(Long buildNumber) {
        this.buildNumber = buildNumber;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (issueNumber != null ? issueNumber.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof IssueBuild)) {
            return false;
        }
        IssueBuild other = (IssueBuild) object;
        if ((this.issueNumber == null && other.issueNumber != null) || (this.issueNumber != null && !this.issueNumber.equals(other.issueNumber))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.netbeans.modules.exceptions.entity.IssueBuild[id=" + issueNumber + "]";
    }

}
