/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */
package org.netbeans.modules.exceptions.entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.netbeans.modules.exceptions.utils.PersistenceUtils;
import org.netbeans.server.uihandler.Utils;

/**
 *
 * @author Jindrich Sedek
 */
@Entity
@Table(name = "prefvalue")
@NamedQueries({
    @NamedQuery(name = "Prefvalue.findByValue", query = "SELECT p FROM Prefvalue p WHERE p.value = :name")
})
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE, include="non-lazy")
public class Prefvalue implements Serializable {

    private static final Cache<String, Integer> prefValueCache = new Cache<String, Integer>(300);
    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "ID", nullable = false)
    private Integer id;
    @Column(name = "VALUE", length = 8196)
    private String value;
    @OneToMany(mappedBy = "value", fetch=FetchType.LAZY)
    private Collection<Preference> preferencesCollection;

    public Prefvalue() {
    }


    public Prefvalue(String value) {
        this.id = Utils.getNextId(Prefvalue.class);
        this.value = value;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public static Prefvalue getExistsOrNew(EntityManager em, String value) {
        assert (value != null);
        Prefvalue prefValue = null;
        Integer id = prefValueCache.get(value);
        if (id != null){
            prefValue = em.find(Prefvalue.class, id);
            if (prefValue == null){ // invalidate cache
                prefValueCache.clear();
            }
        }
        if (prefValue == null){
            prefValue = (Prefvalue) PersistenceUtils.getExist(em, "Prefvalue.findByValue", value);
            if (prefValue == null) {
                prefValue = new Prefvalue(value);
                em.persist(prefValue);
            }else{
                prefValueCache.put(value, prefValue.getId());
            }
        }
        return prefValue;
    }

    public Collection<Preference> getPreferencesCollection() {
        return preferencesCollection;
    }

    public void setPreferencesCollection(Collection<Preference> preferencesCollection) {
        this.preferencesCollection = preferencesCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Prefvalue)) {
            return false;
        }
        Prefvalue other = (Prefvalue) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.netbeans.exceptions.entites.Prefvalue[id=" + id + "]";
    }

    public static void dropCache(){
        prefValueCache.clear();
    }
}
