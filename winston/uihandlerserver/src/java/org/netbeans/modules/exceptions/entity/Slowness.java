/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2009 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */
package org.netbeans.modules.exceptions.entity;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Collections;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.netbeans.modules.exceptions.utils.PersistenceUtils;
import org.netbeans.server.snapshots.SnapshotManager;
import org.netbeans.server.uihandler.api.Submit.Attachment;
import org.openide.util.Exceptions;

/**
 *
 * @author Jindrich Sedek
 */
@Entity
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE, include="non-lazy")
@Table(name = "slowness")
@NamedQueries({
    @NamedQuery(name = "Slowness.dashboard", query= "SELECT new org.netbeans.web.action.SlownBean(s.latestAction, 0, MAX(actionTime), AVG(actionTime), COUNT(s.id)) FROM Slowness s WHERE s.latestAction IS NOT NULL GROUP BY s.latestAction ORDER BY s.latestAction"),
    @NamedQuery(name = "Slowness.dashboardByOs", query= "SELECT new org.netbeans.web.action.SlownBean(s.latestAction, s.operatingsystemId.id, MAX(actionTime), AVG(actionTime), COUNT(s.id)) FROM Slowness s WHERE s.latestAction IS NOT NULL GROUP BY s.latestAction, s.operatingsystemId.id ORDER BY s.latestAction"),
    @NamedQuery(name = "Slowness.findByLatestAction", query = "SELECT s FROM Slowness s WHERE s.latestAction = :action"),
    @NamedQuery(name = "Slowness.findReportsByLatestAction", query = "SELECT DISTINCT s.reportId FROM Slowness s WHERE s.latestAction = :action"),
    @NamedQuery(name = "Slowness.findBySuspiciousMethodName", query = "SELECT s FROM Slowness s WHERE s.suspiciousMethod.name = :methodName"),
    @NamedQuery(name = "Slowness.maxBuild", query = "SELECT MAX(s.logfileId.buildnumber) FROM Slowness s WHERE s.reportId = :reportId"),
    @NamedQuery(name = "Slowness.findLatestByReport", query = "SELECT s FROM Slowness s WHERE s.reportId = :report AND s.id >= ALL(SELECT s2.id FROM Slowness s2 WHERE s2.reportId = s.reportId)"),
    @NamedQuery(name = "Slowness.countByReport", query = "SELECT COUNT(s) FROM Slowness s WHERE s.reportId = :reportId"),
    @NamedQuery(name = "Slowness.getMaximalActionTime", query = "SELECT MAX(s.actionTime) FROM Slowness s WHERE s.reportId = :report"),
    @NamedQuery(name = "Slowness.getAVGActionTime", query =     "SELECT AVG(s.actionTime) FROM Slowness s WHERE s.reportId = :report"),
    @NamedQuery(name = "Slowness.duplicatesFromOneUser", query = "SELECT COUNT(s) FROM Slowness s WHERE s.reportId = :reportId AND s.nbuserId = :nbuser"),
    @NamedQuery(name = "Slowness.countAll", query = "SELECT COUNT(s) from Slowness s"),
    @NamedQuery(name = "Slowness.findReportsBySlownessType", query = "SELECT s.reportId FROM Slowness s WHERE s.slownessType = :type ORDER BY s.reportId.id DESC"),
    @NamedQuery(name = "Slowness.findBySlownessType", query = "SELECT s FROM Slowness s WHERE s.slownessType = :type ORDER BY s.reportId.id DESC"),
    @NamedQuery(name = "Slowness.findByUserId", query = "SELECT s FROM Slowness s WHERE s.logfileId.userdir = :id"),
    @NamedQuery(name = "Slowness.findByUserName", query = "SELECT s FROM Slowness s WHERE s.nbuserId.name = :name"),
    @NamedQuery(name = "Slowness.countByInterval", query = "SELECT COUNT(e) FROM Slowness e WHERE e.reportdate BETWEEN :start AND :end"),
    @NamedQuery(name = "Slowness.duplicatesFromDistinctUsersCount", query = "SELECT COUNT(DISTINCT s.nbuserId) FROM Slowness s WHERE s.reportId = :reportId")

})
public class Slowness extends Submit implements Comparable<Slowness>{
    
    @Column(name="ACTION_TIME", nullable=false)
    private int actionTime;
    @Column(name="LATEST_ACTION")
    private String latestAction;
    @Column(name="SLOWNESS_TYPE", length=30)
    private String slownessType;
    @JoinColumn(name = "SUSPICIOUS_METHOD", referencedColumnName = "ID", nullable=false)
    @ManyToOne
    private Method suspiciousMethod;

    public Slowness() {
    }
    
    public Slowness(Integer id) {
        super(id);
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (getId() != null ? getId().hashCode() : 0);
        return hash;
    }
    
    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Slowness)) {
            return false;
        }
        Slowness other = (Slowness) object;
        
        if (this.getId() != other.getId() &&
                (this.getId() == null || !this.getId().equals(other.getId()))) {
            return false;
        }
        return true;
    }
    
    @Override
    public String toString() {
        return "Slowness[id=" + getId() + "]";
    }

    public long getActionTime() {
        return actionTime;
    }

    public void setActionTime(Long time) {
        this.actionTime = time.intValue();
    }

    public String getLatestAction() {
        return latestAction;
    }

    public void setLatestAction(String latestAction) {
        this.latestAction = latestAction;
    }

    @Override
    public String getMessage() {
        return getSuspiciousMethod().getName();
    }

    public Method getSuspiciousMethod() {
        return suspiciousMethod;
    }

    public void setSuspiciousMethod(Method suspiciousMethod) {
        this.suspiciousMethod = suspiciousMethod;
    }

    public int compareTo(Slowness o) {
        return getId().compareTo(o.getId());
    }

    @Override
    public Long getBuild() {
        return getLogfileId().getBuildnumber();
    }

    public int getMaxActionTime(){
        Map<String, Object> param = Collections.singletonMap("report", (Object)getReportId());
        return (Integer) PersistenceUtils.getInstance().executeNamedQuerySingleResult("Slowness.getMaximalActionTime", param);
    }

    public int getAVGActionTime(){
        Map<String, Object> param = Collections.singletonMap("report", (Object)getReportId());
        Double avg = (Double) PersistenceUtils.getInstance().executeNamedQuerySingleResult("Slowness.getAVGActionTime", param);
        return avg.intValue();
    }

    @Override
    public String getTitleName() {
        return "Slowness";
    }

    public String getSlownessType() {
        return slownessType;
    }

    protected void setSlownessType(String slownessType) {
        try{
            SlownessType.valueOf(slownessType);
        }catch(IllegalArgumentException iae){
            Logger.getLogger(Slowness.class.getName()).log(Level.WARNING, "Invalid Slowness Type: " + slownessType, iae);
        }
        this.slownessType = slownessType;
    }

    public SlownessType getSlownessTypeValue() {
        return SlownessType.valueOf(slownessType);
    }

    public void setSlownessType(SlownessType slownessType) {
        if (slownessType != null){
            this.slownessType = slownessType.toString();
        }else{
            this.slownessType = null;
        }
    }

    @Override
    public boolean containsMethod(String methodName) {
        return SnapshotManager.loadSnapshot(getLogfileId()).AWTcontainsMethod(methodName);
    }

    public static enum SlownessType{
        GoToType, CodeCompletion
    }

    public String getAdditionalComment() {
        return "\nMaximum slowness yet reported was " + getMaxActionTime() + " ms, average is " + getAVGActionTime();
    }

    public String getStatusWhiteboard() {
        return "EXCEPTIONS_REPORT perf-profileme";
    }

    @Override
    public String getKeywords() {
        return "PERFORMANCE";
    }

    @Override
    public Attachment getAttachment() {
        byte[] data = null;
        try {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            SnapshotManager sm = SnapshotManager.loadSnapshot(getLogfileId());
            DataOutputStream dos = new DataOutputStream(bos);
            if (sm != null){
                sm.save(dos);
            }
            dos.close();
            data = bos.toByteArray();
        } catch (IOException ex) {
            Exceptions.printStackTrace(ex);
        }
        return new Attachment("snapshot-%1$s.nps", "nps snapshot", "application/nps", data);
    }
}
