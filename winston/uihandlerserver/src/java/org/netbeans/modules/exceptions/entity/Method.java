/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */

package org.netbeans.modules.exceptions.entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 *
 * @author Jan Horvath
 */
@Entity
@Table(name = "method")
@NamedQueries({@NamedQuery(name = "Method.findByName", query = "SELECT m FROM Method m WHERE m.name = :name")})
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE, include="non-lazy")
public class Method implements Serializable {

    @Id
    @Column(name = "ID", nullable = false)
    private Integer id;
    @Column(name = "NAME", unique = true, nullable = false)
    private String name;
    @JoinColumn(name = "SOURCEJAR")
    @ManyToOne
    private Sourcejar sourcejar;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "method")
    private Collection<Line> lineCollection;
    @OneToMany(mappedBy = "suspiciousMethod")
    private Collection<Slowness> slownesss;

    public Method() {
    }

    public Method(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public String getSimpleMethodName(){
        String[] pieces = name.split(".");
        return pieces[pieces.length -1];
    }

    public String getSimpleClassName(){
        String[] pieces = name.split(".");
        return pieces[pieces.length - 2];
    }

    public String getNameWithoutNumbers(){
        return getName().replaceAll("\\d", ""); // NOI18N
    }

    public void setName(String name) {
        if (name.contains("(")){
            throw new IllegalArgumentException("Method name " + name + " contains brackets!");
        }
        this.name = name;
    }

    public Sourcejar getSourcejar() {
        return sourcejar;
    }

    public static String getMethodNameFromSTE(StackTraceElement element){
        String elemClass = element.getClassName();
        return elemClass.concat(".").concat(element.getMethodName());// NOI18N
    }

    public static String getMethodNameFromSTEWithoutNumbers(StackTraceElement element){
        String method = getMethodNameFromSTE(element);
        return method.replaceAll("\\d", ""); // NOI18N
    }

    public void setSourcejar(Sourcejar sourcejar) {
        this.sourcejar = sourcejar;
    }

    public Collection<Line> getLineCollection() {
        return lineCollection;
    }

    public void setLineCollection(Collection<Line> lineCollection) {
        this.lineCollection = lineCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;

        hash += (id != null ? id.hashCode()
                            : 0);
        return hash;
    }

    public StackTraceElement getStackTraceElement() {
        return getStackTraceElement(getName());
    }
    
    public static StackTraceElement getStackTraceElement(String methodName) {
        int index = 0;
        int lastIndex = 0;
        while (index != -1) {
            lastIndex = index;
            index = methodName.indexOf('.', index + 1);
        }
        return new StackTraceElement(methodName.substring(0, lastIndex), methodName.substring(lastIndex + 1), null, 0);
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Method)) {
            return false;
        }
        Method other = (Method) object;

        if (this.id != other.id &&
                (this.id == null || !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.netbeans.modules.exceptions.entity.Method[id=" + id + "]";
    }

    /**
     * @return the slownesss
     */
    public Collection<Slowness> getSlownesss() {
        return slownesss;
    }

    /**
     * @param slownesss the slownesss to set
     */
    public void setSlownesss(Collection<Slowness> slownesss) {
        this.slownesss = slownesss;
    }

}
