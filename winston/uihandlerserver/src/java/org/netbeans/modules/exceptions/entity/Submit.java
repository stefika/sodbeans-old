/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2009 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */
package org.netbeans.modules.exceptions.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import org.netbeans.modules.exceptions.utils.PersistenceUtils;
import org.netbeans.server.uihandler.api.Submit.Attachment;
import org.netbeans.web.Utils;

/**
 * Base class for Exceptions and Slowness
 *
 * @author Jindrich Sedek
 */
@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class Submit implements Serializable {
    private static final int IS_NEWER_OFFSET = 7;
    public static final String[] generatedComponents = {"user.dir", "user.home", "java.home", "netbeans.home"};

    @Id
    @Column(name = "ID", nullable = false)
    private Integer id;
    @Column(name = "SUMMARY")
    private String summary;
    @Column(name = "REPORTDATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date reportdate;
    @JoinColumn(name = "NBUSER_ID", referencedColumnName = "ID")
    @ManyToOne
    private Nbuser nbuserId;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "submitId", fetch=FetchType.LAZY)
    private Collection<Comment> commentCollection;
    @JoinColumn(name = "LOGFILE_ID", referencedColumnName = "ID")
    @OneToOne(fetch=FetchType.LAZY)
    private Logfile logfileId;
    @JoinColumn(name = "REPORT_ID", referencedColumnName = "ID", nullable = false)
    @ManyToOne(fetch=FetchType.LAZY)
    private Report reportId;
    @JoinColumn(name = "VM_ID", referencedColumnName = "ID")
    @ManyToOne(fetch=FetchType.LAZY)
    private Vm vmId;
    @JoinColumn(name = "OPERATINGSYSTEM_ID", referencedColumnName = "ID")
    @ManyToOne(fetch=FetchType.LAZY)
    private Operatingsystem operatingsystemId;

    public Submit() {
        
    }
    public Submit(int id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public Date getReportdate() {
        return reportdate;
    }

    public void setReportdate(Date reportdate) {
        this.reportdate = reportdate;
    }


    public Collection<Comment> getCommentCollection() {
        return commentCollection;
    }

    public void setCommentCollection(Collection<Comment> commentCollection) {
        this.commentCollection = commentCollection;
    }

    public Logfile getLogfileId() {
        return logfileId;
    }

    public void setLogfileId(Logfile logfileId) {
        this.logfileId = logfileId;
    }

    public Nbuser getNbuserId() {
        return nbuserId;
    }

    public void setNbuserId(Nbuser nbuserId) {
        this.nbuserId = nbuserId;
    }

    public Report getReportId() {
        return reportId;
    }

    public void setReportId(Report reportId) {
        this.reportId = reportId;
    }

    public Vm getVmId() {
        return vmId;
    }

    public void setVmId(Vm vmId) {
        this.vmId = vmId;
    }

    public Operatingsystem getOperatingsystemId() {
        return operatingsystemId;
    }

    public void setOperatingsystemId(Operatingsystem operatingsystemId) {
        this.operatingsystemId = operatingsystemId;
    }

    public String getOrderStr(){
        return Utils.getFixedLengthString(getId());
    }

    public boolean getHasUILog(){
        return getLogfileId().getLoggerFile().exists();
    }

    public boolean getHasMessagesLog(){
        return getLogfileId().getMessagesFile().exists();
    }

    public boolean getHasSlownessLog(){
        return getLogfileId().getNPSFile().exists();
    }

    public boolean getHasHeapDumpLog(){
        return getLogfileId().getHeapDumpFile().exists();
    }

    public long getLatestBuild() {
        return getReportId().getLatestBuild();
    }

    public static Submit getById(EntityManager em, int id){ // em.find(Submit.class, id) is pretty slow!!!
        Submit submit = em.find(Exceptions.class, id);
        if (submit == null){
            submit = em.find(Slowness.class, id);
        }
        return submit;
    }

    public boolean isBuildNewerThan(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DAY_OF_MONTH, IS_NEWER_OFFSET);
        int year = cal.get(Calendar.YEAR) - 2000;
        int month = cal.get(Calendar.MONTH) + 1;
        int day = cal.get(Calendar.DAY_OF_MONTH);
        long buildNumberOfDate = year * 10000 + month * 100 + day;
        return (getBuild() > buildNumberOfDate);
    }

    // api.Submit implementation
    @Transient
    private transient SubmitImpl submitImpl;
    public org.netbeans.server.uihandler.api.Submit getSubmit(EntityManager em){
        if (submitImpl == null){
            submitImpl = new SubmitImpl(em);
        }
        return submitImpl;
    }

    public boolean getIsException(){
        return this instanceof Exceptions;
    }

    private class SubmitImpl implements org.netbeans.server.uihandler.api.Submit {

        private Reporter reporter = null;

        public SubmitImpl(EntityManager em) {
            if (reporter == null){
                Nbuser user = getNbuserId();
                Directuser directuser = PersistenceUtils.getDirectuser(em, user.getName());
                if (directuser != null) {
                    reporter = new Reporter(getNbuserId().getName(), true, directuser.getMessagePrefix());
                } else {
                    reporter = new Reporter(getNbuserId().getName(), false, null);
                }
            }
        }
        
        public String getCommentString() {
            Collection<Comment> commentCol = getCommentCollection();
            if (commentCol != null && !commentCol.isEmpty()) {
                StringBuilder comments = new StringBuilder();
                Iterator<Comment> it = commentCol.iterator();
                while (it.hasNext()) {
                    Comment comment = it.next();
                    comments.append(comment.getComment().trim());
                    if (it.hasNext()) {
                        comments.append("\n");
                    }
                }
                if (comments.length() > 0) {
                    return comments.toString();
                }
            }
            return null;
        }

        public Collection<org.netbeans.server.uihandler.api.Submit> getDuplicates() {
            Collection<Submit> data = Submit.this.getReportId().getSubmitCollection();
            Collection<org.netbeans.server.uihandler.api.Submit> result = new ArrayList<org.netbeans.server.uihandler.api.Submit>();
            for (Submit submit : data) {
                result.add(submit.getSubmit(null));
            }
            return result;
        }

        public String getProductVersion() {
            return getLogfileId().getProductVersionId().getProductVersion();
        }

        public String getOS() {
            return getOperatingsystemId().getName();
        }

        public String getVM() {
            return getVmId().getName();
        }

        public String getBugzillaVersion() {
            Nbversion nbversion = getLogfileId().getProductVersionId().getNbversionId();
            return org.netbeans.modules.exceptions.utils.Utils.getBugzillaVersion(nbversion);
        }

        public boolean isInBugzilla() {
            return Submit.this.getReportId().isInIssuezilla();
        }

        public int getBugzillaId() {
            return Submit.this.getReportId().getIssueId();
        }

        public Reporter getReporter() {
            return reporter;
        }

        public String getAdditionalComment() {
            return Submit.this.getAdditionalComment();
        }

        public String getComponent() {
            return Submit.this.getReportId().getComponent();
        }

        public String getSubcomponent() {
            return Submit.this.getReportId().getSubcomponent();
        }

        public int getId() {
            return Submit.this.getId();
        }

        public String getSummary() {
            String summary = Submit.this.getSummary();
            if ((summary == null) || summary.trim().isEmpty()){
                return getMessage();
            }else{
                return summary;
            }
        }

        public int getReportId() {
            return Submit.this.getReportId().getId();
        }

        public String getStatusWhiteboard() {
            return Submit.this.getStatusWhiteboard();
        }

        public String getKeywords() {
            return Submit.this.getKeywords();
        }

        public Attachment getAttachment() {
            return Submit.this.getAttachment();
        }
    }
    
    public abstract Long getBuild();
    public abstract String getMessage();
    public abstract String getTitleName();
    public abstract String getAdditionalComment();
    public abstract String getStatusWhiteboard();
    public abstract String getKeywords();
    public abstract Attachment getAttachment();
    public abstract boolean containsMethod(String methodName);
}
