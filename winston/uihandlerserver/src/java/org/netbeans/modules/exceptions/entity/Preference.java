/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */
package org.netbeans.modules.exceptions.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.netbeans.server.uihandler.Utils;

/** Represents one item in a preference.
 *
 * @author Jaroslav Tulach
 */
@Entity
@Table(name = "PREFERENCES")
@NamedQueries({
@NamedQuery(name = "Preference.findWithoutKey", query = "SELECT p FROM Preference p WHERE p.statistic = :statistic AND p.prefix = :prefix AND p.logfile = :logfile"),
@NamedQuery(name = "Preference.findWithoutKeyNullPrefix", query = "SELECT p FROM Preference p WHERE p.statistic = :statistic AND p.prefix IS NULL AND p.logfile = :logfile"),
@NamedQuery(name = "Preference.findAllPrefixes", query = "SELECT p FROM Preference p WHERE p.statistic = :statistic AND p.prefix.name LIKE :prefix AND p.logfile = :logfile")
})
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE, include="non-lazy")
public class Preference implements Serializable {
    @Id
    @Column(name = "ID", nullable = false)
    private Integer id;
    @JoinColumn(name = "PREFIX", referencedColumnName = "ID")
    @ManyToOne(fetch=FetchType.EAGER)
    private Prefname prefix;
    @Column(name = "DIRECT_VALUE")
    private Integer directValue;
    @JoinColumn(name = "LOGFILE", referencedColumnName = "ID")
    @ManyToOne(fetch=FetchType.LAZY)
    private Logfile logfile;
    @JoinColumn(name = "NAME", referencedColumnName = "ID")
    @ManyToOne(fetch=FetchType.EAGER)
    private Prefname name;
    @JoinColumn(name = "VALUE", referencedColumnName = "ID")
    @ManyToOne(fetch=FetchType.EAGER)
    private Prefvalue value;
    @JoinColumn(name = "STATISTIC", referencedColumnName = "ID")
    @ManyToOne(fetch=FetchType.LAZY)
    private Statistic statistic;

    @Deprecated
    public Preference() {
    }

    public Preference(EntityManager em, Statistic statistic, Logfile log, String prefix, String name) {
        this.id = Utils.getNextId(Preference.class);
        this.logfile = log;
        this.statistic = statistic;
        this.setName(em, name);
        this.setPrefix(em, prefix);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Prefname getPrefix() {
        return prefix;
    }

    public void setPrefix(Prefname prefix) {
        this.prefix = prefix;
    }

    public void setPrefix(EntityManager em, String prefix) {
        if (prefix != null){
            Prefname prefixName = Prefname.getExistsOrNew(em, prefix);
            this.prefix = prefixName;
        }else{
            this.prefix = null;
        }
    }

    public Logfile getLogfile() {
        return logfile;
    }

    public void setLogfile(Logfile logfile) {
        this.logfile = logfile;
    }

    public String getKey() {
        return name.getName();
    }

    public Prefname getName() {
        return name;
    }

    public void setName(EntityManager em, String name) {
        Prefname prefName = Prefname.getExistsOrNew(em, name);
        this.name = prefName;
    }

    public String getValue() {
        if (directValue != null){
            return String.valueOf(directValue);
        }else{
            return value.getValue();
        }
    }
    
    public void setValue(EntityManager em, String value) {
        try{
            this.directValue = Integer.parseInt(value);
            this.value = null;
        }catch(NumberFormatException exc){
            Prefvalue prefValue = Prefvalue.getExistsOrNew(em, value);
            this.value = prefValue;
            this.directValue = null;
        }
    }

    public Statistic getStatistic() {
        return statistic;
    }

    public void setStatistic(String statistic) {
        Statistic stats = Statistic.getExists(statistic);
        this.statistic = stats;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Preference)) {
            return false;
        }
        Preference other = (Preference) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.netbeans.exceptions.entites.Preferences[id=" + id + "]";
    }

    public Integer getDirectValue() {
        return directValue;
    }

    public void setDirectValue(Integer directValue) {
        this.directValue = directValue;
        this.value = null;
    }
}
