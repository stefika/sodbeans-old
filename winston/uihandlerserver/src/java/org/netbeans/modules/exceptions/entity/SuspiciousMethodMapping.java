/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2007 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */
package org.netbeans.modules.exceptions.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import org.netbeans.modules.exceptions.utils.PersistenceUtils;

/**
 *
 * @author Jindrich Sedek
 */
@Table(name = "SUSPICIOUS_METHOD_MAPPING")
@Entity
public class SuspiciousMethodMapping implements Serializable {

    @Id
    @Column(name = "ORIGINAL_METHOD_NAME", nullable = false)
    private String originalMethodName;

    @Column(name = "MAPPED_METHOD_NAME", nullable = true)
    private String mappedMethodName;

    public SuspiciousMethodMapping() {
    }

    public SuspiciousMethodMapping(String originalMethodName, String mappedMethodName) {
        this.originalMethodName = originalMethodName;
        this.mappedMethodName = mappedMethodName;
    }

    public String getMappedMethodName() {
        return mappedMethodName;
    }

    public void setMappedMethodName(String mappedMethodName) {
        this.mappedMethodName = mappedMethodName;
    }

    public String getOriginalMethodName() {
        return originalMethodName;
    }

    public void setOriginalMethodName(String originalMethodName) {
        this.originalMethodName = originalMethodName;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SuspiciousMethodMapping other = (SuspiciousMethodMapping) obj;
        if ((this.originalMethodName == null) ? (other.originalMethodName != null) : !this.originalMethodName.equals(other.originalMethodName)) {
            return false;
        }
        if ((this.mappedMethodName == null) ? (other.mappedMethodName != null) : !this.mappedMethodName.equals(other.mappedMethodName)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + (this.originalMethodName != null ? this.originalMethodName.hashCode() : 0);
        hash = 89 * hash + (this.mappedMethodName != null ? this.mappedMethodName.hashCode() : 0);
        return hash;
    }

    public static SuspiciousMethodMapping getMappingFor(EntityManager em, String methodName) {
        SuspiciousMethodMapping candidate = null;
        int matchedLength = 0;
        List<SuspiciousMethodMapping> all = PersistenceUtils.getAll(em, SuspiciousMethodMapping.class);
        for (SuspiciousMethodMapping suspiciousMethodMapping : all) {
            String originalName = suspiciousMethodMapping.getOriginalMethodName();
            if (methodName.contains(originalName) && (originalName.length() > matchedLength)){
                matchedLength = originalName.length();
                candidate = suspiciousMethodMapping;
            }
        }
        return candidate;
    }

}