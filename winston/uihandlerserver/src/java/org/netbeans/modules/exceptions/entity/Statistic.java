/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */
package org.netbeans.modules.exceptions.entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.netbeans.modules.exceptions.utils.PersistenceUtils;
import org.netbeans.server.uihandler.Utils;

/**
 *
 * @author Jindrich Sedek
 */
@Entity
@Table(name = "statistic")
@NamedQueries({
@NamedQuery(name = "Statistic.findByStatname", query = "SELECT s FROM Statistic s WHERE s.statname = :name")
})
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE, include="non-lazy")
public class Statistic implements Serializable {
    private static final Map<String, Statistic> statisticCache = new HashMap<String, Statistic>(20);
    
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "ID", nullable = false)
    private Integer id;
    @Column(name = "STATNAME", nullable = false, unique= true, updatable=false)
    private String statname;
    @OneToMany(mappedBy = "statistic", fetch=FetchType.LAZY)
    private Collection<Preference> preferencesCollection;
    @OneToMany(mappedBy = "statistic", fetch=FetchType.LAZY)
    private Collection<LogfileParsed> logfileparsedCollection;
    @OneToMany(mappedBy = "statistic", fetch=FetchType.LAZY)
    private Collection<Prefset> prefsetCollection;

    public Statistic() {
    }

    public Statistic(String statistic) {
        this.id = Utils.getNextId(Statistic.class);
        this.statname = statistic;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getStatname() {
        return statname;
    }

    public void setStatname(String statname) {
        this.statname = statname;
    }

    public static void loadIntoCache(String name) {
        PersistenceUtils pUtils = PersistenceUtils.getInstance();
        Statistic stat = (Statistic) pUtils.getExist("Statistic.findByStatname", name);
        if (stat == null) {
            stat = new Statistic(name);
            pUtils.persist(stat);
        }
        statisticCache.put(name, stat);
    }

    public static Statistic getExists(String statistic) {
        return statisticCache.get(statistic);
    }
    
    public Collection<Preference> getPreferencesCollection() {
        return preferencesCollection;
    }

    public Collection<Prefset> getPrefsetCollection() {
        return prefsetCollection;
    }

    public void setPrefsetCollection(Collection<Prefset> prefsetCollection) {
        this.prefsetCollection = prefsetCollection;
    }

    public void setPreferencesCollection(Collection<Preference> preferencesCollection) {
        this.preferencesCollection = preferencesCollection;
    }

    public Collection<LogfileParsed> getLogfileparsedCollection() {
        return logfileparsedCollection;
    }

    public void setLogfileparsedCollection(Collection<LogfileParsed> logfileparsedCollection) {
        this.logfileparsedCollection = logfileparsedCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Statistic)) {
            return false;
        }
        Statistic other = (Statistic) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.netbeans.modules.exceptions.entity.Statistic[id=" + id + "]";
    }
    
    public static void dropCache(){
        statisticCache.clear();
    }
}
