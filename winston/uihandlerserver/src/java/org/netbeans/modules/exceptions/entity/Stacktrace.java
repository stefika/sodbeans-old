/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */

package org.netbeans.modules.exceptions.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.netbeans.modules.exceptions.utils.LineComparator;

/**
 *
 * @author Jan Horvath
 */
@Entity
@Table(name = "stacktrace")
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE, include="non-lazy")
@NamedQueries({@NamedQuery(name = "Stacktrace.findByMessage", query = "SELECT s FROM Stacktrace s WHERE s.message = :message"),
        @NamedQuery(name = "Stacktrace.findByClass1", query = "SELECT s FROM Stacktrace s WHERE s.class1 = :class1")})
public class Stacktrace implements Serializable {
    @Id
    @Column(name = "ID", nullable = false)
    private Integer id;
    @Column(name = "message", length = 1024)
    private String message;
    @Column(name = "CLASS")
    private String class1;
    @OneToOne(mappedBy = "stacktrace", fetch=FetchType.LAZY)
    private Exceptions exceptions;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "stacktrace", fetch=FetchType.LAZY)
    private Collection<Line> lineCollection;
    @OneToOne(mappedBy = "annotation", fetch=FetchType.LAZY)
    private Stacktrace stacktrace;
    @JoinColumn(name = "ANNOTATION", referencedColumnName = "ID")
    @OneToOne(fetch=FetchType.LAZY)
    private Stacktrace annotation;

    public Stacktrace() {
    }

    public Stacktrace(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getClass1() {
        return class1;
    }

    public void setClass1(String class1) {
        this.class1 = class1;
    }

    public Exceptions getExceptions() {
        return exceptions;
    }

    public void setExceptions(Exceptions exceptions) {
        this.exceptions = exceptions;
    }

    public Collection<Line> getLineCollection() {
        return lineCollection;
    }

    public List<Line> getSortedLines(){
        List<Line> lines = new ArrayList<Line>(getLineCollection());
        Collections.sort(lines, new LineComparator());
        return lines;
    }

    public void setLineCollection(Collection<Line> lineCollection) {
        this.lineCollection = lineCollection;
    }

    public Stacktrace getStacktrace() {
        return stacktrace;
    }

    public void setStacktrace(Stacktrace stacktrace) {
        this.stacktrace = stacktrace;
    }

    public Stacktrace getAnnotation() {
        return annotation;
    }

    public void setAnnotation(Stacktrace annotation) {
        this.annotation = annotation;
    }

    @Override
    public int hashCode() {
        int hash = 0;

        hash += (id != null ? id.hashCode()
                            : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Stacktrace)) {
            return false;
        }
        Stacktrace other = (Stacktrace) object;

        if (this.id != other.id &&
                (this.id == null || !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.netbeans.modules.exceptions.entity.Stacktrace[id=" + id +
               "]";
    }

}
