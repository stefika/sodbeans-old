/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2008 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */
package org.netbeans.modules.exceptions.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.netbeans.modules.exceptions.utils.PersistenceUtils;
import org.netbeans.server.componentsmatch.Component;

/**
 *
 * @author honza
 */
@Entity
@Table(name = "report", schema = "")
@NamedQueries({
@NamedQuery(name = "Report.findAll", query = "SELECT r FROM Report r"),
@NamedQuery(name = "Report.findAllInIZ", query = "SELECT r FROM Report r WHERE r.issueId IS NOT NULL AND r.issueId != 0"),
@NamedQuery(name = "Report.findByID", query = "SELECT r FROM Report r WHERE r.id = :reportId"),
@NamedQuery(name = "Report.findByIssueId", query = "SELECT r FROM Report r WHERE r.issueId = :issueId ORDER BY r.id"),
@NamedQuery(name = "Report.findByLikeLine", query = "SELECT DISTINCT e.reportId FROM Exceptions e, Line l WHERE e.stacktrace = l.stacktrace AND l.method.name LIKE :line"),
@NamedQuery(name = "Report.VMfromExc", query = "select distinct e.vmId.shortName from Exceptions e where e.reportId = :report"),
@NamedQuery(name = "Report.VMfromSlow", query = "select distinct s.vmId.shortName from Slowness s where s.reportId = :report"),
@NamedQuery(name = "Report.OSfromExc", query = "select distinct e.operatingsystemId.name from Exceptions e where e.reportId = :report"),
@NamedQuery(name = "Report.OSfromSlow", query = "select distinct s.operatingsystemId.name from Slowness s where s.reportId = :report"),
@NamedQuery(name = "Report.VersionfromExc", query = "select distinct e.logfileId.productVersionId.nbversionId.version from Exceptions e where e.reportId = :report"),
@NamedQuery(name = "Report.GetSlownesses", query = "select s from Slowness s where s.reportId = :report ORDER BY s.id DESC"),
@NamedQuery(name = "Report.GetExcs", query = "select e from Exceptions e where e.reportId = :report ORDER BY e.id DESC"),
@NamedQuery(name = "Report.VersionfromSlow", query = "select distinct s.logfileId.productVersionId.nbversionId.version from Slowness s where s.reportId = :report")
})
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE, include="non-lazy")
public class Report implements Serializable {
    private static final long serialVersionUID = 1L;
    static final Logger ISSUE_CHANGES_LOGGER = Logger.getLogger("org.netbeans.modules.exceptions.entity.Report.IssueChanges");
    
    public enum Status {
        MAN_COMPONENT_CHANGE(1),
        MAN_ISSUE_CHANGE(2),
        MAN_DUPLICATE_CHANGE(4),
        ISSUEZILLA_TRANSFER(8);

        private final int status;
        Status(int status) {
            this.status = status;
        }
        public int intValue() {
            return status;
        }

        public static EnumSet<Status> getEnumSet(Integer status) {
            EnumSet<Status> set = EnumSet.noneOf(Status.class);
            if (status != null) {
                int i = status.intValue();
                if (testStatus(i, MAN_COMPONENT_CHANGE)) {
                    set.add(MAN_COMPONENT_CHANGE);
                }
                if (testStatus(i, MAN_ISSUE_CHANGE)) {
                    set.add(MAN_ISSUE_CHANGE);
                }
                if (testStatus(i, MAN_DUPLICATE_CHANGE)) {
                    set.add(MAN_DUPLICATE_CHANGE);
                }
                if (testStatus(i, ISSUEZILLA_TRANSFER)) {
                    set.add(ISSUEZILLA_TRANSFER);
                }
            }
            return set;
        }

        public static Integer getInteger(EnumSet<Status> set) {
            int i = 0;
            if (set != null) {
                for (Iterator<Report.Status> it = set.iterator(); it.hasNext();) {
                    Report.Status status = it.next();
                    i |= status.intValue();
                }
            }
            return new Integer(i);
        }

        private static boolean testStatus(int i, Status s) {
            return (i & s.intValue()) == s.intValue();
        }

    }

    @Id
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @Column(name = "COMPONENT")
    private String component;
    @Column(name = "SUBCOMPONENT")
    private String subcomponent;
    @Column(name = "ISSUE_ID")
    private Integer issueId;
    @Column(name = "STATUS")
    private Integer status;
    @OneToMany(mappedBy = "reportId", fetch=FetchType.LAZY)
    private List<Submit> submitCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "reportId", fetch=FetchType.LAZY)
    private Collection<ReportComment> commentCollection;

    @Transient
    private transient EnumSet<Status> statusEnumSet;
    @Transient
    private transient long latestBuild = -1;
    @Transient
    private transient Integer duplicatesCount = null;
    @Transient
    private transient boolean isPreloaded = false;
    
    public Report() {
    }

    public Report(Integer id) {
        this.id = id;
    }

    //    @PostLoad
    public void loadStatus() {
        statusEnumSet = Status.getEnumSet(getStatus());
    }

//    @PreUpdate
//    @PrePersist
    public void saveStatus() {
        setStatus(Status.getInteger(statusEnumSet));
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getComponent() {
        return component;
    }

    public void setComponent(String component) {
        this.component = component;
    }

    public String getSubcomponent() {
        return subcomponent;
    }

    public void setSubcomponent(String subcomponent) {
        this.subcomponent = subcomponent;
    }

    public Integer getIssueId() {
        return issueId;
    }

    public void setIssueId(Integer issueId) {
        this.issueId = issueId;
    }

    public void setIssueId(Integer issueId, Class changer) {
        if ((this.issueId != null) && (this.issueId.equals(issueId))) {
            return;
        }
        ISSUE_CHANGES_LOGGER.log(Level.INFO, 
                "Report id {0} issueId was changed from {1} to {2} by {3}",
                new Object[]{getId(), getIssueId(), issueId, changer.getSimpleName()});
        setIssueId(issueId);
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public List<Submit> getSubmitCollection() {
        assert isPreloaded;
        return submitCollection;
    }

    // TODO fix this performance HACK!!!
    public List<Submit> preloadSubmitCollection(EntityManager em) {
        isPreloaded = true;
        ArrayList<Submit> result = new ArrayList<Submit>();
        result.addAll(getExceptionsCollection(em));
        result.addAll(getSlownessCollection(em));
        setSubmitCollection(result);
        return result;
    }

    public List<Exceptions> getExceptionsCollection(EntityManager em){
        return PersistenceUtils.executeNamedQuery(em, "Report.GetExcs", Collections.singletonMap("report", this), Exceptions.class);
    }

    public List<Slowness> getSlownessCollection(EntityManager em){
        return PersistenceUtils.executeNamedQuery(em, "Report.GetSlownesses", Collections.singletonMap("report", this), Slowness.class);
    }

    public void setSubmitCollection(List<Submit> submitCollection) {
        this.submitCollection = submitCollection;
    }

    public Collection<ReportComment> getCommentCollection() {
        return commentCollection;
    }

    public void setCommentCollection(Collection<ReportComment> commentCollection) {
        this.commentCollection = commentCollection;
    }

    public boolean isInIssuezilla() {
        if ((this.getIssueId() != null) && (this.getIssueId() != 0)) {
            return true;
        }
        return false;
    }

    public long getLatestBuild() {
        if (latestBuild < 0) {
            Map<String, Object> param = Collections.singletonMap("reportId", (Object) this);
            Long excResult =(Long) PersistenceUtils.getInstance().executeNamedQuerySingleResult("Exceptions.maxBuild", param);
            long exc = excResult == null ? 0 :excResult.longValue();
            Long slownResult =(Long) PersistenceUtils.getInstance().executeNamedQuerySingleResult("Slowness.maxBuild", param);
            long slown = slownResult == null ? 0 : slownResult.longValue();
            latestBuild = Math.max(exc, slown);
        }
        return latestBuild;
    }

    public void setComponent(Component comp){
        this.component = comp.getComponent();
        this.subcomponent = comp.getSubComponent();
    }
    
    public boolean getComponentManChanged() {
        loadStatus();
        return statusEnumSet.contains(Status.MAN_COMPONENT_CHANGE);
    }

    public void setComponentManChanged(boolean it) {
        loadStatus();
        setStatusEnum(Status.MAN_COMPONENT_CHANGE, it);
        saveStatus();
    }

    public boolean getIssueManChanged() {
        loadStatus();
        return statusEnumSet.contains(Status.MAN_ISSUE_CHANGE);
    }

    public void setIssueManChanged(boolean it) {
        loadStatus();
        setStatusEnum(Status.MAN_ISSUE_CHANGE, it);
        saveStatus();
    }

    public boolean getDuplicateManChanged() {
        loadStatus();
        return statusEnumSet.contains(Status.MAN_DUPLICATE_CHANGE);
    }

    public void setDuplicateManChanged(boolean it) {
        loadStatus();
        setStatusEnum(Status.MAN_DUPLICATE_CHANGE, it);
        saveStatus();
    }

    public boolean inIssuezillaTransfer() {
        loadStatus();
        return statusEnumSet.contains(Status.ISSUEZILLA_TRANSFER);
    }

    public void setIssuezillaTransfer(boolean it) {
        loadStatus();
        setStatusEnum(Status.ISSUEZILLA_TRANSFER, it);
        saveStatus();
    }

    private void setStatusEnum(Status status, boolean it) {
        if (it) {
            statusEnumSet.add(status);
        } else {
            statusEnumSet.remove(status);
        }
    }

    public int getDuplicates(EntityManager em) {
        if (duplicatesCount == null) {
            Map<String, Object> param = Collections.singletonMap("reportId", (Object) this);
            Long excs =(Long) PersistenceUtils.executeNamedQuerySingleResult(em, "Exceptions.countByReport", param);
            Long slowns =(Long) PersistenceUtils.executeNamedQuerySingleResult(em, "Slowness.countByReport", param);
            duplicatesCount = Math.max(excs.intValue(), slowns.intValue());
        }
        return duplicatesCount;
    }

    public int getDuplicatesFromDistinctUsers(EntityManager em){
        Map<String, Object> param = Collections.singletonMap("reportId", (Object) this);
        Long excs =(Long) PersistenceUtils.executeNamedQuerySingleResult(em, "Exceptions.duplicatesFromDistinctUsersCount", param);
        Long slowns =(Long) PersistenceUtils.executeNamedQuerySingleResult(em, "Slowness.duplicatesFromDistinctUsersCount", param);
        return Math.max(excs.intValue(), slowns.intValue());
    }

    public int getDuplicatesFromOneUser(EntityManager em, Nbuser user) {
        Map<String, Object> param = new HashMap<String, Object>();
        param.put("nbuser", user);
        param.put("reportId",  this);
        Long excs =(Long) PersistenceUtils.executeNamedQuerySingleResult(em, "Exceptions.duplicatesFromOneUser", param);
        Long slowns =(Long) PersistenceUtils.executeNamedQuerySingleResult(em, "Slowness.duplicatesFromOneUser", param);
        return Math.max(excs.intValue(), slowns.intValue());
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (getId() != null ? getId().hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Report)) {
            return false;
        }
        Report other = (Report) object;
        if ((getId() == null && other.getId() != null) || (this.getId() != null && !this.getId().equals(other.getId()))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "test.Report[id=" + getId() + "]";
    }

    public List<String> getJDKS(EntityManager em){
        return mergeQueries(em, "Report.VMfromExc", "Report.VMfromSlow");
    }

    public List<String> getOSes(EntityManager em){
        return mergeQueries(em, "Report.OSfromExc", "Report.OSfromSlow");
    }

    public List<String> getVersions(EntityManager em){
        return mergeQueries(em, "Report.VersionfromExc", "Report.VersionfromSlow");
    }

    private List<String> mergeQueries(EntityManager em, String q1Name, String q2Name){
        Map<String, Report> param = Collections.singletonMap("report", this);
        List<String> fromQ1 = PersistenceUtils.executeNamedQuery(em, q1Name, param, String.class);
        List<String> fromQ2 = PersistenceUtils.executeNamedQuery(em, q2Name, param, String.class);
        List<String> result = new ArrayList<String>(fromQ1);
        result.addAll(fromQ2);
        return result;
    }

    public boolean isSlownessReport(){
        List<Submit> submits = getSubmitCollection();
        if (submits.size() > 0){
            return submits.iterator().next() instanceof Slowness;
        } else {
            return false;
        }
    }
}
