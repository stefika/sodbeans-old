/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */
package org.netbeans.modules.exceptions.entity;

import java.io.File;
import java.io.Serializable;
import java.util.Date;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.netbeans.server.snapshots.ProfilerData;
import org.netbeans.server.uihandler.DbPreferences;
import org.netbeans.server.uihandler.LogFileTask;
import org.netbeans.server.uihandler.Utils;
import org.netbeans.server.uihandler.statistics.CPUInfo;
import org.netbeans.server.uihandler.statistics.Memory;
import org.netbeans.server.uihandler.statistics.SnapshotStatistics;
import org.netbeans.server.uihandler.statistics.SnapshotStatistics.SnapshotData;

/**
 *
 * @author Jan Horvath
 */
@Entity
@Table(name = "logfile")
@NamedQueries({@NamedQuery(name = "Logfile.findByIp", query = "SELECT l FROM Logfile l WHERE l.ip = :ip"),
@NamedQuery(name = "Logfile.findByBuildnumber", query = "SELECT l FROM Logfile l WHERE l.buildnumber = :buildnumber"),
@NamedQuery(name = "Logfile.findByUserdir", query = "SELECT l FROM Logfile l WHERE l.userdir = :userdir"),
@NamedQuery(name = "Logfile.findByUploadNumber", query = "SELECT l FROM Logfile l WHERE l.uploadNumber = :uploadNumber"),
@NamedQuery(name = "Logfile.findByDateCreated", query = "SELECT l FROM Logfile l WHERE l.dateCreated = :dateCreated"),
@NamedQuery(name = "Logfile.findByFile", query = "SELECT l FROM Logfile l WHERE l.userdir = :userdir AND l.uploadNumber = :uploadnumber")
})
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE, include="non-lazy")
public class Logfile implements Serializable {
    @Id
    @Column(name = "ID", nullable = false)
    private Integer id;
    @Column(name = "IP")
    private Long ip;
    @Column(name = "BUILDNUMBER")
    private Long buildnumber;
    @Column(name = "USERDIR", nullable=false)
    private String userdir;
    @Column(name = "UPLOAD_NUMBER", nullable=false)
    private Integer uploadNumber;
    @Column(name = "CHANGESET", nullable=true)
    private Long changeset;
    @Column(name = "DATE_CREATED")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreated;
    @JoinColumn(name = "PRODUCT_VERSION_ID", referencedColumnName = "ID")
    @ManyToOne(fetch=FetchType.LAZY)
    private ProductVersion productVersionId;
    @JoinColumn(name = "NBUSER_ID", referencedColumnName = "ID")
    @ManyToOne(fetch=FetchType.LAZY)
    private Nbuser nbuser;
    
    public Logfile() {
    }
    
    public Logfile(Integer id) {
        this.id = id;
    }
    
    public Logfile(String userdir, int uploadNumber){
        this.id = Utils.getNextId(Logfile.class);
        this.userdir = userdir;
        this.uploadNumber = uploadNumber;
    }
    
    public Integer getId() {
        return id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }
    
    public Long getIp() {
        return ip;
    }
    
    public String getIpAddr(){
        String result = Long.toString(ip % 1000);
        long pom = ip / 1000;
        while (pom != 0){
            result = Long.toString(pom % 1000).concat("."+result);
            pom = pom / 1000;
        }
        return result;
    }
    
    public void setIp(Long ip) {
        this.ip = ip;
    }
    
    public void setIpAddr(String address){
        int index = address.indexOf(',');
        if (index > 0){
            address = address.substring(0, index);
        }
        String[] numbers = address.split("\\.");
        long ipValue = 0;
        for (int i = 0; i < numbers.length; i++){
            Integer value = new Integer(numbers[i]);
            ipValue = ipValue*1000+value;
        }
        this.ip = ipValue;
    }
    
    public Long getBuildnumber() {
        return buildnumber;
    }
    
    public void setBuildnumber(Long buildnumber) {
        this.buildnumber = buildnumber;
    }
    
    public String getUserdir() {
        return userdir;
    }
    
    public void setUserdir(String userdir) {
        this.userdir = userdir;
    }
    
    public Integer getUploadNumber() {
        return uploadNumber;
    }
    
    public void setUploadNumber(Integer uploadNumber) {
        this.uploadNumber = uploadNumber;
    }
    
    public String getFileName() {
        if (userdir == null) {
            return null;
        }
        if ((uploadNumber == null) || (uploadNumber == 0)) {
            return userdir;
        } else {
            return userdir + "." + Integer.toString(uploadNumber - 1);
        }
    }
    
    public Date getDateCreated() {
        return dateCreated;
    }
    
    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }
    
    public ProductVersion getProductVersionId() {
        return productVersionId;
    }
    
    public void setProductVersionId(ProductVersion productVersionId) {
        this.productVersionId = productVersionId;
    }

    public Long getChangeset() {
        return changeset;
    }

    public void setChangeset(Long changeset) {
        this.changeset = changeset;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null
                ? id.hashCode(): 0);
        return hash;
    }
    
    @Override
    public boolean equals(Object object) {
        /*
         * TODO: Warning - this method won't work in the case the id fields are not set
         */
        if (!(object instanceof Logfile)) {
            return false;
        }
        Logfile other = (Logfile) object;
        if (this.id != other.id && (this.id == null || !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }
    
    @Override
    public String toString() {
        return "org.netbeans.modules.exceptions.entity.Logfile[id=" + id + "]";
    }

    /**
     * @return the nbuser
     */
    public Nbuser getNbuser() {
        return nbuser;
    }

    /**
     * @param nbuser the nbuser to set
     */
    public void setNbuser(Nbuser nbuser) {
        this.nbuser = nbuser;
    }

    public File getLoggerFile(){
        String dirPath = Utils.getUploadDirPath(Utils.getRootUploadDirPath(), getUserdir());
        return new File(dirPath, getFileName());
    }

    public File getMessagesFile(){
        String dirPath = Utils.getUploadDirPath(Utils.getMessagesRootUploadDir(), getUserdir());
        return new File(dirPath, getFileName());
    }

    public File getNPSFile(){
        String dirPath = Utils.getUploadDirPath(Utils.getSlownessRootUploadDir(), getUserdir());
        return new File(dirPath, getFileName());
    }

    public File getHeapDumpFile(){
        String dirPath = Utils.getUploadDirPath(Utils.getHeapRootUploadDir(), getUserdir());
        return new File(dirPath, getFileName());
    }

    public Integer getUserMemory(EntityManager em) {
        Memory memory = new Memory();
        Preferences pref = DbPreferences.root(this, memory, em);
        try {
            Map<Long, Integer> result = memory.read(pref);
            if (!result.isEmpty()){
                Long value = result.keySet().iterator().next();
                value = value / 1024 / 1024;
                return value.intValue();
            }
            Logfile previous = getPreviousFromThisUser(em);
            if (previous != null){
                return previous.getUserMemory(em);
            }
        } catch (BackingStoreException ex) {
            Logger.getLogger(Logfile.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public Integer getUserCPUCount(EntityManager em) {
        CPUInfo info = new CPUInfo();
        Preferences pref = DbPreferences.root(this, info, em);
        try {
            Map<Integer, Integer> result = info.read(pref).getCounts();
            if (!result.isEmpty()){
                return result.keySet().iterator().next();
            }
            Logfile previous = getPreviousFromThisUser(em);
            if (previous != null) {
                return previous.getUserCPUCount(em);
            }
        } catch (BackingStoreException ex) {
            Logger.getLogger(Logfile.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public ProfilerData getProfilerStatistics(EntityManager em){
        SnapshotStatistics stats = new SnapshotStatistics();
        Preferences pref = DbPreferences.root(this, stats, em);
        try {
            SnapshotData data = stats.read(pref);
            return ProfilerData.create(data);
        } catch (BackingStoreException ex) {
            Logger.getLogger(Logfile.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;

    }

    private Logfile getPreviousFromThisUser(EntityManager em){
        if (uploadNumber > 0){
            return LogFileTask.getLogInfo(getUserdir(), uploadNumber - 1, em);
        }else{
            return null;
        }
    }
}
