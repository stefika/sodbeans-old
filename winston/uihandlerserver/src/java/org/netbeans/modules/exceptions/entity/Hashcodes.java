/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */
package org.netbeans.modules.exceptions.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 *
 * @author Jan Horvath
 */
@Entity
@Table(name = "hashcodes")
@NamedQueries({@NamedQuery(name = "Hashcodes.findByCode", query = "SELECT h FROM Hashcodes h WHERE h.code = :code ORDER BY h.exceptionid DESC"),
        @NamedQuery(name = "Hashcodes.findByExceptionid", query = "SELECT h FROM Hashcodes h WHERE h.exceptionid = :exceptionid")})
public class Hashcodes implements Serializable {
    @Column(name = "CODE")
    private Integer code;
    @Id
    @Column(name = "EXCEPTIONID", nullable = false)
    private Integer exceptionid;
    @JoinColumn(name = "EXCEPTIONID", referencedColumnName = "ID", insertable = false, updatable = false)
    @OneToOne
    private Exceptions exceptions;

    public Hashcodes() {
    }

    public Hashcodes(Integer exceptionid) {
        this.exceptionid = exceptionid;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public Integer getExceptionid() {
        return exceptionid;
    }

    public void setExceptionid(Integer exceptionid) {
        this.exceptionid = exceptionid;
    }

    public Exceptions getExceptions() {
        return exceptions;
    }

    public void setExceptions(Exceptions exceptions) {
        this.exceptions = exceptions;
    }

    @Override
    public int hashCode() {
        int hash = 0;

        hash += (exceptionid != null ? exceptionid.hashCode()
                                     : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Hashcodes)) {
            return false;
        }
        Hashcodes other = (Hashcodes) object;

        if (this.exceptionid != other.exceptionid &&
                (this.exceptionid == null ||
                !this.exceptionid.equals(other.exceptionid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.netbeans.modules.exceptions.entity.Hashcodes[exceptionid=" +
               exceptionid + "]";
    }

}
