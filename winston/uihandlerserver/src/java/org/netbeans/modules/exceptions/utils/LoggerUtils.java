/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */
package org.netbeans.modules.exceptions.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.MessageFormat;
import java.util.MissingResourceException;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import javax.swing.JButton;
import org.netbeans.modules.exceptions.entity.Logfile;
import org.netbeans.modules.exceptions.entity.Submit;

/**
 *
 * @author honza
 */
public class LoggerUtils {

    static final String USER_CONFIGURATION = "UI_USER_CONFIGURATION";   // NOI18N

    public static InputStream getLoggerAsInputStream(Submit exc) {
        Logfile logfile = exc.getLogfileId();
        String userId = logfile.getUserdir();
        String dirPath = org.netbeans.server.uihandler.Utils.getUploadDirPath(
                org.netbeans.server.uihandler.Utils.getRootUploadDirPath(), userId);
        java.io.InputStream is = null;
        try {
            String fileName = dirPath + File.separator + logfile.getFileName();
            if (new File(fileName).exists()) {
                is = new java.io.FileInputStream(fileName);
            } else {
                fileName = fileName + ".gz";
                is = new java.util.zip.GZIPInputStream(new java.io.FileInputStream(fileName));
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger("global").log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger("global").log(Level.SEVERE, null, ex);
        }
        return is;
    }

    public static void getLogRecord(Submit exceptions, Handler handler) {
        try {
            InputStream is = getLoggerAsInputStream(exceptions);
            org.netbeans.lib.uihandler.LogRecords.scan(is, handler);
        } catch (IOException ioe) {
            Logger.getLogger(LoggerUtils.class.getName()).log(Level.WARNING, "Logfile for report id {0} not found", exceptions.getReportId().getId());
        }
    }

    public static LoggerLine getLine(LogRecord r) {
        String name = null;
        String displayName = null;
        String iconName = null;

        if (r.getThrown() != null) {
            iconName = "exception.gif";
            displayName = r.getMessage();
        }

        if (r.getMessage() == null) {
            displayName = "Seq: " + r.getSequenceNumber();
        } else {
            displayName = r.getMessage();
        }
        if (r.getResourceBundle() != null) {
            try {
                String msg = r.getResourceBundle().getString(r.getMessage());
                if (r.getParameters() != null) {
                    msg = MessageFormat.format(msg, r.getParameters());
                }
                displayName = msg;
            } catch (MissingResourceException ex) {
                Logger.getAnonymousLogger().log(Level.INFO, null, ex);
            }

            try {
                String iconBase = r.getResourceBundle().getString(r.getMessage() + "_ICON_BASE"); // NOI18N
                //setIconBaseWithExtension(iconBase);
                iconName = iconBase;
            } catch (MissingResourceException ex) {
                // ok, use default
                iconName = "def.png";
            }
        }

        if ("UI_ACTION_BUTTON_PRESS".equals(r.getMessage())) { // NOI18N
            displayName = cutAmpersand(getParam(r, 4));
            String thru = getParam(r, 1, String.class);
            if ((thru != null && thru.contains("Toolbar")) || getParam(r, 0, Object.class) instanceof JButton) {
                iconName = "toolbars.gif";
            } else if (thru != null && thru.contains("MenuItem")) {
                iconName = "menus.gif";
            }
        } else if ("UI_ACTION_KEY_PRESS".equals(r.getMessage())) { // NOI18N
            displayName = cutAmpersand(getParam(r, 4));
            iconName = "key.png";
        } else if ("UI_ACTION_EDITOR".equals(r.getMessage())) { // NOI18N
            displayName = cutAmpersand(getParam(r, 4));
            iconName = "key.png";
        } else if ("UI_ENABLED_MODULES".equals(r.getMessage())) { // NOI18N
            // displayName = NbBundle.getMessage(UINode.class, "MSG_EnabledModules"));
            displayName = "Enabled Modules";
            iconName = "module.gif";
        } else if ("UI_DISABLED_MODULES".equals(r.getMessage())) { // NOI18N
            // displayName = NbBundle.getMessage(UINode.class, "MSG_DisabledModules"));
            displayName = "Disabled Modules";
            iconName = "module.gif";
        } else if (USER_CONFIGURATION.equals(r.getMessage())) {// NOI18N
            //setDisplayName(NbBundle.getMessage(UINode.class, "MSG_USER_CONFIGURATION"));
            displayName = "User Configuration";
        }
//        if (r.getParameters() != null && r.getParameters().length > 0) {
//            displayName = "Message Parameters";
//        }

        return new LoggerLine(name, displayName, iconName, r.getParameters());
    }

    private static <T> T getParam(LogRecord r, int index, Class<T> type) {
        if (r == null || r.getParameters() == null || r.getParameters().length <= index) {
            return null;
        }
        Object o = r.getParameters()[index];
        return type.isInstance(o) ? type.cast(o) : null;
    }

    static String getParam(LogRecord r, int index) {
        Object[] arr = r.getParameters();
        if (arr == null || arr.length <= index || !(arr[index] instanceof String)) {
            return "";
        }
        return (String) arr[index];
    }

    public static String cutAmpersand(String text) {
        // XXX should this also be deprecated by something in Mnemonics?
        int i;
        String result = text;

        /* First check of occurence of '(&'. If not found check
         * for '&' itself.
         * If '(&' is found then remove '(&??'.
         */
        i = text.indexOf("(&"); // NOI18N

        if ((i >= 0) && ((i + 3) < text.length()) && /* #31093 */
                (text.charAt(i + 3) == ')')) { // NOI18N
            result = text.substring(0, i) + text.substring(i + 4);
        } else {
            //Sequence '(&?)' not found look for '&' itself
            i = text.indexOf('&');

            if (i < 0) {
                //No ampersand
                result = text;
            } else if (i == (text.length() - 1)) {
                //Ampersand is last character, wrong shortcut but we remove it anyway
                result = text.substring(0, i);
            } else {
                //Remove ampersand from middle of string
                //Is ampersand followed by space? If yes do not remove it.
                if (" ".equals(text.substring(i + 1, i + 2))) {
                    result = text;
                } else {
                    result = text.substring(0, i) + text.substring(i + 1);
                }
            }
        }

        return result;
    }

}
