/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */
package org.netbeans.modules.exceptions.utils;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.Persistence;
import javax.persistence.Query;
import org.netbeans.modules.exceptions.entity.Directuser;
import org.netbeans.server.uihandler.bugs.BugzillaConnector;
import org.netbeans.modules.exceptions.entity.Exceptions;
import org.netbeans.modules.exceptions.entity.Report;
import org.netbeans.modules.exceptions.utils.Query.Operator;
import org.netbeans.server.services.beans.Components;
import org.netbeans.server.uihandler.api.Issue;
import org.openide.util.Parameters;

/**
 *
 * @author Jan Horvath
 */
public final class PersistenceUtils {
    private static final int COMPONENTS_TIMEOUT = 1000 * 3600;//1h
    private final ShortTimeCache<String, Components> comps = new ShortTimeCache<String, Components>(COMPONENTS_TIMEOUT);
    public static final String STRUTS_PU = "ExceptionsHibernatePU";
    
    public static final String TEST_PU = "TestPU";

    public static String DEFAULT_PU = STRUTS_PU;

    public static String SETTINGS_DIRECTORY = null;
    
    public static final String JNDI_NAME = "bean/Utils";
    private static final int QUERY_MAX_RESULT = 4000;
    
    static final Logger LOG = Logger.getLogger(PersistenceUtils.class.getName());
    
    private static PersistenceUtils DEFAULT;

    private EntityManagerFactory emf;
    
    private PersistenceUtils(String pu) {
        emf = Persistence.createEntityManagerFactory(pu, new HashMap());
    }
    
    public static void initTestPersistanceUtils(File db) {
        try {//short timeout before new test
            Thread.sleep(1000);
        } catch (InterruptedException ex) {
            org.openide.util.Exceptions.printStackTrace(ex);
        }
        doInitTestPersistanceUtils(db);
    }
    
    private static synchronized void doInitTestPersistanceUtils(File db) {
        if (DEFAULT != null) {
            DEFAULT.close();
            DEFAULT = null;
        }
        db.mkdirs();
        System.setProperty("derby.system.home", db.getPath());
        DEFAULT_PU = TEST_PU;
    }

    public static void setDefault(String pu) {
        if (DEFAULT != null) {
            throw new IllegalStateException("There already is a factory " + DEFAULT);
        }
        DEFAULT_PU = pu;
    }

    public static PersistenceUtils getInstance() {
        if (DEFAULT == null){
            createPersistenceUtils();
        }
        return DEFAULT;
    }
    
    private static synchronized void createPersistenceUtils(){
         if (DEFAULT == null){
             DEFAULT = new PersistenceUtils(DEFAULT_PU);
         }       
    }
    
    public static void setSettingsDirectory(String def){
        SETTINGS_DIRECTORY = def;
        LOG.log(Level.INFO, "Settings directory was set to: {0}", SETTINGS_DIRECTORY);
    }
    
    public static String getSettingsDirectory(){
        return SETTINGS_DIRECTORY;
    }

    public List<Exceptions> findExceptionsBySimilarLines(String searchedLine, boolean duplicates) {
        EntityManager em = createEntityManager();
        List<Exceptions> result;
        try{
            Query query;
            if (duplicates){
                query = em.createNamedQuery("Exceptions.findByLikeLine");
            }else{
                query = em.createNamedQuery("Report.findByLikeLine");
            }
            query.setParameter("line", "%"+searchedLine+"%");
            query.setMaxResults(200);
            if (duplicates){
                result = query.getResultList();
            } else {
                List<Report> reports = query.getResultList();
                result = new ArrayList<Exceptions>(reports.size());
                for (Report report : reports) {
                    Exceptions exc =  report.getExceptionsCollection(em).iterator().next();
                    result.add(exc);
                }
            }
        } finally {
            em.close();
        }
        return result;
    }
    
    public Components getComponents() {
        Components cd = comps.get("comps");
        if (cd == null){
            cd = BugzillaConnector.getInstance().getComponents();
            if (cd == null){
                Logger.getLogger(PersistenceUtils.class.getName())
                        .severe("Bugzilla connector returned null components!");
            }
            comps.put("comps", cd);
        }
        return cd;
    }

    public static boolean isExistingComponent(String component, String subcomponent){
        Components comps = getInstance().getComponents();
        Set<String> subcomps = comps.getSubcomponentsSet(component);
        if (subcomps == null){
            return false;
        }
        return subcomps.contains(subcomponent);
    }
    
    public void setTestComponents(Components newComps){
        comps.put("comps", newComps);
    }
    
    /**
     * @returns List of results invoked by query
     * @param name name of the named query to call
     * @param params params contains query params in a map
     *  key is String of param name and value is a param object
     *  may be null if there are not any params
     */
    @SuppressWarnings("unchecked")
    public static <T> List<T> executeNamedQuery(EntityManager em, String name, Map<String, ? extends Object> params, Class<T> type){
        return (List<T>) executeNamedQuery(em, name, params);
    }
    public static List executeNamedQuery(EntityManager em, String name, Map<String, ? extends Object> params){
        Query query = em.createNamedQuery(name);
        return executeQuery(name, query, params, QUERY_MAX_RESULT);
    }

    private static List executeQuery(String description, Query query, Map<String, ? extends Object> params, int limit){
        query.setMaxResults(limit);
        setParams(query, params);
        List list = query.getResultList();
        if (list.size() == limit){
            LOG.log(Level.SEVERE, "QUERY {0} REACHED MAX RESULTS", description);
            for (Map.Entry<String, ? extends Object> entry : params.entrySet()) {
                LOG.log(Level.SEVERE, "{0}:{1}", new Object[]{entry.getKey(), entry.getValue().toString()});
            }
        }
        return list;
    }

    /**
     * @returns List of results invoked by query
     * @param name name of the named query to call
     * @param params params contains query params in a map
     *  key is String of param name and value is a param object
     *  may be null if there are not any params
     */
    public Object executeNamedQuerySingleResult(String name, Map<String, ? extends Object> params){
        EntityManager em = getEM();
        Query query = em.createNamedQuery(name);
        setParams(query, params);
        Object result = null;
        try {
            result = query.getSingleResult();
        } catch (NoResultException e) {
        }finally{
            em.close();
        }
        return result;
    }

    public static Object executeNamedQuerySingleResult(EntityManager em, String name, Map<String, ? extends Object> params){
        Query query = em.createNamedQuery(name);
        setParams(query, params);
        Object result = null;
        try {
            result = query.getSingleResult();
        } catch (NoResultException e) {
        }
        return result;
    }

    public static int executeCount(EntityManager em, String nameQueryName, Map<String, Object> params){
        Object obj = PersistenceUtils.executeNamedQuerySingleResult(em, nameQueryName, params);
        Long count = (Long) obj;
        return count.intValue();
    }
    
    /**
     * @returns List of results invoked by query
     * @param query string containg a SQL SELECT
     * @param params params contains query params in a map
     *  key is String of param name and value is a param object
     *  may be null if there are not any params
     *  limit Set the maximum number of results to retrieve. -1 for no limit
     *
     */
    public List executeQuery(String query, Map<String, ? extends Object> params){
        EntityManager em = getEM();
        try{
            return executeQuery(em, query, params);
        }finally{
            em.close();
        }
    }
    
    public static List executeQuery(EntityManager em, String query, Map<String, ? extends Object> params){
        return executeQuery(em, query, params, QUERY_MAX_RESULT);
    }

    public static List executeQuery(EntityManager em, String query, Map<String, ? extends Object> params, int limit){
        Query queryRes = em.createQuery(query);
        return executeQuery(query, queryRes, params, limit);
    }

    public List executeNativeQuery(String query){
        EntityManager em = getEM();
        Query queryRes = em.createNativeQuery(query);
        List list = queryRes.getResultList();
        em.close();
        return list;
    }
    
    private static void setParams(Query query, Map<String, ? extends Object> params){
        if (params != null){
            Set<String> keys = params.keySet();
            Iterator<String> iter = keys.iterator();
            while (iter.hasNext()){
                String key = iter.next();
                query.setParameter(key, params.get(key));
            }
        }
    }
    
    public static <T> List<T> getAll(EntityManager em, Class<T> entity) {
        return (List<T>) PersistenceUtils.executeQuery(em, "SELECT e FROM " + entity.getSimpleName() + " e", null);
    }

    public <T> List<T> getAll(Class<T> entity) {
        return (List<T>) getInstance().executeQuery("SELECT e FROM " + entity.getSimpleName() + " e", null);
    }
    
    public List getDistinct(Class entity, Map<String, List<? extends Object>> params, String column) {
        List result = null;
        EntityManager em = getEM();
        StringBuffer finalQuery = new StringBuffer();
        try {
            
            finalQuery.append("SELECT DISTINCT x.").append(column).append(" ");
            finalQuery.append(createFromClause(entity, params, false, false, column, Operator.AND));
            
            Query q =  em.createQuery(finalQuery.toString());
            result =   q.getResultList();
            
        } catch(Exception e) {
            System.err.println(finalQuery);
            Logger.getLogger(getClass().getName()).log(Level.SEVERE,e.getMessage(), e);
            throw new RuntimeException(e);
        } finally {
            em.close();
        }
        return result;
    }
    
    public List getDistinct(Class entity, String column) {
        List result = null;
        EntityManager em = getEM();
        try {
            Query q =  em.createQuery("SELECT DISTINCT x." + column + " FROM " + entity.getSimpleName() 
                    + " x ORDER BY " + column);
            result = q.getResultList();
        } catch(Exception e) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE,e.getMessage(), e);
            throw new RuntimeException(e);
        } finally {
            em.close();
        }
        return result;
    }
    
    private StringBuffer createWhereClause(Map<String, List<? extends Object>> params, Operator op, String var) {
        StringBuffer query = new StringBuffer();
        boolean insertOperator = false;
        for (Iterator<String> it = params.keySet().iterator(); it.hasNext();) {
            String key = it.next();
            List<? extends Object> l = params.get(key);
            if (insertOperator) {
                if (l.size() > 0) {
                    query.append(" ").append(op.name()).append(" ");
                }
                insertOperator = false;
            }
            StringBuffer sub = new StringBuffer();
            for(Iterator<? extends Object> it1 = l.iterator(); it1.hasNext(); ){
                Object val = it1.next();
                if (val instanceof String) {
                    sub.append(var).append(".").append(key).append(" = '").append(val).append("'");
                } else {
                    sub.append(var).append(".").append(key).append(" = ").append(val);
                }
                if (it1.hasNext()) {
                    sub.append(" OR ");
                }
            }
            if (sub.length() > 0) {
                query.append(" (").append(sub).append(") ");
            }
            if (it.hasNext()) {
                insertOperator = true;
            }
        }
        return query;
    }
    
    public StringBuffer createFromClause(Class entity, Map<String, List<? extends Object>> params, boolean duplicates, boolean original, String orderBy, Operator op) {
        StringBuffer finalQuery = new StringBuffer("FROM " + entity.getSimpleName() + " x");
        StringBuffer query = createWhereClause(params, op, "x");
        // for Exceptions - don't find in duplicates
        if (Exceptions.class.equals(entity) && original) {
            if (query.length() > 0) {
                query.append(" AND ");
            }
            query.append("x.duplicateof is null");
        }
        
        //            not working with mysql 4.0
        if (duplicates) {
            if (query.length() > 0) {
                query.append(" AND ");
            }
            query.append("SIZE(x.duplicatesCollection) > 0 ");
        }
        
        if (query.length() > 0) {
            finalQuery.append(" WHERE ");
            finalQuery.append(query);
        }
        
        if (orderBy != null) {
            finalQuery.append(" ORDER BY x.").append(orderBy).append(" DESC");
        }
        return finalQuery;
    }
    
    public <T> List<T> find(Class<T> entity, Map<String, List<? extends Object>> params) {
        return find(entity, params, false, null);
    }
    public <T> List<T> find(Class<T> entity, Map<String, List<? extends Object>> params, boolean duplicates, String orderBy) {
        return find(entity, params, duplicates, true, orderBy, Operator.AND);
    }
    public <T> List<T> find(Class<T> entity, Map<String, List<? extends Object>> params, boolean duplicates, boolean originals, String orderBy) {
        return find(entity, params, duplicates, originals, orderBy, Operator.AND);
    }
    public <T> List<T> find(Class<T> entity, Map<String, List<? extends Object>> params, Operator out) {
        return find(entity, params, false, true, null, out);
    }
    public <T> List<T> find(Class<T> entity, Map<String, List<? extends Object>> params, boolean duplicates, boolean originals, String orderBy, Operator op) {
        List result = null;
        EntityManager em = getEM();
        try {
            StringBuffer select = new StringBuffer();
            select.append("SELECT x ");
            select.append(createFromClause(entity, params, duplicates, originals, orderBy, op));
            Query q =  em.createQuery(select.toString());
            result = q.getResultList();
        } catch(Exception e) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE,e.getMessage(), e);
            throw new RuntimeException(e);
        } finally {
            em.close();
        }
        return result;
    }
    
    private String createFromClauseForExceptions(Map<String, List<? extends Object>> params) {
        StringBuffer select = new StringBuffer();
        if (params == null || params.isEmpty()) {
                select.append("FROM Exceptions e ");
            } else {
                StringBuffer w1 = createWhereClause(params, Operator.AND, "e");
                StringBuffer w2 = createWhereClause(params, Operator.AND, "ex");
                if (w1.length() > 0) {
                    w1.append(" AND ");
                    w2.append(" AND ");
                }
                select.append("FROM Exceptions e ");
                select.append("WHERE EXISTS (SELECT ex FROM Exceptions ex WHERE ").append(w2).append("ex.duplicateof=e) ");
                select.append("OR (").append(w1).append("e.duplicateof is null) ");
            }
        return select.toString();
    }
    public List<Exceptions> findExceptions(Map<String, List<? extends Object>> params, String orderBy) {
        List result = null;
        EntityManager em = getEM();
        try {
            StringBuffer select = new StringBuffer("SELECT e ");
            select.append(createFromClauseForExceptions(params));
            if (orderBy != null) {
                    select.append(" ORDER BY e.").append(orderBy).append(" DESC");
                }
            Query q =  em.createQuery(select.toString());
            result = q.getResultList();
        } catch(Exception e) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE,e.getMessage(), e);
            throw new RuntimeException(e);
        } finally {
            em.close();
        }
        return result;
    }
    
    public Long countExceptions(Map<String, List<? extends Object>> params) {
        Long result = null;
        EntityManager em = getEM();
        try {
            StringBuffer select = new StringBuffer("SELECT COUNT(e) ");
            select.append(createFromClauseForExceptions(params));
            Query q =  em.createQuery(select.toString());
            result =  (Long) q.getSingleResult();
        } catch(Exception e) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE,e.getMessage(), e);
            throw new RuntimeException(e);
        } finally {
            em.close();
        }
        return result;
    }
    
    public List<Report> findUnmapped() {
        List<Report> result = null;
        EntityManager em = getEM();
        try {
            Query q =  em.createQuery("SELECT r FROM Report r WHERE r.issueId = 0 ");
            result = q.getResultList();
        } catch(Exception e) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE,e.getMessage(), e);
            throw new RuntimeException(e);
        } finally {
            em.close();
        }
        return result;
    }

    /**
     * @return list of candidates to be inserted into issuezilla
     * @param duplicatesNo minimal number of duplicates, that issue need
     * to have to become a candidate
     */ 
    public List<Report> findExceptionCandidates(long duplicatesNo) {
        List<Report> result = null;
        EntityManager em = getEM();
        try {
            Query q =  em.createQuery("SELECT r FROM Report r WHERE (r.issueId = 0 OR r.issueId IS NULL) AND size(r.submitCollection) > " + duplicatesNo);
            result = q.getResultList();
        } catch(Exception e) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE,e.getMessage(), e);
            throw new RuntimeException(e);
        } finally {
            em.close();
        }
        return result;
    }
    
    /**
     * @returns number of entities satisfying the condition
     * @param entity entity class to count
     * @param condition condition to satisfy you can reference the entity by "entity"
     *
     */
    public int count(Class entity, String condition){
        return getSingleResult("SELECT COUNT(entity) FROM " + entity.getSimpleName() + " entity WHERE " + condition);
    }
    
    public int count(Class entity) {
        return getSingleResult("SELECT COUNT(e) FROM " + entity.getSimpleName() + " e");
    }
   
    public int max(Class entity, String field) {
        EntityManager em = getEM();
        Query query = em.createQuery("SELECT MAX(e." + field + ") FROM " + entity.getSimpleName() + " e");
        Integer result = (Integer) query.getSingleResult();
        em.close();
        if (result == null){
            return 0;
        }
        else return result;
    }
   
    private int getSingleResult(String query){
        EntityManager em = getEM();
        Query que = em.createQuery(query);
        Long result = (Long) que.getSingleResult();
        em.close();
        if (result == null) return 0;
        else return result.intValue();
    }
    
    public Long count(Class entity, Map<String, List<? extends Object>> params) {
        return count(entity, params, true, false);
    }
    public Long count(Class entity, Map<String, List<? extends Object>> params, boolean originals, boolean duplicatesFilter) {
        Long result = null;
        EntityManager em = getEM();
        StringBuffer finalQuery = new StringBuffer();
        try {
            
            finalQuery.append("SELECT COUNT(x) ");
            finalQuery.append(createFromClause(entity, params, duplicatesFilter, originals, null, Operator.AND));
            
            Query q =  em.createQuery(finalQuery.toString());
            result =  (Long) q.getSingleResult();
            
        } catch(Exception e) {
            System.err.println(finalQuery);
            Logger.getLogger(getClass().getName()).log(Level.SEVERE,e.getMessage(), e);
            throw new RuntimeException(e);
        } finally {
            em.close();
        }
        return result;
    }
    
    public <T> T getEntity(Class<T> entity, Object key) {
        EntityManager em = createEntityManager();
        T result = em.find(entity, key);
        em.close();
        return result;
    }
    
    /**
     * Returns single object found by query a subject name. Null is returned if there
     * is no such object. Searched object should be unique - NonUniqueResultException 
     * is thrown otherwise.
     * @param queryName
     * @param subjectName
     * @return subject entity or null if not any
     */
    public Object getExist(String queryName, String subjectName) {
        Map<String, Object> params = Collections.singletonMap("name", (Object) subjectName);// NOI18N
        try{
            Object result = this.executeNamedQuerySingleResult(queryName, params);
            return result;
        } catch (NoResultException exc) {
            // no item in DB
            return null;
        }
    }

    /**
     * Returns single object found by query a subject name. Null is returned if there
     * is no such object. Searched object should be unique - NonUniqueResultException 
     * is thrown otherwise.
     * @param Entity manager is given to perform search
     * @param queryName
     * @param subjectName
     * @return subject entity or null if not any
     */
    public static Object getExist(EntityManager em, String queryString, Object param) {
        Query query = em.createNamedQuery(queryString);
        query.setParameter("name", (Object) param);
        try {
            Object result = query.getSingleResult();
            return result;
        } catch (NoResultException exc) {
            // no item in DB
            return null;
        }
    }

    public final EntityManager createEntityManager() {
        return emf.createEntityManager();
    }
    
    private final EntityManager getEM() {
        return emf.createEntityManager();
    }
    
    public void persist(Object object) {
        EntityManager em = getEM();
        em.getTransaction().begin();
        em.persist(object);
        em.getTransaction().commit();
        em.close();
    }
    
    public void merge(Object object) {
        EntityManager em = getEM();
        em.getTransaction().begin();
        em.merge(object);
        em.getTransaction().commit();
        em.close();
    }
    
    public void merge(EntityManager em, Object object) {
        em.getTransaction().begin();
        em.merge(object);
        em.getTransaction().commit();
    }
    
    public void remove(Object object) {
        EntityManager em = getEM();
        em.getTransaction().begin();
        em.remove(object);
        em.getTransaction().commit();
        em.close();
    }

    private void close() {
        try{
            emf.close();
        }catch(Throwable e){
            e.printStackTrace();
        }
    }

    public void updateComponents(Map<String, Object> params){
        String update = "UPDATE Report r set r.component = :component, r.subcomponent = :subcomponent" +
                " WHERE r.component = :oldcomponent AND r.subcomponent = :oldsubcomponent";
        EntityManager em = getEM();
        em.getTransaction().begin();
        Query query = em.createQuery(update);
        setParams(query, params);
        query.executeUpdate();
        em.getTransaction().commit();
        if (em.isOpen()){
            em.close();
        }
    }

    public static boolean issueIsOpen(Integer issuezillaID) {
        Issue issue = BugReporterFactory.getDefaultReporter().getIssue(issuezillaID);
        if (issue == null){
            LOG.severe("Issuezilla connection is broken - no issue found!!!");
            return true;
        }
        return issue.isOpen();
    }

    public static boolean issueIsFixed(Integer issueId) {
        if (issueId == null){
            return false;
        }
        Issue issue = BugReporterFactory.getDefaultReporter().getIssue(issueId);
        if (issue == null){
            return false;
        }
        return issue.isFixed();
    }

    public static boolean exceptionIsOpen(Exceptions exc) {
        Parameters.notNull("exceptions", exc);
        if (!exc.getReportId().isInIssuezilla()){
            return true;
        }
        Issue issue = BugReporterFactory.getDefaultReporter().getIssue(exc.getReportId().getIssueId());
        return issue.isOpen();
    }

    public static Directuser getDirectuser(EntityManager em, String username){
        if (em == null){
            return getDirectuser(username);
        }
       return (Directuser) executeNamedQuerySingleResult(em, "Directuser.findByName", Collections.singletonMap("name", username));
    }
    
    public static Directuser getDirectuser(final String username){
        final Directuser[] result = new Directuser[1];
        org.netbeans.web.Utils.processPersistable(new org.netbeans.web.Persistable.Query() {

            public TransactionResult runQuery(EntityManager em) throws Exception {
               result[0] = getDirectuser(em, username);
               return TransactionResult.NONE;
            }
        });
        return result[0];
    }
}
