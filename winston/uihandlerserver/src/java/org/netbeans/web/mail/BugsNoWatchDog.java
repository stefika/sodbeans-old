/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 2010 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 *
 * Contributor(s):
 *
 * Portions Copyrighted 2010 Sun Microsystems, Inc.
 */
package org.netbeans.web.mail;

import java.util.Calendar;
import java.util.HashSet;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import javax.persistence.EntityManager;
import org.netbeans.modules.exceptions.entity.Report;
import org.netbeans.web.Persistable;
import org.netbeans.web.Utils;

/**
 *
 * @author Jindrich Sedek
 */
public class BugsNoWatchDog extends TimerTask implements Persistable {

    private static final String QUERY = "SELECT DISTINCT e.reportId FROM %1$s e WHERE "
            + " ((e.reportId.issueId IS NULL) OR e.reportId.issueId = 0) "
            + " AND (SELECT COUNT(e2) from %1$s e2 where e2.reportId = e.reportId) >= %2$s";
    private static final Timer timer = new Timer();
    private static final long period = 1000 * 3600 * 24 * 7;// run once a week

    public static void start() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_MONTH, 1);
        while (calendar.get(Calendar.DAY_OF_WEEK) != Calendar.MONDAY) {
            calendar.add(Calendar.DAY_OF_MONTH, 1);
        }
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.HOUR, 0);
        // plan to execute each Monday during night
        timer.schedule(new BugsNoWatchDog(), calendar.getTime(), period);
    }

    public static void stop() {
        timer.cancel();
    }

    @Override
    public void run() {
        Utils.processPersistable(this);
    }

    public TransactionResult runQuery(EntityManager em) throws Exception {
        String msgBody = prepareMessage(em, 30);
        new Emailer().sendEmail(getRecepients(), "Duplicated reports not reported to Bugzilla", msgBody);
        return TransactionResult.NONE;
    }

    String prepareMessage(EntityManager em, int min) {
        String exceptionsQuery = String.format(QUERY, "Exceptions", min);
        String sloqnessQuery = String.format(QUERY, "Slowness", min);
        Set<Report> sbmts = new HashSet<Report>(em.createQuery(exceptionsQuery).getResultList());
        sbmts.addAll(em.createQuery(sloqnessQuery).getResultList());
        StringBuilder sb = new StringBuilder("The list of all reports with at least ").append(min).append(" duplicates:\n");
        for (Report report : sbmts) {
            sb.append(Utils.EXCEPTION_DETAIL).append(report.getId()).append("\n");
        }
        return sb.toString();
    }

    public boolean needsTransaction() {
        return false;
    }

    private static String[] getRecepients() {
        String recepients = org.netbeans.server.uihandler.Utils.getVariable("recepients", String.class);
        return recepients.split(",");
    }
}
