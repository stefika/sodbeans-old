/*
 * ExceptionsComparator.java
 * 
 * Created on 6/04/2007, 22:46:56
 * 
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package org.netbeans.web;

import java.util.Comparator;
import javax.persistence.EntityManager;
import org.netbeans.modules.exceptions.entity.Exceptions;
/**
 *
 * @param Exceptions 
 * @author honza
 */
public class ExceptionsComparator implements Comparator<Exceptions>{

    private String field;
    private EntityManager em;
    public ExceptionsComparator(String field, EntityManager em) {
        this.field = field;
        this.em = em;
    }
    public int compare(Exceptions e1, Exceptions e2) {
        if ("duplicates".equals(field)) {
            return e2.getReportId().getDuplicates(em) - e1.getReportId().getDuplicates(em);
        } 
        else if ("build".equals(field)) {
            return (int) (e2.getLatestBuild() - e1.getLatestBuild());
        } 
        else if ("id".equals(field)) {
            return (int) (e2.getId() - e1.getId());
        }
        else if ("summary".equals(field)) {
            return (int) (e2.getSummary().compareTo(e1.getSummary()));
        }
        else if ("component".equals(field)) {
            return (int) (e2.getReportId().getComponent().compareTo(e1.getReportId().getComponent()));
        }
        else if ("subcomponent".equals(field)) {
            return (int) (e2.getReportId().getSubcomponent().compareTo(e1.getReportId().getSubcomponent()));
        }
        else if ("issuezillad".equals(field)) {
            return (int) (e2.getReportId().getIssueId() - e1.getReportId().getIssueId());
        }
        return 0;    
    }



}
