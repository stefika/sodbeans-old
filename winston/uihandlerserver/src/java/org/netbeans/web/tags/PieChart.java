/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */
package org.netbeans.web.tags;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

/**
 *
 * @author Jindrich Sedek
 */
public final class PieChart extends GoogleChart {
    private static final String GOOGLE_URL = "<img src='http://chart.apis.google.com/chart?" +
            "cht=p3&amp;" +
            "chd=t:%1$s&amp;" + // data
            "chl=%2$s&amp;" + // titles
            "chs=%4$s&amp;" + // resolution
            "chco=%5$s&amp;" + // colors
            "chtt=%3$s' " +
            "alt='%3$s'/>";

    public PieChart() {
        lblFormat = "%1$s (%2$s%%)";
    }

    String prepareResultString() {
        StringBuffer dataBuffer = new StringBuffer();
        StringBuffer titleBuffer = new StringBuffer();
        Object input = getJspContext().getAttribute(collection);
        Map<String, Integer> map = convert(input);
        Iterator<Entry<String, Integer>> it = map.entrySet().iterator();
        while (it.hasNext()) {
            Entry<String, Integer> entry = it.next();
            titleBuffer.append(String.format(lblFormat, entry.getKey(), entry.getValue()));
            dataBuffer.append(entry.getValue());
            if (it.hasNext()) {
                dataBuffer.append(DATA_SEPARATOR);
                titleBuffer.append(TITLE_SEPARATOR);
            }
        }
        String result = String.format(GOOGLE_URL, dataBuffer, titleBuffer, title, resolution, getColorsString(map.size()));
        return result;
    }

    //------------ helper methods -------------------//
    protected Map<String, Integer> convert(Object input) {
        if (input instanceof Map) {
            return convert((Map) input);
        } else if (input instanceof Collection) {
            return convert((Collection) input);
        } else {
            throw new IllegalArgumentException("collection attribute should be a collection or a map");
        }
    }

    private Map<String, Integer> convert(Collection input) {
        long sum = 0;
        Map<String, Integer> result = new LinkedHashMap<String, Integer>();
        try {
            for (Object obj : input) {
                sum += getValue(obj);
            }
            if (sum == 0) {
                return result;
            }
            for (Object obj : input) {
                Long percentage = getValue(obj) * 100 / sum;
                if (percentage != 0) {
                    result.put(getKey(obj).toString(), percentage.intValue());
                }
            }
        } catch (Exception conversion) {
            throw new IllegalArgumentException("Parametters were not set correctly", conversion);
        }
        return result;
    }

    private Map<String, Integer> convert(Map map) {
        long sum = 0;
        Map<String, Integer> result = new LinkedHashMap<String, Integer>();
        try {
            for (Object obj : map.values()) {
                sum += getLongValue(obj);
            }
            if (sum == 0) {
                return result;
            }
            for (Object key : map.keySet()) {
                Object value = map.get(key);
                Long percentage = getLongValue(value) * 100 / sum;
                if (percentage != 0) {
                    result.put(key.toString(), percentage.intValue());
                }
            }
        } catch (Exception conversion) {
            throw new IllegalArgumentException("Parametters were not set correctly", conversion);
        }
        return result;
    }

}