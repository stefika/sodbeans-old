/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */
package org.netbeans.web.tags;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.BodyContent;
import javax.servlet.jsp.tagext.BodyTagSupport;
import org.netbeans.modules.exceptions.utils.PersistenceUtils;

/**
 * @author  Jan Horvath
 * @version
 */

public class DistinctTagHandler extends BodyTagSupport {
    
    /** Creates new instance of tag handler */
    private String entity;
    private String field;
    private String var;
    private Iterator it;
    /** Creates new instance of tag handler */
    public DistinctTagHandler() {
        super();
    }
    
    public int doStartTag() throws JspException, JspException {
        it = getDistinct(entity, field).iterator();
        
        if (setVariable()) {
            return EVAL_BODY_BUFFERED;
        } else {
            return SKIP_BODY;
        }
    }
    
    public int doEndTag() throws JspException, JspException {
        if (shouldEvaluateRestOfPageAfterEndTag()) {
            return EVAL_PAGE;
        } else {
            return SKIP_PAGE;
        }
    }
    
    public int doAfterBody() throws JspException {
        try {
            
            BodyContent bodyContent = getBodyContent();
            JspWriter out = bodyContent.getEnclosingWriter();
            bodyContent.writeOut(out);
            bodyContent.clearBody();
        } catch (Exception ex) {
            handleBodyContentException(ex);
        }
        
        if (setVariable()) {
            return EVAL_BODY_AGAIN;
        } else {
            return SKIP_BODY;
        }
    }
    
    private boolean setVariable() {
        if (it.hasNext()) {
            Object val =  it.next();
            pageContext.setAttribute(var, val, PageContext.PAGE_SCOPE);
            return true;
        } else {
            return false;
        }
    }
    
    private void handleBodyContentException(Exception ex) throws JspException {
        // Since the doAfterBody method is guarded, place exception handing code here.
        throw new JspException("error in NewTag: " + ex);
    }
    
    /**
     * Fill in this method to determine if the rest of the JSP page
     * should be generated after this tag is finished.
     * Called from doEndTag().
     */
    private boolean shouldEvaluateRestOfPageAfterEndTag()  {
        return true;
    }
    
    public void setEntity(String entity) {
        this.entity = entity;
    }
    
    public void setField(String field) {
        this.field = field;
    }
    
    public void setVar(String var) {
        this.var = var;
    }
    
    protected List getDistinct(String entity, String column) {
        try     {
            List list = PersistenceUtils.getInstance()
                    .getDistinct(java.lang.Class.forName("org.netbeans.modules.exceptions.entity." + entity), column);
        return list;
        }
        catch (ClassNotFoundException ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE,"exception caught", ex);
            return Collections.emptyList();
        }
}
    
}
