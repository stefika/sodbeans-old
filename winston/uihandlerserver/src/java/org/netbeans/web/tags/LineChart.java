/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */
package org.netbeans.web.tags;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

/**
 *
 * @author Jindrich Sedek
 */
public final class LineChart extends GoogleChart {
    private static final int LABELS_COUNT = 5;
    private static final String GOOGLE_URL = "<img src='http://chart.apis.google.com/chart?" +
            "chdlp=%8$s&amp;" + // legend possition
            "chxl=%7$s&amp;" + // x-axis code
            "chco=%6$s&amp;" + // colors
            "cht=%5$s&amp;" + // chart type
            "chs=%4$s&amp;" + // resolution
            "%2$s" + // legend
            "chd=t:%1$s&amp;" + // data
            "chxt=x,y&amp;" + // axis
            "chtt=%3$s' " + // title
            "alt='%3$s'/>";

    public LineChart() {
        lblFormat = "%1$s";
        showlegend = true;
    }

    @Override
    String prepareResultString() {
        String type = "lxy";
        StringBuffer dataBuffer = new StringBuffer();
        StringBuffer titleBuffer = new StringBuffer();
        Object input = getJspContext().getAttribute(collection);
        Map<Object, SerieData> series = convert(input);
        if (series.size() == 0) { // no data to show
            return "";
        }
        if (legendPosition == null) {
            legendPosition = LegendPossition.RIGHT;
        }
        Iterator<Entry<Object, SerieData>> it = series.entrySet().iterator();
        Scale scale = scale(series);
        if (scale.maxY == 0 || scale.maxX == 0){ // just zero data
            return "";
        }
        if (scale.minX == scale.maxX || (scale.minX * 100 / (scale.maxX - scale.minX)) < 10){
            scale.minX = 0;
        }
        if (scale.minY == scale.maxY || (scale.minY * 100 / (scale.maxY - scale.minY)) < 10){
            scale.minY = 0;
        }
        while (it.hasNext()) {
            Entry<Object, SerieData> entry = it.next();
            String values = entry.getValue().getValueList(scale);
            titleBuffer.append(String.format(lblFormat, entry.getKey()));
            dataBuffer.append(values);
            if (it.hasNext()) {
                dataBuffer.append(SERIE_SEPARATOR);
                titleBuffer.append(TITLE_SEPARATOR);
            }
        }
        String legend;
        if (showlegend) {
            legend = "chdl=" + titleBuffer + AMP;
        } else {
            legend = "";
        }
        String result = String.format(GOOGLE_URL, dataBuffer, legend, title,
                resolution, type, getColorsString(series.size()), 
                getKeyLabels(scale), series.size(), legendPosition.googleValue());
        return result;
    }

    Scale scale(Map<Object, SerieData> series) {
        Long maxX = 0l, maxY = 0l;
        Long minX = Long.MAX_VALUE;
        Long minY = Long.MAX_VALUE;
        for (SerieData serieData : series.values()) {
            for (Long key : serieData.data.keySet()) {
                maxX = Math.max(maxX, key);
                minX = Math.min(minX, key);
            }
            for (Long value : serieData.data.values()) {
                maxY = Math.max(maxY, value);
                minY = Math.min(minY, value);
            }
        }
        return new Scale(minX, minY, maxX, maxY);
    }

    private String getKeyLabels(Scale scale) {
        String xAxis = "0:|0";
        String yAxis = "1:|0";
        if (showXAxisLabel){
            xAxis = "0:|" + scale.minX + "|";
            long xStep = Math.max((scale.maxX - scale.minX) / LABELS_COUNT, 1);
            for (int i = 1; i < LABELS_COUNT - 1; i++){
                xAxis = xAxis.concat(Long.toString(scale.minX + xStep * i)).concat(SERIE_SEPARATOR);
            }
            xAxis = xAxis.concat(Long.toString(scale.maxX));
        }
        if (showYAxisLabel){
            yAxis = "1:|" + scale.minY + "|";
            long yStep = Math.max((scale.maxY - scale.minY) / LABELS_COUNT, 1);
            for (int i = 1; i < LABELS_COUNT - 1; i++){
                yAxis = yAxis.concat(Long.toString(scale.minY + yStep * i)).concat(SERIE_SEPARATOR);
            }
            yAxis = yAxis.concat(Long.toString(scale.maxY));
        }
        return xAxis + SERIE_SEPARATOR + yAxis;
    }

    Map<Object, SerieData> convert(Object input) {
        if (!(input instanceof Collection)) {
            throw new IllegalArgumentException("collection attribute should be a collection type");
        }
        Collection coll = (Collection) input;
        Map<Object, SerieData> series = new LinkedHashMap<Object, SerieData>();
        for (Object object : coll) {
            try {
                Object serie = getSerie(object);
                SerieData data = series.get(serie);
                Long key = getLongValue(getKey(object));
                if (data == null) {
                    data = new SerieData(key, getValue(object));
                } else {
                    data.add(key, getValue(object));
                }
                series.put(serie, data);
            } catch (Exception conversion) {
                throw new IllegalArgumentException("Parametters were not set correctly", conversion);
            }
        }
        return series;
    }

    static class SerieData {

        LinkedHashMap<Long, Long> data;

        public SerieData(Long key, Long value) {
            data = new LinkedHashMap<Long, Long>();
            data.put(key, value);
        }

        private void add(Long key, Long value) {
            data.put(key, value);
        }

        String getValueList(Scale scale) {
            String resultX = "", resultY="";
            Iterator<Entry<Long, Long>> it = data.entrySet().iterator();
            Long lastX = null;
            while(it.hasNext()){
                Entry<Long, Long> entry = it.next();
                Long x = (entry.getKey() - scale.minX) * 100 / (scale.maxX - scale.minX);
                if (x.equals(lastX)){
                    continue;
                }
                lastX = x;
                Long y = (entry.getValue() - scale.minY)* 100 / (scale.maxY- scale.minY);
                resultX = resultX.concat(x.toString());
                resultY = resultY.concat(y.toString());
                if (it.hasNext()){
                    resultX = resultX.concat(DATA_SEPARATOR);
                    resultY = resultY.concat(DATA_SEPARATOR);
                }
            }
            return resultX + SERIE_SEPARATOR + resultY;
        }
    }

    static class Scale {
        long minX, minY;
        long maxX,  maxY;

        public Scale(long minX, long minY, long maxX, long maxY) {
            this.maxX = maxX;
            this.maxY = maxY;
            this.minX = minX;
            this.minY = minY;
        }
    }
}