/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2009 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */
package org.netbeans.web.tags;

import java.net.URLEncoder;
import java.text.CharacterIterator;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.text.StringCharacterIterator;
import java.util.Date;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.servlet.http.HttpServletRequest;
import org.netbeans.modules.exceptions.entity.Submit;
import org.netbeans.server.uihandler.statistics.GenericMap;

/**
 *
 * @author honza
 */
public class JstlFunctions {

    private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    private static final int CHANGESET_LENGTH = 12;
    public static final String ORIG_URL = "originalUrl";


    public static String getJavaVersion(String vmname){
        String result = "";
        Pattern p = Pattern.compile("1\\.\\d\\.0(_\\d\\d|-ea-b(\\d){1,3})|-rc");

        Matcher m = p.matcher(vmname);
        if (m.find()) {
            result = m.group();
        }
        return result;
    }
    
    
    public static String getBuildNumberLong(Long build) {
        return build == null ? "" : getBuildNumber(build.intValue());
    }

    public static String getBuildNumber(long build) {
        NumberFormat df = DecimalFormat.getInstance();
        df.setMinimumIntegerDigits(6);
        df.setGroupingUsed(false);
        return df.format(build);
    }

    public static String formatDate(Date date) {
        return sdf.format(date);
    }

    public static String formatComment(String comment) {
        StringBuilder sb = new StringBuilder();
        String[] line = comment.split("\r\n|\r|\n");
        for (int i = 0; i < line.length; i++) {
            sb.append(escapeXML(line[i]));
            if ((i + 1) < line.length) {
                sb.append("<br/>");
            }
        }
        return sb.toString();
    }

    public static String escapeXML(String text) {
        final StringBuilder result = new StringBuilder();
        final StringCharacterIterator iterator = new StringCharacterIterator(text);
        char character =  iterator.current();
        while (character != CharacterIterator.DONE ){
            if (character == '<') {
                result.append("&lt;");
            }
            else if (character == '>') {
                result.append("&gt;");
            }
            else if (character == '\"') {
                result.append("&quot;");
            }
            else if (character == '\'') {
                result.append("&#039;");
            }
            else if (character == '&') {
                 result.append("&amp;");
            }
            else {
                result.append(character);
            }
            character = iterator.next();
        }
        return result.toString();
  }

    public static String formatChangeset(Long changeset) {
        String result = "n/a";
        if (changeset != null && changeset != 0) {
            result = Long.toHexString(changeset);
            while (result.length() < CHANGESET_LENGTH) {
                result = "0" + result;
            }
        }
        return result;
    }

    public static String createIssueList( java.util.Collection<Submit> submits ){
        Set<Integer> ids = new TreeSet<Integer>();
        for (Submit submit : submits) {
            if (submit.getReportId().isInIssuezilla()){
                ids.add(submit.getReportId().getIssueId());
            }
        }
        String result = new String();
        for (Integer id : ids) {
            result += id + ",";
        }
        return result;
    }

    public static String getOrigUrl(javax.servlet.ServletRequest request) {
        Object o = request.getAttribute(ORIG_URL);
        String origUrl = null;
        if (o == null) {
            StringBuffer sb = ((HttpServletRequest) request).getRequestURL();
            String qs = ((HttpServletRequest) request).getQueryString();
            if (qs != null) {
                sb.append("?");
                sb.append(qs);
            }
            origUrl = URLEncoder.encode(sb.toString());
        } else {
            origUrl = (String) o;
        }
        return origUrl;
    }

    public static Map<String, Integer> getCounts(GenericMap gm, String key, boolean logsAvare, boolean encode){
        if (logsAvare) {
            return gm.getLogAvareCounts(key, encode);
        } else {
            return gm.getCounts(key, encode);
        }
    }
}
