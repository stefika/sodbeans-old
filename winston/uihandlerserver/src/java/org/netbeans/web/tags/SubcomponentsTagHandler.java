/*
 * SubcomponentsTagHandler.java
 *
 * Created on 19 April 2007, 09:43
 */

package org.netbeans.web.tags;

import java.util.Iterator;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.BodyContent;
import javax.servlet.jsp.tagext.BodyTagSupport;
import org.netbeans.modules.exceptions.utils.PersistenceUtils;
import org.netbeans.server.services.beans.Components;

/**
 *
 * @author  honza
 * @version
 */

public class SubcomponentsTagHandler extends BodyTagSupport {
    /** Creates new instance of tag handler */
    private String entity;
    private String component;
    private String var;
    private Iterator it;
    /** Creates new instance of tag handler */
    public SubcomponentsTagHandler() {
        super();
    }
    
    @Override
    public int doStartTag() throws JspException, JspException {
        Components cmps = PersistenceUtils.getInstance().getComponents();
        it = cmps.getSubcomponentsSet(component).iterator();
        
        if (setVariable()) {
            return EVAL_BODY_BUFFERED;
        } else {
            return SKIP_BODY;
        }
    }
    
    @Override
    public int doEndTag() throws JspException, JspException {
        if (shouldEvaluateRestOfPageAfterEndTag()) {
            return EVAL_PAGE;
        } else {
            return SKIP_PAGE;
        }
    }
    
    @Override
    public int doAfterBody() throws JspException {
        try {
            
            BodyContent bc = getBodyContent();
            JspWriter out = bc.getEnclosingWriter();
            bc.writeOut(out);
            bc.clearBody();
        } catch (Exception ex) {
            handleBodyContentException(ex);
        }
        
        if (setVariable()) {
            return EVAL_BODY_AGAIN;
        } else {
            return SKIP_BODY;
        }
    }
    
    private boolean setVariable() {
        if (it.hasNext()) {
            String val = (String) it.next();
            pageContext.setAttribute(var, val, PageContext.PAGE_SCOPE);
            return true;
        } else {
            return false;
        }
    }
    
    private void handleBodyContentException(Exception ex) throws JspException {
        // Since the doAfterBody method is guarded, place exception handing code here.
        throw new JspException("error in NewTag: " + ex);
    }
    
    /**
     * Fill in this method to determine if the rest of the JSP page
     * should be generated after this tag is finished.
     * Called from doEndTag().
     */
    private boolean shouldEvaluateRestOfPageAfterEndTag()  {
        return true;
    }
    
    public void setEntity(String entity) {
        this.entity = entity;
    }
    
    public void setComponent(String component) {
        this.component = component;
    }
    
    public void setVar(String var) {
        this.var = var;
    }
    
}
