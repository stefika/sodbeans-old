/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */
package org.netbeans.web.tags;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.servlet.jsp.el.ELException;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.regex.Pattern;
import javax.servlet.jsp.tagext.*;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import org.netbeans.modules.exceptions.entity.Line;
import org.netbeans.modules.exceptions.entity.Stacktrace;
import org.netbeans.modules.exceptions.utils.LineComparator;
import org.netbeans.modules.exceptions.utils.PersistenceUtils;
import org.netbeans.web.Persistable;
import org.netbeans.web.Utils;

/**
 *
 * @author  Jan Horvath
 * @version
 */

public class StacktraceTagHandler extends SimpleTagSupport {
    
    /**Called by the container to invoke this tag.
     * The implementation of this method is provided by the tag library developer,
     * and handles all tag processing, body iteration, etc.
     */
    private Stacktrace stacktrace;
    private boolean form;
    private static final Pattern pat = Pattern.compile("\\.");
    
    /**Called by the container to invoke this tag.
     * The implementation of this method is provided by the tag library developer,
     * and handles all tag processing, body iteration, etc.
     */
    @Override
    public void doTag() throws JspException {
        
        Utils.processPersistable(new Persistable() {

            public TransactionResult runQuery(EntityManager em) {
                JspWriter out=getJspContext().getOut();

                try {
                    stacktrace = em.find(Stacktrace.class, stacktrace.getId());
                    while (stacktrace != null) {
                        out.println(stacktrace.getMessage().trim());
                        List<Line> lines = PersistenceUtils.executeNamedQuery(em, "Line.findByStacktrace", Collections.singletonMap("stacktrace", (Object) stacktrace), Line.class);
                        Collections.sort(lines, new LineComparator());
                        for(Line line : lines) {
                            formatLine(out, line);
                        }
                        stacktrace = stacktrace.getAnnotation();
                    }
                    JspFragment f=getJspBody();
                    if (f != null) {
                        f.invoke(out);
                    }

                } catch (JspException ex) {
                    throw new RuntimeException(ex.getMessage());
                } catch (IOException ex) {
                    throw new RuntimeException(ex.getMessage());
                }
                return TransactionResult.NONE;
            }

            public boolean needsTransaction() {
                return false;
            }
        });
        
    }
    
    public void setStacktrace(Stacktrace stacktrace) {
        this.stacktrace = stacktrace;
    }
    
    public void setForm(boolean form) {
        this.form = form;
    }
    
    private void formatLine(JspWriter out, Line line) throws IOException {
        try {
            java.lang.String methodName = line.getMethod().getName();
            java.lang.String[] items = pat.split(methodName);
            int i = items.length - 2;
            if (i < 0) {
                i = 0;
            }
            java.lang.StringBuffer fileName = new java.lang.StringBuffer();
            int s = items[i].indexOf("$");
            if (s >= 0) {
                fileName.append(items[i].substring(0, s));
            } else {
                fileName.append(items[i]);
            }
            fileName.append(".java");
            java.util.List lines = (java.util.List) getJspContext().getVariableResolver().resolveVariable("lines");
            if (form) {
                out.print("<br/><input type=\'checkbox\' name=\'" + line.getLineName() + "\' ");
                if ((lines != null) && (lines.contains((Object) line.getLineName()))) {
                    out.print("checked=\'yes\'");
                }
                out.println("/>");
            }
            out.print("        at " + methodName);
            out.println("(" + fileName + ":" + line.getLinePK().getLinenumber() + ")");
        } catch (ELException ex) {
            Logger.getLogger("global").log(Level.SEVERE, null, ex);
        }
    }
}
