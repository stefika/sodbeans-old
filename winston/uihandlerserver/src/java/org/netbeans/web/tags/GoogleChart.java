/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */
package org.netbeans.web.tags;

import java.awt.Color;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;

/**
 *
 * @author Jindrich Sedek
 */
public abstract class GoogleChart extends SimpleTagSupport {

    private static final String GET = "get";
    private static final int MAX_RESOLUTION = 300000;
    protected static final String AMP = "&amp;";
    protected static final String DATA_SEPARATOR = ",";
    protected static final String COLORS_SEPARATOR = ",";
    protected static final String TITLE_SEPARATOR = "|";
    protected static final String SERIE_SEPARATOR = "|";
    protected static final String EMPTY_COLORS_LIST = "";
    protected String collection;
    protected String title = "";
    protected String resolution = "500x200";
    protected String categoryMethodName;
    protected String valueMethodName;
    protected String serieMethodName;
    protected String lblFormat;
    protected String colorsColl;
    protected LegendPossition legendPosition;
    protected boolean showlegend = false;
    protected boolean showXAxisLabel = true;
    protected boolean showYAxisLabel = true;

    /**
     * Called by the container to invoke this tag. 
     * The implementation of this method is provided by the tag library developer,
     * and handles all tag processing, body iteration, etc.
     */
    @Override
    public void doTag() throws JspException {

        JspWriter out = getJspContext().getOut();

        try {
            out.println(prepareResultString());
        } catch (java.io.IOException ex) {
            throw new JspException(ex.getMessage(), ex);
        }
    }

    abstract String prepareResultString();

    protected String getColorsString(int size) {
        Object input = null;
        if (colorsColl != null){
            input = getJspContext().getAttribute(colorsColl);
        }
        if (input == null) {
            input = getDefaultColors(size);
        }
        if (!(input instanceof Collection)) {
            throw new IllegalArgumentException("Colors must be a collection of java.awt.Color");
        }
        String result = EMPTY_COLORS_LIST;
        Collection coll = (Collection) input;
        Iterator it = coll.iterator();
        while (it.hasNext()) {
            Object object = it.next();
            if (!(object instanceof Color)) {
                throw new IllegalArgumentException("Colors must be a collection of java.awt.Color");
            }
            Color col = (Color) object;
            result = result.concat(toHexString(col.getRed())).concat(toHexString(col.getGreen())).concat(toHexString(col.getBlue()));
            if (it.hasNext()) {
                result = result.concat(COLORS_SEPARATOR);
            }
        }
        return result;
    }

    private String toHexString(int i) {
        String result = Integer.toHexString(i);
        if (i < 16) { // always two chars
            result = "0" + result;
        }
        return result;
    }

    private List<Color> getDefaultColors(int size) {
        List<Color> colors = new ArrayList<Color>(size);
        float step = 1.0f / size;
        for (int i = 0; i < size; i++){
            colors.add(Color.getHSBColor(step * i, 0.6f, 0.7f));
        }
        return colors;
    }

    protected Object getKey(Object obj) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        Object result = invokeMethod(obj, categoryMethodName);
        if (result == null) {
            throw new IllegalArgumentException("There is no getter for category parametter: " + categoryMethodName);
        }
        return result;
    }

    protected Long getLongValue(Object value) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        if (value instanceof Number) {
            Number num = (Number) value;
            return num.longValue();
        } else {
            throw new IllegalArgumentException("Value is not a Number type");
        }
    }

    protected Long getValue(Object obj) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        Object value = invokeMethod(obj, valueMethodName);
        if (value == null) {
            throw new IllegalArgumentException("There is no getter for value parametter: " + valueMethodName);
        }
        return getLongValue(value);
    }

    protected Object getSerie(Object obj) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        Object result = invokeMethod(obj, serieMethodName);
        if (result == null) {
            throw new IllegalArgumentException("There is no getter for serie parametter: " + serieMethodName);
        }
        return result;
    }

    private Object invokeMethod(Object obj, String methodName) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        Method[] methods = obj.getClass().getMethods();
        for (Method method : methods) {
            if (method.getName().equalsIgnoreCase(methodName)) {
                return method.invoke(obj, new Object[0]);
            }
        }
        return null;
    }

    public void setCollection(String collection) {
        this.collection = collection;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @param resolution the resolution to set
     */
    public void setResolution(String resolution) {
        String[] pieces = resolution.split("x");
        int width = Integer.parseInt(pieces[0]);
        int height = Integer.parseInt(pieces[1]);
        if (width * height > MAX_RESOLUTION){
            throw new AssertionError("Max resolution "+ MAX_RESOLUTION +"px overflow");
        }
        this.resolution = resolution;
    }

    /**
     * @param category the category to set
     */
    public void setCategory(String category) {
        this.categoryMethodName = GET + category;
    }

    /**
     * @param value the value to set
     */
    public void setValue(String value) {
        this.valueMethodName = GET + value;
    }

    /**
     * @param value the value to set
     */
    public void setSerie(String serie) {
        this.serieMethodName = GET + serie;
    }

    public void setShowxaxislabel(Boolean showXAxis){
        this.showXAxisLabel = showXAxis;
    }

    public void setShowyaxislabel(Boolean showYAxis){
        this.showYAxisLabel = showYAxis;
    }
    /**
     * @param lblFormat the lblFormat to set
     */
    public void setLblformat(String lblFormat) {
        this.lblFormat = lblFormat;
    }

    public void setColors(String colorsColl) {
        this.colorsColl = colorsColl;
    }

    /**
     * @param showlegend the showlegend to set
     */
    public void setLegendposition(String position) {
        this.legendPosition = LegendPossition.valueOf(position.toUpperCase());
    }

    /**
     * @param showlegend the showlegend to set
     */
    public void setShowlegend(boolean showlegend) {
        this.showlegend = showlegend;
    }

    protected static enum LegendPossition {
        LEFT, RIGHT, TOP, BOTTOM;

        public String googleValue(){
            return this.toString().toLowerCase().substring(0, 1);
        }

        public boolean isVertical(){
            return this.equals(LEFT) || this.equals(RIGHT);
        }

    }

}
