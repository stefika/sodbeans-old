/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */
package org.netbeans.web.tags;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Map.Entry;
import static org.netbeans.web.tags.GoogleChart.LegendPossition.*;

/**
 *
 * @author Jindrich Sedek
 */
public final class BarChart extends GoogleChart {

    private static final Integer COLUMN_SPACE = 4;
    private static final Integer SERIE_SPACE = 6;
    private static final int GRAPH_BORDERS = 50;
    private static final String GOOGLE_URL = "<img src='http://chart.apis.google.com/chart?" +
            "chdlp=%10$s&amp;" + // legend possition
            "chbh=%9$s," + COLUMN_SPACE + "," + SERIE_SPACE + "&amp;" + // column width
            "chxl=%8$s&amp;" + // x-axis code
            "chds=0,%7$s&amp;" + // scale
            "chco=%6$s&amp;" + // colors
            "cht=%5$s&amp;" + // title
            "chs=%4$s&amp;" + // resolution
            "%2$s" + // legend
            "chd=t:%1$s&amp;" + // data
            "chxt=x,y&amp;" + // axis
            "chtt=%3$s' " + // title
            "alt='%3$s'/>";
    private Orientation orientation = Orientation.VERTICAL;
    private boolean stacked = false;

    private Long maxScale = null;
    public BarChart() {
        lblFormat = "%1$s";
        showlegend = true;
    }

    @Override
    String prepareResultString() {
        String type = "b";
        if (orientation.equals(orientation.HORIZONTAL)) {
            type = type.concat("h");
            if (legendPosition == null){
                legendPosition = RIGHT;
            }
        } else {
            type = type.concat("v");
            if ( legendPosition == null){
                legendPosition = TOP;
            }
        }
        if (stacked) {
            type = type.concat("s");
        } else {
            type = type.concat("g");
        }
        StringBuffer dataBuffer = new StringBuffer();
        StringBuffer titleBuffer = new StringBuffer();
        Object input = getJspContext().getAttribute(collection);
        Map<Object, SerieData> series = convert(input);
        if (series.size() == 0){ // no data to show
            return "";
        }
        Collection<Object> keys = getKeys(series);
        Iterator<Entry<Object, SerieData>> it = series.entrySet().iterator();
        if (maxScale == null){
            maxScale = scale(series);    
        }
        if (maxScale == 0){ // just zero data
            return "";
        }
        while (it.hasNext()) {
            Entry<Object, SerieData> entry = it.next();
            String values = entry.getValue().getValueList(keys, maxScale);
            Long sum = entry.getValue().getSum(keys);
            titleBuffer.append(String.format(lblFormat, entry.getKey(), sum * 100 / maxScale));
            dataBuffer.append(values);
            if (it.hasNext()) {
                dataBuffer.append(SERIE_SEPARATOR);
                titleBuffer.append(TITLE_SEPARATOR);
            }
        }
        String legend;
        if (showlegend){
            legend = "chdl=" + titleBuffer + AMP;
        }else{
            legend = "";
        }
        String result = String.format(GOOGLE_URL, dataBuffer, legend, title,
                resolution, type, getColorsString(series.size()), 100,
                getKeyLabels(keys), getColumnWidth(keys.size(), series.size()),
                legendPosition.googleValue());
        return result;
    }

    Long scale(Map<Object, SerieData> series) {
        Long result = 0l;
        if (stacked) {
            Map<Object, Long> sums = new HashMap<Object, Long>();
            for (SerieData serieData : series.values()) {
                for (Entry<Object, Long> entry : serieData.data.entrySet()) {
                    Long previous = sums.get(entry.getKey());
                    Long newValue = entry.getValue();
                    if (previous != null){
                        newValue += previous;
                    }
                    sums.put(entry.getKey(), newValue);
                }
            }
            for (Long value : sums.values()) {
                result = Math.max(result, value);
            }
        } else {
            for (SerieData serieData : series.values()) {
                for (Long value : serieData.data.values()) {
                    result = Math.max(result, value);
                }
            }
        }
        return result;
    }

    private int getColumnWidth(int keysCount, int seriesCount) {
        String[] ress = resolution.split("x");
        int width;
        if (orientation.equals(Orientation.VERTICAL)) {
            width = Integer.parseInt(ress[0]);
            if (legendPosition.isVertical()){
                width = width * 70 / 100;
            }
        } else {
            width = Integer.parseInt(ress[1]);
            if (!legendPosition.isVertical()){
                width = width * 70 / 100;
            }
        }
        width = width - GRAPH_BORDERS;
        if (stacked) {
            width = width - (COLUMN_SPACE * keysCount);
            return width / keysCount - 1;
        } else {
            width = width - (SERIE_SPACE * seriesCount) - (COLUMN_SPACE * keysCount * seriesCount);
            return width / keysCount / seriesCount - 1;
        }
    }

    private String getKeyLabels(Collection<Object> keys) {
        String str;
        if (orientation.equals(Orientation.VERTICAL)) {
            str = "0:";
            str = str + TITLE_SEPARATOR;
            Iterator it = keys.iterator();
            while (it.hasNext()) {
                str = str.concat(it.next().toString()).concat(TITLE_SEPARATOR);
            }
        } else {
            str = "";
            Iterator it = keys.iterator();
            while (it.hasNext()) {
                str = it.next().toString().concat(TITLE_SEPARATOR).concat(str);
            }
            str = "1:".concat(TITLE_SEPARATOR).concat(str);
        }
        return str;
    }

    private Collection<Object> getKeys(Map<Object, SerieData> series) {
        Collection<Object> result = new LinkedHashSet<Object>();
        for (SerieData serieData : series.values()) {
            result.addAll(serieData.data.keySet());
        }
        return result;
    }

    Map<Object, SerieData> convert(Object input) {
        if (!(input instanceof Collection)) {
            throw new IllegalArgumentException("collection attribute should be a collection type");
        }
        Collection coll = (Collection) input;
        Map<Object, SerieData> series = new LinkedHashMap<Object, SerieData>();
        for (Object object : coll) {
            try {
                Object serie = getSerie(object);
                SerieData data = series.get(serie);
                if (data == null) {
                    data = new SerieData(getKey(object), getValue(object));
                } else {
                    data.add(getKey(object), getValue(object));
                }
                series.put(serie, data);
            } catch (Exception conversion) {
                throw new IllegalArgumentException("Parametters were not set correctly", conversion);
            }
        }
        return series;
    }

    public void setOrientation(String orientation) {
        this.orientation = Orientation.valueOf(orientation.toUpperCase());
    }

    public void setStacked(boolean stacked) {
        this.stacked = stacked;
    }

    public void setMaxscale(Integer maxscale) {
        this.maxScale = new Long(maxscale);
    }

    static class SerieData {

        LinkedHashMap<Object, Long> data;

        public SerieData(Object key, Long value) {
            data = new LinkedHashMap<Object, Long>();
            data.put(key, value);
        }

        private void add(Object key, Long value) {
            data.put(key, value);
        }

        long getSum(Collection<Object> keys){
            long result = 0;
            for (Object object : keys) {
                Long value = data.get(object);
                if (value != null) {
                    result += value;
                }
            }
            return result;
        }

        String getValueList(Collection<Object> keys, Long maxScale) {
            String result = "";
            for (Object object : keys) {
                Long value = data.get(object);
                if (value == null) {
                    result = result.concat("-1") + DATA_SEPARATOR;
                } else {
                    value = value * 100  / maxScale;
                    result = result.concat(value.toString()) + DATA_SEPARATOR;
                }
            }
            return result.substring(0, result.length() - 1);
        }
    }

    private static enum Orientation {

        VERTICAL, HORIZONTAL
    }
}