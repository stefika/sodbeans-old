/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */
package org.netbeans.web.tags;

import java.io.IOException;
import java.io.Writer;
import javax.servlet.jsp.tagext.*;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import org.netbeans.modules.exceptions.utils.BugReporterFactory;
import org.netbeans.server.uihandler.bugs.ReporterUtils;
import org.netbeans.modules.exceptions.entity.Exceptions;
import org.netbeans.modules.exceptions.entity.Report;
import org.netbeans.server.uihandler.api.Issue;

/**
 *
 * @author  Jan Horvath
 * @version
 */

public class IssuezillaHandler extends SimpleTagSupport {
    
    public static final String ISSUEZILLA_DETAIL = ReporterUtils.getURLPrefix() + "/issues/show_bug.cgi?id=";
    public static final String ISSUEZILLA_ENTER_BUG = "admin/izredirect?";
    
    private Exceptions exceptions;
    private Report report;
    private boolean lazy = false;
    
    
    @Override
    public void doTag() throws JspException {
        JspWriter out=getJspContext().getOut();
        try {
            if (exceptions != null && report == null) {
                report = exceptions.getReportId();
            }
            if (report != null) {
                if (lazy) {
                    out.println("<span class='issuezilla' title='" + report.getId() + "'><i>-loading-</i></span>");
                } else {
                    generateHTML(out, report);
                }
            } else {
                throw new NullPointerException("report has a null value");
            }
            
            JspFragment f=getJspBody();
            if (f != null) {
                f.invoke(out);
            }
            
        } catch (java.io.IOException ex) {
            throw new JspException(ex.getMessage());
        }
        
    }


    public static void generateHTML(Writer out, Report report) throws IOException {
        Integer issueId = report.getIssueId();
        Issue issue = issueId == null ? null : BugReporterFactory.getDefaultReporter().getIssue(issueId);
        if (issue == null || issue.getId() == 0) {
            if (report.inIssuezillaTransfer()) {
                out.write("in transfer\n");
            } else {
                out.write("<A HREF='");
                out.write(ISSUEZILLA_ENTER_BUG + "reportid=" + report.getId());
                out.write("'>enter</A>\n");
            }
        } else {
            boolean strike = false;
            String issueStatus = null;
            issueStatus = issue.getIssueStatus();
            if ("RESOLVED".equals(issueStatus) ||                          // NOI18N
            "VERIFIED".equals(issueStatus) ||                               // NOI18N
            "CLOSED".equals(issueStatus))  {                                // NOI18N
                strike = true;
            }
            if (strike) {
                out.write("<STRIKE>\n");
            }
            out.write("<A HREF='");
            out.write(ISSUEZILLA_DETAIL + issue.getId());
            out.write("'>" + issue.getId());
            out.write("\n</A>\n");
            if (strike) {
                out.write("</STRIKE>\n");
            }
        }
    }

    public void setExceptions(Exceptions exceptions) {
        this.exceptions = exceptions;
    }

    public void setReport(Report report) {
        this.report = report;
    }

    public void setLazy(boolean lazy) {
        this.lazy = lazy;
    }
}
