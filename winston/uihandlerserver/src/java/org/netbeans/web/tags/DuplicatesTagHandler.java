/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */
package org.netbeans.web.tags;

import java.io.IOException;
import javax.persistence.EntityManager;
import javax.servlet.jsp.tagext.*;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import org.netbeans.modules.exceptions.entity.Report;
import org.netbeans.modules.exceptions.entity.Submit;
import org.netbeans.web.Persistable;
import org.netbeans.web.Persistable.TransactionResult;
import org.netbeans.web.Utils;

/**
 *
 * @author  Jan Horvath
 * @version
 */

public class DuplicatesTagHandler extends SimpleTagSupport {
    private static final int MAX_WIDTH = 60;
    private static final String COLOR = "#EE6B00";
    
    /**Called by the container to invoke this tag.
     * The implementation of this method is provided by the tag library developer,
     * and handles all tag processing, body iteration, etc.
     */
    private Submit submit;
    private Report report;

    /**Called by the container to invoke this tag.
     * The implementation of this method is provided by the tag library developer,
     * and handles all tag processing, body iteration, etc.
     */
    @Override
    public void doTag() throws JspException {
        
        JspWriter out=getJspContext().getOut();
        
        try {            
            final int[] dup = new int[1];
            if (submit != null && report == null) {
                report = submit.getReportId();
            }
            if (report != null) {
                Utils.processPersistable(new Persistable.Query() {

                    public TransactionResult runQuery(EntityManager em) throws Exception {
                        dup[0] = report.getDuplicates(em);
                        return TransactionResult.NONE;
                    }
                });
                
            } else {
                throw new NullPointerException("report is null");
            }
            printBar(out, dup[0]);
            JspFragment f=getJspBody();
            if (f != null) {
                f.invoke(out);
            }
            
        } catch (java.io.IOException ex) {
            throw new JspException(ex.getMessage());
        }
        
    }

    public void setSubmit(Submit submit) {
        this.submit = submit;
    }

    public void setReport(Report report) {
        this.report = report;
    }
    
    
    private void printBar(JspWriter out, int d) throws IOException {
        int width = d * MAX_WIDTH / 15;
        if (width >= MAX_WIDTH) {
            width = MAX_WIDTH - 1;
        }
        out.print("<input type='hidden' value='" + Utils.getFixedLengthString(d) + "'/>");
        out.println("<div style='border-style:solid;" +
                "border-color:" + COLOR + ";border-width:1px;width:" + MAX_WIDTH + "px;height:10px;z-index:2;'>");
        out.print("<div style='border-style:solid;border-color:" + COLOR + ";" +
                "border-width:1px;background-color:" + COLOR + ";width:" + width + "px;height:8px;z-index:1;'/>");
        out.println("<div style='zindex:3;position:relative;top:-3px;'>" + d + "</div></div>"); //position:relative;top:-14px;
    }
}
