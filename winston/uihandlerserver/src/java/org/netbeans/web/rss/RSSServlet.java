/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.netbeans.web.rss;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.netbeans.modules.exceptions.entity.Exceptions;
import org.netbeans.modules.exceptions.utils.Utils;

/**
 *
 * @author honza
 */
public class RSSServlet extends HttpServlet {

    public static final String EXC_URL = "/exception.do?id=";
    private static final int NLINES = 5;
    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");

    private static String baseUrl = null;
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/xml;charset=UTF-8");
        PrintWriter out = response.getWriter();
        RSSFeed feed = new RSSFeed();
        RSSHeader header = new RSSHeader();
        header.setTitle("Exception Reports");
        feed.setHeader(header);
        List<Exceptions> exceptions = (List<Exceptions>) request.getAttribute("exceptions");

            List<RSSEntry> entries = new ArrayList<RSSEntry> ();
            for (Exceptions exc : exceptions) {
                RSSEntry entry = new RSSEntry();
                entry.setLink(getExcUrl(request, exc.getId()));
                entry.setGuid(getExcUrl(request, exc.getId()));
                entry.setTitle("#" + exc.getId() + " " + exc.getSummary());

                entry.setDescription(Utils.getStacktraceLines(exc, NLINES, "<br/>\n"));
                entry.setPubDate(DATE_FORMAT.format(exc.getReportdate()));
                entries.add(entry);
            }
            feed.setEntries(entries);
        try {
            RSSWriter.write(feed, out);
        } catch (Exception ex) {
            Logger.getLogger(RSSServlet.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            out.close();
        }
    }

    public String getExcUrl(HttpServletRequest request, int id) throws ServletException {
        if (baseUrl == null) {
            StringBuffer urlBase = new StringBuffer("http://");
            urlBase.append(request.getServerName());
            if (request.getServerPort() != 80) {
                urlBase.append(":" + request.getServerPort());
            }
            urlBase.append(request.getContextPath());
            urlBase.append(EXC_URL);
            baseUrl = urlBase.toString();
        }
        return baseUrl + id;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
