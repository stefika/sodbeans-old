/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */
package org.netbeans.web;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.TreeSet;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.netbeans.lib.uihandler.LogRecords;
import org.netbeans.modules.exceptions.entity.Logfile;
import org.netbeans.modules.exceptions.entity.Report;
import org.netbeans.modules.exceptions.entity.Submit;
import org.netbeans.modules.exceptions.utils.PersistenceUtils;
import org.netbeans.server.uihandler.Exceptions;
import org.netbeans.server.uihandler.IZInsertion;
import org.netbeans.server.uihandler.PreparedParams;
import org.netbeans.server.uihandler.api.Authenticator.AuthToken;
import org.netbeans.web.Persistable.TransactionResult;

/**
 *
 * @author Jan Horvath
 * @version
 */
public class IssuezillaRedirect extends HttpServlet implements Comparator<Submit> {

    private static final Logger LOG = Logger.getLogger(IssuezillaRedirect.class.getName());

    /** Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        final List<String> errors = new ArrayList<String>();

        Integer tempId = null;
        try {
            tempId = new Integer(request.getParameter("reportid"));
        } catch (NumberFormatException e) {
            LOG.log(Level.SEVERE, "Unable to parse reportid parameter ("
                    + request.getParameter("reportid") + ")", e);
        }
        final Principal principal = ((HttpServletRequest) request).getUserPrincipal();
        if (principal == null) {
            throw new IllegalStateException("User should be logged in");
        }
        final int id = tempId;
        Utils.processPersistable(new Persistable.Transaction() {

            public TransactionResult runQuery(EntityManager em) throws Exception {
                Report report = em.getReference(Report.class, id);
                if (!PersistenceUtils.isExistingComponent(report.getComponent(), report.getSubcomponent())) {
                    errors.add("This report has incorrect component or subcomponent, please update it before reporting.");
                    return TransactionResult.ROLLBACK;
                }
                Integer issuezillaId = report.getIssueId();
                if ((issuezillaId == null) || (issuezillaId.intValue() == 0)) {
                    report.preloadSubmitCollection(em);
                    List<Submit> submts = report.getSubmitCollection();
                    TreeSet<Submit> ts = new TreeSet<Submit>(IssuezillaRedirect.this);
                    ts.addAll(submts);
                    Submit last = ts.last();
                    AuthToken token = null;
                    try {
                        token = getUserAuthToken(last.getLogfileId());
                    } catch (IOException ioe) {
                        LOG.log(Level.SEVERE, "Unable to open source file", ioe);
                    }
                    String message = "This issue was reported manually by " + principal.getName() +".\n";
                    message += "It already has " + report.getDuplicates(em) + " duplicates \n";
                    new IZInsertion(last, token, principal.getName(), message).startForceInsert(em);
                }
                return TransactionResult.COMMIT;
            }
        });
        if (errors.isEmpty()) {
            response.sendRedirect("/analytics/detail.do?id=" + id);
        } else {
            request.setAttribute("error", errors.iterator().next());
            response.sendRedirect("/analytics/error.jsp");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
    // </editor-fold>

    public int compare(Submit o1, Submit o2) {
        return o1.getId().compareTo(o2.getId());
    }

    private AuthToken getUserAuthToken(Logfile lf) throws IOException{
        File file = lf.getLoggerFile();
        if (file == null){
            return null;
        }
        InputStream is = new BufferedInputStream(new FileInputStream(file));
        final AuthToken[] tokenHolder = new AuthToken[1];
        try{
            LogRecords.scan(is, new Handler() {

                @Override
                public void publish(LogRecord rec) {
                    if (Exceptions.USER_CONFIGURATION.equals(rec.getMessage()) && rec.getParameters() != null){
                        PreparedParams pp = PreparedParams.prepare(rec.getParameters(), null);
                        tokenHolder[0] = pp.getUserToken();
                    }
                }

                @Override
                public void flush() {}

                @Override
                public void close() throws SecurityException {}
            });
        }finally{
            is.close();
        }
        return tokenHolder[0];
    }
}
