/*
 * UpdateBuildNumbers.java
 *
 * Created on 16 April 2007, 17:39
 */

package org.netbeans.web;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

/**
 *
 * @author honza
 * @version
 */
public class UpdateBuildNumbers extends HttpServlet {
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        out.println("<html>");
        out.println("<head>");
        out.println("<title>Servlet UpdateBuildNumbers</title>");
        out.println("</head>");
        out.println("<body>");
        out.println("<h1>Servlet UpdateBuildNumbers at " + request.getContextPath () + "</h1>");
        
//        List excList = PersistenceUtils.getInstance().getAll(Exceptions.class);
//        for (Iterator it = excList.iterator(); it.hasNext();) {
//            Exceptions exc = (Exceptions) it.next();
//            String prodVersion = exc.getProductversion();
//            
//            try {
//                Long b = new Long(Utils.getCustomBuildFormat(Utils.getBuildNumber(prodVersion)));
//                exc.setBuild(b);
//                out.println(exc.getProductversion() + " = " + b + "<br/>");
//                PersistenceUtils.getInstance().merge(exc);
//                out.flush();
//            } catch (NumberFormatException e) {
//            }
//        }

        out.println("</body>");
        out.println("</html>");

        out.close();
    }
    
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
    
    /**
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
    
    /**
     * Returns a short description of the servlet.
     */
    public String getServletInfo() {
        return "Short description";
    }
    // </editor-fold>
}
