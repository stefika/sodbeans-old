/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.netbeans.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.logging.Handler;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.netbeans.lib.uihandler.Decorable;
import org.netbeans.lib.uihandler.Decorations;
import org.netbeans.modules.exceptions.entity.Submit;
import org.netbeans.modules.exceptions.utils.LoggerUtils;
import org.netbeans.modules.exceptions.utils.PersistenceUtils;

/**
 *
 * @author vanek
 */
public class SimpleDetailLogger extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    static final Logger LOG = Logger.getLogger(SimpleDetailLogger.class.getName());

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        Integer id = null;
        Integer ide = null;


        try {
            id = new Integer(request.getParameter("id"));
            ide = new Integer(request.getParameter("ide"));
        } catch (NumberFormatException e) {
        }


        try {
            if (id != null) {
                EntityManager em = PersistenceUtils.getInstance().createEntityManager();
                H handler = new H();
                Submit submit = Submit.getById(em, id);
                LoggerUtils.getLogRecord(submit, handler);
                em.close();

                int iter = 1;
                iter = ide - 1;

                List<LogNode> listNodes = handler.getNodes();
                LogRecord rd = listNodes.get(iter).getRecord();

                Object[] params = rd.getParameters();
                if (params != null) {
                    publishParams(out, params);
                }

                if (params == null && rd.getThrown() != null) {
                    publishStackTrace(out, rd.getThrown().getStackTrace(), rd.getThrown().getMessage());

                }
            }
        } finally {
            out.close();

        }
    }

    public static class LogNode implements Decorable {

        private String name;
        private String displayName;
        private String iconBaseWithExtension;
        private String shortDescription;
        private LogRecord record;

        private LogNode(LogRecord record) {
            this.record = record;
        }

        public static LogNode create(LogRecord record) {
            LogNode result = new LogNode(record);
            Decorations.decorate(record, result);
            return result;
        }

        public String getDisplayName() {
            return displayName;
        }

        public void setDisplayName(String displayName) {
            this.displayName = displayName;
        }

        public String getIconBaseWithExtension() {
            return iconBaseWithExtension;
        }

        public void setIconBaseWithExtension(String iconBaseWithExtension) {
            this.iconBaseWithExtension = iconBaseWithExtension;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getShortDescription() {
            return shortDescription;
        }

        public void setShortDescription(String shortDescription) {
            this.shortDescription = shortDescription;
        }

        public LogRecord getRecord() {
            return record;
        }
    }

    class H extends Handler {

        private ArrayList<LogNode> nr = new ArrayList<LogNode>();

        public void publish(LogRecord record) {
            nr.add(LogNode.create(record));
        }

        public void flush() {
        }

        public void close() throws SecurityException {
        }

        private List<LogNode> getNodes() {
            return nr;
        }
    }

    public void publishStackTrace(PrintWriter pw, StackTraceElement[] stackTrace, String excMessage) {
        String oldFileName = null;
        pw.println("<pre class=\"unbroken\">" + excMessage);

        for (int x = 0; x < stackTrace.length; x++) {
            oldFileName = null;
            oldFileName = stackTrace[x].getFileName();
            String name = null;

            StringTokenizer st = new StringTokenizer(oldFileName, "/");
            name = null;

            while (st.hasMoreElements()) {
                name = st.nextToken();
            }
            pw.println("     at " + stackTrace[x].getClassName() + "." + stackTrace[x].getMethodName() + "(" + name + ":" + stackTrace[x].getLineNumber() + ")");
        }
            pw.println("</pre>");

    }

    public void publishParams(PrintWriter pw, Object[] params) {
        for (int i = 0; i < params.length; i++) {
            Object object = params[i];
            if (object != null) {
                if (i == 0) {
                    pw.println("<pre class=\"unbroken\">" + object.toString());
                } else {
                    pw.println(object.toString() + "");
                }
            }
        }
            pw.println("</pre>");

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
