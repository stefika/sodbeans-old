/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2008 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */

package org.netbeans.web;

import java.io.*;
import java.util.logging.LogRecord;
import javax.servlet.*;
import javax.servlet.http.*;
import org.netbeans.modules.exceptions.utils.LoggerUtils;
import org.netbeans.modules.exceptions.utils.PersistenceUtils;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.GZIPInputStream;
import javax.persistence.EntityManager;
import org.apache.catalina.connector.ClientAbortException;
import org.netbeans.lib.uihandler.LogRecords;
import org.netbeans.modules.exceptions.entity.Submit;

/**
 *
 * @author honza
 * @version
 */
public class LoggerAsXML extends HttpServlet {
    private static Logger logger = Logger.getLogger(LoggerAsXML.class.getName());

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Integer id = null;
        try {
            id = new Integer(request.getParameter("id"));
        } catch (NumberFormatException e) {
        }
        if (id != null) {
            EntityManager em = PersistenceUtils.getInstance().createEntityManager();
            Submit exceptions = Submit.getById(em, id);
            InputStream is = LoggerUtils.getLoggerAsInputStream(exceptions);
            em.close();

            response.setContentType("text/xml;charset=UTF-8");
            String fileName = String.format("uilog-%1$s.xml", id.toString());
            response.setHeader( "Content-Disposition", "attachment; filename=" + fileName );
            OutputStream out = response.getOutputStream();
            InputStream buffGunzipped = new BufferedInputStream(new GZIPInputStream(is));
            PrintWriter output = new PrintWriter(out);
            try{
                output.println("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
                output.println("<uigestures>");
                output.flush();

                LogRecords.scan(buffGunzipped, new MyHandler(out));
                output.println("</uigestures>");
                output.flush();
            }catch(DataSubmitException dse){
                logger.log(Level.WARNING, dse.getMessage());
            }finally{
                buffGunzipped.close();
                output.close();
            }

            out.close();
        }
    }

    private class MyHandler extends Handler {

        static final String USER_CONFIGURATION = "UI_USER_CONFIGURATION"; // NOI18N
        OutputStream output;

        public MyHandler(OutputStream oStream) {
            output = oStream;
        }

        public void publish(LogRecord record) {
            if (USER_CONFIGURATION.equals(record.getMessage())) {
                Object[] params = record.getParameters();
                if (params != null && params.length >= 7) {
                    ArrayList<Object> list = new ArrayList<Object>( Arrays.asList(params));
                    list.remove(6);
                    record.setParameters(list.toArray());
                }
            }
            try {
                LogRecords.write(output, record);
            } catch (ClientAbortException cle){
                throw new DataSubmitException("Client has aborded XML log upload", cle);
            } catch (IOException ex) {
                logger.log(Level.SEVERE, "PUBLISHING LOG RECORD", ex);
            }
        }

        public void flush() {
            try {
                output.flush();
            } catch (IOException exception) {
                logger.log(Level.SEVERE, "FLUSHING SERVLET OUTPUT", exception);
            }
        }

        public void close() throws SecurityException {
            try {
                output.close();
            } catch (IOException exception) {
                logger.log(Level.SEVERE, "CLOSING SERVLET OUTPUT", exception);
            }
        }
    }

    private class DataSubmitException extends RuntimeException{

        public DataSubmitException(String msg, ClientAbortException cle) {
            super(msg, cle);
        }
    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     */
    public String getServletInfo() {
        return "Short description";
    }
    // </editor-fold>
}
