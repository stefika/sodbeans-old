/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */
package org.netbeans.web.action;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.persistence.EntityManager;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForward;
import org.netbeans.modules.exceptions.entity.Directuser;
import org.netbeans.modules.exceptions.utils.PersistenceUtils;
import org.netbeans.web.Persistable;
import org.netbeans.web.Persistable.TransactionResult;
import org.netbeans.web.Utils;

/**
 *
 * @author michal
 */
public class DirectUserAction extends org.apache.struts.action.Action {

    /* forward name="success" path="" */
    private final static String SUCCESS = "success";

    /**
     * This is the action called from the Struts framework.
     * @param mapping The ActionMapping used to select this instance.
     * @param form The optional ActionForm bean for this request.
     * @param request The HTTP Request we are processing.
     * @param response The HTTP Response we are processing.
     * @throws java.lang.Exception
     * @return
     */
    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            final HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        final String id = request.getParameter("id");
        final String adUser = request.getParameter("adUser");
        final String delUser = request.getParameter("delUser");
        final String modUser = request.getParameter("modUser");
        final String newUser = request.getParameter("newUser");
        final String adPrefix = request.getParameter("adPrefix");
        final String nPrefix = request.getParameter("newPrefix");

        Utils.processPersistable(new Persistable.Transaction() {

            public TransactionResult runQuery(EntityManager em) throws Exception {
                if (id != null) {
                    List<String> recordObject = PersistenceUtils.executeQuery(em, "select d.name from Directuser d where d.name='" + id + "'", null);
                    List<String> foundUser = stringList(recordObject);
                    request.setAttribute("record", foundUser);
                }

                if (adUser != null) {
                    Directuser d = new Directuser();
                    d.setName(adUser);
                    d.setMessagePrefix(adPrefix);
                    em.persist(d);
                }


                if (delUser != null) {
                    String hqlDelete = "delete Directuser where name = :oldName";
                    em.createQuery(hqlDelete).setParameter("oldName", delUser).executeUpdate();
                }

                if (modUser != null && newUser != null) {
                    String hqlUpdate = "update Directuser set name = :newName where name = :oldName";
                    em.createQuery(hqlUpdate).setParameter("newName", newUser).setParameter("oldName", modUser).executeUpdate();
                }

                if (modUser != null && nPrefix != null) {
                    String hqlUpdate = "update Directuser set message_prefix = :newPref where name = :oldName";
                    em.createQuery(hqlUpdate).setParameter("newPref", nPrefix).setParameter("oldName", modUser).executeUpdate();
                }

                List<Directuser> usersObject = PersistenceUtils.getAll(em, Directuser.class);

                List<Duser> all = allUsers(usersObject);
                request.setAttribute("allUsers", all);
                return TransactionResult.COMMIT;
            }
        });
        return mapping.findForward(SUCCESS);
    }

    private List<String> stringList(List<String> usersObject) {
        List<String> tmp = new ArrayList<String>();
        for (Iterator<String> it = usersObject.iterator(); it.hasNext();) {
            String userName = it.next();
            if (userName.length() <= 0 || !Character.isDigit(userName.charAt(0))) {
                it.remove();
            }
            tmp.add(userName);
        }
        return tmp;
    }

    private List<Duser> allUsers(List<Directuser> usersObject) {
        List<Duser> tmp = new ArrayList<Duser>();
        for (int i = 0; i < usersObject.size(); i++) {
            tmp.add(new Duser(usersObject.get(i).getName(), usersObject.get(i).getMessagePrefix()));
        }
        return tmp;
    }

    public static class Duser {

        private String name;
        private String prefix;

        public Duser(String name, String prefix) {
            this.name = name;
            this.prefix = prefix;
        }

        /**
         * @return the name
         */
        public String getName() {
            return name;
        }

        /**
         * @param name the name to set
         */
        public void setName(String name) {
            this.name = name;
        }

        /**
         * @return the prefix
         */
        public String getPrefix() {
            return prefix;
        }

        /**
         * @param prefix the prefix to set
         */
        public void setPrefix(String prefix) {
            this.prefix = prefix;
        }
    }
}
