/*
 * StacktraceAction.java
 *
 * Created on 20 April 2007, 13:22
 */

package org.netbeans.web.action;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForward;
import org.netbeans.modules.exceptions.entity.Exceptions;
import org.netbeans.modules.exceptions.entity.Line;
import org.netbeans.modules.exceptions.entity.Stacktrace;
import org.netbeans.modules.exceptions.utils.PersistenceUtils;
/**
 *
 * @author honza
 * @version
 */

public class StacktraceAction extends ExceptionsAbstractAction {
    
    /* forward name="success" path="" */
    private final static String ERROR = "error";
    private final static String LIST_QUERY_RESULT = "result";
    private final static String STACKTRACE = "stacktrace";
    static final Logger LOG = Logger.getLogger(StacktraceAction.class.getName());
    
    /**
     * This is the action called from the Struts framework.
     * @param mapping The ActionMapping used to select this instance.
     * @param form The optional ActionForm bean for this request.
     * @param request The HTTP Request we are processing.
     * @param response The HTTP Response we are processing.
     * @throws java.lang.Exception
     * @return
     */
    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm  form,
            final HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        LOG.fine(this.getClass().getName() + ".execute()");
        Integer requestId = null;
        try {
            requestId = new Integer(request.getParameter("id"));
        } catch (NumberFormatException e) {
        }
        if (requestId == null) {
            request.setAttribute("error", "Id is not set you have probably used incorrect or incomplete link");
            return mapping.findForward(ERROR);
        }
        final int id = requestId;
        EntityManager em = getEntityManager(request);
        Exceptions exceptions = em.find(Exceptions.class, id);
        request.setAttribute("exceptions", exceptions);

        ArrayList<Line> lines = new ArrayList<Line>();
        Enumeration en = request.getParameterNames();
        while (en.hasMoreElements()) {
            String name = (String) en.nextElement();
            if (name.matches("[0-9]+_[0-9]+")) {
                lines.add(Line.getLineByName(em, name));
            }
        }
        request.setAttribute("lines", lines);

        StringBuffer sb = new StringBuffer();
        Iterator<Line> it = lines.iterator();
        while(it.hasNext()){
            Line line = it.next();
            sb.append(line.getMethod().getId());
            if (it.hasNext()) {
                sb.append(", ");
            }
        }

        List<Integer> list;
        if (!lines.isEmpty()) {
            list = (List<Integer>) PersistenceUtils.getInstance()
                .executeNativeQuery("select stacktrace_id from (select distinct stacktrace_id, method_id from line " +
                "where method_id in (" + sb + ")) tmp group by stacktrace_id having count(method_id) = "  +
                lines.size());
        } else {
            list = Collections.emptyList();
        }

        Set<Exceptions> result = new HashSet<Exceptions>();
        // get stacktraces by id -- temporary solution
        for (Iterator<Integer> stackId = list.iterator(); stackId.hasNext();) {
            Integer key = stackId.next();
            Stacktrace st = em.find(Stacktrace.class, key);
            if ((st == null) || (st.getExceptions() == null)) {
                continue;
            }
            result.add(st.getExceptions());
        }
        
        if (result.isEmpty()){
            return mapping.findForward(STACKTRACE);
        }
        request.setAttribute("exceptions", result);
        return mapping.findForward(LIST_QUERY_RESULT);
        
    }
}
