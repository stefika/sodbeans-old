/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */
package org.netbeans.web.action;

import java.text.NumberFormat;
import javax.persistence.EntityManager;
import org.netbeans.modules.exceptions.entity.Operatingsystem;

/**
 *
 * @author Jindrich Sedek
 */
public final class SlownBean implements Comparable<SlownBean>{

    private final String action;
    private final double max;
    private final double avg;
    private final long count;
    private final int osId;
    private Operatingsystem os;

    public SlownBean(String action, int osId, int max, double avg, long count) {
        this.action = action;
        this.max = max;
        this.avg = avg;
        this.count = count;
        this.osId = osId;
    }

    /**
     * @return the action
     */
    public String getAction() {
        return action;
    }

    /**
     * @return the max
     */
    public String getMax() {
        if (max == 0){
            return "";
        }
        return getNF().format(max / 1000);
    }

    /**
     * @return the avg
     */
    public String getAvg() {
        if (avg == 0){
            return "";
        }
        return getNF().format(avg / 1000);
    }

    /**
     * @return the count
     */
    public String getCount() {
        if (count != 0){
            return Long.toString(count);
        }
        return "-";
    }

    public Operatingsystem getOs(EntityManager em){
        if (os == null){
            os = em.find(Operatingsystem.class, osId);
        }
        return os;
    }

    @Override
    public String toString() {
        return getAction() + ":max(" + getMax() + "),avg(" + getAvg() + "),count(" + count + ")";
    }

    public int compareTo(SlownBean o) {
        return action.compareTo(o.action);
    }
    
    public NumberFormat getNF(){
        NumberFormat format = NumberFormat.getInstance();
        format.setMaximumFractionDigits(2);
        format.setMinimumFractionDigits(2);
        return format;
    }
}
