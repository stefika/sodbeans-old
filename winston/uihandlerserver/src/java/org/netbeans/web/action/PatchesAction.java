/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */
package org.netbeans.web.action;

import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForward;
import org.netbeans.modules.exceptions.entity.Nbversion;
import org.netbeans.modules.exceptions.entity.Patch;
import org.netbeans.modules.exceptions.utils.PersistenceUtils;
import org.netbeans.web.Persistable;
import org.netbeans.web.Utils;

/**
 *
 * @author Jindrich Sedek
 */
public class PatchesAction extends Action {

    /* forward name="success" path="" */
    private final static String SUCCESS = "success";

    static final Logger LOG = Logger.getLogger(PatchesAction.class.getName());
    
    /**
     * This is the action called from the Struts framework.
     * @param mapping The ActionMapping used to select this instance.
     * @param form The optional ActionForm bean for this request.
     * @param request The HTTP Request we are processing.
     * @param response The HTTP Response we are processing.
     * @throws java.lang.Exception
     * @return
     */
    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        LOG.fine(this.getClass().getName() + ".execute()");
        final String patchName = request.getParameter("patch_name");
        final String moduleName = request.getParameter("module_name");
        final String specVersion = request.getParameter("spec_version");
        final String nbVersion = request.getParameter("version");
        String drop = request.getParameter("drop");
        if (patchName != null) {
            request.setAttribute("last_patch_name", patchName);
        }
        if (nbVersion != null) {
            request.setAttribute("last_version", nbVersion);
        }
        if ((patchName != null) && (moduleName != null) && (specVersion != null) && (nbVersion != null)) {
            Utils.processPersistable(new Persistable.Transaction() {
                public TransactionResult runQuery(EntityManager em) {
                    Nbversion entity = (Nbversion) PersistenceUtils.getExist(em, "Nbversion.findByVersion", nbVersion);
                    Patch newPatch = new Patch(patchName, moduleName, specVersion, entity);
                    em.persist(newPatch);
                    return TransactionResult.COMMIT;
                }
            });
        } else {
            if (moduleName != null) {
                request.setAttribute("last_module_name", moduleName);
            }
            if (specVersion != null) {
                request.setAttribute("last_spec_version", specVersion);
            }
        }
        if (drop != null) {
            final Integer dropId = Integer.parseInt(drop);
            Utils.processPersistable(new Persistable.Transaction() {

                public TransactionResult runQuery(EntityManager em) {
                    Patch removedPatch = em.find(Patch.class, dropId);
                    em.remove(removedPatch);
                    return TransactionResult.COMMIT;
                }
            });
        }
        request.setAttribute("patches", PersistenceUtils.getInstance().getAll(Patch.class));
        request.setAttribute("versions", PersistenceUtils.getInstance().getAll(Nbversion.class));
        return mapping.findForward(SUCCESS);

    }
}
