/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */
package org.netbeans.web.action;

import java.security.Principal;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForward;
import org.netbeans.modules.exceptions.entity.Comment;
import org.netbeans.modules.exceptions.entity.Exceptions;
import org.netbeans.modules.exceptions.entity.Logfile;
import org.netbeans.modules.exceptions.entity.Nbuser;
import org.netbeans.modules.exceptions.entity.Nbversion;
import org.netbeans.modules.exceptions.entity.Patch;
import org.netbeans.modules.exceptions.entity.Submit;
import org.netbeans.modules.exceptions.utils.LoggerUtils;
import org.netbeans.modules.exceptions.utils.PersistenceUtils;
import org.netbeans.server.uihandler.statistics.NetBeansModules;
import org.netbeans.server.uihandler.statistics.NetBeansModules.ModuleInfo;
import org.netbeans.web.SimpleDetailLogger.*;

/**
 *
 * @author Jan Horvath
 * @version
 */
public class DetailAction extends ExceptionsAbstractAction {

    private final static String SUCCESS = "success";
    private final static String PANEL = "panel";
    private final static String LOGGER = "logger";
    private final static String ERROR = "error";
    static final Logger LOG = Logger.getLogger(DetailAction.class.getName());
    private final static String PLEASE_WAIT = "pleasewait";

    /**
     * This is the action called from the Struts framework.
     * @param mapping The ActionMapping used to select this instance.
     * @param form The optional ActionForm bean for this request.
     * @param request The HTTP Request we are processing.
     * @param response The HTTP Response we are processing.
     * @throws java.lang.Exception
     * @return
     */
    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        LOG.log(Level.FINE, "{0}.execute()", this.getClass().getName());
        Integer id = null;
        Integer newduplicateof = getInteger(request, "duplicateof", null);
        Integer newissuezillaid = getInteger(request, "issuezillaid", null);
        String component = request.getParameter("component");
        String subcomponent = request.getParameter("subcomponent");
        String comment = request.getParameter("comment");
        EntityManager em = getEntityManager(request);

        try {
            String idParam = request.getParameter("id");
            if (idParam == null) {
                idParam = (String) request.getAttribute("id");
            }
            if (idParam == null) {
                request.setAttribute("error", "Issue id is not set - you have probably used wrong link");
                return mapping.findForward(ERROR);
            }
            id = new Integer(idParam);
        } catch (NumberFormatException e) {
            request.setAttribute("error", "Issue id is not set correctly - you have probably used wrong link");
            return mapping.findForward(ERROR);
        }
        Submit submit = Submit.getById(em, id);
        if (submit == null) {
            LOG.log(Level.WARNING, "Cannot find exceptions instance with id {0}", id);
            response.setHeader("Refresh", "30"); // refresh the page in 30s
            return mapping.findForward(PLEASE_WAIT);
        }

        // make updates
        boolean merge = false;
        if ((newissuezillaid != null) && (submit != null)) {
            submit.getReportId().setIssueId(newissuezillaid, getClass());
            submit.getReportId().setIssueManChanged(true);
            merge = true;
        }

        if ((newduplicateof != null) && (submit != null)) {
            Submit duplExc = null;
            if (newduplicateof != null) {
                duplExc = Submit.getById(em, newduplicateof);
            }
            submit.setReportId(duplExc.getReportId());
            submit.getReportId().setDuplicateManChanged(true);
            merge = true;
        }

        if ((subcomponent != null) && (subcomponent.length() > 0) && (component != null) && (component.length() > 0)) {
            submit.getReportId().setComponent(component);
            submit.getReportId().setSubcomponent(subcomponent);
            submit.getReportId().setComponentManChanged(true);
            merge = true;
        }

        Comment commentEntity = null;
        if ((comment != null) && (comment.length() > 0)) {
            commentEntity = new Comment();
            commentEntity.generateId();
            commentEntity.setComment(comment);
            Principal p = request.getUserPrincipal();
            if (p != null) {
                List<? extends Object> params = Collections.<String>singletonList(p.getName());
                Map paramMap = Collections.singletonMap("name", params);
                Collection<Nbuser> col = PersistenceUtils.getInstance().find(Nbuser.class, paramMap);
                if (!col.isEmpty()) {
                    Nbuser nbuser = col.iterator().next();
                    commentEntity.setNbuserId(nbuser);
                }
                commentEntity.setSubmitId(submit);
                submit.getCommentCollection().add(commentEntity);
            }
            merge = true;
        }

        if (merge) {
            if (checkPrivileges(request)) {
                em.getTransaction().begin();
                if (commentEntity != null) {
                    em.persist(commentEntity);
                }
                em.merge(submit);
                em.getTransaction().commit();
            } else {
                return loginRedirect(request);
            }
            response.sendRedirect("exception.do?id=" + submit.getId());
        }
        request.setAttribute("isException", submit instanceof Exceptions);
        submit.getReportId().preloadSubmitCollection(em);
        request.setAttribute("submit", submit);
        request.setAttribute("components", PersistenceUtils.getInstance().getComponents().getComponentsSet());

        request.setAttribute("memory", submit.getLogfileId().getUserMemory(em));
        request.setAttribute("cpu_count", submit.getLogfileId().getUserCPUCount(em));
        request.setAttribute("profiler_stats", submit.getLogfileId().getProfilerStatistics(em));
        
        if (request.getParameter("panel") != null) {
            int width = Integer.parseInt(request.getParameter("window_width"));
            int height = Integer.parseInt(request.getParameter("window_height"));
            request.setAttribute("window_width", width * 7 / 10 - 100);
            request.setAttribute("window_height", height * 7 / 10 - 100);
            return mapping.findForward(PANEL);
        }

        String logger = request.getParameter("logger");
        if (submit != null) {
            H handler = new H();
            LoggerUtils.getLogRecord(submit, handler);
            if (logger != null) {
                ArrayList<org.netbeans.modules.exceptions.utils.LoggerLine> lines = new ArrayList<org.netbeans.modules.exceptions.utils.LoggerLine>();
                for (Iterator<LogRecord> it = handler.nr.iterator(); it.hasNext();) {
                    LogRecord record = it.next();
                    lines.add(LoggerUtils.getLine(record));
                }
                request.setAttribute("logger", lines);
                return mapping.findForward(LOGGER);
            }
            if (submit instanceof Exceptions) {
                Exceptions exceptions = (Exceptions) submit;
                request.setAttribute("patches", getPatches(em, exceptions, handler.nr));
            }
            request.setAttribute("detailList", prepareLogger(submit));
        }

        return mapping.findForward(SUCCESS);
    }

    protected List<String> getPatches(EntityManager em, Exceptions exceptions, List<LogRecord> records) {

        Logfile logfile = exceptions.getLogfileId();
        Nbversion version = logfile.getProductVersionId().getNbversionId();
        Map<String, Object> params = Collections.singletonMap("nbversion", (Object) version);
        List<Patch> patches = PersistenceUtils.executeNamedQuery(em, "Patch.findNbVersion", params, Patch.class);
        NetBeansModules modules = new NetBeansModules();
        modules.setRememberDistinctVersions(true);
        List<String> installedPatches = new ArrayList<String>();
        if ((patches != null) && (patches.size() > 0)) {
            NetBeansModules.NBMData data = null;
            for (Iterator<LogRecord> it = records.iterator(); it.hasNext();) {
                data = modules.join(data, modules.process(it.next()));
            }
            if (data != null) {
                for (Patch patche : patches) {
                    ModuleInfo info = data.getEnabled(patche.getModulename());
                    String specVersion = patche.getSpecversion();
                    Integer[] moduleVersion = NetBeansModules.getModuleVersion(specVersion);
                    if ((info != null) && (moduleVersion != null) && (info.count(moduleVersion) > 0)) {
                        installedPatches.add(patche.getPatchname() + ": &nbsp; " + patche.getModulename());
                    }
                }
            }
        }
        return installedPatches;
    }

    private List<LoggerLine> prepareLogger(Submit subm) { //HttpServletRequest request,
        List<LoggerLine> excList = new ArrayList<LoggerLine>();

        HlogNode handler = new HlogNode();
        LoggerUtils.getLogRecord(subm, handler);
        int linkOrder = 1;


        String displName = null;

        for (LogNode node : handler.getNodes()) {
            displName = node.getDisplayName();
            long timestamp = node.getRecord().getMillis();
            String time = DateFormat.getTimeInstance(DateFormat.SHORT).format(new Date(timestamp));
            if (displName == null) {
                displName = node.getName();
            }
            String icon = node.getIconBaseWithExtension();
            if (icon == null) {
                icon = "org/netbeans/lib/uihandler/exception.gif";
            }
            LoggerLine logLine =  new LoggerLine(icon, displName, time, linkOrder, subm.getId());
            excList.add(logLine);

            linkOrder++;
        }
        return excList;
    }

    //  */

    class H extends Handler {

        public ArrayList<LogRecord> nr = new ArrayList<LogRecord>();

        public void publish(LogRecord arg0) {
            nr.add(arg0);
        }

        public void flush() {
        }

        public void close() throws SecurityException {
        }
    }

    class HlogNode extends Handler {

        private ArrayList<LogNode> nr = new ArrayList<LogNode>();

        public void publish(LogRecord record) {
            nr.add(LogNode.create(record));
        }

        public void flush() {
        }

        public void close() throws SecurityException {
        }

        private List<LogNode> getNodes() {
            return nr;
        }
    }

    public static final class LoggerLine {

        private static final int DISPLAY_NAME_MAX_LENGTH = 50;
        private final String icon;
        private final String name;
        private final int order;
        private final int id;
        private final String time;

        public LoggerLine(String icon, String name, String time, int order, int id) {
            if ((name != null) && name.length() > DISPLAY_NAME_MAX_LENGTH){
                name = name.substring(0, DISPLAY_NAME_MAX_LENGTH);
            }
            this.icon = icon;
            this.order = order;
            this.id = id;
            this.name = name;
            this.time = time;
        }

        public String getIcon() {
            return icon;
        }

        public int getId() {
            return id;
        }

        public String getName() {
            return name;
        }

        public int getOrder() {
            return order;
        }

        public String getTime() {
            return time;
        }
    }
}
