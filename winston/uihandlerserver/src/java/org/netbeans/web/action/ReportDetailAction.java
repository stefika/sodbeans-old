/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2008 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */
package org.netbeans.web.action;

import java.security.Principal;
import java.util.Collections;
import java.util.Date;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForward;
import org.netbeans.modules.exceptions.entity.Exceptions;
import org.netbeans.modules.exceptions.entity.Nbuser;
import org.netbeans.modules.exceptions.entity.Report;
import org.netbeans.modules.exceptions.entity.ReportComment;
import org.netbeans.modules.exceptions.utils.PersistenceUtils;
import org.netbeans.server.uihandler.Utils;

/**
 *
 * @author Jan Horvath
 */
public class ReportDetailAction extends ExceptionsAbstractAction {

    /* forward name="success" path="" */
    private final static String SUCCESS = "success";
    private final static String PLEASE_WAIT = "pleasewait";
    private final static String ERROR = "error";
    private final static Logger LOG = Logger.getLogger(ReportDetailAction.class.getName());

    /**
     * This is the action called from the Struts framework.
     * @param mapping The ActionMapping used to select this instance.
     * @param form The optional ActionForm bean for this request.
     * @param request The HTTP Request we are processing.
     * @param response The HTTP Response we are processing.
     * @throws java.lang.Exception
     * @return
     */
    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        EntityManager em = getEntityManager(request);
        int id = getInteger(request, "id", -1);
        assert (id != -1) : "Id was not set";
        Report report = em.find(Report.class, id);
        if (report == null) {
            //workaround for old links from issuezilla
            if (id < 146000) {
                Exceptions exc = em.find(Exceptions.class, id);
                if (exc == null) {
                    LOG.log(Level.SEVERE, "Unable to find Exceptions.id = " + id);
                    request.setAttribute("error", "Exception #" + id + "doesn't exist");
                    return mapping.findForward(ERROR);
                }
                report = exc.getReportId();
            } else {
                Logger.getLogger(ReportDetailAction.class.getName()).warning("Cannot find report instance with id " + id);
                response.setHeader("Refresh", "30"); // refresh the page in 30s
                return mapping.findForward(PLEASE_WAIT);
            }
        }
        if (report == null) {
            request.setAttribute("error", "Report #" + id + " doesn't exist");
            return mapping.findForward(ERROR);
        }
        report.preloadSubmitCollection(em);
        request.setAttribute("report", report);

        request.setAttribute("jdks", report.getJDKS(em));
        request.setAttribute("oss", report.getOSes(em));
        request.setAttribute("versions", report.getVersions(em));
        request.setAttribute("components", PersistenceUtils.getInstance().getComponents().getComponentsSet());

        String comment = request.getParameter("comment");
        if ((comment != null) && (comment.length() > 0)) {
            if (checkPrivileges(request)) {
                Principal p = request.getUserPrincipal();
                if (p != null) {
                    addComment(em, p.getName(), report, comment);
                }else{
                    return loginRedirect(request);
                }
            } else {
                return loginRedirect(request);
            }
        }

        return mapping.findForward(SUCCESS);
    }

    void addComment(EntityManager em, String userName, Report report, String comment) {
        em.getTransaction().begin();
        ReportComment rc = new ReportComment(Utils.getNextId(ReportComment.class));
        rc.setComment(comment);
        rc.setReportId(report);
        rc.setCommentDate(new Date());
        rc.setNbuserId(getNbUser(em, userName));
        em.persist(rc);
        em.getTransaction().commit();
    }

    private Nbuser getNbUser(EntityManager em, String userName) {
        Map<String, Object> params = Collections.singletonMap("name", (Object) userName);
        Nbuser user = (Nbuser) PersistenceUtils.executeNamedQuerySingleResult(em, "Nbuser.findByName", params);
        if (user == null){
            user = new Nbuser(Utils.getNextId(Nbuser.class));
            user.setName(userName);
            em.persist(user);
        }
        return user;
    }
}
