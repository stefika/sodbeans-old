/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */
package org.netbeans.web.action;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForward;
import org.netbeans.modules.exceptions.utils.BugReporterFactory;
import org.netbeans.modules.exceptions.entity.Report;
import org.netbeans.modules.exceptions.entity.Submit;
import org.netbeans.modules.exceptions.utils.PersistenceUtils;
import org.netbeans.server.services.beans.Components;
import org.netbeans.web.EntityManagerFilter;

/**
 *
 * @author Jindrich Sedek
 */
public class DuplicatesQuery extends ExceptionsAbstractAction {

    private final static String RESULT = "result";
    private final static String QUERY = "query";
    private final static String ERROR = "error";
    private final static Logger LOG = Logger.getLogger(DuplicatesQuery.class.getName());
    private final static int MAX_RESULT = 500;
    private final static int FETCH_SIZE = 100;
    private final static String REPORTS_QUERY_BASE =
            "SELECT DISTINCT r from Exceptions e, Report r WHERE e.reportId = r";
    private final static String EXCEPTIONS_QUERY_BASE =
            "SELECT DISTINCT e from Exceptions e, Report r WHERE e.reportId = r";
    private final static String DUPS_WITH_DTS =
            " (SELECT COUNT(e2) from Exceptions e2 where e2.reportId = r AND e2.reportdate BETWEEN :dupFrom AND :dupTo) ";
    private final static String DUPS_WITHOUT_DTS =
            " (SELECT COUNT(e2) from Exceptions e2 where e2.reportId = r) ";
    private final static String DUPS_COUNT_WITH_DATES_QUERY =  DUPS_WITH_DTS + " > :min AND " + DUPS_WITH_DTS + " <= :max ";
    private final static String DUPS_COUNT_WITHOUT_DATES_QUERY = DUPS_WITHOUT_DTS + " >= :min AND " + DUPS_WITHOUT_DTS + " <= :max ";
    private final static String DATES_QUERY = " e.reportdate BETWEEN :from AND :to ";
    private final static String COMPONENTS_QUERY = " r.component IN (:components) ";
    private final static String SUBCOMPONENTS_QUERY = " r.subcomponent IN (:subcomponents) ";
    private final static String IN_IZ = " (r.issueId IS NOT NULL AND r.issueId != 0)";
    private final static String NOT_IN_IZ = " (r.issueId IS NULL OR r.issueId = 0)";
//    private final static String QUERY_ORDER = " ORDER BY e.id ";
    private final static DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");

    /**
     * This is the action called from the Struts framework.
     * @param mapping The ActionMapping used to select this instance.
     * @param form The optional ActionForm bean for this request.
     * @param request The HTTP Request we are processing.
     * @param response The HTTP Response we are processing.
     * @throws java.lang.Exception
     * @return
     */
    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        String submit = request.getParameter("submit");
        if (submit == null) {
            setQueryAttributes(request, false);
            return mapping.findForward(QUERY);
        }

        String[] components = request.getParameterValues("component");
        String[] subComponents = request.getParameterValues("subcomponent");
        final String status = request.getParameter("status");
        final String iz_status = request.getParameter("iz_status");
        final boolean showDuplicates = !isEmpty(request.getParameter("showDuplicates"));
        final boolean useDates = !isEmpty(request.getParameter("useDates"));
        boolean allSubcomponents = !isEmpty(request.getParameter("allsubcomponents"));
        if (allSubcomponents){
            subComponents = null;
        }
        String searchType = request.getParameter("search_type");
        SearchType type;
        if (searchType != null){
            type = SearchType.valueOf(searchType);
        } else {
            type = SearchType.EXCEPTIONS;
        }
        String duplicatesMinStr = request.getParameter("minimum");
        String duplicatesMaxStr = request.getParameter("maximum");
        String creationFromStr = request.getParameter("creationFrom");
        String creationToStr = request.getParameter("creationTo");
        String duplicatesCreationFromStr = request.getParameter("duplicatesCreationFrom");
        String duplicatesCreationToStr = request.getParameter("duplicatesCreationTo");

        final List<Submit> result = new ArrayList<Submit>();
        try {
            Long duplicatesMin = parseLong(duplicatesMinStr);
            Long duplicatesMax = parseLong(duplicatesMaxStr);
            final Map<String, Object> params = new HashMap<String, Object>(10);
            params.put("min", duplicatesMin);
            params.put("max", duplicatesMax);
            if ((components != null) && (components.length > 0)){
                params.put("components", Arrays.asList(components));
            }
            if ((subComponents != null) && (subComponents.length > 0)){
                params.put("subcomponents", Arrays.asList(subComponents));
            }
            if (useDates){
                Date creationFrom = parseDate(creationFromStr, getLastSixMonts());
                Date creationTo = parseDate(creationToStr, new Date());
                Date creationDuplicatesFrom = parseDate(duplicatesCreationFromStr, getLastSixMonts());
                Date creationDuplicatesTo = parseDate(duplicatesCreationToStr, new Date());
                params.put("from", creationFrom);
                params.put("to", creationTo);
                params.put("dupFrom", creationDuplicatesFrom);
                params.put("dupTo", creationDuplicatesTo);
            }
            EntityManager em = (EntityManager) request.getAttribute(EntityManagerFilter.ENTITY_MANAGER);

            result.addAll(duplicatesQuery(em, type, params, status, iz_status, showDuplicates));

        } catch (ParseException exc) {
            LOG.log(Level.INFO, "Incorrect date format", exc);
            request.setAttribute("error", exc.getMessage());
            return mapping.findForward(ERROR);
        } catch (NumberFormatException exc) {
            LOG.log(Level.INFO, "Incorrect number format", exc);
            request.setAttribute("error", exc.getMessage());
            return mapping.findForward(ERROR);
        }
        if (result.isEmpty()){
            setQueryAttributes(request, true);
            return mapping.findForward(QUERY);
        }
        request.setAttribute("exceptions", result);
        return mapping.findForward(RESULT);
    }

    void setQueryAttributes(HttpServletRequest request, boolean afterEmptyReport){
        Components components = PersistenceUtils.getInstance().getComponents();
        request.setAttribute("empty_result", afterEmptyReport);
        request.setAttribute("components", components.getComponentsSet());
        if (afterEmptyReport){
            setAttributeFromParam(request, "minimum");
            setAttributeFromParam(request, "maximum");
            setAttributeFromParam(request, "creationFrom");
            setAttributeFromParam(request, "creationTo");
            setAttributeFromParam(request, "duplicatesCreationFrom");
            setAttributeFromParam(request, "duplicatesCreationTo");
            setAttributeFromParam(request, "iz_status");
            setAttributeFromParam(request, "status");
            setAttributeFromParam(request, "showDuplicates");
            setAttributeFromParam(request, "allsubcomponents");
            request.setAttribute("selectedComponents", request.getParameterValues("component"));
            request.setAttribute("selectedSubcomponents", request.getParameterValues("subcomponent"));
        }else{
            request.setAttribute("minimum", 0);
            request.setAttribute("maximum", 500);
            request.setAttribute("creationFrom", dateFormat.format(getLastSixMonts()));
            request.setAttribute("creationTo", dateFormat.format(new Date()));
            request.setAttribute("duplicatesCreationFrom", dateFormat.format(getLastSixMonts()));
            request.setAttribute("duplicatesCreationTo", dateFormat.format(new Date()));
            request.setAttribute("iz_status", "ALL");
            request.setAttribute("status", "both");
            request.setAttribute("showDuplicates", null);
            request.setAttribute("allsubcomponents", null);
        }
        request.setAttribute("useDates", null);
    }

    void setAttributeFromParam(HttpServletRequest request, String name){
        request.setAttribute(name, request.getParameter(name));
    }
    private Date getLastSixMonts() {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.MONTH, -6);
        return cal.getTime();
    }

    private boolean isEmpty(String str) {
        return ((str == null) || ("".equals(str)));
    }

    private Long parseLong(String str) {
        if (isEmpty(str)){
            return 0l;
        }
        return Long.parseLong(str.trim());
    }

    private Date parseDate(String str, Date defaultValue) throws ParseException {
        if (isEmpty(str)){
            return defaultValue;
        }
        return dateFormat.parse(str.trim());
    }

    Collection<Submit> duplicatesQuery(EntityManager em, SearchType type, Map<String, Object> params, String status, String iz_status, boolean showDuplicates) {
        QueryStr queryStruct;
        if (showDuplicates){
            queryStruct = new QueryStr(EXCEPTIONS_QUERY_BASE);
        }else{
            queryStruct = new QueryStr(REPORTS_QUERY_BASE);
        }
        if (params.containsKey("components")){
            queryStruct.addCondition(COMPONENTS_QUERY);
        }
        if (params.containsKey("subcomponents")){
            queryStruct.addCondition(SUBCOMPONENTS_QUERY);
        }
        if (params.containsKey("to")&&params.containsKey("from")){
            queryStruct.addCondition(DATES_QUERY);
        }
        if (params.containsKey("dupFrom") && params.containsKey("dupTo")){
            queryStruct.addCondition(DUPS_COUNT_WITH_DATES_QUERY);
        }else{
            queryStruct.addCondition(DUPS_COUNT_WITHOUT_DATES_QUERY);
        }
        if ("unreported".equals(status)) {
            queryStruct.addCondition(NOT_IN_IZ);
        } else if ("in issuezilla".equals(status)) {
            queryStruct.addCondition(IN_IZ);
        }
        queryStruct.addOrder();
        Query sbmQuery;
        if (SearchType.EXCEPTIONS.equals(type)){
            sbmQuery = em.createNamedQuery("Exceptions.findLatestByReport");
        } else {
            sbmQuery = em.createNamedQuery("Slowness.findLatestByReport");
            queryStruct.query = queryStruct.query.replaceAll("Exceptions", "Slowness");
        }
        LOG.log(Level.FINE, "QUERY:{0}", queryStruct.query);
        Query mainQuery = em.createQuery(queryStruct.query);
        for (Map.Entry<String, Object> entry : params.entrySet()) {
            mainQuery.setParameter(entry.getKey(), entry.getValue());
        }
        mainQuery.setMaxResults(FETCH_SIZE);
        int firstResult = 0;
        boolean canContinue = true;
        Collection<Submit> result = new HashSet<Submit>(MAX_RESULT);
        while (canContinue){
            LOG.log(Level.INFO, "DupsQuery - already loaded: {0}", result.size());
            mainQuery.setFirstResult(firstResult);
            List<Submit> batch;
            if (showDuplicates){
                batch = mainQuery.getResultList();
            }else{
                List<Report> reps = mainQuery.getResultList();
                batch = new ArrayList<Submit>(reps.size());
                for (Report report : reps) {
                    sbmQuery.setParameter("report", report);
                    Submit exc = (Submit) sbmQuery.getSingleResult();
                    batch.add(exc);
                }
            }
            boolean noMoreResults = (batch.size() < FETCH_SIZE);
            if ("OPEN".equals(iz_status)) {
                filterOpen(batch);
            } else if ("CLOSED".equals(iz_status)) {
                List<Submit> open = new ArrayList<Submit>(batch);
                filterOpen(open);
                batch.removeAll(open);
            }
            result.addAll(batch);
            if (noMoreResults || (result.size() >= MAX_RESULT)){
                canContinue = false;
            }
            firstResult += FETCH_SIZE;
        }
        LOG.log(Level.FINE, "RESULT:{0}", Integer.toString(result.size()));
        return result;
    }

    Collection<Submit> filterOpen(Collection<Submit> candidates){
        if (candidates.isEmpty()){
            return candidates;
        }
        Set<Integer> ids = new HashSet<Integer>();
        for (Submit submit : candidates) {
            assert submit != null;
            if (submit == null){
                continue;
            }
            if (submit.getReportId().isInIssuezilla()){
                ids.add(submit.getReportId().getIssueId());
            }
        }
        Collection<Integer> filtered = BugReporterFactory.getDefaultReporter().filterOpen(ids);

        Iterator<Submit> it = candidates.iterator();
        while (it.hasNext()){
            Submit sbm = it.next();
            Integer bugzillaId = sbm.getReportId().getIssueId();
            if (!filtered.contains(bugzillaId)){
                it.remove();
            }
        }
        return candidates;
    }

    private class QueryStr {

        private String query;

        public QueryStr(String queryBase) {
            this.query = queryBase;
        }

        void addCondition(String cond) {
            query = query + " AND " + cond;
        }

        private void addOrder() {
//            query = query + QUERY_ORDER;
        }
    }
    enum SearchType {

        EXCEPTIONS, SLOWNESS
    }
}
