/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */
package org.netbeans.web.action;

import java.security.Principal;
import javax.persistence.EntityManager;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.RedirectingActionForward;
import org.netbeans.server.uihandler.bugs.ReporterUtils;
import org.netbeans.web.EntityManagerFilter;
import org.netbeans.web.tags.JstlFunctions;

/**
 *
 * @author honza
 */
public abstract class ExceptionsAbstractAction extends Action{
    protected final static String LOGIN = "login";
    public final static String JUNCTION_LOGIN_URL = ReporterUtils.getURLPrefix() + "/people/login?original_uri=";


    protected boolean checkPrivileges(HttpServletRequest request) {
        Principal p = request.getUserPrincipal();
        if (p != null) {
            return true;
        }
        if (!request.getRequestURI().contains("login")){
            request.getSession().setAttribute("logincallback", request.getRequestURI() + "?" + request.getQueryString());
        }
        return false;
    }

    protected ActionForward loginRedirect(HttpServletRequest request) {
        String originalUri = JstlFunctions.getOrigUrl(request);
        return new RedirectingActionForward(JUNCTION_LOGIN_URL + originalUri);
    }
    
    protected EntityManager getEntityManager(HttpServletRequest request) {
        return (EntityManager) request.getAttribute(EntityManagerFilter.ENTITY_MANAGER);
    }

    protected static Integer getInteger(HttpServletRequest request, String name, Integer defaultValue) {
        Integer result;
        try {
            String param = request.getParameter(name);
            if (param == null){
                param = (String) request.getAttribute(name);
            }
            result = Integer.parseInt(param);
        } catch (NumberFormatException e) {
            result = defaultValue;
        }
        return result;
    }

    protected static Long getLong(HttpServletRequest request, String name, Long defaultValue) {
        Long result;
        try {
            result = Long.parseLong(request.getParameter(name));
        } catch (NumberFormatException e) {
            result = defaultValue;
        }
        return result;
    }

    protected static Boolean getBoolean(HttpServletRequest request, String name, Boolean defaultValue) {
        Boolean result;
        try {
            String value = request.getParameter(name);
            result = "on".equals(value)
                  || "true".equals(value);
        } catch (NumberFormatException e) {
            result = defaultValue;
        }
        return result;
    }

}
