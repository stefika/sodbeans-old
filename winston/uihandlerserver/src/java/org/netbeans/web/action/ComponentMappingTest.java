/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */

package org.netbeans.web.action;

import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForward;
import org.netbeans.modules.exceptions.entity.Exceptions;
import org.netbeans.modules.exceptions.entity.Submit;
import org.netbeans.server.componentsmatch.Component;
import org.netbeans.server.componentsmatch.Matcher;
import org.netbeans.server.snapshots.SlownessChecker;
import org.netbeans.web.Persistable;
import org.netbeans.web.Persistable.TransactionResult;
import org.netbeans.web.Utils;

/**
 *
 * @author Jindrich Sedek
 */
public class ComponentMappingTest extends org.apache.struts.action.Action {
    
    /* forward name="success" path="" */
    private final static String SUCCESS = "success";
    
    /**
     * This is the action called from the Struts framework.
     * @param mapping The ActionMapping used to select this instance.
     * @param form The optional ActionForm bean for this request.
     * @param request The HTTP Request we are processing.
     * @param response The HTTP Response we are processing.
     * @throws java.lang.Exception
     * @return
     */
    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm  form,
            final HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        final String className = request.getParameter("class_name");
        final String methodName = request.getParameter("method_name");
        if ((className != null) || (methodName != null)){
            Utils.processPersistable(new Persistable.Query() {

                public TransactionResult runQuery(EntityManager em) {
                    StackTraceElement element = new StackTraceElement(className, methodName, null, 0);
                    Matcher matcher = Matcher.getDefault();
                    Component comp = matcher.match(em, new StackTraceElement[]{element});
                    register(request, comp);
                    return TransactionResult.NONE;
                }
            });
        }
        final String issueIdStr = request.getParameter("issue_id");
        if (issueIdStr != null){
            Utils.processPersistable(new Persistable.Query() {

                public TransactionResult runQuery(EntityManager em) {
                    Integer issueId = Integer.parseInt(issueIdStr);
                    Matcher match = Matcher.getDefault();
                    Submit sbm = Submit.getById(em, issueId);
                    if(sbm == null){
                        register(request, new Component("non existing submit with id", issueId.toString()));
                    }else{
                        Component comp;
                        if (sbm instanceof Exceptions) {
                            comp = match.match(em, ((Exceptions)sbm).getMockThrowable());
                        }else{
                            SlownessChecker checker = new SlownessChecker(em, null, sbm.getLogfileId());
                            comp = checker.getComponentForSlowness();
                        }
                        register(request, comp);
                    }
                    return TransactionResult.NONE;
                }
            });
        }
        return mapping.findForward(SUCCESS);
    }

    private void register(HttpServletRequest request, Component comp){
        Logger.getLogger(ComponentMappingTest.class.getName()).info("registering component");
        if (comp != null){
            request.setAttribute("comp", comp);
        }else{
            request.setAttribute("comp", new Component("null", "null"));
        }
    }
}