/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */
package org.netbeans.web.action;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForward;
import org.netbeans.modules.exceptions.entity.Report;
import org.netbeans.modules.exceptions.utils.PersistenceUtils;
import org.openide.util.RequestProcessor;
import org.openide.util.Task;

/**
 *
 * @author Jan Horvath
 * @version
 */
public class ListAction extends ExceptionsAbstractAction {

    /* forward name="success" path="" */
    private final static String SUCCESS = "success";
    private final static String REPORTS = "reports";
    private final static String QUERY = "query";
    private final static String COUNT = "count";
    private final static String RSS = "rss";
    private final static String DEFAULT_ORDER = "id";
    private final static String COMMAND = "command";
    private final static String DEV = "Dev";
    static final Logger LOG = Logger.getLogger(ListAction.class.getName());
    private final static DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
    private final static Map<ListCommand, QueryExecution> countTasks = new HashMap<ListCommand, QueryExecution>();

    /**
     * This is the action called from the Struts framework.
     * @param mapping The ActionMapping used to select this instance.
     * @param form The optional ActionForm bean for this request.
     * @param request The HTTP Request we are processing.
     * @param response The HTTP Response we are processing.
     * @throws java.lang.Exception
     * @return
     */
    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            final HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        request.setAttribute("startActionTime", new Long(System.currentTimeMillis()));
        boolean count = request.getParameter(COUNT) != null;
        boolean rss = request.getParameter(RSS) != null;

        ListCommand command = new ListCommand(request);
        if (command.isEmpty()) {
            Object o = request.getSession().getAttribute(COMMAND);
            if (o != null) {
                command = (ListCommand) o;
            }
        } else {
            request.getSession().setAttribute(COMMAND, command);
        }
        request.setAttribute(COMMAND, command);

        EntityManager em = getEntityManager(request);

        List<String> versions = PersistenceUtils.executeQuery(em, "select distinct v.version from Nbversion v", null);
        for (Iterator<String> it = versions.iterator(); it.hasNext();) {
            String version = it.next();
            if (version.length() <= 0 || !Character.isDigit(version.charAt(0))) {
                it.remove();
            }
        }
        versions.add(DEV);
        request.setAttribute("versions", versions);

        if (request.getParameter(QUERY) != null || command.isEmpty()) {
            Collection<String> comps = PersistenceUtils.getInstance().getComponents().getComponentsSet();
            request.setAttribute("allComponents", comps);
            return mapping.findForward(QUERY);
        }

        String select;
        if (command.isShowDuplicates()) {
            select = "e";
        } else {
            select = "distinct e.reportId";
        }
        if (count) {
            QueryExecution<Long> execution = null;
            boolean created = false;
            synchronized (countTasks) {
                execution = countTasks.get(command);
                if (execution == null) {
                    while (!countTasks.isEmpty()) {
                        countTasks.wait();
                    }
                    select = "count(" + select + ")";
                    Query q = createQuery(em, select, command);
                    execution = new QueryExecution<Long>(q, false);
                    countTasks.put(command, execution);
                    created = true;
                }
            }
            request.setAttribute(COUNT, execution.getResult());
            if (created) {
                synchronized (countTasks) {
                    countTasks.clear();
                    countTasks.notify();
                }
            }
            return mapping.findForward(COUNT);
        }
        Query query = createQuery(em, select, command);
        query.setMaxResults(2000);

        if (command.isShowDuplicates()) {
            setRssUrl(request);
            if (rss) {
                query.setMaxResults(100);
                request.setAttribute("exceptions", query.getResultList());
                return mapping.findForward(RSS);
            }
            request.setAttribute("exceptions", query.getResultList());
            return mapping.findForward(SUCCESS);
        } else {
            List<Report> result = query.getResultList();
            for (Report report : result) {
                report.preloadSubmitCollection(em);
            }
            request.setAttribute(REPORTS, result);
            return mapping.findForward(REPORTS);
        }
    }

    static Query createQuery(EntityManager em, String select, ListCommand command) {

        StringBuilder result = new StringBuilder("select ");
        result.append(select);
        result.append(" from Exceptions e ");
        StringBuffer condition = new StringBuffer("where ");
        appendStatement(condition, "e.reportId.component IN (:components)", command.getComponents());
        appendStatement(condition, "e.reportId.subcomponent IN (:subcomponents)", command.getSubcomponents());
        appendStatement(condition, "e.logfileId.productVersionId.nbversionId.version IN (:versions)", command.getVersions());
        appendStatement(condition, "e.nbuserId.name = :username", command.getUsername());
        appendStatement(condition, "e.reportDate > :reportDateFrom", command.getReportDateFrom());
        appendStatement(condition, "e.reportDate < :reportDateTo", command.getReportDateTo());
        appendStatement(condition, "e.build > :buildDateFrom", command.getBuildDateFrom());
        appendStatement(condition, "e.build < :buildDateTo", command.getBuildDateTo());
        appendStatement(condition, "(SELECT COUNT(ee) FROM Exceptions ee WHERE ee.reportId.id = e.reportId.id) > :minDuplicates", command.getMinDuplicates());
        appendStatement(condition, "exists (SELECT l.id FROM e.stacktrace.lineCollection l WHERE l.method.name LIKE :line)", command.getStacktraceLine());
        if (condition.length() > 10) {
            result.append(condition);
        }
        result.append(" order by e.id desc");
        Query query = em.createQuery(result.toString());

        setParameter(query, "components", command.getComponents());
        setParameter(query, "subcomponents", command.getSubcomponents());
        setParameter(query, "versions", command.getVersions());
        setParameter(query, "username", command.getUsername());
        setParameter(query, "reportDateFrom", command.getReportDateFrom());
        setParameter(query, "reportDateTo", command.getReportDateTo());
        setParameter(query, "buildDateFrom", command.getBuildDateFrom());
        setParameter(query, "buildDateTo", command.getBuildDateTo());
        if (command.getMinDuplicates() != null){
            setParameter(query, "minDuplicates", command.getMinDuplicates().longValue());
        }
        String line = command.getStacktraceLine();
        if ((line != null) && (line.length() > 0)) {
            query.setParameter("line", line + "%");
        }

        return query;
    }

    private static void setParameter(Query query, String name, Object value) {
        if (value != null) {
            if (!(value instanceof String && value.toString().length() == 0)) {
                query.setParameter(name, value);
            }
        }
    }

    private static void appendStatement(StringBuffer result, String statement, Object value) {
        if (value != null) {
            if (!(value instanceof String && value.toString().length() == 0)) {
                if (!result.toString().trim().endsWith("where")) {
                    result.append("and ");
                }
                result.append(statement);
                result.append(" ");
            }
        }
    }

    private void setRssUrl(HttpServletRequest request) {
        StringBuilder urlBase = new StringBuilder("http://");
        urlBase.append(request.getServerName());
        if (request.getServerPort() != 80) {
            urlBase.append(":" + request.getServerPort());
        }
        urlBase.append(request.getContextPath());
        urlBase.append("/list.do?");
        urlBase.append(request.getQueryString());
        urlBase.append("&rss=1");
        request.setAttribute("rssUrl", urlBase.toString());
    }

    public static class ListCommand {

        List<String> components;
        List<String> subcomponents;
        List<String> versions;
        String username;
        String order;
        Date reportDateFrom;
        Date reportDateTo;
        Long buildDateFrom;
        Long buildDateTo;
        boolean showDuplicates;
        Integer minDuplicates;
        String stacktraceLine;

        public ListCommand(HttpServletRequest request) throws ParseException {
            components = request.getParameterValues("component") == null
                    ? null : Arrays.asList(request.getParameterValues("component"));
            subcomponents = request.getParameterValues("subcomponents") == null
                    ? null : Arrays.asList(request.getParameterValues("subcomponents"));
            versions = request.getParameterValues("versions") == null
                    ? null : Arrays.asList(request.getParameterValues("versions"));
            username = request.getParameter("username");
            order = request.getParameter("order") != null
                    ? request.getParameter("order") : DEFAULT_ORDER;
            reportDateFrom = request.getParameter("reportDateFrom") == null
                    ? null : dateFormat.parse(request.getParameter("reportDateFrom"));
            reportDateTo = request.getParameter("reportDateTo") == null
                    ? null : dateFormat.parse(request.getParameter("reportDateTo"));
            buildDateFrom = getLong(request, "buildDateFrom", null);
            buildDateTo = getLong(request, "buildDateTo", null);
            minDuplicates = getInteger(request, "minDuplicates", null);
            showDuplicates = getBoolean(request, "showDuplicates", false);
            stacktraceLine = request.getParameter("stacktraceLine");
        }

        public String getStacktraceLine() {
            return stacktraceLine;
        }

        public void setStacktraceLine(String stacktraceLine) {
            this.stacktraceLine = stacktraceLine;
        }

        public String getOrder() {
            return order;
        }

        public void setOrder(String order) {
            this.order = order;
        }

        public Long getBuildDateFrom() {
            return buildDateFrom;
        }

        public void setBuildDateFrom(Long buildDateFrom) {
            this.buildDateFrom = buildDateFrom;
        }

        public Long getBuildDateTo() {
            return buildDateTo;
        }

        public void setBuildDateTo(Long buildDateTo) {
            this.buildDateTo = buildDateTo;
        }

        public List<String> getComponents() {
            return components;
        }

        public void setComponents(List<String> components) {
            this.components = components;
        }

        public List<String> getVersions() {
            return versions;
        }

        public void setVersions(List<String> versions) {
            this.versions = versions;
        }

        public Integer getMinDuplicates() {
            return minDuplicates;
        }

        public void setMinDuplicates(Integer minDuplicates) {
            this.minDuplicates = minDuplicates;
        }

        public Date getReportDateFrom() {
            return reportDateFrom;
        }

        public void setReportDateFrom(Date reportDateFrom) {
            this.reportDateFrom = reportDateFrom;
        }

        public Date getReportDateTo() {
            return reportDateTo;
        }

        public void setReportDateTo(Date reportDateTo) {
            this.reportDateTo = reportDateTo;
        }

        public boolean isShowDuplicates() {
            return showDuplicates;
        }

        public void setShowDuplicates(boolean showDuplicates) {
            this.showDuplicates = showDuplicates;
        }

        public List<String> getSubcomponents() {
            return subcomponents;
        }

        public void setSubcomponents(List<String> subcomponents) {
            this.subcomponents = subcomponents;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public boolean isEmpty() {
            return components == null
                    && subcomponents == null
                    && username == null
                    && reportDateFrom == null
                    && reportDateTo == null
                    && buildDateFrom == null
                    && buildDateTo == null
                    && minDuplicates == null
                    && stacktraceLine == null;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final ListCommand other = (ListCommand) obj;
            if (this.components != other.components && (this.components == null || !this.components.equals(other.components))) {
                return false;
            }
            if (this.subcomponents != other.subcomponents && (this.subcomponents == null || !this.subcomponents.equals(other.subcomponents))) {
                return false;
            }
            if (this.versions != other.versions && (this.versions == null || !this.versions.equals(other.versions))) {
                return false;
            }
            if ((this.username == null) ? (other.username != null) : !this.username.equals(other.username)) {
                return false;
            }
            if ((this.order == null) ? (other.order != null) : !this.order.equals(other.order)) {
                return false;
            }
            if (this.reportDateFrom != other.reportDateFrom && (this.reportDateFrom == null || !this.reportDateFrom.equals(other.reportDateFrom))) {
                return false;
            }
            if (this.reportDateTo != other.reportDateTo && (this.reportDateTo == null || !this.reportDateTo.equals(other.reportDateTo))) {
                return false;
            }
            if (this.buildDateFrom != other.buildDateFrom && (this.buildDateFrom == null || !this.buildDateFrom.equals(other.buildDateFrom))) {
                return false;
            }
            if (this.buildDateTo != other.buildDateTo && (this.buildDateTo == null || !this.buildDateTo.equals(other.buildDateTo))) {
                return false;
            }
            if (this.showDuplicates != other.showDuplicates) {
                return false;
            }
            if (this.minDuplicates != other.minDuplicates && (this.minDuplicates == null || !this.minDuplicates.equals(other.minDuplicates))) {
                return false;
            }
            if ((this.stacktraceLine == null) ? (other.stacktraceLine != null) : !this.stacktraceLine.equals(other.stacktraceLine)) {
                return false;
            }
            return true;
        }

        @Override
        public int hashCode() {
            int hash = 7;
            hash = 97 * hash + (this.components != null ? this.components.hashCode() : 0);
            hash = 97 * hash + (this.subcomponents != null ? this.subcomponents.hashCode() : 0);
            hash = 97 * hash + (this.versions != null ? this.versions.hashCode() : 0);
            hash = 97 * hash + (this.username != null ? this.username.hashCode() : 0);
            hash = 97 * hash + (this.order != null ? this.order.hashCode() : 0);
            hash = 97 * hash + (this.reportDateFrom != null ? this.reportDateFrom.hashCode() : 0);
            hash = 97 * hash + (this.reportDateTo != null ? this.reportDateTo.hashCode() : 0);
            hash = 97 * hash + (this.buildDateFrom != null ? this.buildDateFrom.hashCode() : 0);
            hash = 97 * hash + (this.buildDateTo != null ? this.buildDateTo.hashCode() : 0);
            hash = 97 * hash + (this.showDuplicates ? 1 : 0);
            hash = 97 * hash + (this.minDuplicates != null ? this.minDuplicates.hashCode() : 0);
            hash = 97 * hash + (this.stacktraceLine != null ? this.stacktraceLine.hashCode() : 0);
            return hash;
        }
    }

    private static class QueryExecution<T> implements Runnable{

        private final Query q;
        private T result;
        private final boolean multiResults;
        private final Task task;

        private QueryExecution(Query q, boolean multiResults) {
            this.multiResults = multiResults;
            this.q = q;
            this.task = RequestProcessor.getDefault().post(this);
        }

        public void run() {
            if (multiResults) {
                result = (T) q.getResultList();
            } else {
                result = (T) q.getSingleResult();
            }
        }

        public T getResult() {
            task.waitFinished();
            return result;
        }
    }

}
