/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */
package org.netbeans.web.action;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForward;
import org.netbeans.modules.exceptions.entity.Exceptions;
import org.netbeans.modules.exceptions.entity.Slowness;
import org.netbeans.modules.exceptions.utils.PersistenceUtils;
import org.netbeans.server.uihandler.bugs.BugzillaConnector;
import org.netbeans.web.BugReportCounts;
import org.netbeans.web.BugReportCounts.Data;
import org.netbeans.web.Persistable;
import org.netbeans.web.Persistable.TransactionResult;
import org.netbeans.web.Utils;
import org.openide.util.RequestProcessor;

/**
 *
 * @author Jan Horvath
 * @author Jindrich Sedek
 */
public class StatisticsAction extends org.apache.struts.action.Action implements Runnable {

    /* forward name="success" path="" */
    private static final String SUCCESS = "success";
    private static final SimpleDateFormat SDF = new SimpleDateFormat("MM/dd");
    private static final int RECOUNT_INTERVAL = 23 * 1000 * 3600;//23 hours
    private static ArrayList<TableCell> data = null;
    private static List<Data> stats = null;
    static final Logger LOG = Logger.getLogger(StatisticsAction.class.getName());

    /**
     * This is the action called from the Struts framework.
     * @param mapping The ActionMapping used to select this instance.
     * @param form The optional ActionForm bean for this request.
     * @param request The HTTP Request we are processing.
     * @param response The HTTP Response we are processing.
     * @throws java.lang.Exception
     * @return
     */
    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        LOG.log(Level.FINE, "{0}.execute()", StatisticsAction.class.getName());

        request.setAttribute("data", getData());
        request.setAttribute("submitsProgress", getStats());
        return mapping.findForward(SUCCESS);
    }

    private synchronized ArrayList<TableCell> getData() {
        if (data == null) {
            run();
        }
        return data;
    }

    private synchronized List<Data> getStats() {
        if (stats == null){
            run();
        }
        return stats;
    }

    ArrayList<TableCell> recountData(boolean skipIssuezilla) {
        long milis = System.currentTimeMillis();
        Calendar cal = Calendar.getInstance();
        ArrayList<TableCell> pageData = new ArrayList<TableCell>();
        for (int i = 0; i < 12; i++) {
            Date end = cal.getTime();
            cal.add(Calendar.DATE, -7);
            Date start = cal.getTime();
            TableCell cell = new TableCell();
            new Recount(cell, start, end, skipIssuezilla).run();
            pageData.add(cell);
        }
        LOG.log(Level.FINE, "Counting statistic took {0} s", (System.currentTimeMillis() - milis) / 1000);
        return pageData;
    }

    private Number getCountFromInterval(EntityManager em, Date start, Date end, Class type) {
        Map<String, Date> params = new HashMap<String, Date>();
        params.put("start", start);
        params.put("end", end);
        return (Number) PersistenceUtils.executeNamedQuerySingleResult(em, type.getSimpleName() + ".countByInterval", params);
    }

    private static final String countExcsByIntervalWithoutDuplicates = "SELECT COUNT(e.id) FROM Exceptions e"
            + " WHERE e.id <= ALL(SELECT e2.id FROM Exceptions e2 WHERE e2.reportId = e.reportId) AND e.reportdate BETWEEN :start AND :end";
    private static final String countSlownessByIntervalWithoutDuplicates = "SELECT COUNT(s.id) FROM Slowness s "
            + " WHERE s.id <= ALL(SELECT s2.id FROM Slowness s2 WHERE s2.reportId = s.reportId) AND s.reportdate BETWEEN :start AND :end";
    private int getNonDuplicatesCountFromInterval(EntityManager em, Date start, Date end) {
        Number n1 = (Number) execute(em, start, end, countExcsByIntervalWithoutDuplicates);
        Number n2 = (Number) execute(em, start, end, countSlownessByIntervalWithoutDuplicates);
        return n1.intValue() + n2.intValue();
    }

    private static final String findIssuezillaIdsByInterval = "SELECT DISTINCT x.issue_Id FROM (" +
            "(SELECT r.issue_Id FROM Exceptions e,Report r WHERE r.id = e.report_Id AND e.id <= ALL(SELECT e2.id FROM Exceptions e2 WHERE e2.report_id = e.report_id) AND e.reportdate BETWEEN :start AND :end AND r.issue_id IS NOT NULL) " +
            " UNION " +
            "(SELECT r.issue_Id FROM Slowness s,Report r WHERE r.id = s.report_Id AND s.id <= ALL(SELECT s2.id FROM Exceptions s2 WHERE s2.report_id = s.report_id) AND s.reportdate BETWEEN :start AND :end AND r.issue_id IS NOT NULL)" +
            ") AS x";
    private Collection<Integer> getInIssuezillaSet(EntityManager em, Date start, Date end) {
        Query q = em.createNativeQuery(findIssuezillaIdsByInterval).setParameter("start", start).setParameter("end", end);
        return (List<Integer>) q.getResultList();
    }

    private Object execute(EntityManager em, Date start, Date end, String query) {
        Query q = em.createQuery(query).setParameter("start", start).setParameter("end", end);
        return q.getSingleResult();
    }

    public void run() {
        try{
            data = recountData(false);
            stats = BugReportCounts.getStats();
        }finally{
            RequestProcessor.getDefault().post(this, RECOUNT_INTERVAL);
        }
    }

    private final class Recount extends Persistable.Query implements Runnable{

        TableCell tc;
        private final boolean skipIssuezilla;
        public Recount(TableCell cell, Date start, Date end, boolean skipIssuezilla) {
            this.tc = cell;
            this.skipIssuezilla = skipIssuezilla;
            tc.setStart(start);
            tc.setEnd(end);
        }


        public TransactionResult runQuery(EntityManager em) {
            int allExceptions = getCountFromInterval(em, tc.start, tc.end, Exceptions.class).intValue();
            tc.setAllExceptionsReports(allExceptions);
            int allSlowness = getCountFromInterval(em, tc.start, tc.end, Slowness.class).intValue();
            tc.setAllSlownessReports(allSlowness);
            int nonDuplicates = getNonDuplicatesCountFromInterval(em, tc.start, tc.end);
            tc.setNonDuplicateReports(nonDuplicates);
            if (!skipIssuezilla){
                Collection<Integer> inIsuezilla = getInIssuezillaSet(em, tc.start, tc.end);
                inIsuezilla.remove(0);
                tc.setInIssuezilla(inIsuezilla);
            }else{
                tc.setInIssuezilla(Collections.<Integer>emptySet());
            }
            return TransactionResult.NONE;
        }

        public void run() {
            Utils.processPersistable(this);
            BugzillaConnector.Stats stats = BugzillaConnector.getInstance().getFixedCount(tc.inIssuezilla);
            tc.setFixed(stats.getFixed());
            tc.setResolved(stats.getResolved());
        }

    }

    public class TableCell {

        private Date start;
        private Date end;
        private int allExceptions = 0;
        private int allSlownesses = 0;
        private int nonDuplicates = 0;
        private int fixed;
        private int resolved;
        private Collection<Integer> inIssuezilla;

        public String getIssuesURL() {
            StringBuilder sb = new StringBuilder();
            sb.append("http://www.netbeans.org/issues/buglist.cgi?issue_id=");
            for (Integer id : inIssuezilla) {
                sb.append(id);
                sb.append(",");
            }
            return sb.toString();
        }

        public Date getStart() {
            return start;
        }

        public void setStart(Date start) {
            this.start = start;
        }

        public Date getEnd() {
            return end;
        }

        public void setEnd(Date end) {
            this.end = end;
        }

        public int getAllReports() {
            return allExceptions + allSlownesses;
        }

        public int getAllExceptions() {
            return allExceptions;
        }

        public int getAllSlownesses() {
            return allSlownesses;
        }

        private void setAllSlownessReports(int allSlownesses) {
            this.allSlownesses = allSlownesses;
        }

        public void setAllExceptionsReports(int allExceptions) {
            this.allExceptions = allExceptions;
        }

        public int getNonDuplicateReports() {
            return nonDuplicates;
        }

        public void setNonDuplicateReports(int nonDuplicates) {
            this.nonDuplicates = nonDuplicates;
        }

        public String getDateString() {
            return SDF.format(start) + " - " + SDF.format(end);
        }

        public int getFixed() {
            return fixed;
        }

        public int getIssuesCount() {
            return inIssuezilla.size();
        }

        public Collection<Integer> getInIssuezilla() {
            return inIssuezilla;
        }

        public void setFixed(int fixed) {
            this.fixed = fixed;
        }

        public void setInIssuezilla(Collection<Integer> inIssuezilla) {
            this.inIssuezilla = inIssuezilla;
        }

        public int getResolved() {
            return resolved;
        }

        public void setResolved(int resolved) {
            this.resolved = resolved;
        }
    }
}