/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */
package org.netbeans.web.action;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForward;
import org.netbeans.modules.exceptions.entity.Report;
import org.netbeans.modules.exceptions.entity.Submit;
import org.netbeans.web.Persistable;
import org.netbeans.web.Persistable.TransactionResult;
import org.netbeans.web.Utils;

/**
 *
 * @author Jindrich Sedek
 */
public class SplitDuplicates extends ExceptionsAbstractAction {

    /* forward name="success" path="" */
    private final static String SUCCESS = "success";
    private final static String ERROR = "error";
    private static final Logger LOG = Logger.getLogger(SplitDuplicates.class.getName());

    /**
     * This is the action called from the Struts framework.
     * @param mapping The ActionMapping used to select this instance.
     * @param form The optional ActionForm bean for this request.
     * @param request The HTTP Request we are processing.
     * @param response The HTTP Response we are processing.
     * @throws java.lang.Exception
     * @return
     */
    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response) throws Exception {
        LOG.info("starting duplicates splitting");
        if (!checkPrivileges(request)) {
            LOG.info("no privileged user");
            return loginRedirect(request);
        }
        String[] issues = request.getParameterValues("split_duplicates");
        if (issues == null) {
            request.setAttribute("error", "No items were selected");
            return mapping.findForward(ERROR);
        }
        String duplicateOfStr = request.getParameter("duplicate_of").trim();
        List<Submit> redirected = null;
        try {
            if (request.getParameter("new_report") != null) {
                redirected = splitDuplicates(issues);
            } else {
                if (duplicateOfStr == null || "".equals(duplicateOfStr)) {
                    request.setAttribute("error", "The duplicateOf field was not filled");
                    return mapping.findForward(ERROR);
                }
                redirected = splitDuplicates(issues, duplicateOfStr);
            }
        } catch (RuntimeException re) {
            LOG.log(Level.WARNING, re.getMessage(), re);
            request.setAttribute("error", re.getMessage());
            return mapping.findForward(ERROR);
        }
        Report finalReport = redirected.iterator().next().getReportId();
        String finalReportId = finalReport.getId().toString();
        request.setAttribute("id", finalReportId);
        return mapping.findForward(SUCCESS);
    }

    List<Submit> splitDuplicates(final String[] issueNames) {
        final List<Submit> redirected = new ArrayList<Submit>();
        Utils.processPersistable(new Persistable.Transaction() {

            public TransactionResult runQuery(EntityManager em) {
                Report newReport = new Report(org.netbeans.server.uihandler.Utils.getNextId(Report.class));
                em.persist(newReport);
                redirected.addAll(markDuplicates(em, issueNames, newReport, true));
                return TransactionResult.COMMIT;
            }
        });
        return redirected;
    }

    List<Submit> splitDuplicates(final String[] issueNames, String duplicateOfStr) {
        final List<Submit> redirected = new ArrayList<Submit>();
        final Integer duplicateOfId = Integer.parseInt(duplicateOfStr);
        Utils.processPersistable(new Persistable.Transaction() {

            public TransactionResult runQuery(EntityManager em) {
                Report report = em.find(Report.class, duplicateOfId);
                if (report == null){
                    throw new RuntimeException("Report " + duplicateOfId + " doesn't exist.");
                }
                redirected.addAll(markDuplicates(em, issueNames, report, false));
                return TransactionResult.COMMIT;
            }
        });
        return redirected;
    }

    private List<Submit> markDuplicates(EntityManager em, String[] issueNames, Report report, boolean setComponent) {
        if (issueNames.length == 0){
            return Collections.<Submit>emptyList();
        }
        final List<Submit> redirected = new ArrayList<Submit>(issueNames.length);
        final List<Integer> ids = new ArrayList<Integer>(issueNames.length);
        for (String iss : issueNames) {
            Integer duplicateId = Integer.parseInt(iss.trim());
            ids.add(duplicateId);
        }
        if (setComponent){
            Collections.sort(ids);
            Submit sbm = Submit.getById(em, ids.get(ids.size() -1 ));
            report.setComponent(sbm.getReportId().getComponent());
            report.setSubcomponent(sbm.getReportId().getSubcomponent());
        }
        for (Integer id : ids) {
            Submit sbm = Submit.getById(em, id);
            sbm.setReportId(report);
            sbm.getReportId().setDuplicateManChanged(true);
            redirected.add(em.merge(sbm));
        }
        return redirected;
    }
}
