/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2010 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2010 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */
package org.netbeans.web.action.beans;

import javax.persistence.EntityManager;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.netbeans.modules.exceptions.entity.SourcejarMapping;
import org.netbeans.web.Persistable;
import org.netbeans.web.Utils;

/**
 *
 * @author Jindrich Sedek
 */
public class SourcejarMappingBean extends org.apache.struts.action.ActionForm {

    private String subcomponent;
    private String component;
    private String sourcejar;

    /**
     * This is the action called from the Struts framework.
     * @param mapping The ActionMapping used to select this instance.
     * @param request The HTTP Request we are processing.
     * @return
     */
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        if (isClean()){
            return null;
        }
        final ActionErrors errors = new ActionErrors();
        doValidation(errors, getComponent(), "component");
        doValidation(errors, getSubcomponent(), "subcomponent");
        doValidation(errors, getSourcejar(), "sourcejar");
        Utils.processPersistable(new Persistable.Query() {

            public TransactionResult runQuery(EntityManager em) throws Exception {
                if (em.find(SourcejarMapping.class, sourcejar) != null){
                    errors.add("sourcejar", new ActionMessage("sourcejarmapping.sourcejar.exists"));
                }
                return TransactionResult.NONE;
            }
        });
        return errors;
    }

    @Override
    public void reset(ActionMapping mapping, HttpServletRequest request) {
        reset();
    }

    public void reset() {
        subcomponent = null;
        component = null;
        sourcejar = null;
    }

    public boolean isClean(){
        if ((component == null) && (subcomponent == null) && (sourcejar == null)){
            return true;
        }
        return false;
    }

    private void doValidation(ActionErrors errors, String value, String property){
        if (value == null || value.length() < 1) {
            errors.add(property, new ActionMessage("sourcejarmapping." + property + ".ismissing"));
        }
    }

    public String getComponent() {
        return component;
    }

    public void setComponent(String componentName) {
        this.component = componentName;
    }

    public String getSourcejar() {
        return sourcejar;
    }

    public void setSourcejar(String jarName) {
        this.sourcejar = jarName;
    }

    public String getSubcomponent() {
        return subcomponent;
    }

    public void setSubcomponent(String subcomponentName) {
        this.subcomponent = subcomponentName;
    }

    public boolean needsTransaction() {
        return false;
    }

}
