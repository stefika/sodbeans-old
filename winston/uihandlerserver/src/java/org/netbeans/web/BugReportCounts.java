/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2009 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */
package org.netbeans.web;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import javax.persistence.EntityManager;
import org.netbeans.modules.exceptions.entity.Nbversion;
import org.netbeans.modules.exceptions.utils.PersistenceUtils;
import org.netbeans.web.Persistable.TransactionResult;

/**
 *
 * @author Jindrich Sedek
 */
public class BugReportCounts extends Persistable.Query {
    private static final int DAYS_IN_WEEK = 7;

    private List<Data> bugReportCounts = null;
    private final Long min;
    private final Long max;

    public BugReportCounts() {
        this(70208l, 150000l);
    }

    public BugReportCounts(Long min, Long max) {
        this.min = min;
        this.max = max;
    }

    public static List<Data> getStats() {
        BugReportCounts brc = new BugReportCounts();
        Utils.processPersistable(brc);
        return brc.getResult();
    }
    
    public static List<Data> getStats(Long min, Long max) {
        BugReportCounts brc = new BugReportCounts(min, max);
        Utils.processPersistable(brc);
        return brc.getResult();
    }

    public TransactionResult runQuery(EntityManager em) throws Exception {
        Nbversion version = (Nbversion) PersistenceUtils.getExist(em, "Nbversion.findByVersion", Nbversion.DEV_VERSION);
        List<Object[]> rs = PersistenceUtils.executeNamedQuery(em, "Exceptions.groupByBuilds", Collections.singletonMap("version", version));
        if (rs.isEmpty()) {
            bugReportCounts = Collections.<Data>emptyList();
        } else {
            ArrayList<Data> reportCounts = new ArrayList<Data>();
            for (Object[] item : rs) {
                assert (item.length == 2);
                Long build = (Long) item[0];
                Long count = (Long) item[1];
                if (build < min) {
                    continue;
                }
                if (build > max) {
                    continue;
                }
                reportCounts.add(new Data(build, count));
            }
            Collections.sort(reportCounts);
            bugReportCounts = groupBugReports(reportCounts);
        }
        return TransactionResult.NONE;
    }

    private List<Data> getResult() {
        return bugReportCounts;
    }

    private List<Data> groupBugReports(ArrayList<Data> reportCounts) {
        assert(reportCounts.size() > 0);
        ArrayList<Data> result = new ArrayList<Data>(reportCounts.size() / DAYS_IN_WEEK);
        Iterator<Data> it = reportCounts.iterator();
        Data first = it.next();
        long count = first.count;
        long order = 0;
        while (it.hasNext()){
            Data next = it.next();
            if (next.getBuild() - first.getBuild() > 10){
                result.add(new Data(++order, count));
                first = next;
                count = 0;
            }
            count  += next.getCount();
        }
        if (first != null && count > 0){
            result.add(new Data(++order, count));
        }
        return result;
    }

    public static class Data implements Comparable<Data> {
        private static final long MAX_COUNT = 2000;
        private final Long build;
        private final Long count;

        public Data(Long build, Long count) {
            this.build = build;
            if (count > MAX_COUNT){
                this.count = MAX_COUNT;
            }else{
                this.count = count;
            }
        }

        /**
         * @return the build
         */
        public Long getBuild() {
            return build;
        }

        /**
         * @return the count
         */
        public Long getCount() {
            return count;
        }

        public int compareTo(Data o) {
            return build.compareTo(o.build);
        }

        public String getSerie() {
            return "Data";
        }

        @Override
        public boolean equals(Object obj) {
            if (obj == null) {
                return false;
            }
            if (!(obj instanceof Data)) {
                return false;
            }
            Data o = (Data) obj;
            return this.build.equals(o.build) && this.count.equals(o.count);
        }

        @Override
        public int hashCode() {
            int hash = 7;
            hash = 17 * hash + (this.build != null ? this.build.hashCode() : 0);
            hash = 17 * hash + (this.count != null ? this.count.hashCode() : 0);
            return hash;
        }
    }
}
