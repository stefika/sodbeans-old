/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */
package org.netbeans.web;

import javax.persistence.EntityManager;

/**
 *
 * Persistable interface is used to run transactions and queries in web page 
 * requests. See @link Utils#processPersistable(Persistable persistable) for details
 * how to execute persistable.
 * 
 * The biggest adventage of using ersistable is that you can be sure that 
 * transaction and entity manager will be always closed after execution.
 * 
 * @author Jindrich Sedek
 */
public interface Persistable {

    /**
     * This method should implement queries or transaction that needs database access.
     * The implementation should return {@link TransactionResult#COMMIT 
     * TransactionResult.COMMIT} for the transaction that should be committed.
     * {@link TransactionResult#ROLLBACK 
     * TransactionResult.ROLLBACK} for the transaction that should be rolledback.
     * {@link TransactionResult#NONE
     * TransactionResult.NONE} for queries that should have no transaction.
     * 
     * @param em EntityManager instance passed to the procedure
     * @return {@link TransactionResult TransactionResult}as a result of the processing.
     */
    public TransactionResult runQuery(EntityManager em) throws Exception;

    /**
     * Returns true if the processing needs a transaction, false if it is just
     * a query
     * @return true for transaction, false for simple query
     */
    public boolean needsTransaction();

    /**
     * Simple Persistable interface adapter reprezenting a transaction
     */
    public static abstract class Transaction implements Persistable {

        public boolean needsTransaction() {
            return true;
        }
    }

    /**
     * Simple Persistable interface adapter reprezenting a query without 
     * transaction
     */
    public static abstract class Query implements Persistable {

        public boolean needsTransaction() {
            return false;
        }
    }
    /**
     * Transaction result of the query processing
     */
    public enum TransactionResult {
        /** Transaction should be commited */
        COMMIT,
        /** Transaction should be rolled back */
        ROLLBACK, 
        /** Return value for queries */
        NONE
    }
}
