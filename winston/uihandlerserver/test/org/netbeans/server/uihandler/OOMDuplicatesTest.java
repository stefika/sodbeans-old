/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */

package org.netbeans.server.uihandler;

import java.io.File;
import javax.persistence.EntityManager;
import org.netbeans.modules.exceptions.entity.Submit;
import org.netbeans.modules.exceptions.utils.BugReporterFactory;
import org.netbeans.server.uihandler.api.Issue;

/**
 *
 * @author Jindrich Sedek
 */
public class OOMDuplicatesTest extends DatabaseTestCase{
    private ExceptionsData excData;
    private static Integer submitId = 1;

    public OOMDuplicatesTest(String name) {
        super(name);
    }

    public void testOOMDuplicates() throws Exception{
        addNext(1, false, 0);
        addNext("dupres/nonOOM.xml", false, 0);
        addNext(2, true, 1);
        addNext(3, true, 1);
        addNext(4, true, 1);
    }

    public void testOOMDuplicatesForCloseIssue() throws Exception{
        addNext(1, false, 0);
        Issue iss = BugReporterFactory.getDefaultReporter().getIssue(1000);
        iss.setResolution("FIXED");
        iss.setIssueStatus("RESOLVED");

        EntityManager em = perUtils.createEntityManager();
        em.getTransaction().begin();
        Submit sb = em.find(Submit.class, excData.getSubmitId());
        sb.getReportId().setIssueId(1000);
        em.merge(sb);
        em.getTransaction().commit();
        em.close();

        addNext(4, false, 0);
        addNext(2, true, 7);
        addNext(3, true, 7);
    }

    private void addNext(int index, boolean duplicate, int id) throws Exception{
        addNext("dupres/oom"+Integer.toString(index) + ".xml", duplicate, id);
    }
    
    private void addNext(String fileName, boolean duplicate, int id) throws Exception{
        File log = LogsManagerTest.extractResourceAs(data, fileName, "NB2040811605."+submitId);
        excData = addLog(log, "NB2040811605");
        assertNotNull("MAPA SHOULD BE NULL FOR NO SESSIONS", excData);
        assertEquals("THIS ISSUE SHOULD (NOT) BE A DUPLICATE",  duplicate, excData.isDuplicateReport());
        assertEquals("ISSUE SHOULD INCREASE", submitId, excData.getSubmitId());
        if (duplicate){
            assertTrue("THIS EXCEPTIONS SHOULD BE DUPLICATES " + id + " AND " + excData.getSubmitId(),  areDuplicates(id, excData.getSubmitId()));
        }else{
            assertFalse(excData.isDuplicateReport());
        }
        submitId++;
    }
    
}
