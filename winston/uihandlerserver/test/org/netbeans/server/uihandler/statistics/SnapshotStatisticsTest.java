/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2009 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * Copyright 2009 Sun Microsystems, Inc. All rights reserved.
 */

package org.netbeans.server.uihandler.statistics;

import java.io.File;
import java.util.Map;
import javax.servlet.jsp.PageContext;
import org.netbeans.server.uihandler.DatabaseTestCase;
import org.netbeans.server.uihandler.LogsManagerTest;

/**
 *
 * @author Jindrich Sedek
 */
public class SnapshotStatisticsTest extends DatabaseTestCase {

    public SnapshotStatisticsTest(String testName) {
        super(testName);
    }
    
    public void testSnapshotStatistics() throws Exception {
        File log = LogsManagerTest.extractResourceAs(ScreenSizeTest.class, data, "logs/snapshotStats.xml", "log444");
        waitLog(log, "127.0.0.1");
        PageContext png = LogsManagerTest.createPageContext();
        logsManager.preparePageContext(png, "log444", null);

        verify(png.getAttribute("globalSnapshotStatisticsSAMPLES"), 317);
        verify(png.getAttribute("globalSnapshotStatisticsAVG"), 99);
        verify(png.getAttribute("globalSnapshotStatisticsMINIMUM"), 8);
        verify(png.getAttribute("globalSnapshotStatisticsMAXIMUM"), 2885);
        verify(png.getAttribute("globalSnapshotStatisticsSTDDEV"), 174);
    }

    private void verify(Object data, int key) {
        assertNotNull("global statistics created", data);
        assertTrue(data instanceof Map);
        Map<Integer, Integer> m = (Map<Integer, Integer>) data;
        assertTrue(m.containsKey(key));
        assertEquals(1, m.size());
        assertEquals(new Integer(1), m.get(key));
    }
}
