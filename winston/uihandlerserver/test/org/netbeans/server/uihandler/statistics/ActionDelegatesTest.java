/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * Portions Copyrighted 2007 Sun Microsystems, Inc.
 */

package org.netbeans.server.uihandler.statistics;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Level;
import javax.servlet.jsp.PageContext;
import org.netbeans.junit.NbTestCase;
import org.netbeans.modules.exceptions.utils.PersistenceUtils;
import org.netbeans.server.uihandler.DatabaseTestCase;
import org.netbeans.server.uihandler.LogsManager;
import org.netbeans.server.uihandler.LogsManagerTest;
import org.netbeans.server.uihandler.statistics.Actions.Calls;
import org.openide.util.NbCollections;

/**
 *
 * @author Jaroslav Tulach
 */
public class ActionDelegatesTest extends NbTestCase {
    
    public ActionDelegatesTest(String testName) {
        super(testName);
    }

    private LogsManager result;

    
    @Override
    protected Level logLevel() {
        return Level.FINE;
    }
    
    private File logs() throws IOException {
        File f = new File(getWorkDir(), "logs");
        f.mkdirs();
        return f;
    }
    
    
    protected void setUp() throws Exception {
        clearWorkDir();
        DatabaseTestCase.initPersitenceUtils(getWorkDir());

        result = LogsManagerTest.createManager(logs());
        result.preparePageContext(LogsManagerTest.createPageContext(), "", null);
        
        File log = LogsManagerTest.extractResourceAs(HelpTest.class, logs(), "actionDelegates.log", "log444");
//        File log2 = LogsManagerTest.extractResourceAs(HelpTest.class, logs(), "nohelp.log", "log333.1");
//        File log3 = LogsManagerTest.extractResourceAs(HelpTest.class, logs(), "newnohelp.log", "log333");
        
        result.addLog(log, "127.0.0.1");
//        result.addLog(log2, "127.0.0.1");
//        result.addLog(log3, "127.0.0.1");
    }

    public void testGetActionsInvoked() throws Exception {
        PageContext pg = LogsManagerTest.createPageContext();
        
        result.preparePageContext(pg, "noUser", null);
        
        Object attr = pg.getAttribute("globalActions");
        assertNotNull("Value provided", attr);
        assertEquals(true, attr instanceof Calls);
        
        Calls calls = (Calls)attr;
        Map<String,Integer> all = calls.getTopTen(10, null, null, Actions.Invocations.ALL);

        String dumpAll = all.toString();
        if (dumpAll.indexOf("Delegate") >= 0) {
            fail("Some Delegate:\n" + dumpAll);
        }
        if (dumpAll.indexOf("ProjectAction") >= 0) {
            fail("Some ProjectAction:\n" + dumpAll);
        }
        if (dumpAll.indexOf("DebuggerAction") >= 0) {
            fail("Some DebuggerAction:\n" + dumpAll);
        }
        
        if (dumpAll.indexOf("org.netbeans.modules.j2ee.deployment.impl.ui.actions.StopAction") == -1) {
            fail("StopAction is there:\n" + dumpAll);
        }
        
        assertEquals("some elements:\n" + all, 7, all.size());
        assertEquals("one Stop", 1, all.get("org.netbeans.modules.j2ee.deployment.impl.ui.actions.StopAction").intValue());
        assertEquals("one Prev view", 1, all.get("org.netbeans.core.actions.PreviousViewCallbackAction").intValue());
        assertEquals("one test project", 1, all.get("org.netbeans.modules.project.actions.TestProject").intValue());
        assertEquals("one build main project", 1, all.get("org.netbeans.modules.project.actions.BuildMainProject").intValue());
        assertEquals("one build debug", 1, all.get("org.netbeans.modules.debugger.actions.FinishDebuggerSession").intValue());
        assertEquals("one test project", 1, all.get("org.netbeans.modules.project.actions.RunFile").intValue());
        assertEquals("one Stop", 1, all.get("org.openide.actions.SaveAction").intValue());
        
    }
    
    public void testSizeOfData() throws Exception {
        PageContext pg = LogsManagerTest.createPageContext();
        
        result.preparePageContext(pg, "noUser", null);
        
        Object attr = pg.getAttribute("globalActions");
        assertNotNull("Value provided", attr);
        assertEquals(true, attr instanceof Calls);

        Object[] skip = {
            "org.netbeans.modules.openfile".intern(),
            "org.netbeans.modules.project.ui.actions".intern(),
            "org.netbeans.modules.java.actions".intern(),
            "OpenFileAction".intern(),
            "OpenProject".intern(),
            "GoToTypeAction".intern()
        };
        assertSize("Should not be that big 16b with one field + 40b for array", Collections.singleton(attr), 56, skip);
    }
}
