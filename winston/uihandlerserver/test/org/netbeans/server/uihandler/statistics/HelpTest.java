/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * Copyright 2007 Sun Microsystems, Inc. All rights reserved.
 */

package org.netbeans.server.uihandler.statistics;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import javax.servlet.jsp.PageContext;
import org.netbeans.junit.Log;
import org.netbeans.junit.NbTestCase;
import org.netbeans.server.uihandler.DatabaseTestCase;
import org.netbeans.server.uihandler.LogsManager;
import org.netbeans.server.uihandler.LogsManagerTest;
import org.netbeans.server.uihandler.statistics.Help.Invocations;

/**
 *
 * @author Jaroslav Tulach
 */
public class HelpTest extends NbTestCase {
    private LogsManager result;
    private CharSequence seq;

    
    public HelpTest(String testName) {
        super(testName);
    }
    @Override
    protected Level logLevel() {
        return Level.FINE;
    }
    
    private File logs() throws IOException {
        File f = new File(getWorkDir(), "logs");
        f.mkdirs();
        return f;
    }
    
    
    @Override
    protected void setUp() throws Exception {
        clearWorkDir();
        DatabaseTestCase.initPersitenceUtils(getWorkDir());
        DatabaseTestCase.setIZInsertion();
        
        result = LogsManagerTest.createManager(logs());
        result.waitVerificationFinished();
        result.preparePageContext(LogsManagerTest.createPageContext(), "", null);
        
        File log = LogsManagerTest.extractResourceAs(HelpTest.class, logs(), "help.log", "log444");
        File log2 = LogsManagerTest.extractResourceAs(HelpTest.class, logs(), "newnohelp.log", "log333");
        File log3 = LogsManagerTest.extractResourceAs(HelpTest.class, logs(), "nohelp.log", "log444.1");
        
        seq = Log.enable("org.netbeans.server.uihandler.LogsManager", Level.WARNING);
        result.addLog(log, "127.0.0.1");
        result.addLog(log2, "127.0.0.1");
        result.addLog(log3, "127.0.0.1");
    }

    public void testHelpInocations() throws Exception {
        PageContext png = LogsManagerTest.createPageContext();
        result.preparePageContext(png, "log444", null);

        if (seq.toString().toLowerCase().indexOf("rollback") > 0) {
            fail("There was a rollback: " + seq);
        }
        
        Object o = png.getAttribute("globalHelp");
        assertNotNull("global statistics created", o);
        assertEquals("Right class", Invocations.class, o.getClass());
        
        Invocations inv = (Invocations)o;
        Set<Invocations.One> set = inv.getTopTen();
        assertEquals("Two " + set, 2, set.size());
        
        Invocations.One[] arr = new Invocations.One[2];
        set.toArray(arr);
        
        assertEquals("two invocations ", 2, arr[0].getCnt());
        assertEquals("two invocations of TC", "org.openide.windows.TopComponent", arr[0].getId());

        assertEquals("one invocation", 1, arr[1].getCnt());
        if (arr[1].getId().indexOf("j2seproject.ui") < 0) {
            fail("UI should be there: " + arr[1]);
        }
        
        
        Object ratio = png.getAttribute("globalHelpRatio");
        assertTrue("Collection exists", ratio instanceof Map);
        
        Map ratioHasTwo = (Map)ratio;
        assertEquals("Two items", 2, ratioHasTwo.size());
        
        Iterator it = ratioHasTwo.values().iterator();
        assertEquals("integer", Integer.class, it.next().getClass());
        assertEquals("integer2", Integer.class, it.next().getClass());
        it = ratioHasTwo.values().iterator();
        int i1 = (Integer)it.next();
        int i2 = (Integer)it.next();
        
        assertEquals("Amount of sessions that used help", 1, i1);
        assertEquals("Those that ignored help", 1, i2);
    }

}
