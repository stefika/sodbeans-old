/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */
package org.netbeans.server.uihandler.statistics;

import java.io.File;
import java.util.Map;
import javax.servlet.jsp.PageContext;
import org.netbeans.server.uihandler.DatabaseTestCase;
import org.netbeans.server.uihandler.LogsManagerTest;

/**
 *
 * @author Tomas Musil
 */
public class ScanningTest extends DatabaseTestCase {

    public ScanningTest(String name) {
        super(name);
    }

    public void testScanningLog() throws Exception{
        Map m = uploadLog("scanning.log");
        assertNotNull(m);
        assertEquals("", 1, m.get(ScanningExtChanges.USING));
    }
    
    public void testScanningTurnedOffLog() throws Exception{
        Map m = uploadLog("scanning_off.log");
        assertNotNull(m);
        assertEquals("", 1, m.get(ScanningExtChanges.TURNED_OFF));
        //try the same again
        Map m1 = uploadLog("scanning_off.log");
        assertNotNull(m1);
        m.putAll(m1);
        assertEquals("", 2, m.get(ScanningExtChanges.TURNED_OFF));
    }

    private Map uploadLog(String logname) throws Exception{
        File log = LogsManagerTest.extractResourceAs(ScanningTest.class, data, logname, "log444");
        waitLog(log, "127.0.0.1");
        PageContext png = LogsManagerTest.createPageContext();
        logsManager.preparePageContext(png, "log444", null);
        return (Map) png.getAttribute("globalScanningExtChangesRatio");
        
    }

}
