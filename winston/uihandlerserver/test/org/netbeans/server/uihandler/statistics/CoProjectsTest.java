/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * Copyright 2007 Sun Microsystems, Inc. All rights reserved.
 */

package org.netbeans.server.uihandler.statistics;

import java.util.HashMap;
import java.util.Map;
import java.util.prefs.Preferences;
import org.netbeans.junit.NbTestCase;
import org.netbeans.server.uihandler.DatabaseTestCase;
import org.netbeans.server.uihandler.statistics.CoProjects.Sheet;
/**
 *
 * @author Jindrich Sedek
 */
public class CoProjectsTest extends NbTestCase {

    
    public CoProjectsTest(String testName) {
        super(testName);
    }

    @Override
    protected int timeOut() {
        return 10000;
    }

    public void testRecursiveIndexOfInWrite() throws Exception{
        CoProjects projects = new CoProjects();
        Map<String,Integer> techs = new HashMap<String, Integer>();
        String str = " test";
        String longStr = str;
        for (int i = 0; i < 100; i++){
            longStr = longStr.concat(str);
        }
        techs.put(longStr, 1);
        Sheet sheet = new Sheet(techs);
        Preferences prefs = new DatabaseTestCase.TestPreferences();
        projects.write(prefs, sheet);

    }
        
}
