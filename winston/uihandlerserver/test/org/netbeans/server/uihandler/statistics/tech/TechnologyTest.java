/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * Copyright 2007 Sun Microsystems, Inc. All rights reserved.
 */

package org.netbeans.server.uihandler.statistics.tech;

import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.logging.Level;
import javax.servlet.jsp.PageContext;
import org.netbeans.junit.Log;
import org.netbeans.junit.NbTestCase;
import org.netbeans.server.uihandler.DatabaseTestCase;
import org.netbeans.server.uihandler.LogsManager;
import org.netbeans.server.uihandler.LogsManagerTest;

/**
 *
 * @author Jaroslav Tulach
 */
public class TechnologyTest extends NbTestCase {
    private static LogsManager result;
    private CharSequence seq;

    
    public TechnologyTest(String testName) {
        super(testName);
    }
    @Override
    protected Level logLevel() {
        return Level.WARNING;
    }
    
    private File logs() throws IOException {
        File f = new File(getWorkDir(), "logs");
        f.mkdirs();
        return f;
    }
    
    
    @Override
    protected void setUp() throws Exception {
        clearWorkDir();
        if (result == null) {
            DatabaseTestCase.initPersitenceUtils(getWorkDir());

            result = LogsManagerTest.createManager(logs());
            result.waitVerificationFinished();

            result.preparePageContext(LogsManagerTest.createPageContext(), "", null);

            File log = LogsManagerTest.extractResourceAs(TechnologyTest.class, logs(), "two-debugs-on-j2se-one-on-web.log", "log444");
            File log2 = LogsManagerTest.extractResourceAs(TechnologyTest.class, logs(), "closing-two-project-types-and-two-debugs-on-j2se.log", "log333");

            result.addLog(log, "127.0.0.1");
            result.addLog(log2, "127.0.0.1");
//          result.addLog(log3, "127.0.0.1");
        }
        
        seq = Log.enable("org.netbeans.server.uihandler.LogsManager", Level.WARNING);
    }
    
    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    public void testUser444UsesBothDebuggers() throws Exception {
        PageContext png = LogsManagerTest.createPageContext();
        result.preparePageContext(png, "log444", null);

        Object o = png.getAttribute("userTechnology");
        assertNotNull("global statistics created", o);
        assertEquals("Right class: " + o.getClass(), true, Map.class.isAssignableFrom(o.getClass()));

        Map inv = (Map)o;
        assertEquals("Two technologies used: " + inv, 2, inv.size());
        assertEquals("Technology used once: " + inv, 50, inv.get("J2SE Debugger"));
        assertEquals("Technology used once: " + inv, 50, inv.get("Web Debugger"));
    }

    public void testTechnology333() throws Exception {
        PageContext png = LogsManagerTest.createPageContext();
        result.preparePageContext(png, "log333", null);

        Object o = png.getAttribute("userTechnology");
        assertNotNull("global statistics created", o);
        assertEquals("Right class: " + o.getClass(), true, Map.class.isAssignableFrom(o.getClass()));

        Map inv = (Map)o;
        assertEquals("One technology used: " + inv, 1, inv.size());
        assertEquals("Technology used once: " + inv, 100, inv.get("J2SE Debugger"));
    }

    public void testTechnologyGlobal() throws Exception {
        PageContext png = LogsManagerTest.createPageContext();
        result.preparePageContext(png, "log333", null);

        Object o = png.getAttribute("globalTechnology");
        assertNotNull("global statistics created", o);
        assertEquals("Right class: " + o.getClass(), true, Map.class.isAssignableFrom(o.getClass()));

        Map inv = (Map)o;
        assertEquals("Two technology used: " + inv, 2, inv.size());
        assertEquals("j2se used twice: " + inv, 66, inv.get("J2SE Debugger"));
        assertEquals("web used once: " + inv, 33, inv.get("Web Debugger"));
    }
        
}
