/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * Copyright 2007 Sun Microsystems, Inc. All rights reserved.
 */

package org.netbeans.server.uihandler.statistics;

import java.io.File;
import java.io.IOException;
import java.util.TreeMap;
import java.util.logging.Level;
import javax.servlet.jsp.PageContext;
import org.netbeans.junit.NbTestCase;
import org.netbeans.server.uihandler.DatabaseTestCase;
import org.netbeans.server.uihandler.LogsManager;
import org.netbeans.server.uihandler.LogsManagerTest;

/**
 *
 * @author Jaroslav Tulach
 */
public class CodeCompletionTest extends NbTestCase {
    private LogsManager result;

    
    public CodeCompletionTest(String testName) {
        super(testName);
    }
    @Override
    protected Level logLevel() {
        return Level.FINE;
    }
    
    private File logs() throws IOException {
        File f = new File(getWorkDir(), "logs");
        f.mkdirs();
        return f;
    }
    
    
    protected void setUp() throws Exception {
        clearWorkDir();
        DatabaseTestCase.initPersitenceUtils(getWorkDir());
        
        File log = LogsManagerTest.extractResourceAs(CodeCompletionTest.class, logs(), "help.log", "log444");
        File log2 = LogsManagerTest.extractResourceAs(CodeCompletionTest.class, logs(), "nohelp.log", "log333");
        
        result = LogsManagerTest.createManager(logs());
        result.waitVerificationFinished();
    }
    
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    public void testHelpInocations() throws Exception {
        PageContext png = LogsManagerTest.createPageContext();
        result.preparePageContext(png, "log444", null);
        
        Object o = png.getAttribute("keyboardMouseUsage");
        assertNotNull("Keyboard/mouse statistic usage", o);
        assertEquals("Right class", java.util.TreeMap.class, o.getClass());
        
        Object o1 = png.getAttribute("complCancel");
        assertNotNull("Completion/cancellation statistic usage", o1);
        assertEquals("Right class", java.util.TreeMap.class, o1.getClass());
        
        Object o2 = png.getAttribute("implicitExplicit");
        assertNotNull("Implicit/explicit statistic usage", o2);
        assertEquals("Right class", java.util.TreeMap.class, o2.getClass());
        
        Object o3 = png.getAttribute("selectedIndex");
        assertNotNull("Slected index statistic", o3);
        assertEquals("Right class", java.util.TreeMap.class, o3.getClass());
        
        TreeMap m = (TreeMap)o3;
        assertEquals("Two data", 2, m.size());
        Integer i = (Integer)m.get("Default completions");
        assertNotNull("Default filled", i);
        assertEquals("One item", Integer.valueOf(1), i);
    }

}
