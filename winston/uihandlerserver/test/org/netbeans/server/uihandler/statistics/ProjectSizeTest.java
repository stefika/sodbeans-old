/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */
package org.netbeans.server.uihandler.statistics;

import java.io.File;
import java.util.Collections;
import java.util.List;
import javax.servlet.jsp.PageContext;
import org.netbeans.server.uihandler.DatabaseTestCase;
import org.netbeans.server.uihandler.LogsManagerTest;
import static org.netbeans.server.uihandler.statistics.ProjectSize.*;

/**
 *
 * @author Jindrich Sedek
 */
public class ProjectSizeTest extends DatabaseTestCase {

    public ProjectSizeTest(String name) {
        super(name);
    }

    public void testLogProcessing() throws Exception {
        File log = LogsManagerTest.extractResourceAs(ProjectSize.class, data, "logs/projectsize.log", "log444");
        waitLog(log, "127.0.0.1");
        PageContext png = LogsManagerTest.createPageContext();
        png.setAttribute("columns", Integer.toString(ProjectSize.MAXIMUM_DEF));
        logsManager.preparePageContext(png, "log444", Collections.singleton("ProjectSize"));

        List<ProjectSizeItem> javaFiles = (List<ProjectSizeItem>) png.getAttribute("globalProjectSize" + JAVA_FILES);
        assertNotNull(javaFiles);
        assertTrue(javaFiles.contains(new ProjectSizeItem(112, 1)));
        assertNotAny(javaFiles, 111);
        assertNotAny(javaFiles, 70);
        assertNotAny(javaFiles, 0);
        assertTrue(javaFiles.contains(new ProjectSizeItem(57, 1)));
        assertTrue(javaFiles.contains(new ProjectSizeItem(1, 1)));

        List<ProjectSizeItem> virtualFiles = (List<ProjectSizeItem>) png.getAttribute("globalProjectSize" + VIRTUAL_FILES);
        assertNull(virtualFiles);

        List<ProjectSizeItem> userLogJavaFiles = (List<ProjectSizeItem>) png.getAttribute("globalProjectSize" + USER_LOG_JAVA_FILES);
        assertNotNull(userLogJavaFiles);
        assertTrue(userLogJavaFiles.contains(new ProjectSizeItem(1 + 57 + 112, 1)));

        List<ProjectSizeItem> userLogVirtualFiles = (List<ProjectSizeItem>) png.getAttribute("globalProjectSize" + USER_LOG_VIRTUAL_FILES);
        assertNull(userLogVirtualFiles);
    }

    public void testMoreLogsMerge() throws Exception {
        File log = LogsManagerTest.extractResourceAs(ProjectSize.class, data, "logs/projectsize.log", "log444");
        waitLog(log, "127.0.0.1");
        log = LogsManagerTest.extractResourceAs(ProjectSize.class, data, "logs/projectsize2.log", "log444.1");
        waitLog(log, "127.0.0.1");
        PageContext png = LogsManagerTest.createPageContext();
        png.setAttribute("columns", Integer.toString(ProjectSize.MAXIMUM_DEF));
        logsManager.preparePageContext(png, "log444", Collections.singleton("ProjectSize"));

        List<ProjectSizeItem> javaFiles = (List<ProjectSizeItem>) png.getAttribute("globalProjectSize" + JAVA_FILES);
        assertNotNull(javaFiles);
        assertTrue(javaFiles.contains(new ProjectSizeItem(112, 2)));
        assertNotAny(javaFiles, 111);
        assertNotAny(javaFiles, 70);
        assertNotAny(javaFiles, 0);
        assertTrue(javaFiles.contains(new ProjectSizeItem(57, 1)));
        assertTrue(javaFiles.contains(new ProjectSizeItem(58, 1)));
        assertTrue(javaFiles.contains(new ProjectSizeItem(17, 1)));

        List<ProjectSizeItem> virtualFiles = (List<ProjectSizeItem>) png.getAttribute("globalProjectSize" + VIRTUAL_FILES);
        assertNotNull(virtualFiles);
        assertFalse(virtualFiles.isEmpty());
        assertTrue(virtualFiles.contains(new ProjectSizeItem(10, 1)));
        
        List<ProjectSizeItem> userLogJavaFiles = (List<ProjectSizeItem>) png.getAttribute("globalProjectSize" + USER_LOG_JAVA_FILES);
        assertNotNull(userLogJavaFiles);
        assertTrue(userLogJavaFiles.contains(new ProjectSizeItem(1 + 57 + 112, 1)));
        assertFalse("overMaximum", userLogJavaFiles.contains(new ProjectSizeItem(536, 1)));

        List<ProjectSizeItem> userLogVirtualFiles = (List<ProjectSizeItem>) png.getAttribute("globalProjectSize" + USER_LOG_VIRTUAL_FILES);
        assertNotNull(userLogVirtualFiles);
        assertFalse(userLogVirtualFiles.isEmpty());
        assertTrue(userLogVirtualFiles.contains(new ProjectSizeItem(10, 1)));

        log = LogsManagerTest.extractResourceAs(ProjectSize.class, data, "logs/projectsize2.log", "log444.2");
        waitLog(log, "127.0.0.1");
        png = LogsManagerTest.createPageContext();
        png.setAttribute("columns", Integer.toString(ProjectSize.MAXIMUM_DEF));
        logsManager.preparePageContext(png, "log444", Collections.singleton("ProjectSize"));
        javaFiles = (List<ProjectSizeItem>) png.getAttribute("globalProjectSize" + JAVA_FILES);
        assertNotNull(javaFiles);
        assertTrue(javaFiles.contains(new ProjectSizeItem(112, 3)));
        assertNotAny(javaFiles, 111);
        assertNotAny(javaFiles, 70);
        assertNotAny(javaFiles, 0);
        assertTrue(javaFiles.contains(new ProjectSizeItem(57, 1)));
        assertTrue(javaFiles.contains(new ProjectSizeItem(58, 2)));
        assertTrue(javaFiles.contains(new ProjectSizeItem(17, 2)));
        
        userLogJavaFiles = (List<ProjectSizeItem>) png.getAttribute("globalProjectSize" + USER_LOG_JAVA_FILES);
        assertNotNull(userLogJavaFiles);
        assertTrue(userLogJavaFiles.contains(new ProjectSizeItem(1 + 57 + 112, 1)));
        assertFalse("over maximum", userLogJavaFiles.contains(new ProjectSizeItem(536, 2)));

        //test order
        int latest = 0;
        for (ProjectSizeItem projectSizeItem : javaFiles) {
            assert(projectSizeItem.getFilescount() > latest);
            latest = projectSizeItem.getFilescount();
        }
    }
    
    private void assertNotAny(List<ProjectSizeItem> items, Integer key){
        for (ProjectSizeItem projectSizeItem : items) {
            assert (!key.equals(projectSizeItem.getFilescount()));
        }
    }

    public void testFinishUploadEmpty(){
        ProjectSize stat = new ProjectSize();
        ProjectSizeBean empty = stat.newData();
        ProjectSizeBean result = stat.finishSessionUpload("hallo", 0, false, empty);
        assertEquals(result, empty);
    }

}
