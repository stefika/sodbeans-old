/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2008 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * Copyright 2008 Sun Microsystems, Inc. All rights reserved.
 */
package org.netbeans.server.uihandler.statistics;

import java.io.File;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;
import javax.servlet.jsp.PageContext;
import org.netbeans.junit.Log;
import org.netbeans.server.uihandler.DatabaseTestCase;
import org.netbeans.server.uihandler.LogsManagerTest;
import org.netbeans.server.uihandler.statistics.VWComponents.VWComponent;

/**
 *
 * @author Jan Horvath
 */
public class VWComponentsTest extends DatabaseTestCase {

    private CharSequence seq;

    public VWComponentsTest() {
        super("VWComponentsTest");
    }

    @Override
    protected int timeOut() {
        return 0;
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        assertEquals("NO logs", 0, logsManager.getNumberOfLogs());

        logsManager.preparePageContext(LogsManagerTest.createPageContext(), "", null);

        File log = LogsManagerTest.extractResourceAs(VWComponentsTest.class, data, "vwcomponents.log", "log444");

        seq = Log.enable("org.netbeans.server.uihandler.LogsManager", Level.WARNING);
        logsManager.addLog(log, "127.0.0.1");
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    public void testVWComponents() throws Exception {
        PageContext png = LogsManagerTest.createPageContext();
        logsManager.preparePageContext(png, "log444", null);

        Object o = png.getAttribute("globalVWComponents");
        assertNotNull("global statistics created", o);
        if (o instanceof Map) {
            // ok
        } else {
            fail("Right class Map: " + o);
        }
        Map<VWComponent, Integer> m = (Map<VWComponent, Integer>) o;
        assertEquals("6 components: " + m, 6, m.size());

        assertEquals(m.get(new VWComponent("com.sun.webui.jsf.component.Button:Button")), new Integer(2));
        assertEquals(m.get(new VWComponent("com.sun.webui.jsf.component.Label:Label")), new Integer(3));
        assertEquals(m.get(new VWComponent("com.sun.webui.jsf.component.Table:Table")), new Integer(3));
        assertEquals(m.get(new VWComponent("com.sun.webui.jsf.component.TextField:Text Field")), new Integer(1));
        assertEquals(m.get(new VWComponent("javax.faces.component.html.HtmlInputText:Text Field")), new Integer(1));
        assertEquals(m.get(new VWComponent("javax.faces.component.html.HtmlOutputLabel:Component Label")), new Integer(1));
    }

    public void testWrite() throws BackingStoreException {
        VWComponents statistic = new VWComponents();
        Map<VWComponent, Integer> map = new TreeMap<VWComponent, Integer>();
        map.put(new VWComponent("com.sun.data.provider.impl.BasicTransactionalTableDataProvider", "Basic Transactional Table Data Provider"), 10);
        Preferences pref = new DatabaseTestCase.TestPreferences();
        statistic.write(pref, map);
        String[] keys = pref.keys();
        assertEquals("just one item", 1, keys.length);
        assertEquals("saved value", "10", pref.get(keys[0], "0"));
        assertTrue(keys[0].contains("Basic Transactional Table Data Provider"));
    }

    public void testWrite2() throws BackingStoreException {
        VWComponents statistic = new VWComponents();
        Map<VWComponent, Integer> map = new TreeMap<VWComponent, Integer>();
        map.put(new VWComponent("BasicTransactionalTableDataProviderBasicTransactionalTableDataProviderBasicTransactionalTableDataProvider", "Basic Transactional Table Data Provider"), 10);
        Preferences pref = new DatabaseTestCase.TestPreferences();
        statistic.write(pref, map);
        String[] keys = pref.keys();
        assertEquals("just one item", 1, keys.length);
        assertEquals("saved value", "10", pref.get(keys[0], "0"));
        assertTrue(keys[0].contains("Basic Transactional Table Data Provider"));
        System.out.println(keys[0]);
    }
}
