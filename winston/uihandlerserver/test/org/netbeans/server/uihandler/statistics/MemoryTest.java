/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * Copyright 2007 Sun Microsystems, Inc. All rights reserved.
 */

package org.netbeans.server.uihandler.statistics;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.jsp.PageContext;
import org.netbeans.junit.NbTestCase;
import org.netbeans.server.uihandler.DatabaseTestCase;
import org.netbeans.server.uihandler.LogsManager;
import org.netbeans.server.uihandler.LogsManagerTest;
import org.netbeans.server.uihandler.statistics.Memory.MemBean;

/**
 *
 * @author Jaroslav Tulach
 */
public class MemoryTest extends NbTestCase {
    private LogsManager result;
    private Logger LOG;

    
    public MemoryTest(String testName) {
        super(testName);
    }
    @Override
    protected Level logLevel() {
        return Level.WARNING;
    }
    
    private File logs() throws IOException {
        File f = new File(getWorkDir(), "logs");
        f.mkdirs();
        return f;
    }
    
    
    protected void setUp() throws Exception {
        clearWorkDir();
        
        LOG = Logger.getLogger("test." + getName());
        
        DatabaseTestCase.initPersitenceUtils(getWorkDir());

        result = LogsManagerTest.createManager(logs());
        result.preparePageContext(LogsManagerTest.createPageContext(), "", null);
        
        File log = LogsManagerTest.extractResourceAs(HelpTest.class, logs(), "memory/001ebb0c3-ca46-41fd-a3a5-f406ba739e22.0.log", "001ebb0c3-ca46-41fd-a3a5-f406ba739e22.0");
        File log2 = LogsManagerTest.extractResourceAs(HelpTest.class, logs(), "memory/001ebb0c3-ca46-41fd-a3a5-f406ba739e22.log", "001ebb0c3-ca46-41fd-a3a5-f406ba739e22");
        File log3 = LogsManagerTest.extractResourceAs(HelpTest.class, logs(), "memory/047b50ced-fa44-4a09-b936-1d301f6ea1e2.log", "047b50ced-fa44-4a09-b936-1d301f6ea1e2");
        
        result.addLog(log, "127.0.0.1");
        result.addLog(log2, "127.0.0.1");
        result.addLog(log3, "127.0.0.1");
    }
    
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    public void testMemoryCounting() throws Exception {
        PageContext png = LogsManagerTest.createPageContext();
        png.setAttribute("columns", "2", PageContext.REQUEST_SCOPE);
        png.setAttribute("minimum", "1MB", PageContext.REQUEST_SCOPE);
        png.setAttribute("maximum", "2GB", PageContext.REQUEST_SCOPE);
        
        result.preparePageContext(png, "047b50ced-fa44-4a09-b936-1d301f6ea1e2", Collections.<String>singleton("Memory"));
        
        Object o = png.getAttribute("globalMemory");
        assertNotNull("global statistics created", o);
        assertTrue("Right class", o instanceof Collection);
        
        @SuppressWarnings("unchecked")
        Collection<Memory.MemBean> l = (Collection<Memory.MemBean>)o;
        assertEquals("Two buckets", 2, l.size());

        Iterator<MemBean> it = l.iterator();
        
        MemBean mb1 = it.next();
        LOG.info(mb1.toString());
        assertEquals("977 KB is rounded to 1024KB", "1024 KB", mb1.getMemory());
        assertEquals("three sessions ", 3, mb1.getCount());
        MemBean mb2 = it.next();
        LOG.info(mb2.toString());
        assertEquals("2GB", "2 GB", mb2.getMemory());
        assertEquals("one session", 1, mb2.getCount());
    }

}
