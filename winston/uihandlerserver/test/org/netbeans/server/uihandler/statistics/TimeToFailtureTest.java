/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * Copyright 2007 Sun Microsystems, Inc. All rights reserved.
 */
package org.netbeans.server.uihandler.statistics;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import org.netbeans.server.uihandler.DatabaseTestCase;
import static org.junit.Assert.*;

/**
 *
 * @author Jindrich Sedek
 */
public class TimeToFailtureTest extends DatabaseTestCase {

    public TimeToFailtureTest(String str) {
        super(str);
    }

    public void testProcess(){
        final long FIRST_TIME = 150000l;
        final long SECOND_TIME = 200000l;
        LogRecord lr = new LogRecord(Level.CONFIG, TimeToFailture.TTFLoggingMessage + FIRST_TIME);
        TimeToFailture ttf = new TimeToFailture();
        List<TimeToFailture.TTFData> result1 = ttf.process(lr);
        verify(result1, FIRST_TIME, 1);

        assertNull(ttf.process(new LogRecord(Level.INFO, "MSG")));
        assertNull(ttf.process(new LogRecord(Level.CONFIG, "TTF:100")));
        assertNull(ttf.process(new LogRecord(Level.CONFIG, TimeToFailture.TTFLoggingMessage)));
        
        lr.setMessage(TimeToFailture.TTFLoggingMessage + SECOND_TIME);
        List<TimeToFailture.TTFData> result2 = ttf.process(lr);
        verify(result2, SECOND_TIME, 1);
        
        List<TimeToFailture.TTFData> result3 = ttf.join(result1, result2);
        verify(result3, FIRST_TIME, 1);
        verify(result3,SECOND_TIME, 1);
        
        List<TimeToFailture.TTFData> result4 = ttf.join(result1, result1);
        verify(result4, FIRST_TIME, 2);

        List<TimeToFailture.TTFData> result5 = ttf.join(result3, result2);
        verify(result5, FIRST_TIME, 1);
        verify(result5, SECOND_TIME, 2);

        result5 = ttf.join(result2, result3);
        verify(result5, FIRST_TIME, 1);
        verify(result5, SECOND_TIME, 2);

        result5 = ttf.join(result3, result3);
        verify(result5, FIRST_TIME, 2);
        verify(result5, SECOND_TIME, 2);

        result5 = ttf.join(result5, result5);
        verify(result5, FIRST_TIME, 4);
        verify(result5, SECOND_TIME, 4);
    }
    
    public void testGroupData() throws Exception{
        int testItemsCount = 10000;
        List<TimeToFailture.TTFData> testData = new ArrayList<TimeToFailture.TTFData>(testItemsCount);
        for (long i = 0; i < testItemsCount; i++){
            testData.add(new TimeToFailture.TTFData(i * TimeToFailture.TTFData.ROUND_CONST, 1));
        }
        int factor = 2;
        testData = TimeToFailture.groupData(testData, factor);
        for (TimeToFailture.TTFData tTFData : testData) {
            if (tTFData.getTrimmedTime() > 0 ){
                assertEquals((tTFData.getTrimmedTime()+1)/2, tTFData.getCount());
            }
        }

    }
    
    private void verify(List<TimeToFailture.TTFData> res, long key, int value){
        assertNotNull(res);
        assertTrue("KEY", res.contains(new TimeToFailture.TTFData(key, value)));
    }
    
}
