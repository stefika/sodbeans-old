/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * Portions Copyrighted 2007 Sun Microsystems, Inc.
 */

package org.netbeans.server.uihandler.statistics;

import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.logging.Level;
import javax.servlet.jsp.PageContext;
import org.netbeans.junit.NbTestCase;
import org.netbeans.modules.exceptions.utils.PersistenceUtils;
import org.netbeans.server.uihandler.DatabaseTestCase;
import org.netbeans.server.uihandler.LogsManager;
import org.netbeans.server.uihandler.LogsManagerTest;
import org.netbeans.server.uihandler.statistics.Actions.Calls;

/**
 *
 * @author Jaroslav Tulach
 */
public class ActionOldDelegatesTest extends DatabaseTestCase {
    
    public ActionOldDelegatesTest(String testName) {
        super(testName);
    }

    private LogsManager result;

    
    @Override
    protected Level logLevel() {
        return Level.FINE;
    }
    
    private File logs() throws IOException {
        File f = new File(getWorkDir(), "logs");
        f.mkdirs();
        return f;
    }
    
    
    @Override
    protected void setUp() throws Exception {
        clearWorkDir();
        
        super.setUp();

        result = LogsManagerTest.createManager(logs());
        result.preparePageContext(LogsManagerTest.createPageContext(), "", null);
    }
    
    protected void tearDown() throws Exception {
        result.preparePageContext(LogsManagerTest.createPageContext(), "", null);
        super.tearDown();
    }

    public void testCheckEditorCtrlL() throws Exception {
    
     String rec = " <record> " +
     " <date>2007-06-20T17:06:24</date>\n" +
     " <millis>1182351984792</millis>\n" +
     " <sequence>2485</sequence>\n" +
     " <level>FINE</level>\n" +
     " <thread>13</thread>\n" +
     " <message>UI_ACTION_EDITOR</message>\n" +
     " <param>java.awt.event.ActionEvent[ACTION_PERFORMED,cmd=,when=1182351984781,modifiers=Ctrl] on org.openide.text.QuietEditorPane[,0,0,1094x915,layout=javax.swing.plaf.basic.BasicTextUI$UpdateHandler,alignmentX=0.0,alignmentY=0.0,border=javax.swing.plaf.basic.BasicBorders$MarginBorder@c6fe3a,flags=296,maximumSize=,minimumSize=,preferredSize=,caretColor=java.awt.Color[r=0,g=0,b=0],disabledTextColor=javax.swing.plaf.ColorUIResource[r=184,g=207,b=229],editable=true,margin=java.awt.Insets[top=0,left=0,bottom=0,right=0],selectedTextColor=sun.swing.PrintColorUIResource[r=51,g=51,b=51],selectionColor=javax.swing.plaf.ColorUIResource[r=184,g=207,b=229],kit=org.netbeans.modules.editor.java.JavaKit@60abc5,typeHandlers=]</param>\n" +
     " <param>java.awt.event.ActionEvent[ACTION_PERFORMED,cmd=,when=1182351984781,modifiers=Ctrl] on org.openide.text.QuietEditorPane[,0,0,1094x915,layout=javax.swing.plaf.basic.BasicTextUI$UpdateHandler,alignmentX=0.0,alignmentY=0.0,border=javax.swing.plaf.basic.BasicBorders$MarginBorder@c6fe3a,flags=296,maximumSize=,minimumSize=,preferredSize=,caretColor=java.awt.Color[r=0,g=0,b=0],disabledTextColor=javax.swing.plaf.ColorUIResource[r=184,g=207,b=229],editable=true,margin=java.awt.Insets[top=0,left=0,bottom=0,right=0],selectedTextColor=sun.swing.PrintColorUIResource[r=51,g=51,b=51],selectionColor=javax.swing.plaf.ColorUIResource[r=184,g=207,b=229],kit=org.netbeans.modules.editor.java.JavaKit@60abc5,typeHandlers=]</param>\n" +
     " <param>org.netbeans.editor.ActionFactory$WordMatchAction[word-match-prev]</param>\n" +
     " <param>org.netbeans.editor.ActionFactory$WordMatchAction@cbd8ca</param>\n" +
     " <param>word-match-prev</param> \n" +
     " </record> ";
     
        File log = LogsManagerTest.extractString(logs(), rec, "log444");
        result.addLog(log, "127.0.0.1");
        
        PageContext pg = LogsManagerTest.createPageContext();
        
        result.preparePageContext(pg, "log444", null);
        
        Object attr = pg.getAttribute("userActions");
        assertNotNull("Value provided", attr);
        assertEquals(true, attr instanceof Calls);
        
        Calls calls = (Calls)attr;
        Map<String,Integer> all = calls.getTopTen(10, null, null, Actions.Invocations.ALL);

        String dumpAll = all.toString();
        if (dumpAll.indexOf("Delegate") >= 0) {
            fail("Some Delegate:\n" + dumpAll);
        }
        if (dumpAll.indexOf("ProjectAction") >= 0) {
            fail("Some ProjectAction:\n" + dumpAll);
        }
        if (dumpAll.indexOf("DebuggerAction") >= 0) {
            fail("Some DebuggerAction:\n" + dumpAll);
        }
        
        assertEquals("some elements:\n" + all, 1, all.size());
        assertEquals("one Save", 1, all.get("org.netbeans.editor.word-match-prev").intValue());
    }
    
    
    public void testGetActionsInvoked() throws Exception {
        
        String rec =
            "<record>" + 
          "<date>2007-03-12T10:46:54</date>" +
          "<millis>1173656814201</millis>" +
          "<sequence>653</sequence>" +
          "<level>FINER</level>" +
          "<thread>11</thread>" +
          "<message>UI_ACTION_KEY_PRESS</message>" +
          "<param>ctrl pressed S</param>" +
          "<param>ctrl pressed S</param>" +
          "<param>org.openide.util.actions.CookieAction$CookieDelegateAction@1165263[delegate=org.openide.actions.SaveAction@1e54e00]</param>" +
          "<param>org.openide.util.actions.CookieAction$CookieDelegateAction</param>" +
          "<param>&amp;Save</param>" +
    "</record>";

        File log = LogsManagerTest.extractString(logs(), rec, "log444");
        result.addLog(log, "127.0.0.1");
        
        PageContext pg = LogsManagerTest.createPageContext();
        
        result.preparePageContext(pg, "log444", null);
        
        Object attr = pg.getAttribute("userActions");
        assertNotNull("Value provided", attr);
        assertEquals(true, attr instanceof Calls);
        
        Calls calls = (Calls)attr;
        Map<String,Integer> all = calls.getTopTen(10, null, null, Actions.Invocations.ALL);

        String dumpAll = all.toString();
        if (dumpAll.indexOf("Delegate") >= 0) {
            fail("Some Delegate:\n" + dumpAll);
        }
        if (dumpAll.indexOf("ProjectAction") >= 0) {
            fail("Some ProjectAction:\n" + dumpAll);
        }
        if (dumpAll.indexOf("DebuggerAction") >= 0) {
            fail("Some DebuggerAction:\n" + dumpAll);
        }
        
        assertEquals("some elements:\n" + all, 1, all.size());
        assertEquals("one Save", 1, all.get("org.openide.actions.SaveAction").intValue());
    }
    
    
      
    public void testAbilityToParseOldActions() throws Exception {
        
        String rec =
            "<record>" + 
          "<date>2007-03-12T10:46:54</date>" +
          "<millis>1173656814201</millis>" +
          "<sequence>653</sequence>" +
          "<level>FINER</level>" +
          "<thread>11</thread>" +
          "<message>UI_ACTION_KEY_PRESS</message>" +
          "<param>ctrl pressed S</param>" +
          "<param>ctrl pressed S</param>" +
          "<param>org.openide.util.actions.CookieAction$CookieDelegateAction[&amp;Save]</param>" +
          "<param>org.openide.util.actions.CookieAction$CookieDelegateAction</param>" +
          "<param>&amp;Save</param>" +
    "</record>";

        File log = LogsManagerTest.extractString(logs(), rec, "log444");
        result.addLog(log, "127.0.0.1");
        
        PageContext pg = LogsManagerTest.createPageContext();
        
        result.preparePageContext(pg, "log444", null);
        
        Object attr = pg.getAttribute("userActions");
        assertNotNull("Value provided", attr);
        assertEquals(true, attr instanceof Calls);
        
        Calls calls = (Calls)attr;
        Map<String,Integer> all = calls.getTopTen(10, null, null, Actions.Invocations.ALL);

        String dumpAll = all.toString();
        if (dumpAll.indexOf("Delegate") >= 0) {
            fail("Some Delegate:\n" + dumpAll);
        }
        if (dumpAll.indexOf("ProjectAction") >= 0) {
            fail("Some ProjectAction:\n" + dumpAll);
        }
        if (dumpAll.indexOf("DebuggerAction") >= 0) {
            fail("Some DebuggerAction:\n" + dumpAll);
        }
        
        assertEquals("some elements:\n" + all, 1, all.size());
        assertEquals("one Save", 1, all.get("org.openide.actions.SaveAction").intValue());
    }

          public void testParseProject() throws Exception {
        
        String rec =
"<record>\n" +
"  <date>2007-04-15T22:27:29</date>\n" +
"  <millis>1176668849853</millis>\n" +
"  <sequence>173</sequence>\n" +
"  <level>FINER</level>\n" +
"  <thread>11</thread>\n" +
"  <message>Invoking &amp;Run Main Project implemented as org.netbeans.modules.project.ui.actions.MainProjectAction thru javax.swing.JButt\n" +
"on</message>\n" +
"  <key>UI_ACTION_BUTTON_PRESS</key>\n" +
"  <catalog>org.openide.awt.Bundle</catalog>\n" +
"  <param>javax.swing.JButton</param>\n" +
"  <param>javax.swing.JButton</param>\n" +
"  <param>org.netbeans.modules.project.ui.actions.MainProjectAction@bfdbc9</param>\n" +
"  <param>org.netbeans.modules.project.ui.actions.MainProjectAction</param>\n" +
"  <param>&amp;Run Main Project</param>\n" +
"</record>\n";

        File log = LogsManagerTest.extractString(logs(), rec, "log444");
        result.addLog(log, "127.0.0.1");
        
        PageContext pg = LogsManagerTest.createPageContext();
        
        result.preparePageContext(pg, "log444", null);
        
        Object attr = pg.getAttribute("userActions");
        assertNotNull("Value provided", attr);
        assertEquals(true, attr instanceof Calls);
        
        Calls calls = (Calls)attr;
        Map<String,Integer> all = calls.getTopTen(10, null, null, Actions.Invocations.ALL);

        String dumpAll = all.toString();
        if (dumpAll.indexOf("Delegate") >= 0) {
            fail("Some Delegate:\n" + dumpAll);
        }
        if (dumpAll.indexOf("ProjectAction") >= 0) {
            fail("Some ProjectAction:\n" + dumpAll);
        }
        if (dumpAll.indexOf("DebuggerAction") >= 0) {
            fail("Some DebuggerAction:\n" + dumpAll);
        }
        
        assertEquals("some elements:\n" + all, 1, all.size());
        assertEquals("one Project", Integer.valueOf(1), all.get("org.netbeans.modules.project.actions.RunMainProject"));
    }

}
