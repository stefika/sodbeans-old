/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */
package org.netbeans.server.uihandler.statistics;

import java.io.InputStream;
import java.util.Map;
import java.util.logging.Handler;
import java.util.logging.LogRecord;
import org.netbeans.junit.NbTestCase;
import org.netbeans.lib.uihandler.LogRecords;
import org.netbeans.server.uihandler.statistics.NetBeansModules.ModuleInfo;
import org.netbeans.server.uihandler.statistics.NetBeansModules.NBMData;

/**
 *
 * @author Jindrich Sedek
 */
public class NetBeansModulesTest extends NbTestCase {

    public NetBeansModulesTest() {
        super("NetBeansModulesTest");
    }
    private int counter = 0;

    public void testPorcessWithVersions() throws Exception {
        testProcess(true);
    }

    public void testPorcessWithoutVersions() throws Exception {
        testProcess(false);
    }

    private void testProcess(final boolean withVersions) throws Exception {
        InputStream modulesIs = NetBeansModulesTest.class.getResourceAsStream("modules");
        final NetBeansModules modules = new NetBeansModules();
        modules.setRememberDistinctVersions(withVersions);
        final NetBeansModules.NBMData data[] = new NetBeansModules.NBMData[1];
        final String resolver = "org.apache.xml.resolver";
        final Integer[] resolverVersion = {1, 3};
        final String ruby = "org.netbeans.modules.ruby";
        final Integer[] rubyVersion = {0, 85};
        final String bridge = "org.netbeans.modules.lexer.editorbridge";
        final Integer[] bridgeVersion = {1, 5};
        final Integer[] bridgeVersion2 = {1, 5, 1};
        final String j2eeProfiler = "org.netbeans.modules.profiler.j2ee";
        Handler h = new Handler() {

            @Override
            public void publish(LogRecord record) {
                NetBeansModules.NBMData newData = modules.process(record);
                ModuleInfo info;
                assertNotNull(newData);
                data[0] = modules.join(newData, data[0]);

                switch (counter++) {
                    case 0://first record
                        assertNotNull(newData.allEnabled);
                        assertNotNull(newData.allEnabled.get(resolver));
                        assertEquals(1, newData.allEnabled.get(resolver).intValue());
                        if (withVersions) {
                            assertNotNull(newData.enabled);
                            assertNull(newData.disabled);
                            info = newData.enabled.get(resolver);
                            assertEquals(1, info.count(resolverVersion).intValue());
                            assertNull(info.count(rubyVersion));
                            info = newData.enabled.get(ruby);
                            assertNull(info);
                        }
                        break;
                    case 1://second record
                        if (withVersions) {
                            assertNotNull(data[0]);
                            assertNotNull(newData.disabled);
                            assertNull(newData.enabled);
                            info = newData.disabled.get(ruby);
                            assertNull(info);
                            info = newData.disabled.get(bridge);
                            assertNotNull(info);
                            //first + second record
                            assertNotNull(data[0]);
                            assertNotNull(data[0].disabled);
                            assertNotNull(data[0].enabled);
                            info = data[0].disabled.get(bridge);
                            assertNotNull(info);
                        }
                        assertEquals(1, newData.allDisabled.get(bridge).intValue());
                        assertEquals(1, data[0].allEnabled.get(resolver).intValue());
                        assertNull(data[0].allEnabled.get(bridge));
                        assertNull(newData.allDisabled.get(resolver));
                        if (withVersions) {
                            info = data[0].enabled.get(resolver);
                            assertNotNull(info);
                            assertEquals(1, info.count(resolverVersion).intValue());
                            assertNull(info.count(rubyVersion));
                            info = data[0].disabled.get(ruby);
                            assertNull(info);
                        }
                        break;
                    case 2:// first + second + third
                        if (withVersions) {
                            assertNotNull(data[0]);
                            assertNotNull(data[0].disabled);
                            assertNotNull(data[0].enabled);
                            info = data[0].disabled.get(bridge);
                            assertNotNull(info);
                            info = data[0].enabled.get(resolver);
                            assertNotNull(info);
                            assertEquals(2, info.count(resolverVersion).intValue());
                            assertNull(info.count(rubyVersion));
                        }
                        assertEquals(1, data[0].allDisabled.get(bridge).intValue());
                        assertEquals(2, data[0].allEnabled.get(resolver).intValue());
                        assertEquals(1, data[0].allEnabled.get(ruby).intValue());
                        if (withVersions) {
                            info = data[0].enabled.get(ruby);
                            assertNotNull(info);
                            assertEquals(1, info.count(rubyVersion).intValue());
                            assertNull(info.count(resolverVersion));
                        }
                        break;
                    case 3://first...fourth
                        if (withVersions) {
                            assertNotNull(data[0]);
                            assertNotNull(data[0].disabled);
                            assertNotNull(data[0].enabled);
                            info = data[0].disabled.get(bridge);
                            assertNotNull(info);
                            assertEquals(2, info.count(bridgeVersion).intValue());
                            assertNull(info.count(rubyVersion));
                            info = data[0].enabled.get(resolver);
                            assertNotNull(info);
                            assertEquals(2, info.count(resolverVersion).intValue());
                            assertNull(info.count(rubyVersion));
                        }
                        assertEquals(2, data[0].allDisabled.get(bridge).intValue());
                        assertEquals(2, data[0].allEnabled.get(resolver).intValue());
                        assertEquals(1, data[0].allEnabled.get(ruby).intValue());
                        if (withVersions) {
                            info = data[0].enabled.get(ruby);
                            assertNotNull(info);
                            assertEquals(1, info.count(rubyVersion).intValue());
                            assertNull(info.count(resolverVersion));
                        }
                        break;
                    case 4:
                        break;
                    case 5://whole modules file
                        if (withVersions) {
                            assertNotNull(data[0]);
                            assertNotNull(data[0].disabled);
                            assertNotNull(data[0].enabled);
                            info = data[0].disabled.get(bridge);
                            assertNotNull(info);
                            assertEquals(2, info.count(bridgeVersion).intValue());
                            assertEquals(1, info.count(bridgeVersion2).intValue());
                            assertNull(info.count(rubyVersion));
                        }
                        assertEquals(3, data[0].allDisabled.get(bridge).intValue());
                        assertEquals(3, data[0].allEnabled.get(resolver).intValue());
                        if (withVersions) {
                            info = data[0].enabled.get(resolver);
                            assertNotNull(info);
                            assertEquals(3, info.count(resolverVersion).intValue());
                            assertNull(info.count(rubyVersion));
                            info = data[0].enabled.get(ruby);
                            assertNotNull(info);
                            assertEquals(1, info.count(rubyVersion).intValue());
                            assertNull(info.count(resolverVersion));
                            info = data[0].enabled.get(j2eeProfiler);
                            assertNotNull(info);
                            info = data[0].disabled.get(j2eeProfiler);
                            assertNotNull(info);
                        }else{
                            assertNull(data[0].enabled);
                            assertNull(data[0].disabled);
                        }
                        assertEquals(1, data[0].allEnabled.get(ruby).intValue());
                        assertEquals(2, data[0].allEnabled.get(j2eeProfiler).intValue());
                        assertEquals(1, data[0].allDisabled.get(j2eeProfiler).intValue());
                        break;
                    default:
                        assertTrue("seven logs should be loaded", false);
                }
                compare(modules.join(newData, data[0]), modules.join(data[0], newData));
            }

            @Override
            public void flush() {
            }

            @Override
            public void close() throws SecurityException {
            }

            private void compare(Map<String, ModuleInfo> m1, Map<String, ModuleInfo> m2) {
                if (m1 == null) {
                    assertNull(m2);
                } else {
                    assertNotNull(m2);
                    assertEquals(m2.size(), m1.size());
                }
            }

            private void compare(NBMData d1, NBMData d2) {
                compare(d1.enabled, d2.enabled);
                compare(d1.disabled, d2.disabled);
            }
        };
        LogRecords.scan(modulesIs, h);
    }

}
