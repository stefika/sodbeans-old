/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * Copyright 2007 Sun Microsystems, Inc. All rights reserved.
 */

package org.netbeans.server.uihandler.statistics;

import java.io.File;
import java.util.Date;
import java.util.Map;
import org.netbeans.modules.exceptions.entity.Nbversion;
import org.netbeans.server.uihandler.DatabaseTestCase;
import org.netbeans.server.uihandler.LogsManager;
import org.netbeans.server.uihandler.LogsManagerTest;
import org.netbeans.web.Utils;

/**
 *
 * @author Jindrich Sedek
 */
public class ExceptionReportsTest extends DatabaseTestCase{

    public ExceptionReportsTest(String str) {
        super(str);
    }
    
    public void testRegisterPageContext() throws Exception{
        String IP1 = "127.0.0.1";
        ExceptionReports reports = new ExceptionReports();
        ExceptionReports.setMinCountAndBaseDate(0, new Date(108, 11, 1));
        LogsManager.getDefault().preparePageContext(page, IP1, null);
        ExceptionReports.Data exceptionReports =
            (ExceptionReports.Data)page.getAttribute("globalExceptionReports");
        
        assertEquals("items count", 0, exceptionReports.getMonthStats().size());

        File log = LogsManagerTest.extractResourceAs(LogsManagerTest.class, data, "1.log", "NB2040811605.0");
        log = LogsManagerTest.extractResourceAs(LogsManagerTest.class, data, "2.log", "NB2040811605.1");
        waitLog(log, IP1);
        log = LogsManagerTest.extractResourceAs(LogsManagerTest.class, data, "3.log", "NB2040811605.2");
        waitLog(log, IP1);
        log = LogsManagerTest.extractResourceAs(LogsManagerTest.class, data, "dup1", "NB2040811605.3");
        waitLog(log, IP1);
        
        Utils.processPersistable(reports);
        LogsManager.getDefault().preparePageContext(page, IP1, null);
        exceptionReports = (ExceptionReports.Data)page.getAttribute("globalExceptionReports");
        Nbversion version = perUtils.getAll(Nbversion.class).get(0);
        assertEquals(exceptionReports.getMonthStats().get(0), new ExceptionReports.ReportItem(1, 7, 1, version));
        assertEquals(exceptionReports.getMonthStats().get(1), new ExceptionReports.ReportItem(2, 7, 1, version));
        assertEquals(exceptionReports.getMonthStats().get(2), new ExceptionReports.ReportItem(3, 7, 1, version));
        
        log = LogsManagerTest.extractResourceAs(LogsManagerTest.class, data, "dup2", "NB2040811605.4");
        waitLog(log, IP1);
        log = LogsManagerTest.extractResourceAs(LogsManagerTest.class, data, "dup20", "NB2040811605.5");
        waitLog(log, IP1);
        log = LogsManagerTest.extractResourceAs(LogsManagerTest.class, data, "dup30", "NB2040811605.6");
        waitLog(log, IP1);

        Utils.processPersistable(reports);
        LogsManager.getDefault().preparePageContext(page, IP1, null);
        exceptionReports = (ExceptionReports.Data)page.getAttribute("globalExceptionReports");

        assertEquals(exceptionReports.getMonthStats().get(4), new ExceptionReports.ReportItem(8, 7, 1, version));
        assertEquals(exceptionReports.getMonthStats().get(3), new ExceptionReports.ReportItem(7, 7, 1, version));
        assertEquals(exceptionReports.getMonthStats().get(2), new ExceptionReports.ReportItem(3, 7, 2, version));
        assertEquals(exceptionReports.getMonthStats().get(0), new ExceptionReports.ReportItem(1, 7, 1, version));
        assertEquals(exceptionReports.getMonthStats().get(1), new ExceptionReports.ReportItem(2, 7, 1, version));
        assertFalse(exceptionReports.getMonthStats().contains(new ExceptionReports.ReportItem(10, 7, 0, version)));// 0 <= MIN_COUNT

        log = LogsManagerTest.extractResourceAs(LogsManagerTest.class, data, "dup4", "NB2040811605.7");
        waitLog(log, IP1);
        log = LogsManagerTest.extractResourceAs(LogsManagerTest.class, data, "outOfMem", "NB2040811605.8");
        waitLog(log, IP1);

        Utils.processPersistable(reports);
        LogsManager.getDefault().preparePageContext(page, IP1, null);
        exceptionReports = (ExceptionReports.Data)page.getAttribute("globalExceptionReports");
        assertEquals("REPORTS NUMBER", 8, exceptionReports.reportsCount);
        Map<String, Integer> comps = exceptionReports.getComponents();
        assertEquals("COMPS NUMBER", 4, comps.size());
        assertEquals("editor", 4, comps.get("editor").intValue());
        assertEquals("ide", 2, comps.get("ide").intValue());
        assertEquals("openide", 1, comps.get("openide").intValue());
        assertEquals("platform", 1, comps.get("platform").intValue());
        
        assertTrue(exceptionReports.getMonthStats().contains(new ExceptionReports.ReportItem(3, 7, 4, version)));
        assertFalse(exceptionReports.getMonthStats().contains(new ExceptionReports.ReportItem(9, 7, 0, version)));// 0 <= MIN_COUNT
        assertTrue(exceptionReports.getMonthStats().contains(new ExceptionReports.ReportItem(8, 7, 1, version)));
    }
    
    public void testReportItem() throws Exception{
        Nbversion version = new Nbversion();
        ExceptionReports.ReportItem item = new ExceptionReports.ReportItem(8, 7, 5, version);
        assertEquals("Aug7", item.getKey());
        item = new ExceptionReports.ReportItem(11, 4, 9, version);
        assertEquals("Nov4", item.getKey());
        item = new ExceptionReports.ReportItem(1, 2, 1, version);
        assertEquals("Jan2", item.getKey());
    }
}