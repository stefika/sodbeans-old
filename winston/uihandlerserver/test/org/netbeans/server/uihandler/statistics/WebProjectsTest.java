/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */
package org.netbeans.server.uihandler.statistics;

import java.io.File;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.prefs.Preferences;
import javax.servlet.jsp.PageContext;
import org.netbeans.junit.Log;
import org.netbeans.server.uihandler.statistics.WebProjects.WebProjectItem;
import org.netbeans.server.uihandler.DatabaseTestCase;
import org.netbeans.server.uihandler.LogsManagerTest;


/**
 *
 * @author honza
 */
public class WebProjectsTest extends DatabaseTestCase {
    private CharSequence seq;
    
    public WebProjectsTest() {
        super("WebProjectsTest");
    }
    
    @Override
    protected void setUp() throws Exception {
        super.setUp();
        assertEquals("NO logs", 0, logsManager.getNumberOfLogs());

        logsManager.preparePageContext(LogsManagerTest.createPageContext(), "", null);
        
        File log = LogsManagerTest.extractResourceAs(WebProjectsTest.class, data, "webprojects.log", "log444");
        
        seq = Log.enable("org.netbeans.server.uihandler.LogsManager", Level.WARNING);
        logsManager.addLog(log, "127.0.0.1");
    }
    
    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }
    
    public void testJoin() {
        WebProjects wp = new WebProjects();
        Map<WebProjects.WebProjectItem, Integer> one = new TreeMap<WebProjects.WebProjectItem, Integer> ();
        Map<WebProjects.WebProjectItem, Integer> two = new TreeMap<WebProjects.WebProjectItem, Integer> ();
        one.put(WebProjectItem.newServer("glassfish"), 2);
        one.put(WebProjectItem.newServer("tomcat"), 2);
        one.put(WebProjectItem.newFramework("spring"), 20);
        two.put(WebProjectItem.newServer("glassfish"), 1);
        two.put(WebProjectItem.newServer("tomcat"), 1);
        two.put(WebProjectItem.newFramework("spring"), 10);
        Map<WebProjects.WebProjectItem, Integer> joined = wp.join(one, two);
        assertEquals("FRAMEWORK:spring", new Integer(30), joined.get(WebProjectItem.newFramework("spring")));
        assertEquals("SERVER:tomcat", new Integer(3), joined.get(WebProjectItem.newServer("tomcat")));
        assertEquals("SERVER:glassfish", new Integer(3), joined.get(WebProjectItem.newServer("glassfish")));
    }

    public void testWebProjectItems() {
        WebProjectItem item = new WebProjectItem("SERVER:glassfish");
        WebProjectItem item2 = new WebProjectItem("SERVER:glassfish");
        WebProjectItem item3 = WebProjectItem.newServer("glassfish");
        WebProjectItem item4 = WebProjectItem.newServer("tomcat");
        assertEquals(item, item2);
        assertEquals(item.hashCode(), item2.hashCode());
        assertEquals(item, item3);
        assertEquals(item.hashCode(), item3.hashCode());
        assertEquals(WebProjectItem.Type.SERVER, item.getType());
        assertNotSame(item, item4);
        assertEquals("glassfish", item.getName());
        item = new WebProjectItem("FRAMEWORK:Spring Framework 2.5");
        assertEquals(WebProjectItem.Type.FRAMEWORK, item.getType());
        assertEquals("Spring Framework 2.5", item.getName());
    }

    public void testLongName(){
        WebProjectItem item = new WebProjectItem("FRAMEWORK:org.netbeans.modules.web.frameworks.facelets.FaceletsFrameworkProvider");
        assertTrue(item.toString().length() < Preferences.MAX_KEY_LENGTH);
    }

    public void testWebProjects() throws Exception {
        PageContext png = LogsManagerTest.createPageContext();
        logsManager.preparePageContext(png, "log444", null);

        Object o = png.getAttribute("globalWebProjects");
//        System.out.println("--- " + o);
//        assertNotNull("global statistics created", o);
//        if (o instanceof Set) {
//            // ok
//        } else {
//            fail("Right class Set: " + o);
//        }
//        
//        Set inv = (Set)o;
//        assertEquals("Two projects: " + inv, 2, inv.size());
//     
//        Iterator it = inv.iterator();
//        Map.Entry item1 = (Entry) it.next();
//        Map.Entry item2 = (Entry) it.next();
//
//        assertEquals("NbModule", item1.getKey());
//        assertEquals("2/3 of modules", 66, item1.getValue());
//        assertEquals("Suite", item2.getKey());
//        assertEquals("1/3 of suites", 33, item2.getValue());
    }
}
