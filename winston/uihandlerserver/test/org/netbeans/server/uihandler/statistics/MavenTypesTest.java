/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */
package org.netbeans.server.uihandler.statistics;

import java.io.File;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Level;
import javax.servlet.jsp.PageContext;
import org.netbeans.junit.Log;
import org.netbeans.server.uihandler.DatabaseTestCase;
import org.netbeans.server.uihandler.LogsManagerTest;


/**
 *
 * @author Milos Kleint
 */
public class MavenTypesTest extends DatabaseTestCase {
    private CharSequence seq;
    
    public MavenTypesTest() {
        super("MavenTypesTest");
    }
    
    @Override
    protected void setUp() throws Exception {
        super.setUp();
        assertEquals("NO logs", 0, logsManager.getNumberOfLogs());

        logsManager.preparePageContext(LogsManagerTest.createPageContext(), "", null);
        
        File log = LogsManagerTest.extractResourceAs(MavenTypesTest.class, data, "maventypes.log", "log444");
        
        seq = Log.enable("org.netbeans.server.uihandler.LogsManager", Level.WARNING);
        logsManager.addLog(log, "127.0.0.1");
    }
    
    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }
    
    public void testJoin() {
        MavenTypes wp = new MavenTypes();
        Map<MavenTypes.Item, Integer> one = new TreeMap<MavenTypes.Item, Integer> ();
        Map<MavenTypes.Item, Integer> two = new TreeMap<MavenTypes.Item, Integer> ();
        one.put(MavenTypes.Item.newPackaging("pom"), 2);
        one.put(MavenTypes.Item.newPackaging("jar"), 20);
        one.put(MavenTypes.Item.newPackaging("war"), 1);
        
        Map<MavenTypes.Item, Integer> joined = wp.join(one, two);
        assertEquals("SERVER:glassfish", new Integer(2), joined.get(MavenTypes.Item.newPackaging("pom")));
    }
    
    public void testMavenTypesItems() {
        MavenTypes.Item item = MavenTypes.Item.newPackaging("pom");
        MavenTypes.Item item2 = MavenTypes.Item.newPackaging("pom");
        MavenTypes.Item item3 = MavenTypes.Item.newPackaging("pom");
        assertEquals(item, item2);
        assertEquals(item.hashCode(), item2.hashCode());
        assertEquals(item, item3);
        assertEquals(item.hashCode(), item3.hashCode());
        assertEquals("pom", item.getName());
    }
    
    public void testWebProjects() throws Exception {
        PageContext png = LogsManagerTest.createPageContext();
        logsManager.preparePageContext(png, "log444", null);

        Object o = png.getAttribute("globalMavenTypes");
        System.out.println("--- " + o);
//        assertNotNull("global statistics created", o);
//        if (o instanceof Set) {
//            // ok
//        } else {
//            fail("Right class Set: " + o);
//        }
//        
//        Set inv = (Set)o;
//        assertEquals("Two projects: " + inv, 2, inv.size());
//     
//        Iterator it = inv.iterator();
//        Map.Entry item1 = (Entry) it.next();
//        Map.Entry item2 = (Entry) it.next();
//
//        assertEquals("NbModule", item1.getKey());
//        assertEquals("2/3 of modules", 66, item1.getValue());
//        assertEquals("Suite", item2.getKey());
//        assertEquals("1/3 of suites", 33, item2.getValue());
    }
}
