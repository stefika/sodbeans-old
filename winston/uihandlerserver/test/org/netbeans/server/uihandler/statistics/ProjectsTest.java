/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * Copyright 2007 Sun Microsystems, Inc. All rights reserved.
 */

package org.netbeans.server.uihandler.statistics;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.logging.Level;
import javax.servlet.jsp.PageContext;
import org.netbeans.junit.Log;
import org.netbeans.server.uihandler.DatabaseTestCase;
import org.netbeans.server.uihandler.LogsManagerTest;

/**
 *
 * @author Jaroslav Tulach
 */
public class ProjectsTest extends DatabaseTestCase {

    public ProjectsTest(String testName) {
        super(testName);
    }
    @Override
    protected Level logLevel() {
        return Level.FINE;
    }
    
    private File logs() throws IOException {
        File f = new File(getWorkDir(), "logs");
        f.mkdirs();
        return f;
    }
    
    @Override
    protected void setUp() throws Exception {
        super.setUp();
        logsManager.preparePageContext(LogsManagerTest.createPageContext(), "", null);
        
        File log = LogsManagerTest.extractResourceAs(ProjectsTest.class, logs(), "projects.log", "log444");
        
        Log.enable("org.netbeans.server.uihandler.LogsManager", Level.WARNING);
        logsManager.addLog(log, "127.0.0.1");
    }
    
    public void testProjectTypes() throws Exception {
        PageContext png = LogsManagerTest.createPageContext();
        logsManager.preparePageContext(png, "log444", null);

        Object o = png.getAttribute("userProjectTypes");
        assertNotNull("global statistics created", o);
        if (o instanceof Map) {
            // ok
        } else {
            fail("Right class Map: " + o);
        }
        
        Set inv = ((Map)o).entrySet();
        assertEquals("Two projects: " + inv, 2, inv.size());
     
        Iterator it = inv.iterator();
        Map.Entry item1 = (Entry) it.next();
        Map.Entry item2 = (Entry) it.next();

        assertEquals("NbModule", item1.getKey());
        assertEquals("double usage in one log is counted once", 50, item1.getValue());
        assertEquals("Suite", item2.getKey());
        assertEquals("double usage in one log is counted once", 50, item2.getValue());
    }
    
    public void testMinimalLimitsWithProjectTypes() throws Exception {
        File log = LogsManagerTest.extractResourceAs(ProjectsTest.class, logs(), "projects_1.log", "log444.1");
        logsManager.addLog(log, "127.0.0.1");

        PageContext png = LogsManagerTest.createPageContext();
        png.setAttribute("minimal", "50", PageContext.REQUEST_SCOPE);
        logsManager.preparePageContext(png, "log444", null);

        Object o = png.getAttribute("userProjectTypes");
        assertNotNull("global statistics created", o);
        if (o instanceof Map) {
            // ok
        } else {
            fail("Right class Map: " + o);
        }
        
        Set inv = ((Map)o).entrySet();
        assertEquals("Two projects: " + inv, 2, inv.size());
     
        Iterator it = inv.iterator();
        Map.Entry item1 = (Entry) it.next();
        Map.Entry item2 = (Entry) it.next();

        assertEquals("Others", item1.getKey());
        assertEquals("1x nbmodule bellow min", 33, item1.getValue());
        assertEquals("Suite", item2.getKey());
        assertEquals("2x suite", 66, item2.getValue());
    }
}
