/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * Portions Copyrighted 2007 Sun Microsystems, Inc.
 */

package org.netbeans.server.uihandler.statistics;

import java.io.File;
import java.util.Collections;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Level;
import javax.servlet.jsp.PageContext;
import org.netbeans.server.uihandler.DatabaseTestCase;
import org.netbeans.server.uihandler.LogsManagerTest;
import org.netbeans.server.uihandler.statistics.Actions.Calls;
import org.openide.util.NbCollections;

/**
 *
 * @author Jaroslav Tulach
 */
public class ActionsTest extends DatabaseTestCase {
    
    public ActionsTest(String testName) {
        super(testName);
    }

    @Override
    protected Level logLevel() {
        return Level.FINE;
    }
    
    @Override
    protected void setUp() throws Exception {
        super.setUp();

        logsManager.preparePageContext(LogsManagerTest.createPageContext(), "", null);
        
        File log = LogsManagerTest.extractResourceAs(HelpTest.class, data, "actions.log", "log444");
        
        logsManager.addLog(log, "127.0.0.1");
    }

    public void testGetActionsInvoked() throws Exception {
        PageContext pg = LogsManagerTest.createPageContext();
        
        logsManager.preparePageContext(pg, "noUser", null);
        
        Object attr = pg.getAttribute("globalActions");
        assertNotNull("Value provided", attr);
        assertEquals(true, attr instanceof Calls);
        
        Calls calls = (Calls)attr;
        Map<String,Integer> all = calls.getTopTen(10, null, null, Actions.Invocations.ALL);
        
        assertEquals("three elements:\n" + all, 3, all.size());
        assertEquals("two OpenFile", 2, all.get("org.netbeans.modules.openfile.OpenFileAction").intValue());
        assertEquals("two OpenProject", 2, all.get("org.netbeans.modules.project.ui.actions.OpenProject").intValue());
        assertEquals("one goto type", 1, all.get("org.netbeans.modules.java.actions.GoToTypeAction").intValue());
        
        {
            // order is from most used to less used
            Iterator<String> it;
            it = all.keySet().iterator();
            assertEquals("org.netbeans.modules.openfile.OpenFileAction", it.next());
            assertEquals("org.netbeans.modules.project.ui.actions.OpenProject", it.next());
            assertEquals("org.netbeans.modules.java.actions.GoToTypeAction", it.next());
        }
        
        {
            // check includes
            Map<String,Integer> type = calls.getTopTen(10, ".*GoTo.*", null, Actions.Invocations.ALL);
            assertEquals("just one element:\n" + type, 1, type.size());
            assertEquals("only goto type", 1, type.get("org.netbeans.modules.java.actions.GoToTypeAction").intValue());
        }
        
        {
            // check excludes
            Map<String,Integer> type = calls.getTopTen(10, null, ".*Open.*", Actions.Invocations.ALL);
            assertEquals("just one element:\n" + type, 1, type.size());
            assertEquals("only goto type", 1, type.get("org.netbeans.modules.java.actions.GoToTypeAction").intValue());
        }

        //verify invokations
        Map<String,Integer> menu = calls.getTopTen(10, null, null, Actions.Invocations.MENU);
        assertEquals("three elements:\n" + menu, 3, menu.size());
        assertEquals("two OpenFile", 2, menu.get("org.netbeans.modules.openfile.OpenFileAction").intValue());
        assertEquals("one OpenProject", 1, menu.get("org.netbeans.modules.project.ui.actions.OpenProject").intValue());
        assertEquals("one goto type", 1, menu.get("org.netbeans.modules.java.actions.GoToTypeAction").intValue());

        Map<String,Integer> toolbar = calls.getTopTen(10, null, null, Actions.Invocations.TOOLBAR);
        assertEquals("one element:\n" + toolbar, 1, toolbar.size());
        assertEquals("one OpenProject", 1, toolbar.get("org.netbeans.modules.project.ui.actions.OpenProject").intValue());

        Map<String,Integer> shortCut = calls.getTopTen(10, null, null, Actions.Invocations.SHORTCUT);
        assertEquals("no elements:\n" + shortCut, 0, shortCut.size());
    }
    public void testTopTenPublished() throws Exception {
        PageContext pg = LogsManagerTest.createPageContext();
        
        File log = LogsManagerTest.extractResourceAs(HelpTest.class, data, "nohelp.log", "someuser");
        
        logsManager.addLog(log, "127.0.0.1");
        
        
        logsManager.preparePageContext(pg, "someuser", null);
        
        Object attr = pg.getAttribute("globalActionsTop10");
        assertNotNull("Value provided", attr);
        assertEquals(true, attr instanceof Map);
        
        {
            Map<?,?> map = (Map<?,?>)attr;

            if (map.isEmpty()) {
                fail("Should not be empty: " + map);
            }

            Map.Entry<?,?> entry = map.entrySet().iterator().next();
            assertEquals("Strings are keys", String.class, entry.getKey().getClass());
            assertEquals("Ints are values", Integer.class, entry.getValue().getClass());
        }
        
        Object last = pg.getAttribute("lastActionsTop10");
        assertNotNull("Value provided", last);
        assertEquals(true, last instanceof Map);
        
        {
            Map<?,?> map = (Map<?,?>)last;

            Map<String,Integer> counts = NbCollections.checkedMapByFilter(map, String.class, Integer.class, true);
            for (Integer cnt : counts.values()) {
                assertNotNull("Cnt is not null", cnt);
                if (cnt == 0) {
                    fail("There should be always more than 0 elements: " + counts);
                }
            }

        }
    }

    public void testTopTenPublishedWithExcludes() throws Exception {
        PageContext pg = LogsManagerTest.createPageContext();
        pg.setAttribute("ActionsExcludes", ".*Open.*", PageContext.REQUEST_SCOPE);
        
        logsManager.preparePageContext(pg, "noUser", null);
        
        Object attr = pg.getAttribute("globalActionsTop10");
        assertNotNull("Value provided", attr);
        assertEquals(true, attr instanceof Map);
        
        Map<?,?> map = (Map<?,?>)attr;
        
        if (map.isEmpty()) {
            fail("Should not be empty: " + map);
        }
        
        Map.Entry<?,?> entry = map.entrySet().iterator().next();
        assertEquals("Strings are keys", String.class, entry.getKey().getClass());
        assertEquals("Ints are values", Integer.class, entry.getValue().getClass());
        Map<String,Integer> type = NbCollections.checkedMapByFilter(map, String.class, Integer.class, true);
        assertEquals("just one element:\n" + type, 1, type.size());
        assertEquals("only goto type", 1, type.get("org.netbeans.modules.java.actions.GoToTypeAction").intValue());
    }
    public void testTopTenPublishedWithCount() throws Exception {
        PageContext pg = LogsManagerTest.createPageContext();
        pg.setAttribute("ActionsCount", "1", PageContext.REQUEST_SCOPE);
        
        logsManager.preparePageContext(pg, "noUser", null);
        
        Object attr = pg.getAttribute("globalActionsTop10");
        assertNotNull("Value provided", attr);
        assertEquals(true, attr instanceof Map);
        
        Map<?,?> map = (Map<?,?>)attr;
        
        if (map.isEmpty()) {
            fail("Should not be empty: " + map);
        }
        
        Map.Entry<?,?> entry = map.entrySet().iterator().next();
        assertEquals("Strings are keys", String.class, entry.getKey().getClass());
        assertEquals("Ints are values", Integer.class, entry.getValue().getClass());
        Map<String,Integer> type = NbCollections.checkedMapByFilter(map, String.class, Integer.class, true);
        assertEquals("just one element:\n" + type, 1, type.size());
    }
    public void testTopTenWrongRegExp() throws Exception {
        PageContext pg = LogsManagerTest.createPageContext();
        pg.setAttribute("ActionsExcludes", "*", PageContext.REQUEST_SCOPE);
        
        logsManager.preparePageContext(pg, "noUser", null);
        
        Object attr = pg.getAttribute("globalActionsTop10");
        assertNotNull("Value provided", attr);
        assertEquals(true, attr instanceof Map);
        
        Map<?,?> map = (Map<?,?>)attr;
        
        if (map.isEmpty()) {
            fail("Should not be empty: " + map);
        }
        
        Map.Entry<?,?> entry = map.entrySet().iterator().next();
        assertEquals("Strings are keys", String.class, entry.getKey().getClass());
        assertEquals("Ints are values", Integer.class, entry.getValue().getClass());
    }
    
    public void testSizeOfData() throws Exception {
        PageContext pg = LogsManagerTest.createPageContext();
        
        logsManager.preparePageContext(pg, "noUser", null);
        
        Object attr = pg.getAttribute("globalActions");
        assertNotNull("Value provided", attr);
        assertEquals(true, attr instanceof Calls);

        Object[] skip = {
            "org.netbeans.modules.openfile".intern(),
            "org.netbeans.modules.project.ui.actions".intern(),
            "org.netbeans.modules.java.actions".intern(),
            "OpenFileAction".intern(),
            "OpenProject".intern(),
            "GoToTypeAction".intern()
        };
        assertSize("Should not be that big 16b with one field + 16b for array", Collections.singleton(attr), 32, skip);
    }
}
