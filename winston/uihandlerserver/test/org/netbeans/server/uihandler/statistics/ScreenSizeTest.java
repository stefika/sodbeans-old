/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */
package org.netbeans.server.uihandler.statistics;

import java.io.File;
import java.util.Map;
import javax.servlet.jsp.PageContext;
import org.netbeans.server.uihandler.DatabaseTestCase;
import org.netbeans.server.uihandler.LogsManagerTest;
import org.netbeans.server.uihandler.statistics.ScreenSize.Dimension;

/**
 *
 * @author Jindrich Sedek
 */
public class ScreenSizeTest extends DatabaseTestCase {

    public ScreenSizeTest(String name) {
        super(name);
    }

    public void testParsing() throws Exception {
        File log = LogsManagerTest.extractResourceAs(ScreenSizeTest.class, data, "screen.log", "log444");
        waitLog(log, "127.0.0.1");
        PageContext png = LogsManagerTest.createPageContext();
        logsManager.preparePageContext(png, "log444", null);
        Map<Dimension, Integer> screenResolution = (Map<Dimension, Integer>) png.getAttribute("globalScreenSizeRESOLUTION");
        Map<Integer, Integer> monitors = (Map<Integer, Integer>) png.getAttribute("globalScreenSizeMONITORS");
        assertNotNull(screenResolution);
        assertNotNull(monitors);
        assertTrue("not empty", monitors.size() > 0);
        assertTrue("not empty", screenResolution.size() > 0);
        assertEquals(2, screenResolution.get(new ScreenSize.Dimension(1600, 1200)).intValue());
        assertEquals(1, screenResolution.get(new ScreenSize.Dimension(1000, 800)).intValue());
        assertEquals(1, screenResolution.get(new ScreenSize.Dimension(800, 600)).intValue());
        assertEquals(2, monitors.get(1).intValue());
        assertEquals(2, monitors.get(2).intValue());
    }
}

