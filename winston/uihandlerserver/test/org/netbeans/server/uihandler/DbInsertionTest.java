/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */

package org.netbeans.server.uihandler;

import java.io.File;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.junit.Test;
import org.netbeans.modules.exceptions.entity.Comment;
import org.netbeans.modules.exceptions.entity.Exceptions;
import org.netbeans.modules.exceptions.entity.Jarfile;
import org.netbeans.modules.exceptions.entity.Line;
import org.netbeans.modules.exceptions.entity.Logfile;
import org.netbeans.modules.exceptions.entity.Method;
import org.netbeans.modules.exceptions.entity.Nbuser;
import org.netbeans.modules.exceptions.utils.PersistenceUtils;
import org.netbeans.server.componentsmatch.Component;

/**
 *
 * @author Jindrich Sedek
 */
public class DbInsertionTest extends DatabaseTestCase{
    EntityManager em;
    
    public DbInsertionTest(String testName) {
        super(testName);
    }

    @Override
    protected void tearDown() throws Exception {
        em.getTransaction().commit();
        em.close();
        super.tearDown();
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        em = perUtils.createEntityManager();
        em.getTransaction().begin();
    }
    
    public void testDoBothTests() throws Exception {
        doRun();
        em.getTransaction().commit();
        System.out.println("first test has finished");
        em.getTransaction().begin();
        doIssueZillaId();
    }

    private void doRun() throws Exception {
        
        
        List<Logfile> logFiles = AddLogFileTest.logFiles(em);
        assertEquals("Empty at start", 0, logFiles.size());
        
        LogRecord rec= new LogRecord(Level.SEVERE, "TESTING LOG RECORD");
        String[] users = {"GUEST", "GUEST"};
        String[] comments = {"COMMENT", "[versioncontrol/Subversion] \n test"};
        String[] productVersion = {"NetBeans IDE Dev (Build 200702121900)", "NetBeans IDE Dev (Build 20070416-1344)"};
        String[] params = new String[7];
        String[] fileNames = {"file.2", "file.3"};
        params[0] = "Linux, 2.6.15-1.2054_FC5smp, i386";
        params[1] = "Java HotSpot(TM) Client VM, 1.6.0-b105";
        params[2] = productVersion[0];
        params[3] = users[0];
        params[4] = "UnknownError : Summary Message";
        params[5] = comments[0];
        params[6] = "test";
        String userId = "file";
        int session = 3;
        
        rec.setParameters(params);
        Throwable thrown = new NullPointerException("TESTING NULL POINTER");
        Logfile log = new Logfile(userId, session);
        DbInsertion insertion = new DbInsertion(rec, thrown, new TestLogFileTask(em, log));
        ExceptionsData insertionResult = getInsertionData(insertion);
        assertFalse("THIS ISSUE IS NOT ANY DUPLICATE", insertionResult.isDuplicateReport());
        assertNull("NO ISSUEZILLA ID", insertionResult.getIssuezillaId());
        assertEquals("Exception report", ExceptionsData.DataType.EXC_REPORT, insertionResult.getDataType());
        waitLogsParsed();
        
        List<Nbuser> usersList = PersistenceUtils.getAll(em, Nbuser.class);
        Nbuser user = usersList.get(0);
        assertEquals("THERE MUST BE ONE USER", 1, usersList.size());
        assertEquals("USER NAME CHECK", user.getName(), params[3]);
        
        List<org.netbeans.modules.exceptions.entity.Exceptions> exceptionList = PersistenceUtils.getAll(em, org.netbeans.modules.exceptions.entity.Exceptions.class);
        org.netbeans.modules.exceptions.entity.Exceptions exception = exceptionList.get(0);
        assertEquals("THERE MUST BE ONE ISSUE", 1, exceptionList.size());
        assertEquals("THIS ISSUE WAS ADDED WITH ID", insertionResult.getSubmitId(), exception.getId());
        assertEquals("SUMMARY CHECK", exception.getSummary(), params[4]);
        assertEquals(fileNames[0], exception.getLogfileId().getFileName());
        assertEquals("VM MACHINE CHECK", exception.getVmId().getName(), params[1]);
        assertEquals("NB PRODUCT VERSION CHECK", exception.getLogfileId().getProductVersionId().getProductVersion(), params[2]);
        String os = exception.getOperatingsystemId().getName() + ", " + exception.getOperatingsystemId().getVersion() + ", " + exception.getOperatingsystemId().getArch();
        assertEquals("OPERATING SYSTEM NAME CHECK", os, params[0]);
        assertEquals("PRODUCT BUILD NUMBER", exception.getBuild(), Long.valueOf("070212"));
        assertEquals("NR. OF DUPLICATES SHOULD BE 1", 1, exception.getReportId().getSubmitCollection().size());
        assertEquals("STACKTRACE LINES NUMBER", thrown.getStackTrace().length, exception.getStacktrace().getLineCollection().size());

        List<Line> linesList = PersistenceUtils.getAll(em, Line.class);
        Iterator<Line> lineIterator = linesList.iterator();
        assertTrue("THERE MUST BE FEW LINES", linesList.size() > 0);
        List<StackTraceElement> list = Arrays.asList(thrown.getStackTrace());
        do{
            Line line = lineIterator.next();
            assertNotNull("JarFile is not inserted", line.getJarfileId());
            int lineOrder = line.getLinePK().getLineOrder();
            int lineNumber = line.getLinePK().getLinenumber();
            int stackLineNumber = list.get(lineOrder).getLineNumber();
            if (stackLineNumber < 0) stackLineNumber = 0;
            assertEquals(lineNumber, stackLineNumber);
        }while (lineIterator.hasNext());
        assertEquals(linesList.size(), list.size());
        
        Integer count = perUtils.count(Jarfile.class);
        assertTrue("THE NUMBER MUST BE NOT 0", count > 0);
        
        logFiles = AddLogFileTest.logFiles(em);
        assertEquals("JUST ONE logfile: " + logFiles,1, logFiles.size());
        assertEquals("Build Number", logFiles.get(0).getBuildnumber(), Long.valueOf("070212"));
        assertEquals("Upload Number", logFiles.get(0).getUploadNumber(), new Integer(session));
        assertEquals("userDir", logFiles.get(0).getUserdir(), userId);
        assertEquals("File Name", logFiles.get(0).getFileName(), userId+"."+String.valueOf(session-1));
        assertNull("IP is null - it was not set", logFiles.get(0).getIp());
        
        //********* ADD DUPLICATE *********/
        params[3] = users[1];
        params[5] = comments[1];
        params[2] = productVersion[1];
        rec.setParameters(params);
        log = new Logfile(userId, session+1);
        insertion = new DbInsertion(rec, thrown, new TestLogFileTask(em, log));
        insertionResult = getInsertionData(insertion);
        assertTrue("THIS ISSUE IS A DUPLICATE", insertionResult.isDuplicateReport());
        assertAreDuplicates("DUPLICATE INSERTION", em, new Integer(1), insertionResult.getSubmitId());
        assertNull("NO ISSUEZILLA ID", insertionResult.getIssuezillaId());
        assertEquals("Exception report", ExceptionsData.DataType.EXC_REPORT, insertionResult.getDataType());
        waitLogsParsed();
        
        Integer nextCount = perUtils.count(Jarfile.class);
        assertEquals("THE NUMBER OF JAR FILES MUST BE THE SAME", count, nextCount);
        
        usersList = PersistenceUtils.getAll(em, Nbuser.class);
        user = usersList.get(0);
        assertEquals("THERE MUST BE USER", 1, usersList.size());
        assertEquals(users[0], user.getName());
        
        
        
        List<Comment> commentList = PersistenceUtils.getAll(em, Comment.class);
        assertEquals("THERE SHOULD BE TWO COMMENTS", 2, commentList.size());
        //first record
        Comment comment = commentList.get(0);
        assertEquals(comments[0],comment.getComment());
        
        comment = commentList.get(1);
        //second record
        assertEquals(comments[1],comment.getComment());
        
        exceptionList = PersistenceUtils.getAll(em, org.netbeans.modules.exceptions.entity.Exceptions.class);
        exception = exceptionList.get(0);
        assertEquals("THERE MUST BE ONE ISSUE", 2, exceptionList.size());
        //check the first one
        assertEquals("THIS ISSUE WAS ADDED WITH ID", new Integer(1), exception.getId());
        assertEquals("SUMMARY CHECK", exception.getSummary(), params[4]);
        assertEquals(fileNames[0], exception.getLogfileId().getFileName());
        assertEquals("VM MACHINE CHECK", exception.getVmId().getName(), params[1]);
        assertEquals("NB PRODUCT VERSION CHECK", exception.getLogfileId().getProductVersionId().getProductVersion(), productVersion[0]);
        assertEquals("PRODUCT BUILD NUMBER", exception.getBuild(),Long.valueOf("070212"));
        os = exception.getOperatingsystemId().getName() + ", " + exception.getOperatingsystemId().getVersion() + ", " + exception.getOperatingsystemId().getArch();
        assertEquals("OPERATING SYSTEM NAME CHECK", os, params[0]);
//        assertNull("THIS SHOULD NOT BE A DUPLICATE", exception.getDuplicateof());// NULL
        //check the second one
        exception = exceptionList.get(1);
        assertEquals("THIS ISSUE WAS ADDED WITH ID", new Integer(2), exception.getId());
        assertEquals("SUMMARY CHECK", exception.getSummary(), params[4]);
        assertEquals(fileNames[1], exception.getLogfileId().getFileName());
        assertEquals("VM MACHINE CHECK", exception.getVmId().getName(), params[1]);
        assertEquals("NB PRODUCT VERSION CHECK", exception.getLogfileId().getProductVersionId().getProductVersion(), productVersion[1]);
        assertEquals("PRODUCT BUILD NUMBER", exception.getBuild(), Long.valueOf("070416"));
        os = exception.getOperatingsystemId().getName() + ", " + exception.getOperatingsystemId().getVersion() + ", " + exception.getOperatingsystemId().getArch();
        assertEquals("OPERATING SYSTEM NAME CHECK", os, params[0]);
//        assertNotNull("THIS SHOULD BE A DUPLICATE", exception.getDuplicateof());
//        assertEquals("THIS SHOULD BE A DUPLICATE OF ISSUE 1", exception.getDuplicateof(), exceptionList.get(0));
        assertEquals("COMPONENT FROM COMMENT", "versioncontrol", exception.getReportId().getComponent());
        assertEquals("SUBCOMPONENT FROM COMMENT", "Subversion", exception.getReportId().getSubcomponent());
        
        
        logFiles = AddLogFileTest.logFiles(em);
        assertEquals("TWO logfile",2, logFiles.size());
        //--- FIRST --//
        Logfile logFile = logFiles.get(0);
        assertEquals("Build Number", logFile.getBuildnumber(), Long.valueOf("070212"));
        assertEquals("Upload Number", logFile.getUploadNumber(), new Integer(session));
        assertEquals("userDir", logFile.getUserdir(), userId);
        assertEquals("File Name", logFile.getFileName(), userId+"."+String.valueOf(session-1));
        assertNull("IP is null - it was not set", logFile.getIp());
        assertNotNull(logFile.getProductVersionId());
        assertEquals("PRODUCT VERSION CHECK",  productVersion[0], logFile.getProductVersionId().getProductVersion());
        assertEquals("NBVersion CHECK", "Dev", logFile.getProductVersionId().getNbversionId().getVersion());

        //---SECOND--//
        logFile = logFiles.get(1);
        assertEquals("Build Number", logFile.getBuildnumber(), Long.valueOf("070416"));
        assertEquals("Upload Number", logFile.getUploadNumber(), new Integer(session+1));
        assertEquals("userDir", logFile.getUserdir(), userId);
        assertEquals("File Name", logFile.getFileName(), userId+"."+String.valueOf(session));
        assertNull("IP is null - it was not set", logFile.getIp());
        assertNotNull(logFile.getProductVersionId());
        assertEquals("PRODUCT VERSION CHECK",  productVersion[1], logFile.getProductVersionId().getProductVersion());
        assertEquals("NBVersion CHECK", "Dev", logFile.getProductVersionId().getNbversionId().getVersion());
    }
    
    private void doIssueZillaId() throws Exception {
        LogRecord rec= new LogRecord(Level.SEVERE, "TESTING LOG RECORD");
        String[] users = {"userName", "SECOND USER"};
        String[] comments = {"COMMENT", "SECOND COMMENT"};
        String[] productVersion = {"NetBeans IDE Dev (Build 200702121900)", "NetBeans IDE Dev (Build 20070416-1344)"};
        String[] params = new String[6];
        String[] fileNames = {"file.2", "file.3"};
        params[0] = "Linux, 2.6.15-1.2054_FC5smp, i386";
        params[1] = "Java HotSpot(TM) Client VM, 1.6.0-b105";
        params[2] = productVersion[0];
        params[3] = users[0];
        params[4] = "UnknownError : Summary Message";
        params[5] = comments[0];
        String userId = "file";
        int issueZillaId = 10;
        int session = 3;
        
        rec.setParameters(params);
        Throwable thrown = new NullPointerException("TESTING NULL POINTER");
        
        
        Logfile log = new Logfile(userId, session);
        DbInsertion insertion = new DbInsertion(rec, thrown, new TestLogFileTask(em, log));
        ExceptionsData insertionResult = insertionResult = getInsertionData(insertion);
        //called from different class => different stackTrace
        assertFalse("THIS ISSUE IS NOT ANY DUPLICATE", insertionResult.isDuplicateReport());
        assertEquals("NEW INSERTION NEW ID", 3, insertionResult.getSubmitId().intValue());
        assertNull("NO ISSUEZILLA ID", insertionResult.getIssuezillaId());
        assertEquals("Exception report", ExceptionsData.DataType.EXC_REPORT, insertionResult.getDataType());
        waitLogsParsed();
        
        List<org.netbeans.modules.exceptions.entity.Exceptions> exceptionList = PersistenceUtils.getAll(em, org.netbeans.modules.exceptions.entity.Exceptions.class);
        assertEquals("LIST SIZE", 3, exceptionList.size());
        org.netbeans.modules.exceptions.entity.Exceptions exception = exceptionList.get(2);
        exception.getReportId().setIssueId(issueZillaId);
        em.merge(exception);

        // ADD DUPLICATE
        log = new Logfile(userId, session);
        insertion = new DbInsertion(rec, thrown, new TestLogFileTask(em, log));
        insertionResult = insertionResult = getInsertionData(insertion);
        assertTrue("THIS ISSUE A DUPLICATE", insertionResult.isDuplicateReport());
        assertAreDuplicates("DUPLICATE INSERTION", em, 3, insertionResult.getSubmitId());
        waitLogsParsed();

        log = new Logfile(userId, session);
        insertion = new DbInsertion(rec, thrown, new TestLogFileTask(em, log));
        insertionResult = insertionResult = getInsertionData(insertion);
        assertTrue("THIS ISSUE A DUPLICATE", insertionResult.isDuplicateReport());
        assertAreDuplicates("DUPLICATE INSERTION", em, 3, insertionResult.getSubmitId());
        assertEquals("ISSUEZILLA ID", 10, insertionResult.getIssuezillaId().intValue());
        waitLogsParsed();
        
        
    }
    
    public static class TestLogFileTask extends LogFileTask{
        private Logfile logfile;
        private EntityManager testEm;
        public TestLogFileTask(EntityManager em, Logfile logfile) {
            super(new File(logfile.getFileName()), null, null);
            this.testEm = em;
            this.logfile = logfile;
            em.persist(logfile);
            runningTask = this;
        }
        @Override
        public EntityManager getTaskEntityManager() {
            return testEm;
        }

        @Override
        public Logfile getLogInfo() {
            return logfile;
        }
        
        public void markFinished(){
            notifyFinished();
        }
    }

    @Test
    public void testChangesetParse(){
        String str = "76d51071af8a tip";
        testStr(str);
    }

    @Test
    public void testChangesetParse2(){
        String str = "76d51071af8a+";
        testStr(str);
    }
    
    private void testStr(String str){
        LogRecord rec = new LogRecord(Level.CONFIG, str);
        Object[] params = new Object[6];
        params[5] = str;
        rec.setParameters(params);
        Long l = DbInsertion.parseBuildInfo(rec);
        String value = Long.toString(l, 16);
        assertTrue("Well transformed", str.contains(value));
    }

    @Test
    public void testSetChangeset(){
        LogRecord config = new LogRecord(Level.CONFIG, "TESTING LOG RECORD");
        String[] params = new String[7];
        params[0] = "Linux, 2.6.15-1.2054_FC5smp, i386";
        params[1] = "Java HotSpot(TM) Client VM, 1.6.0-b105";
        params[2] = "NetBeans IDE Dev (Build 200702121900)";
        params[3] = "GUEST";
        params[4] = "UnknownError : Summary Message";
        params[5] = "HALLO";
        params[6] = "test";
        config.setParameters(params);
        LogRecord buildInfo = new LogRecord(Level.CONFIG, org.netbeans.server.uihandler.Exceptions.BUILD_INFO_FILE);
        params = new String[6];
        params[0] = "200811100001";
        params[1] = "${buildday}";
        params[2] = "";
        params[3] = "trunk";
        params[4] = "";
        params[5] = "76d51071af8a tip";
        buildInfo.setParameters(params);

        Logfile log = new Logfile("dafadf", 1);
        DbInsertion insertion = new DbInsertion(config, new NullPointerException("null11"), buildInfo, new TestLogFileTask(em, log));
        ExceptionsData insertionResult = getInsertionData(insertion);
        Exceptions exc = em.find(Exceptions.class, insertionResult.getSubmitId());
        assertNotNull(exc);
        assertNotNull(exc.getLogfileId().getChangeset());
        assertEquals(Long.toString(exc.getLogfileId().getChangeset(), 16), "76d51071af8a");
    }

    
    @Test
    public void testGetComponentFromComment() throws Exception {
        String comment = "[javaee/JSP] \n this is my comment";
        Component comp = DbInsertion.getComponentFromComment(comment);
        assertEquals("javaee", comp.getComponent());
        assertEquals("JSP", comp.getSubComponent());

        comment = "unknown/nnnnnnnnnnnnn";
        comp = DbInsertion.getComponentFromComment(comment);
        assertNull("NO BRACKETS", comp);

        comment = "[unknown/nnnnnnnnnnnnn]";
        comp = DbInsertion.getComponentFromComment(comment);
        assertNull("NON EXISTING COMPONENT", comp);

    }

    @Test
    public void testFindBySimilarLine() throws Exception {
        File log = LogsManagerTest.extractResourceAs(data, "dup1", "NB2040811605.0");
        addLog(log, "NB2040811605");
        log = LogsManagerTest.extractResourceAs(data, "dup2", "NB2040811605.1");
        addLog(log, "NB2040811605");
        String e1Line = "org.openide.text.QuietEditorPane.firePropertyChange";
        String e2Line = "java.awt.EventDispatchThread.run";
        String justDup1 = "javax.swing.text.JTextComponent.getSelection";
        String substring ="java.awt.";
        String nothing = "my.class.Error";
        waitLogsParsed();
        List<Exceptions> list = perUtils.findExceptionsBySimilarLines(e1Line, false);
        assertEquals(2, list.size());

        list = perUtils.findExceptionsBySimilarLines(e2Line, false);
        assertEquals(2, list.size());

        list = perUtils.findExceptionsBySimilarLines(substring, false);
        assertEquals(2, list.size());

        list = perUtils.findExceptionsBySimilarLines(nothing, true);
        assertEquals(0, list.size());

        list = perUtils.findExceptionsBySimilarLines(justDup1, true);
        assertEquals(1, list.size());
        List<Exceptions> all = PersistenceUtils.getAll(em, Exceptions.class);

        assertTrue(all.size() >= 2);
        Exceptions latest = all.get(all.size() - 1);
        latest.setReportId(all.get(all.size() - 2).getReportId());
        em.merge(latest);
        em.getTransaction().commit();
        em.getTransaction().begin();

        list = perUtils.findExceptionsBySimilarLines(substring, true);
        assertEquals(2, list.size());

        list = perUtils.findExceptionsBySimilarLines(substring, false);
        assertEquals(1, list.size());
    }

     @Test
     public void testSourceJarInsertion() throws Exception {
        File log = LogsManagerTest.extractResourceAs(data, "dup2833", "NB2040811605.0");
        addLog(log, "NB2040811605");
        refreshEM();
        Map<String, Object> params = Collections.singletonMap("name", (Object)"java.awt.EventDispatchThread.pumpEvents");
        List<Method> methods = PersistenceUtils.executeNamedQuery(em, "Method.findByName", params, Method.class);
        assertEquals(1, methods.size());
        Method method = methods.get(0);
        assertNotNull(method.getSourcejar());
        assertEquals("rt.jar", method.getSourcejar().getName());

        params = Collections.singletonMap("name", (Object)"org.openide.loaders.FolderInstance.defaultProcessObjects");
        refreshEM();
        methods = PersistenceUtils.executeNamedQuery(em, "Method.findByName", params, Method.class);
        assertEquals(1, methods.size());
        method = methods.get(0);
        assertNull(method.getSourcejar());

        log = LogsManagerTest.extractResourceAs(data, "dup2834", "NB2040811605.1");
        addLog(log, "NB2040811605");
        refreshEM();
        methods = PersistenceUtils.executeNamedQuery(em, "Method.findByName", params, Method.class);
        assertEquals(1, methods.size());
        method = methods.get(0);
        assertNotNull(method.getSourcejar());
        assertEquals("org-openide-loaders.jar", method.getSourcejar().getName());
     }

     private void refreshEM(){
        em.getTransaction().commit();
        em.close();
        em = perUtils.createEntityManager();
        em.getTransaction().begin();
     }
     
    @Test
    public void testNotAddingBuildInfo() throws Exception {
        List<Logfile> logFiles = AddLogFileTest.logFiles(em);
        assertEquals("Empty at start", 0, logFiles.size());
        LogRecord rec= new LogRecord(Level.SEVERE, "TESTING LOG RECORD");
        String[] params = new String[9];
        params[0] = "Windows XP, 5.1, x86";
        params[1] = "Java HotSpot(TM) Client VM, 1.6.0_01-b06, Java(TM) SE Runtime Environment, 1.6.0_01-b06";
        params[2] = "NetBeans IDE 6.1 (Build 200804211638)";
        params[3] = "";
        params[4] = "200804211638";
        params[5] = "${buildday}";
        params[6] = "release61";
        params[7] = "";
        params[8] = "17e226212feb tip";
        String userId = "file";
        int session = 3;

        rec.setParameters(params);
        Throwable thrown = new NullPointerException("TESTING NULL POINTER");
        Logfile log = new Logfile(userId, session);
        DbInsertion insertion = new DbInsertion(rec, thrown, new TestLogFileTask(em, log));
        ExceptionsData insertionResult = getInsertionData(insertion);
        waitLogsParsed();
        Query q =  em.createQuery("SELECT e from Exceptions e where id = " + insertionResult.getSubmitId());
        Exceptions exc = (Exceptions) q.getSingleResult();
        assertNotNull(exc);
        q = em.createQuery("select c from Comment c where c.submitId = " + exc.getId());
        assertEquals("no comment should be added", 0, q.getResultList().size());
    }
    
     static ExceptionsData getInsertionData(DbInsertion insertion){
        ExceptionsData excData = new ExceptionsData();
        insertion.start(excData);
        return excData;
     }
     
}

