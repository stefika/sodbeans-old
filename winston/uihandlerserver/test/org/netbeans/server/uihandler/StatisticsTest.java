/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * Copyright 2007 Sun Microsystems, Inc. All rights reserved.
 */

package org.netbeans.server.uihandler;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.prefs.Preferences;
import javax.servlet.jsp.PageContext;
import org.netbeans.junit.MockServices;
import org.netbeans.junit.NbTestCase;

/**
 *
 * @author Jaroslav Tulach
 */
public class StatisticsTest extends NbTestCase {
    private LogsManager result;
    
    
    public StatisticsTest(String testName) {
        super(testName);
    }
    
    @Override
    protected Level logLevel() {
        return Level.FINE;
    }
    
    private File logs() throws IOException {
        File f = new File(getWorkDir(), "logs");
        f.mkdirs();
        return f;
    }
    
    
    protected void setUp() throws Exception {
        clearWorkDir();
        DatabaseTestCase.initPersitenceUtils(getWorkDir());
        
        MockServices.setServices(CntngStat.class);
        File log = LogsManagerTest.extractResourceAs(logs(), "1.log", "log444");
        
        result = LogsManagerTest.createManager(logs());
    }
    
    public void testHowManyLogsInALog() throws Exception {
        PageContext cnt = DatabaseTestCase.createPageContext();
        result.preparePageContext(cnt, "log444", null);
        
        assertTrue(CntngStat.finishSessionUploadCalled);
        assertEquals("log444", CntngStat.finishSessionUploadId);
        assertEquals("first session", 0, CntngStat.finishSessionUploadSession);
        assertEquals("this was not initial parsing", false, CntngStat.finishSessionUploadInitialParse);
        CntngStat.finishSessionUploadCalled = false;
        
        Object obj = cnt.getAttribute("globalCnts");
        assertNotNull("Data are there", obj);
        assertEquals(Integer.class, obj.getClass());
        assertEquals(18, obj);
        
        obj = cnt.getAttribute("lastCnts");
        assertNotNull("Last Data are there", obj);
        assertEquals(Integer.class, obj.getClass());
        assertEquals(18, obj);
        
        obj = cnt.getAttribute("userCnts");
        assertNotNull("User Data are there", obj);
        assertEquals(Integer.class, obj.getClass());
        assertEquals(18, obj);
        
        File newLog = LogsManagerTest.extractResourceAs(logs(), "1.log", "log444.1");
        
        result.addLog(newLog, "127.0.0.1");
        
        result.preparePageContext(cnt, "log444", null);

        assertTrue(CntngStat.finishSessionUploadCalled);
        assertEquals("log444", CntngStat.finishSessionUploadId);
        assertEquals("2nd session", 2, CntngStat.finishSessionUploadSession);
        assertEquals("this was initial parsing", true, CntngStat.finishSessionUploadInitialParse);
        CntngStat.finishSessionUploadCalled = false;
        
        obj = cnt.getAttribute("globalCnts");
        assertNotNull("Data are there", obj);
        assertEquals(Integer.class, obj.getClass());
        assertEquals(36, obj);
        
        obj = cnt.getAttribute("lastCnts");
        assertNotNull("Last Data are there", obj);
        assertEquals(Integer.class, obj.getClass());
        assertEquals(18, obj);
        
        obj = cnt.getAttribute("userCnts");
        assertNotNull("User Data are there", obj);
        assertEquals(Integer.class, obj.getClass());
        assertEquals(36, obj);
        
        
        File newUser = LogsManagerTest.extractResourceAs(logs(), "1.log", "log337");
        
        result.addLog(newUser, "127.0.0.1");
        
        result.preparePageContext(cnt, "log337", null);

        assertTrue(CntngStat.finishSessionUploadCalled);
        assertEquals("log337", CntngStat.finishSessionUploadId);
        assertEquals("1st session", 0, CntngStat.finishSessionUploadSession);
        assertEquals("this was initial parsing", true, CntngStat.finishSessionUploadInitialParse);
        CntngStat.finishSessionUploadCalled = false;
        
        
        obj = cnt.getAttribute("globalCnts");
        assertNotNull("Data are there", obj);
        assertEquals(Integer.class, obj.getClass());
        assertEquals(54, obj);
        
        obj = cnt.getAttribute("lastCnts");
        assertNotNull("Last Data are there", obj);
        assertEquals(Integer.class, obj.getClass());
        assertEquals(18, obj);
        
        obj = cnt.getAttribute("userCnts");
        assertNotNull("User Data are there", obj);
        assertEquals(Integer.class, obj.getClass());
        assertEquals(18, obj);
    }
    
    public static final class CntngStat extends Statistics<Integer> {
        private static boolean finishSessionUploadCalled;
        private static boolean finishSessionUploadInitialParse;
        private static String finishSessionUploadId;
        private static int finishSessionUploadSession;
        
        
        public CntngStat() {
            super("Cnts");
        }
        
        protected Integer newData() {
            return 0;
        }
        
        protected Integer process(LogRecord rec) {
            return 1;
        }
        
        protected Integer join(Integer one, Integer two) {
            return one + two;
        }
        
        protected Integer finishSessionUpload(
            String userId, int sessionNumber,
            boolean initialParse, Integer d
        ) {
            assertFalse("Not yet called", finishSessionUploadCalled);
            finishSessionUploadCalled = true;
            finishSessionUploadId = userId;
            finishSessionUploadSession = sessionNumber;
            finishSessionUploadInitialParse = initialParse;
            return d;
        }

        protected void write(Preferences pref, Integer d) {
            pref.putInt("value", d);
        }

        protected Integer read(Preferences pref) {
            return pref.getInt("value", 0);
        }
    } // end of CntngStat
    
}
