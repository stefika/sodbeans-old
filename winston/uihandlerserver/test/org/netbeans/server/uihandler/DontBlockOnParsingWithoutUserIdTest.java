/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * Copyright 2007 Sun Microsystems, Inc. All rights reserved.
 */

package org.netbeans.server.uihandler;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import javax.servlet.jsp.PageContext;
import org.netbeans.junit.MockServices;
import org.netbeans.junit.NbTestCase;
import static org.netbeans.server.uihandler.StatisticsParsingTest.CntngStat;

/**
 *
 * @author Jindrich Sedek
 */
public class DontBlockOnParsingWithoutUserIdTest extends NbTestCase {
    private LogsManager result;


    public DontBlockOnParsingWithoutUserIdTest(String testName) {
        super(testName);
    }

    @Override
    protected Level logLevel() {
        return Level.FINE;
    }

    private File logs() throws IOException {
        File f = new File(getWorkDir(), "logs");
        f.mkdirs();
        return f;
    }

    @Override
    protected void setUp() throws Exception {
        clearWorkDir();
        DatabaseTestCase.initPersitenceUtils(getWorkDir());
    }

    public void testHowManyLogsInALog() throws Exception {
        MockServices.setServices(CntngStat.class);
        LogsManagerTest.extractResourceAs(logs(), "1.log", "log444");

        result = LogsManager.createManager(logs());
        PageContext cnt = DatabaseTestCase.createPageContext();
        result.preparePageContext(cnt, null, null);

        assertEquals("log is not parsed yet", 0, CntngStat.finishSessionUpload.size());
        Object obj = cnt.getAttribute("globalCnts");
        assertEquals(0, obj);

    }

}
