/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */
package org.netbeans.server.uihandler;

import java.io.File;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import org.netbeans.junit.NbTestCase;
import org.netbeans.modules.exceptions.entity.Logfile;
import org.netbeans.modules.exceptions.entity.Nbversion;
import org.netbeans.modules.exceptions.entity.ProductVersion;
import org.netbeans.server.uihandler.api.Issue;

/**
 *
 * @author Jindrich Sedek
 */
public class UtilsTest extends NbTestCase {

    public UtilsTest(String name) {
        super(name);
    }

    Set<String> used = new HashSet<String>(5);
    public void testGetUploadDirPath() throws Exception {
        String path = getWorkDir().getAbsolutePath();
        used.clear();
        verifyOK(path, "hallo");
        verifyOK(path, "NB_WEBCOMMON_RUBY_GFMOD0d6885781-1b66-46cd-a0f7-db56f01fa9e0.3");
        verifyOK(path, "uigestures.83");
        verifyOK(path, "234234234234");
        verifyOK(path, "1");
        verifyOK(path, "");
        String result = Utils.getUploadDirPath(path, null);
        assertEquals(result, path);
    }

    private void verifyOK(String path, String str) {
        String result = Utils.getUploadDirPath(path, str);
        assertTrue(result.startsWith(path));
        assertTrue(new File(result).exists());
        assertTrue(new File(result).isDirectory());
        assertTrue(result.split(File.separator).length > path.split(File.separator).length);
        assertFalse("different directories", used.contains(result));
        used.add(result);
    }

    public void testGetFirstDevBuild(){
        Date d = new Date(2009, 11, 1);
        assertEquals(new Long(90901l), Utils.getThreeMonthsEarlierDate(d));

        d = new Date(2009, 1, 10);
        assertEquals(new Long(81110l), Utils.getThreeMonthsEarlierDate(d));

        d = new Date(2009, 2, 28);
        assertEquals(new Long(81228l), Utils.getThreeMonthsEarlierDate(d));

        d = new Date(2009, 4, 30);
        assertEquals(new Long(90228l), Utils.getThreeMonthsEarlierDate(d));
    }

    public void testIsCorrectTM() throws Exception {
        performTest("NetBeans IDE 6.1 RC2", "6.5M1", false);
        performTest("NetBeans IDE 6.5", "6.5", true);
        performTest("NetBeans IDE 6.5 Beta", "6.1", true);
        performTest("NetBeans IDE Dev", "6.1", true);
        performTest("NetBeans IDE 6.5 Beta", "Dev", false);
        performTest("NetBeans IDE 6.5 Beta", "TBD", true);
        performTest("NetBeans IDE 6.1", "6.5", false);
        performTest("NetBeans IDE 6.5", "7.0M1", false);
        performTest("NetBeans IDE 7.0 M1", "7.0M1", true);
    }

    private void performTest(String productVersion, String TM, boolean result){
        org.netbeans.modules.exceptions.entity.Exceptions exc = new org.netbeans.modules.exceptions.entity.Exceptions();
        Logfile lf = new Logfile();
        ProductVersion pv = new ProductVersion();
        Nbversion nb = new Nbversion();
        lf.setProductVersionId(pv);
        pv.setNbversionId(nb);
        exc.setLogfileId(lf);
        String nbVersion = org.netbeans.modules.exceptions.utils.Utils.getNbVersion(productVersion);
        nb.setVersion(nbVersion);
        Issue iss = new Issue(1);
        iss.setTargetMilestone(TM);
        assertEquals(result, Utils.isIssueTMBeforeExcProductVersion(exc, iss));
    }
}

