/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */

package org.netbeans.server.uihandler;

import java.io.ByteArrayInputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import org.netbeans.junit.Log;
import org.netbeans.junit.NbTestCase;
import org.netbeans.lib.uihandler.LogRecords;
import org.netbeans.server.uihandler.TipOfTheDay.Tip;
import org.netbeans.server.uihandler.statistics.ProjectTypes;

/**
 *
 * @author Jaroslav Tulach
 */
public class TipOfTheDayTest extends NbTestCase {
    private TipOfTheDay db;
    
    
    public TipOfTheDayTest(String testName) {
        super(testName);
    }
    
    protected void setUp() throws Exception {
        clearWorkDir();
        
    }
    
    protected void tearDown() throws Exception {
        super.tearDown();
    }
    
    protected Level logLevel() {
        return Level.INFO;
    }
    
    public void testGetDefaultReturnsSomething() throws Exception {
        CharSequence log = Log.enable("org.netbeans", Level.WARNING);
        TipOfTheDay t = TipOfTheDay.getDefault();
        assertNotNull(t);
        if (log.toString().indexOf("env") == -1) {
            fail("There should be a warning about environment variables:\n" + log);
        }
        assertNull("no help", t.find(null));
    }

    public void testTipOfTheDayIsParseable() throws Exception {
        db = TipOfTheDay.create(getClass().getResource("kb-2007-03-08.xml"));

        String log = 
"<record>" +
  "<date>2007-01-09T09:53:09</date>" + 
  "<millis>1168332789249</millis>" + 
  "<sequence>43</sequence>" + 
  "<logger>UIHandler:</logger>" + 
  "<level>CONFIG</level>" + 
  "<thread>10</thread>" + 
  "<message>Opening 1 FreeformProject Projects</message>" + 
  "<key>UI_OPEN_PROJECTS</key>" + 
  "<catalog>&lt;null&gt;</catalog>" + 
  "<param>org.netbeans.modules.ant.freeform.FreeformProject</param>" + 
  "<param>FreeformProject</param>" + 
  "<param>1</param>" + 
"</record>" + 
"<record>" + 
  "<date>2007-01-09T09:53:09</date>" + 
  "<millis>1168332789249</millis>" + 
  "<sequence>44</sequence>" + 
  "<logger>UIHandler:</logger>" + 
  "<level>CONFIG</level>" + 
  "<thread>10</thread>" + 
  "<message>Opening 6 J2SEProject Projects</message>" + 
  "<key>UI_OPEN_PROJECTS</key>" + 
  "<catalog>&lt;null&gt;</catalog>" + 
  "<param>org.netbeans.modules.java.j2seproject.J2SEProject</param>" + 
  "<param>J2SEProject</param>" + 
  "<param>6</param>" + 
"</record>";
        
        class H extends Handler {
            LogRecord one;
            LogRecord two;
            
            public void publish(LogRecord arg0) {
                if (one == null) {
                    one = arg0;
                    return;
                }
                if (two == null) {
                    two = arg0;
                    return;
                }
                fail("One two records expected");
            }

            public void flush() {
            }

            public void close() throws SecurityException {
            }
        }
        H h = new H();
        LogRecords.scan(new ByteArrayInputStream(log.getBytes()), h);
        assertNotNull("Two parsed", h.two);
        
        ProjectTypes types = new ProjectTypes();
        Statistics<ProjectTypes.Counts> stats = types;
        
        ProjectTypes.Counts one = stats.process(h.one);
        ProjectTypes.Counts two = stats.process(h.two);
        
        ProjectTypes.Counts res = stats.join(one, two);
        
        List<? extends Tip> tips = db.findAll(res.getUsages());
        
        assertNotNull("There is some tip for J2SE", tips);
        
        if (tips.isEmpty()) {
            fail("should not be empty");
        }
        
        if (tips.size() < 25) {
            fail("There is at least 25 tips for J2SE was " + tips.size() + ":\n" + tips);
        }
    }

    public void testParse() throws Exception{
        db = TipOfTheDay.create(getClass().getResource("kb-2007-03-08.xml"));
        Map<String, Integer> map = new HashMap<String, Integer>();
        map.put("J2SE", 10);
        Tip t = db.find(map.entrySet());
        assertNotNull("Some J2SE tip should be given", t);
    }
}
