/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */

package org.netbeans.server.uihandler;

import java.io.File;

/**
 *
 * @author Jindrich Sedek
 */
public class DuplicatesTest extends DatabaseTestCase{
    private ExceptionsData excData;
    private int reportID;
    
    /** Creates a new instance of DuplicatesTest
     * @param name testName
     */
    public DuplicatesTest(String name) {
        super(name);
    }
 
    public void testDuplicates() throws Exception{
        
        File log = LogsManagerTest.extractResourceAs(data, "dup1", "NB2040811605.0");
        excData = addLog(log, "NB2040811605");
        assertNotNull("MAPA SHOULD BE NOT NULL FOR THIS SESSIONS", excData);
        assertFalse("THIS ISSUE SHOULD HAVE NO DUPLICATES",  excData.isDuplicateReport());
        assertEquals("THIS ISSUE SHOULD HAVE ID 1",  1, excData.getSubmitId().intValue());
        
        //exactly the same
        File log2 = LogsManagerTest.extractResourceAs(data, "dup1", "NB2040811605.1");
        excData = addLog(log2, "NB2040811605");
        assertNotNull("MAPA SHOULD BE NULL FOR NO SESSIONS", excData);
        assertTrue("THIS ISSUE SHOULD HAVE DUPLICATES",  excData.isDuplicateReport());
        assertAreDuplicates("THIS ISSUE SHOULD BE DUPLICATE OF 1",  1, excData.getSubmitId());
        reportID = 2;

        addNext(3, true, 1);//ID 3
        addNext(2, false, 4);//ID 4
        addNext(4, true, 1);// ID 5
        addNext(5, true, 4);// ID 6
        addNext(10, false, 7);// ID 7
        addNext(11, false, 8);// ID 8
        addNext(12, true, 7);// ID 9
        addNext(13, true, 8);// ID 10
    }

    public void testIssue111879() throws Exception{
        addNext(20, false, 11);//ID 11
        addNext(21, false, 12);//ID 12
    }
    
    public void testIssue112883() throws Exception{
        addNext(22, false, 13);//ID 13
        addNext(23, false, 14);//ID 14
    }
    
    public void testIssue112156()throws Exception{// stacoverflow
        addNext(30, false, 15);//ID 15
        addNext(31, true, 15);//ID 16
    }
    
    public void testIssue117816() throws Exception{
        addNext(117816, false, 17);//ID 17
        addNext(117817, true, 17);//ID 18
    }
    
    public void testIssue2833() throws Exception{
        addNext(2833, false, 19);//ID 19
        addNext(2834, false, 20);//ID 20
    }

    private void addNext(int index, boolean duplicate, int id) throws Exception{
        File log = LogsManagerTest.extractResourceAs(data, "dup"+Integer.toString(index), "NB2040811605."+reportID);
        excData = addLog(log, "NB2040811605");
        assertNotNull("MAPA SHOULD BE NULL FOR NO SESSIONS", excData);
        assertEquals("THIS ISSUE SHOULD (NOT) BE A DUPLICATE",  duplicate, excData.isDuplicateReport());
        if (duplicate){
            assertTrue("THIS EXCEPTIONS SHOULD BE DUPLICATES " + id + " AND " + excData.getSubmitId(),  areDuplicates(id, excData.getSubmitId()));
        }else{
            assertEquals("THIS ISSUE SHOULD RETURN ID ",  id, excData.getSubmitId().intValue());
        }
        reportID++;
    }
        
}
