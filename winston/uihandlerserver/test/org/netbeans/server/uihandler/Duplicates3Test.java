/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */

package org.netbeans.server.uihandler;

import java.io.File;

/**
 *
 * @author Jindrich Sedek
 */
public class Duplicates3Test extends DatabaseTestCase{
    private ExceptionsData excData;
    private static Integer reportID = 1;

    public Duplicates3Test(String name) {
        super(name);
    }

    public void testIssue146792() throws Exception{
        addNext(22, false, 1);
        addNext(23, true, 1);
    }

    public void testIssue150349_stackoverflow() throws Exception{
        addNext(31, false, 3);
        addNext(30, false, 4);
    }

    public void testIssue150349() throws Exception{
        addNext(32, false, 5);
        addNext(33, false, 6);
    }

    public void testGeneratedReflectionMethodNamesWithNumbers() throws Exception{
        addNext(35, false, 7);
        addNext(36, true, 7);
        addNext(37, true, 7);
    }

    public void testRequestProcessorItem() throws Exception{
        addNext(41, false, 10);
        addNext(42, true, 10);
    }

    public void testCNFE() throws Exception{
        addNext("dupres/CNFE2.log", false, 12);
        addNext("dupres/CNFE.log", true, 12);
    }

    private void addNext(int index, boolean duplicate, int id) throws Exception{
        addNext("dupres/dup"+Integer.toString(index), duplicate, id);
    }
    private void addNext(String fileName, boolean duplicate, int id) throws Exception{
        File log = LogsManagerTest.extractResourceAs(data, fileName, "NB2040811605."+reportID);
        excData = addLog(log, "NB2040811605");
        assertNotNull("MAPA SHOULD BE NULL FOR NO SESSIONS", excData);
        assertEquals("THIS ISSUE SHOULD (NOT) BE A DUPLICATE",  duplicate, excData.isDuplicateReport());
        assertEquals("ISSUE SHOULD INCREASE", reportID, excData.getSubmitId());
        if (duplicate){
            assertTrue("THIS EXCEPTIONS SHOULD BE DUPLICATES " + id + " AND " + excData.getSubmitId(),  areDuplicates(id, excData.getSubmitId()));
        }else{
            assertFalse(excData.isDuplicateReport());
        }
        reportID++;
    }
    
}
