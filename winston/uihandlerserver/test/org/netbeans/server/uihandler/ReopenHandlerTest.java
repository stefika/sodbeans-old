/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2009 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */
package org.netbeans.server.uihandler;

import org.netbeans.modules.exceptions.utils.BugReporterFactory;
import java.io.File;
import java.util.List;
import javax.persistence.EntityManager;
import static org.junit.Assert.*;
import org.netbeans.modules.exceptions.entity.Exceptions;
import org.netbeans.modules.exceptions.entity.Report;
import org.netbeans.modules.exceptions.entity.Submit;
import org.netbeans.server.uihandler.api.Issue;

/**
 *
 * @author Jindrich Sedek
 */
public class ReopenHandlerTest extends DatabaseTestCase {

    private ExceptionsData excData;
    private int reportID;
    private int issueID = 10;

    public ReopenHandlerTest(String name) {
        super(name);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        Issue iss10 = BugReporterFactory.getDefaultReporter().getIssue(issueID);
        iss10.setIssueStatus("RESOLVED");
        iss10.setResolution("FIXED");
    }

    public void testProcessSplit() throws Exception {
        addNext(117816, false, 1);//ID 1
        addNext(117817, true, 1);//ID 2
        addNext(117816, true, 1);//ID 3
        markReportFixed();
        addNext(117817, true, 1);//ID 4
        processReopenHandler();

        EntityManager em = perUtils.createEntityManager();
        em.getTransaction().begin();
        Exceptions exc1 = em.find(Exceptions.class, 1);
        Exceptions exc2 = em.find(Exceptions.class, 2);
        Exceptions exc3 = em.find(Exceptions.class, 3);
        Exceptions exc4 = em.find(Exceptions.class, 4);
        List<Submit> col1 = exc1.getReportId().getSubmitCollection();
        assertTrue(col1.contains(exc3));
        assertFalse(col1.contains(exc4));
        assertEquals(2, col1.size());

        List<Submit> col2 = exc2.getReportId().getSubmitCollection();
        assertTrue(col2.contains(exc4));
        assertFalse(col2.contains(exc3));
        assertEquals(2, col2.size());

        em.getTransaction().commit();
        em.close();
    }

    public void testOpenCandidateExists() throws Exception {
        addNext(2, false, 5); // id 5
        markReportFixed();
        
        addNext(5, true, 5);//ID 6
        //
        EntityManager em = perUtils.createEntityManager();
        em.getTransaction().begin();
        Exceptions exc6 = em.find(Exceptions.class, 6);
        Report r2 = new Report(Utils.getNextId(Report.class));
        em.persist(r2);
        exc6.setReportId(r2);
        em.merge(exc6);
        em.getTransaction().commit();
        em.close();

        addNext(2, true, 5);//ID 7
        processReopenHandler();

        em = perUtils.createEntityManager();
        em.getTransaction().begin();
        Exceptions exc7 = em.find(Exceptions.class, 7);
        assertEquals("Open duplicate exists", r2, exc7.getReportId());
        em.getTransaction().commit();
        em.close();

    }

    private void addNext(int index, boolean duplicate, int id) throws Exception {
        File log = LogsManagerTest.extractResourceAs(data, "dup" + Integer.toString(index), "NB2040811605." + reportID);
        excData = addLog(log, "NB2040811605");
        assertNotNull("MAPA SHOULD BE NULL FOR NO SESSIONS", excData);
        assertEquals("THIS ISSUE SHOULD (NOT) BE A DUPLICATE", duplicate, excData.isDuplicateReport());
        if (duplicate) {
            assertTrue("THIS EXCEPTIONS SHOULD BE DUPLICATES " + id + " AND " + excData.getSubmitId(), areDuplicates(id, excData.getSubmitId()));
        } else {
            assertEquals("THIS ISSUE SHOULD RETURN ID ", id, excData.getSubmitId().intValue());
        }
        reportID++;
    }

    private void markReportFixed() {
        EntityManager em = perUtils.createEntityManager();
        em.getTransaction().begin();
        Report r = em.find(Exceptions.class, excData.getSubmitId()).getReportId();
        r.setIssueId(issueID);
        em.persist(r);
        em.getTransaction().commit();
        em.close();
    }

    private void processReopenHandler() {
        EntityManager em = perUtils.createEntityManager();
        em.getTransaction().begin();
        Exceptions exc = em.find(Exceptions.class, excData.getSubmitId());
        em.getTransaction().commit();

        em.getTransaction().begin();
        exc = (Exceptions) ReopenHandler.createHandler(exc).process(em);
        em.getTransaction().commit();
        em.close();
    }

    private void verifyIssueChecking(){
        EntityManager em = perUtils.createEntityManager();
        em.getTransaction().begin();
        Exceptions exc = em.find(Exceptions.class, excData.getSubmitId());
        Throwable thr = exc.getMockThrowable();
        assertNotNull(Utils.checkOpenIssues(em, thr));


        assertNotNull(Utils.checkIssue(em, thr));
        assertNull(Utils.checkOpenIssues(em, thr));
    }
}