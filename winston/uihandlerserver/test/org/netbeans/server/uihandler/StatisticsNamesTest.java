/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * Copyright 2007 Sun Microsystems, Inc. All rights reserved.
 */

package org.netbeans.server.uihandler;

import java.io.File;
import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.prefs.Preferences;
import javax.servlet.Servlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.el.ExpressionEvaluator;
import javax.servlet.jsp.el.VariableResolver;
import org.netbeans.junit.MockServices;
import org.netbeans.junit.NbTestCase;
import org.netbeans.modules.exceptions.utils.PersistenceUtils;

/**
 *
 * @author Jaroslav Tulach
 */
public class StatisticsNamesTest extends NbTestCase {
    private LogsManager result;
    
    
    public StatisticsNamesTest(String testName) {
        super(testName);
    }
    
    @Override
    protected Level logLevel() {
        return Level.FINE;
    }

    public void testDefaultName() {
        new CntngStat(0);
    }
    
    public void testSpecifiedName() {
        new CntngStat();
    }
    public static final class CntngStat extends Statistics<Integer> {
        private static boolean finishSessionUploadCalled;
        private static boolean finishSessionUploadInitialParse;
        private static String finishSessionUploadId;
        private static int finishSessionUploadSession;
        
        
        public CntngStat() {
            super("Cnts");
            
            
            assertEquals("The right name", name, "Cnts");
        }
        
        public CntngStat(int x) {
            super();
            
            assertEquals("Name is taken from the class", "CntngStat", name);
        }
        
        protected Integer newData() {
            return 0;
        }
        
        protected Integer process(LogRecord rec) {
            return 1;
        }
        
        protected Integer join(Integer one, Integer two) {
            return one + two;
        }
        
        protected Integer finishSessionUpload(
            String userId, int sessionNumber,
            boolean initialParse, Integer d
        ) {
            assertFalse("Not yet called", finishSessionUploadCalled);
            finishSessionUploadCalled = true;
            finishSessionUploadId = userId;
            finishSessionUploadSession = sessionNumber;
            finishSessionUploadInitialParse = initialParse;
            return d;
        }

        protected void write(Preferences pref, Integer d) {
        }

        protected Integer read(Preferences pref) {
            return newData();
        }
    } // end of CntngStat
}
