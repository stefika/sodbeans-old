/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */

package org.netbeans.server.uihandler;

import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import javax.persistence.EntityManager;
import org.netbeans.modules.exceptions.entity.Logfile;
import org.netbeans.modules.exceptions.utils.PersistenceUtils;
import org.openide.util.NbBundle;

/**
 *
 * @author Jindrich Sedek
 */
public class ExceptionsTest extends DatabaseTestCase {
    EntityManager em;
    
    public ExceptionsTest(String testName) throws IOException {
        super(testName);
    }

    @Override
    protected Level logLevel() {
        return Level.FINE;
    }

    public void testProcess() {
        Exceptions exceptions = new Exceptions();
        LogRecord log = getDataLog();
        LogRecord logThrown = getThrownLog();
        ExceptionsData excData = exceptions.process(log);
        assertEquals("PROCESS DATA", log,excData.getUserDataLog());
        excData = exceptions.process(logThrown);
        assertEquals("PROCESS EXCEPTION", logThrown,excData.getThrownLog());
        excData = exceptions.process(getInfoLog());
        assertNull("PROCESS INFO",excData.getThrownLog());
        excData = exceptions.process(getThrownLogDontInsert());
        assertNull("PROCESS DONT INSERT",excData.getThrownLog());
    }
    
    public void testJoin() {
        Exceptions exceptions = new Exceptions();
        ExceptionsData empty = exceptions.newData();
        ExceptionsData logData = exceptions.process(getDataLog());
        ExceptionsData thr = exceptions.process(getThrownLog());
        LogRecord log2 = getThrownLog();
        log2.setThrown(new UnknownError("ERRRROR"));
        ExceptionsData thr2 = exceptions.process(log2);
        ExceptionsData result;
        result = exceptions.join(empty, logData);
        assertEquals("JOIN DATA", logData, result);
        result = exceptions.join(empty, thr);
        assertEquals("JOIN THROWN", thr, result);
        result = exceptions.join(logData, thr);
        assertEquals("JOIN BOTH",logData.getUserDataLog(), result.getUserDataLog());
        assertEquals("JOIN BOTH",thr.getThrownLog(), result.getThrownLog());
        ExceptionsData result2;
        result2 = exceptions.join(result, empty);
        assertEquals("HOLD DATA", result, result2);
        result2 = exceptions.join(thr2, result);
        assertEquals("GET LAST EXCEPTION", result, result2);
        result2 = exceptions.join(result, thr2);
        if (result.equals(result2)) throw new AssertionError("EXCEPTION SHOULD BE REVRITEN BE THE LAST ONE");
        assertEquals("DATA SHOULD STAY THE SAME",result.getUserDataLog(), result2.getUserDataLog());
        assertNotNull(result.getUserDataLog());
        assertEquals("EXCEPTION SHOULD BE REVRITEN BE THE LAST ONE",thr2.getThrownLog(), result2.getThrownLog());
        assertNotNull(result.getThrownLog());
    }
    
    public void testFinishSessionUpload() throws Exception {
        String userId = "log4445";
        Exceptions exceptions = new Exceptions();
        ExceptionsData logData = exceptions.process(getDataLog());
        ExceptionsData thr = exceptions.process(getThrownLog());
        ExceptionsData both = exceptions.newData();
        ExceptionsData dontInsert = exceptions.process(getThrownLogDontInsert());
        both = exceptions.join(both, thr);
        both = exceptions.join(logData, both);
        ExceptionsData result = exceptions.finishSessionUpload(userId, 1, false, logData);
        checkResult("NOT INITIAL UPLOAD",result, null, null, false);//not INITIAL
        result = exceptions.finishSessionUpload(userId, 1, true, thr);
        checkResult("NO METADATA IN REPORT", result, null, null, false);//not META DATA

        em = PersistenceUtils.getInstance().createEntityManager();
        em.getTransaction().begin();
        Logfile logfile = new Logfile("log4445", 1);
        new DbInsertionTest.TestLogFileTask(em, logfile);//add log file into log LogFileTask -> unfinished

        result = exceptions.finishSessionUpload(userId, 1, true, logData);
        checkResult("NO THROWN IN REPORT", result, null, null, false);//not THROWN
        result = exceptions.finishSessionUpload(userId, 1, true, both);
        checkResult("THERE SHOULD BE NO DUPLICATE OF THIS ISSUE", result, 2, null, true);//not DUPLICATE
        em.getTransaction().commit();
        em.getTransaction().begin();

        logfile = new Logfile("log4445", 2);
        new DbInsertionTest.TestLogFileTask(em, logfile);
        
        result = exceptions.finishSessionUpload(userId, 1, true, both);// INSERT ONCE MORE
        checkResult("THIS ISSUE SHOULD BE A DUPLICATE OF #2", result, 3, 2, true);//duplicate of issue no 1
        em.getTransaction().commit();
        em.close();
    }
    
    private void checkResult(String str, ExceptionsData result, Integer issueId, Integer duplicateId, boolean isExcReport)throws Exception{
        assertNotNull(result);
        if (!isExcReport){
            assertFalse("RECORD SHOULD NOT BE Exception Report", result.isExceptionReport());
            assertFalse(result.isSlownessReport());
            assertFalse(result.isDuplicateReport());
            assertEquals(ExceptionsData.DataType.STATISTIC, result.getDataType());
        }
        else{
            assertTrue("RECORD SHOULD BE Exception Report", result.isExceptionReport());
            assertFalse(result.isSlownessReport());
            assertEquals(str, duplicateId != null, result.isDuplicateReport());
            if (duplicateId != null){
                assertAreDuplicates(str, duplicateId, result.getSubmitId());
            }
            assertEquals(str, issueId, result.getSubmitId());
        }
    }
    
    private LogRecord getDataLog(){
        LogRecord rec = new LogRecord(Level.CONFIG, Exceptions.USER_CONFIGURATION);
        ArrayList<String> params = new ArrayList<String>(6);
        params.add("Linux");
        params.add("HOT SPOT");
        params.add("NetBeans IDE Dev (Build 200810041417)");
        params.add("USER NAME");
        params.add("SUMMARY");
        params.add("COMMENT");
        rec.setResourceBundle(NbBundle.getBundle(ExceptionsTest.class));
        rec.setParameters(params.toArray());
        rec.setResourceBundleName("MUJ BUNDLE");
        return rec;
    }
    
    private LogRecord getThrownLog(){
        Throwable thrown = new NullPointerException("TESTING EXCEPTION");
        LogRecord logThrown = new LogRecord(Level.SEVERE, "CHYBA");
        logThrown.setThrown(thrown);
        return logThrown;
    }

    private LogRecord getThrownLogDontInsert(){
        Throwable thrown = new NullPointerException("TESTING EXCEPTION");
        LogRecord logThrown = new LogRecord(Level.SEVERE, Exceptions.DONT_INSERT + " CHYBA");
        logThrown.setThrown(thrown);
        return logThrown;
    }
    
    private LogRecord getInfoLog(){
        Throwable thrown = new NullPointerException("TESTING EXCEPTION");
        LogRecord logThrown = new LogRecord(Level.INFO, "WARNING");
        logThrown.setThrown(thrown);
        return logThrown;
    }
    
}
