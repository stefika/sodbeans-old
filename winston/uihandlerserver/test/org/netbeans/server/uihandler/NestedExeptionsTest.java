/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */

package org.netbeans.server.uihandler;

import java.io.File;
import java.util.logging.Level;

/**
 *
 * @author Jindrich Sedek
 */
public class NestedExeptionsTest extends DatabaseTestCase{
    private ExceptionsData excData;
    private static Integer reportID = 1;
    
    /** Creates a new instance of DuplicatesTest
     * @param name testName
     */
    public NestedExeptionsTest(String name) {
        super(name);
    }

    @Override
    protected Level logLevel() {
        return Level.FINE;
    }
    
    public void testNestedExceptions()throws Exception{
        addNext(10586, false, 1);//ID 1
        addNext(10733, false, 2);//ID 2
        addNext(10981, false, 3);//ID 3
    }

    public void testNestedExceptionsB()throws Exception{
        addNext(9828, false, 4);//ID 4
        addNext(430, false, 5);//ID 5
        addNext(431, true, 5);//ID 6
    }

    public void testNested() throws Exception{
        addNext(10997, false, 7);// ID 7
        addNext(9384, true, 7);// ID 8
        addNext(3310, false, 9);// ID 9
        addNext(8547, true, 9);// ID 10
        addNext(8523, true, 9);// ID 11
    }
    
    public void testNestedIssue135996() throws Exception{
        addNext(135996, false, 12);// ID 12
        addNext(1359961, true, 12);// ID 13
        addNext(1359962, true, 12);// ID 14
    }
    
    private void addNext(int index, boolean duplicate, int id) throws Exception{
        File log = LogsManagerTest.extractResourceAs(data, "dupres/nested"+Integer.toString(index), "NB2040811605."+reportID);
        excData = addLog(log, "NB2040811605");
        assertNotNull("MAPA SHOULD BE NULL FOR NO SESSIONS", excData);
        assertEquals("THIS ISSUE SHOULD (NOT) BE A DUPLICATE",  duplicate, excData.isDuplicateReport());
        assertEquals("ISSUE SHOULD INCREASE", reportID, excData.getSubmitId());
        if (duplicate){
            assertTrue("THIS EXCEPTIONS SHOULD BE DUPLICATES " + id + " AND " + excData.getSubmitId(),  areDuplicates(id, excData.getSubmitId()));
        }else{
            assertFalse(excData.isDuplicateReport());
        }
        reportID++;
    }
    
}
