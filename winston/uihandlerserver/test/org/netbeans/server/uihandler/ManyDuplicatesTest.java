/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */
package org.netbeans.server.uihandler;

import org.netbeans.modules.exceptions.utils.BugReporterFactory;
import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.Queue;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import javax.persistence.EntityManager;
import org.netbeans.server.uihandler.api.bugs.BugReporter;
import org.netbeans.modules.exceptions.entity.Logfile;
import org.netbeans.modules.exceptions.entity.Nbuser;
import org.netbeans.modules.exceptions.entity.Report;
import org.netbeans.server.uihandler.DbInsertionTest.TestLogFileTask;
import org.netbeans.server.uihandler.api.Issue;

/**
 *
 * @author Jindrich Sedek
 */
public class ManyDuplicatesTest extends DatabaseTestCase {

    private Integer rootId;
    private boolean isReopen = false;
    
    public ManyDuplicatesTest(String str) {
        super(str);
    }

    private LogRecord prepareRec(){
        LogRecord rec = new LogRecord(Level.SEVERE, "TESTING LOG RECORD");
        String directUserName = "exceptions";
        ArrayList<String> params = new ArrayList<String>(7);
        params.add("Linux, 2.6.15-1.2054_FC5smp, i386");
        params.add("Java HotSpot(TM) Client VM, 1.6.0-b105");
        params.add("NetBeans IDE Dev (Build 200702121900)");
        params.add(directUserName);
        String summary = "UnknownError : Summary Message";
        params.add(summary);
        params.add("COMMENT");
        params.add("petrzajac");
        rec.setParameters(params.toArray());
        return rec;
    }
    
 
    public void testManyDuplicatesFilter() throws Exception {
        TestHandler handler = new TestHandler();
        String commentUser = Utils.getReporterUsername();
        BugReporter.LOG.addHandler(handler);
        IZInsertion.setDuplicatesNo(new Long(1));

        LogRecord rec = prepareRec();
        Throwable thrown = new NullPointerException("TESTING NULL POINTER");
        EntityManager em = perUtils.createEntityManager();
        em.getTransaction().begin();
        String userId = "file";
        int session = 1;
        Logfile log = new Logfile(userId, session);
        TestLogFileTask task = new TestLogFileTask(em, log);
        DbInsertion insertion = new DbInsertion(rec, thrown, task);
        ExceptionsData excData = new ExceptionsData();
        insertion.start(excData);
        task.markFinished();

        Integer issueId = (Integer) excData.getSubmitId();
        int issuezillaId = 110;
        org.netbeans.modules.exceptions.entity.Exceptions exc =
                em.find(org.netbeans.modules.exceptions.entity.Exceptions.class, issueId);
        Report report = exc.getReportId();
        report.setIssueId(issuezillaId);
        em.merge(report);
        Issue issue = BugReporterFactory.getDefaultReporter().getIssue(issuezillaId);
        issue.setIssueStatus("NEW");
        issue.setLastResolutionChange(new Date());
        for (int i = 1; i < 3; i++) {
            em.getTransaction().commit();
            em.getTransaction().begin();

            task = new TestLogFileTask(em, log);
            insertion = new DbInsertion(rec, thrown, task);
            insertion.start(excData);
            task.markFinished();
            waitIssuezillaInsertionFinished();
            changeUserName(em, excData.getSubmitId());
        }
        handler.flush();
        for (int i = 3; i <= 9; i++) {
            em.getTransaction().commit();
            em.getTransaction().begin();

            task = new TestLogFileTask(em, log);
            insertion = new DbInsertion(rec, thrown, task);
            insertion.start(excData);
            task.markFinished();
            waitIssuezillaInsertionFinished();
            changeUserName(em, excData.getSubmitId());

            LogRecord result = handler.getResult();
            if (i == 3 || i == 9) {//priorityLog

                Object[] parameters = result.getParameters();
                assertNotNull("MANY DUPLICATES FILTER - ADD COMMENT", result);
                assertEquals(Integer.toString(issuezillaId), parameters[0].toString());
                assertEquals("default user is used", null, parameters[1]);
                String param3 = (String) parameters[3];
                assertNotNull("GENERATED MESSAGE", param3);
                assertTrue(param3 + i, param3.contains("already"));

                result = handler.getResult();
                assertNotNull("MANY DUPLICATES FILTER - INCREASE PRIORITY", result);
                parameters = result.getParameters();
                assertEquals(Integer.toString(issuezillaId), parameters[0].toString());
                assertEquals("default user is used", null, parameters[1]);
                param3 = (String) parameters[3];
                assertNotNull("PRIORITY", param3);
                if (i == 3) {
                    assertEquals("P2", param3);
                } else {
                    assertEquals("P1", param3);
                }

            } else {
                assertNull("NOT SO MANY DUPLICATES", result);
            }
            handler.flush();
        }
    }

    public void testManyDuplicatesFilterWithFixedIssue() throws Exception {
        TestHandler handler = new TestHandler();
        BugReporter.LOG.addHandler(handler);
        IZInsertion.setDuplicatesNo(new Long(1));

        LogRecord rec = prepareRec();
        Throwable thrown = new NullPointerException("TESTING NULL POINTER");
        EntityManager em = perUtils.createEntityManager();
        em.getTransaction().begin();
        String userId = "file";
        int session = 1;
        Logfile log = new Logfile(userId, session);
        TestLogFileTask task = new TestLogFileTask(em, log);
        DbInsertion insertion = new DbInsertion(rec, thrown, task);
        ExceptionsData excData = new ExceptionsData();
        insertion.start(excData);
        task.markFinished();

        Integer issueId = (Integer) excData.getSubmitId();
        int issuezillaId = 11;
        org.netbeans.modules.exceptions.entity.Exceptions exc =
                em.find(org.netbeans.modules.exceptions.entity.Exceptions.class, issueId);
        Report report = exc.getReportId();
        report.setIssueId(issuezillaId);
        em.merge(report);
        Issue issue = BugReporterFactory.getDefaultReporter().getIssue(issuezillaId);
        issue.setIssueStatus("NEW");
        issue.setLastResolutionChange(new Date());
        for (int i = 1; i < 3; i++) {
            em.getTransaction().commit();
            em.getTransaction().begin();

            task = new TestLogFileTask(em, log);
            insertion = new DbInsertion(rec, thrown, task);
            insertion.start(excData);
            task.markFinished();
            waitIssuezillaInsertionFinished();
            changeUserName(em, excData.getSubmitId());
        }
        issue.setIssueStatus("RESOLVED");
        issue.setResolution("FIXED");
        handler.flush();
        for (int i = 3; i <= 9; i++) {
            em.getTransaction().commit();
            em.getTransaction().begin();

            task = new TestLogFileTask(em, log);
            insertion = new DbInsertion(rec, thrown, task);
            insertion.start(excData);
            task.markFinished();
            waitIssuezillaInsertionFinished();
            changeUserName(em, excData.getSubmitId());

            LogRecord result = handler.getResult();
            assertNull("ISSUE IS FIXED", result);
            handler.flush();
        }
    }

    public void testInsertIssuesWithManyDuplicates() throws Exception {
        TestHandler handler = new TestHandler("BUG REPORTER");
        BugReporter.LOG.addHandler(handler);
        IZInsertion.setDuplicatesNo(new Long(2));
        EntityManager em = perUtils.createEntityManager();
        em.getTransaction().begin();
        int session = 0;
        org.netbeans.modules.exceptions.entity.Exceptions exc = insert(em, session++);
        for (int i = 0; i < 5; i++) {
            insert(em, session++);
            org.netbeans.modules.exceptions.entity.Exceptions dup = em.find(org.netbeans.modules.exceptions.entity.Exceptions.class, exc.getId() + i);
            changeUserName(em, dup.getId());
        }
        assertNotNull(exc);
        exc.getReportId().setIssueId(null);
        em.merge(exc);
        em.getTransaction().commit();
        em.close();

        em = perUtils.createEntityManager();
        em.getTransaction().begin();
        handler.flush();
        exc = insert(em, session++);
        waitIssuezillaInsertionFinished();
        LogRecord result = handler.getResult();
        assertNotNull(result);

        em.getTransaction().commit();
        em.close();
    }

    private void changeUserName(EntityManager em, int issueId) throws Exception {
        Nbuser user = new Nbuser(Utils.getNextId(Nbuser.class));
        user.setName("testName" + user.getId());
        em.persist(user);
        org.netbeans.modules.exceptions.entity.Exceptions exc = em.find(org.netbeans.modules.exceptions.entity.Exceptions.class, issueId);
        exc.setNbuserId(user);
        em.merge(exc);
        em.getTransaction().commit();
        em.getTransaction().begin();
    }

    private org.netbeans.modules.exceptions.entity.Exceptions insert(EntityManager em, DbInsertion dbInsertion) throws Exception {
        ExceptionsData eData = new ExceptionsData();
        dbInsertion.start(eData);
        assertTrue((Boolean) eData.isDuplicateReport());
        assertAreDuplicates(rootId, eData.getSubmitId());
        return em.find(org.netbeans.modules.exceptions.entity.Exceptions.class, rootId);
    }

    private class TestHandler extends Handler {

        Queue<LogRecord> result = new LinkedList<LogRecord>();
        final String message;

        private TestHandler() {
            this("BUG REPORTER");
        }

        private TestHandler(String mes) {
            this.message = mes;
        }

        public void publish(LogRecord record) {
            if (message.equals(record.getMessage())) {
                result.add(record);
            }
        }

        public void flush() {
            result.clear();
        }

        public void close() throws SecurityException {
            //
        }

        public LogRecord getResult() {
            return result.poll();
        }
    }

    private org.netbeans.modules.exceptions.entity.Exceptions insert(EntityManager em, int session) throws Exception {
        return insert(em, session, null, null);
    }

    private org.netbeans.modules.exceptions.entity.Exceptions insert(EntityManager em, int session, String userName, String comment) throws Exception {
        LogRecord rec = new LogRecord(Level.SEVERE, "TESTING LOG RECORD");
        if (userName == null){
            userName = "exceptions";
        }
        if (comment == null){
            comment = "COMMENT";
        }
        ArrayList<String> params = new ArrayList<String>(7);
        params.add("Linux, 2.6.15-1.2054_FC5smp, i386");
        params.add("Java HotSpot(TM) Client VM, 1.6.0-b105");
        params.add("NetBeans IDE Dev (Build 200811100001)");
        params.add(userName);
        params.add("UnknownError : Summary Message");
        params.add(comment);
        params.add("petrzajac");
        rec.setParameters(params.toArray());
        Throwable thrown = new NullPointerException("TESTING NULL POINTER");
        String userId = "file";

        ExceptionsData eData = new ExceptionsData();
        Logfile log = new Logfile(userId, session);
        TestLogFileTask task = new TestLogFileTask(em, log);
        DbInsertion dbInsertion = new DbInsertion(rec, thrown, task);
        dbInsertion.start(eData);
        task.markFinished();
        waitIssuezillaInsertionFinished();
        if ((Boolean) eData.isDuplicateReport()) {
            if (!isReopen){
                assertAreDuplicates(rootId, eData.getSubmitId());
            }
        } else {
            rootId = eData.getSubmitId();
        }
        return em.find(org.netbeans.modules.exceptions.entity.Exceptions.class, rootId);
    }

    @Override
    protected void waitLog(File log, String ip) throws Exception {
        super.waitLog(log, ip);
        waitIssuezillaInsertionFinished();
    }
    
}
