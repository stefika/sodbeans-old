/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 2010 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 *
 * Contributor(s):
 *
 * Portions Copyrighted 2010 Sun Microsystems, Inc.
 */
package org.netbeans.server.uihandler.builds;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.ServletException;
import org.netbeans.server.uihandler.DatabaseTestCase;

/**
 *
 * @author Jindrich Sedek
 */
public class BuildProviderTest extends DatabaseTestCase {

    public BuildProviderTest(String name) {
        super(name);
    }

    public void testGetBuild() throws Exception {
        put("1000", "201004150201");
        put("1002", "201004110201");
        put("1004", "201004100201");

        Long result = BuildProvider.getInstance().getBuild(1000);
        assertEquals(result, new Long(201004150201l));

        result = BuildProvider.getInstance().getBuild(1004);
        assertEquals(result, new Long(201004100201l));

        assertNull(BuildProvider.getInstance().getBuild(1001));

    }

    private void put(String issue, String build) throws IOException, ServletException {
        IssueBuildMappingServlet servlet = new IssueBuildMappingServlet();
        Map<String, String> params = new HashMap<String, String>();
        TestHttpSevletRequest request = new TestHttpSevletRequest(null, params);
        TestHttpServletResponse response = new TestHttpServletResponse();
        params.put("issue", issue);
        params.put("build", build);
        servlet.processRequest(request, response);
        assertEquals("true", response.getOutput());
    }
}
