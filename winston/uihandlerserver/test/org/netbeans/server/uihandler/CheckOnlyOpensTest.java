/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2009 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */
package org.netbeans.server.uihandler;

import org.netbeans.modules.exceptions.utils.BugReporterFactory;
import java.io.File;
import javax.persistence.EntityManager;
import org.netbeans.modules.exceptions.entity.Report;
import org.netbeans.modules.exceptions.entity.Exceptions;
import org.netbeans.server.uihandler.api.Issue;
/**
 *
 * @author Jindrich Sedek
 */
public class CheckOnlyOpensTest extends DatabaseTestCase {

    private ExceptionsData excData;
    private int reportID;
    private int issueID = 10;

    public CheckOnlyOpensTest(String name) {
        super(name);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        Issue iss10 = BugReporterFactory.getDefaultReporter().getIssue(issueID);
        iss10.setIssueStatus("RESOLVED");
    }

    public void testWholeStacktraceFilter() throws Exception {

        File log = LogsManagerTest.extractResourceAs(data, "dup1", "NB2040811605.0");
        excData = addLog(log, "NB2040811605");
        assertNotNull("MAPA SHOULD BE NOT NULL FOR THIS SESSIONS", excData);
        assertFalse("THIS ISSUE SHOULD HAVE NO DUPLICATES", excData.isDuplicateReport());
        assertEquals("THIS ISSUE SHOULD HAVE ID 1", 1, excData.getSubmitId().intValue());

        //exactly the same
        File log2 = LogsManagerTest.extractResourceAs(data, "dup1", "NB2040811605.1");
        excData = addLog(log2, "NB2040811605");
        assertNotNull("MAPA SHOULD BE NULL FOR NO SESSIONS", excData);
        assertTrue("THIS ISSUE SHOULD HAVE DUPLICATES", excData.isDuplicateReport());
        assertAreDuplicates("THIS ISSUE SHOULD BE DUPLICATE OF 1", 1, excData.getSubmitId());
        reportID = 2;

        verifyIssueChecking();
    }

    public void testLinesCheckingFilter() throws Exception {
        addNext(117816, false, 3);//ID 3
        addNext(117817, true, 3);//ID 4
        verifyIssueChecking();
    }

    public void testStackOverflowFilter() throws Exception {// stacoverflow
        addNext(30, false, 5);//ID 5
        addNext(31, true, 5);//ID 6
        verifyIssueChecking();
    }

    private void addNext(int index, boolean duplicate, int id) throws Exception {
        File log = LogsManagerTest.extractResourceAs(data, "dup" + Integer.toString(index), "NB2040811605." + reportID);
        excData = addLog(log, "NB2040811605");
        assertNotNull("MAPA SHOULD BE NULL FOR NO SESSIONS", excData);
        assertEquals("THIS ISSUE SHOULD (NOT) BE A DUPLICATE", duplicate, excData.isDuplicateReport());
        if (duplicate) {
            assertTrue("THIS EXCEPTIONS SHOULD BE DUPLICATES " + id + " AND " + excData.getSubmitId(), areDuplicates(id, excData.getSubmitId()));
        } else {
            assertEquals("THIS ISSUE SHOULD RETURN ID ", id, excData.getSubmitId().intValue());
        }
        reportID++;
    }

    private void verifyIssueChecking(){
        EntityManager em = perUtils.createEntityManager();
        em.getTransaction().begin();
        Exceptions exc = em.find(Exceptions.class, excData.getSubmitId());
        Throwable thr = exc.getMockThrowable();
        assertNotNull(Utils.checkOpenIssues(em, thr));

        Report r = em.find(Report.class, excData.getReportId());
        r.setIssueId(issueID);
        em.persist(r);
        em.getTransaction().commit();

        assertNotNull(Utils.checkIssue(em, thr));
        assertNull(Utils.checkOpenIssues(em, thr));
        em.close();
    }

}
