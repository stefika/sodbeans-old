/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */
package org.netbeans.server.uihandler;

import java.util.Date;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import javax.servlet.ServletRequest;
import javax.servlet.jsp.PageContext;
import org.netbeans.modules.exceptions.entity.Report;
import org.netbeans.modules.exceptions.utils.BugReporterFactory;
import org.netbeans.modules.exceptions.utils.PersistenceUtils;
import org.netbeans.server.uihandler.api.Issue;
import static org.junit.Assert.*;

/**
 *
 * @author Jindrich Sedek
 */
public class ExceptionsDataTest extends DatabaseTestCase {

    public ExceptionsDataTest(String name) {
        super(name);
    }

    public void testPreparePageContext() {
        Report report = new Report(1);
        PersistenceUtils.getInstance().persist(report);
        ExceptionsData excData = new ExceptionsData();
        PageContext pageContext = LogsManagerTest.createPageContext();
        ServletRequest request = pageContext.getRequest();
        LogRecord thrownLog = new LogRecord(Level.INFO, "THROWN");
        LogRecord userData = new LogRecord(Level.INFO, "USER DATA");
        excData.prepareUploadDoneContext(request);
        assertTrue((Boolean)request.getAttribute("statSubmit"));
        
        excData.setThrownLog(thrownLog);
        excData.setUserDataLog(userData);
        excData.setDataType(ExceptionsData.DataType.EXC_REPORT);
        assertTrue(excData.isExceptionReport());
        excData.setSubmitId(2);
        excData.setUserName("tonda");

        pageContext = LogsManagerTest.createPageContext();
        request = pageContext.getRequest();
        excData.prepareUploadDoneContext(request);
        assertFalse((Boolean)request.getAttribute("statSubmit"));
        assertEquals(excData.getSubmitId(), request.getAttribute("issueId"));
        assertFalse((Boolean)request.getAttribute("isDuplicate"));
        assertEquals("tonda", request.getAttribute("username"));
        
        excData.setReportId(1);
        pageContext = LogsManagerTest.createPageContext();
        request = pageContext.getRequest();
        excData.prepareUploadDoneContext(request);
        assertFalse((Boolean)request.getAttribute("statSubmit"));
        assertEquals(excData.getSubmitId(), request.getAttribute("issueId"));
        assertTrue((Boolean)request.getAttribute("isDuplicate"));
        assertFalse((Boolean)request.getAttribute("inIssuezilla"));
        
        excData.setIssuezillaId(2);
        pageContext = LogsManagerTest.createPageContext();
        request = pageContext.getRequest();
        excData.prepareUploadDoneContext(request);
        assertFalse((Boolean)request.getAttribute("statSubmit"));
        assertEquals(excData.getSubmitId(), request.getAttribute("issueId"));
        assertTrue((Boolean)request.getAttribute("isDuplicate"));
        assertTrue((Boolean)request.getAttribute("inIssuezilla"));
        assertEquals(new Integer(2), (Integer) request.getAttribute("issuezillaId"));
        assertFalse((Boolean)request.getAttribute("isFixed"));
        
        Issue issue = BugReporterFactory.getDefaultReporter().getIssue(2);
        issue.setIssueStatus("RESOLVED");
        issue.setResolution("FIXED");
        issue.setLastResolutionChange(new Date());
        
        pageContext = LogsManagerTest.createPageContext();
        request = pageContext.getRequest();
        excData.prepareUploadDoneContext(request);
        assertFalse((Boolean)request.getAttribute("statSubmit"));
        assertEquals(excData.getSubmitId(), request.getAttribute("issueId"));
        assertTrue((Boolean)request.getAttribute("isDuplicate"));
        assertTrue((Boolean)request.getAttribute("inIssuezilla"));
        assertEquals(new Integer(2), (Integer) request.getAttribute("issuezillaId"));
        assertTrue((Boolean)request.getAttribute("isFixed"));
        
        
        
    }
}