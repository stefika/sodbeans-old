/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */
package org.netbeans.server.uihandler;

import org.netbeans.modules.exceptions.utils.LogReporter;
import java.io.File;
import java.io.InputStream;
import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.servlet.jsp.PageContext;
import org.netbeans.junit.MockServices;
import org.netbeans.modules.exceptions.entity.Logfile;
import org.netbeans.modules.exceptions.entity.Preference;
import org.netbeans.modules.exceptions.entity.Statistic;
import org.netbeans.modules.exceptions.utils.PersistenceUtils;
import org.netbeans.server.uihandler.statistics.TimeToFailture;
import org.openide.util.RequestProcessor;

/**
 *
 * @author Jindrich Sedek
 */
public class VerificationTest extends DatabaseTestCase {

    public VerificationTest(String name) {
        super(name);
    }

    @Override
    protected void setUp() throws Exception {
        System.out.println("running " + getName());
        LOG = Logger.getLogger("test." + getName());

        clearWorkDir();
        File directory = getWorkDir().getAbsoluteFile();
        LogFileTask.closed = false;

        PersistenceUtils.setSettingsDirectory(directory.toString());

        InputStream inputStream = DatabaseTestCase.class.getResourceAsStream("testPrivKey");
        Authenticate.loadPrivateKey(inputStream);
        IZInsertion.setDuplicatesNo(100l);
        initPersitenceUtils(prepareDir("db"));
        MockServices.setServices(LogReporter.class, MockAuthenticator.class);
        perUtils = PersistenceUtils.getInstance();
        Utils.setParsingTimeout(1000);
        data = prepareDir("logs");
    }

    public void testVerification() throws Exception {
        LogsManagerTest.extractResourceAs(data, "dupres/nested10586", "log444");
        LogsManagerTest.extractResourceAs(data, "dupres/nested8523", "log444.1");
        LogsManagerTest.extractResourceAs(data, "dupres/nested10997", "log444.2");
        logsManager = LogsManager.createManager(data);
        page = createPageContext();

        assertEquals(0, logsManager.getParsedLogs());
        logsManager.waitVerificationFinished();
        assertEquals(3, logsManager.getParsedLogs());
        logsManager.preparePageContext(page, "test", Collections.singleton("TimeToFailture"));
        verifyLoaded(page);

        EntityManager em = perUtils.createEntityManager();
        Logfile globalLogFile = LogFileTask.getLogInfo("global", 0, em);
        assertNotNull("Global data are saved in DB", globalLogFile);

        Query query = em.createQuery("SELECT p FROM Preference p WHERE logfile = :log");
        query.setParameter("log", globalLogFile);
        List<Preference> prefs = query.getResultList();
        assertNotNull("Some preferences are saved", prefs);
        assertTrue("Lot's of prefs are saved", prefs.size() > 20);

        query = em.createNamedQuery("Preference.findWithoutKeyNullPrefix");
        query.setParameter("statistic", Statistic.getExists("TimeToFailture"));
        query.setParameter("logfile", globalLogFile);
        prefs = query.getResultList();
        assertEquals(String.valueOf(111438), prefs.get(0).getKey());
        assertEquals(String.valueOf(3048012), prefs.get(2).getKey());

        em.close();
    }

    public void testRecompute() throws Exception {
        LogsManagerTest.extractResourceAs(data, "dupres/nested10586", "log444");
        LogsManagerTest.extractResourceAs(data, "dupres/nested8523", "log444.1");
        LogsManagerTest.extractResourceAs(data, "dupres/nested10997", "log444.2");
        logsManager = LogsManager.createManager(data);
        page = createPageContext();
        logsManager.close();

        logsManager = LogsManager.createManager(data);
        RequestProcessor.getDefault().post(new Runnable(){

            public void run() {
            }

        }).waitFinished();
        assertEquals(3, logsManager.getNumberOfLogs());
        assertEquals(0, logsManager.getParsedLogs());
        logsManager.waitVerificationFinished();
        assertEquals(3, logsManager.getNumberOfLogs());
        assertEquals(3, logsManager.getParsedLogs());
        page = PgCnt.create();
        logsManager.preparePageContext(page, "test", Collections.singleton("TimeToFailture"));
        verifyLoaded(page);
    }

    public void testMultiDir()throws Exception{
        LogsManagerTest.extractResourceAs(data, "empty.log", "log444");
        File subdir = new File(data, "234/234/22/");
        subdir.mkdirs();
        LogsManagerTest.extractResourceAs(subdir, "empty.log", "log444.1");
        subdir = new File(data, "234/222/");
        subdir.mkdirs();
        LogsManagerTest.extractResourceAs(subdir, "empty.log", "log444.2");
        subdir = new File(data, "234/222/");
        subdir.mkdirs();
        LogsManagerTest.extractResourceAs(subdir, "empty.log", "log444.3");
        subdir = new File(data, "2");
        subdir.mkdirs();
        LogsManagerTest.extractResourceAs(subdir, "empty.log", "log444.4");

        logsManager = LogsManager.createManager(data);
        logsManager.waitVerificationFinished();
        page = createPageContext();
        assertEquals(5, logsManager.getParsedLogs());
    }
    
    private void verifyLoaded(PageContext page) {
        List<TimeToFailture.TTFData> loaded = (List<TimeToFailture.TTFData>) page.getAttribute("globalTimeToFailture");
        assertEquals(3, loaded.size());
        assertEquals(1l, loaded.get(0).getTrimmedTime());
        assertEquals(10l, loaded.get(1).getTrimmedTime());
        assertEquals(50l, loaded.get(2).getTrimmedTime());
    }
}
