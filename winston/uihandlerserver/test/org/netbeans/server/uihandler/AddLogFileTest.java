/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */

package org.netbeans.server.uihandler;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import org.netbeans.modules.exceptions.entity.Logfile;
import org.netbeans.modules.exceptions.utils.PersistenceUtils;
import static junit.framework.Assert.*;

/**
 *
 * @author Jindrich Sedek
 */
public class AddLogFileTest extends DatabaseTestCase{
    
    public AddLogFileTest(String str)  {
        super(str);
    }

    public void testAddLogs() throws Exception{
        String IP1 = "127.0.0.1", IP2 = "20.14.15.17", IP4="255.255.255.255";
        File log = LogsManagerTest.extractResourceAs(data, "2.log", "NB2040811605.0");
        waitLog(log, IP1);
        check(1,  "NetBeans IDE Dev (Build 200701301900)", "127000000001", IP1, 1, "NB2040811605", "NB2040811605.0", "070130");
        log = LogsManagerTest.extractResourceAs(data, "3.log", "NB2040811605.2");
        waitLog(log, IP2);
        check(2,  "NetBeans IDE Dev (Build 200702071900)", "20014015017", IP2, 3, "NB2040811605", "NB2040811605.2", "070207");
        log = LogsManagerTest.extractResourceAs(data, "3.log", "NB204081111");
        waitLog(log, IP4);
        check(3,  "NetBeans IDE Dev (Build 200702071900)", "255255255255", IP4, 0, "NB204081111", "NB204081111", "070207");
        log = LogsManagerTest.extractResourceAs(data, "dup1", "NB204081111.5");
        waitLog(log, IP4);
        check(4,  "NetBeans IDE Dev (Build 200703011900)", "255255255255", IP4, 6, "NB204081111", "NB204081111.5", "070301");
    }

    static List<Logfile> logFiles(EntityManager em) {
        List<Logfile> logFiles = PersistenceUtils.getAll(em, Logfile.class);
        List<Logfile> arr = new ArrayList<Logfile>();
        for (Logfile logfile : logFiles) {
            if (logfile.getUserdir().equals("global")) {
                continue;
            }
            arr.add(logfile);
        }
        return arr;
    }
    private void check(int count, String version, String IP, String IPAddr, int UploadNumber, String userDir, String fileName, String buildNumber){
        assertEquals("LOG file is INSERTED", count, LogsManager.getDefault().getNumberOfLogs());
        Logfile logFile  = LogFileTask.getRunningTask().getLogInfo();
        assertNotNull("Version", logFile.getProductVersionId());
        assertEquals("VERSION", version, logFile.getProductVersionId().getProductVersion());
        assertEquals("UPLOAD NUMBER, ", new Integer(UploadNumber), logFile.getUploadNumber());
        assertEquals("USERDIR NUMBER, ", userDir, logFile.getUserdir());
        assertEquals("FileName", fileName, logFile.getFileName());
        assertEquals("BuildNumber", new Long(buildNumber), logFile.getBuildnumber());
        assertEquals("IP", new Long(IP), logFile.getIp());
        assertEquals("IP Addr", IPAddr, logFile.getIpAddr());
    }
    
}
