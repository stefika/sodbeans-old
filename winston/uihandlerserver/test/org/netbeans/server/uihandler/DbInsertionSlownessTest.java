/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2009 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */

package org.netbeans.server.uihandler;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import javax.persistence.EntityManager;
import org.netbeans.modules.exceptions.entity.Logfile;
import org.netbeans.modules.exceptions.entity.Slowness;
import org.netbeans.modules.exceptions.entity.Slowness.SlownessType;
import org.netbeans.modules.exceptions.utils.PersistenceUtils;
import org.netbeans.server.snapshots.SnapshotManagerTest;

/**
 *
 * @author Jindrich Sedek
 */
public class DbInsertionSlownessTest extends DatabaseTestCase{
    EntityManager em;
    
    public DbInsertionSlownessTest(String testName) {
        super(testName);
    }

    @Override
    protected void tearDown() throws Exception {
        em.getTransaction().commit();
        em.close();
        super.tearDown();
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        em = perUtils.createEntityManager();
        em.getTransaction().begin();
    }
    
    public void testInsertSlowness() throws Exception {
        List<Logfile> logFiles = AddLogFileTest.logFiles(em);
        assertEquals("Empty at start", 0, logFiles.size());

        LogRecord rec= new LogRecord(Level.SEVERE, "TESTING LOG RECORD");
        String[] users = {"GUEST", "GUEST"};
        String[] comments = {"COMMENT", "[subversion/code] \n test"};
        String[] productVersion = {"NetBeans IDE Dev (Build 200702121900)", "NetBeans IDE Dev (Build 20070416-1344)"};
        String[] params = new String[7];
        params[0] = "Linux, 2.6.15-1.2054_FC5smp, i386";
        params[1] = "Java HotSpot(TM) Client VM, 1.6.0-b105";
        params[2] = productVersion[0];
        params[3] = users[0];
        params[4] = "UnknownError : Summary Message";
        params[5] = comments[0];
        params[6] = "test";
        String userId = "file";
        String action = "org.openide.actions.NewFile";
        int session = 3;
        Logfile log = new Logfile(userId, session);

        rec.setParameters(params);
        File nps = log.getNPSFile();
        InputStream is = SnapshotManagerTest.class.getResourceAsStream("snapshot.nps");
        createFileFromStream(nps, is);
        is.close();
        
        DbInsertion insertion = new DbInsertion(rec, null, null, new TestLogFileTask(em, log), 1323l, null, action);
        ExceptionsData insertionResult = getInsertionData(insertion);
        assertEquals(ExceptionsData.DataType.SLOWNESS_REPORT, insertionResult.getDataType());
        assertNull(insertionResult.getIssuezillaId());
        assertTrue(insertionResult.isSlownessReport());
        assertFalse(insertionResult.isExceptionReport());
        assertFalse(insertionResult.isDuplicateReport());

        em.getTransaction().commit();
        em.getTransaction().begin();

        insertion = new DbInsertion(rec, null, null, new TestLogFileTask(em, log), 100323l, null, action);
        insertionResult = getInsertionData(insertion);
        assertEquals(ExceptionsData.DataType.SLOWNESS_REPORT, insertionResult.getDataType());
        assertNull(insertionResult.getIssuezillaId());
        assertTrue(insertionResult.isSlownessReport());
        assertFalse(insertionResult.isExceptionReport());
        assertTrue(insertionResult.isDuplicateReport());

        em.getTransaction().commit();
        em.getTransaction().begin();

        insertion = new DbInsertion(rec, null, null, new TestLogFileTask(em, log), 100323l, SlownessType.GoToType, action);
        insertionResult = getInsertionData(insertion);
        assertEquals(ExceptionsData.DataType.SLOWNESS_REPORT, insertionResult.getDataType());
        assertNull(insertionResult.getIssuezillaId());
        assertTrue(insertionResult.isSlownessReport());
        assertFalse(insertionResult.isExceptionReport());
        assertFalse(insertionResult.isDuplicateReport());
    }

    public void testSlownessTypeLog() throws Exception {
        File dataUploadDir = new File(Utils.getUploadDirPath(Utils.getRootUploadDirPath(), "NB2040811605"));
        File log = LogsManagerTest.extractResourceAs(dataUploadDir, "slownres/gototype.xml", "NB2040811605.0");
        File npsUploadDir = new File(Utils.getUploadDirPath(Utils.getSlownessRootUploadDir(), "NB2040811605"));
        File l2 = LogsManagerTest.extractResourceAs(npsUploadDir, "slownres/gototype.nps", "NB2040811605.0");

        waitLog(log, "127.0.0.1");
        List<Slowness> sls = PersistenceUtils.getAll(em, Slowness.class);
        assertEquals(1, sls.size());
        assertEquals(SlownessType.GoToType, sls.get(0).getSlownessTypeValue());
        assertEquals(15386l, sls.get(0).getActionTime());

    }

    private void createFileFromStream(File target, InputStream is) throws IOException{
        OutputStream os = new BufferedOutputStream(new FileOutputStream(target));
        int count;
        byte[] array = new byte[1024];
        while ((count = is.read(array)) != -1){
            os.write(array, 0, count);
        }
        os.close();
    }
    
    public static class TestLogFileTask extends LogFileTask{
        private Logfile logfile;
        private EntityManager testEm;
        public TestLogFileTask(EntityManager em, Logfile logfile) {
            super(new File(logfile.getFileName()), null, null);
            this.testEm = em;
            this.logfile = logfile;
            em.persist(logfile);
            runningTask = this;
        }
        @Override
        public EntityManager getTaskEntityManager() {
            return testEm;
        }

        @Override
        public Logfile getLogInfo() {
            return logfile;
        }
        
        public void markFinished(){
            notifyFinished();
        }
    }

     static ExceptionsData getInsertionData(DbInsertion insertion){
        ExceptionsData excData = new ExceptionsData();
        insertion.start(excData);
        return excData;
     }
     
}

