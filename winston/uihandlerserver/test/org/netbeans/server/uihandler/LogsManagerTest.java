/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */

package org.netbeans.server.uihandler;

import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.logging.Level;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.PageContext;
import org.netbeans.lib.uihandler.InputGesture;
import org.netbeans.modules.exceptions.entity.Logfile;
import org.openide.util.Exceptions;

/**
 *
 * @author Jaroslav Tulach
 */
public class LogsManagerTest extends DatabaseTestCase {
    private LogsManager result;
    
    static {
        System.setProperty("uihandlerserver.timeout", "10000");
    }
    
    
    public LogsManagerTest(String testName) {
        super(testName);
    }
    
    @Override
    protected Level logLevel() {
        return Level.FINE;
    }
    
    private File logs() throws IOException {
        File f = new File(getWorkDir(), "logs");
        f.mkdirs();
        return f;
    }
    
    
    @Override
    protected void setUp() throws Exception {
        super.setUp();
        logsManager.close();
        File toDir = logs();
        File log = extractResourceAs(toDir, "1.log", "log444");
        File log2 = extractResourceAs(toDir, "2.log", "log4442");
        
        result = createManager(logs());
    }

    @Override
    protected void tearDown() throws Exception {
        result.close();
        super.tearDown();
    }
    public void testErrorWhenNoAnnotationGiven() throws Exception {
        PageContext context = createPageContext();
        try {
            result.preparePageContext(context, Collections.singleton("UnknownStatistic"));
            fail("Should report unknown statistic");
        } catch (IllegalArgumentException ex) {
            if (ex.getMessage().indexOf("UnknownStatistic") == -1) {
                fail("Report message shall contain info about UnknownStatistic:\n" + ex.getMessage());
            }
        }
    }
    
  
    public void testGetParsedLogs() throws Exception {
        result.preparePageContext(createPageContext(), "unknown", null);
        assertEquals("All parsed", result.getNumberOfLogs(), result.getParsedLogs());
        assertEquals("Two parsed", 2, result.getParsedLogs());

        File log3 = extractResourceAs(logs(), "2.log", "log444.1");
        
        result.addLog(log3, "127.0.0.1");
        result.preparePageContext(createPageContext(), "unknown", null);

        assertEquals("All 3 parsed", result.getNumberOfLogs(), result.getParsedLogs());
        assertEquals("Three parsed", 3, result.getParsedLogs());
    }
    
    public void testGetDefault() throws Exception {
        doCountsTest(0);
    }
    public void testUserInc() throws Exception {
        doCountsTest(1);
    }
    public void testUserSessionChange() throws Exception {
        doCountsTest(2);
    }
    
    private void doCountsTest(int type) throws Exception {
        assertNotNull(result);
        
        
        Map<InputGesture,Integer> cnts = null;
        switch (type) {
        case 0: cnts = getGlobalCounts(result); break;
        case 1: cnts = getUserCounts(result, "log444"); break;
        case 2: cnts = Collections.emptyMap();
        }
        
        assertNotNull(cnts);
        
        for (Map.Entry<InputGesture,Integer> e : cnts.entrySet()) {
            if (e.getValue() <= 0) {
                fail("Too less of " + e.getKey() + " = " + e.getValue());
            }
        }
        int numb = result.getNumberOfLogs();
        
        File log = extractResourceAs(logs(), "1.log", "log444.1");
        result.addLog(log, "10.0.0.23");
        
        Map<InputGesture,Integer> newCnts = null;
        switch (type) {
        case 0: newCnts = getGlobalCounts(result); break;
        case 1: newCnts = getUserCounts(result, "log444"); break;
        case 2: newCnts = getLastSessionCounts(result, "log444"); break;
        }
        assertEquals("One log added", numb + 1, result.getNumberOfLogs());
        
        for (Map.Entry<InputGesture,Integer> e : cnts.entrySet()) {
            Integer newValue = newCnts.get(e.getKey());
            if (e.getValue() >= newValue) {
                fail("Too small increment of of " + e.getKey() + " = " + e.getValue() + " and " + newValue);
            }
        }
        
        
        Logfile info = LogFileTask.getRunningTask().getLogInfo();
        assertNotNull("There is a log info", info);
        assertEquals("address is ok", info.getIpAddr(), "10.0.0.23");
    }
    
    static File extractResource(File toDir, String res) throws IOException {
        return extractResourceAs(toDir, res, res);
    }

    public static File extractResourceAs(File toDir, String res, String outFile) throws IOException {
        return extractResourceAs(LogsManagerTest.class, toDir, res, outFile);
    }
    public static File extractResourceAs(Class<?> caller, File toDir, String res, String outFile) throws IOException {
        if (!toDir.exists()){
            toDir.mkdirs();
        }
        InputStream is = caller.getResourceAsStream(res);
        File out = new File(toDir, outFile);
        OutputStream os = new BufferedOutputStream(new FileOutputStream(out));
        byte[] arr = new byte[4096];
        for (;;) {
            int len = is.read(arr);
            if (-1 == len) break;
            os.write(arr, 0, len);
        }
        os.close();
        is.close();
        return out;
    }

    public static File extractString(File toDir, String rec, String outFile) throws IOException {
        InputStream is = new ByteArrayInputStream(rec.getBytes());
        File out = new File(toDir, outFile);
        OutputStream os = new BufferedOutputStream(new FileOutputStream(out));
        byte[] arr = new byte[4096];
        for (;;) {
            int len = is.read(arr);
            if (-1 == len) break;
            os.write(arr, 0, len);
        }
        os.close();
        is.close();
        return out;
    }

    public void testGetIP(){
        Map<String, String> header = new HashMap<String, String>();
        TestHttpSevletRequest request = new TestHttpSevletRequest(header);
        UploadLogs uplLogs = new UploadLogs();
        assertEquals("remoteHost", LOCALHOST_IP, uplLogs.getIP(request));
        String ip = "12.43.32.12";
        header.put("x-forwarded-for", ip);
        assertEquals("x-forwarded", ip, uplLogs.getIP(request));

        ip = "unknown, 200.255.9.38";
        header.put("x-forwarded-for", ip);
        assertEquals("200.255.9.38", uplLogs.getIP(request));
        
    }
    
    public void testRequestParametersCopiedToPageContext() throws Exception {
        
        final Map<String,Object> attr = new HashMap<String,Object>();
        final Map<String,String> param = new HashMap<String,String>();
        HttpServletRequest request = new TestHttpSevletRequest(attr, param);
        
        HttpSession session = new HttpSessionImpl(attr);
        PageContext pg = DatabaseTestCase.createPageContext(session, request);
        
        param.put("ActionsIncludes", ".*All.*");
        param.put("empty", "");
        
        result.preparePageContext(pg, null);
        
        Object value = pg.getAttribute("ActionsIncludes", PageContext.REQUEST_SCOPE);
        
        assertEquals(".*All.*", value);
        assertNull(pg.getAttribute("empty", PageContext.REQUEST_SCOPE));
    }
    
    
    public static LogsManager createManager(File f) {
        LogsManager result = LogsManager.createManager(f);
        try {
            result.waitVerificationFinished();
        } catch (InterruptedException ex) {
            Exceptions.printStackTrace(ex);
        }
        return result;
    }
    
    @SuppressWarnings("unchecked")
    private static Map<InputGesture,Integer> getGlobalCounts(LogsManager result) throws InterruptedException, ExecutionException {
        PageContext page = createPageContext();
        result.waitParsingFinished();
        result.preparePageContext(page, null);
        return (Map<InputGesture, Integer>) page.getAttribute("globalGestures");
    }
    @SuppressWarnings("unchecked")
    private static Map<InputGesture,Integer> getUserCounts(LogsManager result, String id) throws InterruptedException, ExecutionException {
        PageContext page = createPageContext();
        result.preparePageContext(page, id, null);
        return (Map<InputGesture, Integer>) page.getAttribute("userGestures");
    }
    @SuppressWarnings("unchecked")
    private static Map<InputGesture,Integer> getLastSessionCounts(LogsManager result, String id) throws InterruptedException, ExecutionException {
        PageContext page = createPageContext();
        result.preparePageContext(page, id, null);
        return (Map<InputGesture, Integer>) page.getAttribute("lastGestures");
    }

}
