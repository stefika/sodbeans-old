/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */

package org.netbeans.server.uihandler;

import java.io.File;
import javax.persistence.EntityManager;

/**
 *
 * @author Jindrich Sedek
 */
public class Duplicates2Test extends DatabaseTestCase{
    private ExceptionsData excData;
    private static Integer reportID = 1;

    public Duplicates2Test(String name) {
        super(name);
    }

    public void testIssue136675() throws Exception{
        addNext(1, false, 0); // id 1
        addNext(2, false, 0); // id 2
        EntityManager em = perUtils.createEntityManager();
        em.getTransaction().begin();
        org.netbeans.modules.exceptions.entity.Exceptions exc = em.find(org.netbeans.modules.exceptions.entity.Exceptions.class, 2);
        exc.setReportId(em.find(org.netbeans.modules.exceptions.entity.Exceptions.class, 1).getReportId());
        em.merge(exc);
        em.getTransaction().commit();
        em.close();
        addNext(2, true, 1); // id 3 -> no duplicate because of root of 2 is 1
        addNext(3, true, 3); // id4 -> duplicate of 3
    }

    public void testIssue140958()throws Exception{
        addNext(5, false, 0); // id 5
        addNext(6, true, 5); // id 6
        addNext(7, true, 5); // id 7
    }

    public void testIssue143406()throws Exception{
        addNext(8, false, 0); // id 8
        addNext(9, false, 0); // id 9
        addNext(10, false, 0); // id 10
    }

    public void testEmptyStacktrace() throws Exception {
        addNext(15, false, 0); // id 11
        addNext(16, true, 11); // id 12
    }

    public void testIssue144297_ignoreJavaPKG() throws Exception{
        addNext(20, false, 0); // id 13
        addNext(21, false, 0); // id 14
    }

    public void testIssue180135() throws Exception{
        addNext(180135, false, 15);//ID 15
        addNext(180136, true, 15);//ID 16
    }

    private void addNext(int index, boolean duplicate, int id) throws Exception{
        File log = LogsManagerTest.extractResourceAs(data, "dupres/dup"+Integer.toString(index), "NB2040811605."+reportID);
        excData = addLog(log, "NB2040811605");
        assertNotNull("MAPA SHOULD BE NULL FOR NO SESSIONS", excData);
        assertEquals("THIS ISSUE SHOULD (NOT) BE A DUPLICATE",  duplicate, excData.isDuplicateReport());
        assertEquals("ISSUE SHOULD INCREASE", reportID, excData.getSubmitId());
        if (duplicate){
            assertAreDuplicates("These issues should be duplicates ",  id, excData.getSubmitId().intValue());
        }else{
            assertFalse(excData.isDuplicateReport());
        }
        reportID++;
    }
    
}
