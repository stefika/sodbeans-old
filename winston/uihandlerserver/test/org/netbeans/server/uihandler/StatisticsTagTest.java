/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */

package org.netbeans.server.uihandler;

import java.util.logging.LogRecord;
import java.util.prefs.Preferences;
import javax.servlet.jsp.JspException;
import org.netbeans.junit.MockServices;
import org.netbeans.junit.NbTestCase;

/**
 *
 * @author Jaroslav Tulach <jtulach@netbeans.org>
 */
public class StatisticsTagTest extends NbTestCase {

    public StatisticsTagTest(String s) {
        super(s);
    }

    @Override
    public void setUp() throws Exception {
        clearWorkDir();
        LogsManager.createManager(null);
        DatabaseTestCase.initPersitenceUtils(getWorkDir());
    }

    public void testStatistic() throws JspException {
        MockServices.setServices(Cnts.class);

        String statistic = "Cnts";
        StatisticsTag instance = new StatisticsTag();
        instance.setName(statistic);
        instance.setJspContext(DatabaseTestCase.createPageContext());
        instance.doTag();
    }

    public static final class Cnts extends Statistics<Integer> {
        public Cnts() {
            super("Cnts");
        }
        
        protected Integer newData() {
            return 0;
        }
        
        protected Integer process(LogRecord rec) {
            return 1;
        }
        
        protected Integer join(Integer one, Integer two) {
            return one + two;
        }
        
        protected void write(Preferences pref, Integer d) {
            pref.putInt("value", d);
        }

        protected Integer read(Preferences pref) {
            return pref.getInt("value", 0);
        }

        @Override
        protected Integer finishSessionUpload(String userId, int sessionNumber, boolean initialParse, Integer d) {
            return d;
        }
    } // end of CntngStat

}