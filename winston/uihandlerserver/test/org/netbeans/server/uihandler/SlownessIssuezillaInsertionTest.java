/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2009 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */
package org.netbeans.server.uihandler;

import org.netbeans.modules.exceptions.utils.BugReporterFactory;
import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import javax.naming.NamingException;
import javax.persistence.EntityManager;
import org.netbeans.server.uihandler.api.bugs.BugReporter;
import org.netbeans.modules.exceptions.entity.Directuser;
import org.netbeans.modules.exceptions.entity.Logfile;
import org.netbeans.modules.exceptions.entity.Method;
import org.netbeans.modules.exceptions.entity.Nbuser;
import org.netbeans.modules.exceptions.entity.Nbversion;
import org.netbeans.modules.exceptions.entity.ProductVersion;
import org.netbeans.modules.exceptions.entity.Report;
import org.netbeans.modules.exceptions.entity.Slowness;
import org.netbeans.modules.exceptions.utils.BugzillaReporter;
import org.netbeans.modules.exceptions.utils.PersistenceUtils;
import org.netbeans.server.uihandler.DbInsertionTest.TestLogFileTask;
import org.netbeans.server.uihandler.api.Issue;

/**
 *
 * @author Jindrich Sedek
 */
public class SlownessIssuezillaInsertionTest extends DatabaseTestCase {

    private Integer rootId;
    private boolean isReopen = false;
    
    public SlownessIssuezillaInsertionTest(String str) throws NamingException {
        super(str);
    }

    public void testIZInsertion() throws Exception{
        IZInsertion.setOldReportsDate(new Date("10/01/09"));

        TestHandler handler = new TestHandler();
        BugReporter.LOG.addHandler(handler);
        IZInsertion.setDuplicatesNo(new Long(2));
        File dataUploadDir = new File(Utils.getUploadDirPath(Utils.getRootUploadDirPath(), "NB2040811605"));
        File log = LogsManagerTest.extractResourceAs(dataUploadDir, "slownres/logger10.xml", "NB2040811605.1");
        File npsUploadDir = new File(Utils.getUploadDirPath(Utils.getSlownessRootUploadDir(), "NB2040811605"));
        File l2 = LogsManagerTest.extractResourceAs(npsUploadDir, "slownres/npslog10.nps", "NB2040811605.1");

        waitLog(log, "127.0.0.1");
        assertNull("NO DUPLICATE", handler.getResult());
        waitLog(log, "127.0.0.1");
        assertNull("ONE DUPLICATE", handler.getResult());
        waitLog(log, "127.0.0.1");
        LogRecord result = handler.getResult();
        assertNotNull("duplicates no filter", result);
    }

    public void testIssuezillaInsertion() throws Exception {
        TestHandler handler = new TestHandler();
        BugReporter.LOG.addHandler(handler);
        IZInsertion.setDuplicatesNo(new Long(2));
        File dataUploadDir = new File(Utils.getUploadDirPath(Utils.getRootUploadDirPath(), "NB2040811605"));
        File log = LogsManagerTest.extractResourceAs(dataUploadDir, "slownres/slowndup3", "NB2040811605.1");
        File npsUploadDir = new File(Utils.getUploadDirPath(Utils.getSlownessRootUploadDir(), "NB2040811605"));
        File l2 = LogsManagerTest.extractResourceAs(npsUploadDir, "slownres/slowndup3.nps", "NB2040811605.1");

        waitLog(log, "127.0.0.1");
        assertNull("NO DUPLICATE", handler.getResult());
        waitLog(log, "127.0.0.1");
        assertNull("ONE DUPLICATE", handler.getResult());
        log = LogsManagerTest.extractResourceAs(data, "slownres/slowndup3", "NB2040811605.1");
        waitLog(log, "127.0.0.1");
        LogRecord result = handler.getResult();
        assertNotNull(result);
        Object[] params = result.getParameters();
        assertEquals("NullPointerException", params[0]);
        assertEquals("exceptions", params[1]);
        assertEquals("petrzajac", params[2]);
        handler.flush();
        log = LogsManagerTest.extractResourceAs(data, "dup3", "NB2040811605.1");
        waitLog(log, "127.0.0.1");
        assertNull("MANY DUPLICATES FROM ONE USER", handler.getResult());
    }

    public void testIgnoreOldReports() throws Exception {
        Calendar c = Calendar.getInstance();
        c.add(Calendar.YEAR, 1);
        IZInsertion.setOldReportsDate(c.getTime());
        TestHandler handler = new TestHandler();
        BugReporter.LOG.addHandler(handler);
        IZInsertion.setDuplicatesNo(new Long(2));
        File dataUploadDir = new File(Utils.getUploadDirPath(Utils.getRootUploadDirPath(), "NB2040811605"));
        File log = LogsManagerTest.extractResourceAs(dataUploadDir, "slownres/slowndup3", "NB2040811605.1");
        File npsUploadDir = new File(Utils.getUploadDirPath(Utils.getSlownessRootUploadDir(), "NB2040811605"));
        File l2 = LogsManagerTest.extractResourceAs(npsUploadDir, "slownres/slowndup3.nps", "NB2040811605.1");

        waitLog(log, "127.0.0.1");
        assertNull("NO DUPLICATE", handler.getResult());
        waitLog(log, "127.0.0.1");
        assertNull("ONE DUPLICATE", handler.getResult());
        log = LogsManagerTest.extractResourceAs(data, "slownres/slowndup3", "NB2040811605.1");
        waitLog(log, "127.0.0.1");
        LogRecord result = handler.getResult();
        assertNull("Old Report", result);
    }

    public void testQEUserFilter() throws Exception {
        TestHandler handler = new TestHandler();
        BugReporter.LOG.addHandler(handler);
        IZInsertion.setDuplicatesNo(new Long(3));

        String directUserName = "exceptions";
        LogRecord rec = new LogRecord(Level.SEVERE, "TESTING LOG RECORD");
        ArrayList<String> params = new ArrayList<String>(7);
        params.add("Linux, 2.6.15-1.2054_FC5smp, i386");
        params.add("Java HotSpot(TM) Client VM, 1.6.0-b105");
        params.add("NetBeans IDE Dev (Build 200702121900)");
        params.add(directUserName);
        String summary = "UnknownError : Summary Message";
        params.add(summary);
        params.add("COMMENT");
        params.add("petrzajac");
        rec.setParameters(params.toArray());
        EntityManager em = perUtils.createEntityManager();
        em.getTransaction().begin();
        String userId = "file";
        int session = 1;
        Logfile log = new Logfile(userId, session);
        DbInsertion insertion = new DbInsertion(rec, null, null, new TestLogFileTask(em, log), 100L, null, "method");
        insertion.start(new ExceptionsData());

        LogRecord result = handler.getResult();
        assertNull("NOT A DIRECT USER", result);

        em.persist(new Directuser(directUserName));
        em.getTransaction().commit();
        em.getTransaction().begin();

        insertion = new DbInsertion(rec, null, null, new TestLogFileTask(em, log), 100L, null, "method");
        insertion.start(new ExceptionsData());

        waitIssuezillaInsertionFinished();
        result = handler.getResult();
        assertNull("DIRECT USER IGNORED FOR SLOWNESS", result);
    }

    public void testGetDuplicates() throws Exception {
        LogRecord rec = new LogRecord(Level.SEVERE, "TESTING LOG RECORD");
        String userName = "GUEST";
        ArrayList<String> params = new ArrayList<String>(7);
        params.add("Linux, 2.6.15-1.2054_FC5smp, i386");
        params.add("Java HotSpot(TM) Client VM, 1.6.0-b105");
        params.add("NetBeans IDE Dev (Build 200702121900)");
        params.add(userName);
        params.add("UnknownError : Summary Message");
        params.add("COMMENT");
        params.add("test");
        rec.setParameters(params.toArray());
        EntityManager em = perUtils.createEntityManager();
        em.getTransaction().begin();
        String userId = "file";
        int session = 1;
        Logfile log = new Logfile(userId, session);
        DbInsertion insertion = new DbInsertion(rec, null, null, new TestLogFileTask(em, log), 100L, null, "method");
        insertion.start(new ExceptionsData());
        //waitLogsParsed();
        List<Slowness> all =
                PersistenceUtils.getAll(em, Slowness.class);
        Slowness rootSlowness = all.get(all.size() - 1);
        rootId = rootSlowness.getId();
        List<Nbuser> users = PersistenceUtils.getAll(em, Nbuser.class);
        Nbuser user = null;
        for (Nbuser us : users) {
            if (userName.equals(us.getName())) {
                user = us;
            }
        }
        all = PersistenceUtils.getAll(em, Slowness.class);
        rootSlowness = all.get(all.size() - 1);
        assertNotNull(rootSlowness);
        assertEquals(1, rootSlowness.getReportId().getDuplicates(em));
        assertEquals(1, rootSlowness.getReportId().getDuplicatesFromDistinctUsers(em));
        assertEquals(1, rootSlowness.getReportId().getDuplicatesFromOneUser(em, user));

        session = 2;
        log = new Logfile(userId, session);

        // -- refresh persistence context ---//
        em.getTransaction().commit();
        em.close();
        em = perUtils.createEntityManager();
        em.getTransaction().begin();
        all = PersistenceUtils.getAll(em, Slowness.class);
        rootSlowness = all.get(all.size() - 1);

        rootSlowness = insert(em, new DbInsertion(rec, null, null, new TestLogFileTask(em, log), 100L, null, "method"));
        assertEquals(2, rootSlowness.getReportId().getDuplicates(em));
        assertEquals(1, rootSlowness.getReportId().getDuplicatesFromDistinctUsers(em));
        assertEquals(2, rootSlowness.getReportId().getDuplicatesFromOneUser(em, user));


        session = 3;
        log = new Logfile(userId, session);

        // -- refresh persistence context ---//
        em.getTransaction().commit();
        em.close();
        em = perUtils.createEntityManager();
        em.getTransaction().begin();
        all = PersistenceUtils.getAll(em, Slowness.class);
        rootSlowness = all.get(all.size() - 1);

        rootSlowness = insert(em, new DbInsertion(rec, null, null, new TestLogFileTask(em, log), 100L, null, "method"));
        assertEquals(3, rootSlowness.getReportId().getDuplicates(em));
        assertEquals(1, rootSlowness.getReportId().getDuplicatesFromDistinctUsers(em));
        assertEquals(3, rootSlowness.getReportId().getDuplicatesFromOneUser(em, user));
        userName = "exceptions";
        params.set(3, userName);
        params.set(6, "petrzajac");
        rec.setParameters(params.toArray());

        session = 4;
        log = new Logfile(userId, session);

        // -- refresh persistence context ---//
        em.getTransaction().commit();
        em.close();
        em = perUtils.createEntityManager();
        em.getTransaction().begin();
        all = PersistenceUtils.getAll(em, Slowness.class);
        rootSlowness = all.get(all.size() - 1);

        rootSlowness = insert(em, new DbInsertion(rec, null, null, new TestLogFileTask(em, log), 100L, null, "method"));
        users = PersistenceUtils.getAll(em, Nbuser.class);
        Iterator<Nbuser> userIt = users.iterator();
        Nbuser user2 = null;
        while (userIt.hasNext()) {
            user2 = userIt.next();
            if (user2.getName().equals(userName)) {
                break;
            }
        }
        assertEquals(4, rootSlowness.getReportId().getDuplicates(em));
        assertEquals(2, rootSlowness.getReportId().getDuplicatesFromDistinctUsers(em));
        assertEquals(3, rootSlowness.getReportId().getDuplicatesFromOneUser(em, user));
        assertEquals(1, rootSlowness.getReportId().getDuplicatesFromOneUser(em, user2));

        session = 5;
        log = new Logfile(userId, session);
        rootSlowness = insert(em, new DbInsertion(rec, null, null, new TestLogFileTask(em, log), 100L, null, "method"));

        // -- refresh persistence context ---//
        em.getTransaction().commit();
        em.close();
        em = perUtils.createEntityManager();
        em.getTransaction().begin();
        all = PersistenceUtils.getAll(em, Slowness.class);
        rootSlowness = all.get(all.size() - 1);

        assertEquals(5, rootSlowness.getReportId().getDuplicates(em));
        assertEquals(2, rootSlowness.getReportId().getDuplicatesFromDistinctUsers(em));
        assertEquals(3, rootSlowness.getReportId().getDuplicatesFromOneUser(em, user));
        assertEquals(2, rootSlowness.getReportId().getDuplicatesFromOneUser(em, user2));
        em.getTransaction().commit();
        em.close();
    }

    public void testReopen() throws Exception {
        TestHandler handler = new TestHandler();
        BugReporter.LOG.addHandler(handler);
        IZInsertion.setDuplicatesNo(new Long(10));
        Integer issuezillaId = 100;

        EntityManager em = perUtils.createEntityManager();
        em.getTransaction().begin();

        int session = 0;
        Slowness slowness = insert(em, session, null, null);
        slowness.getReportId().setIssueId(issuezillaId);
        em.merge(slowness);

        BugzillaReporter reporter = BugReporterFactory.getDefaultReporter();
        Issue issue = reporter.getIssue(issuezillaId);
        assertNotNull(issue);
        issue.setResolution("FIXED");
        issue.setIssueStatus("RESOLVED");
        issue.setLastResolutionChange(new Date());
        assertNull(handler.getResult());
        insert(em, ++session, null, null);
        assertNull("todays build", handler.getResult());

        insert(em, ++session, "unknown user", null);
        assertNull("guest", handler.getResult());

        insert(em, ++session, null, "");
        assertNull("empty comment", handler.getResult());

        issue.setLastResolutionChange(new Date("09/24/08"));
        issue.setTargetMilestone("7.0M1");
        insert(em, ++session, null, null);
        assertNull("old version", handler.getResult());

        issue.setTargetMilestone("6.0M9");
        isReopen = true;
        insert(em, ++session, null, null);
        isReopen = false;
        LogRecord result = handler.getResult();
        assertNotNull("newer build -> reopen", result);
        Object[] params = result.getParameters();
        assertEquals("exceptions", params[1]);
        assertTrue(params[3].toString(), params[3].toString().contains("originally marked as duplicate of bug 100"));

        em.getTransaction().commit();
        em.close();
    }

    public void testAddToCC() throws Exception {
        TestHandler handler = new TestHandler("BUG REPORTER TO CC");
        BugReporter.LOG.addHandler(handler);
        IZInsertion.setDuplicatesNo(new Long(3));
        Integer issuezillaId = 1000;

        EntityManager em = perUtils.createEntityManager();
        em.getTransaction().begin();

        int session = 0;
        Slowness root = insert(em, session);
        root.getReportId().setIssueId(issuezillaId);
        em.merge(root);
        assertNull("ADD TO CC", handler.getResult());

        BugzillaReporter reporter = BugReporterFactory.getDefaultReporter();
        Issue issue = reporter.getIssue(issuezillaId);
        assertNotNull(issue);
        issue.setLastResolutionChange(new Date());
        assertNull(handler.getResult());
        insert(em, ++session);
        LogRecord result = handler.getResult();
        assertNotNull("ADD TO CC", result);

        em.getTransaction().commit();
        em.close();
    }

    public void testDontInsertUserDirComponents() throws InterruptedException {
        TestHandler handler = new TestHandler();
        BugReporter.LOG.addHandler(handler);
        EntityManager em = perUtils.createEntityManager();
        em.getTransaction().begin();
        Logfile log = new Logfile("testDir", 1);
        TestLogFileTask task = new TestLogFileTask(em, log);
        ProductVersion pv = new ProductVersion(1);
        pv.setProductVersion("NetBeans IDE 6.0 (Build 081204)");
        Nbversion nbv = new Nbversion(1);
        nbv.setVersion("6.0");
        pv.setNbversionId(nbv);
        log.setProductVersionId(pv);
        Nbuser user = new Nbuser(1);
        user.setName("HALLO");
        Slowness slown = new Slowness(100);
        slown.setActionTime(1000L);
        Method m = new Method(30);
        m.setName("unknown");
        Report report = new Report(100);
        slown.setSuspiciousMethod(m);
        slown.setReportId(report);
        slown.setLogfileId(log);
        report.setComponent("user.dir");
        report.setSubcomponent("some.jar");
        slown.setNbuserId(user);
        em.persist(nbv);
        em.persist(pv);
        em.persist(report);
        em.persist(user);
        em.persist(m);
        em.persist(slown);

        new IZInsertion(slown, null).start();
        task.markFinished();
        IZInsertion.waitIZInsertionFinished();
        LogRecord result = handler.getResult();
        assertNull(result);

        em.getTransaction().commit();
        em.close();
    }

    private Slowness insert(EntityManager em, DbInsertion dbInsertion) throws Exception {
        ExceptionsData eData = new ExceptionsData();
        dbInsertion.start(eData);
        assertTrue((Boolean) eData.isDuplicateReport());
        assertAreDuplicates(rootId, eData.getSubmitId());
        return em.find(Slowness.class, rootId);
    }

    private class TestHandler extends Handler {

        Queue<LogRecord> result = new LinkedList<LogRecord>();
        final String message;

        private TestHandler() {
            this("BUG REPORTER");
        }

        private TestHandler(String mes) {
            this.message = mes;
        }

        public void publish(LogRecord record) {
            if (message.equals(record.getMessage())) {
                result.add(record);
            }
        }

        public void flush() {
            result.clear();
        }

        public void close() throws SecurityException {
            //
        }

        public LogRecord getResult() {
            return result.poll();
        }
    }

    private Slowness insert(EntityManager em, int session) throws Exception {
        return insert(em, session, null, null);
    }

    private Slowness insert(EntityManager em, int session, String userName, String comment) throws Exception {
        LogRecord rec = new LogRecord(Level.SEVERE, "TESTING LOG RECORD");
        if (userName == null){
            userName = "exceptions";
        }
        if (comment == null){
            comment = "COMMENT";
        }
        ArrayList<String> params = new ArrayList<String>(7);
        params.add("Linux, 2.6.15-1.2054_FC5smp, i386");
        params.add("Java HotSpot(TM) Client VM, 1.6.0-b105");
        params.add("NetBeans IDE Dev (Build 200811100001)");
        params.add(userName);
        params.add("UnknownError : Summary Message");
        params.add(comment);
        params.add("petrzajac");
        rec.setParameters(params.toArray());
        String userId = "file";

        ExceptionsData eData = new ExceptionsData();
        Logfile log = new Logfile(userId, session);
        TestLogFileTask task = new TestLogFileTask(em, log);
        DbInsertion dbInsertion = new DbInsertion(rec, null, null, task, 10000L, null, "somethingFailed()");
        dbInsertion.start(eData);
        task.markFinished();
        waitIssuezillaInsertionFinished();
        if ((Boolean) eData.isDuplicateReport()) {
            if (!isReopen){
                assertAreDuplicates(rootId, eData.getSubmitId());
            }
        } else {
            rootId = eData.getSubmitId();
        }
        return em.find(Slowness.class, rootId);
    }

    @Override
    protected void waitLog(File log, String ip) throws Exception {
        super.waitLog(log, ip);
        waitIssuezillaInsertionFinished();
    }
    
}