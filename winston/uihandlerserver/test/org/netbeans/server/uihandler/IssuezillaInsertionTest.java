/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */
package org.netbeans.server.uihandler;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import javax.persistence.EntityManager;
import org.netbeans.server.uihandler.api.bugs.BugReporter;
import org.netbeans.modules.exceptions.entity.Directuser;
import org.netbeans.modules.exceptions.entity.Line;
import org.netbeans.modules.exceptions.entity.Logfile;
import org.netbeans.modules.exceptions.entity.Nbuser;
import org.netbeans.modules.exceptions.entity.Nbversion;
import org.netbeans.modules.exceptions.entity.ProductVersion;
import org.netbeans.modules.exceptions.entity.Report;
import org.netbeans.modules.exceptions.entity.Stacktrace;
import org.netbeans.modules.exceptions.utils.PersistenceUtils;
import org.netbeans.modules.exceptions.utils.BugReporterFactory;
import org.netbeans.modules.exceptions.utils.BugzillaReporter;
import org.netbeans.server.uihandler.DbInsertionTest.TestLogFileTask;
import org.netbeans.server.uihandler.api.Issue;

/**
 *
 * @author Jindrich Sedek
 */
public class IssuezillaInsertionTest extends DatabaseTestCase {

    private Integer rootId;
    private boolean isReopen = false;
    
    public IssuezillaInsertionTest(String str) {
        super(str);
    }

    public void testCNFEDirectUser() throws Exception {
        EntityManager em = perUtils.createEntityManager();
        em.getTransaction().begin();
        em.persist(new Directuser("exceptions"));
        em.getTransaction().commit();
        em.close();
        TestHandler handler = new TestHandler();
        BugReporter.LOG.addHandler(handler);
        IZInsertion.setDuplicatesNo(new Long(2));
        File log = LogsManagerTest.extractResourceAs(data, "dupres/CNFE.log", "NB2040811605.0");
        waitLog(log, "127.0.0.1");
        assertNotNull("directUser", handler.getResult());
    }

    public void testIssuezillaInsertion() throws Exception {
        TestHandler handler = new TestHandler();
        BugReporter.LOG.addHandler(handler);
        IZInsertion.setDuplicatesNo(new Long(2));
        File log = LogsManagerTest.extractResourceAs(data, "dup3", "NB2040811605.1");
        waitLog(log, "127.0.0.1");
        assertNull("NO DUPLICATE", handler.getResult());
        waitLog(log, "127.0.0.1");
        assertNull("ONE DUPLICATE", handler.getResult());
        log = LogsManagerTest.extractResourceAs(data, "dup3", "NB2040811605.1");
        waitLog(log, "127.0.0.1");
        LogRecord result = handler.getResult();
        assertNotNull(result);
        Object[] params = result.getParameters();
        assertEquals("NullPointerException", params[0]);
        assertEquals("exceptions", params[1]);
        assertEquals("petrzajac", params[2]);
        handler.flush();
        log = LogsManagerTest.extractResourceAs(data, "dup3", "NB2040811605.1");
        waitLog(log, "127.0.0.1");
        assertNull("MANY DUPLICATES FROM ONE USER", handler.getResult());
    }

    public void testNPE() throws Exception {
        //since issue 114597, 114598 OUT OF MEMORY WAS CHANGED TO NPE
        TestHandler handler = new TestHandler();
        BugReporter.LOG.addHandler(handler);
        IZInsertion.setDuplicatesNo(new Long(1));
        File log = LogsManagerTest.extractResourceAs(data, "outOfMem", "NB2040811605.10");
        waitLog(log, "112.15.14.13");
        LogRecord result = handler.getResult();
        assertNotNull(result);
        Object[] params = result.getParameters();
        assertEquals("OutOfMemoryError", params[0]);
        assertEquals("exceptions", params[1]);
        assertEquals("petrzajac", params[2]);
    }

    public void testAE() throws Exception {
        TestHandler handler = new TestHandler();
        BugReporter.LOG.addHandler(handler);
        IZInsertion.setDuplicatesNo(new Long(1));
        File log = LogsManagerTest.extractResourceAs(data, "dupres/AE.log", "NB2040811605.10");
        waitLog(log, "112.15.14.13");
        LogRecord result = handler.getResult();
        assertNotNull(result);
        Object[] params = result.getParameters();
        assertEquals("AssertionError", params[0]);
        assertEquals("exceptions", params[1]);
        assertEquals("petrzajac", params[2]);
    }

    public void testAEFromOldVersion() throws Exception { // OldVersionsFilter
        TestHandler handler = new TestHandler();
        BugReporter.LOG.addHandler(handler);
        IZInsertion.setDuplicatesNo(new Long(1));
        File log = LogsManagerTest.extractResourceAs(data, "dupres/AEOld.log", "NB2040811605.11");
        waitLog(log, "112.15.14.13");
        LogRecord result = handler.getResult();
        assertNull(result);
    }

    public void testQEUserFilter() throws Exception {
        TestHandler handler = new TestHandler();
        String commentUser = Utils.getReporterUsername();
        BugReporter.LOG.addHandler(handler);
        IZInsertion.setDuplicatesNo(new Long(3));

        String directUserName = "exceptions";
        LogRecord rec = new LogRecord(Level.SEVERE, "TESTING LOG RECORD");
        ArrayList<String> params = new ArrayList<String>(7);
        params.add("Linux, 2.6.15-1.2054_FC5smp, i386");
        params.add("Java HotSpot(TM) Client VM, 1.6.0-b105");
        params.add("NetBeans IDE Dev (Build 200702121900)");
        params.add(directUserName);
        String summary = "UnknownError : Summary Message";
        params.add(summary);
        params.add("COMMENT");
        params.add("petrzajac");
        rec.setParameters(params.toArray());
        Throwable thrown = new NullPointerException("TESTING NULL POINTER");
        EntityManager em = perUtils.createEntityManager();
        em.getTransaction().begin();
        String userId = "file";
        int session = 1;
        Logfile log = new Logfile(userId, session);
        TestLogFileTask task = new TestLogFileTask(em, log);
        DbInsertion insertion = new DbInsertion(rec, thrown, task);
        insertion.start(new ExceptionsData());
        task.markFinished();

        LogRecord result = handler.getResult();
        assertNull("NOT A DIRECT USER", result);

        em.persist(new Directuser(directUserName));
        em.getTransaction().commit();
        em.getTransaction().begin();

        task = new TestLogFileTask(em, log);
        ExceptionsData excData = new ExceptionsData();
        insertion = new DbInsertion(rec, thrown, task);
        insertion.start(excData);
        task.markFinished();

        waitIssuezillaInsertionFinished();
        result = handler.getResult();
        assertNotNull("DIRECT USER", result);
        Object[] parameters = result.getParameters();
        assertEquals(summary, parameters[0]);
        assertEquals("exceptions", parameters[1]);
        assertEquals("petrzajac", parameters[2]);
        handler.flush();

        int issuezillaId = 10;
        org.netbeans.modules.exceptions.entity.Exceptions exc =
                em.find(org.netbeans.modules.exceptions.entity.Exceptions.class, excData.getSubmitId());
        Report report = exc.getReportId();
        report.setIssueId(issuezillaId);
        em.merge(report);
        Issue issue = BugReporterFactory.getDefaultReporter().getIssue(issuezillaId);
        issue.setLastResolutionChange(new Date());

        String undirectUser = "tester";
        params.set(3, undirectUser);
        params.set(6, undirectUser);
        rec.setParameters(params.toArray());
        task = new TestLogFileTask(em, log);
        insertion = new DbInsertion(rec, thrown, task);
        insertion.start(new ExceptionsData());
        task.markFinished();
        waitIssuezillaInsertionFinished();

        result = handler.getResult();
        assertNotNull("MANY DUPLICATES FILTER - ADD COMMENT", result);
        parameters = result.getParameters();
        assertEquals(Integer.toString(issuezillaId), parameters[0].toString());
        assertEquals("default comment reporter", null, parameters[1]);
        String str = (String) parameters[3];
        assertNotNull("GENERATED MESSAGE", str);
        assertTrue(str.contains("already"));
        handler.flush();

        issue.setIssueStatus("RESOLVED");
        issue.setResolution("FIXED");
        em.getTransaction().commit();

        em.getTransaction().begin();
        assertTrue(PersistenceUtils.issueIsFixed(issuezillaId));
        assertFalse(PersistenceUtils.issueIsOpen(issuezillaId));

        task = new TestLogFileTask(em, log);
        insertion = new DbInsertion(rec, thrown, task);
        insertion.start(new ExceptionsData());
        task.markFinished();
        waitIssuezillaInsertionFinished();

        assertNull("CLOSED ISSUE", handler.getResult());
        em.getTransaction().commit();
        em.close();
    }

    public void testGetDuplicates() throws Exception {
        LogRecord rec = new LogRecord(Level.SEVERE, "TESTING LOG RECORD");
        String userName = "GUEST";
        ArrayList<String> params = new ArrayList<String>(7);
        params.add("Linux, 2.6.15-1.2054_FC5smp, i386");
        params.add("Java HotSpot(TM) Client VM, 1.6.0-b105");
        params.add("NetBeans IDE Dev (Build 200702121900)");
        params.add(userName);
        params.add("UnknownError : Summary Message");
        params.add("COMMENT");
        params.add("test");
        rec.setParameters(params.toArray());
        Throwable thrown = new NullPointerException("TESTING NULL POINTER");
        EntityManager em = perUtils.createEntityManager();
        em.getTransaction().begin();
        String userId = "file";
        int session = 1;
        Logfile log = new Logfile(userId, session);
        DbInsertion insertion = new DbInsertion(rec, thrown, new TestLogFileTask(em, log));
        insertion.start(new ExceptionsData());
        //waitLogsParsed();
        List<org.netbeans.modules.exceptions.entity.Exceptions> all =
                PersistenceUtils.getAll(em, org.netbeans.modules.exceptions.entity.Exceptions.class);
        org.netbeans.modules.exceptions.entity.Exceptions rootException = all.get(all.size() - 1);
        rootId = rootException.getId();
        List<Nbuser> users = PersistenceUtils.getAll(em, Nbuser.class);
        Nbuser user = null;
        for (Nbuser us : users) {
            if (userName.equals(us.getName())) {
                user = us;
            }
        }
        all = PersistenceUtils.getAll(em, org.netbeans.modules.exceptions.entity.Exceptions.class);
        rootException = all.get(all.size() - 1);
        assertNotNull(rootException);
        assertEquals(1, rootException.getReportId().getDuplicates(em));
        assertEquals(1, rootException.getReportId().getDuplicatesFromDistinctUsers(em));
        assertEquals(1, rootException.getReportId().getDuplicatesFromOneUser(em, user));

        session = 2;
        log = new Logfile(userId, session);

        // -- refresh persistence context ---//
        em.getTransaction().commit();
        em.close();
        em = perUtils.createEntityManager();
        em.getTransaction().begin();
        all = PersistenceUtils.getAll(em, org.netbeans.modules.exceptions.entity.Exceptions.class);
        rootException = all.get(all.size() - 1);

        rootException = insert(em, new DbInsertion(rec, thrown, new TestLogFileTask(em, log)));
        assertEquals(2, rootException.getReportId().getDuplicates(em));
        assertEquals(1, rootException.getReportId().getDuplicatesFromDistinctUsers(em));
        assertEquals(2, rootException.getReportId().getDuplicatesFromOneUser(em, user));


        session = 3;
        log = new Logfile(userId, session);

        // -- refresh persistence context ---//
        em.getTransaction().commit();
        em.close();
        em = perUtils.createEntityManager();
        em.getTransaction().begin();
        all = PersistenceUtils.getAll(em, org.netbeans.modules.exceptions.entity.Exceptions.class);
        rootException = all.get(all.size() - 1);

        rootException = insert(em, new DbInsertion(rec, thrown, new TestLogFileTask(em, log)));
        assertEquals(3, rootException.getReportId().getDuplicates(em));
        assertEquals(1, rootException.getReportId().getDuplicatesFromDistinctUsers(em));
        assertEquals(3, rootException.getReportId().getDuplicatesFromOneUser(em, user));
        userName = "exceptions";
        params.set(3, userName);
        params.set(6, "petrzajac");
        rec.setParameters(params.toArray());

        session = 4;
        log = new Logfile(userId, session);

        // -- refresh persistence context ---//
        em.getTransaction().commit();
        em.close();
        em = perUtils.createEntityManager();
        em.getTransaction().begin();
        all = PersistenceUtils.getAll(em, org.netbeans.modules.exceptions.entity.Exceptions.class);
        rootException = all.get(all.size() - 1);

        rootException = insert(em, new DbInsertion(rec, thrown, new TestLogFileTask(em, log)));
        users = PersistenceUtils.getAll(em, Nbuser.class);
        Iterator<Nbuser> userIt = users.iterator();
        Nbuser user2 = null;
        while (userIt.hasNext()) {
            user2 = userIt.next();
            if (user2.getName().equals(userName)) {
                break;
            }
        }
        assertEquals(4, rootException.getReportId().getDuplicates(em));
        assertEquals(2, rootException.getReportId().getDuplicatesFromDistinctUsers(em));
        assertEquals(3, rootException.getReportId().getDuplicatesFromOneUser(em, user));
        assertEquals(1, rootException.getReportId().getDuplicatesFromOneUser(em, user2));

        session = 5;
        log = new Logfile(userId, session);
        rootException = insert(em, new DbInsertion(rec, thrown, new TestLogFileTask(em, log)));

        // -- refresh persistence context ---//
        em.getTransaction().commit();
        em.close();
        em = perUtils.createEntityManager();
        em.getTransaction().begin();
        all = PersistenceUtils.getAll(em, org.netbeans.modules.exceptions.entity.Exceptions.class);
        rootException = all.get(all.size() - 1);

        assertEquals(5, rootException.getReportId().getDuplicates(em));
        assertEquals(2, rootException.getReportId().getDuplicatesFromDistinctUsers(em));
        assertEquals(3, rootException.getReportId().getDuplicatesFromOneUser(em, user));
        assertEquals(2, rootException.getReportId().getDuplicatesFromOneUser(em, user2));
        em.getTransaction().commit();
        em.close();
    }

    public void testReopen() throws Exception {
        TestHandler handler = new TestHandler();
        BugReporter.LOG.addHandler(handler);
        IZInsertion.setDuplicatesNo(new Long(10));
        Integer issuezillaId = 100;

        EntityManager em = perUtils.createEntityManager();
        em.getTransaction().begin();

        int session = 0;
        org.netbeans.modules.exceptions.entity.Exceptions rootException = insert(em, session, null, null);
        rootException.getReportId().setIssueId(issuezillaId);
        em.merge(rootException);

        BugzillaReporter reporter = BugReporterFactory.getDefaultReporter();
        Issue issue = reporter.getIssue(issuezillaId);
        assertNotNull(issue);
        issue.setResolution("FIXED");
        issue.setIssueStatus("RESOLVED");
        issue.setLastResolutionChange(new Date());
        assertNull(handler.getResult());
        insert(em, ++session, null, null);
        assertNull("todays build", handler.getResult());

        insert(em, ++session, "unknown user", null);
        assertNull("guest", handler.getResult());

        insert(em, ++session, null, "");
        assertNull("empty comment", handler.getResult());

        issue.setLastResolutionChange(new Date("09/24/08"));
        issue.setTargetMilestone("7.0M1");
        insert(em, ++session, null, null);
        assertNull("old version", handler.getResult());

        issue.setTargetMilestone("6.0M9");
        isReopen = true;
        insert(em, ++session, null, null);
        isReopen = false;
        LogRecord result = handler.getResult();
        assertNotNull("newer build -> reopen", result);
        Object[] params = result.getParameters();
        assertEquals("exceptions", params[1]);
        assertEquals("petrzajac", params[2]);
        assertTrue(params[3].toString().contains("originally marked as duplicate of bug 100"));

        em.getTransaction().commit();
        em.close();
    }

    public void testAddToCC() throws Exception {
        TestHandler handler = new TestHandler("BUG REPORTER TO CC");
        BugReporter.LOG.addHandler(handler);
        IZInsertion.setDuplicatesNo(new Long(3));
        Integer issuezillaId = 1000;

        EntityManager em = perUtils.createEntityManager();
        em.getTransaction().begin();

        int session = 0;
        org.netbeans.modules.exceptions.entity.Exceptions rootException = insert(em, session);
        rootException.getReportId().setIssueId(issuezillaId);
        em.merge(rootException);
        assertNull("ADD TO CC", handler.getResult());

        BugzillaReporter reporter = BugReporterFactory.getDefaultReporter();
        Issue issue = reporter.getIssue(issuezillaId);
        assertNotNull(issue);
        issue.setLastResolutionChange(new Date());
        assertNull(handler.getResult());
        insert(em, ++session);
        LogRecord result = handler.getResult();
        assertNotNull("ADD TO CC", result);

        em.getTransaction().commit();
        em.close();
    }

    public void testDontInsertUserDirComponents() throws InterruptedException {
        TestHandler handler = new TestHandler();
        BugReporter.LOG.addHandler(handler);
        EntityManager em = perUtils.createEntityManager();
        em.getTransaction().begin();
        Logfile log = new Logfile("testDir", 1);
        ProductVersion pv = new ProductVersion(1);
        pv.setProductVersion("NetBeans IDE 6.0 (Build 081204)");
        Nbversion nbv = new Nbversion(1);
        nbv.setVersion("6.0");
        pv.setNbversionId(nbv);
        log.setProductVersionId(pv);
        TestLogFileTask task = new TestLogFileTask(em, log);
        Nbuser user = new Nbuser(1);
        user.setName("HALLO");
        org.netbeans.modules.exceptions.entity.Exceptions exc = new org.netbeans.modules.exceptions.entity.Exceptions(100);
        Report report = new Report(100);
        Stacktrace stacktrace = new Stacktrace(100);
        stacktrace.setLineCollection(Collections.<Line>emptyList());
        stacktrace.setClass1("NullPointerException");
        exc.setReportId(report);
        exc.setLogfileId(log);
        exc.setStacktrace(stacktrace);
        report.setComponent("user.dir");
        report.setSubcomponent("some.jar");
        exc.setNbuserId(user);
        em.persist(nbv);
        em.persist(pv);
        em.persist(report);
        em.persist(stacktrace);
        em.persist(user);
        em.persist(exc);

        new IZInsertion(exc, null).start();
        task.markFinished();
        IZInsertion.waitIZInsertionFinished();
        LogRecord result = handler.getResult();
        assertNull(result);

        em.getTransaction().commit();
        em.close();
    }

    private org.netbeans.modules.exceptions.entity.Exceptions insert(EntityManager em, DbInsertion dbInsertion) throws Exception {
        ExceptionsData eData = new ExceptionsData();
        dbInsertion.start(eData);
        assertTrue((Boolean) eData.isDuplicateReport());
        assertAreDuplicates(rootId, eData.getSubmitId());
        return em.find(org.netbeans.modules.exceptions.entity.Exceptions.class, rootId);
    }

    private class TestHandler extends Handler {

        Queue<LogRecord> result = new LinkedList<LogRecord>();
        final String message;

        private TestHandler() {
            this("BUG REPORTER");
        }

        private TestHandler(String mes) {
            this.message = mes;
        }

        public void publish(LogRecord record) {
            if (message.equals(record.getMessage())) {
                result.add(record);
            }
        }

        public void flush() {
            result.clear();
        }

        public void close() throws SecurityException {
            //
        }

        public LogRecord getResult() {
            return result.poll();
        }
    }

    private org.netbeans.modules.exceptions.entity.Exceptions insert(EntityManager em, int session) throws Exception {
        return insert(em, session, null, null);
    }

    private org.netbeans.modules.exceptions.entity.Exceptions insert(EntityManager em, int session, String userName, String comment) throws Exception {
        LogRecord rec = new LogRecord(Level.SEVERE, "TESTING LOG RECORD");
        if (userName == null){
            userName = "exceptions";
        }
        if (comment == null){
            comment = "COMMENT";
        }
        ArrayList<String> params = new ArrayList<String>(7);
        params.add("Linux, 2.6.15-1.2054_FC5smp, i386");
        params.add("Java HotSpot(TM) Client VM, 1.6.0-b105");
        params.add("NetBeans IDE 6.8 (Build 200811100001)");
        params.add(userName);
        params.add("UnknownError : Summary Message");
        params.add(comment);
        params.add("petrzajac");
        rec.setParameters(params.toArray());
        Throwable thrown = new NullPointerException("TESTING NULL POINTER");
        String userId = "file";

        ExceptionsData eData = new ExceptionsData();
        Logfile log = new Logfile(userId, session);
        TestLogFileTask task = new TestLogFileTask(em, log);
        DbInsertion dbInsertion = new DbInsertion(rec, thrown, task);
        dbInsertion.start(eData);
        task.markFinished();
        waitIssuezillaInsertionFinished();
        if ((Boolean) eData.isDuplicateReport()) {
            if (!isReopen){
                assertAreDuplicates(rootId, eData.getSubmitId());
            }else{
                assertNotNull(eData.getReportBeforeReopenId());
                assertEquals(eData.getReportId() - 1, eData.getReportBeforeReopenId().intValue());
            }
        } else {
            rootId = eData.getSubmitId();
        }
        return em.find(org.netbeans.modules.exceptions.entity.Exceptions.class, rootId);
    }

    @Override
    protected void waitLog(File log, String ip) throws Exception {
        super.waitLog(log, ip);
        waitIssuezillaInsertionFinished();
    }
    
}