/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */
package org.netbeans.server.uihandler;

import org.netbeans.modules.exceptions.utils.LogReporter;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.security.Principal;
import java.util.Collections;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.AbstractPreferences;
import java.util.prefs.BackingStoreException;
import javax.persistence.EntityManager;
import javax.servlet.RequestDispatcher;
import javax.servlet.Servlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.ServletOutputStream;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionContext;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.el.ExpressionEvaluator;
import javax.servlet.jsp.el.VariableResolver;
import org.netbeans.junit.MockServices;
import org.netbeans.junit.NbTestCase;
import org.netbeans.modules.exceptions.entity.InnocentClass;
import org.netbeans.modules.exceptions.entity.Nbversion;
import org.netbeans.modules.exceptions.entity.Submit;
import org.netbeans.modules.exceptions.utils.PersistenceUtils;
import org.netbeans.server.componentsmatch.SourcejarMatcherTest;
import org.netbeans.server.componentsmatch.PackageMatcher;
import org.netbeans.server.services.beans.Components;
import org.netbeans.server.uihandler.LogsManager.Value;
import org.netbeans.server.uihandler.LogsManager.VersionData;
import org.netbeans.server.uihandler.api.Authenticator;

/**
 *
 * @author Jindrich Sedek
 */
public abstract class DatabaseTestCase extends NbTestCase {

    public static final String LOCALHOST_IP = "127.0.0.1";
    protected static Logger LOG;
    protected PersistenceUtils perUtils;
    protected File data;
    protected LogsManager logsManager;
    protected PageContext page;

    public static void initPersitenceUtils(File workDir) {
        PersistenceUtils.initTestPersistanceUtils(new File(workDir, "db"));
    }

    protected File prepareDir(String prefix) throws IOException {
        File f = new File(getWorkDir(), prefix + getName());
        f.mkdirs();
        return f;
    }

    public DatabaseTestCase(String str) {
        super(str);
    }

    public static void initSettings(File directory) throws IOException{
        PersistenceUtils.setSettingsDirectory(directory.toString());
        PersistenceUtils.getInstance().setTestComponents(loadTestComponents());
    }

    @Override
    protected void setUp() throws Exception {
        System.out.println("running " + getName());
        LOG = Logger.getLogger("test." + getName());
        LogFileTask.closed = false;
        clearWorkDir();
        initPersitenceUtils(prepareDir("db"));
        initSettings(getWorkDir());
        Utils.setRootUploadDir(getWorkDirPath());
        MockServices.setServices(LogReporter.class, MockAuthenticator.class, PackageMatcher.class);
        InputStream inputStream = DatabaseTestCase.class.getResourceAsStream("testPrivKey");
        Authenticate.loadPrivateKey(inputStream);
        setIZInsertion();
        perUtils = PersistenceUtils.getInstance();
        updateSourcejarMapping();
        Utils.setParsingTimeout(0);
        data = prepareDir("logs");
        logsManager = LogsManager.createManager(data);
        logsManager.waitVerificationFinished();
        page = createPageContext();
    }

    private void updateSourcejarMapping() throws IOException {
        EntityManager em = perUtils.createEntityManager();
        em.getTransaction().begin();
        InputStream is = SourcejarMatcherTest.class.getResourceAsStream("insert.sql");
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        String query = reader.readLine();
        reader.close();
        em.createNativeQuery(query).executeUpdate();
        em.getTransaction().commit();
        em.close();
    }

    public static void setIZInsertion(){
        IZInsertion.setDuplicatesNo(100l);
        IZInsertion.setReporterTimeout(1000);
        IZInsertion.setOldReportsDate(new Date(103,1,1));
    }
    
    @Override
    protected void tearDown() throws Exception {
        logsManager.close();
        super.tearDown();
    }

    @Override
    protected Level logLevel() {
        return Level.ALL;
    }

    @Override
    protected int timeOut() {
        return 600000; // 10min
    }

    @SuppressWarnings("unchecked")
    protected ExceptionsData addLog(PageContext page, File log, String ip, String userId) throws Exception {
        logsManager.addLog(log, ip);
        waitLogsParsed();
        LOG.log(Level.FINE, "Feeding page for user {0}", userId);
        page.getRequest().setAttribute("id", userId);
        logsManager.preparePageContext(page, null);
        ExceptionsData attributeData = (ExceptionsData) page.getAttribute("lastExceptions");
        ExceptionsData uploadedData = LogsManager.getDefault().getUploadDoneExceptions(page);
        if (uploadedData == null) {
            System.out.println("WARNING - NO UPLOADED DATA");
            return attributeData;
        }
        assertEquals("both data should hold the same data", uploadedData.isDuplicateReport(), attributeData.isDuplicateReport());
        assertEquals("both data should hold the same data", uploadedData.getSubmitId(), attributeData.getSubmitId());
        assertEquals("both data should hold the same data", uploadedData.getIssuezillaId(), attributeData.getIssuezillaId());
        assertEquals("both data should hold the same data", uploadedData.isDuplicateReport(), attributeData.isDuplicateReport());
        assertEquals("both data should hold the same data", uploadedData.isExceptionReport(), attributeData.isExceptionReport());
        return uploadedData;

    }

    protected Object processLog(Class<?> testClass, String logName, String statistic_name) throws Exception{
        File log = LogsManagerTest.extractResourceAs(testClass, data, logName, "log444");
        waitLog(log, "127.0.0.1");
        PageContext png = LogsManagerTest.createPageContext();
        logsManager.preparePageContext(png, "log444", Collections.singleton(statistic_name));
        return png.getAttribute("global" + statistic_name);
    }

    protected final void waitLogsParsed() throws Exception {
        LOG.fine("waitLogsParsed");
        logsManager.preparePageContext(createPageContext(), "noID", null);
        LOG.fine("waitLogsParsed - after preparePageContext");
    }

    protected final void waitVersionRecountFinished(Nbversion version) throws InterruptedException{
        List<Value> value = null;
        int count = 0;
        while (value== null) {
            VersionData vd = LogsManager.getDefault().getVersionStatistics(version);
            if (vd != null){
                value = vd.getData();
            }
            Thread.sleep(1000);
            if (++count == 1000) {
                fail("No data were recounted in time limit");
            }
        }
    }

    public static void waitIssuezillaInsertionFinished() throws Exception {
        IZInsertion.waitIZInsertionFinished();
    }

    protected ExceptionsData addLog(PageContext page, File log, String userId) throws Exception {
        return addLog(page, log, "127.0.0.1", userId);
    }

    protected ExceptionsData addLog(File log, String userId) throws Exception {
        return addLog(page, log, "127.0.0.1", userId);
    }

    protected void waitLog(File log, String ip) throws Exception {
        logsManager.addLog(log, ip);
        waitLogsParsed();
    }

    public boolean areDuplicates(int excId1, int excId2) {
        EntityManager em = perUtils.createEntityManager();
        boolean result = areDuplicates(em, excId1, excId2);
        em.close();
        return result;
    }

    public boolean areDuplicates(EntityManager em, int sbm1, int sbm2) {
        boolean result = false;
        Submit exc1 = em.find(Submit.class, sbm1);
        assert exc1 != null;
        assert exc1.getReportId() != null;
        Submit exc2 = em.find(Submit.class, sbm2);
        assert exc2 != null;
        assert exc2.getReportId() != null;
        result = exc1.getReportId().getId() == exc2.getReportId().getId();
        return result;
    }

    public void assertAreDuplicates(int excId1, int excId2) {
        EntityManager em = perUtils.createEntityManager();
        assertTrue(areDuplicates(em, excId1, excId2));
        em.close();
    }
    public void assertAreDuplicates(EntityManager em, int excId1, int excId2) {
        assertTrue(areDuplicates(em, excId1, excId2));
    }

    public void assertAreDuplicates(String message, int excId1, int excId2) {
        EntityManager em = perUtils.createEntityManager();
        assertTrue(message, areDuplicates(em, excId1, excId2));
        em.close();
    }

    public void assertAreDuplicates(String message, EntityManager em, int excId1, int excId2) {
        assertTrue(message, areDuplicates(em, excId1, excId2));
    }

    public static PageContext createPageContext(HttpSession session, ServletRequest request) {
        return PgCnt.create(session, request);
    }

    public static PageContext createPageContext() {
        final Map<String, Object> attr = new HashMap<String, Object>();
        final Map<String, String> param = new HashMap<String, String>();
        HttpServletRequest request = new TestHttpSevletRequest(attr, param);
        HttpServletResponse response = new TestHttpServletResponse();
        HttpSession session = new HttpSessionImpl(attr);
        return PgCnt.create(session, request, response);
    }

    private static Components loadTestComponents() throws IOException{
        TreeMap<String, String[]> newComponents = new TreeMap<String, String[]>();
        Properties p = new Properties();
        InputStream is = DatabaseTestCase.class.getResourceAsStream("testComponents");
        p.load(is);
        is.close();
        Enumeration comp = p.keys();
        while (comp.hasMoreElements()) {
            String component = (String) comp.nextElement();
            component = component.toLowerCase();
            String[] subArray = p.getProperty(component).split(",");
            TreeSet<String> ts = new TreeSet<String>();
            for (String subComp : subArray) {
                ts.add(subComp.trim());
            }
            newComponents.put(component, ts.toArray(new String[0]));
        }
        return new Components(newComponents);
    }

    public static void setUpInnocents(EntityManager em){
        em.persist(new InnocentClass("org.openide.filesystems.FileUtil"));
        em.persist(new InnocentClass("URLMapper"));
        em.persist(new InnocentClass("org.netbeans.core.TimableEventQueue"));
        em.persist(new InnocentClass("org.netbeans.api.java.source"));
        em.persist(new InnocentClass("org.netbeans.api.java.classpath"));
        em.persist(new InnocentClass("org.netbeans.modules.java.source"));
        em.persist(new InnocentClass("org.netbeans.modules.java.classpath"));
    }

    public static class MockAuthenticator implements Authenticator{

        public AuthToken authenticate(String userName, String passwd) {
            if (userName.equals("exceptions")&&(passwd.equals("petrzajac"))) return new AuthToken(userName, passwd);
            else if (userName.equals("tester")&&(passwd.equals("tester"))) return new AuthToken(userName, passwd);
            else return null;
        }

        public String getDefaultReporterUsername() {
            return "exception_reporter";
        }

        public void loadPrivateKey(InputStream is) {
            //
        }

    }

    public static class TestHttpSevletRequest implements HttpServletRequest {

        Map<String, Object> attr;
        Map<String, String> param;
        Map<String, String> header;

        public TestHttpSevletRequest(Map<String, Object> attr, Map<String, String> param) {
            this.attr = attr;
            this.param = param;
        }

        public TestHttpSevletRequest(Map<String, String> header) {
            this.header = header;
        }

        public String getAuthType() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public Cookie[] getCookies() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public long getDateHeader(String arg0) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public String getHeader(String arg0) {
            return header.get(arg0);
        }

        public Enumeration getHeaders(String arg0) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public Enumeration getHeaderNames() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public int getIntHeader(String arg0) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public String getMethod() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public String getPathInfo() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public String getPathTranslated() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public String getContextPath() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public String getQueryString() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public String getRemoteUser() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public boolean isUserInRole(String arg0) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public Principal getUserPrincipal() {
            return new Principal() {

                public String getName() {
                    return "qarobot";
                }
            };
        }

        public String getRequestedSessionId() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public String getRequestURI() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public StringBuffer getRequestURL() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public String getServletPath() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public HttpSession getSession(boolean arg0) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public HttpSession getSession() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public boolean isRequestedSessionIdValid() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public boolean isRequestedSessionIdFromCookie() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public boolean isRequestedSessionIdFromURL() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public boolean isRequestedSessionIdFromUrl() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public Object getAttribute(String arg0) {
            return attr.get(arg0);
        }

        public Enumeration getAttributeNames() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public String getCharacterEncoding() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public void setCharacterEncoding(String arg0) throws UnsupportedEncodingException {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public int getContentLength() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public String getContentType() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public ServletInputStream getInputStream() throws IOException {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public String getParameter(String arg0) {
            return param.get(arg0);
        }

        public Enumeration getParameterNames() {
            return Collections.enumeration(param.keySet());
        }

        public String[] getParameterValues(String arg0) {
            return param.values().toArray(new String[0]);
        }

        public Map getParameterMap() {
            return param;
        }

        public String getProtocol() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public String getScheme() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public String getServerName() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public int getServerPort() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public BufferedReader getReader() throws IOException {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public String getRemoteAddr() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public String getRemoteHost() {
            return LOCALHOST_IP;
        }

        public void setAttribute(String attrName, Object attrValue) {
            if (attr == null) {
                attr = new HashMap<String, Object>();
            }
            attr.put(attrName, attrValue);
        }

        public void removeAttribute(String arg0) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public Locale getLocale() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public Enumeration getLocales() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public boolean isSecure() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public RequestDispatcher getRequestDispatcher(String arg0) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public String getRealPath(String arg0) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public int getRemotePort() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public String getLocalName() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public String getLocalAddr() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public int getLocalPort() {
            throw new UnsupportedOperationException("Not supported yet.");
        }
    }

    public static class TestHttpServletResponse implements HttpServletResponse {

        StringWriter writer = new StringWriter();
        public String getOutput(){
            return writer.toString();
        }
        public void addCookie(Cookie arg0) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public boolean containsHeader(String arg0) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public String encodeURL(String arg0) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public String encodeRedirectURL(String arg0) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public String encodeUrl(String arg0) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public String encodeRedirectUrl(String arg0) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public void sendError(int arg0, String arg1) throws IOException {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public void sendError(int arg0) throws IOException {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public void sendRedirect(String arg0) throws IOException {
            // nothing to do
        }

        public void setDateHeader(String arg0, long arg1) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public void addDateHeader(String arg0, long arg1) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public void setHeader(String arg0, String arg1) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public void addHeader(String arg0, String arg1) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public void setIntHeader(String arg0, int arg1) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public void addIntHeader(String arg0, int arg1) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public void setStatus(int arg0) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public void setStatus(int arg0, String arg1) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public String getCharacterEncoding() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public String getContentType() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public ServletOutputStream getOutputStream() throws IOException {
            return new ServletOutputStream() {

                @Override
                public void write(int b) throws IOException {
                    writer.write(b);
                }
            };
        }

        public PrintWriter getWriter() throws IOException {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public void setCharacterEncoding(String arg0) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public void setContentLength(int arg0) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public void setContentType(String arg0) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public void setBufferSize(int arg0) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public int getBufferSize() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public void flushBuffer() throws IOException {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public void resetBuffer() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public boolean isCommitted() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public void reset() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public void setLocale(Locale arg0) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public Locale getLocale() {
            throw new UnsupportedOperationException("Not supported yet.");
        }
    }

    static class HttpSessionImpl implements HttpSession {

        private final Map<String, Object> attr;

        public HttpSessionImpl(Map<String, Object> attr) {
            this.attr = attr;
        }

        public long getCreationTime() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public String getId() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public long getLastAccessedTime() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public ServletContext getServletContext() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public void setMaxInactiveInterval(int arg0) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public int getMaxInactiveInterval() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public HttpSessionContext getSessionContext() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public Object getAttribute(String arg0) {
            return attr.get(arg0);
        }

        public Object getValue(String arg0) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public Enumeration getAttributeNames() {
            return Collections.enumeration(attr.keySet());
        }

        public String[] getValueNames() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public void setAttribute(String name, Object value) {
            attr.put(name, value);
        }

        public void putValue(String arg0, Object arg1) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public void removeAttribute(String arg0) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public void removeValue(String arg0) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public void invalidate() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public boolean isNew() {
            throw new UnsupportedOperationException("Not supported yet.");
        }
    }

    static final class PgCnt extends PageContext {

        private Map<String, Object> attr = new HashMap<String, Object>();
        private HttpSession session;
        private ServletRequest request;
        private ServletResponse response;

        private PgCnt() {
        }

        public PgCnt(HttpSession session, ServletRequest request, ServletResponse response) {
            this.session = session;
            this.request = request;
            this.response = response;
        }

        public static PageContext create() {
            return new PgCnt();
        }

        public static PageContext create(HttpSession session, ServletRequest request) {
            return new PgCnt(session, request, null);
        }

        public static PageContext create(HttpSession session, ServletRequest request, ServletResponse response) {
            return new PgCnt(session, request, response);
        }

        public void setAttribute(String arg0, Object arg1) {
            attr.put(arg0, arg1);
        }

        public void setAttribute(String arg0, Object arg1, int arg2) {
            setAttribute(arg0, arg1);
        }

        public Object getAttribute(String arg0) {
            return attr.get(arg0);
        }

        public Object getAttribute(String arg0, int arg1) {
            return getAttribute(arg0);
        }

        public Object findAttribute(String arg0) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public void removeAttribute(String arg0) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public void removeAttribute(String arg0, int arg1) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public int getAttributesScope(String arg0) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public Enumeration getAttributeNamesInScope(int arg0) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public JspWriter getOut() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public ExpressionEvaluator getExpressionEvaluator() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public VariableResolver getVariableResolver() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public void initialize(Servlet arg0, ServletRequest arg1, ServletResponse arg2, String arg3, boolean arg4, int arg5, boolean arg6) throws IOException, IllegalStateException, IllegalArgumentException {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public void release() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public HttpSession getSession() {
            return session;
        }

        public Object getPage() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public ServletRequest getRequest() {
            return request;
        }

        public ServletResponse getResponse() {
            return response;
        }

        public Exception getException() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public ServletConfig getServletConfig() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public ServletContext getServletContext() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public void forward(String arg0) throws ServletException, IOException {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public void include(String arg0) throws ServletException, IOException {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public void include(String arg0, boolean arg1) throws ServletException, IOException {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public void handlePageException(Exception arg0) throws ServletException, IOException {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public void handlePageException(Throwable arg0) throws ServletException, IOException {
            throw new UnsupportedOperationException("Not supported yet.");
        }

    }

    public static class TestPreferences extends AbstractPreferences {

        Map<String, String> map = new HashMap<String, String>();

        public TestPreferences() {
            super(null, "");
        }

        @Override
        protected void putSpi(String key, String value) {
            map.put(key, value);
        }

        @Override
        protected String getSpi(String key) {
            return map.get(key);
        }

        @Override
        protected void removeSpi(String key) {
            map.remove(key);
        }

        @Override
        protected void removeNodeSpi() throws BackingStoreException {
        }

        @Override
        protected String[] keysSpi() throws BackingStoreException {
            return map.keySet().toArray(new String[0]);
        }

        @Override
        protected String[] childrenNamesSpi() throws BackingStoreException {
            return new String[0];
        }

        @Override
        protected AbstractPreferences childSpi(String name) {
            return this;
        }

        @Override
        protected void syncSpi() throws BackingStoreException {
        }

        @Override
        protected void flushSpi() throws BackingStoreException {
        }
    }
}
