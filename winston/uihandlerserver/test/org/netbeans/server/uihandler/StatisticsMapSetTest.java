/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */
package org.netbeans.server.uihandler;

import java.io.File;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.prefs.Preferences;
import javax.persistence.EntityManager;
import org.junit.Test;
import org.netbeans.modules.exceptions.entity.Logfile;
import org.netbeans.modules.exceptions.entity.Statistic;
import org.netbeans.server.uihandler.statistics.Help;
import org.netbeans.server.uihandler.statistics.NetBeansModules;
import org.openide.util.Lookup;

/**
 *
 * @author Jindrich Sedek
 */
public class StatisticsMapSetTest extends DatabaseTestCase {

    public StatisticsMapSetTest(String str) {
        super(str);
    }

    @Test
    public void testSet() throws Exception {
        File log = LogsManagerTest.extractResourceAs(data, "1.log", "log444");
        logsManager.addLog(log, "127.0.0.1");
        Statistics<?> somestats = Lookup.getDefault().lookup(Help.class);
        EntityManager em = perUtils.createEntityManager();
        Logfile logfile = LogFileTask.getLogInfo("log444", em);
        em.getTransaction().begin();
        Preferences prefs = DbPreferences.root(logfile, somestats, em);

        Set<String> testData = new HashSet<String>(10);
        String key = "key";
        testData.add("Test1");
        testData.add("Test2");
        testData.add("Test10");
        Statistics.putSet(somestats, prefs, key, testData);
        assertEqualsSets(testData, Statistics.getSet(somestats, prefs, key));
        
        testData.add("test3");
        testData.add("Test21");

        Statistics.putSet(somestats, prefs, key, testData);

        assertEqualsSets(testData, Statistics.getSet(somestats, prefs, key));

        String key2 = "key2";
        testData.add("testKey");
        Statistics.putSet(somestats, prefs, key2, testData);
        assertEqualsSets(testData, Statistics.getSet(somestats, prefs, key2));
        
        Set<String> result_key1 = Statistics.getSet(somestats, prefs, key);
        assertFalse(result_key1.contains("testKey"));
        em.getTransaction().commit();
        em.close();
    }
    
//    @Test
//    public void testMap() throws Exception {
//        File log = LogsManagerTest.extractResourceAs(data, "1.log", "log444");
//        logsManager.addLog(log, "127.0.0.1");
//        Statistics<?> somestats = Lookup.getDefault().lookup(NetBeansModules.class);
//        Statistic.getExists(somestats.name);
//
//        EntityManager em = perUtils.createEntityManager();
//        Logfile logfile = LogFileTask.getLogInfo("log444", em);
//        em.getTransaction().begin();
//        Preferences prefs = DbPreferences.root(logfile, somestats, em);
//
//        Map<String, Integer> testData = new HashMap<String, Integer>(10);
//        String key = "key";
//        testData.put("Test1", 10);
//        testData.put("Test2", 50);
//        testData.put("Test10", 25);
//
//        Statistics.putMap(somestats, prefs, key, testData);
//        assertEqualsMaps(testData, Statistics.getMap(somestats, prefs, key));
//        
//        testData.put("test3", 3);
//        testData.put("Test21", 21);
//
//        Statistics.putMap(somestats, prefs, key, testData);
//        assertEqualsMaps(testData, Statistics.getMap(somestats, prefs, key));
//
//        String key2 = "key2";
//        testData.put("testKey", 15);
//        testData.put("testKey", 19);
//        testData.put("testKey", 2);
//        testData.put("testKey", 3);
//        Statistics.putMap(somestats, prefs, key2, testData);
//        assertEqualsMaps(testData, Statistics.getMap(somestats, prefs, key2));
//        
//        Map<String, Integer> result_key1 = Statistics.getMap(somestats, prefs, key);
//        assertNull(result_key1.get("testKey"));
//        em.getTransaction().commit();
//        em.close();
//    }
//
//    public void testBigMapNumber() throws Exception{
//        Random random = new Random();
//        File log = LogsManagerTest.extractResourceAs(data, "1.log", "log444");
//        logsManager.addLog(log, "127.0.0.1");
//        Statistics<?> somestats = Lookup.getDefault().lookup(Help.class);
//        EntityManager em = perUtils.createEntityManager();
//        em.getTransaction().begin();
//        Logfile logfile = LogFileTask.getLogInfo("log444", em);
//        Preferences prefs = DbPreferences.root(logfile, somestats, em);
//
//        Map<String, Integer> testData = new HashMap<String, Integer>(10);
//        String key = "key";
//        testData.put("Test1", random.nextInt(Integer.MAX_VALUE));
//        testData.put("Test2", random.nextInt(Integer.MAX_VALUE));
//        testData.put("Test3", random.nextInt(Integer.MAX_VALUE));
//        testData.put("Test4", random.nextInt(Integer.MAX_VALUE));
//        Statistics.putMap(somestats, prefs, key, testData);
//        assertEqualsMaps(testData, Statistics.getMap(somestats, prefs, key));
//
//        
//        for (Integer value = 1; value > 0; value = (value + 3) * 19 / 17) {
//            testData.put("Test"+value, value);
//        }
//        Statistics.putMap(somestats, prefs, key, testData);
//        assertEqualsMaps(testData, Statistics.getMap(somestats, prefs, key));
//        em.getTransaction().commit();
//        em.close();
//    }
//    
//    public void testManyNumbers() throws Exception{
//        File log = LogsManagerTest.extractResourceAs(data, "1.log", "log444");
//        logsManager.addLog(log, "127.0.0.1");
//        Statistics<?> somestats = Lookup.getDefault().lookup(Help.class);
//        EntityManager em = perUtils.createEntityManager();
//        Logfile logfile = LogFileTask.getLogInfo("log444", em);
//        em.getTransaction().begin();
//        Preferences prefs = DbPreferences.root(logfile, somestats, em);
//
//        String key = "key";
//        for (int j = 1; j < 10; j++){
//            Map<String, Integer> testData = new HashMap<String, Integer>(200);
//            for (int i = 1; i < 50; i++){
//                int value = j * 1024 + i;
//                testData.put("test"+value, value);
//            }
//            Statistics.putMap(somestats, prefs, key, testData);
//            assertEqualsMaps(testData, Statistics.getMap(somestats, prefs, key));
//            if (em.getTransaction().isActive()){
//                em.getTransaction().commit();
//                em.getTransaction().begin();
//            }
//        }
//        em.getTransaction().commit();
//        em.close();
//    }
    
    private void assertEqualsSets(Set<String> set1, Set<String> set2){
        assertEquals(set1.size(), set2.size());
        for (String string : set1) {
            assertTrue(set2.contains(string));
        }
    }
    
    private void assertEqualsMaps(Map<String, Integer> map1, Map<String, Integer> map2){
        assertEquals(map1.size(), map2.size());
        for (String string : map1.keySet()) {
            assertEquals(map1.get(string), map2.get(string));
        }
    }
}
