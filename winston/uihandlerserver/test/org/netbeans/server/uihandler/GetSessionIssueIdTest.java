/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */

package org.netbeans.server.uihandler;

import java.io.File;
import java.util.List;

/**
 *
 * @author Jindrich Sedek
 */
public class GetSessionIssueIdTest extends DatabaseTestCase{
    
    public GetSessionIssueIdTest(String testName) {
        super(testName);
    }
    
    
    /** Creates a new instance of GetSessionIssueIdTest */
    public void  testGetLastSessionIssueId() throws Exception{
        ExceptionsData mapa;
        logsManager.preparePageContext(page, null);
        mapa = (ExceptionsData) page.getAttribute("lastExceptions");
        assertNull(mapa);
        
        File log = LogsManagerTest.extractResourceAs(data, "2.log", "log4442.1");
        mapa = addLog(page, log, "log4442");
        assertNotNull("MAPA SHOULD BE NOT NULL", mapa);
        assertTrue("ONE SESSION IS ALREADY LOGGED", mapa.isExceptionReport());
        assertFalse("THIS ISSUE SHOULD HAVE NO DUPLICATES",  mapa.isDuplicateReport());
        assertEquals("THIS ISSUE SHOULD HAVE ID 1",  1, mapa.getSubmitId().intValue());// ID 1
        assertFalse("THIS ISSUE SHOULD HAVE NO DUPLICATE ID",  mapa.isDuplicateReport());// ID 1
        
        log = LogsManagerTest.extractResourceAs(data, "2.log", "log4443.1");
        mapa = addLog(page, log, "log4443");
        assertNotNull("MAPA SHOULD BE NOT NULL", mapa);
        assertTrue("ONE SESSION IS ALREADY LOGGED", mapa.isExceptionReport());
        assertTrue("THIS ISSUE SHOULD BE DUPLICATE OF ISSUE 1", mapa.isDuplicateReport());
        assertAreDuplicates("THIS ISSUE SHOULD BE DUPLICATE OF ISSUE 1", 1, mapa.getSubmitId()); // ID 2
        
        log = LogsManagerTest.extractResourceAs(data, "1.log", "log4442.2");
        logsManager.addLog(log, "127.0.0.1");
        //        I don't have to wait since there are not any DB insertions
        logsManager.preparePageContext(page, "log4442", null);
        mapa = (ExceptionsData) page.getAttribute("lastExceptions");
        assertNotNull("MAPA SHOULD BE NOT NULL", mapa);
        assertFalse("THIS IS NOT AN ERROR REPORT", mapa.isExceptionReport());
        
        /** test issue   #95540 */
        log = LogsManagerTest.extractResourceAs(data, "3.log", "log4445.1");
        mapa = addLog(page, log, "log4445");
        assertNotNull("MAPA SHOULD BE NOT NULL", mapa);
        assertTrue("EXC REPORT IS ALREADY LOGGED", mapa.isExceptionReport());
        assertFalse("THIS SHOULD BE INSERTED AS A NEW ISSUE", mapa.isDuplicateReport());
        assertEquals("THIS SHOULD BE INSERTED AS A NEW ISSUE", 3, mapa.getSubmitId().intValue()); // ID 3
        
        List<org.netbeans.modules.exceptions.entity.Exceptions> list =
                perUtils.getAll(org.netbeans.modules.exceptions.entity.Exceptions.class);
        assertEquals("THERE MUST BE 3 ISSUES", 3, list.size());
    }
    
}
