/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * Portions Copyrighted 2007 Sun Microsystems, Inc.
 */
package org.netbeans.server.uihandler;

import java.io.File;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.prefs.Preferences;
import javax.persistence.EntityManager;
import org.netbeans.modules.exceptions.entity.Logfile;
import org.netbeans.modules.exceptions.utils.PersistenceUtils;
import org.netbeans.server.uihandler.statistics.Help;
import org.openide.util.Lookup;

/**
 *
 * @author Jaroslav Tulach
 */
public class DbPreferencesTest extends DatabaseTestCase {
    private LogsManager result;
    private EntityManager em;
    private Logfile file = null;
   
    public DbPreferencesTest(String testName) {
        super(testName);
    }
    @Override
    protected Level logLevel() {
        return Level.FINE;
    }
    
    @Override protected void setUp() throws Exception {
        super.setUp();
        em = perUtils.createEntityManager();
        em.getTransaction().begin();
        result = logsManager;
        
        assertEquals("NO logs", 0, result.getNumberOfLogs());

        File log = LogsManagerTest.extractResourceAs(data, "1.log", "log444");
        
        result.addLog(log, "127.0.0.1");
        result.waitParsingFinished();
        file = LogFileTask.getRunningTask().getLogInfo();
    }
    
    @Override protected void tearDown() throws Exception {
        if (root != null) {
            // do cleanup
            for (String n : root.childrenNames()) {
                root.node(n).removeNode();
            }
        }
        em.getTransaction().commit();
        em.close();
        LogsManager.getDefault().close();
    }
    
    public void testBasicPrefsOps() throws Exception {
        File log2 = LogsManagerTest.extractResourceAs(data, "2.log", "log4442");
        result.addLog(log2, "127.0.0.1");
        result.waitParsingFinished();
        Logfile file2 = LogFileTask.getRunningTask().getLogInfo();

        assertNotNull("f1", file);
        assertNotNull("f2", file2);
        if (file == file2) {
            fail("log files are same!");
        }
        
        Statistics<?> somestats = Lookup.getDefault().lookup(Help.class);
        assertNotNull("Stat is here", somestats);
        
        root = DbPreferences.root(file, somestats, em);
        
        Preferences prefs1 = root;
        Preferences prefs2 = DbPreferences.root(file2, somestats, em);
        
        prefs1.putBoolean("ahoj", true);
        assertTrue("ahoj is set", prefs1.getBoolean("ahoj", false));
        assertEquals("one node here", 1, prefs1.keys().length);
        assertEquals("ahoj is not set in the other prefs", 0, prefs2.keys().length);
    }

    public void testSubProps() throws Exception {
        assertNotNull("f1", file);
        
        Statistics<?> somestats = Lookup.getDefault().lookup(Help.class);
        assertNotNull("Stat is here", somestats);
        
        Preferences prefs1 = DbPreferences.root(file, somestats, em).node("sub/child");
        Preferences prefs2 = DbPreferences.root(file, somestats, em).node("sub/child2");
        
        prefs1.putBoolean("ahoj", true);
        assertTrue("ahoj is set", prefs1.getBoolean("ahoj", false));
        assertEquals("one node here", 1, prefs1.keys().length);
        assertEquals("ahoj is not set in the other prefs", 0, prefs2.keys().length);

        prefs1.removeNode();

        
        Preferences prefs3 = DbPreferences.root(file, somestats, em).node("sub/child");
        assertEquals("ahoj is not set in removed node", 0, prefs3.keys().length);
    }

    public void testPutTwice() throws Exception {
        assertNotNull("f1", file);
        
        Statistics<?> somestats = Lookup.getDefault().lookup(Help.class);
        assertNotNull("Stat is here", somestats);
        
        Preferences prefs1 = DbPreferences.root(file, somestats, em).node("subTwice");

        prefs1.putBoolean("ahoj", true);
        prefs1.putBoolean("ahoj", false);
        assertFalse("ahoj is not set", prefs1.getBoolean("ahoj", true));
        String[] keys = prefs1.keys();
        assertEquals("one node here: " + Arrays.asList(keys), 1, keys.length);

        prefs1.remove("ahoj");

        keys = prefs1.keys();
        assertEquals("ahoj is not set in removed node:" + Arrays.asList(keys), 0, keys.length);

    }

    public void testSubPropsDeleteKey() throws Exception {
        assertNotNull("f1", file);
        
        Statistics<?> somestats = Lookup.getDefault().lookup(Help.class);
        assertNotNull("Stat is here", somestats);
        
        root = DbPreferences.root(file, somestats, em);
        Preferences prefs1 = root.node("subX");
        Preferences prefs2 = root.node("subX/child2");

        prefs1.putBoolean("ahoj", true);
        prefs2.putInt("kuk", 10);
        assertTrue("ahoj is set", prefs1.getBoolean("ahoj", false));
        String[] keys = prefs1.keys();
        assertEquals("one node here: " + Arrays.asList(keys), 1, keys.length);
        assertEquals("ahoj is not set in the other prefs", null, prefs2.get("ahoj", null));
        assertEquals("kuk is set in the other prefs", 10, prefs2.getInt("kuk", 0));

        prefs1.remove("ahoj");

        keys = prefs1.keys();
        assertEquals("ahoj is not set in removed node:" + Arrays.asList(keys), 0, keys.length);

    }
    
    public void testInsertLong() throws Exception {
        assertNotNull("f1", file);
        
        Statistics<?> somestats = Lookup.getDefault().lookup(Help.class);
        assertNotNull("Stat is here", somestats);

        root = DbPreferences.root(file, somestats, em);
        Preferences prefs1 = root.node("subBelow");
        try {
            prefs1.putInt(
                "org.netbeans.modules.apisupport.project.ui.LibrariesNode$AddModuleDependencyAction",
                10
            );
            fail("Key is supposed to be too long");
        } catch (IllegalArgumentException ex) {
            // ok
        }
        int r = prefs1.getInt(
            "org.netbeans.modules.apisupport.project.ui.LibrariesNode$AddModuleDependencyAction",
            -1
        );
        assertEquals("Nothing set", -1, r);
    }
    
    public void testDeleteSubTree() throws Exception {
        assertNotNull("f1", file);
        
        Statistics<?> somestats = Lookup.getDefault().lookup(Help.class);
        assertNotNull("Stat is here", somestats);

        root = DbPreferences.root(file, somestats, em);
        Preferences prefs1 = root.node("subBelow");
        Preferences prefs2 = root.node("subBelow/child2/child3/child4");

        prefs1.putBoolean("ahoj", true);
        prefs2.putInt("kuk", 10);
        
        prefs2.flush();
        
        em.getTransaction().commit();
        em.close();
        em = PersistenceUtils.getInstance().createEntityManager();
        em.getTransaction().begin();
        root = DbPreferences.root(file, somestats, em);
        assertEquals("One node under root: " + Arrays.asList(root.childrenNames()), 1, root.childrenNames().length);

        prefs1 = root.node("subBelow");
        prefs2 = null;

        assertTrue("ahoj is set", prefs1.getBoolean("ahoj", false));
        String[] keys = prefs1.keys();
        assertEquals("one node here: " + Arrays.asList(keys), 1, keys.length);
        
        assertEquals("One node under root", "subBelow", root.childrenNames()[0]);

        prefs1.removeNode();
        prefs1.flush();

        assertEquals("No node under root", 0, root.childrenNames().length);


        prefs1 = root.node("subBelow");
        keys = prefs1.keys();
        assertEquals("ahoj is not set in removed node:" + Arrays.asList(keys), 0, keys.length);
        assertEquals("No node under pref1", 0, prefs1.childrenNames().length);

        prefs2 = root.node("subBelow/child2/child3/child4");
        assertEquals("ahoj is not set in the other prefs", null, prefs2.get("ahoj", null));
        assertEquals("kuk is not set in the other prefs", 0, prefs2.getInt("kuk", 0));
    }
    
    public void testPutInt() throws Exception{
        assertNotNull("f1", file);
        
        Statistics<?> somestats = Lookup.getDefault().lookup(Help.class);
        assertNotNull("Stat is here", somestats);

        root = DbPreferences.root(file, somestats, em);
        for (int i = 0; i<10; i++){
            root.putInt("KEY" + i, i);
        }
        assertEquals("1", root.get("KEY1", null));
        assertEquals(1, root.getInt("KEY1", 0));
        
    }
    
    private Preferences root;

}
