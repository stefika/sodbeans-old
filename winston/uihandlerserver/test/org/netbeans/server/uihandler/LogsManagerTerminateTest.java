/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */

package org.netbeans.server.uihandler;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.junit.Log;

/**
 *
 * @author Jaroslav Tulach
 */
public class LogsManagerTerminateTest extends DatabaseTestCase {
    private LogsManager result;
    private File logs;
    private Logger LOG;
    
    
    public LogsManagerTerminateTest(String testName) {
        super(testName);
    }
    
    @Override
    protected Level logLevel() {
        return Level.FINE;
    }
    
    private File logs() throws IOException {
        File f = new File(getWorkDir(), "logs");
        f.mkdirs();
        return f;
    }
    
    @Override
    protected void setUp() throws Exception {
        super.setUp();
        logsManager.close();
        LOG = Logger.getLogger("test." + getName());
        File toDir = logs();
        for (int i = 400; i < 600; i++) {
            File log = LogsManagerTest.extractResourceAs(toDir, "3.log", "log" + i);
        }
        LOG.info("files created");
    }
    
    public void testWeAreAbleToStopParsingInMiddle() throws Exception {
        CharSequence log = Log.enable("org.netbeans.server.uihandler", Level.INFO);
        
        Log.controlFlow(LogsManager.LOG, LOG, 
            "THREAD: main MSG: about to create manager" +
            "THREAD: pool.* MSG: Processed.*" +
            "THREAD: main MSG: parsing closed" +
            ""
           , 500);
        
        LOG.info("about to create manager");
        result = LogsManager.createManager(logs());
        LOG.info("manager created: " + result);
        Thread.yield();
        LOG.info("yeileded to other thread");
        
        // stop parsing
        result.close();
        LOG.info("parsing closed");
        
        // wait if there are logs, then parsing is likely going on
        int prev = -1;
        for (int i = 0; i < 100; i++) {
            int len = log.length();
            LOG.info("waiting for no logs. Prev: " + prev + " now: " + len);
            if (prev == len) {
                LOG.info("break from loop");
                break;
            }
            prev = len;
            Thread.sleep(100);
        }
        LOG.info("waiting is over, do the test");
        
        if (log.toString().indexOf("550") >= 0) {
            fail("We shall not parse file log550:\n" + log);
        }
    }
    
}
