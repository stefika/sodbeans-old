/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2009 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */
package org.netbeans.server.snapshots;

import java.io.IOException;
import java.io.InputStream;
import javax.persistence.EntityManager;
import org.junit.Test;
import org.netbeans.modules.exceptions.entity.SuspiciousMethodMapping;
import org.netbeans.server.componentsmatch.Component;
import org.netbeans.server.componentsmatch.Matcher;
import org.netbeans.server.uihandler.DatabaseTestCase;

/**
 *
 * @author Jindrich Sedek
 */
public class SnapshotManagerWithMappingTest extends DatabaseTestCase {

    private SnapshotManager sm;

    public SnapshotManagerWithMappingTest(String name) {
        super(name);
    }

    private void setUp(String fileName) throws IOException {
        InputStream is = SnapshotManagerWithMappingTest.class.getResourceAsStream("res/mapping-" + fileName);
        sm = SnapshotManager.loadSnapshot(is);
        is.close();
        assertNotNull(sm);
    }

    @Test
    public void testSuspicousMethodWithoutMapping() throws IOException{
        setUp("snapshot-278271.nps");
        EntityManager em = perUtils.createEntityManager();
        MethodItem mi = sm.getSuspiciousMethodItem(em);
        assertEquals("correct method", "org.openide.nodes.EntrySupport$Default.getArray", mi.getMethodName());
        Component cmp = sm.getComponent(em);
        assertEquals("correct component", new Component("openide", "nodes"), cmp);
        em.getTransaction().begin();
    }

    @Test
    public void testSuspicousMethodWithMapping() throws IOException{
        setUp("snapshot-278271.nps");
        doTest("org.netbeans.modules.websvc.rest.spi.RestSupport.getRestServicesModel", new Component("websvc", "rest"));
    }

    @Test
    public void testSuspicousMethodWithMapping2() throws IOException{
        setUp("snapshot-257279.nps");
        doTest("org.netbeans.modules.websvc.rest.spi.RestSupport.extendClassPath", new Component("websvc", "rest"));
    }

    @Test
    public void testSuspicousMethodWithMapping3() throws IOException{
        setUp("snapshot-275525.nps");
        doTest("org.netbeans.modules.masterfs.filebasedfs.fileobjects.FileObjectFactory.getCachedOnly", new Component("openide", "filesystems"));
    }

    @Test
    public void testSuspicousMethodWithMapping4() throws IOException{
        setUp("snapshot-255494.nps");
        doTest("org.netbeans.api.queries.VisibilityQuery.isVisible", new Component("projects", "code"));
    }

    @Test
    public void testSuspicousMethodWithMapping5() throws IOException{
        setUp("snapshot-277203.nps");
        doTest("org.netbeans.modules.versioning.system.cvss.CvsVisibilityQuery.isHiddenFolder", new Component("cvs", "code"));

    }

    @Test
    public void testSuspicousMethodWithMapping6() throws IOException{
        setUp("snapshot-269341.nps");
        doTest("org.netbeans.modules.versioning.system.cvss.util.Utils.containsMetadata", new Component("cvs", "code"));
    }

    private void doTest(String correctMethod, Component expectedComp){
        EntityManager em = perUtils.createEntityManager();
        em.getTransaction().begin();
        SuspiciousMethodMapping mapping = new SuspiciousMethodMapping("org.openide.nodes.EntrySupport", "org.openide.nodes.Children.callAddNotify");
        setUpInnocents(em);
        em.persist(mapping);
        em.getTransaction().commit();
        em.close();
        Matcher.getDefault().reload();

        em = perUtils.createEntityManager();
        em.getTransaction().begin();
        MethodItem mi = sm.getSuspiciousMethodItem(em);
        assertEquals("correct method", correctMethod, mi.getMethodName());
        Component cmp = sm.getComponent(em);
        assertEquals("correct component", expectedComp, cmp);
        em.getTransaction().commit();
        em.close();
    }
}

