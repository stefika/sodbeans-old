/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */
package org.netbeans.server.snapshots;

import org.netbeans.server.uihandler.*;
import java.io.File;

/**
 *
 * @author Jindrich Sedek
 */
public class SnapshotDuplicatesTest extends DatabaseTestCase {

    private ExceptionsData excData;
    private static Integer reportID = 1;

    public SnapshotDuplicatesTest(String name) {
        super(name);
    }

    public void testNonDuplicates() throws Exception {
        addNext(1, false, 1);
        addNext(2, false, 2);
    }

    public void testRealDuplicates() throws Exception {
        addNext(3, false, 3);
        addNext(4, true, 3);
    }

    public void testNodesLayoutDuplicates() throws Exception {
        addNext(5, false, 5);
        addNext(6, true, 5);
        addNext(7, true, 5);
    }

    public void testInvalidDuplicates() throws Exception {
        addNext(269498, false, 8);
        addNext(271574, false, 9);
    }

    public void testGoToType() throws Exception {
        File dataUploadDir = new File(Utils.getUploadDirPath(Utils.getRootUploadDirPath(), "NB2040811605"));
        File npsUploadDir = new File(Utils.getUploadDirPath(Utils.getSlownessRootUploadDir(), "NB2040811605"));
        File log = LogsManagerTest.extractResourceAs(DbInsertionSlownessTest.class, dataUploadDir, "slownres/gototype.xml", "NB2040811605." +reportID);
        LogsManagerTest.extractResourceAs(DbInsertionSlownessTest.class, npsUploadDir, "slownres/gototype.nps", "NB2040811605." + reportID);
        addNext(log, false, 10);
        
        log = LogsManagerTest.extractResourceAs(DbInsertionSlownessTest.class, dataUploadDir, "slownres/gototype.xml", "NB2040811605." + reportID);
        LogsManagerTest.extractResourceAs(DbInsertionSlownessTest.class, npsUploadDir, "slownres/gototype.nps", "NB2040811605." + reportID);
        addNext(log, true, 10);
    }
    
    private void addNext(int index, boolean duplicate, int id) throws Exception {
        File dataUploadDir = new File(Utils.getUploadDirPath(Utils.getRootUploadDirPath(), "NB2040811605"));
        File log = LogsManagerTest.extractResourceAs(SnapshotDuplicatesTest.class, dataUploadDir, "res/logger" + Integer.toString(index) + ".xml", "NB2040811605." +reportID);
        File npsUploadDir = new File(Utils.getUploadDirPath(Utils.getSlownessRootUploadDir(), "NB2040811605"));
        File npsLog = LogsManagerTest.extractResourceAs(SnapshotDuplicatesTest.class, npsUploadDir, "res/npslog" + Integer.toString(index) + ".nps", "NB2040811605." + reportID);
        addNext(log, duplicate, id);
    }

    private void addNext(File log, boolean duplicate, int id) throws Exception {
        excData = addLog(log, "NB2040811605");
        assertNotNull("MAPA SHOULD BE NULL FOR NO SESSIONS", excData);
        assertEquals("THIS ISSUE SHOULD (NOT) BE A DUPLICATE", duplicate, excData.getReportId() != 0);
        assertEquals("ISSUE SHOULD INCREASE", reportID, excData.getSubmitId());
        if (duplicate) {
            assertAreDuplicates("These issues should be duplicates ", id, excData.getSubmitId().intValue());
        } else {
            assertFalse(excData.getReportId() != 0);
        }
        reportID++;
    }


}
