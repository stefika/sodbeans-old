/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2009 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */
package org.netbeans.server.snapshots;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.net.URISyntaxException;
import java.util.List;
import javax.persistence.EntityManager;
import org.junit.Test;
import org.netbeans.modules.exceptions.entity.InnocentClass;
import org.netbeans.server.componentsmatch.Component;
import org.netbeans.server.componentsmatch.Matcher;
import org.netbeans.server.uihandler.DatabaseTestCase;

/**
 *
 * @author Jindrich Sedek
 */
public class SnapshotManagerTest extends DatabaseTestCase {

    private SnapshotManager sm;

    public SnapshotManagerTest(String name) {
        super(name);
    }

    private void setUp(String fileName) throws IOException {
        InputStream is = SnapshotManagerTest.class.getResourceAsStream(fileName);
        sm = SnapshotManager.loadSnapshot(is);
        is.close();
        assertNotNull(sm);
    }

    @Test
    public void testLoadSnaphost() throws IOException, URISyntaxException {
        setUp("snapshot.nps");
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        sm.printAWTThread(new PrintWriter(new OutputStreamWriter(new PrintStream(bos))));
        String result = bos.toString();
        assertTrue(result, result.contains("AWT-EventQueue-1: 87"));
        assertTrue(result, result.contains("org.openide.nodes.Children.getNodesCount(): 59"));
        assertTrue(result, result.contains("org.openide.nodes.Children$Keys$KE.nodes(): 37"));
    }

    @Test
    public void testLoadNewFormat() throws Exception {
        setUp("newsnapshot.nps");
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        sm.printAWTThread(new PrintWriter(new OutputStreamWriter(new PrintStream(bos))));
        String result = bos.toString();
        assertTrue(result, result.contains("AWT-EventQueue-1: 5479"));
        assertTrue(result, result.contains("java.awt.EventDispatchThread.pumpOneEventForFilters(): 5479"));
        assertTrue(result, result.contains("java.lang.Exception.<init>(): 8"));
    }

    @Test
    public void testSuspicious() throws IOException {
        setUp("snapshot.nps");
        perUtils.persist(new InnocentClass("org.netbeans.core.TimableEventQueue"));
        Matcher.getDefault().reload();
        List<MethodItem> ml = sm.getSuspiciousStackPrefix();
        assertEquals(62, ml.size());
        assertEquals("java.awt.EventDispatchThread.pumpEvents", ml.get(1).getMethodName());
        assertEquals("org.openide.nodes.ChildrenArray.nodesFor", ml.get(ml.size() - 1).getMethodName());
        EntityManager em = perUtils.createEntityManager();
        MethodItem mi = sm.getSuspiciousMethodItem(em);
        assertEquals("org.openide.nodes.ChildrenArray.nodesFor", mi.getMethodName());
        Component co = sm.getComponent(em);
        assertEquals("openide", co.getComponent());
        assertEquals("nodes", co.getSubComponent());
        em.close();
    }

    @Test
    public void testComponent() throws IOException{
        setUp("res/viewmodel_component.nps");
        perUtils.persist(new InnocentClass("org.netbeans.core.TimableEventQueue"));
        Matcher.getDefault().reload();
        EntityManager em = perUtils.createEntityManager();
        em.getTransaction().begin();
        Component comp = sm.getComponent(em);
        em.getTransaction().commit();
        em.close();
        assertEquals("java and loader classes are ignored", new Component("debuggercore", "code"), comp);

        em = perUtils.createEntityManager();
        em.getTransaction().begin();
        em.persist(new InnocentClass("org.openide.filesystems.FileUtil"));
        em.persist(new InnocentClass("URLMapper"));
        em.getTransaction().commit();
        em.close();
        Matcher.getDefault().reload();

        setUp("res/url_mapper.nps");
        em = perUtils.createEntityManager();
        em.getTransaction().begin();
        comp = sm.getComponent(em);
        em.getTransaction().commit();
        em.close();
        assertEquals("java and loader classes are ignored", new Component("editor", "code"), comp);

        setUp("res/heapdump.nps");
        em = perUtils.createEntityManager();
        em.getTransaction().begin();
        comp = sm.getComponent(em);
        em.getTransaction().commit();
        em.close();
        assertEquals("java and loader classes are ignored", new Component("openide", "actions"), comp);
    }

    @Test
    public void testIssue169810() throws IOException{
        setUp("res/issue169810.nps");
        perUtils.persist(new InnocentClass("org.netbeans.core.TimableEventQueue"));
        Matcher.getDefault().reload();
        EntityManager em = perUtils.createEntityManager();
        em.getTransaction().begin();
        Component comp = sm.getComponent(em);
        em.getTransaction().commit();
        em.close();
        assertEquals("java and loader classes are ignored", new Component("logger", "uigestures"), comp);
    }

    @Test
    public void testIgnoredCloneableEditor() throws IOException{
        setUp("res/cloneable_editor.nps");
        perUtils.persist(new InnocentClass("org.openide.text.CloneableEditor"));
        Matcher.getDefault().reload();
        EntityManager em = perUtils.createEntityManager();
        em.getTransaction().begin();
        Component comp = sm.getComponent(em);
        em.getTransaction().commit();
        em.close();
        assertEquals("CloneableEditor classes are ignored", new Component("qa", "Test Tools"), comp);
    }

    @Test
    public void testPrintAllThreads() throws IOException{
        setUp("snapshot.nps");
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        PrintWriter wr = new PrintWriter(new OutputStreamWriter(new PrintStream(bos)));
        sm.printAllThreads(wr);
        wr.close();
        assertTrue(bos.size() > 1024);
    }

    @Test
    public void testVerifyCorrectMethodName() throws IOException{
        setUp("snapshot-233041.nps");
        List<MethodItem> mi = sm.getSuspiciousStackPrefix();
        for (MethodItem methodItem : mi) {
            verifyMethodName(methodItem.getMethodName());
        }
        EntityManager em = perUtils.createEntityManager();
        verifyMethodName(sm.getSuspiciousMethodItem(em).getMethodName());
        em.close();
    }

    @Test
    public void testTwoTimesFormatLog() throws IOException{
        setUp("two_times.nps");
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        PrintWriter wr = new PrintWriter(new OutputStreamWriter(new PrintStream(bos)));
        sm.printAllThreads(wr);
        wr.close();
        assertTrue(bos.size() > 1024);
    }

    @Test
    public void testContainsMethod() throws IOException{
        setUp("two_times.nps");
        assertTrue(sm.AWTcontainsMethod("java.awt.EventQueue."));
        assertTrue(sm.AWTcontainsMethod("java.util.Random.nextInt"));
        assertFalse(sm.AWTcontainsMethod("java.lang.Object.wait"));
        assertFalse(sm.AWTcontainsMethod("org.openide.util"));
    }

    private void verifyMethodName(String name){
        assertFalse(name, name.contains("["));
        assertFalse(name, name.contains("]"));
        assertFalse(name, name.contains("("));
        assertFalse(name, name.contains(")"));
    }
}

