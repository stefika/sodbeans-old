/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */
package org.netbeans.server.componentsmatch;

import java.io.File;
import java.io.IOException;
import javax.persistence.EntityManager;
import org.netbeans.modules.exceptions.entity.InnocentClass;
import org.netbeans.modules.exceptions.entity.Method;
import org.netbeans.modules.exceptions.entity.Sourcejar;
import org.netbeans.modules.exceptions.entity.SourcejarMapping;
import org.netbeans.server.uihandler.DatabaseTestCase;
import org.netbeans.server.uihandler.LogsManagerTest;

/**
 *
 * @author Jindrich Sedek
 */
public class SourcejarMatcherTest extends DatabaseTestCase {

    private EntityManager em;

    /** Creates a new instance of MatcherTest */
    public SourcejarMatcherTest(String name) {
        super(name);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        em = perUtils.createEntityManager();
    }

    @Override
    protected void tearDown() throws Exception {
        em.close();
        super.tearDown();
    }

    public void testClassLoadingClasses() {
        Matcher matcher = new SourcejarMatcher();
        Component comp = matcher.match(em, new Throwable("java.lang.NoClassDefFoundError: org/netbeans/modules/profiler/actions/SelfSamplerAction$Singleton"));
        assertEquals("platform", comp.getComponent());
        assertEquals("Module System", comp.getSubComponent());
    }

    public void testInnocent() throws IOException {
        createMethod(em, "org.openide.util.lookup.InstanceContent$SimpleItem.<init>", "org-openide-util.jar");
        createMethod(em, "org.openide.util.lookup.SimpleLookup.<init>", "org-openide-util.jar");
        createMethod(em, "org.openide.util.Enumerations.<init>", "org-openide-util.jar");
        Matcher matcher = new SourcejarMatcher();

        Component comp = match(matcher, new StackTraceElement[]{
                    new StackTraceElement("org.openide.util.lookup.InstanceContent$SimpleItem", "<init>", "file", 45)
                });
        assertNotNull(comp);//no Innocent

        comp = match(matcher, new StackTraceElement[]{
                    new StackTraceElement("org.openide.util.lookup.SimpleLookup", "<init>", "file", 45)
                });
        assertNotNull(comp);//no Innocent

        perUtils.persist(new InnocentClass("org.openide.util.lookup")); // look up is innocent
        matcher.reload();

        comp = match(matcher, new StackTraceElement[]{
                    new StackTraceElement("org.openide.util.lookup.InstanceContent$SimpleItem", "<init>", "file", 45)
                });
        assertNull(comp);

        comp = match(matcher, new StackTraceElement[]{
                    new StackTraceElement("org.openide.util.lookup.SimpleLookup", "<init>", "file", 45)
                });
        assertNull(comp);

        comp = match(matcher, new StackTraceElement[]{
                    new StackTraceElement("org.openide.util.lookup.Lookups", "<init>", "file", 45)
                });
        assertNull(comp);

        comp = match(matcher, new StackTraceElement[]{
                    new StackTraceElement("org.openide.util.Enumerations", "<init>", "file", 45)
                });
        assertNotNull(comp);
    }

    public void testJarSourcemapping() throws Exception {
        createMethod(em, "org.openide.util.Mutex.doEvent", "org-openide-util.jar");
        createMethod(em, "org.netbeans.visualweb.insync.models.FacesModel.syncImpl", "testingfile");
        createMethod(em, "org.netbeans.modules.visualweb.insync.models.FacesModel.syncImpl", "testingfile.jar");
        Matcher matcher = new SourcejarMatcher();

        File log = LogsManagerTest.extractResourceAs(data, "dupres/nested10586", "NB2040811605");
        addLog(log, "NB2040811605");

        Component comp = match(matcher, new StackTraceElement[]{
                    new StackTraceElement("org.openide.util.Mutex", "doEvent", "file", 45)
                });
        assertNotNull(comp);// mapping

        em.getTransaction().begin();
        SourcejarMapping mapping = em.find(SourcejarMapping.class, "org-openide-util.jar");
        mapping.setComponent("comp1");
        mapping.setSubcomponent("subcomp1");
        em.merge(mapping);
        em.getTransaction().commit();
        matcher.reload();

        comp = match(matcher, new StackTraceElement[]{
                    new StackTraceElement("org.openide.util.Mutex", "doEvent", "file", 45)
                });

        assertEquals("comp1", comp.getComponent());
        assertEquals("subcomp1", comp.getSubComponent());

        comp = match(matcher, new StackTraceElement[]{
                    new StackTraceElement("org.netbeans.visualweb.insync.models.FacesModel", "syncImpl", "file", 45)
                });
        assertNull(comp);
        //----------------------//

        mapping = new SourcejarMapping("testingfile");
        mapping.setComponent("comp2");
        mapping.setSubcomponent("subcomp2");
        perUtils.persist(mapping);
        matcher.reload();


        comp = match(matcher, new StackTraceElement[]{
                    new StackTraceElement("org.openide.util.Mutex", "doEvent", "file", 45)
                });

        assertEquals("comp1", comp.getComponent());
        assertEquals("subcomp1", comp.getSubComponent());

        comp = match(matcher, new StackTraceElement[]{
                    new StackTraceElement("org.netbeans.modules.visualweb.insync.models.FacesModel", "syncImpl", "file", 45)
                });

        assertEquals("comp2", comp.getComponent());
        assertEquals("subcomp2", comp.getSubComponent());

        mapping = new SourcejarMapping("testingfile.jar");
        mapping.setComponent("comp3");
        mapping.setSubcomponent("subcomp3");
        perUtils.persist(mapping);
        matcher.reload();

        comp = match(matcher, new StackTraceElement[]{
                    new StackTraceElement("org.netbeans.modules.visualweb.insync.models.FacesModel", "syncImpl", "file", 45)
                });
        //better fit matching
        assertEquals("comp3", comp.getComponent());
        assertEquals("subcomp3", comp.getSubComponent());

    }

    private Component match(Matcher matcher, StackTraceElement[] stes) {
        return matcher.match(em, stes);
    }

    public void testExceptionWithCause() {
        createMethod(em, "java.lang.String.getName", "rt.jar");
        createMethod(em, "org.openide.ErrorManager.getDefault", "org-openide-util.jar");

        StackTraceElement[] stes = new StackTraceElement[]{
            new StackTraceElement("java.lang.String", "getName", "file", 45),
            new StackTraceElement("org.openide.ErrorManager", "getDefault", "file", 45)
        };

        Throwable cause = new Throwable();
        cause.setStackTrace(stes);
        Throwable thr = new Throwable(cause);

        Component comp = new SourcejarMatcher().match(perUtils.createEntityManager(), thr);
        assertEquals("platform", comp.getComponent());
        assertEquals("-- Other --", comp.getSubComponent());
    }

    public void testIssue153325() throws Exception {
        Matcher matcher = new SourcejarMatcher();
        createMethod(em, "org.netbeans.modules.visualweb.insync.ModelSet.modelSet", "org-netbeans-modules-visualweb-extension-openide.jar");
        createMethod(em, "org.openide.util.Mutex.doEvent", "org-openide-util.jar");
        createMethod(em, "org.openide.util.Parameters.notNull", "org-openide-util.jar");

        File log = LogsManagerTest.extractResourceAs(data, "dupres/nested10586", "NB2040811605");
        addLog(log, "NB2040811605");

        Component comp = match(matcher, new StackTraceElement[]{
                    new StackTraceElement("org.openide.util.Mutex", "doEvent", "file", 45),
                    new StackTraceElement("org.openide.util.Mutex", "doEvent", "file", 45),
                    new StackTraceElement("org.openide.util.Mutex", "doEvent", "file", 45),
                    new StackTraceElement("org.netbeans.modules.visualweb.insync.ModelSet", "modelSet", "file", 45)
                });
        assertNotNull(comp);
        assertFalse("visualweb".equals(comp.getComponent()));


        comp = match(matcher, new StackTraceElement[]{
                    new StackTraceElement("org.openide.util.Parameters", "notNull", "file", 45),
                    new StackTraceElement("org.openide.util.Mutex", "doEvent", "file", 45),
                    new StackTraceElement("org.openide.util.Mutex", "doEvent", "file", 45),
                    new StackTraceElement("org.openide.util.Mutex", "doEvent", "file", 45),
                    new StackTraceElement("org.netbeans.modules.visualweb.insync.ModelSet", "modelSet", "file", 45)
                });
        assertNotNull(comp);
        assertEquals("visualweb", comp.getComponent());
    }

    private void createMethod(EntityManager em, String methodName, String jarName) {
        em.getTransaction().begin();
        Sourcejar sj = em.find(Sourcejar.class, jarName.hashCode());
        if (sj == null) {
            sj = new Sourcejar(jarName.hashCode());
        }
        sj.setName(jarName);
        em.persist(sj);
        Method m = new Method(methodName.hashCode());
        m.setSourcejar(sj);
        m.setName(methodName);
        em.persist(m);
        em.getTransaction().commit();
    }
}
