/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2007 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */
package org.netbeans.server.componentsmatch;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import org.netbeans.modules.exceptions.entity.InnocentClass;
import org.netbeans.modules.exceptions.utils.PersistenceUtils;
import org.openide.util.Exceptions;

/**
 *
 * @author Jindrich Sedek
 */
public class PackageMatcher extends Matcher {

    private List<InnocentClass> innocents;
    private final Map<String, Component> packages = new HashMap<String, Component>();

    public PackageMatcher() {
        reload();
    }

    @Override
    public void reload() {
        try {
            InputStream is = PackageMatcher.class.getResourceAsStream("septemberComponents.txt");
            read(new InputStreamReader(is));
            is.close();
            innocents = PersistenceUtils.getInstance().getAll(InnocentClass.class);
        } catch (IOException ex) {
            Exceptions.printStackTrace(ex);
        }
    }

    @Override
    public Component match(EntityManager em, StackTraceElement[] stes) {
        for (StackTraceElement stackTraceElement : stes) {
            Component comp = match(stackTraceElement);
            if (comp != null) {
                return comp;
            }
        }
        return null;
    }

    @Override
    public Component getRealComponent(EntityManager em, Component expectedComponent) {
        if (expectedComponent == null){
            return Component.UNKNOWN;
        }
        return expectedComponent;
    }

    public Component match(StackTraceElement ste) {
        String className = ste.getClassName();
        if (isInnocent(className)){
            return null;
        }
        int index = 0;
        while ((index = className.indexOf('.', index)) != -1) {
            if (index + 1 < className.length()) {
                if (Character.isUpperCase(className.charAt(index + 1))) {
                    break;
                }
            }
            index++;
        }
        if (index == -1) {
            return null;
        }
        String packageName = className.substring(0, index);
        Component comp = null;
        do {
            comp = packages.get(packageName);
            int lastDot = packageName.lastIndexOf('.');
            if (lastDot == -1) {
                packageName = null;
            } else {
                packageName = packageName.substring(0, lastDot);
            }
        } while (comp == null && packageName != null);
        return comp;
    }

    private void read(Reader r) throws IOException {
        BufferedReader reader = new BufferedReader(r);
        String line = null;
        //org.openide.util.datatransfer;1;openide;nodes;1;
        while ((line = reader.readLine()) != null) {
            try {
                StringTokenizer tokenizer = new StringTokenizer(line, ";");
                String packageName = tokenizer.nextToken();
                if (packageName != null) {
                    // skip all count
                    tokenizer.nextToken();
                    String component = tokenizer.nextToken();
                    String subComponent = tokenizer.nextToken();
                    Integer compCount = Integer.valueOf(tokenizer.nextToken());
                    packages.put(packageName, new Component(component, subComponent));
                }
            } catch (NumberFormatException nfe) {
                Logger.getLogger(Matcher.class.getName()).log(Level.WARNING, "Incorrect syntax on line " + line, nfe);
            }
        }
        reader.close();
    }

    private boolean isInnocent(String className) {
        for (InnocentClass innocent : innocents) {
            if (className.contains(innocent.getClassname())) {
                return true;
            }
        }
        return false;
    }
}
