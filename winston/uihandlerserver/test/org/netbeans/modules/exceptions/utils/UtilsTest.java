/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */

package org.netbeans.modules.exceptions.utils;

import org.netbeans.modules.exceptions.entity.Nbversion;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author honza
 */
public class UtilsTest {

    @Test
    public void testGetBuildNumber() {
        System.out.println("getBuildNumber");
        testBuildNumber("NetBeans IDE Dev (Build 200702191730)  9879jhgjhghjg", "200702191730");
        testBuildNumber("NetBeans IDE Dev (Build nbms-and-javadoc-4010-on-090930)", "090930");
        testBuildNumber("NetBeans IDE Dev (Build cnd-main-1866-on-091009)", "091009");
        testBuildNumber("NetBeans IDE 6.8 M2 (Build 091010)", "091010");
        testBuildNumber("NetBeans Platform 6.8 Beta (Build 200909281634)", "200909281634");
        testBuildNumber("NetBeans IDE Dev (Build 2010-04-28_15-04-16 )", "20100428");
    }

    private void testBuildNumber(String line, String expectedResult){
        String result = Utils.getBuildNumber(line);
        assertEquals(expectedResult, result);
    }

    @Test
    public void testGetCustomBuildFormat() {
        System.out.println("getCustomBuildFormat");
        String result = Utils.getCustomBuildFormat("200702191730");
        assertEquals("070219", result);

        assertEquals("100428", Utils.getCustomBuildFormat("20100428"));
    }
    
    @Test
    public void testGetNbVersion(){
        String productVersion="NetBeans IDE Dev (Build 20071106001900)";
        String expectedResult="Dev";
        assertEquals(expectedResult,Utils.getNbVersion(productVersion));
        
        productVersion="NetBeans IDE Dev (Build 071031)";
        expectedResult="Dev";
        assertEquals(expectedResult,Utils.getNbVersion(productVersion));

        productVersion=" NetBeans Ruby IDE 20071102114828";
        expectedResult="NetBeans Ruby IDE";
        assertEquals(expectedResult,Utils.getNbVersion(productVersion));
        
        productVersion="NetBeans Ruby IDE 071030";
        expectedResult="NetBeans Ruby IDE";
        assertEquals(expectedResult,Utils.getNbVersion(productVersion));

        productVersion="SQLBrowser 200709141330";
        expectedResult="SQLBrowser";
        assertEquals(expectedResult,Utils.getNbVersion(productVersion));

        productVersion="ErlyBird 070920";
        expectedResult="ErlyBird";
        assertEquals(expectedResult,Utils.getNbVersion(productVersion));

    }

    public void testGetVersionNumber() {
        Nbversion nbv = new Nbversion();
        nbv.setVersion(Utils.getNbVersion("NetBeans IDE 6.0 beta"));
        String result = Utils.getBugzillaVersion(nbv);
        assertEquals("6.0", result);
        nbv.setVersion(Utils.getNbVersion("NetBeans IDE 6.0"));
        result = Utils.getBugzillaVersion(nbv);
        assertEquals(result, "6.0", result);
        nbv.setVersion(Utils.getNbVersion("NetBeans IDE 6.0.1 RC1"));
        result = Utils.getBugzillaVersion(nbv);
        assertEquals("6.0.1", result);
        nbv.setVersion(Utils.getNbVersion("NetBeans IDE 6.5.1"));
        result = Utils.getBugzillaVersion(nbv);
        assertEquals("6.5", result);
        nbv.setVersion(Utils.getNbVersion("NetBeans IDE 6.5"));
        result = Utils.getBugzillaVersion(nbv);
        assertEquals("6.5", result);
        nbv.setVersion(Utils.getNbVersion("NetBeans IDE 6.7"));
        result = Utils.getBugzillaVersion(nbv);
        assertEquals("6.7", result);
        nbv.setVersion(Utils.getNbVersion("NetBeans IDE Dev"));
        result = Utils.getBugzillaVersion(nbv);
        assertEquals(Nbversion.DEFAULT_VERSION, result);
    }

}
