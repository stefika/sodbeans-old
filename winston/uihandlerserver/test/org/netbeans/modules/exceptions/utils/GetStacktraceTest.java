/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */
package org.netbeans.modules.exceptions.utils;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.persistence.EntityManager;
import org.junit.Test;
import org.netbeans.modules.exceptions.entity.Exceptions;
import org.netbeans.modules.exceptions.entity.Line;
import org.netbeans.modules.exceptions.entity.Method;
import org.netbeans.modules.exceptions.entity.Stacktrace;
import org.netbeans.server.uihandler.DatabaseTestCase;
import org.netbeans.server.uihandler.LogsManagerTest;

/**
 *
 * @author Jindrich Sedek
 */
public class GetStacktraceTest extends DatabaseTestCase {

    public GetStacktraceTest(String name) {
        super(name);
    }

    public void testGetStacktraceLines() throws Exception {
        File log = LogsManagerTest.extractResourceAs(data, "dupres/nested10586", "NB2040811605");
        waitLog(log, "112.15.14.13");
        EntityManager em = perUtils.createEntityManager();
        em.getTransaction().begin();
        List<Exceptions> excs = PersistenceUtils.getAll(em, Exceptions.class);
        assertFalse("an exception was already reported", excs.isEmpty());
        Exceptions exc = excs.iterator().next();
        String delimiter = ":::delim:::";
        String str = Utils.getStacktraceLines(exc, 5, delimiter);
        assertTrue(str, str.contains("NullPointerException" + delimiter));
        assertTrue(str, str.contains("org.netbeans.modules.visualweb.insync.models.FacesConfigModel.isBusted"));
        assertTrue(str, str.contains(")" + delimiter));
        em.getTransaction().commit();
        em.close();
    }

    public void testIssue165149() throws Exception {
        File log = LogsManagerTest.extractResourceAs(data, "dupres/issue165455", "NB2040811605.1");
        waitLog(log, "112.15.14.13");
        EntityManager em = perUtils.createEntityManager();
        em.getTransaction().begin();
        List<Exceptions> excs = PersistenceUtils.getAll(em, Exceptions.class);
        assertFalse(excs.isEmpty());
        Exceptions exc = excs.iterator().next();
        String str = Utils.getStacktraceLines(exc, 5, "");
        assertFalse(str.contains("org.openide.util.RequestProcessor$Item"));
        assertTrue(str.contains("org.netbeans.spi.project.support.ant.SourcesHelper.addPrincipalSourceRoot"));
        em.getTransaction().commit();
        em.close();
    }

    @Test
    public void testGetStacktraceLinesSimple(){
        Exceptions exc = new Exceptions(1);
        Stacktrace st = new Stacktrace(1);
        Stacktrace st2 = new Stacktrace(2);
        exc.setStacktrace(st);
        st.setAnnotation(st2);
        st.setMessage("st");
        st.setLineCollection(Collections.<Line>emptyList());
        Method m1 = new Method(1);
        m1.setName("test1");
        Method m2 = new Method(2);
        m2.setName("test2");
        Line l1 = new Line(1,1,3,1);
        Line l2 = new Line(1,1,3,2);
        Line l3 = new Line(1,1,3,3);
        Line l4 = new Line(1,1,3,4);
        l1.setMethod(m1);
        l2.setMethod(m2);
        l3.setMethod(m1);
        l4.setMethod(m2);
        List<Line> lines = new ArrayList<Line>();
        lines.add(l1);
        lines.add(l2);
        lines.add(l3);
        lines.add(l4);
        st2.setMessage("st2");
        st2.setLineCollection(lines);
        String result = Utils.getStacktraceLines(exc, 5,"");
        assertTrue(result.contains("test1("));
        assertTrue(result.contains("test2("));
    }

}
