/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 2009 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 *
 * Contributor(s):
 *
 * Portions Copyrighted 2009 Sun Microsystems, Inc.
 */
package org.netbeans.modules.exceptions.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;
import java.util.logging.Handler;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import javax.persistence.EntityManager;
import org.junit.Test;
import org.netbeans.modules.exceptions.entity.Exceptions;
import org.netbeans.modules.exceptions.entity.Report;
import org.netbeans.modules.exceptions.entity.ReportComment;
import org.netbeans.modules.exceptions.entity.Submit;
import org.netbeans.server.services.beans.Mappings;
import org.netbeans.server.uihandler.DatabaseTestCase;
import org.netbeans.server.uihandler.api.Issue;
import org.netbeans.server.uihandler.bugs.BugzillaConnector;

/**
 *
 * @author Jindrich Sedek
 */
public class SynchronizeIssuesTest extends DatabaseTestCase {

    private int ISSUE_ID = 10;
    private List<String> messages;
    public SynchronizeIssuesTest(String name) {
        super(name);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        messages = new ArrayList<String>();
        Handler loggingHandler = new Handler() {

            @Override
            public void publish(LogRecord record) {
                messages.add(new SimpleFormatter().formatMessage(record));
            }

            @Override
            public void flush() {
                messages.clear();
            }

            @Override
            public void close() throws SecurityException {
                messages = null;
            }
        };
        SynchronizeIssues.SYNCHRONIZATION_LOGGER.addHandler(loggingHandler);
    }


    @Test
    public void testMergeReportsWithOneIssueId() {
        EntityManager em = perUtils.createEntityManager();
        em.getTransaction().begin();
        Report r1 = new Report(1);
        Report r2 = new Report(2);
        Report r3 = new Report(3);
        Exceptions exc1 = new Exceptions(1);
        Exceptions exc2 = new Exceptions(2);
        Exceptions exc3 = new Exceptions(3);
        exc1.setReportId(r1);
        exc2.setReportId(r2);
        exc3.setReportId(r3);
        r1.setIssueId(ISSUE_ID);
        r2.setIssueId(ISSUE_ID);
        r3.setIssueId(ISSUE_ID);
        em.persist(r1);
        em.persist(r2);
        em.persist(r3);
        em.persist(exc1);
        em.persist(exc2);
        em.persist(exc3);
        em.getTransaction().commit();
        em.close();

        BugzillaReporter br = BugReporterFactory.getDefaultReporter();
        Issue iss = br.getIssue(ISSUE_ID);
        iss.setReporterSubmitId(2);

        SynchronizeIssues instance = new SynchronizeIssues();
        instance.mergeReportsWithOneIssueId();

        em = perUtils.createEntityManager();
        em.getTransaction().begin();

        verifyMergedReport(em.find(Report.class, 1));
        verifyMergedReport(em.find(Report.class, 3));

        r2 = em.find(Report.class, 2);
        assertFalse(r2.getSubmitCollection().isEmpty());
        List<Submit> excs = r2.getSubmitCollection();
        assertTrue(excs.contains(exc1));
        assertTrue(excs.contains(exc2));
        assertTrue(excs.contains(exc3));

        em.getTransaction().commit();
        em.close();

        StringBuilder concat = new StringBuilder();
        for (String string : messages) {
            concat.append(string);
        }
        assertFalse("messages should not be empty", messages.isEmpty());
        assertTrue(concat.toString(), messages.contains("merging report 1 to report 2"));
        assertTrue(concat.toString(), messages.contains("merging report 3 to report 2"));
    }

    private void verifyMergedReport(Report r) {
        assertTrue(r.getSubmitCollection().isEmpty());
        assertEquals(1, r.getCommentCollection().size());
        ReportComment rc = r.getCommentCollection().iterator().next();
        assertTrue(rc.getComment(), rc.getComment().contains("This report was marked "
                + "as a duplicate of <a href='http://statistics.netbeans.org/exceptions/detail.do?id=2'>report 2</a>"));
    }

    public void testUpdateMappings() {
        BugzillaConnector.setTestInstance(new TestBZConnector());
        EntityManager em = perUtils.createEntityManager();
        em.getTransaction().begin();
        Report r = new Report(146285);
        r.setIssueId(null);
        em.persist(r);
        r = new Report(146264);
        r.setIssueId(100);
        em.persist(r);
        r = new Report(2081);
        r.setIssueId(157884);
        em.persist(r);

        em.getTransaction().commit();
        em.close();

        new SynchronizeIssues().updateMappings();
        assertTrue(messages.contains("updating mappings"));
        assertFalse("Was changed to possible duplicates", messages.contains("updating report 146264 from 100 to 158498"));
        assertTrue(messages.contains("updating report 146285 from null to 158523"));
        assertFalse(messages.contains("updating report 2081"));

        em = perUtils.createEntityManager();
        em.getTransaction().begin();
        assertEquals(100, em.find(Report.class, 146264).getIssueId().intValue());
        assertEquals(158523, em.find(Report.class, 146285).getIssueId().intValue());
        assertEquals(157884, em.find(Report.class, 2081).getIssueId().intValue());
        em.getTransaction().commit();
        em.close();
    }

    public void testCleanInTransfer(){
        SynchronizeIssues is = new SynchronizeIssues();
        BugzillaConnector.setTestInstance(new TestBZConnector());
        EntityManager em = perUtils.createEntityManager();
        em.getTransaction().begin();
        Report r = new Report(146285);
        r.setIssueId(null);
        r.setIssuezillaTransfer(true);
        em.persist(r);
        r = new Report(146264);
        r.setIssueId(1214);
        r.setIssuezillaTransfer(true);
        em.persist(r);
        r = new Report(500);
        r.setIssueId(null);
        r.setIssuezillaTransfer(true);
        em.persist(r);
        em.getTransaction().commit();
        em.close();

        is.updateInTransferReports();

        em = perUtils.createEntityManager();
        em.getTransaction().begin();
        assertFalse(em.find(Report.class, 146285).inIssuezillaTransfer());
        assertFalse(em.find(Report.class, 146264).inIssuezillaTransfer());
        assertTrue(em.find(Report.class, 500).inIssuezillaTransfer());

        assertEquals(1214, em.find(Report.class, 146264).getIssueId().intValue());
        assertEquals(158523, em.find(Report.class, 146285).getIssueId().intValue());
        assertNull(em.find(Report.class, 500).getIssueId());
        em.getTransaction().commit();
        em.close();

        is.updateInTransferReports();

        em = perUtils.createEntityManager();
        em.getTransaction().begin();
        assertFalse(em.find(Report.class, 500).inIssuezillaTransfer());
        assertNull(em.find(Report.class, 500).getIssueId());
        em.getTransaction().commit();
        em.close();
    }

    private class TestBZConnector extends BugzillaConnector{

        @Override
        public Mappings getMappings() {
            TreeMap<Integer, Integer> tmp = new TreeMap<Integer, Integer>();
            tmp.put(158498, 146264);
            tmp.put(158523, 146285);
            tmp.put(157884, 2081);
            return new Mappings(tmp);
        }

    }
}
