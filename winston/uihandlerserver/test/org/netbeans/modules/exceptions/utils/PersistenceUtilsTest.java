/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */

package org.netbeans.modules.exceptions.utils;

import java.io.IOException;
import java.util.EnumSet;
import java.util.Iterator;
import java.util.List;
import javax.persistence.EntityManager;
import org.junit.Test;
import org.netbeans.junit.MockServices;
import org.netbeans.junit.NbTestCase;
import org.netbeans.modules.exceptions.entity.Comment;
import org.netbeans.modules.exceptions.entity.Exceptions;
import org.netbeans.modules.exceptions.entity.Jarfile;
import org.netbeans.modules.exceptions.entity.Report;
import org.netbeans.server.uihandler.api.Issue;
import static org.junit.Assert.*;

/**
 *
 * @author pblaha
 */
public class PersistenceUtilsTest extends NbTestCase {
    
    protected PersistenceUtils persUtil;
    /**
     *
     */
    public PersistenceUtilsTest(String name) throws Exception {
        super(name);
        try {
            PersistenceUtils.initTestPersistanceUtils(getWorkDir());
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        persUtil = PersistenceUtils.getInstance();
    }

    /**
     *
     */
    @Test
    public void testSingleton() {
        System.out.println("testSingleton");
        PersistenceUtils in1 = PersistenceUtils.getInstance();
        PersistenceUtils in2 = PersistenceUtils.getInstance();
        assertSame(in2, in1);
        
        List<Jarfile> jars = persUtil.getAll(Jarfile.class);
        
        for(Jarfile file : jars) {
            System.out.println(file.getName());
        }
    }
    
    /**
     *
     */
    @Test
    public void testInsertExceptions() {
        String[] summarys ={"test summary 1", "duplicate of 1", "test summary 3"};
        EnumSet<Report.Status> set = Report.Status.getEnumSet(new Integer(10));
        for (Iterator<Report.Status> it = set.iterator(); it.hasNext();) {
            Report.Status status = it.next();
            System.out.println(">> " + status.name());
        }
        assertEquals("EXCEPTION STATUS", new Integer(10), Report.Status.getInteger(set));
        
        
        System.out.println("testInsertExceptions");
        Exceptions exc = new Exceptions();
        Report report = new Report();
        report.setId(1);
        exc.setId(1);
        exc.setSummary(summarys[0]);
        report.setComponent("comp1");
        report.setSubcomponent("subcomp1");
        exc.setReportId(report);
        
        Comment comment = new Comment();
        comment.setComment("comment 1");
        
        persUtil.persist(report);
        persUtil.persist(exc);
        
        exc = new Exceptions();
        report = new Report();
        report.setId(2);
        exc.setId(2);
        exc.setSummary(summarys[1]);
        report.setComponent("comp1");
        report.setSubcomponent("subcomp1");
        exc.setReportId(report);
        comment = new Comment();
        comment.generateId();
        comment.setComment("comment 2");
        
        persUtil.persist(report);
        persUtil.persist(exc);
        
        exc = new Exceptions();
        report = new Report();
        report.setId(3);
        exc.setId(new Integer(3));
        exc.setSummary(summarys[2]);
        report.setComponent("comp2");
        report.setSubcomponent("subcomp2");
        exc.setReportId(report);
        comment = new Comment();
        comment.setComment("comment 3");
        comment.generateId();
        persUtil.persist(report);
        persUtil.persist(exc);
        
        
        List<Exceptions> excList = persUtil.getAll(Exceptions.class);
        
        
        for(int i = 0;i<3;i++) {
            assertEquals(summarys[i], excList.get(i).getSummary());
        }
        
        assertEquals("COUNT", 3, persUtil.count(Exceptions.class));
        
        assertEquals("MAX ID", 3, persUtil.count(Exceptions.class));
        
    }
    
    public void testFindExceptionCandidates(){
        String mainSummary = "SUMMARY";
        String secondSummary = "SECOND SUMMARY";
        Exceptions except = new Exceptions();
        Report report = new Report(5);
        except.setReportId(report);
        report.setIssueId(0);
        except.setId(5);
        except.setSummary(mainSummary);
        persUtil.persist(report);
        persUtil.persist(except);
        for (int i = 0; i < 9; i++){
            Exceptions exc2 = new Exceptions();
            exc2.setId(i+6);
            exc2.setSummary(secondSummary);
            exc2.setReportId(report);
            persUtil.persist(exc2);
        }
        List<Report> result = persUtil.findExceptionCandidates(9);
        assertEquals("NUMBER OF CANDIDATES", 1, result.size());
//        assertEquals("CANDIDATE ISSUE", mainSummary, result.get(0).getSummary());
        result = persUtil.findExceptionCandidates(10);
        assertEquals("NUMBER OF CANDIDATES", 0, result.size());
        
        Exceptions exc2 = new Exceptions();
        exc2.setId(20);
        exc2.setSummary(secondSummary);
        exc2.setReportId(report);
        persUtil.persist(exc2);
        result = persUtil.findExceptionCandidates(10);
        assertEquals("NUMBER OF CANDIDATES", 1, result.size());
        
    }

    @Test
    public void testIssueIsOpen(){
        Issue issue = new Issue();
        issue.setIssueStatus("NEW");
        assertTrue(issue.isOpen());
        issue.setIssueStatus("CLOSED");
        assertFalse(issue.isOpen());
        issue.setIssueStatus("STARTED");
        assertTrue(issue.isOpen());
        issue.setIssueStatus("VERIFIED");
        assertFalse(issue.isOpen());
        issue.setIssueStatus("RESOLVED");
        assertFalse(issue.isOpen());
        issue.setResolution("FIXED");
        assertFalse(issue.isOpen());
    }

    @Test
    public void testIssueIsFixed(){
        Issue issue = new Issue();
        issue.setIssueStatus("NEW");
        assertFalse(issue.isFixed());

        issue.setIssueStatus("RESOLVED");
        issue.setResolution("WONTFIX");
        assertFalse(issue.isFixed());

        issue.setResolution("INVALID");
        assertFalse(issue.isFixed());

        issue.setResolution("WORKSFORME");
        assertFalse(issue.isFixed());

        issue.setResolution("FIXED");
        assertTrue(issue.isFixed());

        issue.setResolution("LATER");
        assertFalse(issue.isFixed());
    }
    
    @Test
    public void testIssueIsWorksForMe(){
        Issue issue = new Issue();
        issue.setIssueStatus("NEW");
        assertFalse(issue.isWorksForMe());

        issue.setIssueStatus("RESOLVED");
        issue.setResolution("WONTFIX");
        assertFalse(issue.isWorksForMe());

        issue.setResolution("INVALID");
        assertFalse(issue.isWorksForMe());

        issue.setResolution("WORKSFORME");
        assertTrue(issue.isWorksForMe());

        issue.setResolution("FIXED");
        assertFalse(issue.isWorksForMe());

        issue.setResolution("LATER");
        assertFalse(issue.isWorksForMe());
    }

    @Test
    public void testExceptionsIsOpen(){
        MockServices.setServices(LogReporter.class);
        EntityManager em = persUtil.createEntityManager();
        em.getTransaction().begin();

        Exceptions exc = new Exceptions(100);
        Report report = new Report(100);
        exc.setReportId(report);
        assertTrue(PersistenceUtils.exceptionIsOpen(exc));

        report.setIssueId(0);
        assertTrue(PersistenceUtils.exceptionIsOpen(exc));

        Issue issue = BugReporterFactory.getDefaultReporter().getIssue(10);
        report.setIssueId(issue.getId());
        em.persist(report);
        em.persist(exc);
        assertTrue(PersistenceUtils.exceptionIsOpen(exc));

        issue.setIssueStatus("RESOLVED");
        assertFalse(PersistenceUtils.exceptionIsOpen(exc));

        em.getTransaction().commit();
        em.close();
    }
}
