/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2009 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */
package org.netbeans.modules.exceptions.utils;

import java.io.IOException;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.util.Random;
import org.junit.Test;
import org.netbeans.server.uihandler.bugs.ReporterUtils;
import org.openide.util.RequestProcessor;
import org.openide.util.RequestProcessor.Task;
import static org.junit.Assert.*;

/**
 *
 * @author Jindrich Sedek
 */
public class HttpUtilsTest implements Runnable {

    private PipedOutputStream pos;
    private PipedInputStream pis;
    private static String hallo = new String("HALLO!@#$%");
    private String result = null;
    private String encoding;


    @Test
    public void testReadUTF8() throws Exception {
        for (int i = 0; i < 100; i++) {
            testReadPagePrefix("UTF-8", i);
        }
    }

    @Test
    public void testReadISO() throws Exception {
        for (int i = 0; i < 100; i++) {
            testReadPagePrefix("ISO-8859-1", i);
        }
    }

    @Test
    public void testReadISO2() throws Exception {
        for (int i = 0; i < 100; i++) {
            testReadPagePrefix("ISO-8859-2", i);
        }
    }

    private void testReadPagePrefix(String encoding, int iterations) throws Exception {
        this.encoding = encoding;
        pis = new PipedInputStream();
        pos = new PipedOutputStream();
        pos.connect(pis);
        Task t = RequestProcessor.getDefault().post(this);
        Random r = new Random();
        int all = 0;
        for (int i = 0; i < iterations; i++) {
            int size = r.nextInt(100);
            String text = "";
            for (int j = 0; j < size; j++) {
                text = text.concat(hallo);
            }
            all += size * 10;
            pos.write(text.getBytes(encoding));
        }
        pos.close();
        t.waitFinished(10000);
        assertNotNull(result);
        int expected = Math.min(all, ReporterUtils.MAX_PREFIX_LEN);
        assertEquals(expected, result.length());
    }

    public void run() {
        try {
            result = ReporterUtils.readPagePrefix(pis, encoding);
        } catch (IOException ex) {
            ex.printStackTrace(System.err);
        }

    }
}

