/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 2009 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 *
 * Contributor(s):
 *
 * Portions Copyrighted 2009 Sun Microsystems, Inc.
 */
package org.netbeans.modules.exceptions.bugs;

import org.netbeans.server.uihandler.bugs.ReporterUtils;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.apache.commons.httpclient.NameValuePair;
import org.netbeans.junit.NbTestCase;
import org.netbeans.server.uihandler.api.Submit;

/**
 *
 * @author Jan Horvath
 */
public class ReporterUtilsTest extends NbTestCase {

    public ReporterUtilsTest(String testName) {
        super(testName);
    }

    public void testSetIssueComment() {
        List<NameValuePair> dat = new ArrayList<NameValuePair> ();

        ReporterUtils.setFirstComment(dat, createSubmit(), null);

        NameValuePair pair = dat.get(0);

        assertEquals("comment", pair.getName());
        String comment = pair.getValue();
        assertTrue(comment, comment.contains("test1"));
        assertTrue(comment, comment.contains("test2"));
        assertTrue(comment, comment.contains("test3"));

        dat = new ArrayList<NameValuePair> ();
        MockSubmit ms = new MockSubmit();
        ms.setCommentString(null);
        ReporterUtils.setFirstComment(dat, ms, null);

        pair = dat.get(0);
        comment = pair.getValue();

        assertFalse(comment, comment.contains("User Comments:"));

    }

    public void testSetIssueCommentWithMessage() {
        List<NameValuePair> pairData = new ArrayList<NameValuePair> ();

        ReporterUtils.setFirstComment(pairData, createSubmit(), "additional message");

        NameValuePair pair = pairData.get(0);

        assertEquals("comment", pair.getName());
        String comment = pair.getValue();
        assertTrue(comment, comment.contains("additional message"));
        assertTrue(comment, comment.contains("test2"));
        assertTrue(comment, comment.contains("test3"));

        pairData = new ArrayList<NameValuePair> ();
        MockSubmit submit = new MockSubmit();
        submit.setCommentString(null);
        ReporterUtils.setFirstComment(pairData, submit, "additional message");

        pair = pairData.get(0);
        comment = pair.getValue();

        assertFalse(comment, comment.contains("User Comments:"));
        assertTrue(comment, comment.contains("additional message"));
    }

    private Submit createSubmit(){
        MockSubmit mockSubmit = new MockSubmit();
        mockSubmit.setCommentString("test1");
        MockSubmit dup1 = new MockSubmit();
        dup1.setCommentString("test2");
        MockSubmit dup2 = new MockSubmit();
        dup2.setCommentString("test3");
        List<Submit> list = new ArrayList<Submit>();
        list.add(dup1);
        list.add(dup2);
        mockSubmit.setDuplicates(list);
        return mockSubmit;
    }


    public void testPrepareMessagePrefixFromReporter() {
        MockSubmit sbm = prepareDuplicatesData();
        assertNotNull(ReporterUtils.prepareMessagePrefix(sbm));
        assertEquals("", ReporterUtils.prepareMessagePrefix(sbm));
        sbm.setReporter(new Submit.Reporter("user1", true, "cat"));

        assertNotNull(ReporterUtils.prepareMessagePrefix(sbm));
        assertEquals("cat ", ReporterUtils.prepareMessagePrefix(sbm));
    }

    public void testPrepareMessagePrefixFromDuplicates() {
        MockSubmit sbm = prepareDuplicatesData();
        assertNotNull(ReporterUtils.prepareMessagePrefix(sbm));
        assertEquals("", ReporterUtils.prepareMessagePrefix(sbm));

        MockSubmit dup1 = (MockSubmit) sbm.getDuplicates().iterator().next();
        dup1.setReporter(new Submit.Reporter("user2", true, "cat"));

        assertNotNull(ReporterUtils.prepareMessagePrefix(sbm));
        assertEquals("cat ", ReporterUtils.prepareMessagePrefix(sbm));
    }

    public void testPrepareWithEmptyMessagePrefix() {
        Submit sbm = prepareDuplicatesData();
        assertNotNull(ReporterUtils.prepareMessagePrefix(sbm));
        assertEquals("", ReporterUtils.prepareMessagePrefix(sbm));

        MockSubmit dup1 = (MockSubmit) sbm.getDuplicates().iterator().next();
        dup1.setReporter(new Submit.Reporter("user2", true, ""));

        assertNotNull(ReporterUtils.prepareMessagePrefix(sbm));
        assertEquals("Ignore empty prefix", "", ReporterUtils.prepareMessagePrefix(sbm));

        List<Submit> duplicates = (List)sbm.getDuplicates();
        MockSubmit du = (MockSubmit) duplicates.get(duplicates.size() - 1);
        du.setReporter(new Submit.Reporter("user9", true, "cat"));

        assertNotNull(ReporterUtils.prepareMessagePrefix(sbm));
        assertEquals("cat ", ReporterUtils.prepareMessagePrefix(sbm));
    }

    public void testPrepareWithNullMessagePrefix() {
        Submit sbm = prepareDuplicatesData();
        assertNotNull(ReporterUtils.prepareMessagePrefix(sbm));
        assertEquals("", ReporterUtils.prepareMessagePrefix(sbm));

        MockSubmit dup1 = (MockSubmit) sbm.getDuplicates().iterator().next();
        dup1.setReporter(new Submit.Reporter("user2", true, null));

        assertNotNull(ReporterUtils.prepareMessagePrefix(sbm));
        assertEquals("Ignore empty prefix", "", ReporterUtils.prepareMessagePrefix(sbm));
    }

    public void testGetCCUsersList() {
        Submit sbm = prepareDuplicatesData();

        String result = ReporterUtils.getCCUsersList(sbm, null);
        assertNotNull(result);
        assertFalse("reporter should not be on the list: " + result, result.contains("user1@testnetbeans.org"));

        result = ReporterUtils.getCCUsersList(sbm, Collections.singleton("tester"));
        assertNotNull(result);
        assertTrue(result, result.contains("tester@testnetbeans.org"));
        assertTrue(result, result.contains("user3@testnetbeans.org"));
        assertTrue(result, result.contains("user4@testnetbeans.org"));
        assertTrue(result, result.contains("user6@testnetbeans.org"));

        result = ReporterUtils.getCCUsersList(sbm, null);
        assertNotNull(result);
        assertFalse(result, result.contains("tester@testnetbeans.org"));
        assertTrue(result, result.contains("user6@testnetbeans.org"));

        result = ReporterUtils.getCCUsersList(sbm, null);
        assertNotNull(result);
        assertFalse(result, result.contains("user1@testnetbeans.org"));
        assertFalse(result, result.contains("tester@testnetbeans.org"));
        assertTrue(result, result.contains("user3@testnetbeans.org"));
        assertTrue(result, result.contains("user4@testnetbeans.org"));

        result = ReporterUtils.getCCUsersList(sbm, Collections.singleton("GUEST"));
        assertNotNull(result);
        assertFalse(result, result.contains("GUEST@testnetbeans.org"));
        assertFalse(result, result.contains("user1@testnetbeans.org"));
        assertFalse(result, result.contains("tester@testnetbeans.org"));
        assertTrue(result, result.contains("user3@testnetbeans.org"));
        assertTrue(result, result.contains("user4@testnetbeans.org"));

        Set<String> extracc = new HashSet<String>();
        extracc.add("GUEST");
        extracc.add("tester");
        extracc.add("tester2");
        result = ReporterUtils.getCCUsersList(sbm, extracc);
        assertNotNull(result);
        assertFalse(result, result.contains("GUEST@testnetbeans.org"));
        assertFalse(result, result.contains("user1@testnetbeans.org"));
        assertTrue(result, result.contains("tester@testnetbeans.org"));
        assertTrue(result, result.contains("tester2@testnetbeans.org"));
        assertTrue(result, result.contains("user3@testnetbeans.org"));
        assertTrue(result, result.contains("user4@testnetbeans.org"));
    }

    private MockSubmit prepareDuplicatesData() {
        MockSubmit root = null;
        root = new MockSubmit();
        root.setReporter(new Submit.Reporter("user" + 1, false, null));
        List<Submit> duplicates = new ArrayList<Submit>();
        for (int i = 2; i < 10; i++) {
            MockSubmit ms = new MockSubmit();
            ms.setReporter(new Submit.Reporter("user" + i, false, null));
            duplicates.add(ms);
        }
        root.setDuplicates(duplicates);
        return root;
    }
}
