/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 2010 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 *
 * Contributor(s):
 *
 * Portions Copyrighted 2010 Sun Microsystems, Inc.
 */

package org.netbeans.modules.exceptions.bugs;

import java.util.Collection;
import java.util.Collections;
import org.netbeans.server.uihandler.api.Submit;

/**
 *
 * @author Jindrich Sedek
 */
public class MockSubmit implements Submit {

    private String commentString = "Hallo World!";
    private Collection<Submit> duplicates = Collections.EMPTY_SET;
    boolean inBugzilla = false;
    int bugzillaId = 0;
    Reporter reporter = new Reporter("tonda", false, null);
    public boolean isInBugzilla() {
        return inBugzilla;
    }

    public int getBugzillaId() {
        return bugzillaId;
    }

    public String getCommentString() {
        return commentString;
    }

    public Collection<Submit> getDuplicates() {
        return duplicates;
    }

    public Reporter getReporter() {
        return reporter;
    }

    public String getVM() {
        return "HotSpot";
    }

    public String getOS() {
        return "Widle";
    }

    public String getProductVersion() {
        return "NetBeans IDE Dev";
    }

    public String getBugzillaVersion() {
        return "6.0";
    }

    public String getAdditionalComment() {
        return null;
    }

    public String getComponent() {
        return "IDE";
    }

    public String getSubcomponent() {
        return "Code";
    }

    public int getId() {
        return 1;
    }

    public int getReportId() {
        return 100;
    }

    public String getSummary() {
        return "Hello World!";
    }

    public String getStatusWhiteboard() {
        return null;
    }

    public String getKeywords() {
        return null;
    }

    public Attachment getAttachment() {
        return null;
    }

    public void setBugzillaId(int bugzillaId) {
        this.bugzillaId = bugzillaId;
    }

    public void setCommentString(String commentString) {
        this.commentString = commentString;
    }

    public void setDuplicates(Collection<Submit> duplicates) {
        this.duplicates = duplicates;
    }

    public void setInBugzilla(boolean inBugzilla) {
        this.inBugzilla = inBugzilla;
    }

    public void setReporter(Reporter reporter) {
        this.reporter = reporter;
    }

    
}
