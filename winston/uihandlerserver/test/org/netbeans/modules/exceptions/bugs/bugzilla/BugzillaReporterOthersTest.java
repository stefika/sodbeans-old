/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 2009 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 *
 * Contributor(s):
 *
 * Portions Copyrighted 2009 Sun Microsystems, Inc.
 */
package org.netbeans.modules.exceptions.bugs.bugzilla;

import org.netbeans.server.uihandler.bugs.BugzillaReporter;
import org.junit.Test;
import org.netbeans.server.uihandler.api.bugs.BugReporter;
import org.netbeans.server.uihandler.BugzillaTestCase;

/**
 *
 * @author Jindrich Sedek
 */
public class BugzillaReporterOthersTest extends BugzillaTestCase {
    BugReporter bzr = new BugzillaReporter();

    public BugzillaReporterOthersTest(String name) {
        super(name);
    }

    @Test
    public void testCreateWithCC() throws Exception{
        testCreateWithCC("qarobot");
    }

    @Test
    public void testCreateWithCCFullName() throws Exception{
        testCreateWithCC("qarobot@testnetbeans.org");
    }

    private void testCreateWithCC(String ccName) throws Exception{
//        EntityManager em = perUtils.createEntityManager();
//        em.getTransaction().begin();
//        Submit sbm = BugzillaReporterTest.prepareData(em, 1, true);
//        Properties props =new Properties();
//        props.put("username", "exceptions_reporter");
//        props.put("password", "vsdsvsds");
//        props.put(BugReporter.CC_PROPERTY, ccName);
//        em.getTransaction().commit();
//        em.close();
//
//        bzr.reportSubmit(sbm, props);
//
//        em = perUtils.createEntityManager();
//        em.getTransaction().begin();
//        sbm = em.find(Submit.class, 1);
//        Integer issueid = sbm.getReportId().getIssueId();
//        assertNotNull(issueid);
//        assertTrue(issueid != 0);
//        Issue iss = bzr.getIssue(issueid);
//        List<String> users = iss.getCcUsers();
//        assertTrue(users.contains("qarobot@testnetbeans.org"));
//        em.getTransaction().commit();
//        em.close();
    }
}
