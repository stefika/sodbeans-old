/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 2009 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 *
 * Contributor(s):
 *
 * Portions Copyrighted 2009 Sun Microsystems, Inc.
 */
package org.netbeans.modules.exceptions.bugs.bugzilla;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.logging.Handler;
import java.util.logging.LogRecord;
import javax.persistence.EntityManager;
import org.junit.Test;
import org.netbeans.junit.MockServices;
import org.netbeans.modules.exceptions.entity.Comment;
import org.netbeans.modules.exceptions.entity.Exceptions;
import org.netbeans.modules.exceptions.entity.Logfile;
import org.netbeans.modules.exceptions.entity.Method;
import org.netbeans.modules.exceptions.entity.Nbuser;
import org.netbeans.modules.exceptions.entity.Nbversion;
import org.netbeans.modules.exceptions.entity.Operatingsystem;
import org.netbeans.modules.exceptions.entity.ProductVersion;
import org.netbeans.modules.exceptions.entity.Report;
import org.netbeans.modules.exceptions.entity.Slowness;
import org.netbeans.modules.exceptions.entity.Stacktrace;
import org.netbeans.modules.exceptions.entity.Submit;
import org.netbeans.modules.exceptions.entity.Vm;
import org.netbeans.modules.exceptions.utils.BugReporterFactory;
import org.netbeans.modules.exceptions.utils.BugzillaReporter;
import org.netbeans.modules.exceptions.utils.PersistenceUtils;
import org.netbeans.server.uihandler.BugzillaTestCase;
import org.netbeans.server.uihandler.api.Authenticator.AuthToken;
import org.netbeans.server.uihandler.api.Issue;
import org.netbeans.server.uihandler.api.bugs.BugReporter;
import org.netbeans.server.uihandler.bugs.AuthenticatorImpl;
import org.netbeans.server.uihandler.bugs.BugzillaReporterImpl;
import org.netbeans.server.uihandler.bugs.ReporterUtils;

/**
 *
 * @author Jindrich Sedek
 */
public class BugzillaReporterTest extends BugzillaTestCase {

    private static final String ccUser = "exceptions_reporter";
    private static final String[] SOME_EXISTING_USERS = {"1983arun","1985aman","1986verma", "19balazs86", "1ateam"};
    private EntityManager em;
    private BugzillaReporter bzr;
    private static Integer issueid = null;

    public BugzillaReporterTest(String name) {
        super(name);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        MockServices.setServices();
        AuthenticatorImpl.setDefaultAuthToken(new AuthToken("exceptions_reporter", "vsdsvsds"));
        bzr = BugReporterFactory.getDefaultReporter();
        bzr.clearCache();
        createEM();
    }

    @Override
    protected void tearDown() throws Exception {
        closeEM();
        super.tearDown();
    }

    @Test
    public void testReportIssue() throws IOException {
        Submit sbm = reportSubmit(1);
        Issue issue = bzr.getIssue(issueid);
        assertEquals(new Integer(1), issue.getReporterSubmitId());
        assertEquals("summary", issue.getShortDesc());
        assertNotNull(issue.getKeywords());
        assertTrue(issue.getStatusWhiteboard().contains("EXCEPTIONS_REPORT"));
        if (sbm instanceof Slowness) {
            assertTrue(issue.getKeywords().contains("PERFORMANCE"));
            assertTrue(issue.getStatusWhiteboard().contains("perf-profileme"));
        }
        assertEquals("P3", issue.getPriority());

        //testAdditionalComment
        bzr.createAttachment(sbm, null);
    }

    @Test
    public void testReportWithCCAndMessage() throws IOException{
        Submit sbm = prepareData(em, 1);
        closeEM();

        bzr.reportSubmit(sbm, null, "Extra message to add", SOME_EXISTING_USERS[2] + "@testnetbeans.org");

        createEM();
        sbm = em.find(Submit.class, 1);
        issueid = sbm.getReportId().getIssueId();
        assertNotNull(issueid);
        assertTrue(issueid != 0);
    }

    @Test
    public void testCreateWithCCs() throws IOException {
        Submit sbm = prepareData(em, 1);
        em.merge(sbm);

        Submit sbm2 = prepareData(em, 2);
        sbm2.setReportId(sbm.getReportId());
        em.merge(sbm2);

        Submit sbm3 = prepareData(em, 3);
        sbm3.setReportId(sbm.getReportId());
        em.merge(sbm3);
        closeEM();

        bzr.reportSubmit(sbm, null, null, sbm.getNbuserId().getName());

        createEM();
        sbm = em.find(Submit.class, 1);
        Integer issId = sbm.getReportId().getIssueId();
        assertNotNull(issId);
        Issue issue = bzr.getIssue(issId);
        assertNotNull(issue);
        assertFalse(issue.getCcUsers().isEmpty());
        assertEquals(3, issue.getCcUsers().size());
        assertTrue(issue.getCcUsers().contains(SOME_EXISTING_USERS[1] + "@testnetbeans.org"));
        assertTrue(issue.getCcUsers().contains(SOME_EXISTING_USERS[2] + "@testnetbeans.org"));
        assertTrue(issue.getCcUsers().contains(SOME_EXISTING_USERS[3] + "@testnetbeans.org"));
    }

    @Test
    public void testSetPriority() {
        assertNotNull(issueid);
        assertNotNull(bzr);
        bzr.setIssuePriority(issueid, "P1");
        Issue issue = bzr.getIssue(issueid);
        assertEquals(new Integer(1), issue.getReporterSubmitId());
        assertEquals("summary", issue.getShortDesc());
        assertEquals("P1", issue.getPriority());
    }

    @Test
    public void testPostTextComment() {
        bzr.postTextComment(issueid, "Hallo World!");
        // TODO check the issue
    }

    @Test
    public void testAddToCCList() {
        bzr.addToCCList(issueid, ccUser, "vsdsvsds");
        Issue issue = bzr.getIssue(issueid);
        assertEquals(new Integer(1), issue.getReporterSubmitId());
        assertEquals("summary", issue.getShortDesc());
        assertTrue(issue.getCcUsers().contains(ReporterUtils.userNameToEMail(ccUser)));
    }

    @Test
    public void testResolveIssue() {
        final List<LogRecord> recs = new ArrayList<LogRecord>();
        BugReporter.LOG.addHandler(new Handler() {

            @Override
            public void publish(LogRecord record) {
                recs.add(record);
            }

            @Override
            public void flush() {
            }

            @Override
            public void close() throws SecurityException {
            }
        });
        BugzillaReporterImpl bri = new BugzillaReporterImpl();
        bri.resolveIssue(issueid, "FIXED", null, null);
        Issue issue = bzr.getIssue(issueid);
        assertEquals(new Integer(1), issue.getReporterSubmitId());
        assertEquals("summary", issue.getShortDesc());
        assertTrue(issue.getCcUsers().contains(ReporterUtils.userNameToEMail(ccUser)));
        assertFalse(Integer.toString(recs.size()), recs.isEmpty());
        assertTrue(recs.get(0).getMessage(), recs.get(0).getMessage().contains("was resolved"));
        assertEquals("RESOLVED", issue.getIssueStatus());
        assertEquals("FIXED", issue.getResolution());
    }

    @Test
    public void testReopenIssue() {
        bzr.reopenIssue(issueid, "Reopenning");
        Issue issue = bzr.getIssue(issueid);
        assertEquals(new Integer(1), issue.getReporterSubmitId());
        assertEquals("summary", issue.getShortDesc());
        assertTrue(issue.getCcUsers().contains(ReporterUtils.userNameToEMail(ccUser)));
        assertEquals("REOPENED", issue.getIssueStatus());
    }

    @Test
    public void testFilterOpen() throws IOException {
        List<Integer> allSubmits = new ArrayList<Integer>();
        Submit first = em.find(Submit.class, 1);
        if (first == null) {
            first = reportSubmit(1);
        }
        allSubmits.add(first.getReportId().getIssueId());
        Submit next = reportSubmit(2);
        allSubmits.add(next.getReportId().getIssueId());
        next = reportSubmit(3);
        allSubmits.add(next.getReportId().getIssueId());
        next = reportSubmit(4);
        allSubmits.add(next.getReportId().getIssueId());

        Collection<Integer> filtered = bzr.filterOpen(allSubmits);
        assertEquals(4, filtered.size());

        BugzillaReporterImpl bri = new BugzillaReporterImpl();
        bri.resolveIssue(allSubmits.get(0), "FIXED", null, null);
        filtered = bzr.filterOpen(allSubmits);
        assertEquals(3, filtered.size());

        bri = new BugzillaReporterImpl();
        bri.resolveIssue(allSubmits.get(1), "INVALID", null, null);
        filtered = bzr.filterOpen(allSubmits);
        assertEquals(2, filtered.size());
    }

    @Test
    public void testDuplicateOf() throws IOException{
        Issue issue = BugReporterFactory.getDefaultReporter().getIssue(12345);
        assertNotNull(issue);
        assertNotNull(issue.getDuplicateOf());
        assertEquals(11533, issue.getDuplicateOf().intValue());
    }

    private Submit reportSubmit(int id) throws IOException {
        Submit sbm = prepareData(em, id);
        closeEM();

        bzr.reportSubmit(sbm, null);

        createEM();
        sbm = em.find(Submit.class, id);
        issueid = sbm.getReportId().getIssueId();
        assertNotNull(issueid);
        assertTrue(issueid != 0);
        return sbm;
    }

    protected Submit prepareData(EntityManager em, int i) throws IOException {
        return prepareData(em, i, true);
    }

    public static Submit prepareData(EntityManager em, int i, boolean isSlowness) throws IOException {
        Nbuser user = new Nbuser(i);
        user.setName(SOME_EXISTING_USERS[i % SOME_EXISTING_USERS.length]);
        em.persist(user);

        Nbversion version = (Nbversion) PersistenceUtils.getExist(em, "Nbversion.findByVersion", "6.8");
        if (version == null) {
            version = new Nbversion(i);
            version.setVersion("6.8");
            em.persist(version);
        }

        ProductVersion pv = (ProductVersion) PersistenceUtils.getExist(em, "ProductVersion.findByProductVersion", "NetBeans IDE 6.8 Beta (Build 200910020947)");
        if (pv == null) {
            pv = new ProductVersion(i);
            pv.setNbversionId(version);
            pv.setProductVersion("NetBeans IDE 6.8 Beta (Build 200910020947)");
            em.persist(pv);
        }

        Vm vm = (Vm) PersistenceUtils.getExist(em, "Vm.findByName", "JDK 1.6");
        if (vm == null) {
            vm = new Vm();
            vm.setId(i);
            vm.setName("JDK 1.6");
            vm.setShortName("1.6");
            em.persist(vm);
        }

        Operatingsystem os = Operatingsystem.getExists(em, "Linux", "1.1", "Linux");
        if (os == null) {
            os = new Operatingsystem();
            os.setId(i);
            os.setName("Linux");
            os.setVersion("1.1");
            os.setArch("Linux");
            em.persist(os);
        }

        Logfile lf = new Logfile(i * 100);
        lf.setBuildnumber(0l);
        lf.setUserdir("dsfddsf");
        lf.setUploadNumber(i);
        lf.setChangeset(0l);
        lf.setProductVersionId(pv);
        lf.setNbuser(user);
        em.persist(lf);

        FileOutputStream fos = new FileOutputStream(lf.getNPSFile());
        fos.write("Hallo World!".getBytes());
        fos.close();

        Report r = new Report(i);
        r.setComponent("ide");
        r.setSubcomponent("Code");
        em.persist(r);
        Submit sbm;
        if (isSlowness) {
            Method m = new Method(i);
            m.setName("test" + i);
            em.persist(m);

            Slowness slowness = new Slowness(i);
            slowness.setActionTime(Long.valueOf(i));
            slowness.setSuspiciousMethod(m);
            sbm = slowness;
        } else {
            Stacktrace stack = new Stacktrace(i);
            stack.setMessage("Something is wrong");
            em.persist(stack);
            Exceptions exc = new Exceptions(i);
            exc.setStacktrace(stack);
            sbm = exc;
        }
        sbm.setReportId(r);
        sbm.setNbuserId(user);
        sbm.setLogfileId(lf);
        sbm.setSummary("summary");
        sbm.setVmId(vm);
        sbm.setOperatingsystemId(os);
        em.persist(sbm);

        Comment comment = new Comment();
        comment.generateId();
        comment.setComment("Test Comment");
        comment.setNbuserId(user);
        comment.setSubmitId(sbm);
        em.persist(comment);

        return sbm;
    }

    private void createEM() {
        em = perUtils.createEntityManager();
        em.getTransaction().begin();
    }

    private void closeEM() {
        em.getTransaction().commit();
        em.close();
    }

    private void reloadEM() {
        closeEM();
        createEM();
    }

}
