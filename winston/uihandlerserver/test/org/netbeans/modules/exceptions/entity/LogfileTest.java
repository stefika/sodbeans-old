/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2009 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */
package org.netbeans.modules.exceptions.entity;

import java.io.File;
import javax.persistence.EntityManager;
import org.netbeans.server.snapshots.ProfilerData;
import org.netbeans.server.uihandler.DatabaseTestCase;
import org.netbeans.server.uihandler.LogFileTask;
import org.netbeans.server.uihandler.LogsManagerTest;
import org.netbeans.server.uihandler.Utils;
import static org.junit.Assert.*;

/**
 *
 * @author Jindrich Sedek
 */
public class LogfileTest extends DatabaseTestCase {

    public LogfileTest(String name) {
        super(name);
    }

    public void testSetCPUAttribute() throws Exception {
        String IP1 = "192.0.0.1";
        File log = LogsManagerTest.extractResourceAs(LogfileTest.class, data, "detailAction.log", "NB2040811605");
        waitLog(log, IP1);
        verifyCPU(0, true);
    }

    public void testSetMemoryAttribute() throws Exception {
        String IP1 = "192.0.0.1";
        File log = LogsManagerTest.extractResourceAs(LogfileTest.class, data, "detailAction.log", "NB2040811605");
        waitLog(log, IP1);
        verifyMemory(0, true);
    }

    public void testSetMemoryAttributeInOlderLogs() throws Exception {
        String IP1 = "192.0.0.1";
        File log = LogsManagerTest.extractResourceAs(LogfileTest.class, data, "detailAction.log", "NB2040811605");
        waitLog(log, IP1);
        log = LogsManagerTest.extractResourceAs(LogsManagerTest.class, data, "2.log", "NB2040811605.0");
        waitLog(log, IP1);
        log = LogsManagerTest.extractResourceAs(LogsManagerTest.class, data, "2.log", "NB2040811605.1");
        waitLog(log, IP1);
        verifyMemory(2, true);
    }

    public void testSetMemoryAttributeWithAllEmpties() throws Exception {
        String IP1 = "192.0.0.1";
        File log = LogsManagerTest.extractResourceAs(LogsManagerTest.class, data, "2.log", "NB2040811605");
        waitLog(log, IP1);
        log = LogsManagerTest.extractResourceAs(LogsManagerTest.class, data, "2.log", "NB2040811605.0");
        waitLog(log, IP1);
        log = LogsManagerTest.extractResourceAs(LogsManagerTest.class, data, "2.log", "NB2040811605.1");
        waitLog(log, IP1);
        verifyMemory(2, false);
    }

    private void verifyMemory(int uploadNumber, boolean exists) {
        verifyAttribute(uploadNumber, false, exists, 3898);
    }

    private void verifyCPU(int uploadNumber, boolean exists) {
        verifyAttribute(uploadNumber, true, exists, 2);
    }

    private void verifyAttribute(int uploadNumber, boolean cpu, boolean exists, Object expectedValue) {
        EntityManager em = perUtils.createEntityManager();
        em.getTransaction().begin();
        Logfile lf = LogFileTask.getLogInfo("NB2040811605", uploadNumber, em);
        Object value;
        if (cpu) {
            value = lf.getUserCPUCount(em);
        } else {
            value = lf.getUserMemory(em);
        }
        em.getTransaction().commit();
        em.close();
        if (exists) {
            assertNotNull(value);
            assertEquals(expectedValue, value);
        } else {
            assertNull(value);
        }
    }

    public void testSnapshotStatistics() throws Exception{
        File dataUploadDir = new File(Utils.getUploadDirPath(Utils.getRootUploadDirPath(), "NB2040811605"));
        File log = LogsManagerTest.extractResourceAs(LogfileTest.class, dataUploadDir, "missing_snapshot_stats.xml", "NB2040811605");
        File npsUploadDir = new File(Utils.getUploadDirPath(Utils.getSlownessRootUploadDir(), "NB2040811605"));
        File npsLog = LogsManagerTest.extractResourceAs(LogfileTest.class, npsUploadDir, "missing_snapshot_stats.nps", "NB2040811605");
        addLog(log, "NB2040811605");
        
        EntityManager em = perUtils.createEntityManager();
        em.getTransaction().begin();
        Logfile lf = LogFileTask.getLogInfo("NB2040811605", 0, em);
        
        ProfilerData pd = lf.getProfilerStatistics(em);
        assertNotNull(pd);
        assertEquals("get last value", 534, pd.getSamples());
        assertEquals("get last value", 10.0, pd.getAvg());
        assertEquals("get last value", 20.2, pd.getMaximum());
        assertEquals("get last value", 0.7, pd.getMinimum());
        assertEquals("get last value", 1.3, pd.getStddev());
        em.getTransaction().commit();
        em.close();
    }
}
