/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 2010 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 *
 * Contributor(s):
 *
 * Portions Copyrighted 2010 Sun Microsystems, Inc.
 */

package org.netbeans.modules.exceptions.entity;

import javax.persistence.EntityManager;
import org.netbeans.server.uihandler.DatabaseTestCase;
import static org.junit.Assert.*;

/**
 *
 * @author Jindrich Sedek
 */
public class SubmitTest extends DatabaseTestCase{

    private EntityManager em;

    public SubmitTest(String testName) {
        super(testName);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        em = perUtils.createEntityManager();
        em.getTransaction().begin();
    }

    @Override
    protected void tearDown() throws Exception {
        if (em.getTransaction().isActive()) {
            em.getTransaction().commit();
        }
        em.close();
        super.tearDown();
    }

    public void testGetAdditionalCommentSlowness() {
        Report r = new Report(1);
        em.persist(r);

        Method m = new Method(1);
        m.setName("test");
        em.persist(m);

        Slowness sbm = new Slowness(1);
        sbm.setReportId(r);
        sbm.setActionTime(200L);
        sbm.setSuspiciousMethod(m);
        em.persist(sbm);

        em.getTransaction().commit();
        em.close();

        em = perUtils.createEntityManager();
        em.getTransaction().begin();

        String str = org.netbeans.modules.exceptions.entity.Submit.getById(em, 1).getAdditionalComment();
        assertEquals("Maximum slowness yet reported was 200 ms, average is 200", str.trim());

        Slowness sbm2 = new Slowness(2);
        sbm2.setReportId(em.merge(r));
        sbm2.setActionTime(400L);
        sbm2.setSuspiciousMethod(em.merge(m));
        em.persist(sbm2);

        em.getTransaction().commit();
        em.close();

        em = perUtils.createEntityManager();
        em.getTransaction().begin();

        str = org.netbeans.modules.exceptions.entity.Submit.getById(em, 2).getAdditionalComment();
        assertEquals("Maximum slowness yet reported was 400 ms, average is 300", str.trim());

    }

    public void testGetAdditionalCommentException() {
        Report r = new Report(1);
        em.persist(r);

        Exceptions sbm = new Exceptions(1);
        sbm.setReportId(r);

        Stacktrace st = new Stacktrace(1);
        st.setMessage("Hello world!");
        sbm.setStacktrace(st);
        em.persist(st);
        em.persist(sbm);

        Method m = new Method(1);
        m.setName("test");
        em.persist(m);

        for (int i = 1; i < 10; i++) {
            Line line = new Line(1, 1, i, i);
            em.persist(line);
        }

        em.getTransaction().commit();
        em.close();

        em = perUtils.createEntityManager();
        em.getTransaction().begin();

        String str = org.netbeans.modules.exceptions.entity.Submit.getById(em, 1).getAdditionalComment();
        assertTrue(str, str.contains("test(test.java:1)"));
        assertTrue(str, str.contains("Hello world!"));
        assertTrue(str, str.contains("Stacktrace:"));
    }

}