/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */
package org.netbeans.modules.exceptions.entity;

import java.util.Date;
import org.junit.Test;
import org.netbeans.junit.NbTestCase;
import static org.junit.Assert.*;

/**
 *
 * @author Jindrich Sedek
 */
public class ExceptionsTest extends NbTestCase {

    public ExceptionsTest(String name) {
        super(name);
    }

    @Test
    public void testIsNewer() {
        Exceptions exc = new Exceptions(1);
        exc.setBuild(80825l);
        assertFalse(exc.isBuildNewerThan(new Date()));
        assertFalse("build date", exc.isBuildNewerThan(new Date("8/25/08")));
        assertFalse("older build", exc.isBuildNewerThan(new Date("8/30/08")));
        assertFalse("older build", exc.isBuildNewerThan(new Date("9/1/08")));
        assertFalse("in offset", exc.isBuildNewerThan(new Date("8/21/08")));
        assertTrue("newer build", exc.isBuildNewerThan(new Date("8/15/08")));
        assertTrue("newer build", exc.isBuildNewerThan(new Date("7/19/08")));

        exc.setBuild(70914l);
        assertFalse(exc.isBuildNewerThan(new Date()));
        assertFalse("build date", exc.isBuildNewerThan(new Date("9/14/07")));
        assertFalse("older build", exc.isBuildNewerThan(new Date("9/30/07")));
        assertFalse("older build", exc.isBuildNewerThan(new Date("11/10/08")));
        assertFalse("older build", exc.isBuildNewerThan(new Date("11/10/07")));
        assertFalse("in offset", exc.isBuildNewerThan(new Date("9/11/07")));
        assertTrue("newer build", exc.isBuildNewerThan(new Date("8/30/07")));

        exc.setBuild(71001l);
        assertFalse("in offset over month", exc.isBuildNewerThan(new Date("9/30/07")));
        assertTrue("newer over month", exc.isBuildNewerThan(new Date("9/23/07")));

        Slowness slown = new Slowness(1);
        Logfile lf = new Logfile(1);
        String buildNumber = org.netbeans.modules.exceptions.utils.Utils.getBuildNumber("NetBeans IDE 6.8 Beta (Build 200910020947)");
        buildNumber = org.netbeans.modules.exceptions.utils.Utils.getCustomBuildFormat(buildNumber);
        lf.setBuildnumber(Long.valueOf(buildNumber));
        slown.setLogfileId(lf);
        assertTrue(slown.isBuildNewerThan(new Date("9/1/2009")));
        assertFalse(slown.isBuildNewerThan(new Date("10/1/2009")));
    }
}