/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */
package org.netbeans.modules.exceptions.entity;

import java.util.Iterator;
import java.util.List;
import javax.persistence.EntityManager;
import org.junit.Test;
import org.netbeans.modules.exceptions.utils.PersistenceUtils;
import org.netbeans.server.uihandler.DatabaseTestCase;

/**
 *
 * @author Jindrich Sedek
 */
public class SetReportIDTest extends DatabaseTestCase {

    EntityManager em;
    public SetReportIDTest(String name) {
        super(name);
    }

    @Test
    public void testSetReportIdWithHistory(){
        em = perUtils.createEntityManager();
        em.getTransaction().begin();
        List<DuplicateLog> logs = PersistenceUtils.getAll(em, DuplicateLog.class);
        assertTrue(logs.isEmpty());

        Report r = new Report(1);
        Exceptions exc = new Exceptions(1);
        exc.setReportId(r);
        em.persist(r);
        em.persist(exc);
        waitAndCommit();
        logs = PersistenceUtils.getAll(em, DuplicateLog.class);
        assertTrue("initial value", logs.isEmpty());

        exc.setReportId(r);
        waitAndCommit();
        logs = PersistenceUtils.getAll(em, DuplicateLog.class);
        assertTrue("the same value", logs.isEmpty());

        Report r2 = new Report(2);
        em.persist(r2);
        exc.setReportId(r2);
        waitAndCommit();
        logs = PersistenceUtils.getAll(em, DuplicateLog.class);
        assertFalse("should be logged", logs.isEmpty());
        assertEquals(1, logs.size());
        DuplicateLog log = logs.iterator().next();
        assertEquals(exc, log.getExceptions());
        assertEquals(r, log.getOldReport());

        Report r3 = new Report(3);
        em.persist(r3);
        exc.setReportId(r3);
        waitAndCommit();
        logs = PersistenceUtils.getAll(em, DuplicateLog.class);
        assertFalse("should be logged", logs.isEmpty());
        assertEquals(2, logs.size());
        Iterator<DuplicateLog> it = logs.iterator();
        log = it.next();
        assertEquals(exc, log.getExceptions());
        assertEquals(r, log.getOldReport());

        log = it.next();
        assertEquals(exc, log.getExceptions());
        assertEquals(r2, log.getOldReport());

        em.getTransaction().commit();
        em.close();
    }

    private void waitAndCommit(){
        Exceptions.waitDupChanges();
        em.getTransaction().commit();
        em.getTransaction().begin();
    }
}
