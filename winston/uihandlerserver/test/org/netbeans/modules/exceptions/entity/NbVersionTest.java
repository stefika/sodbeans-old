/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */
package org.netbeans.modules.exceptions.entity;

import org.junit.Test;
import org.netbeans.junit.NbTestCase;
import org.netbeans.modules.exceptions.utils.Utils;

/**
 *
 * @author Jindrich Sedek
 */
public class NbVersionTest extends NbTestCase {

    public NbVersionTest(String name) {
        super(name);
    }

    @Test
    public void testGetVersionNumber() {
        performTest("NetBeans IDE 6.5", "6.5");
        performTest("NetBeans IDE 6.5 RC1", "6.5");
        performTest("NetBeans IDE 6.1 Beta", "6.1");
        performTest("NetBeans IDE 6.0 RC1", "6.0");
        performTest("NetBeans IDE 6.0", "6.0");
        performTest("NetBeans IDE 6.0.1", "6.0.1");
        performTest("NetBeans IDE Dev", Nbversion.DEFAULT_VERSION);
        performTest("NetBeans Ruby IDE", null);
        performTest("NetBeans IDE 6.0.1 dev", "6.0.1");
        performTest("NetBeans IDE 6.5.1", "6.5.1");
        performTest("NetBeans Platform 6.7 RC2", "6.7");
        performTest("NetBeans Platform 6.7", "6.7");
        performTest("NetBeans Platform Dev", Nbversion.DEFAULT_VERSION);
        performTest("", Nbversion.DEFAULT_VERSION);
    }

    private void performTest(String productVersion, String versionNumber) {
        Nbversion version = new Nbversion(1);
        String nbVersion = Utils.getNbVersion(productVersion);
        version.setVersion(nbVersion);
        assertEquals(versionNumber, version.getVersionNumber());
    }
}