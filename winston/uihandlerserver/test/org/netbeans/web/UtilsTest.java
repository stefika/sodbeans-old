/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */
package org.netbeans.web;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import org.junit.Test;
import org.netbeans.modules.exceptions.entity.Prefname;
import org.netbeans.server.uihandler.DatabaseTestCase;
import static org.junit.Assert.*;

/**
 *
 * @author Jindrich Sedek
 */
public class UtilsTest extends DatabaseTestCase {

    private int id;
    private boolean dataFoundInDb = false;

    public UtilsTest(String name) {
        super(name);
    }

    @Test
    public void testPersistable() throws Exception {
        Persistable persist = new Persistable.Transaction() {

            public TransactionResult runQuery(EntityManager em) {
                Prefname name = new Prefname("name1");
                em.persist(name);
                id = name.getId();
                return TransactionResult.COMMIT;
            }
        };
        Utils.processPersistable(persist);

        Persistable query = new Persistable.Query() {

            public TransactionResult runQuery(EntityManager em) {
                Prefname name = em.find(Prefname.class, id);
                assertNotNull(name);
                dataFoundInDb = true;
                return TransactionResult.NONE;
            }
        };
        Utils.processPersistable(query);
        assertTrue(dataFoundInDb);
    }

    @Test
    public void testRollback() throws Exception {
        Persistable persist = new Persistable.Transaction() {

            public TransactionResult runQuery(EntityManager em) {
                Prefname name = new Prefname("name1");
                em.persist(name);
                id = name.getId();
                return TransactionResult.ROLLBACK;
            }
        };
        Utils.processPersistable(persist);

        Persistable query = new Persistable.Query() {

            public TransactionResult runQuery(EntityManager em) {
                Prefname name = em.find(Prefname.class, id);
                assertNull(name);
                dataFoundInDb = false;
                return null;
            }
        };
        dataFoundInDb = true;
        Utils.processPersistable(query);
        assertFalse(dataFoundInDb);
    }
    
    public void testExceptionDelegation(){
        Persistable persist = new Persistable.Transaction() {

            public TransactionResult runQuery(EntityManager em) {
                Prefname name = new Prefname("name1");
                em.persist(name);
                id = name.getId();
                String str = null;
                str.charAt(0);  // NPE
                return TransactionResult.COMMIT;
            }
        };
        try{
            Utils.processPersistable(persist);
            fail("PersistanceException should be thrown");
        }catch(PersistenceException exc){
            // OK
            assertTrue(exc.getCause() instanceof NullPointerException);
        }

        Persistable query = new Persistable.Query() {

            public TransactionResult runQuery(EntityManager em) {
                Prefname name = em.find(Prefname.class, id);
                assertNull(name);
                dataFoundInDb = false;
                return null;
            }
        };
        dataFoundInDb = true;
        Utils.processPersistable(query);
        assertFalse("Transaction should have not been commited", dataFoundInDb);
    }
}