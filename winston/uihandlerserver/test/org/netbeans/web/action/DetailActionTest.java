/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */


package org.netbeans.web.action;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Handler;
import java.util.logging.LogRecord;
import javax.persistence.EntityManager;
import javax.servlet.ServletRequest;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.netbeans.lib.uihandler.LogRecords;
import org.netbeans.modules.exceptions.entity.Comment;
import org.netbeans.modules.exceptions.entity.Exceptions;
import org.netbeans.modules.exceptions.entity.Logfile;
import org.netbeans.modules.exceptions.entity.Nbversion;
import org.netbeans.modules.exceptions.entity.Patch;
import org.netbeans.modules.exceptions.entity.ProductVersion;
import org.netbeans.modules.exceptions.entity.Report;
import org.netbeans.modules.exceptions.entity.Stacktrace;
import org.netbeans.modules.exceptions.utils.PersistenceUtils;
import org.netbeans.server.uihandler.DatabaseTestCase;
import org.netbeans.server.uihandler.LogsManagerTest;
import org.netbeans.server.uihandler.Utils;
import org.netbeans.web.EntityManagerFilter;

/**
 *
 * @author Jindrich Sedek
 */
public class DetailActionTest extends DatabaseTestCase {
    private static final String commentMessage = "HELLO WORLD";

    public DetailActionTest(String str) {
        super(str);
    }

    public void testGetPatches() throws Exception{
        String IP1 = "192.0.0.1";
        String PatchName = "TEST_PATCH";
        String ModuleName = "org.netbeans.modules.core.kit";
        File log = LogsManagerTest.extractResourceAs(LogsManagerTest.class, data, "2.log", "NB2040811605.1");
        waitLog(log, IP1);
        TestHandler hand = new TestHandler();
        InputStream is = new FileInputStream(log);
        LogRecords.scan(is, hand);
        EntityManager em = perUtils.createEntityManager();
        Nbversion nbVersion = PersistenceUtils.getAll(em, Nbversion.class).get(0);
        Exceptions exc = PersistenceUtils.getAll(em, Exceptions.class).get(0);
        Patch patch = new Patch(PatchName, ModuleName, "1.1", nbVersion);
        em.getTransaction().begin();
        em.persist(patch);
        em.getTransaction().commit();
        List<String> list = new DetailAction().getPatches(em, exc, hand.list);
        assertNotNull(list);
        assertEquals(1, list.size());
        assertEquals(PatchName + ": &nbsp; " + ModuleName, list.get(0));
        em.close();
    }

    public void testAddComment() throws Exception{
        int exceptionId = 1;
        String userId = "user";
        ServletRequest request = page.getRequest();

        EntityManager em = perUtils.createEntityManager();
        em.getTransaction().begin();
        Exceptions exc = new Exceptions(exceptionId);
        Report report = new Report(exceptionId);
        exc.setReportId(report);
        Stacktrace stack = new Stacktrace(Utils.getNextId(Stacktrace.class));
        Logfile logfile = new Logfile(userId, 0);
        ProductVersion pVersion = new ProductVersion(Utils.getNextId(ProductVersion.class));
        pVersion.setProductVersion("pVersion");
        Nbversion version = new Nbversion(Utils.getNextId(Nbversion.class));
        version.setVersion("version");
        pVersion.setNbversionId(version);
        logfile.setProductVersionId(pVersion);
        exc.setLogfileId(logfile);
        exc.setStacktrace(stack);
        em.persist(version);
        em.persist(pVersion);
        em.persist(logfile);
        em.persist(report);
        em.persist(stack);
        em.persist(exc);
        em.getTransaction().commit();
        em.close();
        File f = new File(Utils.getUploadDirPath(Utils.getRootUploadDirPath(), userId), userId);
        f.createNewFile();
        Map<String, Object> mapa = request.getParameterMap();
        mapa.put("id", Integer.toString(exceptionId));
        mapa.put("comment", commentMessage);

        em = perUtils.createEntityManager();
        request.setAttribute(EntityManagerFilter.ENTITY_MANAGER, em);
        new  DetailAction().execute(new TestActionMapping(), null, request, page.getResponse());
        em.close();

        List<Comment> comments = perUtils.getAll(Comment.class);

        assertEquals(1, comments.size());
        assertEquals(commentMessage, comments.iterator().next().getComment());
    }

    private class TestActionMapping extends ActionMapping{

        @Override
        public ActionForward findForward(String arg0) {
            return null;
        }
        
    }
    private class TestHandler extends Handler{
        ArrayList<LogRecord> list = new ArrayList<LogRecord>();
        @Override
        public void publish(LogRecord record) {
            list.add(record);
        }

        @Override
        public void flush() {
            //
        }

        @Override
        public void close() throws SecurityException {
            //
        }
        
    }
    
}