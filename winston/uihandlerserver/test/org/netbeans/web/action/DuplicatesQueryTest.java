/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */
package org.netbeans.web.action;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import org.netbeans.modules.exceptions.utils.BugReporterFactory;
import org.netbeans.modules.exceptions.entity.Exceptions;
import org.netbeans.modules.exceptions.entity.Report;
import org.netbeans.modules.exceptions.entity.Submit;
import org.netbeans.server.uihandler.api.Issue;
import org.netbeans.server.uihandler.DatabaseTestCase;
import org.netbeans.server.uihandler.Utils;
import static org.junit.Assert.*;

/**
 *
 * @author Jindrich Sedek
 */
public class DuplicatesQueryTest extends DatabaseTestCase {

    DuplicatesQuery query;
    EntityManager em;

    public DuplicatesQueryTest(String str) {
        super(str);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        query = new DuplicatesQuery();
        em = perUtils.createEntityManager();
        em.getTransaction().begin();
    }

    @Override
    protected void tearDown() throws Exception {
        em.getTransaction().commit();
        em.close();
        super.tearDown();
    }

    public void testEmpty() throws Exception {
        List<Submit> result = runQuery("both", "ALL");
        assertNotNull(result);
        assertTrue(result.isEmpty());
    }

    public void testNotInIZ() throws Exception {
        Submit e1 = createNewSubmit();
        List<Submit> result = runQuery("both", "ALL");
        assertNotNull(result);
        assertFalse(result.isEmpty());
        assertEquals(e1, result.get(0));

        result = runQuery("unreported", "ALL");
        assertNotNull(result);
        assertFalse(result.isEmpty());
        assertEquals(e1, result.get(0));

        result = runQuery("in issuezilla", "ALL");
        assertNotNull(result);
        assertTrue(result.isEmpty());
    }

    public void testInIZ() throws Exception {
        Submit e1 = createNewSubmit();
        e1.getReportId().setIssueId(1000);

        List<Submit> result = runQuery("both", "ALL");
        assertNotNull(result);
        assertFalse(result.isEmpty());
        assertEquals(e1, result.get(0));

        result = runQuery("in issuezilla", "ALL");
        assertNotNull(result);
        assertFalse(result.isEmpty());
        assertEquals(e1, result.get(0));

        result = runQuery("unreported", "ALL");
        assertNotNull(result);
        assertTrue(result.isEmpty());
    }

    public void testOpen() throws Exception {
        Submit e1 = createNewSubmit();
        e1.getReportId().setIssueId(1000);
        Issue issue = BugReporterFactory.getDefaultReporter().getIssue(1000);
        issue.setIssueStatus("NEW");

        List<Submit> result = runQuery("both", "OPEN");
        assertNotNull(result);
        assertFalse(result.isEmpty());
        assertEquals(e1, result.get(0));

        result = runQuery("both", "CLOSED");
        assertNotNull(result);
        assertTrue(result.isEmpty());

        issue.setIssueStatus("STARTED");
        result = runQuery("both", "OPEN");
        assertNotNull(result);
        assertFalse(result.isEmpty());
        assertEquals(e1, result.get(0));

        result = runQuery("both", "CLOSED");
        assertNotNull(result);
        assertTrue(result.isEmpty());

        issue.setIssueStatus("RESOLVED");
        result = runQuery("both", "OPEN");
        assertNotNull(result);
        assertTrue(result.isEmpty());
    }

    public void testClosed() throws Exception {
        Submit e1 = createNewSubmit();
        e1.getReportId().setIssueId(1000);
        Issue issue = BugReporterFactory.getDefaultReporter().getIssue(1000);
        issue.setIssueStatus("RESOLVED");

        List<Submit> result = runQuery("both", "OPEN");
        assertNotNull(result);
        assertTrue(result.isEmpty());

        result = runQuery("both", "CLOSED");
        assertNotNull(result);
        assertFalse(result.isEmpty());
        assertEquals(e1, result.get(0));

        issue.setIssueStatus("VERIFIED");

        result = runQuery("both", "OPEN");
        assertNotNull(result);
        assertTrue(result.isEmpty());

        result = runQuery("both", "CLOSED");
        assertNotNull(result);
        assertFalse(result.isEmpty());
        assertEquals(e1, result.get(0));
    }

    public void testDescOrder() {
        Submit e1 = createNewSubmit();
        Submit e2 = createNewSubmit(2);
        e2.setReportId(e1.getReportId());
        em.merge(e2);

        List<Submit> result = runQuery(0, 10, true);
        assertNotNull(result);
        assertEquals(2, result.size());
        assertEquals("SECOND", e2, result.get(0));
        assertEquals("FIRST", e1, result.get(1));
    }

    public void testDuplicatesCount() {
        Submit e1 = createNewSubmit();
        Submit e2 = createDuplicate(e1);

        List<Submit> result = runQuery(0, 10, true);
        assertNotNull(result);
        assertEquals(2, result.size());
        assertTrue(result.contains(e2));
        assertTrue(result.contains(e1));

        result = runQuery(0, 10, false);
        assertNotNull(result);
        assertEquals("DONT SHOW DUPLICATES", 1, result.size());
        assertEquals(e2, result.get(0));

        result = runQuery(1, 10, true);
        assertNotNull(result);
        assertEquals("BOTH EXCS HAS 2 DUPLICATES", 2, result.size());
        assertEquals(e1, result.get(0));
    }

    public void testCreationDate() {
        Submit e1 = createNewSubmit();
        Submit e2 = createNewSubmit(2);
        e2.setReportdate(previousMonth(2));
        em.merge(e2);

        List<Submit> result = runQuery(previousMonth(), new Date());
        assertNotNull(result);
        assertEquals(1, result.size());
        assertEquals(e1, result.get(0));

        result = runQuery(previousMonth(4), previousMonth(1));
        assertNotNull(result);
        assertEquals(1, result.size());
        assertEquals(e2, result.get(0));
    }

    public void testDuplicatesCreationDate() {
        Submit e1 = createNewSubmit();
        e1.setReportdate(previousMonth(4));
        em.merge(e1);
        Submit e2 = createDuplicate(e1);

        List<Submit> result = runQuery(0, 10, previousMonth(5), new Date(), true);
        assertNotNull(result);
        assertEquals(2, result.size());

        result = runQuery(1, 10, previousMonth(5), new Date(), true);
        assertNotNull(result);
        assertEquals(2, result.size());

        result = runQuery(1, 10, previousMonth(3), new Date(), true);
        assertNotNull(result);
        assertEquals(0, result.size());
    }

    public void testMultipleComponents() {
        Submit e1 = createNewSubmit(1);
        Submit e2 = createNewSubmit(2);
        e2.getReportId().setComponent("openide");
        e2.getReportId().setSubcomponent("code");
        em.merge(e2);

        List<Submit> result = runQuery(Collections.singleton("openide"), Collections.<String>emptyList(), true);
        assertEquals(1, result.size());
        assertEquals(e2, result.get(0));

        result = runQuery(Collections.singleton("web"), Collections.singleton("jsp editor"), false);
        assertEquals(1, result.size());
        assertEquals(e1, result.get(0));

        result = runQuery(Collections.singleton("web"), Collections.<String>emptySet(), true);
        assertEquals(1, result.size());
        assertEquals(e1, result.get(0));

        List<String> comps = new ArrayList<String>(2);
        comps.add("web");
        comps.add("openide");
        result = runQuery(comps, Collections.singleton("jsp editor"), true);
        assertEquals(2, result.size());
    }

    public void testAllComponents() {
        Submit e1 = createNewSubmit(1);
        Submit e2 = createNewSubmit(2);
        e2.getReportId().setComponent("openide");
        e2.getReportId().setSubcomponent("code");
        em.merge(e2);

        List<Submit> result = runQuery(null, null, false);
        assertEquals(2, result.size());
    }

    public void testMultipleSubComponents() {
        Submit e1 = createNewSubmit(1);
        Submit e2 = createNewSubmit(2);
        e2.getReportId().setComponent("web");
        e2.getReportId().setSubcomponent("code");
        em.merge(e2);

        List<Submit> result = runQuery(Collections.singleton("web"), Collections.singleton("code"), false);
        assertEquals(1, result.size());
        assertEquals(e2, result.get(0));

        result = runQuery(Collections.singleton("web"), Collections.singleton("jsp editor"), false);
        assertEquals(1, result.size());
        assertEquals(e1, result.get(0));

        result = runQuery(Collections.singleton("web"), Collections.<String>emptySet(), true);
        assertEquals(2, result.size());

        List<String> subs = new ArrayList<String>(2);
        subs.add("code");
        subs.add("jsp editor");
        result = runQuery(Collections.singleton("web"), subs, false);
        assertEquals(2, result.size());
    }

    public void testWithoutDates() throws Exception {
        createNewSubmit(1);
        Submit e2 = createNewSubmit(2);
        e2.getReportId().setComponent("openide");
        e2.getReportId().setSubcomponent("code");
        em.merge(e2);

        List<Submit> result = runQuery(0, 10, null, null, true, "both", "ALL", null, null, false);
        assertEquals(2, result.size());
    }

    public void testDuplicatesCountWithoutDates() {
        Submit e1 = createNewSubmit();
        Submit e2 = createDuplicate(e1);

        List<Submit> result = runQuery(0, 10, previousMonth(), new Date(), true);
        assertNotNull(result);
        assertEquals(2, result.size());
        assertTrue(result.contains(e2));
        assertTrue(result.contains(e1));

        result = runQuery(0, 10, previousMonth(), new Date(), false);
        assertNotNull(result);
        assertEquals("DONT SHOW DUPLICATES", 1, result.size());
        assertEquals(e2, result.get(0));

        result = runQuery(1, 10, previousMonth(), new Date(), true);
        assertNotNull(result);
        assertEquals("BOTH EXCS HAS 2 DUPLICATES", 2, result.size());
        assertEquals(e1, result.get(0));
    }

    private Submit createNewSubmit() {
        Submit result = createSubmit();
        setUpSubmit(result);
        return result;
    }

    private Submit createNewSubmit(int id) {
        Submit result = createSubmit(id);
        setUpSubmit(result);
        return result;
    }

    private void setUpSubmit(Submit result){
        Report report = new Report(result.getId());
        result.setReportId(report);
        report.setComponent("web");
        report.setSubcomponent("jsp editor");
        result.setReportdate(new Date());
        em.persist(report);
        em.persist(result);
    }

    private Submit createDuplicate(Submit root) {
        Submit duplicate = createSubmit();
        duplicate.setReportId(root.getReportId());
        duplicate.setReportdate(new Date());
        em.persist(duplicate);
        return duplicate;
    }

    private List<Submit> runQuery(Date from, Date to) {
        return runQuery(0, 10, from, to, true);
    }

    private List<Submit> runQuery(int duplicatesMin, int duplicatesMax, boolean showDuplicates) {
        return runQuery(duplicatesMin, duplicatesMax, previousMonth(), new Date(), showDuplicates);
    }

    private List<Submit> runQuery(int duplicatesMin, int duplicatesMax, Date from, Date to, boolean showDuplicates) {
        return runQuery(duplicatesMin, duplicatesMax, from, to, showDuplicates, "both", "ALL", Collections.singleton("web"), Collections.singleton("jsp editor"), false);
    }

    private List<Submit> runQuery(String status, String iz_stastus) {
        return runQuery(0, 10, previousMonth(), new Date(), true, status, iz_stastus, Collections.singleton("web"), Collections.singleton("jsp editor"), false);
    }

    private List<Submit> runQuery(Collection<String> components, Collection<String> subcomponents, boolean allSubcomponents) {
        return runQuery(0, 10, previousMonth(), new Date(), true, "both", "ALL", components, subcomponents, allSubcomponents);
    }

    private List<Submit> runQuery(int duplicatesMin, int duplicatesMax, Date from, Date to, boolean showDuplicates, String status, String iz_stastus,
            Collection<String> components, Collection<String> subcomponents, boolean allSubcomponents) {
        final Map<String, Object> params = new HashMap<String, Object>(10);
        long min = duplicatesMin;
        long max = duplicatesMax;
        params.put("min", min);
        params.put("max", max);
        addIfNotNull(params, "components", components);
        if (!allSubcomponents) {
            addIfNotNull(params, "subcomponents", subcomponents);
        }
        addIfNotNull(params, "from", from);
        addIfNotNull(params, "to", to);
        addIfNotNull(params, "dupFrom", from);
        addIfNotNull(params, "dupTo", to);
        return new ArrayList<Submit>(query.duplicatesQuery(em, getSearchType(), params, status, iz_stastus, showDuplicates));
    }

    void addIfNotNull(Map<String, Object> map, String key, Object value) {
        if (value != null) {
            map.put(key, value);
        }
    }

    private Date previousMonth() {
        return previousMonth(1);
    }

    private Date previousMonth(int count) {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.MONTH, -count);
        return cal.getTime();
    }

    // protected
    protected Submit createSubmit() {
        return createSubmit(Utils.getNextId(Exceptions.class));
    }

    protected Submit createSubmit(int id) {
        return new Exceptions(id);
    }

    DuplicatesQuery.SearchType getSearchType(){
        return DuplicatesQuery.SearchType.EXCEPTIONS;
    }
}


