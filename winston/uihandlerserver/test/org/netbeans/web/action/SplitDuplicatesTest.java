/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */
package org.netbeans.web.action;

import java.util.List;
import javax.persistence.EntityManager;
import org.netbeans.modules.exceptions.entity.Exceptions;
import org.netbeans.modules.exceptions.entity.Method;
import org.netbeans.modules.exceptions.entity.Report;
import org.netbeans.modules.exceptions.entity.Slowness;
import org.netbeans.modules.exceptions.entity.Submit;
import org.netbeans.server.uihandler.DatabaseTestCase;

/**
 *
 * @author Jindrich Sedek
 */
public class SplitDuplicatesTest extends DatabaseTestCase {

    public SplitDuplicatesTest(String str) {
        super(str);
    }

    public void testSplitDuplicates() throws Exception {
        SplitDuplicates splitDups = new SplitDuplicates();
        EntityManager em = perUtils.createEntityManager();
        em.getTransaction().begin();
        for (int i = 0; i < 10; i++) {
            createNewSubmit(em, i);
        }
        em.getTransaction().commit();
        em.close();
        em = perUtils.createEntityManager();
        Submit id1 = Submit.getById(em, 1);
        List<Submit> result = splitDups.splitDuplicates(new String[]{"2", "3", "4"}, "1");
        assertEquals(3, result.size());
        for (Submit sbm : result) {
            assertEquals(id1.getReportId(), sbm.getReportId());
        }

        result = splitDups.splitDuplicates(new String[0], "1");
        assertTrue(result.isEmpty());

        result = splitDups.splitDuplicates(new String[]{"1"}, "1");
        assertEquals(result.iterator().next(), id1);
        assertEquals(new Integer(1), id1.getReportId().getId());
        em.close();
        result = splitDups.splitDuplicates(new String[]{"2", "3", "4", "5", "9"}, "3");
        assertEquals(5, result.size());
        em = perUtils.createEntityManager();
        em.getTransaction().begin();
        Submit id3 = Submit.getById(em, 3);
        for (Submit exceptions : result) {
            assertEquals(id3.getReportId(), exceptions.getReportId());
            assertTrue(exceptions.getReportId().getDuplicateManChanged());
        }
        em.getTransaction().commit();
        em.close();
    }

    public void testSplitToNewReport() throws Exception {
        SplitDuplicates splitDups = new SplitDuplicates();
        EntityManager em = perUtils.createEntityManager();
        em.getTransaction().begin();
        for (int i = 0; i < 10; i++) {
            createNewSubmit(em, i);
        }
        em.getTransaction().commit();
        em.close();
        em = perUtils.createEntityManager();
        List<Submit> excs = splitDups.splitDuplicates(new String[]{"2", "3", "4"});
        assertEquals(3, excs.size());
        for (Submit exceptions : excs) {
            assertEquals(new Integer(10), exceptions.getReportId().getId());
        }
        
        excs = splitDups.splitDuplicates(new String[]{"2", "3", "4", "5", "9"});
        assertEquals(5, excs.size());
        for (Submit exceptions : excs) {
            assertEquals(new Integer(11), exceptions.getReportId().getId());
        }
        assertEquals("test", excs.get(0).getReportId().getComponent());
        assertEquals("testSub", excs.get(0).getReportId().getSubcomponent());

        em.close();
    }

    private Submit createNewSubmit(EntityManager em, int id) {
        Submit sbm;
        if (id%2 == 0){
             sbm = new Exceptions(id);
        }else{
            Slowness slown = new Slowness(id);
            Method m = new Method(id);
            m.setName("hallo" + id);
            em.persist(m);
            slown.setSuspiciousMethod(m);
            sbm = slown;
        }
        Report report = new Report(id);
        report.setComponent("test");
        report.setSubcomponent("testSub");
        sbm.setReportId(report);
        em.persist(report);
        em.persist(sbm);
        return sbm;
    }
}