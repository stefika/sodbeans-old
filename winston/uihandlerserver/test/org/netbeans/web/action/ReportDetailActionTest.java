/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2009 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */
package org.netbeans.web.action;

import java.util.List;
import javax.persistence.EntityManager;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.netbeans.modules.exceptions.entity.Nbuser;
import org.netbeans.modules.exceptions.entity.Report;
import org.netbeans.modules.exceptions.entity.ReportComment;
import org.netbeans.modules.exceptions.utils.PersistenceUtils;
import org.netbeans.server.uihandler.DatabaseTestCase;

/**
 *
 * @author Jindrich Sedek
 */
public class ReportDetailActionTest extends DatabaseTestCase {

    private EntityManager em;
    private Nbuser user, guest;
    private Report report;

    public ReportDetailActionTest(String name) {
        super(name);
    }

    @Before
    @Override
    protected void setUp() throws Exception {
        super.setUp();
        em = perUtils.createEntityManager();

        user = new Nbuser(1);
        user.setName("simpleUser");
        em.getTransaction().begin();
        em.persist(user);
        guest = new Nbuser(2);
        guest.setName("GUEST");
        em.persist(guest);

        report = new Report(1);
        em.persist(report);

        em.getTransaction().commit();

    }

    @After
    @Override
    protected void tearDown() throws Exception {
        em.close();
        super.tearDown();
    }

    @Test
    public void testAddComment() {
        ReportDetailAction action = new ReportDetailAction();
        action.addComment(em, user.getName(), report, "testComment");
        action.addComment(em, guest.getName(), report, "testComment22");

        em.getTransaction().begin();
        List<ReportComment> reports = PersistenceUtils.getAll(em, ReportComment.class);
        assertEquals(2, reports.size());
        assertEquals(user, reports.get(0).getNbuserId());
        assertEquals(guest, reports.get(1).getNbuserId());
        assertEquals("testComment", reports.get(0).getComment());
        assertEquals("testComment22", reports.get(1).getComment());
        em.getTransaction().commit();
    }

    @Test
    public void testAddCommentByNewUser() {
        ReportDetailAction action = new ReportDetailAction();
        action.addComment(em, "novy_user", report, "testComment");
        action.addComment(em, "novy2", report, "testComment22");

        em.getTransaction().begin();
        List<ReportComment> reports = PersistenceUtils.getInstance().getAll(em, ReportComment.class);
        assertEquals(2, reports.size());
        assertEquals("novy_user", reports.get(0).getNbuserId().getName());
        assertEquals("novy2", reports.get(1).getNbuserId().getName());
        assertEquals("testComment", reports.get(0).getComment());
        assertEquals("testComment22", reports.get(1).getComment());
        em.getTransaction().commit();
    }
}

