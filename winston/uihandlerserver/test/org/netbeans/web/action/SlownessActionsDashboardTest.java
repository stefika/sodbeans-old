/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */

package org.netbeans.web.action;

import java.util.List;
import javax.persistence.EntityManager;
import org.junit.Test;
import org.netbeans.modules.exceptions.entity.Method;
import org.netbeans.modules.exceptions.entity.Operatingsystem;
import org.netbeans.modules.exceptions.entity.Report;
import org.netbeans.modules.exceptions.entity.Slowness;
import org.netbeans.server.uihandler.DatabaseTestCase;

/**
 *
 * @author Jindrich Sedek
 */
public class SlownessActionsDashboardTest extends DatabaseTestCase {

    private static int slownessId = 1;
    private Report r1;
    private Report r2;
    private Method m;
    private Operatingsystem linux;
    private Operatingsystem solaris;
    private EntityManager em;
    
    public SlownessActionsDashboardTest(String name) {
        super(name);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        em = perUtils.createEntityManager();
        em.getTransaction().begin();
        m = new Method(1);
        m.setName("bla bla");
        em.persist(m);
        r1 = new Report(1);
        r2 = new Report(2);
        em.persist(r1);
        em.persist(r2);
        linux = new Operatingsystem(1);
        linux.setName("Linux");
        em.persist(linux);
        solaris = new Operatingsystem(2);
        solaris.setName("SunOS");
        em.persist(solaris);
    }

    @Override
    protected void tearDown() throws Exception {
        em.getTransaction().commit();
        em.close();
        super.tearDown();
    }

    @Test
    public void testPrepare() throws Exception {
        createNewSubmit(em, r1, m, "new File", 2000l);
        createNewSubmit(em, r1, m, "new File", 1000l);
        createNewSubmit(em, r1, m, "new File", 3000l);

        SlownessActionsDashboard dashBoard = new SlownessActionsDashboard();
        List<ResultBean> resultData = dashBoard.prepareData(em);
        assertEquals(1, resultData.size());
        SlownBean sb = resultData.iterator().next().getGlobal();
        verifyBean(sb, "new File", "3.00", "2.00", 3l);
    }

    @Test
    public void testPrepareMoreActions() throws Exception{
        createNewSubmit(em, r1, m, "new File", 2000l);
        createNewSubmit(em, r1, m, "new File", 1000l);
        createNewSubmit(em, r1, m, "new File", 3000l);

        createNewSubmit(em, r2, m, "new Project", 5000l);
        createNewSubmit(em, r2, m, "new Project", 2000l);
        createNewSubmit(em, r2, m, "new Project", 8000l);

        createNewSubmit(em, r1, m, "Options", 10000l);
        createNewSubmit(em, r2, m, "Options", 8000l);

        SlownessActionsDashboard dashBoard = new SlownessActionsDashboard();
        List<ResultBean> resultData = dashBoard.prepareData(em);

        verifyGlobaLogs(resultData);
    }

    public void testMoreOSes() throws Exception {
        createNewSubmit(em, r1, m, "new File", 2000l, solaris);
        createNewSubmit(em, r1, m, "new File", 1000l, linux);
        createNewSubmit(em, r1, m, "new File", 3000l, linux);

        createNewSubmit(em, r2, m, "new Project", 5000l, linux);
        createNewSubmit(em, r2, m, "new Project", 2000l, solaris);
        createNewSubmit(em, r2, m, "new Project", 8000l, solaris);

        createNewSubmit(em, r1, m, "Options", 10000l, solaris);
        createNewSubmit(em, r2, m, "Options", 8000l, solaris);
        List<ResultBean> resultData = new SlownessActionsDashboard().prepareData(em);
        verifyGlobaLogs(resultData);


        ResultBean optionsBean = resultData.get(0);
        SlownBean optForSol = optionsBean.getForOs(solaris.getName());
        verifyBean(optForSol, "Options", "10.00", "9.00", 2l);
        assertNull(optionsBean.getForOs(linux.getName()));

        ResultBean newFile = resultData.get(1);
        verifyBean(newFile.getForOs(solaris.getName()), "new File", "2.00", "2.00", 1l);
        verifyBean(newFile.getForOs(linux.getName()), "new File", "3.00", "2.00", 2l);
        assertNull(optionsBean.getForOs("Windows"));

        ResultBean newProject = resultData.get(2);
        verifyBean(newProject.getForOs(solaris.getName()), "new Project", "8.00", "5.00", 2l);
        verifyBean(newProject.getForOs(linux.getName()), "new Project", "5.00", "5.00", 1l);
    }


    private void verifyGlobaLogs(List<ResultBean> resultData){
        assertEquals(3, resultData.size());
        verifyBean(resultData.get(1).getGlobal(), "new File", "3.00", "2.00", 3l);
        verifyBean(resultData.get(2).getGlobal(), "new Project", "8.00", "5.00", 3l);
        verifyBean(resultData.get(0).getGlobal(), "Options", "10.00", "9.00", 2l);
    }

    private void verifyBean(SlownBean sb, String actionName, String max, String avg, long count){
        assertNotNull(sb);
        assertEquals(actionName, sb.getAction());
        assertEquals(max, sb.getMax());
        assertEquals(avg, sb.getAvg());
        assertEquals(Long.toString(count), sb.getCount());
    }

    private void createNewSubmit(EntityManager em, Report r, Method suspMethod, String action, Long actionTime) {
        createNewSubmit(em, r, suspMethod, action, actionTime, linux);
    }
    
    private void createNewSubmit(EntityManager em, Report r, Method suspMethod, String action, Long actionTime, Operatingsystem os) {
        Slowness sl = new Slowness(++slownessId);
        sl.setSuspiciousMethod(suspMethod);
        sl.setReportId(r);
        sl.setActionTime(actionTime);
        sl.setLatestAction(action);
        sl.setOperatingsystemId(os);
        em.persist(sl);
    }
}
