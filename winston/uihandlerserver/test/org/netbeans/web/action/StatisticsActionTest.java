/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */
package org.netbeans.web.action;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import javax.persistence.EntityManager;
import org.junit.Test;
import org.netbeans.modules.exceptions.entity.Exceptions;
import org.netbeans.modules.exceptions.entity.Report;
import org.netbeans.server.uihandler.BugzillaTestCase;
import org.netbeans.web.action.StatisticsAction.TableCell;

/**
 *
 * @author Jindrich Sedek
 */
public class StatisticsActionTest extends BugzillaTestCase {

    private int id = 0;

    public StatisticsActionTest(String name) {
        super(name);
    }

    @Test
    public void testEmpty(){
        ArrayList<TableCell> cellData = new StatisticsAction().recountData(true);
        assertNotNull(cellData);
        assertEquals(12, cellData.size());
        for (TableCell tableCell : cellData) {
            assertEquals(0, tableCell.getAllReports());
        }
    }

    @Test
    public void testRecount(){
        Calendar cal = Calendar.getInstance();
        addExc(cal.getTime());
        cal.roll(Calendar.DAY_OF_YEAR, false);
        addExc(cal.getTime());
        cal.roll(Calendar.DAY_OF_YEAR, false);
        Exceptions last = addExc(cal.getTime());
        addExc(cal.getTime(), last);
        addExc(cal.getTime(), last);
        addExc(cal.getTime(), last);

        ArrayList<TableCell> cellData = new StatisticsAction().recountData(true);
        assertNotNull(cellData);
        assertEquals(12, cellData.size());
        TableCell tableCell = cellData.get(0);
        assertEquals(6, tableCell.getAllReports());
        assertEquals(3, tableCell.getNonDuplicateReports());

        last = addExc(cal.getTime(), null, 1);
        addExc(cal.getTime(), null, 2);
        addExc(cal.getTime(), last, 3);
        last = addExc(cal.getTime(), last, 4);
        addExc(cal.getTime(), last, 4);

        cellData = new StatisticsAction().recountData(true);
        assertNotNull(cellData);
        assertEquals(12, cellData.size());
        tableCell = cellData.get(0);
        assertEquals(11, tableCell.getAllReports());
        assertEquals(5, tableCell.getNonDuplicateReports());
        assertEquals(0, tableCell.getResolved());
        assertEquals(0, tableCell.getFixed());
    }


    private Exceptions addExc(Date date){
        return addExc(date, null, null);
    }

    private Exceptions addExc(Date date, Exceptions duplicateOf){
        return addExc(date, duplicateOf, null);
    }

    private Exceptions addExc(Date date, Exceptions duplicateOf, Integer issuezillaId){
        EntityManager em = perUtils.createEntityManager();
        em.getTransaction().begin();
        Exceptions exc = new Exceptions(++id);

        exc.setReportdate(date);
        Report report = null;
        if (duplicateOf != null) {
            report = duplicateOf.getReportId();
        } else {
            report = new Report(id);
            em.persist(report);
        }
        exc.setReportId(report);
        if (issuezillaId != null) {
            report.setIssueId(issuezillaId);
            em.merge(report);
        }
        em.persist(exc);
        em.getTransaction().commit();
        em.close();
        return exc;
    }

}

