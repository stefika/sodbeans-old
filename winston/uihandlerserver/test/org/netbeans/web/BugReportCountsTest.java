/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2009 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */

package org.netbeans.web;

import java.util.List;
import javax.persistence.EntityManager;
import org.junit.Test;
import org.netbeans.modules.exceptions.entity.Exceptions;
import org.netbeans.modules.exceptions.entity.Logfile;
import org.netbeans.modules.exceptions.entity.Nbversion;
import org.netbeans.modules.exceptions.entity.ProductVersion;
import org.netbeans.modules.exceptions.entity.Report;
import org.netbeans.server.uihandler.DatabaseTestCase;
import static org.junit.Assert.*;

/**
 *
 * @author Jindrich Sedek
 */
public class BugReportCountsTest extends DatabaseTestCase {

    public BugReportCountsTest(String name) {
        super(name);
    }

    @Test
    public void testGetStats() {
        EntityManager em = perUtils.createEntityManager();
        em.getTransaction().begin();

        Nbversion nv = new Nbversion(1);
        nv.setVersion(Nbversion.DEV_VERSION);
        em.persist(nv);

        ProductVersion pw = new ProductVersion(1);
        pw.setNbversionId(nv);
        pw.setProductVersion("pw");
        em.persist(pw);

        Logfile lf = new Logfile(10);
        lf.setUploadNumber(0);
        lf.setUserdir("gestures");
        lf.setProductVersionId(pw);
        em.persist(lf);

        Report report = new Report(0);
        em.persist(report);

        Exceptions exc = new Exceptions(1);
        exc.setBuild(70927l);
        exc.setReportId(report);
        exc.setLogfileId(lf);
        em.persist(exc);

        exc = new Exceptions(2);
        exc.setBuild(70910l);
        exc.setReportId(report);
        exc.setLogfileId(lf);
        em.persist(exc);

        exc = new Exceptions(3);
        exc.setBuild(70928l);
        exc.setReportId(report);
        exc.setLogfileId(lf);
        em.persist(exc);

        em.getTransaction().commit();
        em.close();
        List<BugReportCounts.Data> result = BugReportCounts.getStats();
        assertNotNull(result);
        assertEquals(2, result.size());
        assertEquals(new BugReportCounts.Data(1l, 1l), result.get(0));
        assertEquals(new BugReportCounts.Data(2l, 2l), result.get(1));
    }

}