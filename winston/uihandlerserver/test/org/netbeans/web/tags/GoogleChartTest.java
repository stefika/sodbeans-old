/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */
package org.netbeans.web.tags;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.jsp.PageContext;
import org.netbeans.junit.NbTestCase;
import org.netbeans.server.uihandler.DatabaseTestCase;
import static junit.framework.Assert.*;

/**
 *
 * @author Jindrich Sedek
 */
public class GoogleChartTest extends NbTestCase{
    Logger LOG = Logger.getLogger(GoogleChartTest.class.getName());
    BarChart chart;

    public GoogleChartTest(String name) {
        super(name);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        LOG.setLevel(Level.WARNING);
        chart = new BarChart();
        chart.setCollection("collection");
        chart.setCategory("key");
        chart.setValue("value");
        chart.setSerie("serie");
    }

    public void testGetColorStringDefValues(){
        PageContext context = DatabaseTestCase.createPageContext();
        for (int size = 1; size< 30; size++){
            chart.setJspContext(context);
            String[] colors = chart.getColorsString(size).split(",");
            assertEquals(size, colors.length);
            for (String col : colors) {
                assertEquals(col.length(), 6);
            }
        }
    }

    public void testGetColorString(){
        PageContext context = DatabaseTestCase.createPageContext();
        List<Color> colors = new ArrayList<Color>();
        colors.add(Color.RED);
        colors.add(Color.GREEN);
        colors.add(Color.BLUE);
        colors.add(Color.BLACK);
        colors.add(Color.ORANGE);
        colors.add(Color.GRAY);
        colors.add(Color.CYAN);
        colors.add(Color.WHITE);
        context.setAttribute("colors", colors);
        chart.setColors("colors");
        chart.setJspContext(context);
        assertEquals("ff0000,00ff00,0000ff,000000,ffc800,808080,00ffff,ffffff", chart.getColorsString(colors.size()));
    }

    public static class DataType implements Comparable<DataType> {

        private String serie,  key;
                Integer value;

        public DataType(String serie, String key, Integer value) {
            this.serie = serie;
            this.key = key;
            this.value = value;
        }

        public String getKey() {
            return key;
        }

        public String getSerie() {
            return serie;
        }

        public Integer getValue() {
            return value;
        }

        public int compareTo(DataType other) {
            return key.compareTo(other.key);
        }
    }
}
