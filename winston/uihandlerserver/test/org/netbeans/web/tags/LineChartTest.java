/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */

package org.netbeans.web.tags;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.logging.Logger;
import javax.servlet.jsp.PageContext;
import org.junit.Test;
import org.netbeans.junit.NbTestCase;
import org.netbeans.server.uihandler.DatabaseTestCase;
import static org.netbeans.web.tags.BarChartTest.*;

/**
 *
 * @author Jindrich Sedek
 */
public class LineChartTest extends NbTestCase{
    Logger LOG = Logger.getLogger(LineChartTest.class.getName());
    LineChart chart;

    public LineChartTest(String str) {
        super(str);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        chart = new LineChart();
        chart.setCollection("collection");
        chart.setCategory("key");
        chart.setValue("value");
        chart.setSerie("serie");
    }

    @Test
    public void testSimpleChart()throws Exception{
        PageContext context = DatabaseTestCase.createPageContext();
        Collection<DataType> coll = Collections.singleton(new DataType("s1", 5, 10));
        context.setAttribute("collection", coll);
        chart.setShowlegend(false);
        chart.setJspContext(context);
        String str = chart.prepareResultString();
        LOG.info(str);
        assertResolution(str, "500x200");
        assertTrue("graph type: " + str, str.contains("cht=lxy"));
        assertColors(str, "b34747");
        assertData(str, "t:100|100");
        assertFalse("no legend", str.contains("chdl="));
    }

    @Test
    public void testSerie()throws Exception{
        String title = "Hello World!";
        PageContext context = DatabaseTestCase.createPageContext();
        context.setAttribute("collection", getSerieData());
        chart.setJspContext(context);
        chart.setTitle(title);
        String str = chart.prepareResultString();
        LOG.info(str);
        assertResolution(str, "500x200");
        assertTrue("graph type: " + str, str.contains("cht=lxy"));
        assertColors(str, "b34747,47b347,4747b3");
        assertData(str, "t:0,25,50|0,5,1|25,50,75|0,1,9|0,50,100|2,100,5");
        assertTrue("graph title: " + str, str.contains("chtt=" + title));
        assertTrue("legend text" + str, str.contains("chdl=s1|s2|s3"));
        assertLegend(str, "0:|1|2|3|4|5|1:|0|311|622|933|1555");
    }

    @Test
    public void testMultivalue()throws Exception{
        PageContext context = DatabaseTestCase.createPageContext();
        Collection<DataType> coll = new ArrayList<DataType>(1);
        coll.add(new DataType("s1", 1, 2));
        coll.add(new DataType("s1", 1, 3));
        coll.add(new DataType("s1", 100, 100));
        context.setAttribute("collection", coll);
        chart.setJspContext(context);
        String str = chart.prepareResultString();
        LOG.info(str);
        assertData(str, "t:1,100|3,100");
    }

    @Test
    public void testRemoveDuplicitKeys()throws Exception{
        PageContext context = DatabaseTestCase.createPageContext();
        Collection<DataType> coll = new ArrayList<DataType>(1);
        coll.add(new DataType("s1", 10, 1));
        coll.add(new DataType("s1", 11, 50));//should not be in result
        coll.add(new DataType("s1", 1000, 100));
        context.setAttribute("collection", coll);
        chart.setJspContext(context);
        String str = chart.prepareResultString();
        LOG.info(str);
        assertData(str, "t:1,100|1,100");
    }

    @Test
    public void testScale()throws Exception{
        PageContext context = DatabaseTestCase.createPageContext();
        Collection<DataType> coll = new ArrayList<DataType>(1);
        coll.add(new DataType("s1", 100, 100));
        coll.add(new DataType("s1", 110, 80));
        coll.add(new DataType("s1", 120, 90));
        context.setAttribute("collection", coll);
        chart.setJspContext(context);
        String str = chart.prepareResultString();
        LOG.info(str);
        assertData(str, "t:0,50,100|100,0,50");
        assertLegend(str, "0:|100|104|108|112|120|1:|80|84|88|92|100");
    }

    private Collection<DataType> getSerieData() {
        Collection<DataType> coll = new ArrayList<DataType>(10);
        coll.add(new DataType("s1", 1, 10));
        coll.add(new DataType("s1", 2, 80));
        coll.add(new DataType("s1", 3, 20));
        coll.add(new DataType("s2", 2, 12));
        coll.add(new DataType("s2", 3, 20));
        coll.add(new DataType("s2", 4, 150));
        coll.add(new DataType("s3", 1, 40));
        coll.add(new DataType("s3", 3, 1555));
        coll.add(new DataType("s3", 5, 85));
        return coll;
    }

}