/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */
package org.netbeans.web.tags;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.jsp.PageContext;
import org.netbeans.junit.NbTestCase;
import org.netbeans.server.uihandler.DatabaseTestCase;
import static junit.framework.Assert.*;
import static java.awt.Color.*;

/**
 *
 * @author Jindrich Sedek
 */
public class BarChartTest extends NbTestCase{
    Logger LOG = Logger.getLogger(BarChartTest.class.getName());
    BarChart chart;

    public BarChartTest(String name) {
        super(name);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        LOG.setLevel(Level.WARNING);
        chart = new BarChart();
        chart.setCollection("collection");
        chart.setCategory("key");
        chart.setValue("value");
        chart.setSerie("serie");
    }

    public void testScale(){
        Map<Object, BarChart.SerieData> series = chart.convert(getSerieData());
        assertEquals("no stacked", new Long(1555), chart.scale(series));
        chart.setStacked(true);
        assertEquals("stacked", new Long(1655), chart.scale(series));
    }

    public void testScaleKeyIsSerie(){
        Collection<DataType> coll = new ArrayList<DataType>(10);
        coll.add(new DataType("s1", "s1", 10));
        coll.add(new DataType("s2", "s2", 80));
        Map<Object, BarChart.SerieData> series = chart.convert(coll);
        assertEquals("no stacked", new Long(80), chart.scale(series));
        chart.setStacked(true);
        assertEquals("stacked", new Long(80), chart.scale(series));
    }

    public void testSimpleChart() throws Exception {
        PageContext context = DatabaseTestCase.createPageContext();
        Collection<DataType> coll = Collections.singleton(new DataType("s1", "key", 10));
        context.setAttribute("collection", coll);
        chart.setJspContext(context);
        String str = chart.prepareResultString();
        LOG.info(str);
        assertColors(str, "b34747");
        assertResolution(str, "500x200");
        assertData(str, "t:10");
        assertLegend(str, "0:|key|");
        assertColumnWidth(str, "439,4,6");
        assertLegendPossition(str, "t");
        assertVertical(str);
    }

    public void testOneSerie() throws Exception{
        String serie = "s1";
        PageContext context = DatabaseTestCase.createPageContext();
        Collection<DataType> coll = new ArrayList<DataType>(10);
        coll.add(new DataType(serie, "a", 10));
        coll.add(new DataType(serie, "b", 20));
        coll.add(new DataType(serie, "c", 40));
        coll.add(new DataType(serie, "d", 80));
        coll.add(new DataType(serie, "e", 15));
        context.setAttribute("collection", coll);
        Collection<Color> colors = Collections.singleton(RED);
        context.setAttribute("colors", colors);
        chart.setJspContext(context);
        String str = chart.prepareResultString();
        LOG.info(str);
        assertColors(str, "b34747");
        assertResolution(str, "500x200");
        assertData(str, "t:12,25,50,100,18");
        assertLegend(str, "0:|a|b|c|d|e|");
        assertColumnWidth(str, "83,4,6");
        assertLegendPossition(str, "t");
        assertVertical(str);
    }

    public void testOneSerieOrder() throws Exception{
        String serie = "s1";
        PageContext context = DatabaseTestCase.createPageContext();
        Collection<DataType> coll = new ArrayList<DataType>(10);
        coll.add(new DataType(serie, "b", 20));
        coll.add(new DataType(serie, "c", 40));
        coll.add(new DataType(serie, "a", 10));
        coll.add(new DataType(serie, "e", 15));
        coll.add(new DataType(serie, "d", 80));
        context.setAttribute("collection", coll);
        Collection<Color> colors = Collections.singleton(RED);
        context.setAttribute("colors", colors);
        chart.setJspContext(context);
        String str = chart.prepareResultString();
        LOG.info(str);
        assertData(str, "t:25,50,12,18,100");
        assertLegend(str, "0:|b|c|a|e|d|");
        assertVertical(str);
    }

    public void testMoreSeries()throws Exception{
        PageContext context = DatabaseTestCase.createPageContext();
        context.setAttribute("collection", getSerieData());
        Collection<Color> colors = new ArrayList<Color>();
        colors.add(BLACK);
        colors.add(RED);
        colors.add(GREEN);
        colors.add(GRAY);
        context.setAttribute("colors", colors);
        chart.setColors("colors");
        chart.setJspContext(context);
        String str = chart.prepareResultString();
        LOG.info(str);
        assertColors(str, "000000,ff0000,00ff00,808080");
        assertResolution(str, "500x200");
        assertData(str, "t:0,5,1|0,1,9|2,100,5");
        assertLegend(str, "0:|a|b|c|");
        assertColumnWidth(str, "43,4,6");
        assertLegendPossition(str, "t");
        assertVertical(str);
    }

    public void testMoreSeriesOrder()throws Exception{
        PageContext context = DatabaseTestCase.createPageContext();
        Collection data = getSerieData();
        Object[] dataArr = data.toArray();
        Object pom = dataArr[2];
        dataArr[2] = dataArr[1];
        dataArr[1] = pom;
        context.setAttribute("collection", Arrays.asList(dataArr));
        chart.setJspContext(context);
        String str = chart.prepareResultString();
        LOG.info(str);
        assertData(str, "t:0,1,5|0,9,1|2,5,100");
        assertLegend(str, "0:|a|c|b|");
    }

    public void testHorizontal() throws Exception{
        PageContext context = DatabaseTestCase.createPageContext();
        context.setAttribute("collection", getSerieData());
        chart.setOrientation("HORIZONTAL");
        chart.setResolution("400x200");
        chart.setJspContext(context);
        String str = chart.prepareResultString();
        LOG.info(str);
        assertHorizontal(str);
        assertStacked(str, false);
        assertResolution(str, "400x200");
        assertData(str, "t:0,5,1|0,1,9|2,100,5");
        assertLegend(str, "1:|c|b|a|");
        assertColumnWidth(str, "9,4,6");
        assertLegendPossition(str, "r");
    }

    public void testStacked() throws Exception{
        PageContext context = DatabaseTestCase.createPageContext();
        context.setAttribute("collection", getSerieData());
        chart.setOrientation("VERTICAL");
        chart.setResolution("400x200");
        chart.setStacked(true);
        chart.setJspContext(context);
        String str = chart.prepareResultString();
        LOG.info(str);
        assertVertical(str);
        assertResolution(str, "400x200");
        assertData(str, "t:0,4,1|0,1,9|2,93,5");
        assertStacked(str, true);
        assertLegendPossition(str, "t");
        assertColumnWidth(str, "111,4,6");
    }

    public void testDefaultLegendPossition() throws Exception{
        PageContext context = DatabaseTestCase.createPageContext();
        context.setAttribute("collection", getSerieData());
        chart.setOrientation("horizontal");
        chart.setJspContext(context);
        String str = chart.prepareResultString();
        LOG.info(str);
        assertTrue("\n"+str+"\n", str.contains("chdlp=r"));
    }

    public void testLegendPossition() throws Exception{
        PageContext context = DatabaseTestCase.createPageContext();
        context.setAttribute("collection", getSerieData());
        chart.setOrientation("vertical");
        chart.setJspContext(context);
        String str = chart.prepareResultString();
        LOG.info(str);
        assertTrue("\n"+str+"\n", str.contains("chdlp=t"));

        chart.setLegendposition("left");
        chart.setJspContext(context);
        str = chart.prepareResultString();
        LOG.info(str);
        assertTrue("\n"+str+"\n", str.contains("chdlp=l"));

        chart.setLegendposition("right");
        chart.setJspContext(context);
        str = chart.prepareResultString();
        LOG.info(str);
        assertTrue("\n"+str+"\n", str.contains("chdlp=r"));

        chart.setLegendposition("top");
        chart.setJspContext(context);
        str = chart.prepareResultString();
        LOG.info(str);
        assertTrue("\n"+str+"\n", str.contains("chdlp=t"));

        chart.setLegendposition("bottom");
        chart.setJspContext(context);
        str = chart.prepareResultString();
        LOG.info(str);
        assertTrue("\n"+str+"\n", str.contains("chdlp=b"));
    }

    static void assertData(String str, String expectedResult) {
        assertTrue("\n"+str+"\n", str.contains("chd="+expectedResult));
    }

    static void assertLegendPossition(String str, String expectedResult) {
        assertTrue("\n"+str+"\n", str.contains("chdlp="+expectedResult));
    }

    static void assertColumnWidth(String str, String expectedResult) {
        assertTrue("\n"+str+"\n", str.contains("chbh="+expectedResult));
    }

    static void assertLegend(String str, String expectedResult) {
        assertTrue("\n"+str+"\n", str.contains("chxl="+expectedResult));
    }

    static void assertColors(String str, String expectedResult) {
        assertTrue("\n"+str+"\n", str.contains("chco="+expectedResult));
    }

    static void assertResolution(String str, String expectedResult) {
        assertTrue("\n"+str+"\n", str.contains("chs="+expectedResult));
    }

    static void assertHorizontal(String str) {
        assertTrue("\n"+str+"\n", str.contains("bh"));
    }

    static void assertVertical(String str) {
        assertTrue("\n"+str+"\n", str.contains("bv"));
    }

    private void assertStacked(String str, boolean isStacked) {
        if (isStacked){
            assertTrue("\n"+str+"\n", str.matches(".*b.s.*"));
        }else{
            assertTrue("\n"+str+"\n", str.matches(".*b.g.*"));
        }
    }

    public static class DataType {

        private String serie;
                Object key;
                Integer value;

        public DataType(String serie, Object key, Integer value) {
            this.serie = serie;
            this.key = key;
            this.value = value;
        }

        public Object getKey() {
            return key;
        }

        public String getSerie() {
            return serie;
        }

        public Integer getValue() {
            return value;
        }
    }

    private Collection<DataType> getSerieData() {
        Collection<DataType> coll = new ArrayList<DataType>(10);
        coll.add(new DataType("s1", "a", 10));
        coll.add(new DataType("s1", "b", 80));
        coll.add(new DataType("s1", "c", 20));
        coll.add(new DataType("s2", "a", 12));
        coll.add(new DataType("s2", "b", 20));
        coll.add(new DataType("s2", "c", 150));
        coll.add(new DataType("s3", "a", 40));
        coll.add(new DataType("s3", "b", 1555));
        coll.add(new DataType("s3", "c", 85));
        return coll;
    }
}
