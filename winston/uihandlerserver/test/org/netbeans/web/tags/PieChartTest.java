/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */
package org.netbeans.web.tags;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.servlet.jsp.PageContext;
import org.netbeans.junit.NbTestCase;
import org.netbeans.server.uihandler.DatabaseTestCase;
import static org.netbeans.web.tags.BarChartTest.*;

/**
 *
 * @author Jindrich Sedek
 */
public class PieChartTest extends NbTestCase {

    public PieChartTest(String name) {
        super(name);
    }

    public void testMapData() throws Exception {
        PageContext context = DatabaseTestCase.createPageContext();
        Map<String, Integer> mapa = new LinkedHashMap<String, Integer>();
        mapa.put("piece1", 11);
        mapa.put("piece2", 30);
        mapa.put("piece3", 1);
        mapa.put("piece4", 50);
        mapa.put("piece5", 120);
        context.setAttribute("collection", mapa);
        PieChart chart = new PieChart();
        chart.setCollection("collection");
        chart.setCategory("key");
        chart.setValue("value");
        chart.setJspContext(context);
        String str = chart.prepareResultString();
        assertColors(str, "b34747,7db347,47b3b3,7d47b3");
        assertData(str, "t:5,14,23,56");
        assertResolution(str, "500x200");
        assertTitles(str, "piece1 (5%)|piece2 (14%)|piece4 (23%)|piece5 (56%)");
    }

    public void testTagCollection() throws Exception {
        PageContext context = DatabaseTestCase.createPageContext();
        Collection<DataType> coll = Collections.singleton(new DataType());
        context.setAttribute("collection", coll);
        PieChart chart = new PieChart();
        chart.setCollection("collection");
        chart.setCategory("key");
        chart.setValue("value");
        chart.setJspContext(context);
        String str = chart.prepareResultString();
        assertColors(str, "b34747");
        assertData(str, "t:100");
        assertResolution(str, "500x200");
        assertTitles(str, "key (100%)");
    }

    static void assertTitles(String str, String expectedResult){
        assertTrue("\n" + str + "\n", str.contains("chl="+ expectedResult)); // legend
    }

    private static class DataType {

        public String getDisplayName(){
            return "displayName";
        }
        public String getKey() {
            return "key";
        }

        public Integer getValue() {
            return 11;
        }
    }
}