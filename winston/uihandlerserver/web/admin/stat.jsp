<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 
<%@include file="/WEB-INF/jspf/header.jspf" %>
<%--
The taglib directive below imports the JSTL library. If you uncomment it,
you must also add the JSTL library to the project. The Add Library... action
on Libraries node in Projects view can be used to add the JSTL 1.1 library.
--%>

<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%> 
<%@taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@taglib prefix="exc" uri="/WEB-INF/tlds/Exc"%>

<p class="tasknav">
              <a href="list.do?query">Query</a> | <a href="list.do?unmapped">Unmapped</a> | <a href="list.do?reset">All</a>
          </p>
<div class="h2">
<h2>Exceptions</h2>

  
<table>
    <tr>
        <th>
            <a href='list.do?order=id'>Id</a>
        </th>
        <th>
            <a href='list.do?order=summary'>Summary</a>
        </th>
        <th>
            <a href='list.do?order=component'>Component</a>
        </th>
        <th>
            <a href='list.do?order=subcomponent'>Subcomponent</a>
        </th>
        <th>
            <a href='list.do?order=duplicates'>Duplicates</a>
        </th>
        <th>
            <a href='list.do?order=build'>Build</a>
        </th>
        <th>
            <a href='list.do?order=issuezillaid'>Bugzilla</a>
        </th>        
    </tr>

    <c:forEach var="issue" items='${exceptions}' varStatus="stat">
        <tr class="${(stat.index % 2) == 0 ? "b" : "a"}">
            <td>
                <a href='exception.do?id=${issue.id}'>
                    ${issue.id}                    
                </a>
            </td>
            <td>
                <span class="summary">${fn:substring(issue.summary,0,60)}</span>
            </td>
            <td>
                <span class="component">${issue.component}</span>
            </td>
            <td>
                <span class="subcomponent">${issue.subcomponent}</span>
            </td>
            <td>
                <exc:duplicates submit='${issue}'/>
            </td>
            <td>
               ${exc:buildNumberFormat(issue.latestBuild)} 

            </td>
            <td>
                <exc:issuezilla exceptions='${issue}'/>
            </td>
            
        </tr>  
    </c:forEach>
</table>
count: ${fn:length(exceptions)}<br/>
</div>
<%@include file="/WEB-INF/jspf/footer.jspf" %>