<%-- 
    Document   : directUser
    Created on : Feb 2, 2009, 3:20:41 PM
    Author     : michal
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@include file="/WEB-INF/jspf/header.jspf" %>
<%@ taglib prefix="exc" uri="/WEB-INF/tlds/Exc"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ page import="java.util.List" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ page language="java" contentType="text/html" %>
<%@ page import="java.util.*" %>


<script language='javascript' type='text/javascript' src='/resources/detaillog.js'></script>

<body>
    <br/>

    <FORM METHOD="POST">
        <B>Add record #:</B>
        name: <INPUT TYPE="TEXT" NAME="adUser" SIZE="7">&nbsp;&nbsp;
        prefix<INPUT TYPE="TEXT" NAME="adPrefix" SIZE="7">&nbsp;&nbsp;
        <input type="submit" value="send">
    </FORM><br/><br/>

    <h2> List of all user </h2>

    <table border="1px">
        <tr>
            <th>direct user</th>
            <th>direct prefix</th>
            <th>edit user</th>
            <th>edit prefix</th>
            <th>delete user</th>
        </tr>
        <c:forEach var="word" items='${allUsers}' >
            <tr>
                <td>${word.name}</td>
                <td>${word.prefix}</td>
                <td>
                    <a href='#'  onclick='return editUser("${word.name}");'>edit username</a>
                </td>
                <td>
                    <a href='#'  onclick='return editPrefix("${word.name}");'>edit prefix</a>
                </td>
                <td>
                    <a href='#' onclick='return deleteRecord("${word.name}");'>delete</a>
                </td>


            </tr>
        </c:forEach>
    </table>

<%--
    <div>
        <FORM ACTION="http://localhost:8888/analytics/directUser.do" METHOD="GET">
            <li><B>Search for record #:</B>
            <INPUT TYPE="TEXT" NAME="id" SIZE="7">&nbsp;&nbsp;
            <input type="submit" value="send">
        </FORM>

        <br/> found users:
        <c:forEach var="word" items='${record}' >
        ${word}<br/>
        </c:forEach>
        <br/>


        <FORM ACTION="http://localhost:8888/analytics/directUser.do" METHOD="POST">
            <li><B>Add record #:</B>
            name: <INPUT TYPE="TEXT" NAME="adUser" SIZE="7">&nbsp;&nbsp;
            prefix<INPUT TYPE="TEXT" NAME="adPrefix" SIZE="7">&nbsp;&nbsp;
            <input type="submit" value="send">
        </FORM><br/><br/>

        <FORM ACTION="http://localhost:8888/analytics/directUser.do" METHOD="GET">
            <li><B>Delete record #:</B>
            <INPUT TYPE="TEXT" NAME="delUser" SIZE="7">&nbsp;&nbsp;
            <input type="submit" value="send">
        </FORM><br/><br/>

        <FORM ACTION="http://localhost:8888/analytics/directUser.do" METHOD="GET">
            <li><B>Modify record #:</B>old name
            <INPUT TYPE="TEXT" NAME="modUser" SIZE="7">&nbsp;&nbsp;
            new name<INPUT TYPE="TEXT" NAME="newUser" SIZE="7">&nbsp;&nbsp;
            <input type="submit" value="send">
        </FORM><br/><br/>
        
    </div>

    --%>

</body>

<%@include file="/WEB-INF/jspf/footer.jspf" %>