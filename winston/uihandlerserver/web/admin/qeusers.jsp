<%@ page import="java.util.List,org.netbeans.modules.exceptions.utils.PersistenceUtils,org.netbeans.modules.exceptions.entity.Directuser" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@include file="/WEB-INF/jspf/header.jspf" %>
<%@taglib prefix="exc" uri="/WEB-INF/tlds/Exc"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%> 


<%  
    List<Directuser> users = PersistenceUtils.getInstance().getAll(Directuser.class);
    request.setAttribute("directusers", users);
    
%>

<h3>User with enabled direct reporting</h3>
<table>
    <tr>
        <th style="width: 200px">User name</th>
        <th style="width: 200px">Message prefix</th>
    </tr>
    <c:forEach var="directuser" items='${directusers}'>
    <tr>
        <td>${directuser.name}</td>
        <td>${directuser.messagePrefix}</td>
    </tr>
    </c:forEach>
</table>

<%@include file="/WEB-INF/jspf/footer.jspf" %>