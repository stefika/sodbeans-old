<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 
<%@include file="/WEB-INF/jspf/header.jspf" %>
<%--
The taglib directive below imports the JSTL library. If you uncomment it,
you must also add the JSTL library to the project. The Add Library... action
on Libraries node in Projects view can be used to add the JSTL 1.1 library.
--%>

<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%> 
<%@taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@taglib prefix="exc" uri="/WEB-INF/tlds/Exc"%>

<p>&nbsp;</p>

<div id="tlogin" class="application">

   <p>New user? <a href="http://www.netbeans.org/servlets/Join">Register for an account</a> to receive a password and log in.</p>
  
<form id="loginform" action="j_security_check" method="post">

<table class="axial">
 <tr>
  <th>Username or email</th>
  <td><input type="text" name="j_username" value="" size="20" maxlength="99" />
  </td>
 </tr>
 <tr>
  <th>Password</th>
   <td><input type="password" name="j_password" value="" size="20" maxlength="32" />
      <a href="http://www.netbeans.org/servlets/PasswordRequest">Forgot your password?</a>
      </td> </tr>
</table>
 <div class="functnbar3">
  <input type="submit" name="Login" value="Login" />
 </div>

</form>


</div>


<%@include file="/WEB-INF/jspf/footer.jspf" %>