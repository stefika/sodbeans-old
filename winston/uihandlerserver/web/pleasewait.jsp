<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>

<%@include file="/WEB-INF/jspf/header.jspf" %>

<%--
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 
--%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<div class="f-page-cell bg-sky" >
    <h1>Please wait</h1>
    
    <p>
        while processing your report.
    </p>
</div>


<%@include file="/WEB-INF/jspf/footer.jspf" %> 