<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>

<%@include file="/WEB-INF/jspf/header.jspf" %>

<div id="tlogin" class="application">

   <p>New user? <a href="http://www.netbeans.org/servlets/Join">Register for an account</a> to receive a password and log in.</p>
  
<form id="loginform" action="http://www.netbeans.org/servlets/TLogin" method="post">

<table class="axial">
 <tr>
  <th>Username or email</th>
  <td><input type="text" name="loginID" id="loginID" value="" size="20" maxlength="99" />
  </td>
 </tr>
 <tr>
  <th>Password</th>
   <td><input type="password" name="password" id="password" value="" size="20" maxlength="32" />
      <a href="http://www.netbeans.org/servlets/PasswordRequest">Forgot your password?</a>
      </td> </tr>
</table>
 <div class="functnbar3">
  <input type="submit" name="Login" value="Login" />
 </div>

</form>


</div>


<%@include file="/WEB-INF/jspf/footer.jspf" %>