<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 
<%@taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@include file="/WEB-INF/jspf/header.jspf" %>

<jsp:useBean id="sourcejarMappings" scope="request" class="org.netbeans.web.action.beans.MappingsBean"/>


<h3>Create new mapping</h3>
<html:form action="/admin/sourcejarmapping">
    <table>
        <tr>
            <td>Sourcejar: </td>
            <td>
                <html:text property="sourcejar"/>
            </td>
        </tr>
        <tr>
            <td>Component: </td>
            <td>
                <html:text property="component"/>
            </td>
        </tr>
        <tr>
            <td>Subcomponent: </td>
            <td>
                <html:text property="subcomponent"/>
            </td>
        </tr>
    </table>
    <div style="color: chocolate">
        <html:errors />
    </div>
    <html:submit value="Create"/>
</html:form>
<hr/>
<h2>Sourcejar Mappings</h2>
<table>
    <thead>
        <tr>
            <th>Sourcejar</th>
            <th>Component</th>
            <th>Subcomponent</th>
        </tr>
    </thead>
    <tbody>
        <c:forEach var="element" items="${sourcejarMappings.mappings}">
            <tr>
                <td>${element.sourcejar}</td>
                <td>${element.component}</td>
                <td>${element.subcomponent}</td>
                <td>
                    <a href="sourcejarmapping.do?drop=${element.sourcejar}">drop</a>
                </td>
            </tr>
        </c:forEach>
    </tbody>
</table>

<%@include file="/WEB-INF/jspf/footer.jspf" %>
