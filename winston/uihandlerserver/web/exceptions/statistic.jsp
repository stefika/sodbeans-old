<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 

<c:set var="path" value='/ <a href="../index.jsp">Analytics</a> / Exceptions / Statistics'/>
<%@include file="/WEB-INF/jspf/header.jspf" %>
<%--
The taglib directive below imports the JSTL library. If you uncomment it,
you must also add the JSTL library to the project. The Add Library... action
on Libraries node in Projects view can be used to add the JSTL 1.1 library.
--%>

<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%> 
<%@taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@taglib prefix="exc" uri="/WEB-INF/tlds/Exc"%>
<%@taglib prefix="ui" uri="/WEB-INF/statistics.tld" %>

<div class="h2">
    <h2>Reporting Statistics</h2>
    <table>
        
        <c:forEach var="cell" items='${data}' varStatus="stat">
        <c:if test="${(stat.index % 4) == 0}">
        <tr>
            <td>
                <table>
                    <tr>
                        <th>
                            Date:
                        </th>
                    </tr>
                    <tr>
                        <th>
                            All Reports (exceptions/slowness):
                        </th>
                    </tr>
                    <tr>
                        <th>
                            Non Duplicate Reports:
                        </th>
                    </tr>
                    <tr>
                        <th>
                            Reports in Bugzilla:
                        </th>
                    </tr>
                    <tr>
                        <th>
                            Resolved,Verified,Closed:
                        </th>
                    </tr>
                    <tr>
                        <th>
                            Fixed:
                        </th>
                    </tr>
                </table>
            </td>
        
        </c:if>
            <td>
                <table>
                    <tr>
                        <th>
                            ${cell.dateString}
                        </th>
                    </tr>
                    <tr>
                        <td>
                            ${cell.allReports} (${cell.allExceptions}/${cell.allSlownesses})
                        </td>
                    </tr>
                    <tr>
                        <td>
                            ${cell.nonDuplicateReports}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a href='${cell.issuesURL}'>${cell.issuesCount}</a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            ${cell.resolved}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            ${cell.fixed}
                        </td>
                    </tr>
                </table>
            </td>
        <c:if test="${(((stat.index + 1) % 4) == 0) || (stat.last)}">
        </tr>
        
        </c:if>
        </c:forEach>
    </table>
    <c:if test="${submitsProgress != null}">
        <%
            pageContext.setAttribute("submitsProgress", request.getAttribute("submitsProgress"));
        %>
        <h2>Number of reports by build date</h2>
        <ui:line collection="submitsProgress"
            category="build"
            value="count"
            showlegend="false"
            resolution="900x300"
            serie="serie"
            showxaxislabel="false"
       />
    <p>
        The graph describes how many exception reports were reported for development builds from the beginning of
        the exeption reporter project until the present. The data are grouped by week. Release versions are not counted.
    </p>
    </c:if>
</div>
<%@include file="/WEB-INF/jspf/footer.jspf" %>
