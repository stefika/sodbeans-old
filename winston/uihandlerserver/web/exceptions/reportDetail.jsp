<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 
<c:set var="title" value="Report \#${report.id}"/>
<%@include file="/WEB-INF/jspf/header.jspf" %>
<%@ taglib prefix="exc" uri="/WEB-INF/tlds/Exc"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%> 
<%@ page import="java.util.List" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>

<script language='javascript' type='text/javascript' src='exceptions/components.js'>
</script>

<script type="text/javascript" language="javascript">

    var dialog;

    function updateReport(callback) {
        $.getJSON("${pageContext.request.contextPath}/report-update?id=${report.id}",
        function(data){
            $('#component').text(data.report.component);
            $('#subcomponent').text(data.report.subcomponent);
            if (data.report.issueId)
                $('#issueId').text(data.report.issueId);
            callback();
        });
    }

    function saveComp() {
        var component = $("select[name='component']  :selected").val();
        var subcomponent = $("select[name='subcomponent'] :selected").val();
        var issueId = $("input[name='issueId']").val();
        $.get("${pageContext.request.contextPath}/report-update", {
            id: "${report.id}",
            component: component,
            subcomponent: subcomponent,
            issueId: issueId
        }, function () {
            updateEditable();
        });
    }

    function updateSubcomponent() {
        var component = $("select[name='component'] :selected").val();
        var subcomponent = $("select[name='subcomponent']");
        $.get("${pageContext.request.contextPath}/subcomponents", {
            component: component
        }, function (data) {
            subcomponent.empty();
            list = data.split(",");
            for (i=0;i<list.length;i++) {
                subcomponent.append("<option value='" + list[i] + "'>" + list[i] + "</option>");
            }
            var comp = "select[name='subcomponent'] option[value='" + $('#subcomponent').text() + "']";
            $(comp).attr('selected', 'selected');
        }, 'text');
    }

    function askComp() {
        updateReport(function () {
            options = jQuery.extend({modal: true, closeable: false},
            {},
            {show: true, unloadOnHide: true});

            var body = jQuery('<div></div>').append("<p><table>"
                + "<tr><td>Component:</td><td><select name='component' style='width: 180px;'>"
                + "<c:forEach var='component' items='${components}' varStatus='stat'>"
                + "  <option value='${component}'>${component}</option>"
                + "</c:forEach>"
                + "</select></td></tr>"
                + "<tr><td>Subcomponent:</td><td><select name='subcomponent'/></td></tr>"
                + "<tr><td>Issue:</td><td><input type='text' name='issueId' value=''></td></tr>"
                + "</table></p>");

            var buttons = jQuery('<form class="answers"></form>');
            buttons.html("<input type='button' value='Cancel'/><input type='button' name='save' value='Save'/>");
            jQuery('input[type=button]', buttons).click(function() {
                if (this.name == 'save') {
                    saveComp();
                }
                Boxy.get(this).hide(function() {
                });
            });

            body.append(buttons);
            new Boxy(body, options);

            var comp = "select option[value='" + $('#component').text() + "']";
            $(comp).attr('selected', 'selected');
            $("input[name='issueId']").value = $('#issueId').text();
            $("select[name='component']").change(function () {
                updateSubcomponent();
            });
            updateSubcomponent();
        });
    }

    function updateEditable(){
        $("#editableBox").load("${pageContext.request.contextPath}/exceptions/reportDetailEditable.jsp?id=${report.id}");
    }
        
    $(document).ready(function() {
        dialog = new Boxy("&nbsp;", {
            title: "&nbsp;",
            show: false
        });

        dialog.resize(500, 300, null);

        $(window).resize(function(){
            dialog.center();
        });
        updateEditable();
    });

</script>

<div id="issuezilla" class="application">
    <!--BEGINNING OF H2 CLASS-->

    <h2>Report #${report.id}</h2>


    <div>
        <div style="background-color:#d6e6ee;padding:3px 5px 0px 5px;">
            <div id="editableBox" class="editable" style="width:300px; height:60px">
            </div>
            <div>
                <table class="axial">
                    <tr>
                        <th align="left">VM Versions:</th>

                        <td>${jdks}</td>

                    </tr>
                    <tr>
                        <th align="left">OS:</th>
                        <td>${oss}</td>
                    </tr>
                    <tr>
                        <th align="left">Versions:</th>
                        <td>${versions}</td>
                    </tr>
                    <tr>
                        <th align="left">Duplicates:</th>
                        <td>
                            <exc:duplicates report='${report}'/>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>

    <exc:duplicatesTag report="${report}"/>

    <c:if test="${fn:length(report.commentCollection) > 0}">
        <h3>Additional Report Comments:</h3>
        <table width="100%">
            <tr>
                <th>User</th>
                <th>Date</th>
                <th>Comment</th>
            </tr>
            <c:forEach var="reportcomment" items='${report.commentCollection}' varStatus="stat">
                <tr class="${(stat.index % 2) == 0 ? "b" : "a"}">
                    <td>${reportcomment.nbuserId.name}</td>
                    <td>${reportcomment.commentDate}</td>
                    <td>
                        <pre>${reportcomment.comment}</pre>
                    </td>
                </tr>
            </c:forEach>
        </table>
    </c:if>

    <div>
        <h3>Add Comment:</h3>
        <p>
            The comment will be visible on this page and it will be also included
            into the page shown after new report upload. You can add an additional request
            to users that are facing this issue or you can provide a workaround
            how to avoid this issue.
            The comment will be shown in the same formatting that you use as a preformatted HTML section.
            You can also use HTML inline formatting as &lt;b&gt;, &lt;i&gt;, or &lt;u&gt;.
            Using links and images is not supported and will not work in the IDE.
        </p>
        <form action="detail.do" method="GET">
            <input type='hidden' name='id' value='${report.id}'/>
            <table width="300px">
                <tr>
                    <th width="40px">
                        Comment:
                    </th>
                    <td>
                        <textarea cols="80" rows="6" name="comment"></textarea><br/>
                    </td>
                </tr>
                <tr>
                    <th colspan="2" align="right">
                        <input type="submit" value="Send"/>
                    </th>
                </tr>
            </table>
        </form>
    </div>

    <!--ENDING OF CLASS H2-->
</div>
<p></p><!--ENDING OF CLASS APP-->
<%@include file="/WEB-INF/jspf/footer.jspf" %>
