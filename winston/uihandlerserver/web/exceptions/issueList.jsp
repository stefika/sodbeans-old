<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 

<c:set var="path" value='/ <a href="../index.jsp">Analytics</a> / Exceptions / List'/>
<%@include file="/WEB-INF/jspf/listHeader.jspf" %>
<%--
The taglib directive below imports the JSTL library. If you uncomment it,
you must also add the JSTL library to the project. The Add Library... action
on Libraries node in Projects view can be used to add the JSTL 1.1 library.
--%>

<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%> 
<%@taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@taglib prefix="exc" uri="/WEB-INF/tlds/Exc"%>

<link rel="stylesheet" type="text/css" href="/resources/stylesheets/tablesorter.css" media="screen">

<script type="text/javascript" src="/resources/javascripts/jquery.tablesorter.js"></script>

<script type="text/javascript">
    $(document).ready(function()
    {
        $("#sortable").tablesorter({
            widgets: ['zebra'],
            textExtraction: function(node) {
            	// extract data from markup and return it
            	return $(node).text();
            }
        });
    }
);
</script>

<div class="h2">
<h2>Exceptions</h2>


<table id="sortable" class="tablesorter">
    <thead>
    <tr>
        <th>
            Report
        </th>
        <th>
            Exc.
        </th>
        <th>
            Build
        </th>
        <th>
            Summary
        </th>
        <th>
            Component
        </th>
        <th>
            Subcomponent
        </th>
        <th>
            Duplicates
        </th>
        <th>
            Bugzilla
        </th>        
    </tr>
    </thead>
    <tbody>
    <c:forEach var="issue" items='${exceptions}' varStatus="stat">
        <tr>
            <td>
                <a href='detail.do?id=${issue.reportId.id}'>
                    ${issue.reportId.id}
                </a>
            </td>
            <td>
                <a href='exception.do?id=${issue.id}'>
                    ${issue.id}                    
                </a>
            </td>
            <td>
               ${exc:buildNumberFormat(issue.build)}
            </td>
            <td>
                <span class="summary">${fn:substring(issue.summary,0,60)}</span>
            </td>
            <td>
                <span class="component">${issue.reportId.component}</span>
            </td>
            <td>
                <span class="subcomponent">${issue.reportId.subcomponent}</span>
            </td>
            <td>
                <exc:duplicates submit='${issue}'/>
            </td>
            <td>
                <exc:issuezilla exceptions='${issue}' lazy='true'/>
            </td>
            
        </tr>  
    </c:forEach>
    </tbody>
</table>
count: ${fn:length(exceptions)}<br/>
</div>
<%@include file="/WEB-INF/jspf/footer.jspf" %>