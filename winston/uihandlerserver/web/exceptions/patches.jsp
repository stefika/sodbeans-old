<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 
<%@include file="/WEB-INF/jspf/header.jspf" %>

<h2>Patches</h2>
<table>
    <thead>
        <th>
            NetBeans version
        </th>
        <th>
            Patch name
        </th>
        <th>
            Module name
        </th>
        <th>
            Specification version
        </th>
    </thead>
    <tbody>
        <c:forEach items="${patches}" var="patch">
            <tr>
                <td>
                    ${patch.nbversionId.version}
                </td>
                <td>
                    ${patch.patchname}
                </td>
                <td>
                    ${patch.modulename}
                </td>
                <td>
                    ${patch.specversion}
                </td>
                <td>
                    <a href="patches.do?drop=${patch.id}">drop</a>
                </td>
            </tr>
        </c:forEach>    
    </tbody>
</table>
<hr/>
<h3>Create new patch</h3>
<form>
    <table>
        <tr>
            <td>Patch name: </td><td><input type=text name='patch_name' value="${last_patch_name}"/> </td>
        </tr>
        <tr>
            <td>Patched module name:</td><td> <input type=text name='module_name' value="${last_module_name}"/> </td>
        </tr>
        <tr>
            <td>Patched module specification version:</td><td> <input type=text name='spec_version' value="${last_spec_version}"/> </td>
        </tr>
        <tr>
            <td>Patched NetBeans version:</td>
            <td>
                <select name="version">
                    <c:forEach items="${versions}" var="vers">
                        <c:choose>
                            <c:when test="${vers.version == last_version}">
                                <option selected="selected">
                            </c:when>
                            <c:otherwise>
                                <option>
                            </c:otherwise>
                        </c:choose>
                            ${vers.version}
                        </option> 
                    </c:forEach>
                </select>
            </td>
        </tr>
    </table>
    <input type=submit name=create/>
</form>


<%@include file="/WEB-INF/jspf/footer.jspf" %>