<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 

<c:set var="path" value='/ <a href="../index.jsp">Analytics</a> / Admin / Component Mapping'/>
<%@include file="/WEB-INF/jspf/header.jspf" %>
<%--
The taglib directive below imports the JSTL library. If you uncomment it,
you must also add the JSTL library to the project. The Add Library... action
on Libraries node in Projects view can be used to add the JSTL 1.1 library.
--%>

<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%> 
<%@taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@taglib prefix="exc" uri="/WEB-INF/tlds/Exc"%>

<p class="tasknav">
              <a href="list.do?query">Query</a> | <a href="list.do?unmapped">Unmapped</a> | <a href="list.do?reset">All</a>
          </p>
<div class="h2">
<h2>Mapping Edit</h2>
<form>
<input type=hidden name=component value='${mapping.mappingPK.oldComponent}'/>
<input type=hidden name=subcomponent value='${mapping.mappingPK.oldSubcomponent}'/>
    <table>
        <tr>
            <th>
                &nbsp;
            </th>
            <th>
                Component
            </th>
            <th>
                Subcomponent
                </th>
            </tr>
        <tr>
            <th>
                Old values:
            </th>
            <td>
                ${mapping.mappingPK.oldComponent}
            </td>
            <td>
                ${mapping.mappingPK.oldSubcomponent}
            </td>
        </tr>
        <tr>
            <th>
                New values:
            <td>
                <input type=text name='newcomponent' value='${mapping.newComponent}'/>
            </td>
            <td>
                <input type=text name='newsubcomponent' value='${mapping.newSubcomponent}'/>
            </td>
        </tr>  
        </table>
        <input type=submit name=submit/><input type=reset name=reset/>
    </form>
</div>
<%@include file="/WEB-INF/jspf/footer.jspf" %>