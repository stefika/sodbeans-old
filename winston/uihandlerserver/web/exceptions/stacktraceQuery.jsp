<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<c:set var="path" value='/ <a href="../index.jsp">Analytics</a> / Exceptions / Stacktrace Query'/>
<%@include file="/WEB-INF/jspf/header.jspf" %>
<%@ taglib prefix="exc" uri="/WEB-INF/tlds/Exc"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<h2>Exception <a href='exception.do?id=${exceptions.id}'>${exceptions.id}</a></h2>
<div>
            <table class="axial">
                <tr>
                    <th>Exception #:</th>
                    <td>${exceptions.id}</td>
                    <td>&nbsp;</td>
                    <th>VM:</th>

                    <td>${exceptions.vmId.name}</td>
                    <td>&nbsp;</td>
                    <th>&nbsp;</th>
                    <td></td>
                </tr>
                <tr>
                    <th>Component:</th>

                    <td>
                        ${exceptions.reportId.component}
                    </td>
                    <td>&nbsp;</td>
                    <th>OS:</th>
                    <td>${exceptions.operatingsystemId.name}</td>
                    <td>&nbsp;</td>
                    <td></td>
                    <td></td>

                </tr>
                <tr>
                    <th>Subcomponent:</th>
                    <td>
                        ${exceptions.reportId.subcomponent}
                    </td>
                    <td>&nbsp;</td>
                    <th>Version:</th>
                    <td>${exceptions.logfileId.productVersionId.nbversionId.version}</td>

                    <td>&nbsp;</td>
                    <td rowspan="4"></td>
                    <td rowspan="4"></td>
                </tr>
                <tr>
                    <th>Summary:</th>
                    <td colspan="7">${exceptions.summary}</td>
                </tr>
                <tr>
                    <th>Message:</th>
                    <td colspan="7">${exceptions.stacktrace.message}</td>
                </tr>
                <tr>
                    <th>Bugzilla:</th>
                    <td><exc:issuezilla exceptions='${exceptions}'/></td>
                    <td>&nbsp;</td>

                    <th>Duplicates:</th>
                    <td colspan="7"><exc:duplicates submit='${exceptions}'/></td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>

                </tr>
            </table>
        </div>

    <div id="desc1" title="stacktrace">
        <h3>Stacktrace:</h3>
        <form action="stacktrace.do" method=GET  id="stacktracequery">
            <input type='hidden' name='id' value='${exceptions.id}'/>
            <exc:stacktrace stacktrace='${exceptions.stacktrace}' form='true' />
            <br/>
            <input type='submit' value='Find'/>
            <input type='reset' value='Reset'/>
        </form>
        <br/>
    </div>

    <!--ENDING OF CLASS H2-->
<p></p><!--ENDING OF CLASS APP-->
<%@include file="/WEB-INF/jspf/footer.jspf" %>