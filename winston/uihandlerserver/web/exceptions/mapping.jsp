<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 

<c:set var="path" value='/ <a href="../index.jsp">Analytics</a> / Admin / Component mapping'/>
<%@include file="/WEB-INF/jspf/header.jspf" %>
<%--
The taglib directive below imports the JSTL library. If you uncomment it,
you must also add the JSTL library to the project. The Add Library... action
on Libraries node in Projects view can be used to add the JSTL 1.1 library.
--%>

<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%> 
<%@taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@taglib prefix="exc" uri="/WEB-INF/tlds/Exc"%>

<p class="tasknav">
              <a href="list.do?query">Query</a> | <a href="list.do?unmapped">Unmapped</a> | <a href="list.do?reset">All</a>
          </p>
<div class="h2">
<h2>Mapping</h2>

    <table>
            <tr>
                <th>
                    Component
                </th>
                <th>
                    Subcomponent
                </th>
                <th>
                    New Component
                </th>
                <th>
                    New Subcomponent
                </th>
            </tr>
    <c:forEach var="mapping" items='${mappings}' varStatus="stat">
        <tr class="${(stat.index % 2) == 0 ? "b" : "a"}">
            <td>
                ${mapping.mappingPK.oldComponent}
            </td>
            <td>
                ${mapping.mappingPK.oldSubcomponent}
            </td>
            <td>
                ${mapping.newComponent}
            </td>
            <td>
                ${mapping.newSubcomponent}
            </td>
            <td>
                <a href='mapping.do?component=${mapping.mappingPK.oldComponent}&subcomponent=${mapping.mappingPK.oldSubcomponent}'>
                edit
                </a>
            </td>
        </tr>  
    </c:forEach>
    </table>
    <hr/>
    Create new Mapping for:
    <form>
        Component: <input type=text name='component'/> Subcomponent: <input type=text name='subcomponent'/>
        <input type=submit name=create/>
    </form>
</div>
<%@include file="/WEB-INF/jspf/footer.jspf" %>