function getComp() {
    var result="";
    var options = document.getElementById("queryform").component.options;
    for (i = 0; i < options.length; i++){
        if (options[i].selected){
            result= result + options[i].value + ",";
        }
    }
    return result;
}

function getSub()
{
    var req = null;
    document.getElementById("queryform").subcomponent.add(new Option("aa","aa"),null);
    if(window.XMLHttpRequest)
        req = new XMLHttpRequest();
    else if (window.ActiveXObject)
        req  = new ActiveXObject(Microsoft.XMLHTTP);

    req.onreadystatechange = function () {
        if(req.readyState == 4)
        {
            if(req.status == 200)
            {
                document.getElementById("queryform").subcomponent.length=0;
                list = req.responseText.split(",");
                for (i=0;i<list.length;i++) {
                    document.getElementById("queryform").subcomponent.add(new Option(list[i],list[i]),null);
                }
            }
        }
    };
    var url = "subcomponents?component="+getComp();
    req.open("GET", url, true);
    req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    req.send(null);
}

function validate(f) {
    if(f.duplicateof.value=="${exceptions.id}") {
        alert ("Can't create duplicate of itself.");
        return false;
    }
    return true;
}
