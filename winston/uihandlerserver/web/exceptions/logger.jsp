<!-- %@include file="/WEB-INF/jspf/header.jspf" % -->

<%@include file="/WEB-INF/jspf/styles.jspf" %>
<%@ taglib prefix="exc" uri="/WEB-INF/tlds/Exc"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%> 
        <div id="logger">
            <table>
            <c:forEach var="loggerLine" items='${logger}'>
                <tr>
                    <td><img src='icons/${loggerLine.iconName}'/></td>
                    <td>${loggerLine.displayName}</td>                    
                    <td>${loggerLine.name}</td>
                </tr>
            </c:forEach>
            </table>
        </div>
        <!--ENDING OF CLASS H2-->
    <p></p><!--ENDING OF CLASS APP-->
<!-- %@include file="/WEB-INF/jspf/footer.jspf" % -->