<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 
<%@ taglib prefix="exc" uri="/WEB-INF/tlds/Exc"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%> 
<%@ page import="java.util.List" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<div id="issuezilla" class="application">
    <div class="wrap">
        <span style="float:right;"><b>Changeset:</b> ${exc:formatChangeset(submit.logfileId.changeset)}</span>
        <table class="axial">
            <tr>
                <th align="left">Message:</th>
                <td>${fn:substring(submit.message,0,200)}</td>
            </tr>
            <tr>
                <th align="left">OS:</th>
                <td>${submit.operatingsystemId.name}, ${submit.operatingsystemId.version}, ${submit.operatingsystemId.arch}</td>
            </tr>
            <tr>
                <th align="left">VM:</th>
                <td>${submit.vmId.name}</td>
            </tr>
            <c:if test="${memory != null}">
                <tr>
                    <th align="left">Memory :</th>
                    <td colspan="5">${memory}MB</td>
                </tr>
            </c:if>
            <c:if test="${cpu_count != null}">
                <tr>
                    <th align="left">CPU count :</th>
                    <td colspan="5">${cpu_count}</td>
                </tr>
            </c:if>
            <c:if test="${!isException}">
                    <c:if test="${submit.latestAction != null}">
                        <tr>
                            <th align="left">Latest invoked action:</th>
                                <td colspan="5" >${submit.latestAction}</td>
                        </tr>
                    </c:if>
                    <c:if test="${submit.slownessType != null}">
                        <tr>
                            <th align="left">Slowness Type:</th>
                                <td colspan="5" >${submit.slownessType}</td>
                        </tr>
                    </c:if>
                    <tr>
                        <th align="left">Time spent in AWT:</th>
                        <td colspan="5" style="font-weight: bold">${submit.actionTime} ms</td>
                    </tr>
                    <tr>
                        <th align="left">Suspicious method:</th>
                        <td colspan="5" >${submit.suspiciousMethod.name}</td>
                    </tr>
                    <c:if test="${profiler_stats != null}">
                        <tr>
                            <th align="left">Profiler Statistics :</th>
                            <th>Samples count: </th>
                            <td colspan="3">${profiler_stats.samples}</td>
                        </tr>
                        <tr>
                            <td></td>
                            <th>Average: </th>
                            <td colspan="3">${profiler_stats.avg}</td>
                        </tr>
                        <tr>
                            <td></td>
                            <th>Minimum: </th>
                            <td colspan="3">${profiler_stats.minimum}</td>
                        </tr>
                        <tr>
                            <td></td>
                            <th>Maximum: </th>
                            <td colspan="3">${profiler_stats.maximum}</td>
                        </tr>
                        <tr>
                            <td></td>
                            <th>Std. deviation: </th>
                            <td colspan="3">${profiler_stats.stddev}</td>
                        </tr>
                    </c:if>
            </c:if>
        </table>
    </div>
    <h3>
        <c:if test="${submit.hasUILog}">
            <a href="loggerasxml?id=${submit.id}"><i>UI Actions XML</i></a>
        </c:if>
        <c:if test="${submit.hasMessagesLog}">
            / <a href="messageslog?id=${submit.id}">Messages Log</a>
        </c:if>
        <c:if test="${submit.hasSlownessLog}">
            / <a href="npslog?id=${submit.id}">Netbeans Profiler Snapshot</a>
        </c:if>
        <c:if test="${submit.hasHeapDumpLog}">
            / <a href="heaplog?id=${submit.id}">Heap Dump(gzipped)</a>
        </c:if>
    </h3>

    <c:choose>
        <c:when test="${isException}">
            <div id="desc1" title="stacktrace">
                <h3>Stacktrace:</h3>
                <pre style="max-height: ${window_height}px; overflow: scroll; max-width: ${window_width}px; white-space: -moz-pre-wrap; white-space: -pre-wrap; white-space: -o-pre-wrap; white-space: pre-wrap; word-wrap: break-word;">
<exc:stacktrace stacktrace='${submit.stacktrace}'/>
                </pre>
                <a href='stacktrace.do?id=${submit.id}'><i>Find Similar Stacktrace</i></a>
            </div>
        </c:when>
    </c:choose>

</div>
