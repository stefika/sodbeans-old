<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 

<c:set var="path" value='/ <a href="../index.jsp">Analytics</a> / Exceptions / Statistics'/>
<%@include file="/WEB-INF/jspf/header.jspf" %>
<%--
The taglib directive below imports the JSTL library. If you uncomment it,
you must also add the JSTL library to the project. The Add Library... action
on Libraries node in Projects view can be used to add the JSTL 1.1 library.
--%>

<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%> 
<%@taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@taglib prefix="exc" uri="/WEB-INF/tlds/Exc"%>

<div class="h2">
    <h2>Reporting Statistics for User <b><%=request.getParameter("username")%></b></h2>
    <table class="axial">
        <tr>
            <th>
                All&nbsp;Exception&nbsp;Reports:
            </th>
            <td>
                ${cell.allReports}
            </td>
        </tr>
        <tr>
            <th>
                Non&nbsp;Duplicate&nbsp;Reports:
            </th>
            <td>
                ${cell.nonDuplicateReports}
            </td>
        </tr>
        <tr>
            <th>
                Reports&nbsp;in&nbsp;Bugzilla:
            </th>
            <td>
                <a href='${cell.issuesURL}'>${cell.issuesCount}</a>
            </td>
        </tr>
        <tr>
            <th>
                Fixed&nbsp;Bugs:
            </th>
            <td>
                ${cell.fixed}
            </td>
            
        </tr>
        <tr>
            <th>
                Duplicates:
            </th>
            <td>
                ${cell.duplicates}
            </td>
        </tr>
    </table>

    <h3>Fixed Bugs</h3>
    Bugs reported by you that have been already fixed.
    <table width = '90%'>
        <tr>
            <th>Id</th>
            <th>Summary</th>
            <th>Target Milestone</th>
        </tr>
        <c:forEach var="issue" items='${cell.fixedIssues}' varStatus="stat">
        <tr class="${(stat.index % 2) == 0 ? "b" : "a"}">
            <td><a href='http://www.netbeans.org/issues/show_bug.cgi?id=${issue.issueId}'>${issue.issueId}</a></td>
            <td>${fn:substring(issue.shortDesc,0,75)}</td>
            <td>${issue.targetMilestone}</td>
        </tr>
        </c:forEach>
    </table>
    
    
    <h3>Open Bugs</h3>
    We are sill working on fixes of these bugs.
    <table width = '90%'>
        <tr>
            <th>Id</th>
            <th>Summary</th>
            <th>Target Milestone</th>
        </tr>
        <c:forEach var="issue" items='${cell.openIssues}' varStatus="stat">
        <tr class="${(stat.index % 2) == 0 ? "b" : "a"}">
            <td><a href='http://www.netbeans.org/issues/show_bug.cgi?id=${issue.issueId}'>${issue.issueId}</a></td>
            <td>${fn:substring(issue.shortDesc,0,75)}</td>
            <td>${issue.targetMilestone}</td>
        </tr>
        </c:forEach>
    </table>
</div>
<%@include file="/WEB-INF/jspf/footer.jspf" %>