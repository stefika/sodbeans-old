<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 
<%@include file="/WEB-INF/jspf/header.jspf" %>

<h2>Innocent classes for matching components and subcomponents</h2>
<table>
    <thead>
        <th>
            Innocent class:
        </th>
    </thead>
    <tbody>
        <c:forEach items="${innocents}" var="innocent">
            <tr>
                <td>
                    ${innocent.classname}
                </td>
                <td>
                    <a href="innocents.do?drop=${innocent.classname}">drop</a>
                </td>
            </tr>
        </c:forEach>    
    </tbody>
</table>
<hr/>
<h3>Create new innocent class</h3>
<form>
    <table>
        <tr>
            <td>Class name: </td>
            <td><input type=text name='class_name' /></td>
        </tr>
    </table>
    <input type=submit name=create/>
</form>


<%@include file="/WEB-INF/jspf/footer.jspf" %>