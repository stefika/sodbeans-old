<%@ taglib prefix="exc" uri="/WEB-INF/tlds/Exc"%>
<%@page import="org.netbeans.modules.exceptions.utils.PersistenceUtils" %>
<%@page import="org.netbeans.modules.exceptions.entity.Report" %>
<%
Integer id = Integer.parseInt(request.getParameter("id"));
Report report = PersistenceUtils.getInstance().getEntity(Report.class, id);
request.setAttribute("report", report);
%>

<span class="editable">
    <a href="javascript:askComp()">[edit]</a>
</span>
<table class="axial">
   <tr>
        <th align="left">Component:</th>
        <td><span id="component">${report.component}</span></td>
    </tr>
    <tr>
        <th align="left">Subcomponent:</th>
        <td>
            <span id="subcomponent">${report.subcomponent}</span>
        </td>
    </tr>
    <tr>
        <th align="left">Bugzilla:</th>
        <td>
            <span id="issueId">
                <exc:issuezilla report='${report}'/>
            </span>
        </td>
    </tr>
</table>
