<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:set var="title" value="${submit.titleName} \#${submit.id}"/>
<%@include file="/WEB-INF/jspf/header.jspf" %>
<%@ taglib prefix="exc" uri="/WEB-INF/tlds/Exc"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%> 
<%@ page import="java.util.List" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>


<script language='javascript' type='text/javascript' src='exceptions/components.js'></script>
<script language='javascript' type='text/javascript' src='/resources/detaillog.js'></script>



<div id="issuezilla" class="application">
    <!--BEGINNING OF H2 CLASS-->

    <h2>${submit.titleName}  #${submit.id}</h2>

    <div>
        <div>
            <form action="exception.do" method=GET  id="queryform" onsubmit="return validate(this);">
                <input type='hidden' name='id' value='${submit.id}'/>
                <table class="axial">
                    <tr>
                        <th align="left" style="min-width:130px">Reporter:</th>
                        <td style="min-width:200px">${submit.nbuserId.name}</td>
                        <th align="left">VM:</th>

                        <td>${submit.vmId.name}</td>

                    </tr>
                    <tr>
                        <th align="left">Component:</th>
                        <td style="font-size: larger">
                            ${submit.reportId.component}
                        </td>
                        <th align="left">OS:</th>
                        <td>${submit.operatingsystemId.name}</td>

                    </tr>
                    <tr>
                        <th align="left">Subcomponent:</th>
                        <td style="font-size: larger">
                            ${submit.reportId.subcomponent}
                        </td>
                        <th>Version:</th>
                        <td>${submit.logfileId.productVersionId.productVersion}</td>


                    </tr>
                    <tr>
                        <th align="left">Summary:</th>
                        <td colspan="5">${submit.summary}</td>
                    </tr>
                    <tr>
                        <th align="left">Changeset:</th>
                        <td>${exc:formatChangeset(submit.logfileId.changeset)}</td>
                        <th align="left"></th>
                        <td></td>
                    </tr>
                    <c:choose>
                        <c:when test="${isException}">
                            <tr>
                                <th align="left">Message:</th>
                                <td colspan="5">${submit.stacktrace.message}</td>
                            </tr>
                            <tr>
                                <th align="left">Class:</th>
                                <td colspan="5">${submit.stacktrace.class1}</td>
                            </tr>
                        </c:when>
                        <c:otherwise>
                            <c:if test="${submit.latestAction != null}">
                                <tr>
                                    <th align="left">Latest invoked action:</th>
                                        <td colspan="5" >${submit.latestAction}</td>
                                </tr>
                            </c:if>
                            <c:if test="${submit.slownessType != null}">
                                <tr>
                                    <th align="left">Slowness Type:</th>
                                        <td colspan="5" >${submit.slownessType}</td>
                                </tr>
                            </c:if>
                            <tr>
                                <th align="left">Time spent:</th>
                                <td colspan="5" style="font-weight: bold">${submit.actionTime} ms</td>
                            </tr>
                            <tr>
                                <th align="left">Suspicious method:</th>
                                <td colspan="5">${submit.suspiciousMethod.name}</td>
                            </tr>
                            <c:if test="${profiler_stats != null}">
                                <tr>
                                    <th align="left">Profiler Statistics :</th>
                                    <th>Samples count: </th>
                                    <td colspan="3">${profiler_stats.samples}</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <th>Average: </th>
                                    <td colspan="3">${profiler_stats.avg}</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <th>Minimum: </th>
                                    <td colspan="3">${profiler_stats.minimum}</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <th>Maximum: </th>
                                    <td colspan="3">${profiler_stats.maximum}</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <th>Std. deviation: </th>
                                    <td colspan="3">${profiler_stats.stddev}</td>
                                </tr>
                            </c:if>
                        </c:otherwise>
                    </c:choose>
                    <c:if test="${memory != null}">
                        <tr>
                            <th align="left">Memory :</th>
                            <td colspan="5">${memory} MB</td>
                        </tr>
                    </c:if>
                    <c:if test="${cpu_count != null}">
                        <tr>
                            <th align="left">CPU count :</th>
                            <td colspan="5">${cpu_count}</td>
                        </tr>
                    </c:if>
                    <c:forEach items="${patches}" var="patch">
                        <tr>
                            <th></th>
                            <td></td>
                            <th></th>
                            <td>${patch}</td>
                        </tr>
                    </c:forEach>
                    <tr>
                        <th align="left">Duplicates:</th>
                        <td>
                            <exc:duplicates submit='${submit}' />
                        </td>
                        <th align="left">Report:</th>
                        <td><a href='detail.do?id=${submit.reportId.id}'>${submit.reportId.id}</a></td>
                    </tr>
                </table>
            </form>
        </div>
    </div>
    <hr />

    <h3>
        <c:if test="${submit.hasUILog}">
            <a href="loggerasxml?id=${submit.id}"><i>UI Actions XML</i></a>
        </c:if>
        <c:if test="${submit.hasMessagesLog}">
            / <a href="messageslog?id=${submit.id}">Messages Log</a>
        </c:if>
        <c:if test="${submit.hasSlownessLog}">
            / <a href="npslog?id=${submit.id}">Netbeans Profiler Snapshot</a>
        </c:if>
        <c:if test="${submit.hasHeapDumpLog}">
            / <a href="heaplog?id=${submit.id}">Heap Dump(gzipped)</a>
        </c:if>
    </h3>

    <c:choose>
        <c:when test="${isException}">
            <div id="desc1" title="stacktrace">
                <h3>Stacktrace:</h3>
                <pre style="width: 95%; white-space: -moz-pre-wrap; white-space: -pre-wrap; white-space: -o-pre-wrap; white-space: pre-wrap; word-wrap: break-word;">
<exc:stacktrace stacktrace='${submit.stacktrace}'/>
                </pre>
                <a href='stacktrace.do?id=${submit.id}'><i>Find Similar Stacktrace</i></a>
            </div>
        </c:when>
        <c:otherwise>
            <c:if test="${submit.hasSlownessLog && (submit.slownessType == null)}">
                <h3>AWT thread dump:</h3>
                <div id="desc1" title="stacktrace">
                    <pre style="width: 95%; white-space: -moz-pre-wrap; white-space: -pre-wrap; white-space: -o-pre-wrap; white-space: pre-wrap; word-wrap: break-word;">
<exc:NPSTagHandler slowness="${submit}"/>
                    </pre>
                </div>
            </c:if>
        </c:otherwise>
    </c:choose>


    <div id="desc2" title="comments" >
        <h3>Logger:</h3>
    </div>

    <div id="desc2" class="wrapper" title="logger" style="width: 97%">
        <div class="left">
            <c:forEach var="line" items='${detailList}'>
                <img src="/resources/ide-icons/${line.icon}" alt=""/>
                <span class="time_info">${line.time}</span>
                <a href="javascript: exceptionDetail(${line.id},${line.order});"> ${line.name}</a>
                <br/>
            </c:forEach>
        </div>
        <div id="detail" class="right"></div>
    </div>

    <div id="desc2" title="comments">
        <h3>Comments:</h3>
        <table width = '90%' class='axial'>
            <tr>
                <th>Reporter</th>
                <th>Comment</th>
            </tr>
            <c:forEach var="comment" items='${submit.commentCollection}'>
                <tr>
                    <th align="left">${comment.nbuserId.name}:</th>
                    <td align="left">${exc:formatComment(comment.comment)}</td>
                </tr>
            </c:forEach>
        </table>
    </div>

    <exc:duplicatesTag report="${submit.reportId}"/>

    <div id="desc2" title="comments">
        <h3>Add Comment:</h3>
        <form action="exception.do" method=GET  id="commentform">
            <input type='hidden' name='id' value='${submit.id}'/>
            <table  width = '90%'>
                <tr>
                    <th>
                        Comment:
                    </th>
                    <td>
                        <textarea cols="80" rows="6" name='comment'></textarea><br/>
                    </td>
                </tr>
                <tr>
                    <th colspan="2">
                        <input type="submit" value="Send"/>
                    </th>
                </tr>
            </table>
        </form>
    </div>
    <hr />
    <!--ENDING OF CLASS H2-->
</div>
<p></p><!--ENDING OF CLASS APP-->
<%@include file="/WEB-INF/jspf/footer.jspf" %>