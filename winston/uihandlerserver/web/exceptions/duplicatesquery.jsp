<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@include file="/WEB-INF/jspf/header.jspf" %>
<script language='javascript' type='text/javascript' src='exceptions/components.js'></script>
<script type="text/javascript" LANGUAGE="JavaScript" SRC="exceptions/CalendarPopup.js"></script>
<script type="text/javascript" LANGUAGE="JavaScript">
    var cal = new CalendarPopup();

    function disableSelect(){
        var checked = document.forms['queryform'].allsubcomponents.checked;
        if (checked){
            document.forms['queryform'].subcomponent.disabled=true;
        }else{
            document.forms['queryform'].subcomponent.disabled=false;
        }
    }

    function disableDates(){
        var checked = document.forms['queryform'].useDates.checked;
        if (checked){
            document.forms['queryform'].creationFrom.disabled=false;
            document.forms['queryform'].creationTo.disabled=false;
            document.forms['queryform'].duplicatesCreationFrom.disabled=false;
            document.forms['queryform'].duplicatesCreationTo.disabled=false;
        }else{
            document.forms['queryform'].creationFrom.disabled=true;
            document.forms['queryform'].creationTo.disabled=true;
            document.forms['queryform'].duplicatesCreationFrom.disabled=true;
            document.forms['queryform'].duplicatesCreationTo.disabled=true;
        }
    }

    function setIZDisabled(disabled){
        // TODO
        //document.forms['queryform'].iz_status.disabled = disabled;
    }
    function load(){
        getSub();
    }
</script>
<style type="text/css">
    th{
        vertical-align: top;
        font-weight:bold;
    }
</style>
<c:if test="${empty_result}">
    <h4>
        Previous query returned empty result, try to change query parametters.
    </h4>
</c:if>
<form action="dupsquery.do" id="queryform">
    <table>
        <tr>
            <th>
                search in:
            </th>
            <td>
                <input type="radio" name="search_type" value="EXCEPTIONS" checked="checked"/>Exceptions<br/>
                <input type="radio" name="search_type" value="SLOWNESS"/>Slowness
            </td>
        </tr>
        <tr>
            <th>
                minimal duplicates count:
            </th>
            <td>
                <input type="text" name="minimum" value="${minimum}"/>
            </td>
        </tr>
        <tr>

            <th>
                maximal duplicates count:
            </th>
            <td>
                <input type="text" name="maximum" value="${maximum}"/>
            </td>
        </tr>
        <tr>
            <th>
                components:
            </th>
            <td>
                <select name="component" onchange="getSub();" style="width: 180px;" size="7" multiple="multiple">
                    <c:forEach var="component" items='${components}' varStatus="stat">
                        <exc:addSelectedComponent name="${component}"/>
                    </c:forEach>
                </select>
            </td>
        </tr>
        <tr>
            <th>
                subcomponents:
            </th>
            <td>
                <select name="subcomponent" style="width: 180px;" size="7" multiple="multiple">
                </select>
                <c:choose>
                    <c:when test="${allsubcomponents != null}">
                        <input name="allsubcomponents" onchange="disableSelect()" type="checkbox" checked="checked"/> All subcomponents
                    </c:when>
                    <c:otherwise>
                        <input name="allsubcomponents" onchange="disableSelect()" type="checkbox"/> All subcomponents
                    </c:otherwise>
                </c:choose>

            </td>
        </tr>
        <tr>
            <th>
                report status:
            </th>
            <td>
                <c:choose>
                    <c:when test="${status == 'unreported'}">
                        <input type="radio" name="status" value="unreported" checked="checked" onclick="setIZDisabled(true)" >unreported<br/>
                        <input type="radio" name="status" value="in issuezilla" onclick="setIZDisabled(false)">in Bugzilla<br/>
                        <input type="radio" name="status" value="both" onclick="setIZDisabled(false)">both
                    </c:when>
                    <c:when test="${status == 'in issuezilla'}">
                        <input type="radio" name="status" value="unreported" onclick="setIZDisabled(true)" >unreported<br/>
                        <input type="radio" name="status" value="in issuezilla" checked="checked" onclick="setIZDisabled(false)">in Bugzilla<br/>
                        <input type="radio" name="status" value="both" onclick="setIZDisabled(false)">both
                    </c:when>
                    <c:otherwise>
                        <input type="radio" name="status" value="unreported" onclick="setIZDisabled(true)" >unreported<br/>
                        <input type="radio" name="status" value="in issuezilla" onclick="setIZDisabled(false)">in Bugzilla<br/>
                        <input type="radio" name="status" value="both" checked="checked" onclick="setIZDisabled(false)">both
                    </c:otherwise>
                </c:choose>
            </td>
        </tr>
        <tr>
            <th>
                Bugzilla bug status:
            </th>
            <td>
                <c:choose>
                    <c:when test="${iz_status == 'OPEN'}">
                <input type="radio" name="iz_status" value="OPEN" checked="checked">open<br/>
                <input type="radio" name="iz_status" value="CLOSED">closed<br/>
                <input type="radio" name="iz_status" value="ALL">all
                    </c:when>
                    <c:when test="${iz_status == 'CLOSED'}">
                <input type="radio" name="iz_status" value="OPEN">open<br/>
                <input type="radio" name="iz_status" checked="checked" value="CLOSED">closed<br/>
                <input type="radio" name="iz_status" value="ALL">all
                    </c:when>
                    <c:otherwise>
                <input type="radio" name="iz_status" value="OPEN">open<br/>
                <input type="radio" name="iz_status" value="CLOSED">closed<br/>
                <input type="radio" name="iz_status" value="ALL" checked="checked">all
                    </c:otherwise>
                </c:choose>
            </td>
        </tr>
        <tr>
            <td>
                <c:choose>
                    <c:when test="${useDates != null}">
                        <input name="useDates" onclick="disableDates()" type="checkbox" checked="checked"/> Use dates constraints
                    </c:when>
                    <c:otherwise>
                        <input name="useDates" onclick="disableDates()" type="checkbox"/> Use dates constraints
                    </c:otherwise>
                </c:choose>
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <th>
                report creation date:
            </th>
            <td>
                <table>
                    <tr>
                        <td>
                            From:
                        </td>
                        <td>
                            <input type="text" name="creationFrom" value="${creationFrom}" disabled="disabled"/>
                        </td>
                        <td>
                            <A HREF="#" onClick="cal.select(document.forms['queryform'].creationFrom,'anchor1','MM/dd/yyyy'); return false;" NAME="anchor1" ID="anchor1">use calendar</A>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            To:
                        </td>
                        <td>
                            <input type="text" name="creationTo" value="${creationTo}" disabled="disabled"/>
                        </td>
                        <td>
                            <A HREF="#" onClick="cal.select(document.forms['queryform'].creationTo,'anchor1','MM/dd/yyyy'); return false;" NAME="anchor1" ID="anchor1">use calendar</A>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <th>
                duplicates creation date:
            </th>
            <td>
                <table>
                    <tr>
                        <td>
                            From:
                        </td>
                        <td>
                            <input type="text" name="duplicatesCreationFrom" value="${duplicatesCreationFrom}" disabled="disabled">
                        </td>
                        <td>
                            <A HREF="#" onClick="cal.select(document.forms['queryform'].duplicatesCreationFrom,'anchor1','MM/dd/yyyy'); return false;" NAME="anchor1" ID="anchor1">use calendar</A>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            To:
                        </td>
                        <td>
                            <input type="text" name="duplicatesCreationTo" value="${duplicatesCreationTo}" disabled="disabled">
                        </td>
                        <td>
                            <A HREF="#" onClick="cal.select(document.forms['queryform'].duplicatesCreationTo,'anchor1','MM/dd/yyyy'); return false;" NAME="anchor1" ID="anchor1">use calendar</A>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>

            </td>
            <td>
                <c:choose>
                    <c:when test="${showDuplicates!= null}">
                        <input type="checkbox" name="showDuplicates" checked="checked">Also show duplicated reports<br/>
                    </c:when>
                    <c:otherwise>
                        <input type="checkbox" name="showDuplicates">Also show duplicated reports<br/>
                    </c:otherwise>
                </c:choose>
                <br/>
            </td>
        </tr>
        <tr>
            <td>
                <input type=submit name=submit value="submit query"/>
            </td>
        </tr>
    </table>
</form>
<%@include file="/WEB-INF/jspf/footer.jspf" %>
