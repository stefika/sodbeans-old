<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 

<c:set var="path" value='/ <a href="../index.jsp">Analytics</a> / Exceptions / Statistics'/>
<%@include file="/WEB-INF/jspf/header.jspf" %>
<%-- 
    Document   : statisticbybuild
    Created on : Oct 31, 2007, 6:19:00 PM
    Author     : honza
--%>

<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%> 
<%@taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@taglib prefix="exc" uri="/WEB-INF/tlds/Exc"%>

<h3>Reports by Build Date</h3>
    <table>
        <tr>
            <th>Build date</th>
            <th>Reports</th>
        </tr>
        <c:forEach var="build" items='${data}' varStatus="stat">
            <tr>
                <td>
                    ${build.key}
                </td>
                <td>
                    ${build.value}
                </td>
            </tr>
        </c:forEach>
    </table>

<%@include file="/WEB-INF/jspf/footer.jspf" %>