<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:set var="path" value='/ <a href="../index.jsp">Analytics</a> / Exceptions / Slowness Dashboard'/>
<%@include file="/WEB-INF/jspf/listHeader.jspf" %>

<h2>Slowness Actions Dashboard</h2>
<p>
    Maximum and average times are shown in seconds. Not all operating systems are
    in the table so global count might be different than sum of all operating systems columns.

<table style="text-align:left; width: 100%" class="sortable" id="sortabletable">
    <thead>
        <tr>
            <th>&nbsp;</th>
            <c:forEach items="${supportedOses}" var="os" >
                <th colspan="3">${os}</th>
            </c:forEach>
            <th colspan="3">Global</th>
        </tr>
        <tr>
            <th>Action name</th>
            <c:forEach begin="0" end="${osCount}" var="os" >
                <th style="width: 50px">Max</th>
                <th style="width: 50px">Avg</th>
                <th style="width: 60px">Count</th>
            </c:forEach>
        </tr>
    </thead>
    <tbody>
        <c:forEach items="${resultBeans}" var="result" varStatus="stat">
            <tr class="${(stat.index % 2) == 0 ? "b" : "a"}">
                <td>
                    <a href="findAction.do?action=${result.global.action}">${result.global.action}</a>
                </td>
                <c:forEach items="${supportedOses}" var="os">
                    <td style="text-align:right">${exc:beanForOs(result, os).max}</td>
                    <td style="text-align:right">${exc:beanForOs(result, os).avg}</td>
                    <td style="text-align:center">${exc:beanForOs(result, os).count}</td>
                </c:forEach>
                <td style="text-align:right">${result.global.max}</td>
                <td style="text-align:right">${result.global.avg}</td>
                <td style="text-align:center">${result.global.count}</td>
            </tr>
        </c:forEach>
    </tbody>
</table>
<%@include file="/WEB-INF/jspf/footer.jspf" %>

