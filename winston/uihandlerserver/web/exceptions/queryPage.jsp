<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<c:set var="path" value='/ <a href="../index.jsp">Analytics</a> / Exceptions / Query'/>
<%@include file="/WEB-INF/jspf/header.jspf"%>
<%@include file="/WEB-INF/jspf/script.jspf"%>

<script language="javascript">

    var selectedSubcomponents = new Array();
    <c:forEach var="subcomponent" items='${command.subcomponents}'>
        selectedSubcomponents.push("${subcomponent}");
    </c:forEach>

    function getSubcomponents() {
            var component = $("select[name='component'] :selected").val();
            var subcomponent = $("select[name='subcomponents']");
            $.get("${pageContext.request.contextPath}/subcomponents", {
                component: component
            }, function (data) {
                subcomponent.empty();
                list = data.split(",");
                for (i=0;i<list.length;i++) {
                    if (jQuery.inArray(list[i], selectedSubcomponents) >= 0) {
                        subcomponent.append("<option value='" + list[i] + "' selected>" + list[i] + "</option>");
                    } else {
                        subcomponent.append("<option value='" + list[i] + "'>" + list[i] + "</option>");
                    }
                }
                count();
            }, 'text');
    }

    function count() {
        $("#count").html("<i>--loading--</i>");
        $.get("${pageContext.request.contextPath}/list.do?count&" + $("#queryform").serialize(),
            function (data) {
                $("#count").html(data);
            });
    }

    $(document).ready(function() {

        getSubcomponents();

        $("select[name='component']").change(function(){
            getSubcomponents();
        });
        
        $("#queryform").change(function () {
            count();
        });
    });
    
</script>

<div id="issuezilla" class="application">
    <!--BEGINNING OF H2 CLASS-->

    <h2>Query</h2>
    <form method="get" action="list.do" enctype="application/x-www-form-urlencoded" id="queryform">
        <script src="exceptions/query.js" type="text/javascript"></script>
        <table>
            <tr>
                <th>Component</th>
                <th>Subcomponent</th>
                <th>Version</th>
                <td></td>
            </tr>
            <tr>
                <td>
                    <select name="component" size="7" multiple="multiple" style="width: 170px;">
                        <c:forEach items='${allComponents}' var='component'>
                            <option value="${component}"
                                <c:if test="${fn:contains(command.components, component)}">selected</c:if>
                                >
                                ${component}</option>
                        </c:forEach>
                    </select>
                </td>
                <td>
                    <select name="subcomponents" size="7" multiple="multiple" onchange="countIssues();"  style="width: 170px;">
                    </select>
                </td>

                <td>
                    <select name="versions" size="7" multiple="multiple" onchange='countIssues();'  style="width: 170px;">
                        <c:forEach var="version" items='${versions}'>
                            <option value="${version}"
                                <c:if test="${fn:contains(command.versions, version)}">selected</c:if>
                                >
                                ${version}</option>
                        </c:forEach>
                    </select>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td colspan="3"><input type='checkbox' name='showDuplicates' <c:if test="${command.showDuplicates}">checked</c:if> onchange="countIssues();"/> Show duplicates</td>
                <td></td>
            </tr>


            <tr>
                <th align="left">Prefix of a stacktrace line:</th>
                <td colspan="3">
                    <input type="text" name="stacktraceLine" value="${command.stacktraceLine}" style="width: 300px"/>
                </td>
            </tr>
            <tr>
                <th align="left">Min number of duplicates: </th>
                <td colspan="3">
                    <input type="text" name="minDuplicates" value="${command.minDuplicates}" style="width: 300px"/>
                </td>
            </tr>
            <tr>
                <th align="left">Username: </th>
                <td colspan="3">
                    <input type="text" name="username" value="${command.username}" style="width: 300px"/>
                </td>
            </tr>
            <tr>
                <td colspan='4' style="background-color:#CCCCCC;">
                    <span id="count"></span>
                </td>
            </tr>
            <tr>
                <td colspan="4" align="right">
                    <input type="submit" value="Search" />
                </td>
            </tr>
        </table>
    </form>
    <%--
        <table class="axial">
            <tr>
                <th>User:</th>
                <th>&nbsp;</th>
            </tr>
            <tr>
                <td><input type="text" name="username" size=32/>
                <td>&nbsp;</td>
            </tr>
        </table>
    --%>
</div>
<%@include file="/WEB-INF/jspf/footer.jspf" %>