<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<c:set var="path" value='/ <a href="../index.jsp">Analytics</a> / Exceptions / List'/>
<%@include file="/WEB-INF/jspf/listHeader.jspf" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@taglib prefix="exc" uri="/WEB-INF/tlds/Exc"%>
<script type="text/javascript">
    function selectAll(select){
        var checkboxes = document.getElementsByTagName("input");
        for(i = 0; i < checkboxes.length; i++){
            cb = checkboxes[i];
            if (cb.getAttribute("name") == "split_duplicates"){
                cb.checked = select;
            }
        }
    }
</script>
<div class="h2">
    <h2>Exceptions List</h2>

    <form action="splitduplicates.do"  method="GET">
        <table class="sortable" id="sortableOutput">
            <tr>
                <th class="unsortable">
                    &nbsp;
                </th>
                <th>
                    Duplicates
                </th>
                <th>
                    Id
                </th>
                <th>
                    Report
                </th>
                <th>
                    Bugzilla
                </th>
                <th>
                    Latest build
                </th>
                <th>
                    Version
                </th>
                <th>
                    OS
                </th>
                <th>
                    JDK
                </th>
                <th>
                    Memory
                </th>
                <th>
                    Suspicious Method
                </th>
                <th>
                    Summary
                </th>
            </tr>

            <c:forEach var="issue" items='${exceptions}' varStatus="stat">
                <tr class="${(stat.index % 2) == 0 ? "b" : "a"}">
                    <td>
                        <input type="checkbox" name="split_duplicates" value="${issue.id}"/>
                    </td>
                    <td>
                        <exc:duplicates submit='${issue}'/>
                    </td>
                    <td>
                        <span style='visibility:hidden;font-size: 1px;'>${issue.orderStr}</span>
                        <a href='exception.do?id=${issue.id}'>
                            ${issue.id}
                        </a>
                    </td>
                    <td>
                       <a href='detail.do?id=${issue.reportId.id}'>
                           ${issue.reportId.id}
                       </a>
                    </td>
                    <td>
                        <exc:issuezilla report='${issue.reportId}' lazy='true'/>
                    </td>
                    <td>
                       ${exc:buildNumberFormat(issue.latestBuild)}
                    </td>
                    <td>
                       ${issue.logfileId.productVersionId.nbversionId.version}
                    </td>
                    <td>
                       ${issue.operatingsystemId.name}
                    </td>
                    <td>
                       ${exc:getJavaVersion(issue.vmId.name)}
                    </td>
                    <td>
                        <exc:memoryTag logfile="${issue.logfileId}"/>
                    </td>
                    <c:if test="${issue.isException}">
                        <td></td>
                    </c:if>
                    <c:if test="${!issue.isException}">
                        <td>
                            <span class="suspicious_method">${fn:substring(issue.suspiciousMethod.name,0,60)}</span>
                        </td>
                    </c:if>
                    <td>
                        <span class="summary">${fn:substring(issue.summary,0,60)}</span>
                    </td>
                </tr>
            </c:forEach>
        </table>
        <input type="button" name="" value="Select all" onclick="selectAll(true);"/>
        <input type="button" name="" value="Clear all" onclick="selectAll(false);"/><br/>
        <hr/>
        Mark selected as a new report:<br/>
        <input type="submit" name="new_report" value="New Report" title="Mark selected as a new report"/><br/>
        Mark selected as duplicates of report id:<br/>
        <input type="text" name="duplicate_of" value="0"/><br/>
        <input type="submit" name="duplicate" value="Duplicate"/>
    </form>
    count: ${fn:length(exceptions)}<br/>

    <a href='http://www.netbeans.org/issues/buglist.cgi?issue_id=${exc:issuezillaLinkList(exceptions)}' >
    Show</a> all reported bugs directly in Bugzilla.

</div>
<%@include file="/WEB-INF/jspf/footer.jspf" %>