<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 

<c:set var="path" value='/ <a href="../index.jsp">Analytics</a> / Exceptions / List'/>
<%@include file="/WEB-INF/jspf/listHeader.jspf" %>
<%--
The taglib directive below imports the JSTL library. If you uncomment it,
you must also add the JSTL library to the project. The Add Library... action
on Libraries node in Projects view can be used to add the JSTL 1.1 library.
--%>

<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%> 
<%@taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@taglib prefix="exc" uri="/WEB-INF/tlds/Exc"%>

<link rel="stylesheet" type="text/css" href="/resources/stylesheets/tablesorter.css" media="screen">

<script type="text/javascript" src="/resources/javascripts/jquery.tablesorter.js"></script>

<script type="text/javascript">
    $(document).ready(function()
    {
        $("#sortable").tablesorter({
            widgets: ['zebra'],
            textExtraction: function(node) {
            	// extract data from markup and return it
            	return $(node).text();
            }
        });
    }
);
</script>

<div class="h2">
<h2>Reports</h2>


<table id="sortable" class="tablesorter">
    <thead>
    <tr>
        <th>
            Id
        </th>
        <th>
            Build
        </th>
        <th>
            Summary
        </th>
        <th>
            Component
        </th>
        <th>
            Subcomponent
        </th>
        <th>
            Duplicates
        </th>
        <th>
            Bugzilla
        </th>        
    </tr>
    </thead>
    <tbody>
    <c:forEach var="report" items='${reports}' varStatus="stat">
        <tr>
            <td>
                <a href='detail.do?id=${report.id}'>
                    ${report.id}
                </a>
            </td>
            <td>
               <span>${exc:buildNumberFormat(report.latestBuild)}</span>
            </td>
            <td>
                <span class="summary">${fn:substring(report.submitCollection[0].summary,0,60)}</span>
            </td>
            <td>
                <span class="component">${report.component}</span>
            </td>
            <td>
                <span class="subcomponent">${report.subcomponent}</span>
            </td>
            <td>
                <exc:duplicates report='${report}'/>
            </td>
            <td>
                <exc:issuezilla report='${report}' lazy='true'/>
            </td>
            
        </tr>  
    </c:forEach>
    </tbody>
</table>
count: ${fn:length(reports)}<br/>
</div>
<%@include file="/WEB-INF/jspf/footer.jspf" %>