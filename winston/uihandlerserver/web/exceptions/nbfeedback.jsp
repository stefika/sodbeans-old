<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
   <%@taglib  prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Exception reporter</title>
    </head>
    <body>
        <h1>List of all your submissions</h1>
        <table>
        <c:forEach items="${exceptions}" var="item">
            <tr>
                <td>
                    <a href="<%= org.netbeans.web.Utils.getContextURL(request) %>/../exception.do?id=${item.id}">${item.id}</a>
                </td>
                <td>
                    ${item.summary}
                </td>
            </tr>
        </c:forEach>
        </table>
    </body>
</html>
