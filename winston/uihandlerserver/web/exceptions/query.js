    function getUrlParams() {
    var elements = document.getElementById("queryform").elements;
    var url="";
        url = getOptions(url, document.getElementById("queryform").component);
        url = getOptions(url, document.getElementById("queryform").subcomponent);
        url = getOptions(url, document.getElementById("queryform").build);
        // url = getOptions(url, document.getElementById("queryform").os);
        // url = getOptions(url, document.getElementById("queryform").vm);
        if (document.getElementById("queryform").showDuplicates.checked) {
            url = url + "&showDuplicates=on";
        }
        url = url + "&minDuplicates=" + document.getElementById("queryform").minDuplicates.value;
        url = url + "&stacktraceLine=" + document.getElementById("queryform").stacktraceLine.value;
        url = url + "&username=" + document.getElementById("queryform").username.value;
        return url;
    }

    function countIssues() {
    var elements = document.getElementById("queryform").elements;
    var url=getUrlParams();
        setIssueNumber("&nbsp;");
        var req = null; 
            if(window.XMLHttpRequest)
                    req = new XMLHttpRequest(); 
            else if (window.ActiveXObject)
                    req  = new ActiveXObject(Microsoft.XMLHTTP); 
            req.onreadystatechange = function () {
            if(req.readyState == 4)
                    {
                            if(req.status == 200)
                            {
                            setIssueNumber(req.responseText);

                            }
                        }
              };
            var qurl = "list.do?count&"+url;
            req.open("GET", qurl, true); 
            req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded"); 
            req.send(null); 


    }
    
    function getOptions(url, element) {
    for (i=0; i < element.options.length; i++ ) {
        if (element.options[i].selected) {
                url = url + element.name+'=';            
                url = url + element.options[i].value+'&';
            }
        }
        return url;
    }
    
    function setIssueNumber(text) {
         document.getElementById("issue_number").innerHTML=text;
    }