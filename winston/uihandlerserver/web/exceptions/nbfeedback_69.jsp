<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
   <%@taglib  prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Exception reporter</title>
        <style type="text/css">
            th.fixed_header{
                width: 90px;
                text-align: left;
            }
            th.summary_header{
                text-align: left;
            }
        </style>
    </head>
    <body>
        <h1>List of all your submissions</h1>
        <c:if test="${!exceptionsIsEmpty}">
            <h3>Submitted exceptions</h3>
            <table>
                <tr>
                    <th class="fixed_header">Submit</th>
                    <th class="fixed_header">Issue</th>
                    <th class="summary_header">Summary</th>
                </tr>
                <c:forEach items="${exceptions}" var="item">
                    <tr>
                        <td>
                            <a href="<%= org.netbeans.web.Utils.getContextURL(request)%>/../exception.do?id=${item.id}">${item.id}</a>
                        </td>
                        <td>
                            <c:if test="${item.reportId.inIssuezilla}">
                                <a href="https://netbeans.org/bugzilla/show_bug.cgi?id=${item.reportId.issueId}" id="${item.reportId.issueId}">${item.reportId.issueId}</a>
                            </c:if>
                            <c:if test="${!item.reportId.inIssuezilla}">
                                not filed
                            </c:if>
                        </td>
                        <td>
                            ${item.summary}
                        </td>
                    </tr>
                </c:forEach>
            </table>
        </c:if>
        <c:if test="${!slownessIsEmpty}">
            <h3>Submitted slowness reports</h3>
            <table>
                <tr>
                    <th class="fixed_header">Submit</th>
                    <th class="fixed_header">Issue</th>
                    <th class="summary_header">Summary</th>
                </tr>
                <c:forEach items="${slowness}" var="item">
                    <tr>
                        <td>
                            <a href="<%= org.netbeans.web.Utils.getContextURL(request)%>/../exception.do?id=${item.id}">${item.id}</a>
                        </td>
                        <td>
                            <c:if test="${item.reportId.inIssuezilla}">
                                <a href="https://netbeans.org/bugzilla/show_bug.cgi?id=${item.reportId.issueId}" id="${item.reportId.issueId}">${item.reportId.issueId}</a>
                            </c:if>
                            <c:if test="${!item.reportId.inIssuezilla}">
                                not filed
                            </c:if>
                        </td>
                        <td>
                            ${item.summary}
                        </td>
                    </tr>
                </c:forEach>
            </table>
        </c:if>
    </body>
</html>
