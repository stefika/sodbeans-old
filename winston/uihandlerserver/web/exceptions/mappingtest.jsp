<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 
<c:set var="path" value='/ <a href="../index.jsp">Analytics</a> / Admin / Component Mapping'/>
<%@include file="/WEB-INF/jspf/header.jspf" %>

<div class="h2">
    <h2>Mapping Test</h2>
    <c:choose>
        <c:when test="${comp != null}">
            <p>
                Result: ${comp.component}/${comp.subComponent}
            </p>
        </c:when>
        <c:otherwise>
            <p>
                No result is registered
            </p>
        </c:otherwise>
    </c:choose>
    <p>
        <form>
            Class name with package: <input type=text name="class_name"/><br/>
            Method name: <input type=text name="method_name"/><br/>
            <input type=submit name=submit/>
        </form>
        <br/>
        <form>
            Find new mapping for issue: <input type=text name="issue_id"/><br/>
            <input type=submit name=submit/>
        </form>
    </p>
</div>
<%@include file="/WEB-INF/jspf/footer.jspf" %>  
