<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@ taglib uri="/WEB-INF/statistics.tld" prefix="ui" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 

<c:set var="path" value='/ Analytics'/>
<%@include file="/WEB-INF/jspf/header.jspf" %> 


<div class="f-page-cell bg-sky" >
    <h2>NetBeans Analytics Community</h2>
    <p>
        This is home of the NetBeans Analytics Community that collects UI gestures from 
        users when they work within any application based on the <a href="http://platform.netbeans.org">NetBeans 
        Platform</a>. UI gestures are stored and uploaded to a netbeans.org server for 
        archiving and analysis.
    </p>
    <p>
        To collect information from a NetBeans-based application, you need to install 
        a special module that does the collecting and logging. Read more about this from
        the <a href="http://bits.netbeans.org/dev/javadoc/org-netbeans-modules-uihandler/overview-summary.html">infrastructure description</a>
        of the <em>UI Gestures module</em>.
    </p>
</div>
<div class="b-bottom-dashed b-top f-page-cell">
	<h1 class="font-light normal">News</h1>
<table>
<tr><td>17&nbsp;May&nbsp;2011</td>
    <td>
        Improved slowness duplication algorithm.<a href="http://wiki.netbeans.org/SlownessDuplicationAlgorithm">
        More detailed description</a> of this improvement.
    </td>
</tr>
    <tr><td>17&nbsp;Dec&nbsp;2010</td>
    <td>
        New statistic to track the <a href="${pageContext.request.contextPath}/graph/osandjvm.jsp">
        usage of different Java virtual machines</a>
        on various operating systems.
    </td>
</tr>
<tr><td>30&nbsp;Aug&nbsp;2010</td>
    <td>
        New statistic for monitoring "Scan for external changes" feature.
    </td>
</tr>
<tr><td>30&nbsp;Jun&nbsp;2009</td>
    <td>
        New CPU count statistics was added to map this information on post 6.7 development builds.
    </td>
</tr>
<tr><td>4&nbsp;Nov&nbsp;2008</td>
    <td>
        Are you interested in statistics of different NetBeans versions?
        Just look at some of them and select your favorit NetBeans version from the list.
    </td>
</tr>
<tr><td>08&nbsp;Aug&nbsp;2008</td>
    <td>
        What is the size of projects? How many files are in them? New statistics
        describing this metrics can be found at 
        <a href="graph/projectsize.jsp">Project Size</a>.
    </td>
</tr>
<tr><td>20&nbsp;Jul&nbsp;2008</td>
    <td>
        What is the screen resolution of NetBeans users and how many monitors are
        they using? See <a href="graph/screensize.jsp">Screen Size</a> statistics
        to get the response.
    </td>
</tr>
<tr><td>23&nbsp;Jun&nbsp;2008</td>
    <td>
        NetBeans statistics graphs are migrated to google graph. See 
        <a href="graph/memory.jsp">Memory</a> or 
        <a href="graph/projectjavaeeservers.jsp">Java EE Servers Usage</a> and
        many others.
    </td>
</tr>
<tr><td>20&nbsp;Jun&nbsp;2008</td>
    <td>
        Watch the various technologies used in our IDE by use of the new
        <a href="graph/technologies.jsp">Technology</a> statistic.
    </td>
</tr>
<tr><td>24&nbsp;Jan&nbsp;2008</td>
    <td>
        New statistics describing the quality and stability of NetBeans was added. 
        See <a href="graph/timetofailture.jsp">Time To Failture</a> statistics.
    </td>
</tr>
<tr><td>24&nbsp;Jan&nbsp;2008</td>
    <td>
        A new statistic that shows usage of distinct NetBeans modules was created. 
        See <a href="graph/netbeansmodules.jsp">NetBeans modules usage</a>.
    </td>
</tr>
<tr><td>12&nbsp;Dec&nbsp;2007</td>
    <td>Analytics was merged with the Exceptions database. You can now <a href="list.do?query">browse</a> 
        Exception Reports by components, build dates, etc.
    </td>
</tr>
<tr><td>10&nbsp;Dec&nbsp;2007</td>
    <td>A new statistic that displays exception reports distribution was added. See <a href="graph/exceptionreports.jsp">Exception reports</a>.
    </td>
</tr>
<tr><td>19&nbsp;Nov&nbsp;2007</td>
    <td>A new statistic that counts <a href="graph/projectsessions.jsp">project types by session</a> was added.
    </td>
</tr>
<tr><td>15&nbsp;Oct&nbsp;2007</td>
    <td>Rewritten to store data in a persistent database.
    </td>
</tr>
<tr><td>16&nbsp;Jun&nbsp;2007</td>
    <td>First interactive statistic is here! Search through your <a href="graph/actions.jsp">usage 
        of actions</a> with the new <a href="graph/actions.jsp">Actions statistic</a>.
    </td>
</tr>
<tr><td>9&nbsp;Jun&nbsp;2007</td>
    <td>Guys from the Czech Technical university provided us a new statistic -
        the usage of <a href="graph/codecompletion.jsp">code completion</a>.
        Thank you very much Jakub and Jan!
    </td>
</tr>
<tr><td>29&nbsp;Mar&nbsp;2007</td>
    <td>We are back after a week outage and we bring you new statistic.
        Choose the Help graph in the in the right column to see how much you 
        use the integrated help.
    </td>
</tr>
<tr><td>13&nbsp;Mar&nbsp;2007</td>
    <td>If you perform an upload, we are able to generate a <b>Tip of the Day</b>
        for you based on the projects that you have been working with.
    </td>
</tr>
<tr><td>07&nbsp;Feb&nbsp;2007</td>
    <td>We have our second graph! See the <em>Graphs</em> section in the right column 
        of this page.
    </td>
</tr>
<tr><td>03&nbsp;Jan&nbsp;2007</td>
    <td>This site got a new look and feel - similar to those used on the
        <a href="http://www.netbeans.org">netbeans.org</a> 
        <a href="http://www.netbeans.org/community/index.html">Community Portal</a>.
    </td>
</tr>

</table>
<%--
	<br/>
	<div class="align-right">
		<img src="http://www.netbeans.org/images/v4/redcross.gif" alt="" border="0" height="7" width="10"> <a href="http://www.netbeans.org/servlets/ProjectNewsAdd"><b>add news</b></a>&nbsp;&nbsp;&nbsp;<a href="http://www.netbeans.org/rss-091.xml" title="RSS Newsfeed"><img src="http://www.netbeans.org/images/v4/btn_xml.gif" alt="RSS Newsfeed" align="bottom" border="0" height="10" width="27"></a>
		&nbsp;&nbsp;&nbsp;<img src="http://www.netbeans.org/images/v4/star_r.gif" alt="" border="0" height="7" width="10"> <a href="news/index.html"><b>archive</b></a>

	</div>
    --%>
</div>
    <%--

<div class="b-bottom-dashed f-page-cell">
	<img alt="" src="http://www.netbeans.org/images/v5/flame.png" class="float-left" style="margin-right:10px;"/>
	<h1 class="font-light normal">Hot Threads</h1>

<table>
<tr>
<td>28&nbsp;Dec</td>
<td>yossarian</td>
<td><a href="http://www.netbeans.org/servlets/ReadMsg?msgNo=82033&list=nbusers"><b>Having trouble understanding JSF ExternalContext</b></a></td>
</tr>
<tr>
<td>22&nbsp;Dec</td>
<td>Thomas&nbsp;Kellerer</td>
<td><a href="http://www.netbeans.org/servlets/ReadMsg?msgNo=81913&list=nbusers"><b>Java Database</b></a></td>
</tr>
<tr>
<td>25&nbsp;Dec</td>
<td>Masaki&nbsp;Katakai</td>
<td><a href="http://www.netbeans.org/servlets/ReadMsg?msgNo=81978&list=nbusers"><b>Localize Standard Actions/Menus etc.</b></a></td>
</tr>
</table>	<br/>
	<div class="align-right">
		<img src="http://www.netbeans.org/images/v4/redtick.gif" alt="" border="0" height="7" width="10">
		<noscript>
		<a href="&#109;&#97;&#105;&#108;&#116;&#111;&#58;&#110;&#98;&#117;&#115;&#101;&#114;&#115;&#45;&#115;&#117;&#98;&#115;&#99;&#114;&#105;&#98;&#101;&#x40;&#110;&#101;&#116;&#98;&#101;&#97;&#110;&#115;&#46;&#111;&#114;&#103;"><b>subscribe</b></a>
		</noscript>	&nbsp;&nbsp;&nbsp;
		<img src="http://www.netbeans.org/images/v4/star_r.gif" alt="" border="0" height="7" width="10"> <a href="http://www.netbeans.org/servlets/SummarizeList?listName=nbusers">browse archive</a>
	</div>
</div>
--%>

<%--
<div class="b-bottom-dashed f-page-cell">
	<img alt="" src="http://www.netbeans.org/images/v5/date.png" class="float-left" style="margin-right:10px;"/>
	<h1 class="font-light normal">Upcoming events</h1>
	<div style="margin-left:60px;">

<table>
<div class="rarticle">
  <div class="rarticletitle"><span class='normal'>4th Dec, 2006</span> <a href="/community/news/calendar.html#118">Simplifying Data Access using Netbeans Visual Web Pack</a>
</div>
</div>
<div class="rarticle">
  <div class="rarticletitle"><span class='normal'>11th Dec, 2006</span> <a href="/community/news/calendar.html#117">Javapolis Belgium</a>
</div>
</div>
<div class="rarticle">
  <div class="rarticletitle"><span class='normal'>16th Jan, 2007</span> <a href="/community/news/calendar.html#107">NetBeans Day - Atlanta</a>
</div>
</div>

</table>

		<div class="articleall"><img src="http://www.netbeans.org/images/v4/star_r.gif" alt="" border="0" height="7" width="10"><a href="/community/news/calendar.html">Full Calendar</a></div>
	</div>
</div>
--%>

<!-- mailing lilsts start -->

<%--
<div class="b-bottom-dashed f-page-cell">
<img alt="" src="http://www.netbeans.org/images/v5/kmail.png" class="float-left" style="margin-right:10px;"/>
<h1 class="font-light normal">Mailing Lists</h1>
<div style="margin-left:60px;">
<table>
      <tr>
      <td class="tbltd1"><h4>
&#110;&#98;&#117;&#115;&#101;&#114;&#115;
&#x40;
&#110;&#101;&#116;&#98;&#101;&#97;&#110;&#115;&#46;&#111;&#114;&#103;
</h4>
<div class="ml15">The NetBeans users and support mailing list.
General discussion of NetBeans use.
This is the place to ask for help and to help others.
<BR><b>Recommended for :</b> <B>General support and discussion</B>,
for NetBeans users of all levels.
Developers and experienced users are encouraged to subscribe and help out newcomers.</div>
      <div class="ml15"><a href="http://www.netbeans.org/servlets/SummarizeList?listName=nbusers">Browse&nbsp;Archives</a></div>

            <div class="nic18">&nbsp;</div>
      </td>
      <td class="tbltd1" align="center">
<a href="&#109;&#97;&#105;&#108;&#116;&#111;&#58;&#110;&#98;&#117;&#115;&#101;&#114;&#115;&#45;&#115;&#117;&#98;&#115;&#99;&#114;&#105;&#98;&#101;&#x40;&#110;&#101;&#116;&#98;&#101;&#97;&#110;&#115;&#46;&#111;&#114;&#103;"><img src="http://www.netbeans.org/images/v4/btn_subscribe.gif" alt="Subscribe" title="Subscribe" width=63 height=13 border=0 vspace=3></a>

<a href="&#109;&#97;&#105;&#108;&#116;&#111;&#58;&#110;&#98;&#117;&#115;&#101;&#114;&#115;&#45;&#117;&#110;&#115;&#117;&#98;&#115;&#99;&#114;&#105;&#98;&#101;&#x40;&#110;&#101;&#116;&#98;&#101;&#97;&#110;&#115;&#46;&#111;&#114;&#103;"><img src="http://www.netbeans.org/images/v4/btn_unsubscribe.gif" alt="Unsubscribe" title="Unsubscribe" width=63 height=13 border=0 vspace=3></a>


      </td>

      </tr>

</table>
<br/>
<div class="align-right">
<img src="http://www.netbeans.org/images/v4/star_r.gif" alt="" border="0" height="7" width="10"><a href="lists/top.html">Full list of NetBeans mailing list</a> &nbsp;&nbsp;&nbsp;
<img src="http://www.netbeans.org/images/v4/star_r.gif" alt="" border="0" height="7" width="10"><a href="lists/top.html#localised">Lists in your language</a>
</div>
</div>
</div>

--%>
<!-- mailing lists ens -->
<div class="f-page-cell b-bottom">
	<h1 class="font-light normal">Useful Links</h1>
	<div style="margin-left:35px;">
	<table>
	<tr>
		<td>
			<ul>
			<li><a href="http://wiki.netbeans.org">NetBeans Wiki</a> to share ideas</li>
			<li><a href="http://www.planetnetbeans.org/">Planet NetBeans -- Blogs by community members</a></li>
			</ul>
		</td>
	</tr>
	</table>
	</div>
</div>
<%--
<div class="f-page-cell bg-sky">
	<h2 class="normal">Desktop Wallpapers, Buttons &amp; Banners</h2>
	<div class="align-center">
	<center>
			<table>
			<tr>
				<td><a href="http://www.netbeans.org/images/wallpapers/wallpaper1.png"><img src="http://www.netbeans.org/images/wallpapers/thumbs/small_wallpaper1.png" alt="NetBeans wallpaper" border="0" height="135" width="180"></a></td>
				<td><a href="http://www.netbeans.org/images/wallpapers/wallpaper4-6.png"><img src="http://www.netbeans.org/images/wallpapers/thumbs/small_wallpaper4-6.png" alt="NetBeans wallpaper" border="0" height="135" width="180"></a></td>
				<td>
					<ul>
					<li><a href="../community/teams/evangelism/collateral.html#banners">Banners</a></li>
					<li><a href="../community/teams/evangelism/collateral.html#logos">Logos</a></li>
					<li><a href="../community/teams/evangelism/collateral.html#buttons">Icons &amp; Buttons</a></li>
					<li><a href="../community/teams/evangelism/collateral.html#wallpapers">Desktop Wallpapers</a></li>
					</ul>
</td>
</tr>
</table>
	</center>
	</div>
</div>

--%>


<!-- End Content Area -->
<%@include file="/WEB-INF/jspf/footer.jspf" %> 

