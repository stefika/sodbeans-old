<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="ccc"%>

<%
            String userAgent = request.getHeader("user-agent");
            if ((userAgent != null) && userAgent.equalsIgnoreCase("NetBeans")) {
//                System.out.println("NetBeans AGENT");
%>
<!-- NetBeans -->

    <html>
        <head>
            <title>Upload done</title>
        </head>
        <body>
            <%@include file="uploaddone_content.jspf" %>
        </body>
    </html>

<%
            } else {
//                System.out.println("AGENT " + userAgent);
%>
<!-- browser -->

    <ccc:set var="path" value='/ <a href="index.jsp">Analytics</a> / Upload Logs'/>
    <%@include file="/WEB-INF/jspf/header.jspf"%>

    <%@include file="uploaddone_content.jspf" %>

    <%@include file="/WEB-INF/jspf/footer.jspf"%>

<%
            }
%>

