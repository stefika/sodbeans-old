<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@ page import="org.netbeans.server.uihandler.statistics.ProjectTypes" %>
<%@ page import="org.netbeans.server.uihandler.TipOfTheDay" %>
<%@ page import="org.netbeans.server.uihandler.LogsManager" %>
<%@ page import="org.netbeans.server.uihandler.ExceptionsData" %>
<%@ page import="java.util.Map" %>
<%@ taglib uri="/WEB-INF/statistics.tld" prefix="ui" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<ui:useStatistic name="Exceptions"/>

<%
            request.setAttribute("statSubmit", true);
            ExceptionsData lastExceptions = LogsManager.getDefault().getUploadDoneExceptions(pageContext);
            if (lastExceptions == null) {
                lastExceptions = (ExceptionsData) pageContext.getAttribute("lastExceptions");
            }
            if (lastExceptions != null) {
                lastExceptions.prepareUploadDoneContext(request);
            }
            String context = org.netbeans.web.Utils.getContextURL(request);
            request.setAttribute("page_context", context);
%>    
<c:choose>
    <c:when test="${statSubmit}">
        <%-- STATISTICS --%>
        <div class="f-page-cell bg-sky" >
        <h2>Upload of NetBeans Analytics Logs Complete</h2>

        <p>
            Thank you very much for sending data on your UI gestures to our server. This
            data forms an important contribution that helps us in our ongoing efforts to
            improve the NetBeans IDE and platform.
        </p>

        <p>
            Please see the graph section on the right to view various comparisons
            between tasks that you performed and those that were done by other users.
        </p>
        <ui:useStatistic name="ProjectTypes"/>

        <%
            Map last = (Map) pageContext.getAttribute("lastProjectTypes");
            TipOfTheDay.Tip tip = null;
            if (last != null) {
                tip = TipOfTheDay.getDefault().find(last.entrySet());
            }
            if (tip != null) {
                pageContext.setAttribute("tip", tip);
        %>

        <p>
            From an analysis of your previous log file, we are happy to offer you the
            following tip of the day:
        </p>


        <div class="b-bottom-dashed b-top f-page-cell">
            <img alt="" src="http://www.netbeans.org/images/v5/wp.png" class="float-left" style="margin-right:10px;"/>
            <h1 class="font-light normal">Tip of the Day</h1>

            <table>
                <thead>
                    <tr>
                        <th align="left">${tip.title}</th>
                    </tr>
                </thead>
                <tr>
                    <td>
                        ${tip.description}
                    </td>
                </tr>
                <tr align="left">
                    <td>
                        <jsp:element name="a">
                            <jsp:attribute name="href">${tip.url}</jsp:attribute>
                            <jsp:body>Read more...</jsp:body>
                        </jsp:element>
                    </td>
                </tr>

            </table>
        </div>

        <% }%>

        <div class="b-bottom-dashed b-top f-page-cell">
            <img alt="Thank you" src="http://www.netbeans.org/images/v5/kpackage.png" class="float-left" style="margin-right:10px;"/>
            <h1 class="font-light normal">Thank You!</h1>
            <p>
                Once again thank you for helping us with this application. We hope that
                the process of providing UI gesture data was at least slightly interesting.
                Please do not hesitate to submit data on additional sessions! We are
                continually working on ways to improve statistics.
            </p>
        </div>

        </div>
    </c:when>
    <c:otherwise>
        <%-- ERROR REPORT --%>
        <div class="f-page-cell bg-sky" >
            <h2>NetBeans Error Report Upload Complete</h2>
            <table border="0" cellpadding="2">
                <tbody>
                    <tr>
                        <td>
                            It has now been added to the database with
                                <a href="${page_context}/exception.do?id=${issueId}">id #${issueId}</a>.
                            <c:choose>
                                <c:when test="${reopen}">
                                    <%-- REOPEN --%>
                                        It has been classified as a duplicate of
                                        <a href="${page_context}/detail.do?id=${reportBeforeReopenId}">report #${reportBeforeReopenId}</a>.
                                        This bug was already resolved, but since you are using a newer build 
                                        <em>a new bug will be filed</em>
                                        for this problem. You will be able to see the new bug from
                                        <a href="${page_context}/detail.do?id=${reportId}">report #${reportId}</a> in a few minutes.
                                </c:when>
                                <c:when test="${isDuplicate}">
                                    <%-- DUPLICATE --%>
                                        It has been classified as a duplicate of
                                        <a href="${page_context}/detail.do?id=${reportId}">report #${reportId}</a>.
                                </c:when>
                                <c:otherwise>
                                    <%-- NO DUPLICATE --%>
                                        It has been classified as a new report.
                                </c:otherwise>
                            </c:choose>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <c:if test="${!reopen}">
                                <c:choose>
                                    <c:when test="${isDuplicate}">
                                        <c:choose>
                                            <c:when test="${inIssuezilla}">
                                                <c:choose>
                                                    <c:when test="${isClosed && isFixed}">
                                                        <h5>
                                                            This bug was already fixed.
                                                        </h5>
                                                        <p>
                                                            <c:choose>
                                                                <c:when test="${TM != null}">
                                                                    Please <b>update</b> your build of NetBeans to the latest build of ${TM} where
                                                                    <a href="http://www.netbeans.org/issues/show_bug.cgi?id=${issuezillaId}" id="${issuezillaId}">bug #${issuezillaId}</a> is fixed.
                                                                </c:when>
                                                                <c:otherwise>
                                                                    See <a href="http://www.netbeans.org/issues/show_bug.cgi?id=${issuezillaId}" id="${issuezillaId}">Bug #${issuezillaId}</a> for more details.
                                                                </c:otherwise>
                                                            </c:choose>
                                                            If you are already using a newer build and your report meets certain
                                                            <a href="http://wiki.netbeans.org/CriteriaForReportingExceptionsToIssuezilla">criteria</a>
                                                            a new bug will be filed automatically.
                                                        </p>
                                                    </c:when>
                                                    <c:when test="${isClosed && !isFixed}">
                                                        <h5>
                                                            This bug was already resolved.
                                                        </h5>
                                                        <p>
                                                            You can check
                                                            <a href="http://www.netbeans.org/issues/show_bug.cgi?id=${issuezillaId}" id="${issuezillaId}">bug #${issuezillaId}</a>
                                                            in Bugzilla for more information.
                                                        </p>
                                                    </c:when>
                                                    <c:otherwise>
                                                        <p>
                                                            We will endeavor to find the cause of the problem. You
                                                            can view the status at
                                                            <a href="http://www.netbeans.org/issues/show_bug.cgi?id=${issuezillaId}" id="${issuezillaId}">bug ${issuezillaId}</a>.
                                                        </p>
                                                    </c:otherwise>
                                                </c:choose>
                                            </c:when>
                                            <c:otherwise>
                                                This bug was not filed in Bugzilla yet. It will be processed and if it
                                                meets certain <a href="http://wiki.netbeans.org/CriteriaForReportingExceptionsToIssuezilla">criteria</a> it will be transferred to NetBeans'
                                                Bugzilla bug tracking system automatically.
                                            </c:otherwise>
                                        </c:choose>

                                        <c:if test='${additionalComments != null}'>
                                            <h3>Additional comments:</h3>
                                            <table>
                                            <c:forEach var="comment" items='${additionalComments}' >
                                                <tr>
                                                    <td>
                                                        <pre>${comment}</pre>
                                                    </td>
                                                </tr>
                                            </c:forEach>
                                            </table>
                                        </c:if>
                                    </c:when>
                                    <c:otherwise>
                                        This problem will soon be evaluated by the
                                        <a href="http://qa.netbeans.org">NetBeans QE</a> team.
                                        Reports that meet certain <a href="http://wiki.netbeans.org/CriteriaForReportingExceptionsToIssuezilla">criteria</a> are transferred to NetBeans'
                                        Bugzilla bug tracking system automatically.
                                        If you provided your NetBeans.org
                                        username, you will be informed of any updates and changes to the bug.
                                        <p>
                                            If you believe this bug is severe and you are registered on NetBeans.org,
                                            you can use the above link to check all the generated information in our web
                                            application, and you can enter this bug directly into Bugzilla.
                                        </p>
                                    </c:otherwise>
                                </c:choose>
                            </c:if>
                        </td>
                    </tr>
                    <c:if test="${username != null}">
                        <tr>
                            <td colspan="2">
                                <a href ="${page_context}/list.do?order=duplicates&username=${username}">Your previous reports</a>
                            </td>
                        </tr>
                    </c:if>
                </tbody>
            </table>
        </div>

    </c:otherwise>
</c:choose>
<p>
    Thanks again for your help.
    <br><br>
    The <a href="http://www.netbeans.org">NetBeans</a> development team!
</p>
