<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 
<%@tag description="duplicates tag" pageEncoding="UTF-8"%>
<%@ taglib prefix="exc" uri="/WEB-INF/tlds/Exc"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<%@attribute name="report" type="org.netbeans.modules.exceptions.entity.Report" %>

<script type="text/javascript">
    function showDuplicate (id, title) {
          var myWidth = 0, myHeight = 0;
          if( typeof( window.innerWidth ) == 'number' ) {
            //Non-IE
            myWidth = window.innerWidth;
            myHeight = window.innerHeight;
          } else if( document.documentElement && ( document.documentElement.clientWidth || document.documentElement.clientHeight ) ) {
            //IE 6+ in 'standards compliant mode'
            myWidth = document.documentElement.clientWidth;
            myHeight = document.documentElement.clientHeight;
          } else if( document.body && ( document.body.clientWidth || document.body.clientHeight ) ) {
            //IE 4 compatible
            myWidth = document.body.clientWidth;
            myHeight = document.body.clientHeight;
          }
        $.ajax({
            url: "exception.do?panel=1&id=" + id + "&window_width=" + myWidth + "&window_height=" + myHeight,
            cache: false,
            success: function(html){
                dialog.setContent(html);
                dialog.setTitle(title + " <a href=${pageContext.request.contextPath}/exception.do?id=" + id + ">#" + id + "</a>");
                dialog.center();
                dialog.show();
            }
        });

    }
</script>
                
<div id="desc2" title="comments">
    <h3>Duplicates:</h3>
    <table width="100%">
        <tr style="text-align:left">
            <th width="70">Id</th>
            <th width="60">Build</th>
            <th width="50">Version</th>
            <th width="80">Report Date</th>
            <th width="80">OS</th>
            <th width="50">JVM</th>
            <c:if test="${report.slownessReport}">
                <th width="80">Invocation Time </th>
            </c:if>
            <th>Message</th>
        </tr>
        <c:forEach var="exception" items='${report.submitCollection}' varStatus="stat">
            <tr class="${(stat.index % 2) == 0 ? "b" : "a"}">
                <td>
                    <a href='javascript:showDuplicate(${exception.id}, "${exception.titleName}")'>${exception.id}</a>
                    <a href='exception.do?id=${exception.id}'><img src='/resources/images/ext_link.gif' alt="external link"></a>
                </td>
                <td>${exc:buildNumberFormatLong(exception.logfileId.buildnumber)}</td>
                <td>${exception.logfileId.productVersionId.nbversionId.version}</td>
                <td>${exc:formatDate(exception.reportdate)}</td>
                <td>${exception.operatingsystemId.name}</td>
                <td>${exc:getJavaVersion(exception.vmId.name)}</td>
                <c:if test="${report.slownessReport}">
                    <td>${exception.actionTime} ms</td>
                </c:if>
                <td>${fn:substring(exception.message,0,55)}</td>
            </tr>
            <c:if test="${fn:length(exception.commentCollection) > 0}">
                <c:forEach var="comment" items='${exception.commentCollection}'>
                    <c:if test="${comment.comment != ''}">
                        <tr class="${(stat.index % 2) == 0 ? "b" : "a"}">
                            <td>
                                &nbsp;
                            </td>
                            <td colspan="7">
                                <i>
                                    <a href='javascript:showDuplicate(${exception.id})'>${comment.nbuserId.name}</a>:
                                    ${exc:formatComment(comment.comment)}
                                </i>
                            </td>
                        </tr>
                    </c:if>
                </c:forEach>
            </c:if>
        </c:forEach>
    </table>
    <a href="showduplicateslist.do?id=${report.id}">show extended duplicates list</a>
    <br/>
    <form action="filterduplicatesbymethod.do">
        filter duplicates by method prefix: <input type="text" name="method"/>
        <input type="hidden" name="report_id" value="${report.id}"/><br/>
        <input type="submit" value="Filter"/>
    </form>
</div>
