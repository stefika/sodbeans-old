<%@page import="org.netbeans.server.uihandler.statistics.CompilerErrorStatistics"%>
<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@ taglib uri="/WEB-INF/statistics.tld" prefix="ui" %>
<c:set var="path" value='/ <a href="../index.jsp">Analytics</a> / Graph / Sodbeans Compiler Errors'/>
<%@include file="/WEB-INF/jspf/header.jspf" %>
<ui:useStatistic name="CompilerErrorStatistics"/>

<h2>Sodbeans Compiler Error Analysis</h2>

<div class="f-page-cell bg-sky" >
    <h2>How is the compiler used?</h2>
    <p>
        The compiler is used all the time to debug and build code. It's important to know how the IDE's debug and run
        features are being use. The statistics listed here give us insight into the use of the debugger and
        the compiler error messages provided by the Quorum language.
    </p>
</div>

<table width="100%">
    <%
        java.util.Collection<CompilerErrorStatistics.DataBean.ViewBean> all = (java.util.Collection) pageContext.getAttribute("globalCompilerErrorStatisticsAvg");

        if (all != null && !all.isEmpty()) {
    %>
    <tr>
        <td align="center" width ="600">
            <ui:pie
                collection="globalCompilerErrorStatisticsUsages"
                category="key"
                value="value"
                title="How often are builds successful?"
                resolution="600x200"
                />
        </td>
    </tr>
    <tr>
        <td align="center" width="600">
            <ui:bar
                collection="globalCompilerErrorStatisticsAvg"
                category="name"
                value="value"
                serie="name"
                stacked="true"
                title="Seconds between compiles"
                resolution="600x400"
                />

        </td>
    </tr>
    <% }%>
</table>


<table cellpadding="5" width="100%" >
    <thead class="tblheader">
    <th width="40%">Group (seconds)</th>
    <th>Number</th>
</thead>
<% for (CompilerErrorStatistics.DataBean.ViewBean vb : all) {%>
<tr>
    <td class="tbltd1"><% out.println(vb.getName());%></td>
    <td class="tbltd1"><% out.println(vb.getValue());%></td>
</tr>
<% }%>
</table>



<table cellpadding="5" width="100%" >
    <%
        java.util.Collection<CompilerErrorStatistics.DataBean.ViewBean> err = (java.util.Collection) pageContext.getAttribute("globalCompilerErrorStatisticsErrors");

        if (err != null && !err.isEmpty()) {
    %>
    <thead class="tblheader">
    <th width="40%">Compiler Error Types</th>
    <th>Number</th>
</thead>
<% for (CompilerErrorStatistics.DataBean.ViewBean vb : err) {%>
<tr>
    <td class="tbltd1"><% out.println(vb.getName());%></td>
    <td class="tbltd1"><% out.println(vb.getValue());%></td>
</tr>
<% }%>
<% }%>
</table>

<%@include file="/WEB-INF/jspf/footer.jspf" %>