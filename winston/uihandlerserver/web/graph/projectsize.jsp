<%@page contentType="text/html" import="org.netbeans.server.uihandler.statistics.ProjectSize.ProjectSizeItem"%>
<%@page import="org.netbeans.server.uihandler.statistics.ProjectSize"%>
<%@page pageEncoding="UTF-8"%>
<%@ taglib uri="/WEB-INF/statistics.tld" prefix="ui" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<ui:useStatistic name="ProjectSize"/>

<c:set var="path" value='/ <a href="../index.jsp">Analytics</a> / Graph / Project Size'/>
<%@include file="/WEB-INF/jspf/header.jspf" %>
<div class="f-page-cell bg-sky" >
    <h2>Statistics of NetBeans users projects size</h2>
    <p>
        This statistics describes how big projects are used by NetBeans users.
        The size is given as a count of files in a source root of a project.
        The session size is the sum of all source roots open in one IDE session.
    </p>

    <hr>

    <h3>Restrict the Query!</h3>
    <form method="get" action="<%= org.netbeans.web.Utils.getContextURL(request)%>/projectsize.jsp" >
        <table width="100%">
            <tr>
                <td>Minimum:</td>
                <td>Maximum:</td>
                <td>Number of columns:</td>
                <td>NetBeans version:</td>
                <td></td>
            </tr>
            <tr>
                <td><input type="text" name="minimum" value='${minimum != null ? minimum : 0}'/></td>
                <td><input type="text" name="maximum" value="${maximum != null ? maximum : 500}"/></td>
                <td><input type="text" name="columns" value='${columns != null ? columns : 10}'/></td>
                <td><%@include file="../WEB-INF/jspf/nbVersionComboBox.jspf" %> </td>
                <td><input type="submit"/></td>
            </tr>
        </table>
    </form>

</div>
<table width="100%">
    <c:if test="${globalProjectSizeJAVA_FILES != null}" >
        <tr>
            <td>
                <h3>The graph describes how many java files are in source roots. </h3>
                The x-axis determines a count of java files and y-axis determines
                the number of users using source root of the given size.
            </td>
        </tr>
        <%
            pageContext.setAttribute("collection", pageContext.getAttribute("globalProjectSizeJAVA_FILES"));
        %>
        <%@include file="projectsizegraph.jspf" %>
    </c:if>
    <c:if test="${globalProjectSizeVIRTUAL_FILES != null}" >
        <tr>
            <td>
                <h3>The graph describes how many virtual files are in source roots. </h3>
                The x-axis determines a count of virtual files and y-axis determines
                the number of users using source root of the given size.
            </td>
        </tr>
        <%
            pageContext.setAttribute("collection", pageContext.getAttribute("globalProjectSizeVIRTUAL_FILES"));
        %>
        <%@include file="projectsizegraph.jspf" %>
    </c:if>
    <c:if test="${globalProjectSizeUSER_LOG_JAVA_FILES != null}" >
        <tr>
            <td>
                <h3>
                    The graph describes how many java files are in all source roots for one user session.
                </h3>
                The x-axis determines a count of java files and y-axis determines
                the number of users having such count of java files in all source roots in one IDE session.
            </td>
        </tr>
        <%
            pageContext.setAttribute("collection", pageContext.getAttribute("globalProjectSizeUSER_LOG_JAVA_FILES"));
        %>
        <%@include file="projectsizegraph.jspf" %>
    </c:if>
    <c:if test="${globalProjectSizeUSER_LOG_VIRTUAL_FILES != null}" >
        <tr>
            <td>
                <h3>
                    The graph describes how many virtual files are in all source roots for one user session.
                </h3>
                The x-axis determines a count of virtual files and y-axis determines
                the number of users having such count of virtual files in all source roots in one IDE session.
            </td>
        </tr>
        <%
            pageContext.setAttribute("collection", pageContext.getAttribute("globalProjectSizeUSER_LOG_VIRTUAL_FILES"));
        %>
        <%@include file="projectsizegraph.jspf" %>
    </c:if>
</table>

<%@include file="/WEB-INF/jspf/footer.jspf" %> 