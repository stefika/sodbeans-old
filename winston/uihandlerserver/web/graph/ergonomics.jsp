package graph;

<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@page import="java.util.*" %>
<%@taglib uri="/WEB-INF/statistics.tld" prefix="ui" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 
<c:set var="path" value='/ <a href="../index.jsp">Analytics</a> / Graph / Technologies'/>
<%@include file="/WEB-INF/jspf/header.jspf" %>
<ui:useStatistic name="Ergonomics"/>

    <div class="f-page-cell bg-sky" >
        <h2>How exactly people enable functionality on demand?</h2>
        
        <p>
            Are you currious to know how people use the on demand features
            of NetBeans IDE? Whether they always enable everything that
            see, or whether they can change their mind?
        </p>
    </div>
    
    <table width="100%">
        <% 
        Collection<?> last = (Collection<?>)pageContext.getAttribute("lastErgonomics");
        if (last != null) {
        %>
        <tr width="100%">
            <td width="100%">
                <div align="center">
                    <b>Your Last Session</b>
                    <ui:bar collection="lastErgonomics" 
                        category="reason" stacked="accepted" serie="cluster"
                        value="count"
                        lblformat="%1$s = %2$s%%" resolution="800x200"/>
                </div>
            </td>
        </tr>
        <% }
        Collection<?> user = (Collection<?>)pageContext.getAttribute("userErgonomics");
        if (user != null) {
        %>
        <tr width="100%">
            <td width="100%">
                <div align="center">
                    <b>All Your Sessions</b>
                    <ui:bar collection="userErgonomics"
                        category="reason" stacked="accepted" serie="cluster"
                        value="count"
                        lblformat="%1$s = %2$s%%" resolution="800x200"/>
                </div>
            </td>
        </tr>
        <% } 
        
        Collection<?> all = (Collection<?>)pageContext.getAttribute("globalErgonomics");
        if (all != null) {
        %>
        <tr width="100%">
            <td width="100%">
                <div align="center">
                    <b>Global Statistics</b>
                    <ui:bar collection="globalErgonomics"
                        category="reason" stacked="accepted" serie="cluster"
                        value="count"
                        lblformat="%1$s" resolution="800x200"/>
                </div>
            </td>
        </tr>
        <% } %>
    </table>
    
    <hr>
    
<!--
    <div class="f-page-cell bg-sky" >
    <h3><a name="query"></a>Restrict the Query!</h3>
    <p>
        Often you are interested in siblings of a particular project.
        Here is your chance:
        This interactive statistics page allows you to restrict your query to display 
        only results matching specific <a href="http://java.sun.com/javase/6/docs/api/java/util/regex/Pattern.html">
        regular expression</a>. That way you can use for example <code>.*Ejb.*</code>
        to see just enterprise projects and combinations in which they
        appear. The expression <code>.*</code> signifies everything.
        For more advanced usages, see the
        <a href="http://java.sun.com/javase/6/docs/api/java/util/regex/Pattern.html">
        JDK documentation</a>.
    </p>
    
    <p>
        
    </p>
    <form method="get" action="<% out.print(HttpUtils.getRequestURL(request)); %>" >
        <table width="100%">
           <tr>
            <td>Include pattern:</td>
            <td>Exclude pattern:</td>
            <td></td>
           </tr>
           <tr>
            <td><input type="text" name="includes" value='${includes!=null?includes:".*"}'/></td>
            <td><input type="text" name="excludes" value="${excludes!=null?excludes:""}"/></td>
            <td><input type="submit"/></td>
           </tr>
        </table>
    </form>
    </div>
-->
</center>
<%@include file="/WEB-INF/jspf/footer.jspf" %> 
