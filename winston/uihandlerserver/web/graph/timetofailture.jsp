<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@ page import="java.util.List" import="java.util.Iterator"%>
<%@ page import="org.netbeans.server.uihandler.statistics.TimeToFailture" %>
<%@ taglib uri="/WEB-INF/statistics.tld" prefix="ui" %>
<ui:useStatistic name="TimeToFailture"/>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 

<c:set var="path" value='/ <a href="../index.jsp">Analytics</a> / Graph / Time To Failture'/>
<%@include file="/WEB-INF/jspf/header.jspf" %>
<%! static int COLUMNS_NO = 1;%>
<div class="f-page-cell bg-sky" >
    <h2>Statistics on NetBeans Mean Time To Failure</h2>
    
    <p>
        This statistic measures the IDE's mean time to failure. Mean time to failure
        is simply the reciprocal of the failure rate. In other words, it is the 
        expected time leading to system failure. 
    </p>
    <p>
        You can see the graph indicating the Time To Failure values that are measured 
        by UI Gestures Collector. The time between exceptions occurring in the IDE is 
        measured. The graph matches the exponential distribution, which serves as a 
        theoretical model for system reliability. You can get more information about 
        the <a href="http://en.wikipedia.org/wiki/Failure_rate">failure rate</a> or the
        <a href="http://en.wikipedia.org/wiki/MTTF">mean time between failures</a> at
        Wikipedia. Mean time between failures is similar to mean time to failure in this 
        case.
    </p>
    <%@include file="restrictQuery.jspf" %>
</div>

<table width="100%">
    <c:if test="${globalTimeToFailture != null}" >
    <tr>
        <td align="center" width="480">
            <ui:line serie="serie"
                        collection="globalTimeToFailture"
                        category="trimmedTime"
                        value="count"
                        title="Time To Failure"
                        showlegend="false"
                        resolution="500x500"
                        />
            <p>
                This is the overall statistic of all submitted data. X axis is the NetBeans time to failure and Y is the
                number of logs submitted with such data.
            </p>
        </td>
        <td>
            <table>
                <thead class="tblheader">
                    <% for (int j = 0; j < COLUMNS_NO; j++) {%>
                    <th>
                        Time To Failure &nbsp;
                    </th>
                    <th>
                        Number of Events
                    </th>
                    <% }%>
                </thead>
                <tbody>
                    <%
            List<TimeToFailture.TTFData> data = (List<TimeToFailture.TTFData>) pageContext.getAttribute("globalTimeToFailture");
            data = TimeToFailture.groupData(data, 2);
            int column = data.size() / COLUMNS_NO;
            long previousTime = 0;
            for (int i = 0; i < column; i++) {%>
                    <tr>
                        <%
    for (int j = 0; j < COLUMNS_NO; j++) {
        TimeToFailture.TTFData dat = data.get(column * j + i);
                        %>
                        <td class="tbltd1"><%= previousTime %> mins - <%= dat.getTrimmedTime()%> mins</td>
                        <td class="tbltd1"><%= dat.getCount()%></td>
                        <%
        previousTime = dat.getTrimmedTime();
    }
                        %>
                    </tr>
                    
                    <%}%>
                </tbody>
            </table>
        </td>
    </tr>
    <tr style="font-size: larger">
        <td>
            <h3>Global Mean Time To Failture: 
                <span style="color:red">
                    <%= pageContext.getAttribute("globalTimeToFailtureMTTF")%>
                    minutes
                </span>
            </h3>        
        </td>
    </tr>
    </c:if>
    <c:if test="${userTimeToFailture != null}" >
    <tr>
        <td align="center" width="480">
            <ui:line serie="serie"
                        collection="userTimeToFailture"
                        category="trimmedTime"
                        value="count"
                        title="Time To Failure"
                        showlegend="false"
                        resolution="500x500"
                        />
            <p>
                This statistic reflects all of your submitted data.
            </p>
        </td>
        <td>
            <table>
                <thead class="tblheader">
                    <% for (int j = 0; j < COLUMNS_NO; j++) {%>
                    <th>
                        Time To Failture (mins)
                    </th>
                    <th>
                        Number of Events
                    </th>
                    <% }%>
                </thead>
                <tbody>
                    <% 
           List<TimeToFailture.TTFData> userData = (List<TimeToFailture.TTFData>) pageContext.getAttribute("userTimeToFailture");
                    int userColumn = userData.size() / COLUMNS_NO;
            for (int i = 0; i < userColumn; i++) {%>
                    <tr>
                        <%
                        for (int j = 0; j < COLUMNS_NO; j++) {
                            TimeToFailture.TTFData dat = userData.get(userColumn * j + i);
                        %>
                        <td class="tbltd1"><%= dat.getTrimmedTime()%></td>
                        <td class="tbltd1"><%= dat.getCount()%></td>
                        <%}%>
                    </tr>
                    
                    <%}%>
                </tbody>
            </table>
        </td>
    </tr>
    <tr style="font-size:larger">
        <td>
            <h3>Your Mean Time To Failure: 
                <span style="color:red">
                    <%= pageContext.getAttribute("userTimeToFailtureMTTF")%>
                    minutes
                </span>
            </h3>        
        </td>
    </tr>
    </c:if>
</table>

<%@include file="/WEB-INF/jspf/footer.jspf" %> 