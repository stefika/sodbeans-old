<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@ page import="org.netbeans.server.uihandler.statistics.DatabaseServers" %>
<%@ page import="java.util.Map" %>
<%@ page import="java.util.Set" %>
<%@ taglib uri="/WEB-INF/statistics.tld" prefix="ui" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 

<c:set var="path" value='/ <a href="../index.jsp">Analytics</a> / Graph / Database Servers'/>
<%@include file="/WEB-INF/jspf/header.jspf" %>
<ui:useStatistic name="DatabaseServers"/>

    <div class="f-page-cell bg-sky" >
        <h2>Database Server Usage</h2>
        
        <p>
            The database support in NetBeans allows users to connect
            to a database, and view and modify database structure and data.
            These graphs show the database servers that users connect to most often.
        </p>
        <%@include file="restrictQuery.jspf" %>
    </div>
    
    <table width="100%">
        <% 
        if (pageContext.getAttribute("globalDatabaseServers") != null) {
        %>
        <tr>
            <td align="center" width="480">
                <ui:pie
                    collection="globalDatabaseServers"
                    category="key"
                    value="value"
                    title="History of All Users"
                />
                <p>
                    This is the overall ratio of percentage of usage of individual
                    database servers from all submitted data.
                </p>
            </td>
        </tr>
        <% }
        %>
        <% 
        if (pageContext.getAttribute("userDatabaseServers") != null) {
        %>
        <tr>
            <td align="center" width="480">
                <ui:pie
                    collection="userDatabaseServers"
                    category="key"
                    value="value"
                    title="All Your Sessions"
                />
                <p>
                    Here is the history of your usage of various database servers
                    from all your submitted sessions.
                </p>
            </td>
        </tr>
        <% }
        %>
        <% 
        if (pageContext.getAttribute("lastDatabaseServers") != null) {
        %>
        <tr>
            <td align="center" width="480">
                <ui:pie
                    collection="lastDatabaseServers"
                    category="key"
                    value="value"
                    title="Your Last Session"
                />
                <p>
                    This is the percentage of database servers used by you during
                    your last session.
                </p>
            </td>
        </tr>
        <% }
        %>
    </table>

</center>
<%@include file="/WEB-INF/jspf/footer.jspf" %> 
