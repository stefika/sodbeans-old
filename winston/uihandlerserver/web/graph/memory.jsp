<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@ page import="java.util.Map" %>
<%@ page import="java.util.Set" %>
<%@ page import="org.netbeans.server.uihandler.statistics.Memory" %>
<%@ taglib uri="/WEB-INF/statistics.tld" prefix="ui" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 

<c:set var="path" value='/ <a href="../index.jsp">Analytics</a> / Graph / Available memory'/>
<%@include file="/WEB-INF/jspf/header.jspf" %>
<ui:useStatistic name="Memory"/>

    <div class="f-page-cell bg-sky" >
        <h2>How much memory do our users have?</h2>
        
        <p>
            The amount of physical memory is important for the runtime performance of NetBeans. 
            Below is a graph indicating the amount of memory various users of the IDE have.
        </p>

        <hr>

        <h3>Restrict the Query!</h3>
        <form method="get" action="<%= org.netbeans.web.Utils.getContextURL(request)%>/memory.jsp" >
            <table width="100%">
                <tr>
                    <td>Minimum:</td>
                    <td>Maximum:</td>
                    <td>Number of columns:</td>
                    <td>NetBeans version:</td>
                    <td></td>
                </tr>
                <tr>
                    <td><input type="text" name="minimum" value='${minimum!=null?minimum:"0GB"}'/></td>
                    <td><input type="text" name="maximum" value="${maximum!=null?maximum:"4GB"}"/></td>
                    <td><input type="text" name="columns" value='${columns!=null?columns:"9"}'/></td>
                    <td><%@include file="../WEB-INF/jspf/nbVersionComboBox.jspf" %> </td>
                    <td><input type="submit"/></td>
                </tr>
            </table>
        </form>
            
    </div>
    
        <table width="100%">
            <%
            java.util.Collection<Memory.MemBean> all= (java.util.Collection)
                    pageContext.getAttribute("globalMemory");

            if (all != null && !all.isEmpty()) {
            %>
            <tr>
                <td align="center" width="480">
                    <ui:bar
                        collection="globalMemory"
                        serie="memory"
                        category="memory"
                        value="count"
                        title="Histogram of available memory"
                        showlegend="false"
                        stacked="true"
                    />
                </td>
            </tr>
            <% } %>
        </table>
        
        <table cellpadding="5" width="100%" >
        <thead class="tblheader">
            <th width="40%">Memory</th>
            <th>Users</th>
        </thead>
        <% for (Memory.MemBean mb : all) { %>
            <tr>
                <td class="tbltd1"><% out.println(mb.getMemory()); %></td>
                <td class="tbltd1"><% out.println(mb.getCount()); %></td>
            </tr>
        <% } %>
        </table>
                
<%@include file="/WEB-INF/jspf/footer.jspf" %> 
