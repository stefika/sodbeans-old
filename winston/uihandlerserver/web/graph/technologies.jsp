<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@ page import="java.util.*" %>
<%@ taglib uri="/WEB-INF/statistics.tld" prefix="ui" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 
<c:set var="path" value='/ <a href="../index.jsp">Analytics</a> / Graph / Technologies'/>
<%@include file="/WEB-INF/jspf/header.jspf" %>
<ui:useStatistic name="Technology"/>

<div class="f-page-cell bg-sky" >
    <h2>What technologies are used by you and others?</h2>

    <p>
        This statistic allows you to see the types of actions that you and
        others use when working. The technology names are predefined,
        to cover the major technologies used known to the system and
        they may be more detaily re-classified to include also the
        projects that the technologies were used on. Please note, that
        this is interactive page and you can query it using the seach boxes
        on <a href="#query">bottom</a> of this page.
    </p>

    <hr/>
    
    <h3>Restrict the Query!</h3>
    <p>
        Often you are interested in only a particular technology or project.
        Here is your chance:
        This interactive statistics page allows you to restrict your query to display
        only results matching specific <a href="http://java.sun.com/javase/6/docs/api/java/util/regex/Pattern.html">
        regular expression</a>. That way you can use for example <code>.*Ejb.*</code>
        to see just enterprise technologies, or you can use
        <code>.*Web.*|.*J2EE.*</code> to compare the usage of the Web with
        that of the J2EE. The expression <code>.*</code> signifies everything. The
        "<code>|</code>" means "or". For more advanced usages, see the
        <a href="http://java.sun.com/javase/6/docs/api/java/util/regex/Pattern.html">
        JDK documentation</a>.
    </p>

    <form method="get" action="<% out.print(HttpUtils.getRequestURL(request));%>" >
        <table width="100%">
            <tr>
                <td>Include pattern:</td>
                <td>Exclude pattern:</td>
                <td>NetBeans version:</td>
                <td></td>
            </tr>
            <tr>
                <td><input type="text" name="includes" value='${includes!=null?includes:".*"}'/></td>
                <td><input type="text" name="excludes" value="${excludes!=null?excludes:".*Unidentified.*"}"/></td>
                <td><%@include file="../WEB-INF/jspf/nbVersionComboBox.jspf" %> </td>
                <td><input type="submit"/></td>
            </tr>
        </table>
    </form>

</div>

    <table width="100%">
        <% 
        Map<String,Integer> last = (Map<String,Integer>)pageContext.getAttribute("lastTechnology");
        if (last != null) {
        %>
        <tr width="100%">
            <td width="100%">
                <div align="center">
                    <b>Your Last Session</b>
                    <p>
                        This is the "top ten" of technologies you have used during your
                        last session.
                    </p>
                    <ui:pie collection="lastTechnology" category="key" value="value" lblformat="%1$s = %2$s%%" resolution="800x200"/>
                </div>
            </td>
        </tr>
        <% }
        Map<String,Integer> user = (Map<String,Integer>)pageContext.getAttribute("userTechnology");

        if (user != null) {
        %>
        <tr width="100%">
            <td width="100%">
                <div align="center">
                    <b>All Your Sessions</b>
                    <p>
                        According to our database, this is a list of the most commonly
                        used technologies in all your submitted sessions.
                    </p>
                    <ui:pie collection="userTechnology" category="key" value="value" lblformat="%1$s = %2$s%%" resolution="800x200"/>
                </div>
            </td>
        </tr>
        <% } 
        
        
        Map<String,Integer> all = (Map<String,Integer>)pageContext.getAttribute("globalTechnology");

        if (all != null) {
        %>
        <tr width="100%">
            <td width="100%">
                <div align="center">
                    <b>Global Statistics</b>
                    <p></p>
                    <ui:pie collection="globalTechnology" category="key" value="value" lblformat="%1$s = %2$s%%" resolution="800x200"/>
                </div>
            </td>
        </tr>
        <% } %>
    </table>
    
</center>
<%@include file="/WEB-INF/jspf/footer.jspf" %> 
