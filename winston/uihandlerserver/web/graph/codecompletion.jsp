<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@ page import="org.netbeans.server.uihandler.statistics.CodeCompletion" %>
<%@ page import="org.netbeans.server.uihandler.statistics.CodeCompletionData" %>
<%@ page import="java.util.Map" %>
<%@ page import="java.util.TreeMap" %>
<%@ page import="java.util.Set" %>
<%@ taglib uri="/WEB-INF/statistics.tld" prefix="ui" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 

<c:set var="path" value='/ <a href="../index.jsp">Analytics</a> / Graph / CodeCompletion'/>
<%@include file="/WEB-INF/jspf/header.jspf" %>
<ui:useStatistic name="CodeCompletion"/>

<script type="text/javascript">
  function switchView(s) {
    if (s == 'stat1ToGraph') {
      document.getElementById('stat1Tab').style.display = 'none';
      document.getElementById('stat1Graph').style.display = 'block';
    } else if (s == 'stat1ToTab') {
      document.getElementById('stat1Tab').style.display = 'block';
      document.getElementById('stat1Graph').style.display = 'none';
    } else if (s == 'stat2ToTab') {
      document.getElementById('stat2Tab').style.display = 'block';
      document.getElementById('stat2Graph').style.display = 'none';
    } else if (s == 'stat2ToGraph') {
      document.getElementById('stat2Tab').style.display = 'none';
      document.getElementById('stat2Graph').style.display = 'block';
    } else if (s == 'stat3ToTab') {
      document.getElementById('stat3Tab').style.display = 'block';
      document.getElementById('stat3Graph').style.display = 'none';
    } else if (s == 'stat3ToGraph') {
      document.getElementById('stat3Tab').style.display = 'none';
      document.getElementById('stat3Graph').style.display = 'block';
    } else if (s == 'stat4ToTab') {
      document.getElementById('stat4Tab').style.display = 'block';
      document.getElementById('stat4Graph').style.display = 'none';
    } else if (s == 'stat4ToGraph') {
      document.getElementById('stat4Tab').style.display = 'none';
      document.getElementById('stat4Graph').style.display = 'block';
    }
  }
</script>

    <div class="f-page-cell bg-sky" >
        <h2>How do people use the code completion feature?</h2>
        
        <p>
            Code completion is probably one of the most time-saving features provided by 
            IDEs. This statistic helps NetBeans developers improve the IDE's support for 
            code completion and in this 
            way make yours development in NetBeans even faster and more efficient.
        </p>
        <%@include file="restrictQuery.jspf" %>
    </div>
    
    <% 
        CodeCompletionData ccGlobal = (CodeCompletionData)pageContext.getAttribute("globalCodeCompletion");
        CodeCompletionData ccLast = (CodeCompletionData)pageContext.getAttribute("lastCodeCompletion");
        CodeCompletionData ccUser = (CodeCompletionData)pageContext.getAttribute("userCodeCompletion");
        
        Map<String,Integer> keyboardMouseUsage = new TreeMap<String,Integer>();
    %>
    <!-- Global code completion usage -->
    <div class="f-page-cell">
        <!-- keyboard/mouse completion usage -->
        <h2>1) Keyboard / mouse completion usage</h2>
          <div id="stat1Tab">
            <p>
                    This statistic shows how many code completion events took place using a mouse and keyboard.
            </p>
            <a href="javascript:switchView('stat1ToGraph');">View as graph</a><br /><br />
            <table id="answerList">
              <tr>
                <th>Total completions</th><th>Completed with keyboard (%)</th><th>Completed with mouse (%)</th>
              </tr>
              <tr class="answerLine1">
                <td class="alignLeft"><%=(ccGlobal.getMouseCompletion()+ccGlobal.getKeyboardCompletion())%></td><td><%=ccGlobal.getKeyboardCompletion()%> (<%=ccGlobal.getKeyboardCompletionP()%>)</td><td><%=ccGlobal.getMouseCompletion()%> (<%=ccGlobal.getMouseCompletionP()%>)</td>
              </tr>
            </table>
        </div>
        
        <div style="display:none;" id="stat1Graph">
            <a href="javascript:switchView('stat1ToTab');">View as table</a><br /><br />
            <ui:pie collection="keyboardMouseUsage" category="key" value="value"/> 
         </div>
         <!-- Completion/Cancelation -->
         <h2>2) Completion / cancellation</h2>
          <div id="stat2Tab">
            <p>
                This statistic shows the number of completed/cancelled invocations of code completion. 
                The "Total invocations" column shows both explicit (on-user demand) and implicit 
                (automatically provided by NetBeans) code completions. If you are wondering why
                the sum of the "Completed" and "Cancelled" columns does not equal the number listed in
                "Total Invocations," this is due to the fact that some of the code completion events 
                are currently not logged properly (e.g, "Default Cancellation" - NetBeans IDE cancels 
                code completion when you continue typing without using any suggestions. At the moment, 
                only user invoked cancellations (i.e., depressing the ESC key) are logged.
            </p>
            <a href="javascript:switchView('stat2ToGraph');">View as graph</a><br /><br />
            <table id="answerList">
              <tr>
                <th>Total Invocations</th><th>Completed (%)</th><th>Cancelled (%)</th>
              </tr>
              <tr class="answerLine1">
                <td class="alignLeft"><%=ccGlobal.getTotalRecords()%></td><td><%=ccGlobal.getKeyboardCompletion()+ccGlobal.getMouseCompletion()%> (<%=ccGlobal.getCompletedInvocationsP()%>)</td><td><%=ccGlobal.getCancelledInvocations()%> (<%=ccGlobal.getCancelledInvocationsP()%>)</td>
              </tr>
            </table>
          </div>
          <div style="display:none;" id="stat2Graph">
            <a href="javascript:switchView('stat2ToTab');">View as table</a><br /><br />
            <ui:pie collection="complCancel" category="key" value="value"/> 
        </div>
        <!-- Implicit/Explicit invocation -->
        <h2>3) Implicit / Explicit Invocation</h2>
          <div id="stat3Tab">
            <p>
                This statistic shows how many times users invoked code completion manually (Ctrl-Space),
                and how many times it was provided by the IDE automatically.
            </p>
            <a href="javascript:switchView('stat3ToGraph');">View as graph</a><br /><br />
            <table id="answerList">
              <tr>
                <th>Total Invocations</th><th>Implicit (%)</th><th>Explicit (%)</th>
              </tr>
              <tr class="answerLine1">
                <td class="alignLeft"><%=ccGlobal.getTotalRecords()%></td><td><%=ccGlobal.getImplicitInvocations()%> (<%=ccGlobal.getImplicitInvocationsP()%>)</td><td><%=ccGlobal.getExplicitInvocations()%> (<%=ccGlobal.getExplicitInvocationsP()%>)</td>
              </tr>
            </table>
          </div>
          <div style="display:none;" id="stat3Graph">
            <a href="javascript:switchView('stat3ToTab');">View as table</a><br /><br />
            <ui:pie collection="implicitExplicit" category="key" value="value"/> 
          </div>
         <!-- Selected index -->
         <h2>4) Selected index</h2>
          <div id="stat4Tab">
            <p>
                This statistic shows the number of times the default item from the list
                of code completion was selected by the user.<br />
                
                The statistic also shows the "Average selection index" of the selected code 
                completion item. Since the default selection has an index of zero, it is reasonable 
                to show the average index of non-default selections as well. This number shows 
                the average selection index of a selected item when user did not choose 
                the default one.
            </p>
            <a href="javascript:switchView('stat4ToGraph');">View as graph</a><br /><br />
            <table id="answerList">
              <tr>
                <th>Total Completions</th><th>Default Selections (%)</th><th>Average Selection Index (Non-Default)</th>
              </tr>
              <tr class="answerLine1">
                <td class="alignLeft"><%=ccGlobal.getKeyboardCompletion()+ccGlobal.getMouseCompletion()%></td><td><%=ccGlobal.getDefaultSelect()%> (<%=ccGlobal.getDefaultSelectP()%>)</td><td><%=ccGlobal.getAverageSelectionIndex()%> (<%=ccGlobal.getAverageNonDefaultSelectionIndex()%>)</td>
              </tr>
            </table>
          </div>
          <div style="display:none;" id="stat4Graph">
            <a href="javascript:switchView('stat4ToTab');">View as table</a><br /><br />
            <ui:pie collection="selectedIndex" category="key" value="value"/> 
          </div>

	<h2>5) Selected Index Histogram</h2>
          <div id="stat4Tab">
            <p>
                    This histogram shows the selected indexes. Default selections are omitted 
                    for purposes of clarity; so Index 0 represents the item following the default 
                    selection.
            </p>
	    <ui:pie collection="selectedIndexes" category="key" value="value"/> 
	  </div>

     </div>
<%@include file="/WEB-INF/jspf/footer.jspf" %> 
