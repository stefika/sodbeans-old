<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@ page import="org.netbeans.server.uihandler.statistics.Help" %>
<%@ page import="org.netbeans.server.uihandler.statistics.Help.Invocations" %>
<%@ page import="java.util.Map" %>
<%@ page import="java.util.Set" %>
<%@ taglib uri="/WEB-INF/statistics.tld" prefix="ui"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 

<c:set var="path" value='/ <a href="../index.jsp">Analytics</a> / Graph / JavaHelp'/>
<%@include file="/WEB-INF/jspf/header.jspf" %>
<ui:useStatistic name="Help"/>

    <div class="f-page-cell bg-sky" >
        <h2>Do people use contextual help?</h2>
        
        <p>
            Sometimes our users need a little bit of help. Do they search
            for it on Google or do they invoke our integrated help documentation?
            This project cannot answer the first question (yet), but it can inform  
            us of the topics people most frequently invoke help on.
        </p>
        <%@include file="restrictQuery.jspf" %>
    </div>

    <% 
        Invocations invLast = (Invocations)pageContext.getAttribute("lastHelp");
        Set<Invocations.One> last = invLast == null ? null : invLast.getTopTen();
        Invocations invUser = (Invocations)pageContext.getAttribute("userHelp");
        Set<Invocations.One> user = invUser == null ? null : invUser.getTopTen();
        Invocations invAll = (Invocations)pageContext.getAttribute("globalHelp");
        Set<Invocations.One> all = invAll == null ? null : invAll.getTopTen();
    %>

    <p class="b-bottom-dashed b-top f-page-cell">
        <% if (last == null || last.isEmpty()) {%>
        It seems that you either have not used the help since your last submission,
        or you are using a NetBeans 6.0 Milestone 8 build (or older). If you want to 
        access the integrated help, press <code>F1</code> in any dialog or window and 
        a context-sensitive help window displays with content related to the place
        and task you are currently performing.
        <% } else {%>
        Here is the list of the most popular contexts on which you invoked help
        during the last session.
    </p>
    <table width="100%" class="tblheader" >
        <thead><tr><th align="left">Help Topic</th><th align="left">Invocations</th></tr></thead>

        <% for (Invocations.One inv : last) {%>
        <tr class="tblheader">
            <td class="tbltd1"><% out.println(inv.getId());%></td>
            <td class="tbltd1"><% out.println(inv.getCnt());%></td>
        </tr>
        <% }%>
    </table>
    <% }%>

    <% if (user == null || user.isEmpty()) { %>
    <p>
        It seems that you either have not used the help since your last submission,
        or you are using a NetBeans 6.0 Milestone 8 build (or older). If you want to
        access the integrated help, press <code>F1</code> in any dialog or window and
        a context-sensitive help window displays with content related to the place
        and task you are currently performing.
        <% } else { %>
        Here is a cumulative list of your help invocations, and the number of times
        they occurred:
    </p>
    <table width="100%" class="tblheader" >
        <thead><tr><th align="left">Help Topic</th><th align="left">Invocations</th></thead>

        <% for (Invocations.One inv : user) { %>
        <tr class="tblheader">
            <td class="tbltd1"><% out.println(inv.getId()); %></td>
            <td class="tbltd1"><% out.println(inv.getCnt()); %></td>
        </tr>
        <% } %>
    </table>
    <% } %>

    <p class="b-bottom-dashed b-top f-page-cell">
        <% if (all == null || all.isEmpty()) { %>
            No data about help usage yet, sorry.
        <% } else { %>
            Here is a table showing the top ten most <q>confusing</q> places within
            the application you have just used. These are the places where users most often
            invoke help on.

    </p>
    <table width="100%" class="tblheader" >
        <thead><tr><th align="left">Help Topic</th><th align="left">Invocations</th></thead>

        <% for (Invocations.One inv : all) { %>
        <tr class="tblheader">
            <td class="tbltd1"><% out.println(inv.getId()); %></td>
            <td class="tbltd1"><% out.println(inv.getCnt()); %></td>
        </tr>
        <% } %>
    </table>
    <% } %>
    
    <p class="b-bottom-dashed b-top f-page-cell">
        Another interesting way to look at our data is how many times people
        really do need help. Here is a table showing the number of sessions 
        (e.g. uploaded logs) that contain the log data vs. those that do not:
        
        <ui:pie collection="globalHelpRatio" category="key" value="value"/>
    </p>
<%@include file="/WEB-INF/jspf/footer.jspf" %> 
