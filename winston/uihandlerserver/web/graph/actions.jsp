<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@ page import="org.netbeans.server.uihandler.statistics.Actions" %>
<%@ page import="org.netbeans.lib.uihandler.InputGesture" %>
<%@ page import="java.util.*" %>
<%@ taglib uri="/WEB-INF/statistics.tld" prefix="ui" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="f" uri="http://java.sun.com/jsp/jstl/functions" %>
<c:set var="path" value='/ <a href="../index.jsp">Analytics</a> / Graph / Actions'/>
<%@include file="/WEB-INF/jspf/header.jspf" %>
<ui:useStatistic name="Actions"/>

    <div class="f-page-cell bg-sky" >
        <h2>What actions are used by you and others?</h2>
        
        <p>
            This statistic allows you to see the actions that you and
            others were using while working. The actions' names are the
            actual names of the classes in the application and each of
            them is associated with the number of their invocations. This 
            page usually displays a "top ten" of the most used actions,
            however it also offers a way to customize the list of actions
            by use the of regular expressions. 
        </p>

        <hr>

        <h3>Restrict the Query!</h3>
        <p>
            Showing just a top ten of most used actions is interesting, but perhaps not
            sufficient. In order to understand users' behavior in a particular area of
            the application, it is much better to simply view a <q>local top ten</q> - e.g.
            a list of the most frequently used actions from a component of the IDE, such
            as the profiler, debugger or window system.
        </p>
        <p>
            This interactive statistics page allows you to restrict your query to display
            only actions matching specific <a href="http://java.sun.com/javase/6/docs/api/java/util/regex/Pattern.html">
            regular expression</a>. That way you can use for example <code>.*profiler.*</code>
            to see actions defined just by the profiler module, or you can use
            <code>.*profiler.*|.*debugger.*</code> to compare the usage of the profiler with
            that of the debugger. The expression <code>.*</code> signifies everything. The
            "<code>|</code>" means "or". For more advanced usages, see the
            <a href="http://java.sun.com/javase/6/docs/api/java/util/regex/Pattern.html">
            JDK documentation</a>.
        </p>

        <form method="get" action="<% out.print(HttpUtils.getRequestURL(request)); %>" >
            <table width="100%">
               <tr>
                <td>Include pattern:</td>
                <td>Exclude pattern:</td>
                <td>Number of lines:</td>
                <td>Action invocation:</td>
                <td>NetBeans version:</td>
                <td></td>
               </tr>
               <tr>
                <td><input type="text" name="ActionsIncludes" value='${ActionsIncludes!=null?ActionsIncludes:".*"}'/></td>
                <td><input type="text" name="ActionsExcludes" value="${ActionsExcludes}"/></td>
                <td><input type="text" name="ActionsCount" value='${ActionsCount!=null?ActionsCount:"10"}'/></td>
                <td>
                    <select name="ActionsInvocation">
                        <c:forEach items="${allInvocations}" var="invocItem">
                            <c:choose>
                                <c:when test="${f:contains(invocItem, ActionsInvocation)}">
                                    <option selected="selected">${invocItem}</option>
                                </c:when>
                                <c:otherwise>
                                    <option>${invocItem}</option>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </select>
                </td>
                <td><%@include file="../WEB-INF/jspf/nbVersionComboBox.jspf" %> </td>
                <td><input type="submit"/></td>
               </tr>
            </table>
        </form>

    </div>
    
    <table width="100%">
        <% 
        Map<String,Integer> last = (Map<String,Integer>)pageContext.getAttribute("lastActionsTop10");
        if (last != null) {
        %>
        <tr width="100%">
            <td width="100%">
                <div align="center">
                    <b>Your Last Session</b>
                    <p>
                        This is the top ten actions you have used during your
                        last session.
                    </p>
                    <ui:pie collection="lastActionsTop10" category="key" value="value" lblformat="%1$s = %2$s%%" resolution="800x200"/>
                </div>
            </td>
        </tr>
        <% }
        Map<String,Integer> user = (Map<String,Integer>)pageContext.getAttribute("userActionsTop10");

        if (user != null) {
        %>
        <tr width="100%">
            <td width="100%">
                <div align="center">
                    <b>All Your Sessions</b>
                    <p>
                        According to our database, this is a list of the most commonly
                        used actions in all your submitted sessions.
                    </p>
                    <ui:pie collection="userActionsTop10" category="key" value="value" lblformat="%1$s = %2$s%%" resolution="800x200"/>
                </div>
            </td>
        </tr>
        <% } 
        
        
        Map<String,Integer> all = (Map<String,Integer>)pageContext.getAttribute("globalActionsTop10");

        if (all != null) {
        %>
        <tr width="100%">
            <td width="100%">
                <div align="center">
                    <b>Global Statistics</b>
                    <p></p>
                    <ui:pie collection="globalActionsTop10" category="key" value="value" lblformat="%1$s = %2$s%%" resolution="800x200"/>
                </div>
            </td>
        </tr>
        <tr>
            <td>
            <table cellpadding="5" width="100%" >
            <thead class="tblheader">
                <th width="90%">Action Name</th>
                <th>Invocations</th>
            </thead>
            <% for (Map.Entry<String,Integer> entry : all.entrySet()) { %>
                <tr>
                    <td class="tbltd1"><% out.println(entry.getKey()); %></td>
                    <td class="tbltd1"><% out.println(entry.getValue()); %></td>
                </tr>
            <% } %>
            </table>
            </td>
        </tr>
        <% } %>
    </table>
    
<%@include file="/WEB-INF/jspf/footer.jspf" %> 
