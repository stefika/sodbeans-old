<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@ page import="org.netbeans.server.uihandler.statistics.ProjectJavaEEServers" %>
<%@ page import="java.util.Map" %>
<%@ page import="java.util.Set" %>
<%@ taglib uri="/WEB-INF/statistics.tld" prefix="ui" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 

<c:set var="path" value='/ <a href="../index.jsp">Analytics</a> / Graph / Java EE Servers'/>
<%@include file="/WEB-INF/jspf/header.jspf" %>
<ui:useStatistic name="ProjectJavaEEServers"/>

    <div class="f-page-cell bg-sky" >
        <h2>Java EE Servers Usage</h2>
        
        <p>
            For your enterprise projects you can use various Java EE servers in the IDE. 
            The following graphs show usage of different servers within enterprise projects.
        </p>
        <%@include file="restrictQuery.jspf" %>
    </div>
    
    <table width="100%">
        <% 
        Map<ProjectJavaEEServers.ProjectServer, Integer> global = (Map<ProjectJavaEEServers.ProjectServer, Integer>) pageContext.getAttribute("globalProjectJavaEEServers");
        if (global != null && !global.isEmpty()) {
            pageContext.setAttribute("webGlobal",
                    ProjectJavaEEServers.filter(ProjectJavaEEServers.ProjectType.WEB, global));
            pageContext.setAttribute("ejbGlobal",
                    ProjectJavaEEServers.filter(ProjectJavaEEServers.ProjectType.EJB, global));
            pageContext.setAttribute("earGlobal",
                    ProjectJavaEEServers.filter(ProjectJavaEEServers.ProjectType.EAR, global));
            pageContext.setAttribute("appGlobal",
                    ProjectJavaEEServers.filter(ProjectJavaEEServers.ProjectType.APP, global));
        %>
        <tr>
            <td align="center" width="480">
                <ui:pie
                    collection="webGlobal"
                    category="key"
                    value="value"
                    title="History of All Users - Web Project"
                    resolution="600x200"
                />
                <p>Usage count for a particular server in a web project.</p>
            </td>
        </tr>
        <tr>
            <td align="center" width="480">
                <ui:pie
                    collection="ejbGlobal"
                    category="key"
                    value="value"
                    title="History of All Users - EJB Project"
                    resolution="600x200"
                />
                <p>Usage count for a particular server in an EJB project.</p>
            </td>
        </tr>
        <tr>
            <td align="center" width="480">
                <ui:pie
                    collection="earGlobal"
                    category="key"
                    value="value"
                    title="History of All Users - EAR Project"
                    resolution="600x200"
                />
                <p>Usage count for a particular server in an EAR project.</p>
            </td>            
        </tr>
        <% }
        %>
        <% 
        Map<ProjectJavaEEServers.ProjectServer, Integer> user = (Map<ProjectJavaEEServers.ProjectServer, Integer>) pageContext.getAttribute("userProjectJavaEEServers");
        if (user != null && !user.isEmpty()) {
            pageContext.setAttribute("webUser",
                    ProjectJavaEEServers.filter(ProjectJavaEEServers.ProjectType.WEB, user));
            pageContext.setAttribute("ejbUser",
                    ProjectJavaEEServers.filter(ProjectJavaEEServers.ProjectType.EJB, user));
            pageContext.setAttribute("earUser",
                    ProjectJavaEEServers.filter(ProjectJavaEEServers.ProjectType.EAR, user));
            pageContext.setAttribute("appUser",
                    ProjectJavaEEServers.filter(ProjectJavaEEServers.ProjectType.APP, user));
        %>
        <tr>
            <td align="center" width="480">
                <ui:pie
                    collection="webUser"
                    category="key"
                    value="value"
                    title="All Your Sessions - Web Project"
                    resolution="600x200"
                />
                <p>
                    Usage count for a particular server in a web project, using only data 
                    submitted by you.
                </p>
            </td>
        </tr>
        <tr>
            <td align="center" width="480">
                <ui:pie
                    collection="ejbUser"
                    category="key"
                    value="value"
                    title="All Your Sessions - EJB Project"
                    resolution="600x200"
                />
                <p>
                    Usage count for a particular server in an EJB project, using only data 
                    submitted by you.
                </p>
            </td>
        </tr>
        <tr>
            <td align="center" width="480">
                <ui:pie
                    collection="earUser"
                    category="key"
                    value="value"
                    title="All Your Sessions - EAR Project"
                    resolution="600x200"
                />
                <p>
                    Usage count for a particular server in an EAR project, using only data 
                    submitted by you.
                </p>
            </td>
        </tr>         
        <% }
        %>
        <% 
        Map<ProjectJavaEEServers.ProjectServer, Integer> last = (Map<ProjectJavaEEServers.ProjectServer, Integer>) pageContext.getAttribute("lastProjectJavaEEServers");
        if (last != null && !last.isEmpty()) {
            pageContext.setAttribute("webLast",
                    ProjectJavaEEServers.filter(ProjectJavaEEServers.ProjectType.WEB, last));
            pageContext.setAttribute("ejbLast",
                    ProjectJavaEEServers.filter(ProjectJavaEEServers.ProjectType.EJB, last));
            pageContext.setAttribute("earLast",
                    ProjectJavaEEServers.filter(ProjectJavaEEServers.ProjectType.EAR, last));
            pageContext.setAttribute("appLast",
                    ProjectJavaEEServers.filter(ProjectJavaEEServers.ProjectType.APP, last));
        %>
        <tr>
            <td align="center" width="480">
                <ui:pie
                    collection="webLast"
                    category="key"
                    value="value"
                    title="Your Last Session - Web Project"
                    resolution="600x200"
                />
                <p>Usage count for a particular server in a web project. Your last session only.</p>
            </td>
        </tr>
        <tr>
            <td align="center" width="480">
                <ui:pie
                    collection="ejbLast"
                    category="key"
                    value="value"
                    title="Your Last Session - EJB Project"
                    resolution="600x200"
                />
                <p>Usage count for a particular server in an EJB project. Your last session only.</p>
            </td>
        </tr>
        <tr>
            <td align="center" width="480">
                <ui:pie
                    collection="earLast"
                    category="key"
                    value="value"
                    title="Your Last Session - EAR Project"
                    resolution="600x200"
                />
                <p>Usage count for a particular server in an EAR project. Your last session only.</p>
            </td>
        </tr>        
        <% }
        %>
    </table>

</center>
<%@include file="/WEB-INF/jspf/footer.jspf" %> 
