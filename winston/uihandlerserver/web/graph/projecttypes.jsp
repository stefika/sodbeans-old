<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@ page import="org.netbeans.server.uihandler.statistics.ProjectTypes" %>
<%@ page import="java.util.Map" %>
<%@ page import="java.util.Set" %>
<%@ taglib uri="/WEB-INF/statistics.tld" prefix="ui" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 

<c:set var="path" value='/ <a href="../index.jsp">Analytics</a> / Graph / Project Sessions'/>
<%@include file="/WEB-INF/jspf/header.jspf" %>
<ui:useStatistic name="ProjectTypes"/>

<div class="f-page-cell bg-sky" >
    <h2>Project Types Usage</h2>

    <p>
        NetBeans supports various project types. For example, there is a project
        type for a standard Java application, another for a web application, yet
        another for Java mobility, as well as free-form projects based on plain
        Ant scripts, etc. The following graph displays statistics on the project
        types that are being used:
    </p>

    <hr>

    <h3>Restrict the Query!</h3>
    <p>
            You can modify the minimal percentage of usage of
            a project type. All types used less than this percentage
            will be included in the 'Others' category:
    </p>
    <form method="get" action="<% out.print(HttpUtils.getRequestURL(request));%>" >
        <table width="70%">
            <tr>
                <td>Minimal usage of a project type:</td>
                <td>NetBeans version: </td>
                <td></td>
            </tr>
            <tr>
                <td><input size="2" maxlength="2" type="text" name="minimal" value='${minimal!=null?minimal:"5"}'/> %</td>
               <td><%@include file="../WEB-INF/jspf/nbVersionComboBox.jspf" %></td>
                <td><input type="submit"/></td>
            </tr>
        </table>
    </form>
</div>
    
    <table width="100%">
        <% 
        if (pageContext.getAttribute("globalProjectTypes") != null) {
        %>
<%--        <tr>
            <td align="center" width="480">
                <ui:pie
                    collection="globalProjectTypes"
                    category="key"
                    value="value"
                    title="History of All Users"
                />
                <p>
                    This chart shows what percentage is hold be each project in
                    comparision to the others.
                </p>
            </td>
        </tr>--%>
        <tr>
            <td align="center" width="480">
                <ui:bar
                    collection="globalProjectTypesList"
                    category="key"
                    value="value"
                    orientation="vertical"
                    serie="serie"
                    title="History of All Users"
                    showlegend="true"
                    legendposition="right"
                    resolution="600x250"
                    stacked="true"
                    lblformat="%1$s (%2$s%%)"
                    maxscale='<%= (Integer) pageContext.getAttribute("globalProjectTypesMaxScale") %>'
                />
                <p>
                    This chart shows what percentage of users is using individual project types. One
                    user can use more project types in one session.
                </p>
            </td>
        </tr>
        <% }
        %>
        <% 
        if (pageContext.getAttribute("userProjectTypes") != null) {
        %>
        <tr>
            <td align="center" width="480">
                <ui:pie
                    collection="userProjectTypes"
                    category="key"
                    value="value"
                    title="All Your Sessions"
                />
                <p>
                    Here is the history of your usage of various projects
                    over all your submitted sessions.
                </p>
            </td>
        </tr>
        <% }
        %>
        <% 
        if (pageContext.getAttribute("lastProjectTypes") != null) {
        %>
        <tr>
            <td align="center" width="480">
                <ui:pie
                    collection="lastProjectTypes"
                    category="key"
                    value="value"
                    title="Your Last Session"
                />
                <p>
                    This is the percentage of project types affected by you during
                    your last session.
                </p>
            </td>
        </tr>
        <% }
        %>
    </table>

<%@include file="/WEB-INF/jspf/footer.jspf" %> 
