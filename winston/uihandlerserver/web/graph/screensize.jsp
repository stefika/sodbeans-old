<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@ taglib uri="/WEB-INF/statistics.tld" prefix="ui" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<ui:useStatistic name="ScreenSize"/>

<c:set var="path" value='/ <a href="../index.jsp">Analytics</a> / Graph / Screen Size'/>
<%@include file="/WEB-INF/jspf/header.jspf" %>
<div class="f-page-cell bg-sky" >
    <h2>Statistics on NetBeans users screen size and monitors count</h2>
    
    <p>
        This statistics describes what is the mostly used screen resolution by NetBeans users
        and how many monitors are they using.
    </p>
    <%@include file="restrictQuery.jspf" %>
</div>
<table width="100%">
    <c:if test="${globalScreenSizeRESOLUTION != null}" >
        <tr>
            <td align="center" width="480">
                <ui:pie collection="globalScreenSizeRESOLUTION"
                        category="key"
                        value="value"
                        title="Screen size of NetBeans users"/>
            </td>
        </tr>
    </c:if>
    <c:if test="${globalScreenSizeMONITORS != null}" >
        <tr>
            <td align="center" width="480">
                <ui:pie collection="globalScreenSizeMONITORS"
                        category="key"
                        value="value"
                        title="Monitors count of NetBeans users"/>
            </td>
        </tr>
    </c:if>
</table>

<%@include file="/WEB-INF/jspf/footer.jspf" %> 