<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@ page import="org.netbeans.server.uihandler.statistics.GenericMap" %>
<%@ page import="java.util.Map" %>
<%@ taglib uri="/WEB-INF/statistics.tld" prefix="ui" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/functions" %>
<c:set var="path" value='/ <a href="../index.jsp">Analytics</a> / Graph / Miscellaneous'/>

<%@include file="/WEB-INF/jspf/header.jspf" %>
<ui:useStatistic name="GenericStatistic"/>

<div class="f-page-cell bg-sky" >
    <h2>This is a generic statistic for miscellaneous data.</h2>


    <p>
        Do you want your chart here? Just start logging into org.netbeans.ui logger with a message <i>GENERIC_XYZ</i>.
        The XYZ will be used here as a name of statistic and first parametter is used as a value that whose occurences are being counted.
    </p>
    <hr>

    <h3>Restrict the Query!</h3>
    <form method="get" action="<% out.print(HttpUtils.getRequestURL(request));%>" >
        <c:if test="${empty category_message}">
            <c:set var="category_message" value="All"/>
        </c:if>
        <c:if test="${empty generic_type}">
            <c:set var="generic_type" value="Both"/>
        </c:if>
        <table width="70%">
            <tr>
                <td>Message: </td>
                <td>Statistic type: </td>
                <td>NetBeans version: </td>
                <td></td>
            </tr>
            <tr>
                <td>
                    <select name="category_message" style="min-width: 150px">
                        <c:choose>
                            <c:when test="${category_message == 'All'}">
                                <option selected="selected">All</option>
                            </c:when>
                            <c:otherwise>
                                <option>All</option>
                            </c:otherwise>
                        </c:choose>
                        <c:forEach items="${globalGenericStatistic.categories}" var="categoryItem">
                            <c:choose>
                                <c:when test="${f:contains(categoryItem, category_message)}">
                                    <option selected="selected">${categoryItem}</option>
                                </c:when>
                                <c:otherwise>
                                    <option>${categoryItem}</option>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </select>
                </td>
                <td>
                    <select name="generic_type" style="min-width: 150px">
                        <c:forEach items="Both,Invocations,Logs" var="typeItem">
                            <c:choose>
                                <c:when test="${f:contains(typeItem, generic_type)}">
                                    <option selected="selected">${typeItem}</option>
                                </c:when>
                                <c:otherwise>
                                    <option>${typeItem}</option>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </select>
                </td>
                <td><%@include file="../WEB-INF/jspf/nbVersionComboBox.jspf" %></td>
                <td><input type="submit"/></td>
            </tr>
        </table>
    </form>
</div>
<table>
    <c:forEach items="${globalGenericStatistic.categories}" var="categoryItem">
        <c:if test="${empty category_message|| category_message == 'All' || category_message == categoryItem}">
            <c:if test="${empty generic_type || generic_type == 'Invocations' || generic_type == 'Both'}">
                <c:set var="data" value="${exc:getCounts(globalGenericStatistic, categoryItem, false, true)}"/>
                <tr>
                    <td align="center" width="600">
                        <ui:pie
                            collection="data"
                            category="key"
                            value="value"
                            title="All invocations of the ${categoryItem}"
                            />
                    </td>
                    <td>
                        <table width="100%" >
                            <thead class="tblheader">
                            <th width="80%">Value name</th>
                            <th>Invocations</th>
                            </thead>
                            <tbody>
                                <c:forEach items="${exc:getCounts(globalGenericStatistic, categoryItem, false, false)}" var="entry">
                                    <tr>
                                        <td class="tbltd1">${entry.key}</td>
                                        <td class="tbltd1">${entry.value}</td>
                                    </tr>
                                </c:forEach>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </c:if>
            <c:if test="${empty generic_type || generic_type == 'Logs' || generic_type == 'Both'}">
                <c:set var="data" value="${exc:getCounts(globalGenericStatistic, categoryItem, true, true)}"/>
                <tr>
                    <td align="center" width="600">
                        <ui:pie
                            collection="data"
                            category="key"
                            value="value"
                            title="Number of logs with an occurence of the ${categoryItem}"
                            />
                    </td>
                    <td>
                        <table width="100%" >
                            <thead class="tblheader">
                            <th width="80%">Value name</th>
                            <th>Number of logs with an occurence</th>
                            </thead>
                            <tbody>
                                <c:forEach items="${exc:getCounts(globalGenericStatistic, categoryItem, true, false)}" var="entry">
                                    <tr>
                                        <td class="tbltd1">${entry.key}</td>
                                        <td class="tbltd1">${entry.value}</td>
                                    </tr>
                                </c:forEach>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </c:if>
            <tr style="border-bottom: solid black 2px"></tr>
        </c:if>
    </c:forEach>
</table>
<%@include file="/WEB-INF/jspf/footer.jspf" %> 
