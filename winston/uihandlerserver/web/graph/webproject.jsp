<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@ page import="org.netbeans.lib.uihandler.InputGesture, java.util.TreeMap, java.util.Comparator, java.util.Collections, java.util.List, java.util.ArrayList, org.netbeans.server.uihandler.statistics.WebProjects, org.netbeans.server.uihandler.statistics.WebProjects.WebProjectItem, java.util.Iterator" %>
<%@ page import="java.util.Map" %>
<%@ page import="java.util.Set" %>
<%@ taglib uri="/WEB-INF/statistics.tld" prefix="ui" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 

<c:set var="path" value='/ <a href="../index.jsp">Analytics</a> / Graph / WebProject'/>

<%@include file="/WEB-INF/jspf/header.jspf" %>
<ui:useStatistic name="WebProjects"/>
<%
    Map<WebProjects.WebProjectItem, Integer> data = (Map) pageContext.getAttribute("globalWebProjects");
    if (data != null) {
        List<WebProjects.ChartBean> frameworks = new ArrayList ();
        List<WebProjects.ChartBean> servers = new ArrayList ();
        Map<String, Integer> j2ee = new TreeMap<String, Integer> ();

        if (data != null && !data.isEmpty()) {
            for (Iterator<WebProjectItem> it = data.keySet().iterator(); it.hasNext();) {
               WebProjectItem item = it.next();
               if (item.getType() == WebProjectItem.Type.FRAMEWORK) {
                   frameworks.add(new WebProjects.ChartBean(item.getName(), data.get(item), "1"));
               }
               if (item.getType() == WebProjectItem.Type.SERVER) {
                   servers.add(new WebProjects.ChartBean(item.getName(), data.get(item), "1"));
               }
            }
            Collections.sort(frameworks);
            Collections.sort(servers);
        }
        pageContext.setAttribute("frameworks", frameworks);
        pageContext.setAttribute("servers", servers);
        %>

    <div class="f-page-cell bg-sky" >
        <h2>Web Project Statistics</h2>
        
        <p>
            A web project can use many different frameworks and application servers. 
            This statistic shows which frameworks and servers where added at project creation.
        </p>
        <%@include file="restrictQuery.jspf" %>
    </div>
    <table width="100%">
        <tr>
            <td align="center" width="480">
                <ui:bar
                    resolution="600x400"
                    serie="serie"
                    orientation="HORIZONTAL"
                    collection="frameworks" 
                    category="name" 
                    value="value" 
                    title="Frameworks"
                    showlegend="false"
                />
                <!-- explodePercent="0.25" explodePosition="0"  -->
                <p>
                    This graph displays the usage of frameworks in web projects.
                </p>
            </td>
        </tr>
        <tr>
            <td align="center" width="480">
                <ui:bar
                    resolution="600x400"
                    serie="serie"
                    orientation="HORIZONTAL"
                    collection="servers" 
                    category="name" 
                    value="value" 
                    title="Servers"
                    showlegend="false"
                />
                <!-- explodePercent="0.25" explodePosition="0"  -->
                <p>
                    This graph displays the usage of servers in newly created web projects.
                </p>
            </td>
        </tr>
        <%
        }
        %>
    </table>
    </center>
<%@include file="/WEB-INF/jspf/footer.jspf" %> 
