<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@ page import="org.netbeans.lib.uihandler.InputGesture" %>
<%@ page import="java.util.Map" %>
<%@ page import="java.util.Set" %>
<%@ taglib uri="/WEB-INF/statistics.tld" prefix="ui" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 

<c:set var="path" value='/ <a href="../index.jsp">Analytics</a> / Graph / Gestures'/>

<%@include file="/WEB-INF/jspf/header.jspf" %>
<ui:useStatistic name="Gestures"/>

    <div class="f-page-cell bg-sky" >
        <h2>Are people using the mouse or the keyboard?</h2>
        
        <p>
            Most of the actions in NetBeans can be invoked either through a menu, 
            toolbar, or keyboard. Some users prefer mouse usage while others 
            predominantly use the keyboard. This graphs provide an overview of these
            actions, and allow you to compare your own statistics to the statistics 
            of others.
        </p>
        <%@include file="restrictQuery.jspf" %>
    </div>
    
    <table width="100%">
        <% 
        Map<InputGesture,Integer> last = (Map<InputGesture,Integer>)pageContext.getAttribute("lastGestures");
        if (last != null && !last.isEmpty()) {
        %>
        <tr>
            <td align="center" width="480">
                <ui:pie 
                    collection="lastGestures" 
                    category="key" 
                    value="value" 
                    title="Last Session"
                />
<!--                    align="center" 
                    rotation="anticlockwise"
                    -->
                <!-- explodePercent="0.25" explodePosition="0"  -->
                <p>
                    This graph shows data gathered during your last session. It shows 
                    how many actions you have invoked, as well as the percentage of those 
                    invoked using a mouse in a toolbar or menu, or directly by the keyboard.
                </p>
            </td>
        </tr>
        <% }
        Map<InputGesture,Integer> user = (Map<InputGesture,Integer>)pageContext.getAttribute("userGestures");

        if (user != null && !user.isEmpty()) {
        %>
        <tr>
            <td align="center" width="480">
                <ui:pie 
                    collection="userGestures" 
                    category="key" 
                    value="value" 
                    title="All Your Sessions" 
                />
                <!-- explodePercent="0.25" explodePosition="0"  -->
                <p>
                    This graph shows data collected from all your sessions. Again, the 
                    total sum is the number of all started actions. The pie graphs describe 
                    those executed via menu, toolbar and keyboard.
                </p>
                
            </td>
        </tr>
        <% } 
        
        
        Map<InputGesture,Integer> all= (Map<InputGesture,Integer>)pageContext.getAttribute("globalGestures");

        if (all != null && !all.isEmpty()) {
        %>
        <tr>
            <td align="center" width="480">
                <ui:pie 
                    collection="globalGestures" 
                    category="key" 
                    value="value" 
                    title="Global" 
                />
                <!-- explodePercent="0.25" explodePosition="0"  -->
                <p>
                    In order to give you an idea of how others use the IDE, here is a 
                    total aggregation of invoked actions grouped by the way they were 
                    invoked over all recorded sessions from all users. Feel free to compare
                    your usage with the average.
                </p>
            </td>
        </tr>
        <% } %>
    </table>
<%@include file="/WEB-INF/jspf/footer.jspf" %> 
