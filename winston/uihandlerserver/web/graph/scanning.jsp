<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@ page import="java.util.Map" %>
<%@ page import="java.util.Set" %>
<%@ taglib uri="/WEB-INF/statistics.tld" prefix="ui"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<c:set var="path" value='/ <a href="../index.jsp">Analytics</a> / Graph / JavaHelp'/>
<%@include file="/WEB-INF/jspf/header.jspf" %>
<ui:useStatistic name="ScanningExtChanges"/>

    <div class="f-page-cell bg-sky" >
        <h2>Do people use scanning for external changes?</h2>
            <p>
        This statistics shows if users are using "Scanning for external changes" feature.
    </p>


        <%@include file="restrictQuery.jspf" %>
    </div>

   <p class="b-bottom-dashed b-top f-page-cell">

          <ui:pie collection="globalScanningExtChangesRatio"
                        category="key"
                        value="value"
                        title="Scanning for external changes"/>
   </p>

<%@include file="/WEB-INF/jspf/footer.jspf" %>
