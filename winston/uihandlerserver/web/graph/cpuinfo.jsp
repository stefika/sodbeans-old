<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@ taglib uri="/WEB-INF/statistics.tld" prefix="ui" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<ui:useStatistic name="CPUInfo"/>

<c:set var="path" value='/ <a href="../index.jsp">Analytics</a> / Graph / Screen Size'/>
<%@include file="/WEB-INF/jspf/header.jspf" %>
<div class="f-page-cell bg-sky" >
    <h2>Statistics on NetBeans users CPU count</h2>
    
    <p>
        This statistics describes what how many CPUs are NetBeans users using.
    </p>
    <%@include file="restrictQuery.jspf" %>
</div>
<table width="100%">
    <c:if test="${globalCPUInfo != null}" >
        <tr>
            <td align="center" width="480">
                <ui:pie collection="globalCPUInfo"
                        category="key"
                        value="value"
                        title="CPUs count of NetBeans users"/>
            </td>
        </tr>
    </c:if>
</table>

<%@include file="/WEB-INF/jspf/footer.jspf" %> 