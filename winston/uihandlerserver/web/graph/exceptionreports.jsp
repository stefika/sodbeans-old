<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@ page import="java.util.TreeMap" import="java.util.Iterator"%>
<%@ page import="org.netbeans.server.uihandler.LogsManager" %>
<%@ page import="org.netbeans.server.uihandler.statistics.ExceptionReports" %>
<%@ taglib uri="/WEB-INF/statistics.tld" prefix="ui" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 

<c:set var="path" value='/ <a href="../index.jsp">Analytics</a> / Graph / Exception Reports'/>
<%@include file="/WEB-INF/jspf/header.jspf" %>
<ui:useStatistic name="ExceptionReports"/>

<div class="f-page-cell bg-sky" >
    <h2>Exception Reports Count</h2>
    
    <p>
        The exception reporting module is a component of the <b>UI Gestures Collector</b>.
        The <b>UI Gestures Collector</b> helps developers reproduce steps required to  
        cause an exception in NetBeans IDE, and consequently how to determine the root 
        of a problem. All gestures are send with an exception report, but you can also
        send them by using the UI Gestures button in IDE. This statistic shows how many 
        reports are sent as statistics data, and how many are actually reporting exceptions.            
    </p>
</div>

<table width="100%">
    <%
            ExceptionReports.Data exceptionReports =
                    (ExceptionReports.Data) pageContext.getAttribute("globalExceptionReports");
            if (exceptionReports != null) {
                TreeMap results = new TreeMap<String, Integer>();
                results.put("Exception reports", exceptionReports.getReportsCount());
                results.put("Slowness reports", exceptionReports.getSlownessCount());
                results.put("Gestures reports", exceptionReports.getGesturesCount());
                pageContext.setAttribute("results", results);
    %>
    <tr>
        <td align="center" width="480">
            <ui:pie
                collection="results"
                category="key"
                value="value"
                title="Comparison of exceptions and gesture reports"
                resolution="600x200"
                />
            <p>
                This is the overall statistic of all the submitted data.
            </p>
        </td>
    </tr>
    <%
        pageContext.setAttribute("components", exceptionReports.getComponents());
        if (exceptionReports.getComponents() != null) {%>
    <tr>
        <td align="center" width="480">
            <ui:pie
                collection="components"
                category="key"
                value="value"
                title="Components issues numbers"
                resolution="600x200"
                />
            <p>
                This is the overall statistic of issue numbers assigned to 
                different components.
            </p>
        </td>
        <td>
            <table align="center" style="border: 2px solid;">
                <thead>
                    <td style="border: 1px solid">Component</td>
                    <td style="border: 1px solid">Number of reports</td>
                </thead>
                <tbody>
                    <% Iterator<String> iterator = exceptionReports.getSortedKeys().iterator();
        while (iterator.hasNext()) {
            String key = iterator.next();
            Integer value = exceptionReports.getComponents().get(key);
            out.println("<tr> <td style='border: 1px solid'> " + key + "</td>");
            out.println("<td style='border: 1px solid'> " + value + "</td> </tr>");
        }
                    %>
                </tbody>
            </table>   
            
        </td>
    </tr>
    <% }
        pageContext.setAttribute("monthStats", exceptionReports.getMonthStats());
        if ((exceptionReports.getMonthStats() != null) && (exceptionReports.getMonthStats().size() > 0)) {%>
    <tr>
        <td align="center" width="480">
            <div id="monthStats">
              <ui:bar
                    resolution="900x300"
                    collection="monthStats"
                    category="key"
                    serie="serie"
                    value="value"
                    title="Reports by the month of the build date"
                    stacked="true"
                    showlegend="true"
                    legendposition="right"
              />
            </div>
            <p>
                This is a statistic describing the number of error reports selected by 
                the month of the build date.
            </p>
        </td>
        <td></td>
    </tr>
    
    <%  }
        pageContext.setAttribute("reportDateStats", exceptionReports.getReportDateStats());
        if ((exceptionReports.getReportDateStats() != null) && (exceptionReports.getReportDateStats().size() > 0)) {%>
    <tr>
        <td align="center" width="480">
            <div id="reportDateStats">
                <ui:bar
                    resolution="900x300"
                    collection="reportDateStats"
                    category="key"
                    serie="serie"
                    value="value"
                    title="Reports by the month of the report date"
                    stacked="true"
                    showlegend="true"
                    legendposition="right"
                    />
            </div>
            <p>
                This is a statistic describing the number of error reports selected by 
                the month of the report date.
            </p>
        </td>
    </tr>
    
    <%  } %>
    
    <% } %>
    
</table>

<%@include file="/WEB-INF/jspf/footer.jspf" %> 
