<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@ taglib uri="/WEB-INF/statistics.tld" prefix="ui" %>
<c:set var="path" value='/ <a href="../index.jsp">Analytics</a> / Graph / Sodbeans Output Usage'/>
<%@include file="/WEB-INF/jspf/header.jspf" %>
<ui:useStatistic name="SodbeansOutputStatistics"/>

<h2>Sodbeans Output Window Analysis</h2>
        
<ui:pie
   collection="globalSodbeansOutputStatisticsUsages"
   category="key"
   value="value"
   title="In how many logs was the Sodbeans output window used?"
   resolution="600x200"
   />

<ui:pie
    collection="globalSodbeansOutputStatisticsAvg"
    category="name"
    value="value"
    title="Was the Sodbeans output window used for compiler errors or other output?"
    resolution="600x200"
    />
    
<%@include file="/WEB-INF/jspf/footer.jspf" %>