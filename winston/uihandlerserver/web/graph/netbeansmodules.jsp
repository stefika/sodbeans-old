<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@ page import="java.util.List" import="java.util.Iterator"%>
<%@ page import="org.netbeans.server.uihandler.statistics.NetBeansModules" %>
<%@ taglib uri="/WEB-INF/statistics.tld" prefix="ui" %>
<ui:useStatistic name="NetBeansModules"/>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 

<c:set var="path" value='/ <a href="../index.jsp">Analytics</a> / Graph / NetBeans Modules'/>
<%@include file="/WEB-INF/jspf/header.jspf" %>

<div class="f-page-cell bg-sky" >
    <h2>NetBeans Modules Usage Statistics</h2>
    
    <p>
        These statistics measure the usage of individual modules in the IDE.
    </p>
    <%@include file="restrictQuery.jspf" %>
</div>

<table class="tblheader" width="100%">
    <tr>
        <th><a href="netbeansmodules.jsp?sortedBy=0">Module name:</a></th>
        <th><a href="netbeansmodules.jsp?sortedBy=1">Enabled modules count:</a></th>
        <th><a href="netbeansmodules.jsp?sortedBy=2">Disabled modules count:</a></th>
    </tr>
    <%
            NetBeansModules.NBMData data = (NetBeansModules.NBMData) pageContext.getAttribute("globalNetBeansModules");
            if (data != null) {
                out.print(data.getSorted(pageContext.getRequest().getParameter("sortedBy")));
            }
    %>
    
</table>

<%@include file="/WEB-INF/jspf/footer.jspf" %> 