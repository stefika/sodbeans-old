<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@ page import="org.netbeans.lib.uihandler.InputGesture, java.util.TreeMap, java.util.Comparator, java.util.Collections, java.util.List, java.util.ArrayList, org.netbeans.server.uihandler.statistics.MavenTypes, org.netbeans.server.uihandler.statistics.MavenTypes.Item, java.util.Iterator" %>
<%@ page import="java.util.Map" %>
<%@ page import="java.util.Set" %>
<%@ taglib uri="/WEB-INF/statistics.tld" prefix="ui" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 

<c:set var="path" value='/ <a href="../index.jsp">Analytics</a> / Graph / Maven Project Packagings'/>

<%@include file="/WEB-INF/jspf/header.jspf" %>
<ui:useStatistic name="MavenTypes"/>
<%
    Map<MavenTypes.Item, Integer> data = (Map) pageContext.getAttribute("globalMavenTypes");
    if (data != null) {
    List<MavenTypes.ChartBean> maventypes = new ArrayList ();
        
    if (data != null && !data.isEmpty()) {
        for (Iterator<MavenTypes.Item> it = data.keySet().iterator(); it.hasNext();) {
           MavenTypes.Item item = it.next();
           maventypes.add(new MavenTypes.ChartBean(item.getName(), data.get(item), "1"));
        }
        Collections.sort(maventypes);
    }
    pageContext.setAttribute("maventypes", maventypes);
        %>

    <div class="f-page-cell bg-sky" >
        <h2>Maven Project Statistics</h2>
        
        <p>
            Maven project's funtionality spans across multiple classical Ant based project types. Based on the project's packaging
            (as defined in the pom.xml file) the IDE will make specific features available.
            This graph shows what packagings are being used by NetBeans Maven users.
        </p>
        <%@include file="restrictQuery.jspf" %>
    </div>
    <table width="100%">
        <tr>
            <td align="center" width="480">
                <ui:bar
                    resolution="600x400"
                    serie="serie"
                    orientation="HORIZONTAL"
                    collection="maventypes"
                    category="name" 
                    value="value" 
                    title="Maven Packaging"
                    showlegend="true"
                />
                <!-- explodePercent="0.25" explodePosition="0"  -->
                <p>
                    This graph displays the packaging defined in Maven projects.
                </p>
            </td>
        </tr>
        <%
        }
        %>
    </table>
    </center>
<%@include file="/WEB-INF/jspf/footer.jspf" %> 
