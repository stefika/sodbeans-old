<%@page import="org.netbeans.server.uihandler.statistics.OSAndJVMTypes"%>
<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@ page import="org.netbeans.server.uihandler.statistics.OSAndJVMTypes" %>
<%@ page import="org.netbeans.lib.uihandler.InputGesture" %>
<%@ page import="java.util.*" %>
<%@ taglib uri="/WEB-INF/statistics.tld" prefix="ui" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="f" uri="http://java.sun.com/jsp/jstl/functions" %>
<c:set var="path" value='/ <a href="../index.jsp">Analytics</a> / Graph / OSes and JVMs'/>
<%@include file="/WEB-INF/jspf/header.jspf" %>
<ui:useStatistic name="OSAndJVM"/>

    <div class="f-page-cell bg-sky" >
        <h2>Operating Systems and Virtual Machines</h2>
        
        <p>
            This statistic allows you to see what operating system
            and various Java virtual machines are people using. To 
            get best from this statistic you need to interact with it
            and query it to restrict and format the results. 
        </p>

        <hr>

        <h3>Query!</h3>
        <p>
            Showing just a top ten of most used operating systems 
            and virtual machines is interesting, but perhaps you want to
            group some results, or include just those interesting to you.
            You can do a lot of that by using regular expressions.
        </p>
        <p>
            This interactive statistics page allows you to restrict your query to display
            only actions matching specific <a href="http://java.sun.com/javase/6/docs/api/java/util/regex/Pattern.html">
            regular expression</a>. That way you can use for example <code>.*OpenJDK.*</code>
            to see results for OpenJDK only. Or you can use
            <code>.*(OpenJDK|HotSpot).*</code> to group all flavors of 
            OpenJDK and HotSpot into two groups and display just those.
            For more advanced usages, see the
            <a href="http://java.sun.com/javase/6/docs/api/java/util/regex/Pattern.html">
            JDK documentation</a>.
        </p>

        <form method="get" action="<% out.print(HttpUtils.getRequestURL(request)); %>" >
            <table width="100%">
               <tr>
                <td>Filter & Group:</td>
                <td>Number of results:</td>
                <td>NetBeans version:</td>
                <td></td>
               </tr>
               <tr>
                <td><input type="text" name="Includes" value='${Includes!=null?Includes:"(.*):(.*):(.*):(.*)"}'/></td>
                <td><input type="text" name="Count" value='${Count!=null?Count:"10"}'/></td>
                <td><%@include file="../WEB-INF/jspf/nbVersionComboBox.jspf" %> </td>
                <td><input type="submit"/></td>
               </tr>
            </table>
        </form>
    </div>
    
    <table width="100%">
        <% 
        Object last = pageContext.getAttribute("lastOSAndJVM");
        if (last != null) {
        %>
        <tr width="100%">
            <td width="100%">
                <div align="center">
                    You have used <b>${lastOSAndJVM}</b> during your last 
                    session.
                </div>
            </td>
        </tr>
        <% }
        if (pageContext.getAttribute("userOSAndJVM") != null) {
        %>
        <tr width="100%">
            <td width="100%">
                <div align="center">
                    <b>All Your Sessions</b>
                    <p>
                        This can be more interesting. Did you changed your
                        operating system or Java virtual machine over time?
                    </p>
                    <ui:pie collection="userOSAndJVM" category="key" value="value" lblformat="%1$s = %2$s%%" resolution="800x200"/>
                </div>
            </td>
        </tr>
        <% } 
        
        if (pageContext.getAttribute("globalOSAndJVM") != null) {
        %>
        <tr width="100%">
            <td width="100%">
                <div align="center">
                    <b>Global Statistics</b>
                    <p></p>
                    <ui:pie collection="globalOSAndJVM" category="key" value="value" lblformat="%1$s = %2$s%%" resolution="800x200"/>
                </div>
            </td>
        </tr>
        <% } %>
    </table>
    
<%@include file="/WEB-INF/jspf/footer.jspf" %> 
