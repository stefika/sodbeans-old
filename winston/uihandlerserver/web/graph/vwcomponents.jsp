<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@ page import="java.util.ArrayList, java.text.NumberFormat, java.util.List, java.util.Collections, java.util.Map, java.util.Map.Entry, java.util.Comparator, org.netbeans.server.uihandler.statistics.VWComponents, org.netbeans.server.uihandler.statistics.VWComponents.VWComponent, java.util.Iterator, java.util.TreeMap" %>
<%@ page import="java.util.Map" %>
<%@ page import="java.util.Set" %>
<%@ taglib uri="/WEB-INF/statistics.tld" prefix="ui" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 

<c:set var="path" value='/ <a href="../index.jsp">Analytics</a> / Graph / VWComponents'/>

<%@include file="/WEB-INF/jspf/header.jspf" %>
<div class="f-page-cell bg-sky" >
    <ui:useStatistic name="VWComponents"/>
        <h2>Visual Web Components Usage</h2>
        <p>
            This statistics describes the usage of Visual Web Components.
        </p>
        <%@include file="restrictQuery.jspf" %>
</div>
<table width="100%">
    <tr>
        <td>
            <%
            Map<VWComponents.VWComponent, Integer> data = (Map) pageContext.getAttribute("globalVWComponents");
            if (data != null) {
                Integer sum = 0;
                List<Map.Entry<VWComponent, Integer>> l = new ArrayList<Map.Entry<VWComponent, Integer>>(data.entrySet());
                Collections.sort(l, new Comparator<Map.Entry<VWComponent, Integer>>() {

                    public int compare(Entry<VWComponent, Integer> o1, Entry<VWComponent, Integer> o2) {
                        return o2.getValue() - o1.getValue();
                    }
                });
                for (Integer value : data.values()) {
                    sum += value;
                }
            %>

            <table>
                <tr class="tblheader">
                    <th>
                        Component Name
                    </th>
                    <th>
                        Class
                    </th>
                    <th>
                        Usages&nbsp;&nbsp;
                    </th>
                    <th>
                        %&nbsp;&nbsp;
                    </th>
                </tr>
                <%
                int i = 0;
                for (Map.Entry<VWComponent, Integer> entry : l) {
                %>
                <tr class=<%=(i++ % 2) == 0 ? "a" : "b"%>>
                    <td>
                        <b><%=entry.getKey().getName()%></b>
                    </td>
                    <td>
                        <%=entry.getKey().getClazz()%>
                    </td>
                    <td>
                        <%=entry.getValue()%>
                    </td>
                    <td>
                        <b><%=NumberFormat.getInstance().format(entry.getValue() * 100 / sum)%></b>
                    </td>
                </tr>
                <%
                }
                %>
            </table>
            <%
            }
            %>
        </td>
    </tr>
</table>
<%@include file="/WEB-INF/jspf/footer.jspf" %> 
