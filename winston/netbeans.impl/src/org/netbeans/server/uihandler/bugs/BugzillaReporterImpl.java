/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 2009 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 *
 * Contributor(s):
 *
 * Portions Copyrighted 2009 Sun Microsystems, Inc.
 */
package org.netbeans.server.uihandler.bugs;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.httpclient.Header;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.multipart.ByteArrayPartSource;
import org.apache.commons.httpclient.methods.multipart.FilePart;
import org.apache.commons.httpclient.methods.multipart.MultipartRequestEntity;
import org.apache.commons.httpclient.methods.multipart.Part;
import org.apache.commons.httpclient.methods.multipart.StringPart;
import org.netbeans.server.uihandler.api.bugs.BugReporter;
import org.netbeans.server.uihandler.api.bugs.BugReporterException;
import org.netbeans.server.uihandler.api.Submit;
import org.openide.util.NbBundle;

/**
 *
 * @author Jindrich Sedek
 */
public class BugzillaReporterImpl {

    private static final Pattern SHOW_BUG_PATTERN = Pattern.compile("<a href=\"show_bug.cgi\\?id=([0-9]*)\">");
    private static final String URL_PREFIX = NbBundle.getMessage(BugzillaReporterImpl.class, "BZ_URL");
    public static final String SHOW_BUG_URL = URL_PREFIX + "show_bug.cgi?id=";
    public static final String PROCESS_BUG_URL = URL_PREFIX + "process_bug.cgi";
    public static final String POST_BUG_URL = URL_PREFIX + "post_bug.cgi";
    public static final String POST_ATTACHMENT = URL_PREFIX + "attachment.cgi";
    public static final String LOGIN_URL = URL_PREFIX + "index.cgi";
    public static final String EXCEPTION_DETAIL = "http://statistics.netbeans.org/exceptions/detail.do?id=";
    public static final String UNIVERZAL_VERSION = "7.0";

    public Integer reportSubmit(Submit submit, String password, String extraComment, String extraCC) throws BugReporterException {
        try {
            HttpClient client = login(submit.getReporter().getUserName(), password);
            Set<String> allExtraCC = new HashSet<String>();
            if (password == null){
                allExtraCC.add(submit.getReporter().getUserName());
                if (extraCC != null){
                    allExtraCC.add(extraCC);
                }
            }
            return createNewBug(submit, submit.getBugzillaVersion(), client, false, extraComment, allExtraCC);
        } catch (IOException ex) {
            throw new BugReporterException(ex);
        }
    }

    public void createAttachment(Submit submit, String password) throws BugReporterException {
        try {
            HttpClient client = login(submit.getReporter().getUserName(), password);
            createAttachment(submit.getAttachment(), submit.getBugzillaId(), submit.getCommentString(), client);
            String userName = submit.getReporter().getUserName();
            if (BugReporter.GUEST_USER.equals(userName)){
                return;
            }
            String email = ReporterUtils.userNameToEMail(userName);
            try {
                addToCCList(submit.getBugzillaId(), email, client);
            } catch (Exception e) {
                BugReporter.LOG.log(Level.WARNING, "Adding user " + email + " at cc failed", e);
            }

        } catch (IOException ex) {
            BugReporter.LOG.log(Level.SEVERE, "Creating attachement failed", ex);
            throw new BugReporterException(ex);
        }
    }

    public void addToCCList(int bugId, String username, String password) {
        try {
            addToCCList(bugId, username, login(username, password));
        } catch (IOException ex) {
            BugReporter.LOG.log(Level.SEVERE, "Adding user to CC list failed for bug #" + bugId, ex);
            throw new BugReporterException(ex);
        }
    }

    public void postTextComment(int issueId, String text, String username, String password) {
        try {
            ArrayList<NameValuePair> data = new ArrayList<NameValuePair>();
            data.add(new NameValuePair("comment", text));
            runBZQuery(issueId, PROCESS_BUG_URL, data, "Changes submitted", login(username, password));
            BugReporter.LOG.log(Level.INFO, "Comment was added to bug {0}", issueId);
        } catch (IOException e) {
            BugReporter.LOG.log(Level.SEVERE, "Comment was not added", e);
            throw new BugReporterException(e);
        }
    }

    public void setIssuePriority(int issueId, String priority, String username, String password) {
        try {
            ArrayList<NameValuePair> data = new ArrayList<NameValuePair>();
            data.add(new NameValuePair("priority", priority));
            runBZQuery(issueId, PROCESS_BUG_URL, data, "Changes submitted", login(username, password));
            BugReporter.LOG.log(Level.INFO, "Priority of bug {0} was changed to {1}", new Object[]{issueId, priority});
        } catch (IOException ex) {
            BugReporter.LOG.log(Level.SEVERE, "Priority was not changed", ex);
            throw new BugReporterException(ex);
        }
    }

    public void reopenIssue(int issueId, String comment, String username, String password) {
        try {
            ArrayList<NameValuePair> data = new ArrayList<NameValuePair>();
            data.add(new NameValuePair("comment", comment));
            data.add(new NameValuePair("bug_status", "REOPENED"));
            runBZQuery(issueId, PROCESS_BUG_URL, data, null, login(username, password));
            BugReporter.LOG.log(Level.INFO, "Bug {0} was reopen", issueId);
        } catch (IOException e) {
            BugReporter.LOG.log(Level.SEVERE, "Reopening the bug #" + issueId + " failed", e);
            throw new BugReporterException(e);
        }
    }

    // private methods
    private static HttpClient login(String username, String passwd) throws IOException {
        try{
            if (username == null || passwd == null || username.equals(BugReporter.GUEST_USER)){
                return doLogin(AuthenticatorImpl.getDefaultAuthToken().getUserName(),
                        AuthenticatorImpl.getDefaultAuthToken().getPasswd());
            }
            return doLogin(username, passwd);
        }catch (IllegalArgumentException ie){
            // wait a moment and try again
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                org.openide.util.Exceptions.printStackTrace(ex);
            }
            return doLogin(username, passwd);
        }
    }
    
    private static HttpClient doLogin(String username, String password) throws IOException {
        HttpClient client = ReporterUtils.createHttpClient();
        PostMethod method = new PostMethod(LOGIN_URL);
        NameValuePair[] data = {
            new NameValuePair("Bugzilla_login", username),
            new NameValuePair("Bugzilla_password", password),
            new NameValuePair("GoAheadAndLogIn", "Login")
        };
        method.setRequestBody(data);

        try {
            client.executeMethod(method);
            Header header = method.getResponseHeader("Set-Cookie");
            if (header == null) {
                throw new IllegalArgumentException("Login failed, check username/password " + username + ":***");
            }
        } finally {
            method.releaseConnection();
        }
        return client;
    }

    private int createNewBug(Submit sbm, String version, HttpClient client, boolean skipCC, String extraComment, Set<String> extraCC) throws IOException {
        String messagePrefix = ReporterUtils.prepareMessagePrefix(sbm);
        ArrayList<NameValuePair> data = new ArrayList<NameValuePair>();
        data.add(new NameValuePair("product", sbm.getComponent()));
        data.add(new NameValuePair("component", sbm.getSubcomponent()));
        data.add(new NameValuePair("short_desc", messagePrefix + sbm.getSummary()));
        data.add(new NameValuePair("version", version));
        data.add(new NameValuePair("exceptionsid", Integer.toString(sbm.getId())));
        data.add(new NameValuePair("form_name", "enter_bug"));
        data.add(new NameValuePair("reporter", ReporterUtils.userNameToEMail(sbm.getReporter().getUserName())));
        data.add(new NameValuePair("rep_platform", "All"));
        data.add(new NameValuePair("cf_bug_type", "DEFECT"));
        data.add(new NameValuePair("op_sys", "All"));
        data.add(new NameValuePair("priority", "P3"));
        data.add(new NameValuePair("bug_status", "NEW"));
        data.add(new NameValuePair("target_milestone", "TBD"));
        data.add(new NameValuePair("cf_autoreporter_id", Integer.toString(sbm.getReportId())));
        String ccList = ReporterUtils.getCCUsersList(sbm, extraCC);
        if (ccList != null && !skipCC){
            data.add(new NameValuePair("cc", ccList));
        }
        if (sbm.getKeywords() != null){
            data.add(new NameValuePair("keywords", sbm.getKeywords()));
        }
        if (sbm.getStatusWhiteboard() != null){
            data.add(new NameValuePair("status_whiteboard", sbm.getStatusWhiteboard()));
        }
        ReporterUtils.setFirstComment(data, sbm, extraComment);

        PostMethod method = null;
        String methodResult;
        try {
            method = new PostMethod(POST_BUG_URL);
            NameValuePair[] values = new NameValuePair[data.size()];
            method.setRequestBody((NameValuePair[]) data.toArray(values));
            methodResult = executeMethod(client, method);
        } finally {
            if (method != null) {
                method.releaseConnection();
            }
        }

        String bugId = null;
        Matcher m = SHOW_BUG_PATTERN.matcher(methodResult);
        boolean result = m.find();
        if (result) {
            bugId = m.group(1);
        }
        if (bugId != null) {
            BugReporter.LOG.log(Level.INFO, "Bug #{0} was created", bugId);
            BugReporter.LOG.log(Level.INFO, "Bug was reported for version:", version);
            Integer bugIdNo = Integer.parseInt(bugId.trim());
            createAttachment(sbm.getAttachment(), bugIdNo, null, client);
            return bugIdNo;
        } else if (methodResult.contains("A legal Version was not set") && !UNIVERZAL_VERSION.equals(version)) {
            BugReporter.LOG.log(Level.INFO, "Version {0} was not legal", version);
            return createNewBug(sbm, UNIVERZAL_VERSION, client, skipCC, extraComment, extraCC);
        } else if (methodResult.contains("Please go back and try other names or email addresses")) {
            BugReporter.LOG.log(Level.INFO, "Some email address was not correct");
            return createNewBug(sbm, version, client, true, extraComment, extraCC);
        } else {
            String message = String.format("Bug was not created for report %1$s, and here is the reason: %2$s",
                    new Object[]{Integer.toString(sbm.getId()), methodResult});
            throw new BugReporterException(message);
        }
    }

    private void addToCCList(int bugId, String username, HttpClient client) throws IOException {
        ArrayList<NameValuePair> data = new ArrayList<NameValuePair>();
        data.add(new NameValuePair("newcc", ReporterUtils.userNameToEMail(username)));
        runBZQuery(bugId, PROCESS_BUG_URL, data, "Changes submitted", client);
        BugReporter.LOG.log(Level.INFO, "User {0} was added to bug {1}", new Object[]{username, bugId});
    }

    private String executeMethod(HttpClient client, PostMethod method) throws IOException {
        //execute the method
        client.executeMethod(method);
        //handle possible redirect
        Header locationHeader = method.getResponseHeader("location");
        if (locationHeader != null) {
            String redirectLocation = locationHeader.getValue();
            GetMethod gm = new GetMethod(redirectLocation);
            client.executeMethod(gm);
            return gm.getResponseBodyAsString(ReporterUtils.MAX_PREFIX_LEN);
        } else {
            return method.getResponseBodyAsString(ReporterUtils.MAX_PREFIX_LEN);
        }
    }

    public void resolveIssue(int bugId, String resolution, String username, String password){
        try {
            ArrayList<NameValuePair> data = new ArrayList<NameValuePair>();
            data.add(new NameValuePair("comment", "Closing this bug as " + resolution));
            data.add(new NameValuePair("bug_status", "RESOLVED"));
            data.add(new NameValuePair("resolution", resolution));
            runBZQuery(bugId, PROCESS_BUG_URL, data, null, login(username, password));
            BugReporter.LOG.log(Level.INFO, "Bug {0} was resolved", bugId);
        } catch (IOException e) {
            BugReporter.LOG.log(Level.SEVERE, "Resolving the bug #" + bugId + " failed", e);
            throw new BugReporterException(e);
        }
    }

    private String runBZQuery(int bugId, String url, List<NameValuePair> data, String expectedResult, HttpClient client) throws IOException {
        String[] params = ReporterUtils.getPageTokens(SHOW_BUG_URL, bugId, client);
        data.add(new NameValuePair("id", String.valueOf(bugId)));
        data.add(new NameValuePair("longdesclength", params[0]));
        data.add(new NameValuePair("token", params[1]));
        String response = null;
        PostMethod method = new PostMethod(url);
        NameValuePair[] values = new NameValuePair[data.size()];
        method.setRequestBody((NameValuePair[]) data.toArray(values));
        try {
            response = executeMethod(client, method);
            if (expectedResult != null && !response.contains(expectedResult)) {
                String message = String.format("BZQuery to %1$s failed because of: %2$s",
                        new Object[]{url, response});
                throw new BugReporterException(message);
            }
        } finally {
            method.releaseConnection();
        }
        return response;
    }

    private void createAttachment(Submit.Attachment attachment, Integer bugId, String additionalComment, HttpClient client) throws IOException {
        if (additionalComment == null){
            additionalComment = "";
        }
        String[] params = ReporterUtils.getPageTokens(POST_ATTACHMENT +"?action=enter&bugid=", bugId, client);
        String fileName = String.format(attachment.getFileNameFormat(), bugId);
        Part[] parts = {
            new StringPart("bugid", bugId.toString()),
            new StringPart("action", "insert"),
            new StringPart("token", params[1]),
            new StringPart("description", attachment.getDescription()),
            new StringPart("contenttypemethod", "manual"),
            new StringPart("contenttypeentry", attachment.getType()),
            new StringPart("comment", additionalComment),
            new FilePart("data", new ByteArrayPartSource(fileName, attachment.getData()))
        };
        PostMethod method = new PostMethod(POST_ATTACHMENT);
        method.setRequestEntity(new MultipartRequestEntity(parts, method.getParams()));
        try {
            client.executeMethod(method);
        } finally {
            method.releaseConnection();
        }
        BugReporter.LOG.log(Level.INFO, "Attachment for bug #{0} was created", bugId);
    }
}
