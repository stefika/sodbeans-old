/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 2010 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 *
 * Contributor(s):
 *
 * Portions Copyrighted 2010 Sun Microsystems, Inc.
 */
package org.netbeans.server.uihandler.bugs;

import java.io.IOException;
import java.io.InputStream;
import java.security.GeneralSecurityException;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.RequestEntity;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.netbeans.lib.uihandler.PasswdEncryption;
import org.netbeans.server.uihandler.api.Authenticator;
import org.openide.util.lookup.ServiceProvider;

/**
 *
 * @author Jindrich Sedek
 */

@ServiceProvider(service = Authenticator.class)
public class AuthenticatorImpl implements Authenticator {

    public static String AUTHENTICATE_URL = "https://netbeans.org/api/login/authenticate.json";
    private static final String CONTENT_TYPE = "application/x-www-form-urlencoded";
    private static final String EXCEPTIONS_REPORTER = "exceptions_reporter";
    private static final Pattern USER_NAME_PATTERN = Pattern.compile("\"username\":\"([^\"]*)\"");
    private static final int HTTP_200 = 200;
    private static PrivateKey privateKey;
    private static Logger LOGGER = Logger.getLogger(AuthenticatorImpl.class.getName());
    private static Logger AUTHENTICATION = Logger.getLogger("org.netbeans.server.uihandler.authentication");

    public AuthToken authenticate(String username, String password) {
        if (username == null || password == null || BugzillaReporter.GUEST_USER.equals(username) || username.isEmpty()) {
            return null;
        }
        String decrypted = decryptPasswd(password);
        AuthToken token = null;
        if (decrypted != null){
            token = check(username, decrypted);
            if (token != null){
                return token;
            }
        }
        //no encrypted (NB 6.0, 6.0.1)
        return check(username, password);
    }

    public void loadPrivateKey(InputStream inputStream) {
        try {
            byte[] encodedKey = new byte[inputStream.available()];
            inputStream.read(encodedKey);
            PKCS8EncodedKeySpec privateKeySpec = new PKCS8EncodedKeySpec(encodedKey);
            KeyFactory kf = KeyFactory.getInstance("RSA");// NOI18N
            privateKey = kf.generatePrivate(privateKeySpec);
        } catch (IOException exc) {
            LOGGER.log(Level.SEVERE, "GETTING PRIVATE KEY", exc);// NOI18N
        } catch (GeneralSecurityException exc) {
            LOGGER.log(Level.SEVERE, "GETTING PRIVATE KEY", exc);// NOI18N
        }
    }

    private AuthToken check(String username, String password){
        HttpClient client = new HttpClient();
        PostMethod method = new PostMethod(AUTHENTICATE_URL);
        RequestEntity entity =
                new StringRequestEntity("username=" + username + "&password=" + password);
        method.setParameter("Content-Type", CONTENT_TYPE);
        method.setRequestEntity(entity);
        int response = -1;
        String body = null;
        try {
            response = client.executeMethod(method);
            body = method.getResponseBodyAsString(1024);
            if (username.contains("@")){
                // user is logging in with his email
                Matcher matcher = USER_NAME_PATTERN.matcher(body);
                if (matcher.find()){
                    username = matcher.group(1);
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(AuthenticatorImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally{
            method.releaseConnection();
        }
        if (HTTP_200 == response){
            AUTHENTICATION.log(Level.INFO, "authentication passed for {0}", username);
            return new AuthToken(username, password);
        }else{
            Object[] params = {"response: " + response, body};
            AUTHENTICATION.log(Level.WARNING, "authentication failed for " + username, params);
            return null;
        }
    }

    private static String doDecrypt(String encrypted) {
        try {
            return PasswdEncryption.decrypt(encrypted, privateKey);
        } catch (IOException exception) {
            LOGGER.log(Level.SEVERE, "DECRIPTING PASSWORD", exception);
        } catch (GeneralSecurityException exception) {
            LOGGER.log(Level.SEVERE, "DECRIPTING PASSWORD", exception);
        }
        return "";
    }


    private static String decryptPasswd(String passwd){
        if (passwd.length() < 20) {
            return null;
        }
        return doDecrypt(passwd);
    }
    private static String getDefaultReporterPassword() {
        return doDecrypt("88:2:-36:-126:-95:-124:-85:52:96:75:-127:25:-2:123:"
                + "-73:-4:-37:-74:-44:-7:37:29:-12:-1:103:93:-127:-40:-19:-49:"
                + "-96:96:13:-109:-100:-95:72:39:-35:115:-20:-39:-10:-123:47:"
                + "-110:88:-39:12:-11:89:-6:43:63:-51:99:-11:-74:-49:103:3:16:72:"
                + "-91:-98:74:-40:-51:-69:-81:-72:-128:118:8:-11:-46:95:65:-119:"
                + "-31:60:81:64:121:98:91:12:-89:109:58:-31:-26:56:105:-63:-119:"
                + "113:68:47:74:-12:-30:15:5:3:-14:51:101:-23:86:-42:-20:1:98:"
                + "101:-87:-42:-48:-2:9:-65:-30:20:-101:110:-22:-53:-37:");
    }

    public String getDefaultReporterUsername() {
        return EXCEPTIONS_REPORTER;
    }

    private static AuthToken defaultAuthToken = null;
    public static AuthToken getDefaultAuthToken() {
        if (defaultAuthToken == null){
            defaultAuthToken = new AuthToken(EXCEPTIONS_REPORTER, getDefaultReporterPassword());
        }
        return defaultAuthToken;
    }

    public static void setDefaultAuthToken(AuthToken token){
        defaultAuthToken = token;
    }

}
