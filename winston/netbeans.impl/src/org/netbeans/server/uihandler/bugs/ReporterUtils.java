/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 2009 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 *
 * Contributor(s):
 *
 * Portions Copyrighted 2009 Sun Microsystems, Inc.
 */
package org.netbeans.server.uihandler.bugs;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.cookie.CookiePolicy;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.params.HttpConnectionManagerParams;
import org.apache.commons.httpclient.util.EncodingUtil;
import org.netbeans.server.uihandler.api.Submit;
import org.openide.util.NbBundle;

/**
 *
 * @author Jindrich Sedek
 */
public class ReporterUtils {

    public static final String UNIVERZAL_VERSION = "current";
    private static final Pattern LONGDESCLENGTH_PATTERN = Pattern.compile("<input type=\"hidden\" name=\"longdesclength\" value=\"([0-9]*)\">");
    private static final Pattern TOKEN_PATTERN = Pattern.compile("<input type=\"hidden\" name=\"token\" value=\"([-0-9a-zA-Z]*)\">");
    private static final String DOMAIN_NAME = NbBundle.getMessage(ReporterUtils.class, "DOMAIN_NAME");
    private static final String URL_PREFIX = "http://" + DOMAIN_NAME;

    public static void setFirstComment(List<NameValuePair> data, Submit submit, String extraMessage) {
        StringBuffer comments = new StringBuffer();
        Collection<Submit> duplicates = new HashSet<Submit>(submit.getDuplicates());
        duplicates.add(submit);
        for (Submit dupl : duplicates) {
            String comment = dupl.getCommentString();
            if (comment != null) {
                comments.append(dupl.getReporter().getUserName()).append(": ");
                comments.append(comment);
                comments.append("\n\n");
            }
        }
        StringBuffer sb = new StringBuffer();
        if (extraMessage != null) {
            sb.append(extraMessage).append("\n\n");
        }
        sb.append("Build: ").append(submit.getProductVersion());
        sb.append("\nVM: ").append(submit.getVM()).append("\nOS: ").append(submit.getOS());
        if (comments.length() > 0) {
            sb.append("\n\nUser Comments:\n").append(comments).append("\n");
        }
        sb.append(submit.getAdditionalComment());
        data.add(new NameValuePair("comment", sb.toString()));
    }

    /**
     * @param bugURL 
     * @param issue issue to load tokens
     * @return [longdesclength, token]
     * @throws IOException
     */
    public static String[] getPageTokens(String bugURL, int issue) throws IOException {
        return getPageTokens(bugURL, issue, createHttpClient());
    }

    public static String[] getPageTokens(String bugURL, int issueId, HttpClient client) throws IOException {
        String form = null;
        InputStream is = null;
        GetMethod formPage = new GetMethod(bugURL + issueId);
        try {
            client.executeMethod(formPage);
            is = formPage.getResponseBodyAsStream();
            form = readPagePrefix(is, formPage.getResponseCharSet());
        } finally {
            if (is != null){
                is.close();
            }
            formPage.releaseConnection();
        }

        Matcher m = LONGDESCLENGTH_PATTERN.matcher(form);
        String longdesclength = "0";
        String token = "a0a0a0";
        if (m.find()){
            longdesclength = m.group(1);
        }
        m = TOKEN_PATTERN.matcher(form);
        if (m.find()){
            token = m.group(1);
        }

        return new String[]{longdesclength, token};
    }

    public static String prepareMessagePrefix(Submit sbm) {
        String prefix = sbm.getReporter().getDirectUserPrefix();
        if (prefix != null && prefix.length() > 0) {
            return prefix + " ";
        } else {
            for (Submit submit : sbm.getDuplicates()) {
                prefix = submit.getReporter().getDirectUserPrefix();
                if (prefix != null && prefix.length() > 0) {
                    return prefix + " ";
                }
            }
        }
        return "";
    }

    public static HttpClient createHttpClient(){
        // Create a trust manager that does not validate certificate chains
        TrustManager tm = new X509TrustManager() {

            public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                return null;
            }

            public void checkClientTrusted(X509Certificate[] certs, String authType) {
            }

            public void checkServerTrusted(X509Certificate[] certs, String authType) {
            }
        };
        // Install the all-trusting trust manager
        try {
            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, new TrustManager[]{tm}, new SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        } catch (KeyManagementException ex) {
            org.openide.util.Exceptions.printStackTrace(ex);
        } catch (NoSuchAlgorithmException ex) {
            org.openide.util.Exceptions.printStackTrace(ex);
        }

        HttpClient client = new HttpClient();
        client.getParams().setParameter("http.protocol.cookie-policy", CookiePolicy.BROWSER_COMPATIBILITY);
        HttpConnectionManagerParams params = client.getHttpConnectionManager().getParams();
        params.setConnectionTimeout(20000);
        params.setSoTimeout(40000);
        return client;
    }

    public static String getCCUsersList(Submit sbm, Set<String> additionalCC) {
        Collection<Submit> duplicates = sbm.getDuplicates();
        Set<String> users = new HashSet<String>();
        for (Submit submit : duplicates) {
            users.add(userNameToEMail(submit.getReporter().getUserName()));
        }
        users.remove(userNameToEMail(sbm.getReporter().getUserName()));
        if (additionalCC != null){
            for (String aCC : additionalCC) {
                users.add(userNameToEMail(aCC));
            }
        }
        users.remove(userNameToEMail(BugzillaReporter.GUEST_USER));
        if (users.isEmpty()){
            return null;
        }
        StringBuilder result = new StringBuilder();
        Iterator<String> it = users.iterator();
        result.append(it.next());
        while (it.hasNext()) {
            result.append(",");
            result.append(it.next());
        }
        return result.toString();
    }
    public static String userNameToEMail(String username) {
        if (!username.contains(DOMAIN_NAME)) {
            username = username + "@" + DOMAIN_NAME;
        }
        return username;
    }

    public static String getURLPrefix(){
        return URL_PREFIX;
    }

    public static final int MAX_PREFIX_LEN = 1024 * 1024; //1MB
    public static final int BUFFER_SIZE = 32 * 1024; //4KB

    public static String readPagePrefix(InputStream instream, String charset) throws IOException {
        byte[] body = new byte[0];
        if (instream != null) {
            ByteArrayOutputStream rawdata = new ByteArrayOutputStream(BUFFER_SIZE);
            byte[] buffer = new byte[BUFFER_SIZE];
            int pos = 0;
            int len;
            do {
                len = instream.read(buffer, 0, Math.min(buffer.length, MAX_PREFIX_LEN - pos));
                if (len == -1) {
                    break;
                }
                rawdata.write(buffer, 0, len);
                pos += len;
            } while (pos < MAX_PREFIX_LEN);
            body = rawdata.toByteArray();
        }
        return EncodingUtil.getString(body, charset);
    }

}
