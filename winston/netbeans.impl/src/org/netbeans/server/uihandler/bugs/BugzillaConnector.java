/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */
package org.netbeans.server.uihandler.bugs;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.core.util.MultivaluedMapImpl;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import javax.ws.rs.core.MultivaluedMap;
import org.netbeans.server.services.beans.CommunicationHelper;
import org.netbeans.server.services.beans.Components;
import org.netbeans.server.services.beans.Mappings;
import org.netbeans.server.uihandler.api.Issue;
import org.openide.util.NbBundle;

/**
 *
 * @author Jindrich Sedek
 */
public class BugzillaConnector {

    private final Client client;
    public static final String SERVER_URL = NbBundle.getMessage(BugzillaConnector.class, "BZ_SERVICE_URL");
    private static final String RESOURCE_URL = "resources/";
    private static final Integer BUGZILLA_TIMEOUT = 120 * 1000; // 2min
    protected BugzillaConnector() {
        client = Client.create();
        client.setConnectTimeout(BUGZILLA_TIMEOUT);
        client.setReadTimeout(BUGZILLA_TIMEOUT);
    }

    Issue getIssue(int id){
        String url = SERVER_URL + RESOURCE_URL + "bug/" + id;
        WebResource webResource = client.resource(url);
        Issue result = webResource.get(Issue.class);
        return result;
    }

    Collection<Integer> filterOpen(Collection<Integer> ids) {
        String url = SERVER_URL + RESOURCE_URL + "filter";
        WebResource webResource = client.resource(url);
        MultivaluedMap queryParams = new MultivaluedMapImpl();
        queryParams.add("ids", CommunicationHelper.pack(ids));
        String result = webResource.queryParams(queryParams).get(String.class);
        
        Integer[] filteredArray = CommunicationHelper.unpack(result);
        return  new HashSet(Arrays.asList(filteredArray));
    }

    public Components getComponents(){
        String url = SERVER_URL + RESOURCE_URL + "components";
        WebResource webResource = client.resource(url);
        Components result = webResource.get(Components.class);
        return result;
    }

    public Mappings getMappings(){
        String url = SERVER_URL + RESOURCE_URL + "mappings";
        WebResource webResource = client.resource(url);
        Mappings result = webResource.get(Mappings.class);
        return result;
    }

    private static BugzillaConnector testInstance = null;
    public static BugzillaConnector getInstance(){
        if (testInstance != null){
            return testInstance;
        }
        return new BugzillaConnector();
    }

    public static void setTestInstance(BugzillaConnector bz){
        testInstance = bz;
    }

    public Stats getFixedCount(final Collection<Integer> list) {
        int fixed = 0;
        int resolved = 0;
        Iterator<Integer> it = list.iterator();
        while (it.hasNext()) {
            int next = it.next();
            Issue iss = getIssue(next);
            if ("FIXED".equals(iss.getResolution())) {
                fixed++;
            }
            String status = iss.getIssueStatus();
            if ("RESOLVED".equals(status) || "VERIFIED".equals(status) || "CLOSED".equals(status)) {
                resolved++;
            }
        }
        return new Stats(fixed, resolved);
    }

    public static final class Stats {

        private int fixed;
        private int resolved;

        public Stats(int fixed, int resolved) {
            this.fixed = fixed;
            this.resolved = resolved;
        }

        public int getFixed() {
            return fixed;
        }

        public void setFixed(int fixed) {
            this.fixed = fixed;
        }

        public int getResolved() {
            return resolved;
        }

        public void setResolved(int resolved) {
            this.resolved = resolved;
        }
    }
}
