/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */
package org.netbeans.modules.exceptions.realm;
/*
 * IZRealm.java
 *
 * Created on May 15, 2007, 3:26:49 PM
 *
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.security.Principal;
import java.util.Collections;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.catalina.Container;
import org.apache.catalina.realm.GenericPrincipal;
import org.apache.catalina.realm.RealmBase;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.RequestEntity;
import org.apache.commons.httpclient.methods.StringRequestEntity;

/**
 *
 * @author Jan Horvath
 */
public class IssuezillaRealm extends RealmBase {
    private Container container;
    private static boolean testMode = false;
    private static final String AUTHENTICATE_URL = "https://netbeans.org/api/login/authenticate.json";
    private static final String CONTENT_TYPE = "application/x-www-form-urlencoded";
    private static final int HTTP_200 = 200;
    @Override
    public Principal authenticate(String username, String credentials) {
        Principal principal = null;
        if (checkIssuezillaLogin(username, credentials)) {
            principal = new GenericPrincipal(this, username, credentials, Collections.singletonList("issuezilla"));
        }
        return principal;
    }

    protected String getName() {
        return "IZRealm";
    }

    protected String getPassword(String arg0) {
        return "";
    }

    protected Principal getPrincipal(String arg0) {
        return null;
    }

     /**
     * @param args the command line arguments
     */
    private static boolean auth(String username, String password) {
        HttpClient client = new HttpClient();
        PostMethod method = new PostMethod(AUTHENTICATE_URL);
        RequestEntity entity =
                new StringRequestEntity("username=" + username + "&password=" + password);
        method.setParameter("Content-Type", CONTENT_TYPE);
        method.setRequestEntity(entity);
        method.setRequestEntity(entity);
        int response = -1;
        try {
            response = client.executeMethod(method);
        } catch (IOException ex) {
            Logger.getLogger(IssuezillaRealm.class.getName()).log(Level.SEVERE, null, ex);
        }
        return HTTP_200 == response;
    }

    public static boolean checkIssuezillaLogin(String username, String password) {
        boolean response;
        if (testMode) {
            if (username.equals("exceptions")&&(password.equals("petrzajac"))) response = true;
            else if (username.equals("tester")&&(password.equals("tester"))) response = true;
            else response = false;
        } else {
            response = auth(username, password);
        }
        return response;
    }
    
    public static void setTestMode(boolean mode) {
        testMode = mode;
    }
}
