/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.netbeans.web.sso;

import java.io.IOException;
import java.util.Collections;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import org.apache.catalina.connector.Request;
import org.apache.catalina.connector.Response;
import org.apache.catalina.valves.ValveBase;

/**
 *
 * @author honza
 */
public class SSOValve extends ValveBase {

    public static final String DOMAIN = "netbeans.org";
    public static final String INFO = DOMAIN + " SSO Valve";
    public static final String COOKIE_NAME = "SSO";

    @Override
    public String getInfo() {
        return INFO;
    }

    /**
     * If the 'SSO' cookie is present in the request check if it is valid
     * using the netbeans.org api call
     *
     */
    public void invoke(Request request, Response response)
            throws IOException, ServletException {

        String ssoCookie = null;
        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            for (int i = 0; i < cookies.length; i++) {
                Cookie cookie = cookies[i];
                if (COOKIE_NAME.equals(cookie.getName())) {
                    ssoCookie = cookie.getValue();
                }
            }
        }

        if (ssoCookie != null) {
            String username = SSOValidator.instance().validateSSO(ssoCookie);
            if (username != null) {
                request.setUserPrincipal(new SSOPrincipal(request.getContext().getRealm(), username, Collections.singletonList("issuezilla")));
            }
        }

        getNext().invoke(request, response);
    }
}
