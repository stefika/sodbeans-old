/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.netbeans.web.sso;

import java.io.IOException;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.GetMethod;

/**
 *
 * @author honza
 */
public class SSOValidator {

    private static final String SSO_API_URL = "https://" + SSOValve.DOMAIN + "/api/login/validate/";
    private static final int OK = 200;
    private static SSOValidator instance;
    private static final String DELIM = "%3A";

    private SSOCache cache;
    

    private SSOValidator() {
        cache = new SSOCache();
    }

    public static synchronized SSOValidator instance() {
        if (instance == null) {
            instance = new SSOValidator();
        }
        return instance;
    }

    public String validateSSO(String ssoCookie) throws IOException {
        String response = null;
        boolean valid = cache.get(ssoCookie);
        if (! valid) {
            valid = callValidateWS(ssoCookie);
            if (valid) {
                cache.put(ssoCookie);
            }
        }
        if (valid) {
            response = getUsernameFromCookie(ssoCookie);
        }
        return response;
    }

    private boolean callValidateWS(String ssoCookie) throws IOException {
        GetMethod method = new GetMethod(SSO_API_URL + ssoCookie);
        int r = new HttpClient().executeMethod(method);
        method.releaseConnection();
        return OK == r;
    }
    private String getUsernameFromCookie(String ssoCookie) {
        String[] parts = ssoCookie.split(DELIM);
        return parts[0];
    }

    
}
