/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.netbeans.web.sso;

import java.util.List;
import org.apache.catalina.Realm;
import org.apache.catalina.realm.GenericPrincipal;

/**
 *
 * @author honza
 */
public class SSOPrincipal extends GenericPrincipal {

    public SSOPrincipal(Realm realm, String name, List roles) {
        super(realm, name, "", roles);
    }
}
