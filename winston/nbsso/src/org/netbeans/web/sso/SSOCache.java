    /*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.netbeans.web.sso;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author honza
 */
public class SSOCache {

    /** Expiration time in ms */
    private static final int TTL = 60000;
    /** Cookies w/ timestamps are stored here */
    private final Map<String, Long> cache = Collections.synchronizedMap(new HashMap<String, Long>());
    /** This executor is used for removing expired entries */
    private final ExecutorService threads;

    public SSOCache() {
        
        this.threads = Executors.newFixedThreadPool(128);

        Executors.newScheduledThreadPool(1).scheduleWithFixedDelay(createCleaner(), 0, 30, TimeUnit.SECONDS);
    }

    /**
     * Returns true if the cookie is still valid
     *
     * @param cookie
     *
     * @return cookie is valid
     *
     */
    public boolean get(String cookie) {
        Long ts = cache.get(cookie);
        boolean result = false;
        if (ts != null) {
            result = ts > System.currentTimeMillis();
        } 
        return result;
    }

    /**
     * Puts the cookie into the cache
     *
     * @param ssoCookie
     */
    public void put(String ssoCookie) {
        cache.put(ssoCookie, System.currentTimeMillis() + TTL);
    }

    private Runnable createRemoveRunnable(final String cookie) {
        return new Runnable() {

            @Override
            public void run() {
                cache.remove(cookie);
            }
        };
    }

    private Runnable createCleaner() {
        return new Runnable() {

            @Override
            public void run() {
                synchronized (cache) {
                    for (final Map.Entry<String, Long> entry : cache.entrySet()) {
                        if (entry.getValue() >= System.currentTimeMillis()) {
                            threads.execute(createRemoveRunnable(entry.getKey()));
                        }
                    }
                }
            }
        };
    }
}
