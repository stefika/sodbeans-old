/*
 Copyright (c) 2006-2008, Guillaume Pothier
 All rights reserved.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met: 

 1. Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer. 
 2. Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution. 

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 The views and conclusions contained in the software and documentation are those
 of the authors and should not be interpreted as representing official policies, 
 either expressed or implied, of the FreeBSD Project.
 */
package tod.agent;

/**
 * Represents the value of some object. It is always possible to deserialize an
 * {@link ObjectValue} in the database's or client's JVM, whereas it would not
 * necessarily be possible to deserialize the actual object (for classes that
 * are not part of the JDK).
 *
 * @author gpothier
 */
public class ObjectValue {
    // These constants are used for encoding/decoding

    public static final byte TYPE_STRING = 10;
    public static final byte TYPE_INT = 11;
    public static final byte TYPE_LONG = 12;
    public static final byte TYPE_BYTE = 13;
    public static final byte TYPE_CHAR = 14;
    public static final byte TYPE_SHORT = 15;
    public static final byte TYPE_FLOAT = 16;
    public static final byte TYPE_DOUBLE = 17;
    public static final byte TYPE_BOOLEAN = 18;
    public static final byte TYPE_OBJECTID = 19;
    public static final byte TYPE_VALUE = 20;
    public static final byte TYPE_REF = 21;
    public static final byte TYPE_NULL = 22;
    private String itsClassName;
    private FieldValue[] itsFields;
    private boolean itsThrowable;

    public ObjectValue(String aClassName, boolean aThrowable) {
        itsClassName = aClassName;
        itsThrowable = aThrowable;
    }

    public FieldValue[] getFields() {
        return itsFields;
    }

    public void setFields(FieldValue[] aFields) {
        itsFields = aFields;
    }

    public String getClassName() {
        return itsClassName;
    }

    /**
     * Whether the represented object is a {@link Throwable}.
     */
    public boolean isThrowable() {
        return itsThrowable;
    }

    /**
     * Returns the value for the (first encountered match of the) given field.
     */
    public Object getFieldValue(String aFieldName) {
        for (FieldValue theValue : itsFields) {
            if (theValue.fieldName.equals(aFieldName)) {
                return theValue.value;
            }
        }
        return null;
    }

    /**
     * Returns a user-readable representation of the object.
     */
    public String asString() {
        return asString(1);
    }

    public String asString(int aLevel) {
        if (aLevel == 0) {
            return "";
        }
        StringBuilder theBuilder = new StringBuilder();
        for (FieldValue theFieldValue : itsFields) {
            theBuilder.append(theFieldValue.asString(aLevel - 1));
            theBuilder.append(' ');
        }
        return theBuilder.toString();
    }

    @Override
    public String toString() {
        return "ObjectValue [" + asString() + "]";
    }

    public static class FieldValue {

        public final String fieldName;
        public Object value;

        public FieldValue(String aFieldName) {
            this(aFieldName, null);
        }

        public FieldValue(String aFieldName, Object aValue) {
            fieldName = aFieldName;
            value = aValue;
        }

        public void setValue(Object aValue) {
            value = aValue;
        }

        public String asString(int aLevel) {
            String theValueString;
            if (value instanceof ObjectValue) {
                ObjectValue theObjectValue = (ObjectValue) value;
                theValueString = theObjectValue.asString(aLevel);
            } else {
                theValueString = "" + value;
            }

            return fieldName + "='" + theValueString + "'";
        }
    }
}
