/*
 Copyright (c) 2006-2008, Guillaume Pothier
 All rights reserved.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met: 

 1. Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer. 
 2. Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution. 

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 The views and conclusions contained in the software and documentation are those
 of the authors and should not be interpreted as representing official policies, 
 either expressed or implied, of the FreeBSD Project.
 */
package tod.agent;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.tod.io._IO;

/**
 * This class groups several flags that are used to disable certain features for
 * testing purposes.
 *
 * @author gpothier
 */
public class AgentDebugFlags {

    /**
     * If true, the {@link EventInterpreter} prints all the events it receives
     */
    public static final boolean COLLECTOR_LOG = false;
    /**
     * Stream to which the {@link EventInterpreter} sends debug info. Default is
     * System.out
     */
    public static final PrintStream EVENT_INTERPRETER_PRINT_STREAM =
            System.out;
//		createStream("eventInterpreter-" + AgentConfig.getHostName()+".log");
    /**
     * Causes the socket collector to not send events
     */
    public static final boolean DISABLE_EVENT_SEND = false;
    /**
     * Causes the high level collectors to ignore all events
     */
    public static final boolean COLLECTOR_IGNORE_ALL = false;
    /**
     * Enables logging of long packets processing.
     */
    public static final boolean TRANSPORT_LONGPACKETS_LOG = false;

    private static PrintStream createStream(String aName) {
        try {
            File theFile = new File(aName);
            theFile.delete();
            File theParentFile = theFile.getParentFile();
            if (theParentFile != null) {
                theParentFile.mkdirs();
            }
//			//_IO.out(theFile.getAbsolutePath());
            return new PrintStream(new FileOutputStream(theFile));
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    static {
        if (DISABLE_EVENT_SEND == true) {
            _IO.err("******* Warning: DISABLE_EVENT_SEND (AgentDebugFlags)");
        }
        if (COLLECTOR_IGNORE_ALL == true) {
            _IO.err("******* Warning: COLLECTOR_IGNORE_ALL (AgentDebugFlags)");
        }
        if (TRANSPORT_LONGPACKETS_LOG == true) {
            _IO.err("******* Warning: TRANSPORT_LONGPACKETS_LOG (AgentDebugFlags)");
        }
    }
}
