/*
 Copyright (c) 2006-2008, Guillaume Pothier
 All rights reserved.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met: 

 1. Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer. 
 2. Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution. 

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 The views and conclusions contained in the software and documentation are those
 of the authors and should not be interpreted as representing official policies, 
 either expressed or implied, of the FreeBSD Project.
 */
package tod.agent;

import java.tod.io._IO;

/**
 * Configuration of the agent in the target VM.
 *
 * @author gpothier
 */
public class AgentConfig {

    /**
     * Signature for connections from the native side.
     */
    public static final int CNX_NATIVE = 0x3a71be0;
    /**
     * Signature for connections from the java side.
     */
    public static final int CNX_JAVA = 0xcafe0;
    public static final String PARAM_COLLECTOR_HOST = "collector-host";
    public static final String PARAM_COLLECTOR_PORT = "collector-port";
    public static final String PARAM_CAPTURE_AT_START = "capture-at-start";
    /**
     * This parameter defines the name of the host the agent runs on.
     */
    public static final String PARAM_CLIENT_NAME = "client-name";
    /**
     * Number of bits used to represent the host of an event.
     */
    public static final int HOST_BITS = AgentUtils.readInt("host-bits", 0);
    public static final long HOST_MASK = BitUtilsLite.pow2(HOST_BITS) - 1;
    /**
     * Size of {@link SocketCollector} buffer.
     */
    public static final int COLLECTOR_BUFFER_SIZE = 4096; //16384;
}
