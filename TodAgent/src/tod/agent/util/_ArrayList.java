/*
 Copyright (c) 2006-2008, Guillaume Pothier
 All rights reserved.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met: 

 1. Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer. 
 2. Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution. 

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 The views and conclusions contained in the software and documentation are those
 of the authors and should not be interpreted as representing official policies, 
 either expressed or implied, of the FreeBSD Project.
 */
package tod.agent.util;

/**
 * A simple version of {@link ArrayList}. We don't use {@link ArrayList} because
 * it might be instrumented
 *
 * @author gpothier
 */
public class _ArrayList<T> {

    private T[] itsData;
    private int itsSize;

    public _ArrayList() {
        this(16);
    }

    public _ArrayList(int aInitialSize) {
        itsData = (T[]) new Object[aInitialSize];
    }

    public T get(int aIndex) {
        if (aIndex >= itsSize || aIndex < 0) {
            throw new IndexOutOfBoundsException("" + aIndex + "/" + itsSize);
        }
        return itsData[aIndex];
    }

    public int size() {
        return itsSize;
    }

    public boolean isEmpty() {
        return itsSize == 0;
    }

    public void add(int aIndex, T aValue) {
        if (aIndex > itsSize || aIndex < 0) {
            throw new IndexOutOfBoundsException("" + aIndex + "/" + itsSize);
        }
        ensureSize(aIndex + 1);
        itsData[aIndex] = aValue;
        itsSize = Math.max(itsSize, aIndex + 1);
    }

    public void add(T aValue) {
        add(size(), aValue);
    }

    public void clear() {
        itsSize = 0;
    }

    private void ensureSize(int aSize) {
        if (itsData.length >= aSize) {
            return;
        }

        int theNewSize = Math.max(aSize, itsData.length * 2);
        T[] theNewData = (T[]) new Object[theNewSize];
        System.arraycopy(itsData, 0, theNewData, 0, itsData.length);
        itsData = theNewData;
    }

    public T[] toArray() {
        T[] theResult = (T[]) new Object[size()];
        System.arraycopy(itsData, 0, theResult, 0, size());
        return theResult;
    }

    public T[] toArray(T[] aDest) {
        System.arraycopy(itsData, 0, aDest, 0, size());
        return aDest;
    }
}
