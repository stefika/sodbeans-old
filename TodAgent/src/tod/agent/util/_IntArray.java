/*
 Copyright (c) 2006-2008, Guillaume Pothier
 All rights reserved.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met: 

 1. Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer. 
 2. Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution. 

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 The views and conclusions contained in the software and documentation are those
 of the authors and should not be interpreted as representing official policies, 
 either expressed or implied, of the FreeBSD Project.
 */
package tod.agent.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Copied from zz.utils
 *
 * @author gpothier
 */
public class _IntArray {

    private int[] itsData;
    private int itsSize;

    public _IntArray() {
        this(16);
    }

    public _IntArray(int aInitialSize) {
        itsData = new int[aInitialSize];
    }

    public int get(int aIndex) {
        return aIndex < itsSize ? itsData[aIndex] : 0;
    }

    public int size() {
        return itsSize;
    }

    public boolean isEmpty() {
        return itsSize == 0;
    }

    protected void setSize(int aSize) {
        itsSize = aSize;
    }

    public void set(int aIndex, int aValue) {
        ensureSize(aIndex + 1);
        itsData[aIndex] = aValue;
        itsSize = Math.max(itsSize, aIndex + 1);
    }

    public void clear() {
        itsSize = 0;
    }

    private void ensureSize(int aSize) {
        if (itsData.length >= aSize) {
            return;
        }

        int theNewSize = Math.max(aSize, itsData.length * 2);
        int[] theNewData = new int[theNewSize];
        System.arraycopy(itsData, 0, theNewData, 0, itsData.length);
        itsData = theNewData;
    }

    public int[] toArray() {
        int[] theResult = new int[size()];
        System.arraycopy(itsData, 0, theResult, 0, size());
        return theResult;
    }

    /**
     * Transforms a collection of {@link Integer}s to an array of native ints.
     */
    public static int[] toIntArray(Collection<Integer> aCollection) {
        int[] theResult = new int[aCollection.size()];
        int i = 0;
        for (Integer theInt : aCollection) {
            theResult[i++] = theInt;
        }
        return theResult;
    }

    public static List<Integer> toList(int[] aArray) {
        if (aArray == null) {
            return null;
        }
        List<Integer> theList = new ArrayList<Integer>();
        for (int i : aArray) {
            theList.add(i);
        }
        return theList;
    }
}
