/*
 Copyright (c) 2006-2008, Guillaume Pothier
 All rights reserved.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met: 

 1. Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer. 
 2. Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution. 

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 The views and conclusions contained in the software and documentation are those
 of the authors and should not be interpreted as representing official policies, 
 either expressed or implied, of the FreeBSD Project.
 */
package java.tod;


import tod.agent.AgentConfig;
import tod.agent.AgentUtils;

/**
 * Entry point for debugged applications that need to control the activity of
 * TOD.
 *
 * @author gpothier
 */
public class TOD {

    static {
        int a = 5;
        int b = a + 5;
        double c = b;
        //if (AgentReady.isNativeAgentLoaded()) //_IO.out("[TOD] Native agent detected.");
        //else //_IO.out("[TOD] Native agent not detected.");
    }
    /**
     * If > 0, trace capture is activated
     */
    private static int CAPTURE_ENABLED = 0;

    public static void loadInitialCaptureState() {
        boolean theEnabled = AgentUtils.readBoolean(AgentConfig.PARAM_CAPTURE_AT_START, true);
        //_IO.out("[TOD] Capture enabled at start: "+theEnabled);
        CAPTURE_ENABLED = theEnabled ? 1 : 0;
        AgentReady.CAPTURE_ENABLED = CAPTURE_ENABLED > 0;
    }

    /**
     * Clears all previously recorded events.
     */
    public static void clearDatabase() {
        if (AgentReady.isNativeAgentLoaded()) {
            //_IO.out("[TOD] Sending clearDatabase request...");
            EventCollector.INSTANCE.clear();
            //_IO.out("[TOD] clearDatabase request done.");
        } else {
            //_IO.out("[TOD] Ignoring clearDatabase request: native agent not detected.");
        }
    }

    /**
     * Flushes buffered events.
     */
    public static void flushEvents() {
        if (AgentReady.isNativeAgentLoaded()) {
            //_IO.out("[TOD] Sending flushEvents request...");
            EventCollector.INSTANCE.flush();
            //_IO.out("[TOD] flushEvents request done.");
        } else {
            //_IO.out("[TOD] Ignoring flushEvents request: native agent not detected.");
        }
    }

    public static void enableCapture() {
        CAPTURE_ENABLED++;
        AgentReady.CAPTURE_ENABLED = CAPTURE_ENABLED > 0;
    }

    public static void disableCapture() {
        CAPTURE_ENABLED--;
        AgentReady.CAPTURE_ENABLED = CAPTURE_ENABLED > 0;
    }

    static int captureEnabled() {
        return CAPTURE_ENABLED;
    }
}
