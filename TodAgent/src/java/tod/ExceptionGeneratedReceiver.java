/*
 Copyright (c) 2006-2008, Guillaume Pothier
 All rights reserved.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met: 

 1. Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer. 
 2. Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution. 

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 The views and conclusions contained in the software and documentation are those
 of the authors and should not be interpreted as representing official policies, 
 either expressed or implied, of the FreeBSD Project.
 */
package java.tod;

import java.tod.io._IO;

/**
 * This class provides a method that is called by the JNI side when an exception
 * is generated.
 *
 * @author gpothier
 */
public class ExceptionGeneratedReceiver {
    // Ensures the collector is loaded

    private static EventCollector COLLECTOR = EventCollector.INSTANCE;
    private static final ThreadLocal<Boolean> processingExceptions =
            new ThreadLocal<Boolean>() {
        @Override
        protected Boolean initialValue() {
            return false;
        }
    };
    private static final ThreadLocal<Boolean> ignoreExceptions =
            new ThreadLocal<Boolean>() {
        @Override
        protected Boolean initialValue() {
            return false;
        }
    };

    /**
     * Sets the ignore next exception flag of the current thread. This is called
     * by instrumented classes.
     */
    public static void ignoreNextException() {
        if (AgentReady.COLLECTOR_READY) {
            COLLECTOR.ignoreNextException();
        }
    }

    /**
     * Used to avoid processing exceptions while registered objects are sent.
     */
    public static void setIgnoreExceptions(boolean aIgnore) {
        ignoreExceptions.set(aIgnore);
    }

    public static void exceptionGenerated(
            String aMethodName,
            String aMethodSignature,
            String aMethodDeclaringClassSignature,
            int aOperationBytecodeIndex,
            Throwable aThrowable) {
        try {
            if (ignoreExceptions.get()) {
                return;
            }
            if (!AgentReady.COLLECTOR_READY) {
                return;
            }
            if (!AgentReady.CAPTURE_ENABLED) {
                return;
            }

            if (processingExceptions.get()) {
                _IO.err("[TOD] Recursive exception, probably because we got disconnected from the database.");
                System.exit(1);
            }
            processingExceptions.set(true);

//			//_IO.out(String.format("Exception generated: %s.%s, %d", aMethodDeclaringClassSignature, aMethodName, aOperationBytecodeIndex));
            COLLECTOR.logExceptionGenerated(
                    aMethodName,
                    aMethodSignature,
                    aMethodDeclaringClassSignature,
                    aOperationBytecodeIndex,
                    aThrowable);

            processingExceptions.set(false);
        } catch (Throwable e) {
            _IO.err("[TOD] Exception in ExceptionGeneratedReceiver.exceptionGenerated:");
            e.printStackTrace();
            System.exit(1);
        }
    }
}
