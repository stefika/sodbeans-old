/*
 Copyright (c) 2006-2008, Guillaume Pothier
 All rights reserved.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met: 

 1. Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer. 
 2. Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution. 

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 The views and conclusions contained in the software and documentation are those
 of the authors and should not be interpreted as representing official policies, 
 either expressed or implied, of the FreeBSD Project.
 */
package java.tod;

import tod.agent.BitUtilsLite;

/**
 * A thread that maintains the current timestamp, with a granularity. Permits to
 * avoid too many system calls for obtaining the timestamp.
 *
 * @author gpothier
 */
public class Timestamper extends Thread {

    /**
     * Number of bits to shift timestamp values.
     */
    public static final int TIMESTAMP_ADJUST_SHIFT = TimestampCalibration.shift;
    /**
     * Number of bits of original timestamp values that are considered
     * inaccurate.
     */
    public static final int TIMESTAMP_ADJUST_INACCURACY = TimestampCalibration.inaccuracy;
    /**
     * Mask of artificial timestamp bits.
     */
    public static final long TIMESTAMP_ADJUST_MASK =
            BitUtilsLite.pow2(TIMESTAMP_ADJUST_INACCURACY + TIMESTAMP_ADJUST_SHIFT) - 1;

    private Timestamper() {
        super("[TOD] Timestamper");
        setDaemon(true);
        start();
    }
    public transient static long t = System.nanoTime() << TIMESTAMP_ADJUST_SHIFT;

    @Override
    public void run() {
        try {
            while (true) {
                update();
                sleep(1);
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static long update() {
        t = System.nanoTime() << TIMESTAMP_ADJUST_SHIFT;
        return t;
    }
}
