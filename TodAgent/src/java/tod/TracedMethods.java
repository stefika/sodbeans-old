/*
 Copyright (c) 2006-2008, Guillaume Pothier
 All rights reserved.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met: 

 1. Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer. 
 2. Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution. 

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 The views and conclusions contained in the software and documentation are those
 of the authors and should not be interpreted as representing official policies, 
 either expressed or implied, of the FreeBSD Project.
 */
package java.tod;

/**
 * This class keeps a registry of traced methods. This is used by the
 * instrumentation of method calls: the events generated for a call to a traced
 * method are not the same as those of a non-traced method.
 *
 * @author gpothier
 */
public class TracedMethods {

    private static boolean[] traced = new boolean[10000];

    public static final void setTraced(int aId) {
        if (aId >= traced.length) {
            boolean[] room = new boolean[aId * 2];
            System.arraycopy(traced, 0, room, 0, traced.length);
            //_IO.out("Reallocated TracedMethods: "+room.length);
            traced = room;
        }

        ////_IO.out("Marking traced: "+aId);
        traced[aId] = true;
    }

    public static final boolean isTraced(int aId) {
        ////_IO.out("isTraced: "+aId);
        return aId >= traced.length ? false : traced[aId];
    }
}
