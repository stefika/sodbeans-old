/*
 Copyright (c) 2006-2008, Guillaume Pothier
 All rights reserved.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met: 

 1. Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer. 
 2. Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution. 

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 The views and conclusions contained in the software and documentation are those
 of the authors and should not be interpreted as representing official policies, 
 either expressed or implied, of the FreeBSD Project.
 */
package java.tod.transport;

import java.tod.util._IdentityHashMap;

import tod.agent.ObjectValue;
import tod.agent.ObjectValue.FieldValue;
import tod.agent.io._ByteBuffer;

/**
 * This class handles our custom serialization of objects. See
 * {@link ObjectValue}
 *
 * @author gpothier
 */
public class ObjectEncoder {

    /**
     * Encodes an object into the specified buffer.
     */
    public static void encode(Object aObject, _ByteBuffer aBuffer) {
        encode(aObject, aBuffer, new _IdentityHashMap<ObjectValue, Integer>());
    }

    private static void encode(Object aObject, _ByteBuffer aBuffer, _IdentityHashMap<ObjectValue, Integer> aMapping) {
        if (aObject == null) {
            aBuffer.put(ObjectValue.TYPE_NULL);
        } else if (aObject instanceof String) {
            String v = (String) aObject;
            writeString(v, aBuffer);
        } else if (aObject instanceof Integer) {
            Integer v = (Integer) aObject;
            aBuffer.put(ObjectValue.TYPE_INT);
            aBuffer.putInt(v.intValue());
        } else if (aObject instanceof Long) {
            Long v = (Long) aObject;
            aBuffer.put(ObjectValue.TYPE_LONG);
            aBuffer.putLong(v.longValue());
        } else if (aObject instanceof Byte) {
            Byte v = (Byte) aObject;
            aBuffer.put(ObjectValue.TYPE_BYTE);
            aBuffer.put(v.byteValue());
        } else if (aObject instanceof Short) {
            Short v = (Short) aObject;
            aBuffer.put(ObjectValue.TYPE_SHORT);
            aBuffer.putShort(v.shortValue());
        } else if (aObject instanceof Character) {
            Character v = (Character) aObject;
            aBuffer.put(ObjectValue.TYPE_CHAR);
            aBuffer.putChar(v.charValue());
        } else if (aObject instanceof Float) {
            Float v = (Float) aObject;
            aBuffer.put(ObjectValue.TYPE_FLOAT);
            aBuffer.putFloat(v.floatValue());
        } else if (aObject instanceof Double) {
            Double v = (Double) aObject;
            aBuffer.put(ObjectValue.TYPE_DOUBLE);
            aBuffer.putDouble(v.doubleValue());
        } else if (aObject instanceof Boolean) {
            Boolean v = (Boolean) aObject;
            aBuffer.put(ObjectValue.TYPE_BOOLEAN);
            aBuffer.put(v.booleanValue() ? (byte) 1 : (byte) 0);
        } else if (aObject instanceof _ObjectId) {
            _ObjectId v = (_ObjectId) aObject;
            aBuffer.put(ObjectValue.TYPE_OBJECTID);
            aBuffer.putLong(v.id);
        } else if (aObject instanceof ObjectValue) {
            ObjectValue v = (ObjectValue) aObject;

            Integer theId = aMapping.get(v);
            if (theId == null) {
                theId = aMapping.size() + 1;
                aMapping.put(v, theId);
                writeObjectValue(v, aBuffer, aMapping);
            } else {
                aBuffer.put(ObjectValue.TYPE_REF);
                aBuffer.putInt(theId.intValue());
            }
        } else {
            throw new RuntimeException("Not handled: " + aObject);
        }
    }

    private static void writeString(String v, _ByteBuffer aBuffer) {
        aBuffer.put(ObjectValue.TYPE_STRING);
        aBuffer.putInt(v.length());
        for (int i = 0; i < v.length(); i++) {
            aBuffer.putChar(v.charAt(i));
        }
    }

    private static void writeObjectValue(ObjectValue aObjectValue, _ByteBuffer aBuffer, _IdentityHashMap<ObjectValue, Integer> aMapping) {
        aBuffer.put(ObjectValue.TYPE_VALUE);

        aBuffer.putString(aObjectValue.getClassName());
        aBuffer.put(aObjectValue.isThrowable() ? (byte) 1 : (byte) 0);

        FieldValue[] theFields = aObjectValue.getFields();
        aBuffer.putInt(theFields.length);
        for (FieldValue theField : theFields) {
            aBuffer.putString(theField.fieldName);
            encode(theField.value, aBuffer, aMapping);
        }
    }
}
