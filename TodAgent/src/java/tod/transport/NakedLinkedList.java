/*
 Copyright (c) 2006-2008, Guillaume Pothier
 All rights reserved.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met: 

 1. Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer. 
 2. Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution. 

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 The views and conclusions contained in the software and documentation are those
 of the authors and should not be interpreted as representing official policies, 
 either expressed or implied, of the FreeBSD Project.
 */
package java.tod.transport;

import java.util.NoSuchElementException;

/**
 * Copy of {@link zz.utils.list.NakedLinkedList} (we don't want to depend on
 * zz.utils here).
 *
 * @author gpothier
 */
public class NakedLinkedList<E> {

    private int itsSize = 0;
    private Entry<E> itsRoot;

    public NakedLinkedList() {
        itsRoot = new Entry<E>(null);
        itsRoot.setPrev(itsRoot);
        itsRoot.setNext(itsRoot);
    }

    /**
     * Creates a new entry. Subclasses can override this method to create custom
     * entries.
     */
    public Entry<E> createEntry(E aElement) {
        return new Entry<E>(aElement);
    }

    public int size() {
        return itsSize;
    }

    public void addAfter(Entry<E> aBase, Entry<E> aEntry) {
        assert aEntry.getNext() == null;
        assert aEntry.getPrev() == null;

        aEntry.setPrev(aBase);
        aEntry.setNext(aBase.getNext());
        aBase.getNext().setPrev(aEntry);
        aBase.setNext(aEntry);

        itsSize++;
    }

    public E getLast() {
        return getLastEntry().getValue();
    }

    public Entry<E> getLastEntry() {
        Entry<E> theLast = itsRoot.getPrev();
        if (theLast == itsRoot) {
            throw new NoSuchElementException();
        }
        return theLast;
    }

    public void addLast(E aElement) {
        addLast(createEntry(aElement));
    }

    public void addLast(Entry<E> aEntry) {
        addAfter(itsRoot.getPrev(), aEntry);
    }

    public E getFirst() {
        return getFirstEntry().getValue();
    }

    public Entry<E> getFirstEntry() {
        Entry<E> theFirst = itsRoot.getNext();
        if (theFirst == itsRoot) {
            throw new NoSuchElementException();
        }
        return theFirst;
    }

    public void addFirst(E aElement) {
        addFirst(createEntry(aElement));
    }

    public void addFirst(Entry<E> aEntry) {
        addAfter(itsRoot, aEntry);
    }

    public void remove(Entry<E> aEntry) {
        aEntry.getPrev().setNext(aEntry.getNext());
        aEntry.getNext().setPrev(aEntry.getPrev());
        aEntry.setPrev(null);
        aEntry.setNext(null);
        itsSize--;
    }

    public static class Entry<E> {

        private Entry<E> itsNext;
        private Entry<E> itsPrev;
        private E itsValue;

        public Entry(E aValue) {
            itsValue = aValue;
        }

        Entry<E> getNext() {
            return itsNext;
        }

        void setNext(Entry<E> aNext) {
            itsNext = aNext;
        }

        Entry<E> getPrev() {
            return itsPrev;
        }

        void setPrev(Entry<E> aPrev) {
            itsPrev = aPrev;
        }

        public E getValue() {
            return itsValue;
        }

        public void setValue(E aValue) {
            itsValue = aValue;
        }

        public boolean isAttached() {
            assert (getNext() == null) == (getPrev() == null);
            return getNext() != null;
        }
    }
}
