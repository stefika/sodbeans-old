/*
 Copyright (c) 2006-2008, Guillaume Pothier
 All rights reserved.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met: 

 1. Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer. 
 2. Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution. 

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 The views and conclusions contained in the software and documentation are those
 of the authors and should not be interpreted as representing official policies, 
 either expressed or implied, of the FreeBSD Project.
 */
package java.tod.transport;

import java.tod.io._IO;

import tod.agent.AgentConfig;
import tod.agent.AgentDebugFlags;
import tod.agent.io._ByteBuffer;

/**
 * Emulates a {@link DataOutputStream} to which event packets can be sent. Uses
 * double buffering to handle sending of buffers.
 *
 * @author gpothier
 */
public class PacketBuffer {

    private final IOThread itsIOThread;
    /**
     * Id of the thread that uses this buffer.
     */
    public final int itsThreadId;
    /**
     * The buffer currently written to. When this buffer is full, buffers are
     * swapped. See {@link #swapBuffers()}
     */
    //private _ByteBuffer itsCurrentBuffer;
    /**
     * The "reserve" buffer.
     */
    //private _ByteBuffer itsOtherBuffer;
    /**
     * The buffer that is pending to be sent, if any.
     */
    private _ByteBuffer itsPendingBuffer;
    /**
     * True if the pending buffer starts with a new packet
     */
    private boolean itsPendingCleanStart = true;
    /**
     * True if the pending buffer ends at the end of a packet.
     */
    private boolean itsPendingCleanEnd = true;
    private Object lock = new Object();
    public byte[] theBytes = null;
    public int theLength = 0;
    public boolean theCanSplit = false;
    //private boolean itsCurrentCleanStart = true;
    //private boolean itsCurrentCleanEnd = true;

    PacketBuffer(IOThread aIOThread, int aThreadId) {
        itsIOThread = aIOThread;
        itsThreadId = aThreadId;

        itsPendingBuffer = _ByteBuffer.allocate(AgentConfig.COLLECTOR_BUFFER_SIZE);
        //itsOtherBuffer = _ByteBuffer.allocate(AgentConfig.COLLECTOR_BUFFER_SIZE);
    }

    public int getThreadId() {
        return itsThreadId;
    }

    public _ByteBuffer getPendingBuffer() {
        return itsPendingBuffer;
    }

    public boolean hasCleanStart() {
        return itsPendingCleanStart;
    }

    public boolean hasCleanEnd() {
        return itsPendingCleanEnd;
    }

    /**
     * Remaining bytes in the current buffer.
     */
    private int remaining() {
        return itsPendingBuffer.remaining();
    }

    /**
     * This method is called periodically to prevent data from being held in
     * buffers for too long when a thread is inactive. If this buffer has not
     * been swapped for a long time, this method swaps it.
     */
    public void pleaseSwap() {
        /*if (itsCurrentBuffer.position() == 0) return;
         synchronized (this)
         {
         if (itsPendingBuffer == null) swapBuffers();
         }*/
    }

    /**
     * Sends the content of the current buffer and swaps the buffers. This
     * method might have to wait for the buffer to be sent.
     */
    public synchronized void swapBuffers() {
        ////System.out.println("swapBuffers");
		/*try
         {
         while (itsPendingBuffer != null) {
         //System.out.println("STILL PENDING");
         wait();
         }
         }
         catch (InterruptedException e)
         {
         throw new RuntimeException(e);
         }*/
        // Another thread might have called swapBuffers 
        // during the above wait.
        //if (itsPendingBuffer.position() == 0) return;
        //itsPendingBuffer = itsCurrentBuffer;
        //itsPendingCleanStart = itsCurrentCleanStart;
        //itsPendingCleanEnd = itsCurrentCleanEnd;
        //itsCurrentBuffer = itsOtherBuffer;
        //itsCurrentCleanStart = true;
        //itsCurrentCleanEnd = true;
        //itsOtherBuffer = null;
        //itsIOThread.pushBuffer(this);
        ////System.out.println("done");
    }

    public synchronized void sent() {
        //otifyAll();
    }

    public synchronized void write(byte[] aBuffer, int aLength, boolean aCanSplit) {
        PacketBuffer x = new PacketBuffer(this.itsIOThread, this.itsThreadId);
        x.theBytes = aBuffer.clone();
        x.theLength = aLength;
        x.theCanSplit = aCanSplit;
        this.itsIOThread.pushBuffer(x);
        /*try {wait();
			
         } catch (InterruptedException ex) {}*/
    }
}