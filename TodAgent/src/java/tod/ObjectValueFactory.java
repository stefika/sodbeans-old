/*
 Copyright (c) 2006-2008, Guillaume Pothier
 All rights reserved.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met: 

 1. Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer. 
 2. Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution. 

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 The views and conclusions contained in the software and documentation are those
 of the authors and should not be interpreted as representing official policies, 
 either expressed or implied, of the FreeBSD Project.
 */
package java.tod;

import java.lang.reflect.Field;
import java.tod.transport.LowLevelEventWriter;
import java.tod.transport._ObjectId;
import java.tod.util._IdentityHashMap;

import tod.agent.ObjectValue;
import tod.agent.ObjectValue.FieldValue;
import tod.agent.util._ArrayList;

/**
 * This is part of a trick to avoid loading _IdentityHashMap and similar in
 * non-agent VMs.
 *
 * @author gpothier
 */
public class ObjectValueFactory {

    /**
     * Converts an object to an {@link ObjectValue}, using reflection to obtain
     * field values.
     */
    private static ObjectValue toObjectValue(Object aOriginal, int aDepth, Object aObject, Mapping aMapping) {
        Class<?> theClass = aObject.getClass();
        ObjectValue theResult = new ObjectValue(theClass.getName(), aObject instanceof Throwable);
        aMapping.put(aObject, theResult);

        _ArrayList<FieldValue> theFieldValues = new _ArrayList<FieldValue>();

        while (theClass != null) {
            Field[] theFields = theClass.getDeclaredFields();
            for (Field theField : theFields) {
                boolean theWasAccessible = theField.isAccessible();
                theField.setAccessible(true);

                Object theValue;
                try {
                    theValue = theField.get(aObject);
                } catch (Exception e) {
                    theValue = "Cannot obtain field value: " + e.getMessage();
                }

                theField.setAccessible(theWasAccessible);

                FieldValue theFieldValue = new FieldValue(theField.getName());
                convert(aOriginal, aDepth + 1, theValue, aMapping, theFieldValue);

                theFieldValues.add(theFieldValue);
            }

            theClass = theClass.getSuperclass();
        }

        theResult.setFields(theFieldValues.toArray(new FieldValue[theFieldValues.size()]));

        return theResult;
    }

    /**
     * Ensures that the specified object graph is portable, converting nodes to
     * {@link ObjectValue} as needed.
     */
    public static Object convert(Object aObject) {
        Mapping theMapping = new Mapping();
        FieldValue theResultHolder = new FieldValue(null);
        convert(aObject, 0, aObject, theMapping, theResultHolder);
        theMapping.resolve();
        return theResultHolder.value;
    }

    private static void convert(Object aOriginal, int aDepth, Object aObject, Mapping aMapping, FieldValue aTarget) {
        if (aDepth > 20) {
            //_IO.out("ObjectValueFactory.convert() - big - "+aObject.getClass()+" "+aObject+" "+aOriginal);
        }

        if (aObject == null) {
            aTarget.setValue(null);
        } else if (isPortable(aObject)) {
            aTarget.setValue(aObject);
        } else if (!LowLevelEventWriter.shouldSendByValue(aObject)) {
            aTarget.setValue(new _ObjectId(ObjectIdentity.get(aObject)));
        } else {
            ObjectValue theObjectValue = aMapping.get(aObject);
            if (theObjectValue != null) {
                aTarget.setValue(theObjectValue);
            } else if (aMapping.isProcessing(aObject)) {
                aMapping.register(aTarget, aObject);
            } else {
                aMapping.processing(aObject);
                theObjectValue = toObjectValue(aOriginal, aDepth, aObject, aMapping);
                aTarget.setValue(theObjectValue);
            }
        }
    }

    private static boolean isPortable(Object aObject) {
        return (aObject instanceof String) || (aObject instanceof Number) || (aObject instanceof Boolean);
    }

    private static class Mapping {

        private _IdentityHashMap<Object, ObjectValue> itsMap = new _IdentityHashMap<Object, ObjectValue>();
        private _ArrayList<FieldResolver> itsResolvers = new _ArrayList<FieldResolver>();

        public void processing(Object aKey) {
            itsMap.put(aKey, null);
        }

        public boolean isProcessing(Object aKey) {
            return itsMap.containsKey(aKey);
        }

        public void register(FieldValue aTarget, Object aKey) {
            itsResolvers.add(new FieldResolver(aTarget, aKey));
        }

        public void put(Object aKey, ObjectValue aValue) {
            itsMap.put(aKey, aValue);
        }

        public ObjectValue get(Object aKey) {
            return itsMap.get(aKey);
        }

        public void resolve() {
            for (int i = 0; i < itsResolvers.size(); i++) {
                FieldResolver theResolver = itsResolvers.get(i);
                theResolver.resolve(this);
            }
        }
    }

    private static class FieldResolver {

        private final FieldValue itsFieldValue;
        private final Object itsKey;

        public FieldResolver(FieldValue aFieldValue, Object aKey) {
            itsFieldValue = aFieldValue;
            itsKey = aKey;
        }

        public void resolve(Mapping aMapping) {
            itsFieldValue.setValue(aMapping.get(itsKey));
        }
    }
}
