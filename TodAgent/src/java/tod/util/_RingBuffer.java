/*
 Copyright (c) 2006-2008, Guillaume Pothier
 All rights reserved.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met: 

 1. Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer. 
 2. Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution. 

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 The views and conclusions contained in the software and documentation are those
 of the authors and should not be interpreted as representing official policies, 
 either expressed or implied, of the FreeBSD Project.
 */
package java.tod.util;

public class _RingBuffer<T> {

    private T[] itsBuffer;
    private int itsSize;
    private int itsInputIndex;
    private int itsOutputIndex;

    public _RingBuffer(int aCapacity) {
        itsBuffer = (T[]) new Object[aCapacity];
        itsSize = 0;
        itsInputIndex = 0;
        itsOutputIndex = 0;
    }

    public int getCapacity() {
        return itsBuffer.length;
    }

    public boolean isFull() {
        return itsSize == itsBuffer.length;
    }

    public boolean isEmpty() {
        return itsSize == 0;
    }

    /**
     * Returns the number of elements stored in this buffer.
     */
    public int size() {
        return itsSize;
    }

    public void add(T aObject) {
        if (isFull()) {
            throw new IllegalStateException("Buffer is full");
        }
        itsBuffer[itsInputIndex] = aObject;
        itsInputIndex = (itsInputIndex + 1) % itsBuffer.length;
        itsSize++;
    }

    public T remove() {
        if (isEmpty()) {
            throw new IllegalStateException("Buffer is empty");
        }
        T theObject = itsBuffer[itsOutputIndex];
        itsBuffer[itsOutputIndex] = null;
        itsOutputIndex = (itsOutputIndex + 1) % itsBuffer.length;
        itsSize--;

        return theObject;
    }

    /**
     * Returns the element at the specified index. Index 0 is the last added
     * object that has not been removed.
     */
    public T get(int aIndex) {
        if (aIndex >= itsSize) {
            throw new IndexOutOfBoundsException("" + aIndex + " >= " + itsSize);
        }
        return itsBuffer[(itsOutputIndex + aIndex) % itsBuffer.length];
    }

    /**
     * Returns the element that would be returned by {@link #remove()}, or null
     * if there is no element.
     *
     * @return
     */
    public T peek() {
        if (isEmpty()) {
            return null;
        } else {
            return itsBuffer[itsOutputIndex];
        }
    }

    /**
     * Sets the element at the specified index. Index 0 is the last added object
     * that has not been removed.
     *
     * @return The element that is being overwritten.
     */
    public T set(int aIndex, T aObject) {
        if (aIndex >= itsSize) {
            throw new IndexOutOfBoundsException("" + aIndex + " >= " + itsSize);
        }
        T theOld = itsBuffer[(itsOutputIndex + aIndex) % itsBuffer.length];
        itsBuffer[(itsOutputIndex + aIndex) % itsBuffer.length] = aObject;
        return theOld;
    }
}
