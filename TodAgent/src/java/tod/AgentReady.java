/*
 Copyright (c) 2006-2008, Guillaume Pothier
 All rights reserved.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met: 

 1. Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer. 
 2. Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution. 

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 The views and conclusions contained in the software and documentation are those
 of the authors and should not be interpreted as representing official policies, 
 either expressed or implied, of the FreeBSD Project.
 */
package java.tod;

/**
 * Contains a few flags that indicate the state of the native & java agent, as
 * well as the enabling/disabling of trace capture.
 *
 * @author gpothier
 */
public class AgentReady {

    /**
     * Set to true once the {@link EventCollector} is ready to receive events.
     */
    public static boolean COLLECTOR_READY = false;
    /**
     * This flag is set to true by the native agent, if it is properly loaded.
     */
    private static boolean NATIVE_AGENT_LOADED = false;
    /**
     * Whether trace capture is currently enabled.
     *
     * @see TOD#enableCapture()
     * @see TOD#disableCapture()
     */
    public static boolean CAPTURE_ENABLED = false;

    /**
     * Called by the native agent.
     */
    private static void nativeAgentLoaded() {
        NATIVE_AGENT_LOADED = true;
    }

    /**
     * Whether the native agent is enabled.
     */
    public static boolean isNativeAgentLoaded() {
        return NATIVE_AGENT_LOADED;
    }

    /**
     * Called by the native agent when the system is ready to start capturing
     */
    private static void start() {
        //_IO.out("invoke start");
        EventCollector.INSTANCE.init();
        TOD.loadInitialCaptureState();
    }
}
