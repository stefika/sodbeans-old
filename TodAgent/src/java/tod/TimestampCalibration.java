/*
 Copyright (c) 2006-2008, Guillaume Pothier
 All rights reserved.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met: 

 1. Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer. 
 2. Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution. 

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 The views and conclusions contained in the software and documentation are those
 of the authors and should not be interpreted as representing official policies, 
 either expressed or implied, of the FreeBSD Project.
 */
package java.tod;

import tod.agent.BitUtilsLite;

/**
 * This class performs some calibration of the timestamp parameters
 *
 * @author gpothier
 */
public class TimestampCalibration {

    /**
     * Maximum number of events per second we want to be able to handle.
     */
    private static final long evps = 1000 * 1000 * 1000;

    static {
        calibrate();
    }
    public static int shift;
    public static int inaccuracy;

    /**
     * Calculates the average delay, in ns, between two distinct timestamp
     * values.
     */
    private static void calibrate() {
        long total = 0;
        long c = 0;
        for (int i = 0; i < 10; i++) {
            long d = getTimestampDelta();
//			//_IO.out("[TOD] Delay: "+d+"ns.");
            total += d;
            c++;
        }

        long avg = total / c;

        if (avg > Integer.MAX_VALUE) {
            avg = Integer.MAX_VALUE;
        }

        inaccuracy = BitUtilsLite.log2ceil((int) avg) - 1;

        int extraRange = (int) (evps / 1000000000L);
        shift = Math.max(BitUtilsLite.log2ceil(extraRange), 0);

        //_IO.out("[TOD] Timer calibration done (d: "+avg+", s: "+shift+", i: "+inaccuracy+").");
    }

    /**
     * Returns the time elapsed between two different timestamp values.
     */
    private static long getTimestampDelta() {
        long s = System.nanoTime();
        long s2;
        while ((s2 = System.nanoTime()) == s);

        return s2 - s;
    }

    public static void main(String[] args) {
    }
}
