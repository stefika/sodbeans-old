/*
 Copyright (c) 2006-2008, Guillaume Pothier
 All rights reserved.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met: 

 1. Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer. 
 2. Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution. 

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 The views and conclusions contained in the software and documentation are those
 of the authors and should not be interpreted as representing official policies, 
 either expressed or implied, of the FreeBSD Project.
 */
package java.tod;

import tod.agent.AgentConfig;

/**
 * Provides a helper method that retrieves the id of an object.
 *
 * @author gpothier
 */
public class ObjectIdentity {

    /**
     * Retrieves the identifier of an object. Returns a positive value if the
     * object was already tagged. If this call causes the object to be tagged,
     * the opposite of the actual tag value is returned.
     */
    public static long get(Object aObject) {
        return _AgentConfig.JAVA14 ? get14(aObject) : get15(aObject);
    }

    private static native long get15(Object aObject);
    private static final WeakLongHashMap MAP = _AgentConfig.JAVA14 ? new WeakLongHashMap() : null;
    private static long itsNextId = 1;

    private static synchronized long nextId() {
        return itsNextId++;
    }

    private static long get14(Object aObject) {
        long theId = MAP.get(aObject);
        if (theId != 0) {
            return theId;
        } else {
            theId = (nextId() << AgentConfig.HOST_BITS) | _AgentConfig.HOST_ID;
            MAP.put(aObject, theId);
            return -theId;
        }
    }
}
